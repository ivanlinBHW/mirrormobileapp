/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mirror;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.jht.at_mirror_retail.app";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1060304;
  public static final String VERSION_NAME = "1.6.3";
}
