
package com.facebook.react;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainPackageConfig;
import com.facebook.react.shell.MainReactPackage;
import java.util.Arrays;
import java.util.ArrayList;

// react-native-flipper
import com.facebook.flipper.reactnative.FlipperPackage;
// @react-native-community/async-storage
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
// @react-native-community/blur
import com.cmcewen.blurview.BlurViewPackage;
// @react-native-community/geolocation
import com.reactnativecommunity.geolocation.GeolocationPackage;
// @react-native-community/netinfo
import com.reactnativecommunity.netinfo.NetInfoPackage;
// @react-native-community/slider
import com.reactnativecommunity.slider.ReactSliderPackage;
// @react-native-community/toolbar-android
import com.reactnativecommunity.toolbarandroid.ReactToolbarPackage;
// lottie-react-native
import com.airbnb.android.react.lottie.LottiePackage;
// react-native-android-open-settings
import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;
// react-native-android-wifi-with-ap-status
import com.devstepbcn.wifi.AndroidWifiPackage;
// react-native-app-auth
import com.rnappauth.RNAppAuthPackage;
// react-native-background-timer
import com.ocetnik.timer.BackgroundTimerPackage;
// react-native-ble-plx
import com.polidea.reactnativeble.BlePackage;
// react-native-charts-wrapper
import com.github.wuxudong.rncharts.MPAndroidChartPackage;
// react-native-date-picker
import com.henninghall.date_picker.DatePickerPackage;
// react-native-device-info
import com.learnium.RNDeviceInfo.RNDeviceInfo;
// react-native-email-link
import agency.flexible.react.modules.email.EmailPackage;
// react-native-events
import com.lufinkey.react.eventemitter.RNEventEmitterPackage;
// react-native-fast-image
import com.dylanvann.fastimage.FastImageViewPackage;
// react-native-firebase
import io.invertase.firebase.RNFirebasePackage;
// react-native-fs
import com.rnfs.RNFSPackage;
// react-native-gesture-handler
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
// react-native-google-api-availability-bridge
import com.ivanwu.googleapiavailabilitybridge.ReactNativeGooglePlayServicesPackage;
// react-native-image-marker
import com.jimmydaddy.imagemarker.ImageMarkerPackage;
// react-native-image-picker
import com.imagepicker.ImagePickerPackage;
// react-native-linear-gradient
import com.BV.LinearGradient.LinearGradientPackage;
// react-native-localize
import com.reactcommunity.rnlocalize.RNLocalizePackage;
// react-native-network-info
import com.pusherman.networkinfo.RNNetworkInfoPackage;
// react-native-orientation-locker
import org.wonday.orientation.OrientationPackage;
// react-native-permissions
import com.zoontek.rnpermissions.RNPermissionsPackage;
// react-native-ping
import com.reactlibrary.LHPing.RNReactNativePingPackage;
// react-native-reanimated
import com.swmansion.reanimated.ReanimatedPackage;
// react-native-restart
import com.avishayil.rnrestart.ReactNativeRestartPackage;
// react-native-safe-area-context
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
// react-native-screens
import com.swmansion.rnscreens.RNScreensPackage;
// react-native-sensitive-info
import br.com.classapp.RNSensitiveInfo.RNSensitiveInfoPackage;
// react-native-share
import cl.json.RNSharePackage;
// react-native-system-setting
import com.ninty.system.setting.SystemSettingPackage;
// react-native-vector-icons
import com.oblador.vectoricons.VectorIconsPackage;
// react-native-version-number
import com.apsl.versionnumber.RNVersionNumberPackage;
// react-native-video
import com.brentvatne.react.ReactVideoPackage;
// react-native-view-shot
import fr.greweb.reactnativeviewshot.RNViewShotPackage;
// react-native-webview
import com.reactnativecommunity.webview.RNCWebViewPackage;
// react-native-wifi-reborn
import com.reactlibrary.rnwifi.RNWifiPackage;
// react-native-zeroconf
import com.balthazargronon.RCTZeroconf.ZeroconfReactPackage;
// rn-fetch-blob
import com.RNFetchBlob.RNFetchBlobPackage;

public class PackageList {
  private Application application;
  private ReactNativeHost reactNativeHost;
  private MainPackageConfig mConfig;

  public PackageList(ReactNativeHost reactNativeHost) {
    this(reactNativeHost, null);
  }

  public PackageList(Application application) {
    this(application, null);
  }

  public PackageList(ReactNativeHost reactNativeHost, MainPackageConfig config) {
    this.reactNativeHost = reactNativeHost;
    mConfig = config;
  }

  public PackageList(Application application, MainPackageConfig config) {
    this.reactNativeHost = null;
    this.application = application;
    mConfig = config;
  }

  private ReactNativeHost getReactNativeHost() {
    return this.reactNativeHost;
  }

  private Resources getResources() {
    return this.getApplication().getResources();
  }

  private Application getApplication() {
    if (this.reactNativeHost == null) return this.application;
    return this.reactNativeHost.getApplication();
  }

  private Context getApplicationContext() {
    return this.getApplication().getApplicationContext();
  }

  public ArrayList<ReactPackage> getPackages() {
    return new ArrayList<>(Arrays.<ReactPackage>asList(
      new MainReactPackage(mConfig),
      new FlipperPackage(),
      new AsyncStoragePackage(),
      new BlurViewPackage(),
      new GeolocationPackage(),
      new NetInfoPackage(),
      new ReactSliderPackage(),
      new ReactToolbarPackage(),
      new LottiePackage(),
      new AndroidOpenSettingsPackage(),
      new AndroidWifiPackage(),
      new RNAppAuthPackage(),
      new BackgroundTimerPackage(),
      new BlePackage(),
      new MPAndroidChartPackage(),
      new DatePickerPackage(),
      new RNDeviceInfo(),
      new EmailPackage(),
      new RNEventEmitterPackage(),
      new FastImageViewPackage(),
      new RNFirebasePackage(),
      new RNFSPackage(),
      new RNGestureHandlerPackage(),
      new ReactNativeGooglePlayServicesPackage(),
      new ImageMarkerPackage(),
      new ImagePickerPackage(),
      new LinearGradientPackage(),
      new RNLocalizePackage(),
      new RNNetworkInfoPackage(),
      new OrientationPackage(),
      new RNPermissionsPackage(),
      new RNReactNativePingPackage(),
      new ReanimatedPackage(),
      new ReactNativeRestartPackage(),
      new SafeAreaContextPackage(),
      new RNScreensPackage(),
      new RNSensitiveInfoPackage(),
      new RNSharePackage(),
      new SystemSettingPackage(),
      new VectorIconsPackage(),
      new RNVersionNumberPackage(),
      new ReactVideoPackage(),
      new RNViewShotPackage(),
      new RNCWebViewPackage(),
      new RNWifiPackage(),
      new ZeroconfReactPackage(),
      new RNFetchBlobPackage()
    ));
  }
}
