require('react-native').unstable_enableLogBox();

import 'react-native-url-polyfill/auto';
import 'react-native-gesture-handler';
import 'react-native-console-time-polyfill';

import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';

(() => {
  AppRegistry.registerComponent(appName, () => require('App/App').default);

  console.disableYellowBox = true;
})();
