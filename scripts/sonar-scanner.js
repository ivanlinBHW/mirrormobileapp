const config = require('react-native-config') || {};
const sonarScanner = require('sonarqube-scanner');
const serverUrl = config.SONARQUBE_URL || 'http://localhost:9000';

sonarScanner(
  {
    serverUrl,
    options: {
      'sonar.sources': '.',
      'sonar.inclusions': 'src/**',
      'sonar.javascript.lcov.reportPaths': 'coverage/lcov.info',
    },
  },
  () => {},
);
