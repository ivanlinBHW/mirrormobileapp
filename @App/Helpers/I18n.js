import i18n from 'i18n-js';
import Locales from 'App/Locales';
import { memoize } from 'lodash';
import { I18nManager } from 'react-native';
import * as RNLocalize from 'react-native-localize';

export const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

export const setI18nConfig = () => {
  const fallback = { languageTag: 'en', isRTL: false };

  const { languageTag, isRTL } =
    RNLocalize.findBestAvailableLanguage(Object.keys(Locales)) || fallback;
  translate.cache.clear();
  I18nManager.forceRTL(isRTL);
  i18n.translations = { 
    ['en']: Locales['en']() ,
    [languageTag]: Locales[languageTag]() 
  };
  i18n.locale = languageTag;
  i18n.fallbacks = true;
};

export default {
  setI18nConfig,
  translate,
  t: translate,
};
