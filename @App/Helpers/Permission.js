import UUID from 'uuid-generate';
import firebase from 'react-native-firebase';
import Permissions, { PERMISSIONS } from 'react-native-permissions';
import { Alert, Platform, ToastAndroid } from 'react-native';
import { Permissions as ExpoPermissions } from 'react-native-unimodules';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog } from 'App/Helpers';

export const requestLocationPermission = async () => {
  await ExpoPermissions.askAsync(ExpoPermissions.LOCATION);
};

export const requestFcmPermission = async () => {
  try {
    const { status: getAsyncStatus } = await ExpoPermissions.getAsync(
      ExpoPermissions.NOTIFICATIONS,
      ExpoPermissions.USER_FACING_NOTIFICATIONS,
    );
    console.log('getAsync getAsyncStatus=>', getAsyncStatus);
    if (getAsyncStatus !== 'granted') {
      const { status } = await ExpoPermissions.askAsync(
        ExpoPermissions.NOTIFICATIONS,
        ExpoPermissions.USER_FACING_NOTIFICATIONS,
      );
      console.log('askAsync status=>', status);
      if (status !== 'granted') {
        Dialog.requestNotificationPermissionFromSystemAlert();
      }
    }
  } catch (error) {
    __DEV__ && console.log('@requestFcmPermission: permission rejected, ', error);
  }
};

export const checkFcmPermission = async () => {
  try {
    const { status } = await ExpoPermissions.getAsync(
      ExpoPermissions.NOTIFICATIONS,
      ExpoPermissions.USER_FACING_NOTIFICATIONS,
    );
    console.log('checkFcmPermission status=>', status);
    return status === 'granted';
  } catch (error) {
    __DEV__ && console.log('checkFcmPermission rejected, ', error);
  }
};

export const requestFcmToken = async () => {
  let fcmToken = '';
  if (Platform.OS === 'android') {
    const utils = firebase.utils();
    const { isAvailable } = utils.playServicesAvailability;

    if (isAvailable) {
      fcmToken = await firebase.messaging().getToken();
    } else {
      ToastAndroid.show(
        'You should install Google Play Services in order to use Firebase Cloud Messaging.',
        ToastAndroid.SHORT,
      );
    }
  } else {
    fcmToken = await firebase.messaging().getToken();
  }
  if (!fcmToken) {
    fcmToken = UUID.generate();
  }
  return fcmToken;
};

export const permissionType = {
  photo: 'photo',
  camera: 'camera',
  speech: 'speech',
  location: 'location',

  CAMERA: Platform.select({
    ios: PERMISSIONS.IOS.CAMERA,
    android: PERMISSIONS.ANDROID.CAMERA,
  }),
  PHOTO: Platform.select({
    ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
    android: PERMISSIONS.ANDROID.CAMERA,
  }),
  GEOLOCATION_LOW: Platform.select({
    ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    android: PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
  }),
  GEOLOCATION_HIGH: Platform.select({
    ios: PERMISSIONS.IOS.LOCATION_ALWAYS,
    android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
  }),
  BODY_SENSORS: Platform.select({
    ios: PERMISSIONS.IOS.MOTION,
    android: PERMISSIONS.ANDROID.BODY_SENSORS,
  }),
  MICROPHONE: Platform.select({
    ios: PERMISSIONS.IOS.MICROPHONE,
    android: PERMISSIONS.ANDROID.RECORD_AUDIO,
  }),
  SPEECH_RECOGNITION: Platform.select({
    ios: PERMISSIONS.IOS.SPEECH_RECOGNITION,
    android: PERMISSIONS.ANDROID.RECORD_AUDIO,
  }),
  NOTIFICATION: Platform.select({
    ios: PERMISSIONS.IOS.MICROPHONE,
    android: PERMISSIONS.ANDROID.RECORD_AUDIO,
  }),
};

const permissionInfo = (type) => {
  switch (type) {
    case permissionType.PHOTO:
      return {
        title: t('permission_request_title'),
        description: t('permission_request_desc_photo'),
      };
    case permissionType.GEOLOCATION_LOW:
      return {
        title: t('permission_request_title'),
        description: t('permission_request_desc_location'),
      };
    case permissionType.GEOLOCATION_HIGH:
      return {
        title: t('permission_request_title'),
        description: t('permission_request_desc_location'),
      };

    default:
      return {
        title: t('permission_request_title'),
        description: t('permission_request_desc_photo'),
      };
  }
};

export const checkAndRequestPermission = async (
  type,
  requestPermissionWhenCheck = true,
) => {
  try {
    console.log('type=>', type);
    if (type) {
      let permissionStatus;
      if (typeof type === 'object') {
        const checkPermission = await Promise.all(
          type.map((item) => Permissions.check(item)),
        );
        console.log('checkPermission=>', checkPermission);
      } else {
        permissionStatus = await Permissions.check(type);
      }
      console.log('permissionStatus=>', permissionStatus);
      if (permissionStatus === 'granted') {
        return true;
      }
      if (Platform.OS === 'ios' && permissionStatus === 'blocked') {
        Alert.alert(permissionInfo(type).title, permissionInfo(type).description, [
          {
            text: t('__cancel'),
            style: 'cancel',
          },
          { text: t('__open_system_setting'), onPress: Permissions.openSettings },
        ]);
      } else if (Platform.OS === 'android' && permissionStatus === 'blocked') {
        Alert.alert(permissionInfo(type).title, permissionInfo(type).description, [
          {
            text: t('__cancel'),
            style: 'cancel',
          },
          { text: t('__open_system_setting'), onPress: Permissions.openSettings },
        ]);
      } else {
        if (requestPermissionWhenCheck) {
          const res = await Permissions.request(type);
          if (res === 'granted') {
            return true;
          }
        }
        return false;
      }
      return false;
    }
    return true;
  } catch (error) {
    console.log('check and request permission error', error);
  }
};

export default {
  checkFcmPermission,
  requestFcmToken,
  requestFcmPermission,
  requestLocationPermission,
  checkAndRequestPermission,
  permissionType,
};
