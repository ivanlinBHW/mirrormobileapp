// import Spotify from 'rn-spotify-sdk';
import { Alert, Platform } from 'react-native';

import { store } from 'App/App';
import { Handler } from 'App/Api';
import { Config } from 'App/Config';
import { translate as t } from 'App/Helpers/I18n';
import { SpotifyActions, MirrorActions, AppStore } from 'App/Stores';

const Spotify = {
  isInitializedAsync: () => {},
  initialize: () => {},
  loginWithSession: () => {},
  getSessionAsync: () => {},
  renewSession: () => {},
  authenticate: () => {},
  logout: () => {},
  sendRequest: () => {},
};

import { authorize, refresh, revoke } from 'react-native-app-auth';

const spotifyAuthConfig = {
  scopes: Config.SPOTIFY_SCOPES,
  clientId: Config.SPOTIFY_CLIENT_ID,
  redirectUrl: `ts.com.johnsonfitness.mirror.client:${
    Platform.OS === 'ios' ? '/' : '//'
  }spotify-oauthredirect`,
  serviceConfiguration: {
    authorizationEndpoint: 'https://accounts.spotify.com/authorize',
    tokenEndpoint:
      Config.SPOTIFY_TOKEN_SWAP_URL || 'https://accounts.spotify.com/api/token',
  },
};

export const refreshLogin = async () => {
  const { spotify: { currentSession: { refreshToken } = {} } = {} } = AppStore.getState();

  console.log('spotify refreshLogin refreshToken=>', refreshToken);
  const result = await refresh(
    {
      ...spotifyAuthConfig,
      serviceConfiguration: {
        authorizationEndpoint: 'https://accounts.spotify.com/authorize',
        tokenEndpoint: Config.SPOTIFY_TOKEN_REFRESH_URL,
      },
    },
    {
      refreshToken: refreshToken,
    },
  );
  console.log('result=>', result);

  store.dispatch(SpotifyActions.onSpotifyLogin(result));
  return result;
};

export const initializeIfNeeded = async ({
  scopes = Config.SPOTIFY_SCOPES || [],
  clientID = Config.SPOTIFY_CLIENT_ID || '',
  secret = Config.SPOTIFY_CLIENT_SECRET || '',
  redirectURL = Config.SPOTIFY_REDIRECT_URL || '',
  tokenSwapURL = Config.SPOTIFY_TOKEN_SWAP_URL || '',
  tokenRefreshURL = Config.SPOTIFY_TOKEN_REFRESH_URL || '',
  tokenRefreshEarliness = Config.SPOTIFY_TOKEN_REFRESH_EARLINESS || 300,
  sessionUserDefaultsKey = Config.SPOTIFY_SESSION_USER_DEFAULTS_KEY || 'SpotifySession',
} = {}) => {
  try {
    let loggedIn = false;
    if (!(await Spotify.isInitializedAsync())) {
      loggedIn = await Spotify.initialize({
        scopes,
        clientID,
        secret,
        redirectURL,
        tokenSwapURL,
        tokenRefreshURL,
        tokenRefreshEarliness,
        sessionUserDefaultsKey,
      });
    }
    const {
      user: {
        spotifyTokens: { expireTime = 0, accessToken = '', refreshToken = '' } = {},
      },
    } = store.getState();
    if (!loggedIn && (accessToken && refreshToken && expireTime)) {
      store.dispatch(
        MirrorActions.onMirrorLog(
          `User's Spotify token founded, try to auto login with session.`,
        ),
      );
      return Spotify.loginWithSession({
        showDialog: false,
        expireTime,
        accessToken,
        refreshToken,
        scopes: Config.SPOTIFY_SCOPES || [],
      })
        .then(async (e) => {
          const session = await Spotify.getSessionAsync();
          store.dispatch(SpotifyActions.onSpotifyLogin(session));
          return true;
        })
        .catch(async (e) => {
          __DEV__ && Alert.alert('Spotify loginWithSession Error', e.message);
          if (e.message.includes('credentials')) {
            await Spotify.renewSession();
          }
          throw e;
        });
    } else if (loggedIn) {
      await Spotify.renewSession();
    }
    return loggedIn;
  } catch (e) {
    __DEV__ && Alert.alert('Spotify initialization Error', e.message);
    store.dispatch(
      MirrorActions.onMirrorLog(`Spotify initialization Error: ${e.message}.`),
    );
    if (e.message.includes('credentials')) {
      await Spotify.renewSession();
    } else {
      logout();
    }
    return false;
  }
};

export const login = async ({
  scopes = Config.SPOTIFY_SCOPES || [],
  clientID = Config.SPOTIFY_CLIENT_ID || '',
  redirectURL = Config.SPOTIFY_REDIRECT_URL || '',
  tokenSwapURL = Config.SPOTIFY_TOKEN_SWAP_URL || '',
  tokenRefreshURL = Config.SPOTIFY_TOKEN_REFRESH_URL || '',
} = {}) => {
  try {

    const session = await authorize(spotifyAuthConfig);
    console.log('SpotifyHelper session=>', session);
    if (session) {
      store.dispatch(SpotifyActions.onSpotifyLogin(session));
    }
    return !!session;
  } catch (e) {
    if (typeof e.message === 'string' && e.message.includes('Premium')) {
      Alert.alert(t('alert_title_oops'), t('_spotify_account_not_premium'));
    } else {
      __DEV__ && Alert.alert('Spotify login Error', e.message);
    }
    store.dispatch(MirrorActions.onMirrorLog(`Spotify login Error: ${e.message}.`));
    return false;
  }
};

export const logout = async () => {
  store.dispatch(SpotifyActions.onSpotifyLogout());
};

export const getMyPlaylists = async (accessToken) => {
  try {
    console.log('accessToken=>', accessToken);
    const { data: res } = await Handler.get({
      Authorization: accessToken,
      isDefaultHandlerEnable: false,
      refreshTokenHandler: refreshLogin,
      errorHandler: async (err) => {
        console.log('getMyPlaylists err=>', err);
        if (err.status === 401) {
          await login();
        }
      },
    })('https://api.spotify.com/v1/me/playlists');
    console.log('getMyPlaylists res=>', res);
    const { spotify: { playlists = [] } = {} } = AppStore.getState();
    const playlistsWithPlayingStatus = res.items.map((e) => {
      const isPlaying = playlists.some((p) => p.id === e.id && p.isPlaying);
      console.log('isPlaying=>', isPlaying);
      return {
        ...e,
        isPlaying: playlists.some((p) => p.id === e.id && p.isPlaying),
      };
    });
    store.dispatch(SpotifyActions.onSpotifyUpdatePlayLists(playlistsWithPlayingStatus));
    return res.items;
  } catch (e) {
    __DEV__ && Alert.alert('Spotify getMyPlaylists Error', e.message);
    store.dispatch(
      MirrorActions.onMirrorLog(`Spotify getMyPlaylists Error: ${e.message}.`),
    );
    if (typeof e.message === 'string' && e.message.includes('token')) {
    }
    return false;
  }
};

export default {
  ...Spotify,
  getMyPlaylists,
  initializeIfNeeded,
  login,
  logout,
  refreshLogin,
};
