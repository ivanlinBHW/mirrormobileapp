import { get, isString } from 'lodash';
import { Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase';

import { Config } from 'App/Config';
import { isJSON } from 'App/Helpers';

const showNotifs = ({
  id,
  title,
  body,
  data = {},
  channelId = Config.CHANNEL_ID,
  priority = firebase.notifications.Android.Priority.High,
  icon = 'ic_stat_app_notification_logo_white',
  sound = 'default',
  show_in_foreground = true,
  vibrate = 250,
}) => {
  console.debug('Notifs: showNotifs!');

  const notification = new firebase.notifications.Notification({
    show_in_foreground,
    sound,
  })
    .setNotificationId(id)
    .setTitle(title)
    .setBody(body)
    .setData(data);

  if (Platform.OS === 'android') {
    console.debug('Notifs: add Android settings');

    notification.android
      .setAutoCancel(true)
      .android.setChannelId(channelId)
      .android.setSmallIcon(icon || 'ic_stat_app_notification_logo_white')
      .android.setPriority(priority)
      .android.setVibrate(vibrate);
  }

  return firebase
    .notifications()
    .displayNotification(notification)
    .then((res) => {
      console.debug('Notifs: displayNotification!', res);

      return res;
    })
    .catch((err) => {
      console.error(`Notifs: ${err.message}`, JSON.stringify(err, null, 2));

      return err;
    });
};

const onBackgroundMessageReceived = (message) => {
  console.debug('Notifs: onBackgroundMessageReceived=>', message);

  const { data: { default: payload } = {}, messageId } = message;
  console.debug('Notifs: payload=>', payload);

  if (isJSON(payload)) {
    const { title, body, data } = JSON.parse(payload);
    showNotifs({
      id: messageId,
      title,
      body,
      data: data || {},
    });
  }
  return Promise.resolve();
};

const onMessageReceived = (message) => {
  console.debug('Notifs: onMessageReceived=>', message);

  if (isString(get(message, '_data.default'))) {
    const { title, body, data = {} } = JSON.parse(message._data.default);

    let notificationData = {};

    if (data) {
      const { NotifyEventTypeKey } = data;
      if (NotifyEventTypeKey.StringValue === 'training_event_invite') {
        notificationData = {
          notifyEventTypeKey: data.NotifyEventTypeKey.StringValue,
          userFullName: data.UserFullName.StringValue,
          trainingEventId: data.TrainingEventId.StringValue,
          trainingEventTitle: data.TrainingEventTitle.StringValue,
        };
      } else if (NotifyEventTypeKey.StringValue === 'friend_add') {
        notificationData = {
          notifyEventTypeKey: data.NotifyEventTypeKey.StringValue,
          userFullName: data.UserFullName.StringValue,
          friendId: data.friendId.StringValue,
        };
      }

      showNotifs({
        id: message._messageId,
        title,
        body,
        data: notificationData,
      });
    } else if (title && body) {
      showNotifs({
        id: message._messageId,
        title,
        body,
      });
    }
  } else if (message._notificationId && message._title && message._body) {
    let notificationData = {};

    if (message._data) {
      const { NotifyEventTypeKey } = message._data;
      console.debug('Notifs: NotifyEventTypeKey=>', NotifyEventTypeKey);

      if (NotifyEventTypeKey === 'training_event_invite') {
        notificationData = {
          notifyEventTypeKey: NotifyEventTypeKey,
          userFullName: get(message._data, 'UserFullName'),
          trainingEventId: get(message._data, 'TrainingEventId'),
          trainingEventTitle: get(message._data, 'TrainingEventTitle'),
        };
      } else if (NotifyEventTypeKey === 'friend_add') {
        notificationData = {
          notifyEventTypeKey: NotifyEventTypeKey,
          userFullName: get(message._data, 'UserFullName'),
          friendId: get(message._data, 'friendId'),
        };
      }
    }

    return showNotifs({
      id: message._notificationId,
      title: message._title,
      body: message._body,
      data: notificationData,
    });
  }

  return Promise.resolve();
};
const onNotifsOpened = ({
  data,
  isPaired,
  isConnected,
  getUserDetail,
  getTrainingEvent,
  updateMirrorStore,
}) => {
  if (!isPaired) {

    Actions.NotificationScreen();
  } else {
    const notifyEventTypeKey = get(data, 'notifyEventTypeKey', '');
    switch (notifyEventTypeKey) {
      case 'training_event_invite': {
        if (isConnected) {
          getTrainingEvent(data.trainingEventId);
        } else {
          updateMirrorStore({
            preRecordedNextScreen: 'NotificationScreen',
            preRecordedTrainingEventId: data.trainingEventId,
          });
        }
        break;
      }

      case 'friend_add': {
        if (isConnected) {
          getUserDetail(data.friendId);
        } else {
          updateMirrorStore({
            preRecordedNextScreen: 'NotificationScreen',
            preRecordedFriendId: data.friendId,
          });
        }
        break;
      }

      default:
        if (isConnected) {
          Actions.NotificationScreen();
        } else {
          updateMirrorStore({
            preRecordedNextScreen: 'NotificationScreen',
          });
        }
        break;
    }
  }
};

export default {
  onBackgroundMessageReceived,
  onMessageReceived,
  onNotifsOpened,
  showNotifs,
};
