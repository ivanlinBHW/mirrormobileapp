import { Alert, Platform } from 'react-native';
import { authorize, refresh } from 'react-native-app-auth';

import { store } from 'App/App';
import { Handler } from 'App/Api';
import { Config } from 'App/Config';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog, Date as D } from 'App/Helpers';
import { SpotifyActions, MirrorActions, AppStore } from 'App/Stores';

const TAG = '[!] SPOTIFY HELPER';

const IS_DEV = true;

const CONSOLE = {
  groupEnd: () => IS_DEV && console.groupEnd(TAG),
  group: () => IS_DEV && console.group(TAG),
  log: (...arg) => IS_DEV && console.log(...arg),
};

export const DEFAULT_AUTH_CONFIG = {
  scopes: Config.SPOTIFY_SCOPES,
  clientId: Config.SPOTIFY_CLIENT_ID,
  redirectUrl: `ts.com.johnsonfitness.mirror.client:${
    Platform.OS === 'ios' ? '/' : '//'
  }spotify-oauthredirect`,
  serviceConfiguration: {
    authorizationEndpoint: 'https://accounts.spotify.com/authorize',
    tokenEndpoint:
      Config.SPOTIFY_TOKEN_SWAP_URL || 'https://accounts.spotify.com/api/token',
  },
};

export const refreshLogin = async (config = DEFAULT_AUTH_CONFIG) => {
  store.dispatch(SpotifyActions.onSpotifyLog('Preparing to refresh the auth token...'));
  CONSOLE.group();
  try {
    config = Object.assign(DEFAULT_AUTH_CONFIG, config);
    const { spotify: { currentSession = {} } = {} } = AppStore.getState();

    store.dispatch(
      SpotifyActions.onSpotifyLog(
        `Current Session expired at: ${currentSession.accessTokenExpirationDate}`,
      ),
    );
    CONSOLE.log(`currentSession=>`, currentSession);

    let result = await refresh(
      {
        ...config,
        serviceConfiguration: {
          authorizationEndpoint: 'https://accounts.spotify.com/authorize',
          tokenEndpoint: Config.SPOTIFY_TOKEN_REFRESH_URL,
        },
      },
      {
        refreshToken: currentSession.refreshToken,
        refresh_token: currentSession.refreshToken,
      },
    );
    CONSOLE.log(`refreshLogin result=>`, result);

    store.dispatch(
      SpotifyActions.onSpotifyLog(
        `Refresh response: \n\t${JSON.stringify(result, null, 2)}`,
      ),
    );

    if (result.accessToken && result.refreshToken) {
      CONSOLE.log(`Refresh success`);
      store.dispatch(SpotifyActions.onSpotifyLog(`Refresh success!`));
      store.dispatch(
        SpotifyActions.onSpotifyLogin({
          ...result,
          access_token: result.accessToken,
          refresh_token: result.refreshToken,
        }),
      );
    } else {
      CONSOLE.log(`Refresh failed`);
      store.dispatch(SpotifyActions.onSpotifyLog(`Refresh failed.`));
      Dialog.showSpotifyLogoutAlert();
    }
    return result;
  } catch (error) {
    CONSOLE.log(`error=>`, error);
    store.dispatch(SpotifyActions.onSpotifyLogError(error));
    return error;
  } finally {
    CONSOLE.groupEnd();
  }
};

export const checkExpiredStatus = () => {
  CONSOLE.group();
  const { spotify: { isLogin, currentSession } = {} } = AppStore.getState();

  const isExpired = isLogin
    ? D.isAfterDate(new Date(), currentSession.accessTokenExpirationDate)
    : true;
  const countdown = isLogin
    ? D.transformDiffMinuteTime(currentSession.accessTokenExpirationDate)
    : 0;
  CONSOLE.log(`isExpired=>`, isExpired);
  CONSOLE.log(`countdown=>`, countdown);

  CONSOLE.groupEnd();
  return {
    isLogin,
    isExpired,
    countdown,
  };
};

export const initializeIfNeeded = async (config = DEFAULT_AUTH_CONFIG) => {
  let loggedIn = false;
  store.dispatch(
    SpotifyActions.onSpotifyLog('Checking if Spotify need to login or refresh...'),
  );
  CONSOLE.group();
  try {
    const { spotify: { isLogin, currentSession } = {} } = AppStore.getState();
    store.dispatch(
      SpotifyActions.onSpotifyLog(`Login status is: ${JSON.stringify(isLogin)}.`),
    );

    CONSOLE.log(`isLogin=>`, isLogin);
    CONSOLE.log(`currentSession=>`, currentSession);

    if (isLogin && currentSession) {
      store.dispatch(
        SpotifyActions.onSpotifyLog(
          `Current Session expired at: ${currentSession.accessTokenExpirationDate}`,
        ),
      );
      if (currentSession.accessTokenExpirationDate) {
        const { isExpired, countdown } = checkExpiredStatus();
        store.dispatch(
          SpotifyActions.onSpotifyLog(
            `Token is expired: ${isExpired}, time diff is ${countdown} mins.`,
          ),
        );

        CONSOLE.log(`countdown=>`, countdown);

        if (isExpired || (!isExpired && countdown <= 5)) {
          const result = await refreshLogin(config);

          CONSOLE.log(`config=>`, config);
          CONSOLE.log(`initializeIfNeeded refreshLogin result=>`, result);

          if (result.accessToken && result.refreshToken) {
            store.dispatch(
              SpotifyActions.onSpotifyLog(
                `Token refresh success, new expiration is: ${
                  result.accessTokenExpirationDate
                }`,
              ),
            );
            loggedIn = true;
          } else {
            store.dispatch(SpotifyActions.onSpotifyLogout());
            login();
          }
        } else {
          loggedIn = true;
        }
      }
    }

    CONSOLE.log(`loggedIn=>`, loggedIn);
    return loggedIn;
  } catch (e) {
    store.dispatch(SpotifyActions.onSpotifyLogError(e));

    if (typeof e.message === 'string' && e.message.includes('credentials')) {
      await refreshLogin();
    } else {
      store.dispatch(SpotifyActions.onSpotifyLogout());
    }
    return false;
  } finally {
    CONSOLE.groupEnd();
  }
};

export const login = async (config = DEFAULT_AUTH_CONFIG) => {
  CONSOLE.group();
  try {
    const session = await authorize(config);

    if (session.accessTokenExpirationDate) {
      session.accessTokenExpirationDate = D.moment()
        .add(3, 'm')
        .utc()
        .toISOString();
    }
    CONSOLE.log(`login result=>`, session);

    if (session) {
      store.dispatch(SpotifyActions.onSpotifyLogin(session));
    }
    return !!session;
  } catch (e) {
    store.dispatch(SpotifyActions.onSpotifyLogError(e));

    if (typeof e.message === 'string' && e.message.includes('Premium')) {
      Alert.alert(t('alert_title_oops'), t('_spotify_account_not_premium'));
    } else {
      __DEV__ && Alert.alert('Spotify login Error', e.message);
    }
    store.dispatch(MirrorActions.onMirrorLog(`Spotify login Error: ${e.message}.`));
    return false;
  } finally {
    CONSOLE.groupEnd();
  }
};

export const logout = async () => {
  CONSOLE.log(`logout!`);
  store.dispatch(SpotifyActions.onSpotifyLogout());
};

export const cleanLog = () => {
  CONSOLE.log(`cleanLog!`);
  store.dispatch(SpotifyActions.onClearLogs());
};

export const getMyPlaylists = async (accessToken) => {
  try {
    const { data: res } = await Handler.get({
      Authorization: accessToken,
      isDefaultHandlerEnable: false,
      refreshTokenHandler: refreshLogin,
      errorHandler: async (e) => {
        CONSOLE.log('getMyPlaylists e=>', e);

        if (typeof e.message === 'string' && e.message.includes('token')) {
          await refreshLogin();
        }

        if (e.status === 401) {
          await login();
        }
      },
    })('https://api.spotify.com/v1/me/playlists');

    const { spotify: { playlists = [] } = {} } = AppStore.getState();
    const playlistsWithPlayingStatus = res.items.map((e) => {
      const isPlaying = playlists.some((p) => p.id === e.id && p.isPlaying);
      CONSOLE.log('isPlaying=>', isPlaying);
      return {
        ...e,
        isPlaying: playlists.some((p) => p.id === e.id && p.isPlaying),
      };
    });
    store.dispatch(SpotifyActions.onSpotifyUpdatePlayLists(playlistsWithPlayingStatus));
    return res.items;
  } catch (e) {
    store.dispatch(SpotifyActions.onSpotifyLogError(e));
    return false;
  } finally {
    CONSOLE.groupEnd();
  }
};

export const getMe = async (accessToken) => {
  try {
    const { data: res } = await Handler.get({
      Authorization: accessToken,
      refreshTokenHandler: refreshLogin,
      isDefaultHandlerEnable: false,
    })('https://api.spotify.com/v1/me');
    return res;
  } catch (e) {
    store.dispatch(SpotifyActions.onSpotifyLogError(e));
    return e;
  } finally {
    CONSOLE.groupEnd();
  }
};

export const search = async (
  accessToken,
  query,
  type,
  option = { limit: 5, offset: 0 },
) => {
  try {
    CONSOLE.log('query=>', query);
    CONSOLE.log('type=>', type);
    const { data: res } = await Handler.get({
      Authorization: accessToken,
      refreshTokenHandler: refreshLogin,
      isDefaultHandlerEnable: false,

      data: { q: query, type, limit: option.limit, offset: option.offset },
    })('https://api.spotify.com/v1/search');
    return res;
  } catch (e) {
    store.dispatch(SpotifyActions.onSpotifyLogError(e));
    return e;
  }
};

export default {
  search,
  getMe,
  getMyPlaylists,
  cleanLog,
  initializeIfNeeded,
  login,
  logout,
  refreshLogin,
  checkExpiredStatus,
};
