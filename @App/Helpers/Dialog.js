import { Alert, Linking, Platform } from 'react-native';
import {
  AppStateActions,
  AppStore,
  DeviceVersionInfoActions,
  MirrorActions,
  UserActions,
} from 'App/Stores';

import { Actions } from 'react-native-router-flux';
import AndroidOpenSettings from 'react-native-android-open-settings';
import { Config } from 'App/Config';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import Permissions from 'react-native-permissions';
import { translate as t } from 'App/Helpers/I18n';
import { get, debounce } from 'lodash';

export const requestCompareEventAlert = ({
  name = '',
  onPressNo = () => {},
  onPressYes = () => {},
}) => {
  console.log('======= requestCompareEventAlert ==========');
  return Alert.alert(t('compare_alert_title'), t('compare_alert_content', { name }), [
    {
      text: t('compare_alert_no'),
      onPress: () => {
        AppStore.dispatch(
          AppStateActions.onAlertingChange({ isGoBackPageAlerting: false }),
        );
        onPressNo();
      },
    },
    {
      text: t('compare_alert_yes'),
      onPress: () => {
        AppStore.dispatch(
          AppStateActions.onAlertingChange({ isGoBackPageAlerting: false }),
        );
        AppStore.dispatch(MirrorActions.lastEventThing());
      },
    },
  ]);
};

export const exitFamilyAccountAlert = () => {
  return Alert.alert(
    t('setting_account_family_del_subscription_code_with_member_title'),
    t('setting_account_family_quit_family_desc'),
    [
      {
        text: t('program_detail_leave'),
        onPress: () => AppStore.dispatch(UserActions.fetchDelMemberSubscription()),
      },
      {
        style: 'cancel',
        text: t('__cancel'),
      },
    ],
  );
};

export const delSubscriptionCodeWithMemberAlert = (id) => () => {
  return Alert.alert(
    t('setting_account_family_del_subscription_code_with_member_title'),
    t('setting_account_family_del_subscription_code_with_member_desc'),
    [
      {
        style: 'destructive',
        text: t('__delete'),
        onPress: () => AppStore.dispatch(UserActions.fetchDelMemberSubscriptionCode(id)),
      },
      {
        style: 'cancel',
        text: t('__cancel'),
      },
    ],
  );
};

export const requestDistanceAdjustmentWhenEnableMotionCapture = (callback) => {
  return Alert.alert(t('setting_mii_distance_mesuring'), t('distance_alert_content'), [
    {
      text: t('distance_alert_adjust'),
      onPress: () => {
        callback(true);
        AppStore.dispatch(AppStateActions.onLoading(true));
      },
    },
    {
      style: 'cancel',
      text: t('__skip'),
      onPress: () => {
        AppStore.dispatch(AppStateActions.onLoading(false));
        callback();
      },
    },
    {
      text: t('distance_alert_dont_remind_me'),
      onPress: () => {
        AppStore.dispatch(AppStateActions.onLoading(false));
        AppStore.dispatch(UserActions.updateUserStore({ isNextShowDistance: false }));
        callback();
      },
    },
  ]);
};

export const requestAccountSubscriptionCodeAlert = (forceCb) => () => {
  const buttonArray = [
    {
      text: t('__enter'),
      onPress: Actions.EnterSubscriptionScreen,
    },
    {
      style: 'cancel',
      text: t('__cancel'),
    },
  ];
  if (__DEV__) {
    buttonArray.push({
      text: 'Just do it (DEV)',
      onPress: () => requestAnimationFrame(forceCb),
    });
  }
  return Alert.alert(
    t('class_detail_request_account_subscription_alert_title'),
    t('class_detail_request_account_subscription_alert_desc'),
    buttonArray,
  );
};

export const requestAccountSubscriptionExpiredAlert = (forceCb) => () => {
  const buttonArray = [
    {
      text: t('__pay'),
      onPress: () => Linking.openURL(Config.SHOP_LINK),
    },
    {
      style: 'cancel',
      text: t('__cancel'),
    },
  ];
  if (__DEV__) {
    buttonArray.push({
      text: 'Just do it (DEV)',
      onPress: () => requestAnimationFrame(forceCb),
    });
  }
  return Alert.alert(
    t('class_detail_request_account_subscription_expired_title'),
    t('class_detail_request_account_subscription_expired_desc'),
    buttonArray,
  );
};

export const requestClassSubscriptionAlert = (forceCb) => () => {
  const buttonArray = [
    {
      text: t('__pay'),
      onPress: () => Linking.openURL(Config.SHOP_LINK),
    },
    {
      style: 'cancel',
      text: t('__cancel'),
    },
  ];
  if (__DEV__) {
    buttonArray.push({
      text: 'Just do it (DEV)',
      onPress: () => requestAnimationFrame(forceCb),
    });
  }
  return Alert.alert(
    t('class_detail_request_class_subscription_alert_title'),
    t('class_detail_request_class_subscription_alert_desc'),
    buttonArray,
  );
};

export const send1on1NotificationSuccessAlert = () => {
  return Alert.alert(
    t('community_1on1_alert_send_success_title'),
    t('community_1on1_alert_send_success_content'),
  );
};

export const reserveNew1on1EventSuccessAlert = () => {
  return Alert.alert(
    t('community_1on1_reserve_success_title'),
    t('community_1on1_reserve_success_content'),
    [
      {
        text: t('__ok'),
        onPress: () => Actions.pop(),
      },
    ],
  );
};

export const requestCameraPermissionFromSystemAlert = () => {
  return Alert.alert(
    t('permission_no_camera_title'),
    t('permission_request_desc_camera'),
    [
      {
        text: t('permission_request_open_setting'),
        onPress: Permissions.openSettings,
      },
      {
        style: 'cancel',
        text: t('__cancel'),
      },
    ],
  );
};

export const requestNotificationPermissionFromSystemAlert = () => {
  return Alert.alert(
    t('permission_request_title'),
    t('permission_request_desc_notification'),
    [
      {
        text: t('permission_request_open_setting'),
        onPress: Permissions.openSettings,
      },
      {
        style: 'cancel',
        text: t('__cancel'),
      },
    ],
  );
};

export const requestBluetoothPermissionFromSystemAlert = () => {
  return Alert.alert(
    t('permission_no_bluetooth_title'),
    t('permission_request_desc_bluetooth'),
    [
      {
        text: t('permission_request_open_setting'),
        onPress: Permissions.openSettings,
      },
      {
        style: 'cancel',
        text: t('__cancel'),
      },
    ],
  );
};

export const bluetoothPowerOffAlert = () => {
  return Alert.alert(t('bluetooth_power_off_title'), t('bluetooth_power_off_desc'), [
    {
      text: t('permission_request_open_setting'),
      onPress:
        Platform.OS === 'android'
          ? AndroidOpenSettings.bluetoothSettings
          : () => Linking.openURL('App-Prefs:root=BLUETOOTH'),
    },
    {
      style: 'cancel',
      text: t('__cancel'),
    },
  ]);
};

export const bluetoothUnsupportedAlert = () => {
  return Alert.alert(t('bluetooth_unsupported_title'), t('bluetooth_unsupported_desc'), [
    {
      text: t('permission_request_open_setting'),
      onPress:
        Platform.OS === 'android'
          ? AndroidOpenSettings.bluetoothSettings
          : () => Linking.openURL('App-Prefs:root=BLUETOOTH'),
    },
    {
      style: 'cancel',
      text: t('__cancel'),
    },
  ]);
};

export const offlineAlert = (callback) => {
  return Alert.alert(t('modal_offline'), t('modal_offline_desc'), [
    {
      text: t('__ok'),
      onPress: callback,
    },
  ]);
};

export const iosNeedToUpdateAlert = (callback) => {
  return Alert.alert(t('alert_title_oops'), t('ios_need_to_update'), [
    {
      text: t('__ok'),
      onPress: callback,
    },
  ]);
};

export const showConfirmChangeSubscriptionCodeAlert = (callback) => {
  return Alert.alert(
    t('alert_title_oops'),
    t('subscription_code_account_change_warring'),
    [
      {
        text: t('__ok'),
        onPress: callback,
      },
      {
        style: 'cancel',
        text: t('__cancel'),
      },
    ],
  );
};

export const requestUserReLoginDueToRegionChangedAlert = (callback) => {
  return Alert.alert(t('alert_title_oops'), t('need_relogin_due_to_region_changed'), [
    {
      text: t('__ok'),
      onPress: callback,
    },
  ]);
};

export const regionIsNotSupportAlert = (callback) => () => {
  return Alert.alert(t('alert_title_oops'), t('current_region_is_not_support'), [
    {
      text: t('__ok'),
      onPress: callback,
    },
  ]);
};

export const showConfirmCameraUsageAlert = (callback) => {
  return Alert.alert(t('community_1on1_notice'), t('community_1on1_notice_content'), [
    {
      text: t('__allow'),
      onPress: callback,
    },
    {
      style: 'cancel',
      text: t('__cancel'),
    },
  ]);
};

export const showConfirmConnectLastDeviceAlert = (lastDeviceName, connector) => {
  return Alert.alert(
    t('mirror_last_device_found'),
    t('mirror_last_device_found_content', { deviceName: lastDeviceName }),
    [
      {
        text: t('__ok'),
        onPress: () => connector(),
      },
      {
        style: 'cancel',
        text: t('__cancel'),
        onPress: () => {
          AppStore.dispatch(
            AppStateActions.onAlertingChange({ isAutoConnectAlerting: false }),
          );
        },
      },
    ],
  );
};

export const showConfirmDeleteAcclountAlert = (callback) => {
  return Alert.alert(
    t('6-7-1-a_delete_account_dialog_title'),
    t('6-7-1-a_delete_account_dialog_content'),
    [
      {
        text: t('__ok'),
        onPress: callback,
      },
      {
        style: 'cancel',
        text: t('__cancel'),
      },
    ],
  );
};
function openAppStore() {
  if (Platform.OS === 'android') {
    Linking.openURL('market://details?id=com.jht.at_mirror_retail.app');
  } else if (Platform.OS === 'ios') {
    const link = 'itms-apps://apps.apple.com/id/app/johnson-mirror/id1517944953';
    Linking.canOpenURL(link).then(
      (supported) => {
        supported && Linking.openURL(link);
      },
      (err) => console.log(err),
    );
  }
}

export const requestAppUpdateOrNotAlert = (skippedVersion, callback) => {
  return Alert.alert(t('alert_update_app_version'), t('request_app_update_or_not'), [
    {
      style: 'cancel',
      text: t('__no'),
      onPress: () => {
        if (callback) callback();
      },
    },
    {
      text: t('__ok'),
      onPress: () => {
        openAppStore();
      },
    },
  ]);
};

export const requestAppMustUpdateAlert = (callback) => {
  return Alert.alert(t('alert_update_app_version1'), t('request_app-must_update'), [
    {
      style: 'cancel',
      text: t('__no'),
      onPress: () => {
        console.log('=== requestAppMustUpdateAlert no ===');
        if (callback) callback();
      },
    },
    {
      text: t('__ok'),

      onPress: () => {
        openAppStore();
      },
    },
  ]);
};

export const requestAppOlderVersionUpdateOrNotAlert = () => {
  return Alert.alert(
    t('alert_update_app_version'),
    t('reques_app_older_version_update_or_not_alert'),
    [
      {
        style: 'cancel',
        text: t('__no'),
        onPress: () => {
          console.log('=====Disconnect Device=====');
          AppStore.dispatch(MirrorActions.onMirrorDisconnectByUser());
        },
      },
      {
        text: t('__ok'),
        onPress: () => {
          AppStore.dispatch(MirrorActions.onMirrorDisconnectByUser());
          openAppStore();
        },
      },
    ],
  );
};

export const requestDeviceOlderVersionUpdateOrNotAlert = (fcmToken) => {
  return Alert.alert(
    t('alert_update_app_version'),
    t('request_device_older_version_update_or_not_alert'),
    [
      {
        style: 'cancel',
        text: t('__no'),
        onPress: () => {
          console.log('=====Disconnect Device=====');
          AppStore.dispatch(MirrorActions.onMirrorDisconnectByUser());
        },
      },
      {
        text: t('__ok'),
        onPress: () => {
          console.log('=====Request Device Update=====');
          AppStore.dispatch(
            MirrorActions.onMirrorCommunicate(MirrorEvents.DEVICE_CHECK_UPDATE_REQUEST, {
              mobileDeviceId: fcmToken,
              updateIfHasNewVersion: true,
            }),
          );
        },
      },
    ],
  );
};

export const showConfirmDisconnectHeartRateDeviceAlert = (callback) => {
  return Alert.alert(
    t('alert_confirm_disconnect_heart_rate_device_title'),
    t('alert_confirm_disconnect_heart_rate_device_content'),
    [
      {
        style: 'cancel',
        text: t('__no'),
        onPress: () => {},
      },
      {
        text: t('__ok'),
        onPress: () => {
          callback();
        },
      },
    ],
  );
};

export const showSpotifyLogoutAlert = (callback) => {
  return Alert.alert(t('alert_title_oops'), t('alert_spotify_logout_content'));
};

export const showClassNotFoundAlert = () => {
  return Alert.alert(t('alert_title_oops'), t('class_404'));
};

export const showApiExceptionAlert = (response) => {
  const button = [
    {
      text: t('__ok'),
      style: 'cancel',
    },
    {
      text: t('more'),
      onPress: () =>
        Alert.alert(
          `${t('more')}`,
          `Status Code: ${response.status}\nErr Message: "${get(
            response,
            'data.message',
            '',
          )}"`,
        ),
    },
  ];
  return Alert.alert(t('alert_title_oops'), t('modal_connect_failure_desc'), button, {
    cancelable: true,
  });
};

export const showServiceUnavailableAlert = (response) => {
  if (global.showServiceUnavailableAlert === true) {
    return false;
  }
  const debouncer = debounce(() => {
    if (global.showServiceUnavailableAlert) {
      global.showServiceUnavailableAlert = false;
    }
  }, 3 * 1000);
  const button = [
    {
      text: t('__ok'),
      onPress: debouncer,
      style: 'cancel',
    },
    {
      text: t('more'),
      onPress: () => {
        Alert.alert(
          `${t('more')}`,
          `Status Code: ${response.status}\nErr Message: "${get(
            response,
            'data.message',
          ) || get(response, 'message')}"`,
          [
            {
              text: t('__ok'),
              onPress: debouncer,
              style: 'cancel',
            },
          ],
        );
      },
    },
  ];
  global.showServiceUnavailableAlert = true;
  return Alert.alert(t('modal_connect_failure'), t('alert_server_unavailable'), button, {
    cancelable: true,
  });
};

export const showRetryOrCancelAlert = (lastMirrorDevice, callbackNo, callbackYes) => {
  if (global.showRetryOrCancelAlert === true) {
    return false;
  }
  const debouncer = (callback) =>
    debounce(() => {
      if (global.showRetryOrCancelAlert) {
        global.showRetryOrCancelAlert = false;
      }
      callback();
    }, 3 * 1000);

  const button = [
    {
      text: lastMirrorDevice ? t('__no') : t('__ok'),
      onPress: debouncer(callbackNo),
    },
  ];
  if (lastMirrorDevice) {
    button.push({
      text: t('__retry'),
      onPress: debouncer(callbackYes),
    });
  }
  global.showRetryOrCancelAlert = true;
  return Alert.alert(
    t('alert_connection_lost_title'),
    lastMirrorDevice ? t('alert_connection_lost_content') : t('alert_connect_failed'),
    button,
    {
      cancelable: false,
    },
  );
};

export const showLoginTimeoutAlert = () => {
  return Alert.alert(t('alert_title_oops'), t('login_session_timeout'));
};

export const showLoginBlacklistedAlert = () => {
  return Alert.alert(t('alert_title_blacklisted'), t('login_session_blacklisted'));
};

export const showDeviceHasNoCameraAlert = () => {
  return Alert.alert(
    t('alert_title_device_no_camera'),
    t('alert_content_device_no_camera'),
  );
};

export const showDeviceNoPartnerWorkoutAlert = () => {
  return Alert.alert(
    t('alert_title_device_no_partner_workout'),
    t('alert_content_device_no_partner_workout'),
  );
};

export default {
  offlineAlert,
  iosNeedToUpdateAlert,
  requestBluetoothPermissionFromSystemAlert,
  requestCameraPermissionFromSystemAlert,
  requestNotificationPermissionFromSystemAlert,
  requestDistanceAdjustmentWhenEnableMotionCapture,
  requestCompareEventAlert,
  requestAccountSubscriptionCodeAlert,
  requestUserReLoginDueToRegionChangedAlert,
  regionIsNotSupportAlert,
  requestClassSubscriptionAlert,
  bluetoothUnsupportedAlert,
  bluetoothPowerOffAlert,
  delSubscriptionCodeWithMemberAlert,
  exitFamilyAccountAlert,
  send1on1NotificationSuccessAlert,
  reserveNew1on1EventSuccessAlert,
  showConfirmChangeSubscriptionCodeAlert,
  showConfirmCameraUsageAlert,
  showConfirmConnectLastDeviceAlert,
  requestAccountSubscriptionExpiredAlert,
  showConfirmDeleteAcclountAlert,
  requestAppUpdateOrNotAlert,
  requestAppMustUpdateAlert,
  requestAppOlderVersionUpdateOrNotAlert,
  requestDeviceOlderVersionUpdateOrNotAlert,
  showConfirmDisconnectHeartRateDeviceAlert,
  showSpotifyLogoutAlert,
  showApiExceptionAlert,
  showClassNotFoundAlert,
  showServiceUnavailableAlert,
  showRetryOrCancelAlert,
  showLoginTimeoutAlert,
  showLoginBlacklistedAlert,
  showDeviceHasNoCameraAlert,
  showDeviceNoPartnerWorkoutAlert,
};
