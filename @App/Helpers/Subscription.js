import { isEmpty, get } from 'lodash';
import { default as d } from './Date';

export const isExpired = (mainSubscription, playerDetail) => {
  let isSubscriptionExpired = true;

  const code = get(playerDetail, 'channel.code');
  console.log('=== training class data ===', playerDetail.channel);

  if (!isEmpty(mainSubscription) && mainSubscription.items) {
    mainSubscription.items.forEach((item) => {
      console.log('=== training class data ===', item);
      if (
        code === item.product.subscriptionTypeId &&
        d.moment() <= d.moment(item.expireAt)
      ) {
        isSubscriptionExpired = false;
      }
    });
  }

  return isSubscriptionExpired;
};

export default { isExpired };
