import { padStart } from 'lodash';
import moment from 'moment';
import 'moment-timezone';
import momentTimezone from 'moment-timezone';
import { translate as t } from 'App/Helpers/I18n';

export const DEFAULT_DATE_FORMAT = 'YYYY/MM/DD';

export const FORMAT_YY_MM_DD_HH_MM_SS = 'YYYY-MM-DD HH:mm:ss';

export const FULL_DATE_FORMAT = 'MMM DD, YYYY [at] hh:mm A';

export const MINUTE_FORMAT = 'hh:mm A';

export const US_DATE_FORMAT = 'MM:DD, YYYY';

export const ACHIEVEMENT_DATE_FORMAT = 'MMM DD, YYYY';

export const formatDate = (date = new Date(), format = DEFAULT_DATE_FORMAT) => {
  if (typeof date === 'string' && date.includes(' ')) {
    return moment(date).format(format);
  }
  return moment(new Date(date)).format(format);
};

export const transformLastOnlineDiff = (count) => {
  if (count < 60) {
    return `${count} ${count <= 1 ? t('__minute') : t('__minutes')}`;
  } else if (count >= 60 && count < 1440) {
    return `${parseInt(count / 60, 10)} ${
      parseInt(count / 60, 10) <= 1 ? t('__hour') : t('__hours')
    }`;
  } else if (count >= 1440 && count < 43200) {
    return `${parseInt(count / 1440, 10)} ${
      parseInt(count / 1440, 10) <= 1 ? t('__day') : t('__days')
    }`;
  } else if (count >= 43200) {
    return `${parseInt(count / 43200, 10)} ${
      parseInt(count / 43200, 10) <= 1 ? t('__month') : t('__months')
    }`;
  }
};

export const transformMinute = (duration) => {
  if (duration > 60) {
    let min = Math.floor(duration / 60);
    let sec = duration - min * 60;
    if (min < 10) {
      min = `0${min}`;
    }
    if (sec < 10) {
      sec = `0${sec}`;
    }
    return `${min}:${sec}`;
  } else if (duration <= 60 && duration > 0) {
    if (duration < 10) {
      return `00:0${duration}`;
    } else {
      return `00:${duration}`;
    }
  } else {
    return '00:00';
  }
};

export const transformDiffTime = (time) => {
  const scheduledAtUTC = momentTimezone.tz(time, 'UTC');
  return moment.duration(scheduledAtUTC.diff(moment()));
};

export const transformDiffMinuteTime = (time) => {
  const scheduledAtUTC = momentTimezone.tz(time, 'UTC');
  return parseInt(moment.duration(scheduledAtUTC.diff(moment())).asMinutes(), 10);
};

export const transformDiffSecondTime = (time, timezone = 'UTC') => {
  const scheduledAtUTC = momentTimezone.tz(time, timezone);
  return parseInt(moment.duration(scheduledAtUTC.diff(moment())).asSeconds(), 10);
};

export const transformDate = (
  time = new Date(),
  timezone = 'UTC',
  format = FORMAT_YY_MM_DD_HH_MM_SS,
) => {
  return momentTimezone.tz(moment.utc(time), timezone).format(format);
};

export const transformDateToUTC = (time, timezone, format = 'YYYY-MM-DD HH:mm:ss') => {
  if (typeof time === 'string' && time.includes(' ')) {
    return momentTimezone
      .tz(moment.utc(moment(time, 'YYYY-MM-DD HH:mm:ss'), timezone))
      .format(format);
  }

  console.log('time=>', time);
  console.log('moment.utc(time)=>', moment.utc(time));
  return momentTimezone.tz(moment.utc(time), timezone).format(format);
};

export const toTz = (time, timezone, format = 'YYYY-MM-DD HH:mm:ss') => {
  return momentTimezone.tz(time, timezone).format(format);
};

export const toUTC = (time, format = 'YYYY-MM-DD HH:mm:ss') => {
  return momentTimezone.tz(time, 'UTC').format(format);
};

export const fromUTC = (time, format = 'YYYY/MM/DD') => {
  return moment.utc(time, format);
};

export const getLastFewDays = (days, fromDate = new Date()) => {
  return moment(fromDate)
    .subtract(days, 'days')
    .format(DEFAULT_DATE_FORMAT);
};

export const getNextFewDays = (days, fromDate = new Date()) => {
  return moment(fromDate)
    .add(days, 'days')
    .format(DEFAULT_DATE_FORMAT);
};

export const getMonthRangeByYear = (
  year = new Date().getFullYear(),
  month = new Date().getMonth() + 1,
) => {
  const startDate = moment([year, month - 1]);
  const endDate = moment(startDate).endOf('month');
  return {
    start: startDate.format(DEFAULT_DATE_FORMAT),
    end: endDate.format(DEFAULT_DATE_FORMAT),
  };
};

export const getMonthRangeByDate = (date, format = DEFAULT_DATE_FORMAT) => {
  const today = moment(date);
  return {
    startDate: today.startOf('month').format(format),
    endDate: today.endOf('month').format(format),
  };
};

export const getWeekRangeByDate = (date, format = DEFAULT_DATE_FORMAT) => {
  const today = moment(date);
  const weekdays = [];
  for (let i = 1; i <= 7; i++) {
    weekdays.push(
      today
        .clone()
        .isoWeekday(i)
        .format(format),
    );
  }
  return {
    startDate: today.startOf('isoWeek').format(format),
    endDate: today.endOf('isoWeek').format(format),
    weekdays,
  };
};

export const hhmmss = (secs) => {
  let minutes = Math.floor(secs / 60);
  secs = secs % 60;
  let hours = Math.floor(minutes / 60);
  minutes = minutes % 60;
  return `${padStart(hours, 2, '0')}:${padStart(minutes, 2, '0')}:${padStart(
    secs,
    2,
    '0',
  )}`;
};

export const mmss = (secs) => {
  return moment.utc(secs * 1000).format('mm:ss');
};

export const mm = (secs) => {
  let minutes = Math.floor(secs / 60);
  return `${padStart(minutes, 2, '0')}`;
};

export const getTimeZoneOffset = (timezone) => {
  const now = moment().utc();
  return 0 - moment.tz.zone(timezone).utcOffset(now) / 60;
};

export const formatUTCDate = (time) => {
  return moment(time, 'YYYYMMDDTHH:mm:ssZZ').format('YYYY-MM-DDTHH:mm:ssZZ');
};

export const isAfterDate = (compareDate, targetDate) => {
  return moment(compareDate).isAfter(targetDate);
};

export const isSameOrAfterDate = (compareDate, targetDate) => {
  return moment(compareDate).isSameOrAfter(targetDate);
};

export const generateDaysByNow = (
  today = new Date(),
  daysToAdd = 30,
  format = DEFAULT_DATE_FORMAT,
) => {
  const days = [];
  const dateStart = moment(today);
  const dateEnd = moment().add(daysToAdd, 'days');
  while (dateEnd.diff(dateStart, 'days') >= 0) {
    days.push(dateStart.format(format));
    dateStart.add(1, 'days');
  }
  return days;
};

export const getUtcDate = (
  date = new Date(),
  {
    dateFormat = DEFAULT_DATE_FORMAT,
    timezone = 'UTC',
    format = DEFAULT_DATE_FORMAT,
  } = {},
) => {
  return momentTimezone.tz(moment.utc(date, dateFormat), timezone).format(format);
};

export default {
  ACHIEVEMENT_DATE_FORMAT,
  DEFAULT_DATE_FORMAT,
  FULL_DATE_FORMAT,
  MINUTE_FORMAT,
  US_DATE_FORMAT,
  generateDaysByNow,
  formatDate,
  transformMinute,
  transformDiffTime,
  transformDiffMinuteTime,
  transformDiffSecondTime,
  transformLastOnlineDiff,
  transformDate,
  transformDateToUTC,
  getLastFewDays,
  getNextFewDays,
  getMonthRangeByYear,
  getWeekRangeByDate,
  formatUTCDate,
  getTimeZoneOffset,
  getMonthRangeByDate,
  hhmmss,
  mmss,
  mm,
  moment,
  toUTC,
  fromUTC,
  isAfterDate,
  isSameOrAfterDate,
  toTz,
  FORMAT_YY_MM_DD_HH_MM_SS,
  getUtcDate,
};
