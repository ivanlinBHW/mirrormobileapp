import { store } from 'App/App';
export const calculate = (data) => {
  const {
    age: uAge = 20,
    settings: { weight: uWeight = 1, uGender = 1 },
  } = store.getState().setting.setting;

  const { heartRate, weight = uWeight, age = uAge, gender = uGender } = data;

  if (gender === 0) {
    const result =
      (-55.0969 + 0.6309 * heartRate + 0.1988 * weight + 0.2017 * age) / 4.184;
    return result > 0 ? result : 0;
  } else if (gender > 0) {
    const result =
      (-20.4022 + 0.4472 * heartRate - 0.1263 * weight + 0.074 * age) / 4.184;
    return result > 0 ? result : 0;
  }
};

export default {
  calculate,
};
