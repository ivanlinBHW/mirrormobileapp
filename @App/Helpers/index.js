import { isEqual, isObject, first } from 'lodash';

export { ScaledSheet as StyleSheet } from 'react-native-size-matters/extend';
export { ScaledSheet } from 'react-native-size-matters/extend';
export { Screen } from '@ublocks-react-native/helper';
export {
  ifIphoneX,
  isIphoneX,
  getStatusBarHeight,
  getBottomSpace,
} from 'react-native-iphone-x-helper';
export { activateKeepAwake, deactivateKeepAwake } from 'expo-keep-awake';

export { default as ApiErrorParser } from './ApiErrorParser';
export { default as Subscription } from './Subscription';
export { default as Permission } from './Permission';
export { default as ApiHandler } from './ApiHandler';
export { default as Calories } from './Calories';
export { default as Spotify } from './Spotify';
export { default as Dialog } from './Dialog';
export { default as Notifs } from './Notifs';
export { default as Photo } from './Photo';
export { default as Date } from './Date';
export { default as I18n } from './I18n';
export { default as Fcm } from './Fcm';

export const shouldUpdate = (nextProps, nextState) => {
  const { sceneKey, routeName } = this.props;
  return (
    routeName === sceneKey &&
    (!isEqual(this.props, nextProps) || !isEqual(this.state, nextState))
  );
};

export const isJSON = (str) => {
  const { isError, attempt } = require('lodash');
  return !isError(attempt(JSON.parse, str));
};

export const getCircularReplacer = () => {
  const seen = new WeakSet();
  return (key, value) => {
    if (typeof value === 'object' && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};

export const getRoundNumber = (val) => Math.round(val * 10) / 10;

export const getS3SignedHeaders = (headers) => {
  if (isObject(headers)) {
    return {
      Host: first(headers.host),
      Authorization: first(headers.authorization),
      'X-Amz-Date': first(headers.xAmzDate),
      'X-Amz-Content-Sha256': first(headers.xAmzContentSha256),
    };
  }
  return undefined;
};
