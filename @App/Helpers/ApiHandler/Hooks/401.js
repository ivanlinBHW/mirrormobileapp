import { has } from 'lodash';
import { AppStore, UserActions } from 'App/Stores';

export default (failedRequest, instance) => {
  const { response } = failedRequest;
  if (has(response, 'headers.authorization')) {
    AppStore.dispatch(UserActions.onUserRefreshToken(response.headers.authorization));

    instance.interceptors.request.use((request) => {
      if (request.headers['Authorization']) {
        request.headers['Authorization'] = response.headers.authorization;
      }
      return request;
    });
  }

  return Promise.resolve();
};
