import axios from 'axios';
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import { Alert } from 'react-native';
import { get } from 'lodash';

import { hook401 } from './Hooks';
import { Config } from 'App/Config';
import { AppStore, AppApiActions } from 'App/Stores';
import DefaultErrorHandler from './DefaultErrorHandler';

const Runtime = async ({
  url,
  method,
  options: {
    Authorization = '',
    data = undefined,
    errorHandler = undefined,
    successHandler = undefined,
    finallyHandler = undefined,
    acceptLanguage = undefined,
    skipAuthRefresh = false,
    isDefaultHandlerEnable = true,
    refreshTokenHandler = hook401,
    header = {},
    cancelToken = undefined,
    ...options
  } = {},
}) => {
  try {
    if (!acceptLanguage) {
      const appLocales = AppStore.getState().appState.currentLocales;
      if (appLocales instanceof Array) {
        acceptLanguage = appLocales
          .map((e) => e.languageTag)
          .slice(0, 3)
          .toString();
      }
    }
    __DEV__ && AppStore.dispatch(AppApiActions.onApiFetching(url, method, options));
    if (!global.canceler || global.canceler.token.reason) {
      global.canceler = axios.CancelToken.source();
    }
    const instance = axios.create({
      baseURL: options.baseURL || Config.API_BASE_URL,

      timeout: options.timeout || Config.API_TIMEOUT || 15 * 1000,

      headers: {
        'Content-Type': 'application/json',
        'Accept-Language': acceptLanguage,
        'X-Device-ADDRESS': get(AppStore.getState(), 'user.fcmToken'),
        ...(Authorization && {
          Authorization: 'Bearer ' + Authorization,
        }),
        ...header,
      },

      cancelToken: cancelToken === undefined ? global.canceler.token : cancelToken,

      ...options,
    });

    createAuthRefreshInterceptor(
      instance,
      (failedRequest) => {
        return (
          typeof refreshTokenHandler === 'function' &&
          refreshTokenHandler(failedRequest, instance)
        );
      },
      {
        skipAuthRefresh,
      },
    );

    if (url && (url.includes('undefined') || url.includes('null'))) {
      const message = `API Url contains "null" or "undefined". Check input API Url: "${url}".`;
      __DEV__ && console.log(message);
      return __DEV__ && Alert.alert('@ApiHandler', message);
    }

    let res = await instance[method](url, data);

    if (typeof successHandler === 'function') {
      await successHandler(res);
    }
    __DEV__ && AppStore.dispatch(AppApiActions.onApiFetchSuccess(res));
    return res;
  } catch (err) {
    let errorObject = err;
    if (err.response) {
      errorObject = err.response;
    }
    __DEV__ && AppStore.dispatch(AppApiActions.onApiFetchFailure(errorObject));

    if (typeof errorHandler === 'function') {
      await errorHandler(errorObject);
    }

    if (isDefaultHandlerEnable) {
      await DefaultErrorHandler(errorObject, axios, global.canceler);
    }
    throw errorObject;
  } finally {
    if (__DEV__) {
    }
    if (typeof finallyHandler === 'function') {
      return await finallyHandler();
    }
  }
};

export default Runtime;
