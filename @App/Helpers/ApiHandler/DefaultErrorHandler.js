import { get, has } from 'lodash';
import { Alert } from 'react-native';

import { Actions } from 'react-native-router-flux';
import { AppStore, UserActions, MirrorActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog } from 'App/Helpers';

global.isAlertOff = true;

const DefaultErrorHandler = (response = {}, axios, canceler) => {
  const isCancel = axios.isCancel(response);
  __DEV__ && console.log('isCancel=>', isCancel);

  __DEV__ &&
    console.warn(
      `API Response Error: ${isCancel ? '(Request Canceled) ' : ''}${response.status ||
        response.message}
      \n[ Request Path ]\n${get(response, 'config.url')}\n\n[ Full Response ]\n`,
      JSON.stringify(response, null, 2),
    );

  let shouldShowAlert = false;

  switch (response.status) {
    case 400: {
      break;
    }
    case 401: {
      if (!has(response, 'headers.authorization')) {
        AppStore.dispatch(UserActions.onUserReset());
        shouldShowAlert = false;
        const {
          appRoute: { routeName },
          mirror: { isConnected },
        } = AppStore.getState();

        if (isConnected) {
          AppStore.dispatch(MirrorActions.onMirrorDisconnectByUser());
        }

        if (
          routeName !== 'UserLoginScreen' &&
          routeName !== 'IntroScreen' &&
          routeName !== 'UserRegisterScreen' &&
          routeName !== 'UserRegisterVerifyEmailScreen' &&
          routeName !== 'SendForgotMailScreen' &&
          routeName !== 'SendForgotMailSuccessScreen'
        ) {
          if (get(response, 'data.message').includes('badRequest.token.blacklisted')) {
            requestAnimationFrame(Dialog.showLoginBlacklistedAlert);
          } else {
            requestAnimationFrame(Dialog.showLoginTimeoutAlert);
          }
          Actions.UserLoginScreen({
            type: 'replace',
            reason: 'timeout',
          });
        }
      }
      break;
    }
    case 500: {
      if (get(response, 'data.message') === 'This app is not allowed in your region.') {
        alert(t('auth_login_region_not_allowed'));
        shouldShowAlert = false;
      } else {
        Dialog.showApiExceptionAlert(response);
      }
      break;
    }
    case 501:
    case 502:
    case 503:
    case 504: {
      if (canceler) {
        canceler.cancel(
          `${response.status} - ${get(response, 'data.message', response.message)}`,
        );
      }
      Dialog.showServiceUnavailableAlert(response);
      break;
    }
    default: {
      if (
        typeof response.message === 'string' &&
        (response.message.includes('Network Error') ||
          response.message.includes('timeout'))
      ) {
        if (canceler) {
          canceler.cancel(
            `${response.status} - ${get(response, 'data.message', response.message)}`,
          );
        }
        Dialog.showServiceUnavailableAlert(response);
      } else {
        __DEV__ &&
          shouldShowAlert &&
          Alert.alert(
            `API ERROR: ${response.statusCode}:`,
            `[Url]: ${get(response, 'config.url') &&
              response.config.url}\n\n[Response]: ${JSON.stringify(response, null, 2)}`,
          );
      }
    }
  }
};

export default DefaultErrorHandler;
