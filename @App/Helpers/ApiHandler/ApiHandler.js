import { get } from 'lodash';
import ApiConst from './ApiConst';
import ApiRuntime from './ApiRuntime';
import { Config } from 'App/Config';
import { AppStore } from 'App/Stores';

export const getBaseUrl = () => {
  const env = get(AppStore.getState(), 'appState.env');
  let baseURL = '';
  if (env && env !== Config.DEFAULT_SERVER_ENV) {
    return Config.API_BASE_URL[env];
  }

  let currentCountryCode = get(AppStore.getState(), 'user.currentCountryCode');
  if (Config.IS_ENABLE_AUTO_REGION_SWITCH && currentCountryCode) {
    if (
      !currentCountryCode ||
      (typeof currentCountryCode === 'string' && currentCountryCode.length < 1)
    ) {
      currentCountryCode = Config.DEFAULT_SERVER_ENV;
    }

    if (!baseURL) {
      return Config.API_BASE_URL[currentCountryCode];
    }
  }

  return Config.API_BASE_URL[Config.DEFAULT_SERVER_ENV];
};

const ApiHandler = (method = ApiConst.GET, options = {}) => async (url) => {
  const TIME_TAG = `${
    ApiConst.TAG
  }[${method.toUpperCase()}][${url}](${new Date().getTime()})`;
  if (__DEV__) {
    console.time(TIME_TAG);
  }

  if ((options && options.baseURL == null) || !options) {
    options.baseURL = getBaseUrl();
  }

  const result = await ApiRuntime({
    url,
    method,
    options,
  });
  if (__DEV__) {
    console.timeEnd(TIME_TAG);
  }
  return result;
};

export default ApiHandler;
