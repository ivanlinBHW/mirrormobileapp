import { Alert, Linking, Platform } from 'react-native';
import { FileSystem } from 'react-native-unimodules';
import Share from 'react-native-share';
import Marker, { Position, ImageFormat } from 'react-native-image-marker';
import * as MediaLibrary from 'expo-media-library';
import * as ImageManipulator from 'expo-image-manipulator';

import { Colors } from 'App/Theme';
import { Permission, Dialog } from 'App/Helpers';
import { translate as t } from './I18n';
import d from './Date';

export const openGallery = () => {
  switch (Platform.OS) {
    case 'ios':
      Linking.openURL('photos-redirect://');
      break;
    case 'android':
      Linking.openURL('content://media/internal/images/media');
      break;
    default:
      console.log('Could not open gallery app');
  }
};

export const base64ToImage = async (base64) => {
  const filePath = `${FileSystem.cacheDirectory}mirror-${d.formatDate(
    new Date(),
    'YYYY-MM-DD-hh-mm-ss',
  )}.jpg`;
  await FileSystem.writeAsStringAsync(
    filePath,
    base64.replace('data:image/png;base64,', '').replace('data:image/jpeg;base64,', ''),
    {
      encoding: FileSystem.EncodingType.Base64,
    },
  );
  return filePath;
};

export const saveToGallery = async (
  imageUri,
  fileName = d.formatDate(new Date(), 'YYYY-MM-DD-HH-mm-ss'),
) => {
  try {
    const permission = await MediaLibrary.getPermissionsAsync();

    if (!permission.granted && permission.canAskAgain) {
      await MediaLibrary.requestPermissionsAsync();
    }

    await MediaLibrary.createAssetAsync(imageUri);
  } catch (e) {
    Alert.alert(t('alert_title_oops'), `${t('alert_save_image_error')}(${e.message})`);
    console.warn(e);
  }
};

export const shareImage = async (image) => {
  try {
    await Share.open({
      title: t('alert_share_image_title'),
      type: 'image/png',
      url: image,
    });
  } catch (e) {
    __DEV__ &&
      Alert.alert(t('alert_title_oops'), `${t('alert_share_image_error')}(${e.message})`);
    console.warn(e);
  }
};

export const appendTextToImage = async (image) => {
  const getUri = (i) => ({ uri: i });
  const common = {
    fontName: 'OpenSans-Regular',
    fontSize: 44,
    scale: 1,
    quality: 100,
    saveFormat: ImageFormat.base64,
  };
  try {
    const imageWithAppend1 = await Marker.markText({
      ...common,
      src: getUri(image),
      text: 'PREFECT BODY',
      color: Colors.secondary,
      position: Position.topCenter,
    });
    const imageWithAppend2 = await Marker.markText({
      ...common,
      src: getUri(imageWithAppend1),
      text: 'Workout Completed',
      color: Colors.secondary,
      position: Position.topCenter,
      y: 50,
    });
    const imageWithAppend3 = await Marker.markText({
      ...common,
      src: getUri(imageWithAppend2),
      text: `Duration\n14:15`,
      color: Colors.secondary,
      position: Position.bottomLeft,
    });
    const imageWithAppend4 = await Marker.markText({
      ...common,
      src: getUri(imageWithAppend3),
      text: `Calories\n101 cal`,
      color: Colors.secondary,
      position: Position.bottomCenter,
    });
    const imageWithAppend5 = await Marker.markText({
      ...common,
      src: getUri(imageWithAppend4),
      text: `MIRROR LOGO`,
      color: Colors.secondary,
      position: Position.bottomRight,
    });
    return imageWithAppend5;
  } catch (e) {
    Alert.alert(t('alert_title_oops'), `${t('alert_append_image_error')}(${e.message})`);
    console.log('================= appendTextToImage error ===================');
    console.log(e);
    console.log('================= appendTextToImage error ===================');
  }
};

export const cropImage = async (
  filePath,
  actions = [
    {
      crop: { originX: 0, originY: 0, width: 380, height: 380 },
    },
  ],
  saveOptions = { compress: 1, base64: true },
) => {
  try {
    return await ImageManipulator.manipulateAsync(filePath, actions, saveOptions);
  } catch (err) {
    console.log('cropImage err =>', err);
  }
};

export default {
  base64ToImage,
  openGallery,
  shareImage,
  saveToGallery,
  appendTextToImage,
  cropImage,
};
