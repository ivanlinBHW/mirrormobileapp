import { AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';

const getToken = async () => {
  let fcmToken = await AsyncStorage.getItem('fcmToken');
  if (!fcmToken) {
    fcmToken = await firebase.messaging().getToken();

    if (fcmToken) {
      await AsyncStorage.setItem('fcmToken', fcmToken);
    }
  }

  return fcmToken;
};

const requestPermission = async () => {
  try {
    await firebase.messaging().requestPermission();
  } catch (error) {
    throw new Error('@FCM: Notification permission not granted.');
  }
};

const attemptToGetToken = async () =>
  requestPermission()
    .then(() => getToken())
    .catch(() => {});

function scheduleLocalNotification(type, delay, id) {}

function displayNotification(notification) {
  firebase.notifications().displayNotification(notification);
  return notification;
}

function getScheduledNotifications() {
  return firebase.notifications().getScheduledNotifications();
}

function cancelAllNotifications() {
  return firebase.notifications().cancelAllNotifications();
}

const createNotificationListeners = ({
  CHANNEL_ID = 'test-channel',
  SUBSCRIBE_TOPIC_NAME = null,

  onDisplayedHandler = () => {},
  onMessageHandler = () => {},
  onIncomingHandler = () => {},
  onOpenedHandler = () => {},
  onTokenRefreshHandler = () => {},
} = {}) => {
  const onIncoming = firebase.notifications().onNotification((notification) => {
    console.debug('Fcm: onIncoming', notification);
    notification.android.setChannelId(CHANNEL_ID);

    if (typeof onIncomingHandler === 'function') {
      return onIncomingHandler(notification);
    }
    return firebase.notifications().displayNotification(notification);
  });

  const onDisplayed = firebase.notifications().onNotificationDisplayed((notification) => {
    console.debug('Fcm: onNotificationDisplayed=>', notification);

    onDisplayedHandler(notification);
  });

  const onOpened = firebase.notifications().onNotificationOpened((notificationOpen) => {
    const notif = notificationOpen.notification;
    console.debug('Fcm: onNotificationOpened=>', notif);

    onOpenedHandler(notif);
  });

  const onTokenRefresh = firebase.messaging().onTokenRefresh((token) => {
    onTokenRefreshHandler(token);
  });

  const onMessage = firebase.messaging().onMessage((message) => {
    console.debug('Fcm: onMessage=>', message, message.sentTime);
    onMessageHandler(message);
  });
  if (SUBSCRIBE_TOPIC_NAME) {
    firebase.messaging().subscribeToTopic(SUBSCRIBE_TOPIC_NAME);
  }

  return { onIncoming, onOpened, onTokenRefresh, onMessage, onDisplayed };
};

const createAndroidChannel = ({
  CHANNEL_ID,
  CHANNEL_NAME,
  CHANNEL_IMPORTANT_LEVEL = firebase.notifications.Android.Importance.Max,
  CHANNEL_DESCRIPTION = '',
}) => {
  const androidChannel = new firebase.notifications.Android.Channel(
    CHANNEL_ID,
    CHANNEL_NAME,
    CHANNEL_IMPORTANT_LEVEL,
  ).setDescription(CHANNEL_DESCRIPTION);
  firebase.notifications().android.createChannel(androidChannel);
};

const getInitialNotif = (handler = () => {}) => {
  firebase
    .notifications()
    .getInitialNotification()
    .then((notificationOpen) => {
      console.debug('Fcm: onInitialNotification=>', notificationOpen);
      if (notificationOpen) {
        if (typeof handler === 'function') handler(notificationOpen);
      }
    });
};

function removeAllDeliveredNotifications() {
  return firebase.notifications().removeAllDeliveredNotifications();
}

const clearListeners = ({
  onIncoming,
  onOpened,
  onTokenRefresh,
  onMessage,
  onDisplayed,
}) => {
  if (typeof onTokenRefresh === 'function') onTokenRefresh();
  if (typeof onDisplayed === 'function') onDisplayed();
  if (typeof onIncoming === 'function') onIncoming();
  if (typeof onMessage === 'function') onMessage();
  if (typeof onOpened === 'function') onOpened();
};

export default {
  requestPermission,
  attemptToGetToken,
  createNotificationListeners,
  createAndroidChannel,
  getInitialNotif,
  clearListeners,
  displayNotification,
  scheduleLocalNotification,
  getScheduledNotifications,
  removeAllDeliveredNotifications,
  cancelAllNotifications,
};
