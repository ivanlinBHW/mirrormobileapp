/**
 * 將 PHP Larval 錯誤訊息轉為可讀字串，並加上 i18n
 * 原始格式 error.data
 *"data": {
    "success": false,
    "statusCode": 400,
    "message": "badRequest.invalid.parameter",
    "data": {},
    "errors": {
      "password": [
        "validation.min.string"
      ]
    }
  },
 * @param {*} param0 
 */
import { translate as t } from 'App/Helpers/I18n';

export const parse = ({ success, statusCode, message, data = {}, errors = [] }) => {
  let errorMessage = '';

  switch (statusCode) {
    case 400: {
      if (message === 'badRequest.invalid.parameter') {
        errorMessage = Object.keys(errors).map((e) => {
          return `${e}`;
        });
      }
      break;
    }
    default:
      errorMessage = t(message);
      break;
  }

  return errorMessage;
};

export default {
  parse,
};
