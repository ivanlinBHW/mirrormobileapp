import { AppStore } from 'App/Stores';
import { Config } from 'App/Config';

const isRegionSupport = () => {
  let env = AppStore.getState().appState.env;
  if (env !== Config.DEFAULT_SERVER_ENV) return true;

  if (!Config.IS_ENABLE_AUTO_REGION_SWITCH) return true;

  let currentCountryCode = AppStore.getState().user.currentCountryCode;

  if (!currentCountryCode || !Config.API_BASE_URL[currentCountryCode]) {
    return true;
  }
};

export default {
  isRegionSupport,
};
