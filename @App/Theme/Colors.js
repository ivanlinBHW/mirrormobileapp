/**
 * This file contains the application's colors.
 *
 * Define color here instead of duplicating them throughout the components.
 * That allows to change them more easily later on.
 */

const colors = {
  brightBlue: '#007aff',
  turquoiseBlue: '#00a2c0',
  white: '#ffffff',
  white_02: '#f8f8f8',
  black: '#323232',
  black_15: 'rgba(0, 0, 0, 0.15)',
  greyOpacity03: 'rgba(50,50,50,0.3)',
  shadow_black_03: 'rgba(0,0,0,0.3)',
  gray_01: '#545454',
  gray_02: '#a0a0a0',
  gray_03: '#c7c7cd',
  gray_04: '#f0f0f0',
  red_02: '#e75555',
  shadow: 'rgba(50,50,50,0.3)',
  orangeRed: '#ff3b30',
};

const colorSets = {
  transparent: 'rgba(0,0,0,0)',

  primary: colors.red_02,
  primaryContrast: colors.white,
  secondary: colors.gray_02,
  secondaryContrast: colors.white,
  danger: 'red',
};

const button = {
  primary: {
    content: {
      background: colorSets.primary,
      text: colorSets.primaryContrast,
    },
    outline: {
      background: colorSets.primaryContrast,
      text: colorSets.primary,
      border: colorSets.primary,
    },
    text: { background: colorSets.primaryContrast, text: colorSets.primary },
    image: {
      background: colorSets.transparent,
    },
    unSelected: {
      background: colors.white,
      text: colors.black,
      shadow: colors.shadow_black_03,
    },
  },
  secondary: {
    content: {
      background: colorSets.secondary,
      text: colorSets.secondaryContrast,
    },
    outline: {
      background: colorSets.secondaryContrast,
      text: colorSets.secondary,
      border: colorSets.secondary,
    },
    text: { background: colorSets.secondaryContrast, text: colorSets.secondary },
  },
  third: {
    content: {
      background: colors.black,
    },
  },
  delete: {
    text: { text: colors.orangeRed },
  },
};

const icon = {
  primary: colorSets.primary,
};

const inputBox = {
  primary: {
    background: colors.white,
    shadow: colors.greyOpacity03,
  },
};

const checkBox = {
  primary: {
    background: colors.black,
    border: colors.gray_03,
  },
};

const switchBox = {
  primary: {
    false: colors.gray_02,
    true: colorSets.primary,
    text: colorSets.primaryContrast,
  },
};

const radioForm = {
  primary: {
    button: colors.gray_02,
    selected: colorSets.primary,
  },
};

const calendar = {
  primary: {
    selectedDayBackgroundColor: colorSets.primary,
  },
};
const titleText = {
  primary: colors.black,

  danger: colorSets.danger,

  secondary: colors.gray_02,

  third: colors.gray_01,

  active: colorSets.primary,
};

const background = {
  primary: '#f8f8f8',
};
const tab = {
  selected: colorSets.primary,
};

const tabBar = {
  background: colors.white_02,
  focus: colorSets.primary,
  shadow: colors.black,
  text: colors.gray_02,
  focusText: colorSets.primaryContrast,
};

const primaryTabBar = {
  background: colors.shadow,
  shadow: colors.black,
  borderTop: colors.shadow,
  icon: colors.black,
  focusIcon: colorSets.primary,
  focusText: colorSets.primary,
};

const fontIcon = {
  primary: colorSets.primary,
};

const navbar = {
  primary: { background: colors.white_02 },

  community: {
    background: colorSets.transparent,
  },
};

const horizontalLine = {
  primary: colors.black_15,
};

const avatar = {
  primary: {
    border: colorSets.primary,
  },
};

const progressBarChart = {
  primary: colorSets.primary,
};

const slider = {
  primary: {
    track: colorSets.primary,
  },
};

const colorSettings = {
  transparent: 'rgba(0,0,0,0)',
  text: '#212529',
  primary: '#e75555',
  success: '#28a745',
  error: '#dc3545',
  black: '#323232',
  white: '#ffffff',
  white_02: '#f8f8f8',
  white_03: '#F5FCFF',
  gray_02: '#a0a0a0',
  gray_01: '#545454',
  gray_12: 'rgba(118, 118, 128, 0.12)',
  gray_03: '#c7c7cd',
  gray_04: '#f0f0f0',
  secondary: '#3efac3',
  shadow: 'rgba(50,50,50,0.3)',
  shadow_70: 'rgba(50,50,50,0.7)',
  shadow_black_03: 'rgba(0,0,0,0.3)',
  black0: 'rgba(50,50,50,0)',
  deepBlack: '#000',
  black_15: 'rgba(0, 0, 0, 0.15)',
  black_05: 'rgba(0, 0, 0, 0.05)',
  black_80: 'rgba(0, 0, 0, 0.8)',
  black_70: 'rgba(0, 0, 0, 0.7)',
  black_20: 'rgba(0, 0, 0, 0.2)',
  black_10: 'rgba(0, 0, 0, 0.1)',
  goldenYellow: '#ffcc1d',
  brownGrey: '#a0a0a0',
  bright_light_blue: '#1dd0ff',
  very_light_pink: '#f0f0f0',
  weird_green: '#4cd964',
  sky_blue: '#007aff',
  tertiary_02color: '#dc992f',
  green_blue: '#09c58e',
  button,
  inputBox,
  titleText,
  background,
  tabBar,
  primaryTabBar,
  navbar,
  icon,
  horizontalLine,
  checkBox,
  switchBox,
  calendar,
  tab,
  fontIcon,
  progressBarChart,
  avatar,
  radioForm,
  slider,
};

export default colorSettings;
