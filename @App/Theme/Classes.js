import { StyleSheet } from 'react-native';
import { Colors, Metrics } from 'App/Theme';

export default StyleSheet.create({
  fill: {
    flex: 1,
  },
  halfFill: {
    flex: 0.5,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  fullHeight: {
    height: '100%',
  },
  fillCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  rowReverse: {
    flexDirection: 'row-reverse',
  },
  column: {
    flexDirection: 'column',
  },
  columnReverse: {
    flexDirection: 'column-reverse',
  },
  mainStart: {
    justifyContent: 'flex-start',
  },
  mainCenter: {
    justifyContent: 'center',
  },
  mainEnd: {
    justifyContent: 'flex-end',
  },
  mainSpaceBetween: {
    justifyContent: 'space-between',
  },
  mainSpaceAround: {
    justifyContent: 'space-around',
  },
  crossStart: {
    alignItems: 'flex-start',
  },
  crossCenter: {
    alignItems: 'center',
  },
  crossEnd: {
    alignItems: 'flex-end',
  },
  crossStretch: {
    alignItems: 'stretch',
  },
  selfStretch: {
    alignSelf: 'stretch',
  },
  rowMain: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rowCross: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  colMain: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  colCross: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  colCenter: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fillRow: {
    flex: 1,
    flexDirection: 'row',
  },
  fillRowReverse: {
    flex: 1,
    flexDirection: 'row-reverse',
  },
  fillRowMain: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  fillRowCross: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  fillRowCenter: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fillCol: {
    flex: 1,
    flexDirection: 'column',
  },
  fillColReverse: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  fillColMain: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  fillColCross: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  fillColCenter: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textCenter: {
    textAlign: 'center',
  },
  textJustify: {
    textAlign: 'justify',
  },
  textLeft: {
    textAlign: 'left',
  },
  textRight: {
    textAlign: 'right',
  },
  backgroundReset: {
    backgroundColor: 'transparent',
  },
  fullWidth: {
    width: '100%',
  },
  absolute: {
    position: 'absolute',
  },
  white_02_background: {
    backgroundColor: Colors.white_02,
  },
  margin: {
    margin: Metrics.baseMargin,
  },
  marginRight: {
    marginRight: Metrics.baseMargin,
  },
  marginLeft: {
    marginLeft: Metrics.baseMargin,
  },
  marginRightHalf: {
    marginRight: Metrics.baseMargin / 2,
  },
  marginLeftHalf: {
    marginLeft: Metrics.baseMargin / 2,
  },
  marginTop: {
    marginTop: Metrics.baseMargin,
  },
  marginTopHalf: {
    marginTop: Metrics.baseMargin / 2,
  },
  marginBottom: {
    marginBottom: Metrics.baseMargin,
  },
  marginBottomHalf: {
    marginBottom: Metrics.baseMargin / 2,
  },
  padding: {
    padding: Metrics.baseMargin,
  },
  paddingRight: {
    paddingRight: Metrics.baseMargin,
  },
  paddingLeft: {
    paddingLeft: Metrics.baseMargin,
  },
  paddingTop: {
    paddingTop: Metrics.baseMargin,
  },
  paddingBottom: {
    paddingBottom: Metrics.baseMargin,
  },
  paddingHorizontal: {
    paddingHorizontal: Metrics.baseMargin,
  },
  marginHorizontal: {
    marginHorizontal: Metrics.baseMargin,
  },
  paddingVertical: {
    paddingVertical: Metrics.baseMargin,
  },
  marginVertical: {
    marginVertical: Metrics.baseMargin,
  },
});
