/**
 * This file defines the base application styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */
import { Platform } from 'react-native';
import { Colors, Classes, Fonts, Metrics } from 'App/Theme';

export default {
  colorWhite: {
    color: Colors.white,
  },
  colorGreen: {
    color: Colors.secondary,
  },
  colorSkyBlue: {
    color: Colors.sky_blue,
  },
  safeAreaWrapper: {
    flex: 1,
  },
  containerWhite02: {
    flex: 1,
    backgroundColor: Colors.white_02,
  },
  screen: {
    container: {
      flex: 1,
      marginTop: 20,
    },
  },
  layoutHeight: {
    height: '104@vs',
  },
  tabIndicator: {
    backgroundColor: Colors.primary,
  },
  screenPaddingLeft: {
    paddingLeft: '16@vs',
  },
  screenPaddingRight: {
    paddingRight: '16@vs',
  },
  screenMargin: {
    marginHorizontal: Metrics.baseMargin,
  },
  fullMargin: {
    margin: Metrics.baseMargin,
  },
  marginHorizontal: {
    marginHorizontal: Metrics.baseMargin,
  },
  marginVertical: {
    marginVertical: Metrics.baseVerticalMargin,
  },
  layout: {
    height: '100%',
    flex: 1,
    marginTop: 20,
  },
  layoutScreen: {
    paddingLeft: '16@s',
    paddingRight: '16@s',
    height: '100%',
  },
  container: {
    paddingLeft: '16@s',
    paddingRight: '16@s',
  },
  marginContainer: {
    marginHorizontal: Metrics.baseMargin,
  },
  navBar: {
    height: '44@vs',
  },
  modalNavBar: {
    minHeight: '48@vs',
    backgroundColor: Colors.transparent,
  },
  PrimaryTabBar: {
    bottomTab: {
      flexDirection: 'row',
    },
    tab: {
      flex: 1,
      alignItems: 'center',
      paddingTop: '9@vs',
    },
    text: {
      fontSize: Platform.isPad ? '8@s' : '10@s',
      textTransform: 'uppercase',
      paddingBottom: '5@vs',
      paddingTop: '2@vs',
    },
  },
  RoundButton: {
    button: {
      width: '180@s',
      height: '40@sr',
      borderRadius: '20@sr',
      flexDirection: 'row',
      backgroundColor: Colors.button.primary.content.background,
      alignItems: 'center',
    },
    buttonOutline: {
      borderRadius: '6@s',
      borderWidth: '2@vs',
      borderStyle: 'solid',
      borderColor: Colors.button.primary.outline.border,
      backgroundColor: Colors.button.primary.outline.background,
    },
    text: {
      ...Fonts.style.regular,
      color: Colors.button.primary.content.text,
    },
    uppercase: {
      textTransform: 'uppercase',
    },
    bold: {
      fontWeight: 'bold',
    },
    image: {
      width: '30@s',
      height: '30@vs',
      marginRight: '8@vs',
    },
    icon: {
      width: '30@s',
      height: '30@vs',
    },
  },
  BaseTab: {
    tabBox: {
      flexDirection: 'row',
      height: '44@vs',
      marginLeft: '16@s',
      marginRight: '16@s',
      backgroundColor: '#f8f8f8',
      borderRadius: '40@s',
    },
    tab: {
      flex: 1,
      alignItems: 'center',
    },
    text: {
      fontSize: '12@vs',
      lineHeight: 30,
    },
  },
  border: { borderWidth: 1, borderColor: 'black' },
};
