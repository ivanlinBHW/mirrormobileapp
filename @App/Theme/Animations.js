/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  on_one_one_red: require('App/Assets/Animations/1on1-red.json'),
  on_one_one_grey: require('App/Assets/Animations/1on1-grey.json'),
  on_one_one_green: require('App/Assets/Animations/1on1-mid-green.json'),
};
