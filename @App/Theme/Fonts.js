import { Screen } from 'App/Helpers';

const fontFamily = {
  default: 'OpenSans-Regular',
};

const size = {
  h1: Screen.scale(36),
  h2: Screen.scale(34),
  h3: Screen.scale(30),
  h4: Screen.scale(26),
  h5: Screen.scale(24),
  h6: Screen.scale(22),
  h7: Screen.scale(20),
  input: Screen.scale(18),
  regular: Screen.scale(16),
  medium: Screen.scale(14),
  small: Screen.scale(12),
  extraSmall: Screen.scale(10),
};

const style = {
  h1: {
    fontFamily: fontFamily.default,
    fontSize: size.h1,
    fontWeight: 'normal',
  },
  h1_500: {
    fontFamily: fontFamily.default,
    fontSize: size.h1,
    fontWeight: '700',
  },
  h2: {
    fontFamily: fontFamily.default,
    fontSize: size.h2,
    fontWeight: 'normal',
  },
  h3: {
    fontFamily: fontFamily.default,
    fontSize: size.h3,
    fontWeight: 'normal',
  },
  h4: {
    fontFamily: fontFamily.default,
    fontSize: size.h4,
    fontWeight: 'normal',
  },
  h4_500: {
    fontFamily: fontFamily.default,
    fontSize: size.h4,
    fontWeight: '500',
  },
  h5: {
    fontFamily: fontFamily.default,
    fontSize: size.h5,
    fontWeight: 'normal',
  },
  h5_500: {
    fontFamily: fontFamily.default,
    fontSize: size.h5,
    fontWeight: '500',
  },
  h6: {
    fontFamily: fontFamily.default,
    fontSize: size.h6,
    fontWeight: 'normal',
  },
  h6_500: {
    fontFamily: fontFamily.default,
    fontSize: size.h6,
    fontWeight: '500',
  },
  h7: {
    fontFamily: fontFamily.default,
    fontSize: size.h7,
    fontWeight: 'normal',
  },
  h7_500: {
    fontFamily: fontFamily.default,
    fontSize: size.h7,
    fontWeight: '500',
  },
  input600: {
    fontFamily: fontFamily.default,
    fontSize: size.input,
    fontWeight: '700',
  },
  inputBold: {
    fontFamily: fontFamily.default,
    fontSize: size.input,
    fontWeight: 'bold',
  },
  regular: {
    fontFamily: fontFamily.default,
    fontSize: size.regular,
    fontWeight: 'normal',
  },
  regular500: {
    fontFamily: fontFamily.default,
    fontSize: size.regular,
    fontWeight: '700',
  },
  medium: {
    fontFamily: fontFamily.default,
    fontSize: size.medium,
    fontWeight: 'normal',
  },
  medium500: {
    fontFamily: fontFamily.default,
    fontSize: size.medium,
    fontWeight: '700',
  },
  mediumBold: {
    fontFamily: fontFamily.default,
    fontSize: size.medium,
    fontWeight: 'bold',
  },
  small: {
    fontFamily: fontFamily.default,
    fontSize: size.small,
    fontWeight: 'normal',
  },
  small500: {
    fontFamily: fontFamily.default,
    fontSize: size.small,
    fontWeight: '700',
  },
  smallBold: {
    fontFamily: fontFamily.default,
    fontSize: size.small,
    fontWeight: 'bold',
  },
  extraSmall: {
    fontFamily: fontFamily.default,
    fontSize: size.extraSmall,
    fontWeight: 'normal',
  },
  extraSmall500: {
    fontFamily: fontFamily.default,
    fontSize: size.extraSmall,
    fontWeight: '700',
  },
  fontWeight500: {
    fontFamily: fontFamily.default,
    fontWeight: '700',
  },
  bold: { fontWeight: 'bold' },
  italic: { fontStyle: 'italic' },
  underline: { textDecorationLine: 'underline' },
};

export default {
  size,
  style,
  fontFamily,
};
