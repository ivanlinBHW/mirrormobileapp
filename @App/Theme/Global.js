/**
 * This file defines the basic global component styles.
 *
 * Use it to define generic component styles (e.g. the default text styles, default button styles...).
 */

import { Platform } from 'react-native';
import { Screen } from 'App/Helpers';
import Fonts from './Fonts';

export default {
  ViewProps: {},
  ImageProps: {},
  TextProps: {
    style: {
      fontFamily: Fonts.fontFamily.default,
      letterSpacing: 0,
    },
  },
  TextInputProps: {
    style: {
      paddingTop:
        Platform.OS === 'ios' ? Screen.verticalScale(10) : Screen.verticalScale(2),
      alignItems: 'center',
      textAlignVertical: 'bottom',
    },
  },
  TouchableOpacityProps: {},
};
