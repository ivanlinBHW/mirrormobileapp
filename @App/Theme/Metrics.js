/**
 * This file contains metric values that are global to the application.
 */
import { Screen } from 'App/Helpers';

export default {
  baseMargin: Screen.scale(16),
  baseVerticalMargin: Screen.verticalScale(16),
  baseHorizontalMargin: Screen.scale(16),
  baseNavBarHeight: Screen.verticalScale(44),
  baseNavBarHeightRaw: 44,
  homeNavBarHeight: Screen.verticalScale(104),
  homeNavBarHeightRaw: 104,
  homeTabBarItemHeight: Screen.verticalScale(30),
  homeTabBarItemHeightRaw: 30,

  communityNavBarHeight: Screen.verticalScale(150),
};
