import en from './en';
import zh from './zh';
import de from './de';
import th from './th';
import cn from './cn';

export default {
  en: () => en,
  zh: () => zh,
  de: () => de,
  th: () => th,
  'zh-Hans-CN': () => cn,
};
