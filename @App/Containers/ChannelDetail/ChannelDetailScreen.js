import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, View, Text, FlatList } from 'react-native';
import { bindActionCreators } from 'redux';

import { Classes, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { ClassActions } from 'App/Stores';
import {
  BaseButton,
  SecondaryNavbar,
  ClassCard,
  FlatListEmptyLoading,
} from 'App/Components';

import styles from './ChannelDetailScreenStyle';
import { ChannelActions } from 'App/Stores/index';

class ChannelDetailScreen extends React.Component {
  static propTypes = {
    channelDetail: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
    getClassDetail: PropTypes.func.isRequired,
    getChannelSeeAll: PropTypes.func.isRequired,
    setChannelBookmark: PropTypes.func.isRequired,
    delChannelBookmark: PropTypes.func.isRequired,
    token: PropTypes.string,
    channelId: PropTypes.string,
  };

  static defaultProps = {};

  componentDidMount() {
    __DEV__ && console.log('@ChannelDetailScreen');
  }

  onPressSeeAll = (genreId) => {
    const { channelId, getChannelSeeAll } = this.props;
    getChannelSeeAll(channelId, genreId);
  };

  onPressBookmark = (groupId) => (id, isBookmarked) => {
    const { setChannelBookmark, delChannelBookmark } = this.props;
    if (isBookmarked) {
      delChannelBookmark(groupId, id);
    } else {
      setChannelBookmark(groupId, id);
    }
  };

  renderClassItem = (groupId) => ({ item }) => {
    const { getClassDetail } = this.props;
    return (
      <ClassCard
        data={item}
        size="large"
        onPressBookmark={this.onPressBookmark(groupId)}
        onPress={() => getClassDetail(item.id, 'ChannelClassDetailScreen')}
      />
    );
  };

  renderItem = ({ item, index }) => {
    const { channelDetail } = this.props;
    return (
      <View
        style={[styles.itemBox, index < channelDetail.length - 1 && styles.borderWidth]}
      >
        <View style={styles.headerBox}>
          <Text style={styles.title}>{item.group.title}</Text>
          {item.trainingClasses.length > 6 && (
            <BaseButton
              style={styles.seeallBtn}
              text={t('see_all')}
              textStyle={styles.seeallTextColor}
              textColor={Colors.button.primary.text.text}
              transparent
              onPress={() => this.onPressSeeAll(item.group.id)}
            />
          )}
        </View>
        <FlatList
          data={item.trainingClasses}
          renderItem={this.renderClassItem(item.group.id)}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  renderEmpty = () => {
    const { isLoading } = this.props;
    return (
      <View style={styles.loadingBox}>
        <FlatListEmptyLoading isLoading={isLoading} />
      </View>
    );
  };

  render() {
    const { channelDetail } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar navTitle={t('__channel')} back />
        <View style={[Classes.fill, Classes.row, Classes.mainStart]}>
          <FlatList
            data={channelDetail}
            ListEmptyComponent={this.renderEmpty()}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => `class_${index}`}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    channelDetail: state.channel.channelDetail,
    channelId: state.channel.channelId,
    isLoading: state.appState.isLoading,
    token: state.user.token,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getClassDetail: ClassActions.getClassDetail,
        setChannelBookmark: ChannelActions.setChannelBookmark,
        delChannelBookmark: ChannelActions.delChannelBookmark,
        getChannelSeeAll: ChannelActions.getChannelSeeAll,
      },
      dispatch,
    ),
)(ChannelDetailScreen);
