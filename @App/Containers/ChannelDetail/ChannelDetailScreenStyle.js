import { Colors, Fonts, Metrics } from 'App/Theme';

import { ScaledSheet } from 'App/Helpers';

export default ScaledSheet.create({
  cardListFooter: {
    paddingBottom: 128,
    marginBottom: '-16@vs',
  },
  loadingBox: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  seeallBtn: {
    width: '60@s',
    height: 'auto',
    marginRight: Metrics.baseMargin,
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 0,
    paddingBottom: 0,
  },
  seeallTextColor: {
    ...Fonts.style.small500,
  },
  headerBox: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: Metrics.baseMargin / 2,
  },
  title: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
  },
  itemBox: {
    paddingBottom: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin,
  },
  borderWidth: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_15,
  },
});
