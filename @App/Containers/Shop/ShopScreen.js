import React from 'react';
import { WebView } from 'react-native-webview';

import { Classes } from 'App/Theme';
import { Config } from 'App/Config';
import { Screen } from 'App/Helpers';

class ShopScreen extends React.Component {
  static propTypes = {};

  static defaultProps = {};

  componentDidMount() {
    __DEV__ && console.log('@ShopScreen enter');
  }

  componentWillUnmount() {
    __DEV__ && console.log('@ShopScreen leave');
  }

  getInjectedJavascript() {
    return `
    const meta = document.createElement('meta');
    meta.setAttribute('content', 'width='device-width', initial-scale=${Screen.scale(
      0.99,
    )}, maximum-scale=${Screen.scale(0.99)}, user-scalable=0'); 
    meta.setAttribute('name', 'viewport');
    document.getElementsByTagName('head')[0].appendChild(meta);
    document.body.style.userSelect = 'none';
    `;
  }

  render() {
    return (
      <WebView
        injectedJavaScript={this.getInjectedJavascript()}
        scrollEnabled
        scalesPageToFit
        bounces={false}
        allowsFullscreenVideo={false}
        allowsInlineMediaPlayback={true}
        useWebKit={true}
        source={{
          uri: Config.SHOP_LINK,
        }}
        style={Classes.fill}
      />
    );
  }
}

export default ShopScreen;
