import { ScaledSheet, Screen } from 'App/Helpers';
import { Metrics, Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  wrapper: {
    flex: 1,
    height: '100%',
  },
  listBox: {
    height: '100%',
  },
  emptyBox: {
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Screen.scale(266),
  },
  emptyTitle: {
    fontSize: Screen.scale(24),
    color: Colors.black,
    textAlign: 'center',
    ...Fonts.style.fontWeight500,
  },
  emptyContent: {
    ...Fonts.style.small500,
    marginTop: '12@vs',
    color: Colors.gray_01,
    textAlign: 'center',
  },
  cardWrapper: {
    marginBottom: Metrics.baseMargin,
  },
});
