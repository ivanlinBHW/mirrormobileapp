import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, FlatList, View } from 'react-native';
import { bindActionCreators } from 'redux';

import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, EventList } from 'App/Components';
import { CommunityActions } from 'App/Stores';
import { Classes, Colors } from 'App/Theme';
import styles from './AllEventsScreenStyle';

class AllEventsScreen extends React.Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    token: PropTypes.string,
    timezone: PropTypes.string.isRequired,
    joinEvent: PropTypes.func.isRequired,
    getTrainingEvent: PropTypes.func.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@AllEventsScreen');
  }

  renderItem = ({ item, index }) => {
    const { timezone, getTrainingEvent, joinEvent } = this.props;
    return (
      <EventList
        data={item}
        timezone={timezone}
        onPressAdd={() => joinEvent(item.id)}
        onPressDetail={() => getTrainingEvent(item.id)}
        backgroundColor={index % 2 ? Colors.gray_04 : Colors.white_02}
      />
    );
  };

  render() {
    const { list } = this.props;
    return (
      <SafeAreaView style={styles.wrapper}>
        <SecondaryNavbar back navTitle={t('community_more_event_lower')} />
        <View style={Classes.fill}>
          <FlatList data={list} renderItem={this.renderItem} />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    list: state.community.allEventsList,
    timezone: state.appState.currentTimeZone,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getTrainingEvent: CommunityActions.getTrainingEvent,
        joinEvent: CommunityActions.joinEvent,
      },
      dispatch,
    ),
)(AllEventsScreen);
