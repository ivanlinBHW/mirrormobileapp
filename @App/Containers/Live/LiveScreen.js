import { AutoHideFlatList, BaseCalendar as LiveCalendar, LiveCard } from 'App/Components';
import { ClassActions, LiveActions } from 'App/Stores';
import { Classes, Colors, Fonts, Images, Metrics } from 'App/Theme';
import { FlatList, Platform, SafeAreaView, Text, View } from 'react-native';
import { Screen, Date as d, ifIphoneX } from 'App/Helpers';

import PropTypes from 'prop-types';
import React from 'react';
import { bindActionCreators } from 'redux';
import { first } from 'lodash';
import { connect } from 'react-redux';
import moment from 'moment';
import styles from './LiveScreenStyle';
import { translate as t } from 'App/Helpers/I18n';

class LiveScreen extends React.Component {
  static propTypes = {
    sceneKey: PropTypes.string.isRequired,
    liveDate: PropTypes.array,
    list: PropTypes.array.isRequired,
    user: PropTypes.object.isRequired,
    getLiveDetail: PropTypes.func.isRequired,
    getLiveDate: PropTypes.func.isRequired,
    getLiveDateDetail: PropTypes.func.isRequired,
    removeLiveList: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    currentTimeZone: PropTypes.string.isRequired,
    currentLocale: PropTypes.object.isRequired,
    setLiveBookmark: PropTypes.func.isRequired,
    removeLiveBookmark: PropTypes.func.isRequired,
  };

  state = {
    markDate: {},
    calendarWeeklyTitle: '111',
    calendarTheme: {
      dayHeaderTextStyle: {
        textTransform: 'uppercase',
      },
      textDayHeaderFontFamily: Fonts.fontFamily.default,
      textDayHeaderFontSize: Fonts.style.small500.fontSize,
      textDayHeaderFontWeight: Fonts.style.small500.fontWeight,
      textSectionTitleColor: Colors.black,
      todayBackgroundColor: Colors.black,
      todayTextColor: Colors.primary,
      textDisabledColor: Colors.gray_02,
      selectedDayBackgroundColor: Colors.calendar.primary.selectedDayBackgroundColor,
      selectedDayTextColor: Colors.white,
      dotColor: Colors.primary,
      selectedDotTextColor: Colors.white,
      selectedDotColor: Colors.primary,
      selectedDotStyle: {
        paddingTop: 0,
        margin: 0,
        height: Screen.scale(22),
        width: Screen.scale(22),
        marginBottom: Screen.scale(20),
      },
      textDayStyle: {
        marginTop: Screen.scale(1),
        ...Fonts.style.medium500,
      },
      dotStyle: {
        height: parseInt(Screen.scale(8), 10),
        width: parseInt(Screen.scale(8), 10),
        borderWidth: 1,
        borderColor: Colors.white,
        borderRadius: parseInt(Screen.scale(4), 10),
        zIndex: 9001,
        ...Platform.select({
          ios: {
            marginRight: -12,
            marginTop: -20,
          },
          android: {
            marginRight: -13,
            marginTop: -22,
          },
        }),
      },
    },
    date: '',
    current: new Date(),
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter LiveScreen!');
    this.onDateChanged(moment(this.state.current).format('YYYY-MM-DD'));
  }

  componentDidUpdate(prevProps) {
    if (prevProps.liveDate !== this.props.liveDate) {
      this.getMarkedDates(this.state.date);
    }
  }

  onPressDetail = (id) => {
    const { getLiveDetail } = this.props;
    getLiveDetail(id);
  };

  getLiveDate = (params) => {
    const { getLiveDate, user } = this.props;
    getLiveDate(params, user.token);
  };

  getLiveDateDetail = (day) => {
    const { getLiveDateDetail, currentTimeZone } = this.props;
    getLiveDateDetail(day, currentTimeZone);
  };

  onPressBookmark = (data) => {
    const { removeLiveBookmark, setLiveBookmark } = this.props;
    if (data.isBookmarked) {
      removeLiveBookmark(data.id);
    } else {
      setLiveBookmark(data.id);
    }
  };

  renderFlatItem = ({ item, index }) => {
    return (
      <LiveCard
        key={index}
        data={item}
        onPress={this.onPressDetail}
        onPressBookmark={this.onPressBookmark}
      />
    );
  };

  renderTimeTitle = (date) => {
    const { currentTimeZone } = this.props;
    const transformDate = moment(date, 'YYYYMMDDTHH:mm:ssZZ').format(
      'YYYY-MM-DDTHH:mm:ssZZ',
    );

    const rawDate = d.transformDate(transformDate, currentTimeZone);
    return d.formatDate(rawDate, d.MINUTE_FORMAT);
  };

  renderItem = ({ item, index }) => {
    return (
      <View
        key={item.id}
        style={[styles.item, index % 2 === 1 ? styles.whiteBox : styles.grayBox]}
      >
        <View style={styles.timeBox}>
          <Text style={styles.itemHourText}>{this.renderTimeTitle(item.title)}</Text>
        </View>

        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={item.data}
          renderItem={this.renderFlatItem}
        />
      </View>
    );
  };

  renderEmpty = () => {
    return (
      <View style={styles.emptyBox}>
        <View style={styles.emptyTitleBox}>
          <Text style={styles.emptyTitle}>{t('live_empty_title')}</Text>
        </View>
      </View>
    );
  };

  onDateChanged = (date) => {
    const { startDate, endDate } = d.getWeekRangeByDate(date);

    const month = t(d.moment(startDate, d.DEFAULT_DATE_FORMAT).format('MMMM'));
    const calendarWeeklyTitle = `${month} ${startDate.split('/')[2]}-${
      endDate.split('/')[2]
    }`;
    this.setState(
      {
        calendarWeeklyTitle,
        date,
      },
      () => this.fetchBookedLiveDate(date, startDate, endDate),
    );
  };

  fetchBookedLiveDate = (date, startDate, endDate) => {
    this.getLiveDate({
      start: d.getUtcDate(d.moment(startDate, d.DEFAULT_DATE_FORMAT).startOf('day')),
      end: d.getUtcDate(d.moment(endDate, d.DEFAULT_DATE_FORMAT).endOf('day')),
    });
    const weekDays = this.getMarkedDates(date);
    if (weekDays[date] && !weekDays[date].disabled) {
      this.getLiveDateDetail(date);
    } else {
      const { removeLiveList } = this.props;
      removeLiveList();
    }
  };

  getMarkedDates = (selectedDate) => {
    const marked = {};
    const { liveDate } = this.props;
    const today = moment().format('YYYY-MM-DD');

    const { weekdays } = d.getWeekRangeByDate(selectedDate);

    weekdays.forEach((weekDay) => {
      const thatDay = d.moment(weekDay, 'YYYY/MM/DD').format('YYYY-MM-DD');
      marked[thatDay] = {
        selected: false,
        disabled: !d.isSameOrAfterDate(new Date(thatDay), today),
      };

      liveDate.forEach((item) => {
        marked[d.formatDate(item, 'YYYY-MM-DD')] = {
          marked: true,
          selected: false,
          disabled: !d.isSameOrAfterDate(new Date(thatDay), today),
        };
      });
    });
    this.setState({ marked });

    return marked;
  };

  render() {
    const { list, isLoading, sceneKey, currentLocale } = this.props;
    const { current, marked, calendarTheme, calendarWeeklyTitle } = this.state;
    return (
      <SafeAreaView style={[Classes.fill, styles.backgroundColorWhite02]}>
        <AutoHideFlatList
          data={list}
          style={{
            marginTop: Platform.select({
              android: Screen.scale(-Metrics.baseMargin),
              ios: ifIphoneX(
                Screen.scale(-Metrics.baseMargin / 4),
                Screen.scale(Metrics.baseMargin / 2),
              ),
            }),
          }}
          sceneKey={sceneKey}
          ListEmptyComponent={this.renderEmpty}
          ListHeaderComponent={
            <View style={styles.calendarWrapper}>
              <LiveCalendar
                locale={currentLocale.languageCode}
                leftArrowImageSource={Images.nav_back}
                rightArrowImageSource={Images.nav_next}
                displayLoadingIndicator={isLoading}
                onDateChanged={this.onDateChanged}
                dateTitle={calendarWeeklyTitle}
                calendarTheme={calendarTheme}
                markedDates={marked}
                current={current}
                expanded={false}
                allowShadow={false}
                disablePan
                hideKnob
              />
            </View>
          }
          renderItem={this.renderItem}
          footerStyle={styles.footerStyle}
          keyExtractor={(item, index) => `${index}`}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    sceneKey: params.name,
    user: state.user,
    list: state.live.list,
    liveDate: state.live.liveDate,
    isLoading: state.appState.isLoading,
    currentTimeZone: state.appState.currentTimeZone,
    currentLocale: first(state.appState.currentLocales, {}),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getLiveDetail: LiveActions.getLiveDetail,
        getLiveDate: LiveActions.getLiveDate,
        removeLiveList: LiveActions.removeLiveList,
        getLiveDateDetail: LiveActions.getLiveDateDetail,

        setLiveBookmark: ClassActions.setClassLiveBookmark,
        removeLiveBookmark: ClassActions.removeClassLiveBookmark,
      },
      dispatch,
    ),
)(LiveScreen);
