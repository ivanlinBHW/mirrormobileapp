import { ScaledSheet, Screen } from 'App/Helpers';
import { Colors, Metrics, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    flex: 1,
  },
  emptyBox: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyTitleBox: {
    marginTop: Metrics.baseMargin,
    width: '267@s',
    marginBottom: Metrics.baseMargin * 2,
  },
  emptyTitle: {
    ...Fonts.style.fontWeight500,
    fontSize: '24@vs',
    textAlign: 'center',
  },
  emptyContentBox: {
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin * 2,
    width: '249@s',
  },
  emptyContent: {
    ...Fonts.style.medium,
    color: Colors.gray_01,
    textAlign: 'center',
  },
  item: {
    padding: 20,
    flexDirection: 'row',
  },
  timeBox: {
    marginRight: 10,
    borderRightWidth: 2,
    borderRightColor: '#DDDDDD',
    paddingRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    width: '98@s',
  },
  itemHourText: {
    color: 'black',
  },
  itemDurationText: {
    color: 'grey',
    fontSize: 12,
    marginTop: 4,
    marginLeft: 4,
  },
  whiteBox: {
    backgroundColor: Colors.white,
  },
  grayBox: {
    backgroundColor: Colors.white_02,
  },
  footerStyle: {
    paddingBottom: Metrics.homeNavBarHeight + Screen.scale(10),
  },
  backgroundColorWhite02: { backgroundColor: Colors.white_02 },
  calendarWrapper: {
    marginBottom: '16@s',
    backgroundColor: 'red',
  },
});
