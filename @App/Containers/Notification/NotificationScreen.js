import { BaseButton, DateTimeReadMore, SecondaryNavbar } from 'App/Components';
import { Classes, Colors, Images } from 'App/Theme';
import { CommunityActions, LiveActions, NotificationActions } from 'App/Stores';
import {
  FlatList,
  Image,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Screen, Date as d } from 'App/Helpers';
import { has, isEmpty } from 'lodash';

import PropTypes from 'prop-types';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './NotificationScreenStyle';
import { translate as t } from 'App/Helpers/I18n';

class NotificationScreen extends React.Component {
  static propTypes = {
    getNotificationList: PropTypes.func.isRequired,
    list: PropTypes.array,
    timezone: PropTypes.string,
    notificationGetUserDetail: PropTypes.func.isRequired,
    notificationGetTrainingEvent: PropTypes.func.isRequired,
    notificationGetLiveClass: PropTypes.func.isRequired,
  };

  static defaultProps = {
    list: [],
    timezone: 'Asia/Taipei',
  };

  state = {};

  componentDidMount() {
    __DEV__ && console.log('@NotificationScreen');
    const { getNotificationList } = this.props;
    getNotificationList();
  }

  getNotificationListItem = ({
    component,
    item,
    onPress,
    style,
    showDateTime = true,
    isRead,
  }) => {
    const { timezone } = this.props;
    return (
      <TouchableOpacity
        disabled={!onPress}
        style={[styles.borderWrapper, style, isRead && { opacity: 0.5 }]}
        onPress={onPress}
      >
        <View style={styles.itemWrapper}>
          <Image source={Images.defaultAvatar} style={styles.itemImg} />
          <View style={Classes.fillRowCenter}>
            <View style={styles.textWrapper}>
              {component}
              {showDateTime && (
                <Text style={styles.itemTime}>
                  {d
                    .moment(d.transformDate(item.updatedAt, timezone))
                    .format(d.FULL_DATE_FORMAT)}
                </Text>
              )}
            </View>
          </View>
          {onPress && <Image source={Images.next} style={styles.imgArrow} />}
        </View>
      </TouchableOpacity>
    );
  };

  renderItem = ({ item, index }) => {
    const {
      timezone,
      notificationGetUserDetail,
      notificationGetTrainingEvent,
      notificationGetLiveClass,
    } = this.props;
    if (!isEmpty(item.notifyEventType) && has(item.notifyEventType, 'key')) {
      switch (item.notifyEventType.key) {
        case 'friend_add':
          return this.getNotificationListItem({
            onPress: () => notificationGetUserDetail(item.genericId),
            component: (
              <>
                <Text style={styles.titleText}>{t('notification_friend_title')}</Text>
                <Text style={styles.secondTitleText}>{item.content}</Text>
              </>
            ),
            isRead: item.isRead,
            item,
          });
        case 'training_event_invite':
          return this.getNotificationListItem({
            onPress: () => notificationGetTrainingEvent(item.genericId),
            component: (
              <>
                <Text style={styles.titleText}>{t('notification_event_title')}</Text>
                <Text style={styles.secondTitleText}>
                  {t('training_event_invite', {
                    owner: item.attributesData.userFullName.stringValue,
                    eventTitle: item.attributesData.trainingEventTitle.stringValue,
                  })}
                </Text>
              </>
            ),
            isRead: item.isRead,
            item,
          });
        case 'Booked_Live_Class_Reminder_Over_Six_Hours':
          return this.getNotificationListItem({
            component: (
              <>
                <Text style={styles.secondTitleText}>
                  {t('Booked_Live_Class_Reminder_Over_Six_Hours', {
                    className: item.attributesData.title.stringValue,
                    date: item.attributesData.scheduledAt.stringValue,
                  })}
                </Text>
              </>
            ),
            isRead: item.isRead,
            item,
          });
        case 'Booked_Live_Class_Reminder_In_Six_Hours':
          return this.getNotificationListItem({
            component: (
              <>
                <Text style={styles.secondTitleText}>
                  {t('Booked_Live_Class_Reminder_Over_Six_Hours', {
                    className: item.attributesData.title.stringValue,
                    date: item.attributesData.scheduledAt.stringValue,
                  })}
                </Text>
              </>
            ),
            isRead: item.isRead,
            item,
          });
        case 'Booked_Live_Class_Reminder_In_One_Hour':
          return this.getNotificationListItem({
            onPress: () => notificationGetLiveClass(item.genericId),
            component: (
              <>
                <Text style={styles.secondTitleText}>
                  {t('Booked_Live_Class_Reminder_Over_Six_Hours', {
                    className: item.attributesData.title.stringValue,
                    date: item.attributesData.scheduledAt.stringValue,
                  })}
                </Text>
              </>
            ),
            isRead: item.isRead,
            item,
          });
        default: {
          return this.getNotificationListItem({
            component: (
              <>
                <View style={styles.readMoreWrapper}>
                  <DateTimeReadMore
                    style={styles.itemText}
                    text={item.content}
                    moreText="See more"
                    expandTextColor={Colors.black}
                    expandTextStyle={styles.expandStyle}
                    dateTimeTextStyle={styles.itemTime}
                    showLine={2}
                    dateTime={d
                      .moment(d.transformDate(item.updatedAt, timezone))
                      .format(d.FULL_DATE_FORMAT)}
                  />
                </View>
              </>
            ),
            isRead: item.isRead,
            item,
            style: { height: Screen.scale(88) },
            showDateTime: false,
          });
        }
      }
    } else {
      const dateTime = item.scheduledAt ? item.scheduledAt : item.updatedAt;
      return this.getNotificationListItem({
        component: (
          <>
            <View style={styles.readMoreWrapper}>
              <View style={styles.readMoreTitleWrapper}>
                <Text style={styles.txtReadMoreTitle}>{item.subject}</Text>
              </View>
              <DateTimeReadMore
                style={styles.itemText}
                text={item.content}
                moreText="See more"
                expandTextColor={Colors.black}
                expandTextStyle={styles.expandStyle}
                dateTimeTextStyle={styles.itemTime}
                showLine={1}
                dateTime={d
                  .moment(d.transformDate(dateTime, timezone))
                  .format(d.FULL_DATE_FORMAT)}
              />
            </View>
          </>
        ),
        isRead: item.isRead,
        item,
        style: { minHeight: Screen.scale(88), height: 'auto' },
        showDateTime: false,
      });
    }
  };

  renderEmpty = () => {
    return (
      <View style={styles.emptyBox}>
        <Text style={styles.emptyText}>{t('notification_empty')}</Text>
      </View>
    );
  };

  render() {
    const { getNotificationList, list } = this.props;
    console.log('list =>', list);
    return (
      <SafeAreaView style={[Classes.fill]}>
        <SecondaryNavbar back navTitle={t('notification_nav_title')} />
        <View style={styles.wrapper}>
          <FlatList
            data={list}
            ListEmptyComponent={this.renderEmpty}
            renderItem={this.renderItem}
            refreshing={false}
            showRefresh
            onRefresh={getNotificationList}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    list: state.notification.list,
    timezone: state.appState.currentTimeZone,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getNotificationList: NotificationActions.getNotificationList,
        notificationGetUserDetail: CommunityActions.notificationGetUserDetail,
        notificationGetTrainingEvent: CommunityActions.notificationGetTrainingEvent,
        notificationGetLiveClass: LiveActions.getLiveDetail,
      },
      dispatch,
    ),
)(NotificationScreen);
