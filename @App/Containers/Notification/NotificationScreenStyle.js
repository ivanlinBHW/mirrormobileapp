import { ScaledSheet, Screen } from 'App/Helpers';
import { Metrics, Colors, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  wrapper: {
    backgroundColor: Colors.white_02,
    flex: 1,
  },

  borderWrapper: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_15,
    justifyContent: 'center',
    minHeight: '88@s',
  },
  itemWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: Metrics.baseMargin,
  },

  itemImg: {
    width: '40@s',
    height: '40@s',
    borderRadius: '20@s',
    marginRight: Metrics.baseMargin,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textWrapper: {
    flex: 1,
    justifyContent: 'center',
  },

  imgArrow: {
    width: '6@s',
    height: '10@vs',
    resizeMode: 'contain',
  },
  readMoreWrapper: {
    flex: 1,
    width: '100%',
    minHeight: '96@s',
    height: 'auto',
    paddingVertical: '12@vs',
    alignItems: 'center',
    justifyContent: 'center',
  },

  expandStyle: {
    ...Fonts.style.small500,
    marginTop: 0,
  },
  emptyBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    fontSize: Screen.scale(24),
    marginTop: Screen.scale(284),
    ...Fonts.style.fontWeight500,
  },

  titleText: {
    ...Fonts.style.small500,
    color: Colors.gray_02,
  },
  secondTitleText: {
    ...Fonts.style.small500,
    maxWidth: '90%',
  },

  itemText: {
    ...Fonts.style.small500,
    width: '99%',
    paddingHorizontal: 0,
    zIndex: 999,
  },
  itemTime: {
    ...Fonts.style.small500,
    color: Colors.gray_02,
  },

  txtReadMoreTitle: {
    ...Fonts.style.smallBold,
    color: Colors.black,
  },
  readMoreTitleWrapper: {
    flex: 1,
    alignSelf: 'flex-start',
    paddingLeft: '2@s',
    paddingBottom: '2@s',
  },
});
