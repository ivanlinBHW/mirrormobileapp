import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { FeedbackPanel } from 'App/Components';
import { ProgramActions } from 'App/Stores';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';

class ProgramFeedbackScreen extends React.Component {
  static propTypes = {
    programSubmitFeedback: PropTypes.func.isRequired,
    token: PropTypes.string,
    detailId: PropTypes.string,
    detail: PropTypes.object,
    isProgramFinished: PropTypes.bool,
    onMirrorCommunicate: PropTypes.func.isRequired,
  };

  componentDidMount() {
    console.log('======= ProgramFeedbackScreen ========');
  }

  static defaultProps = {
    isProgramFinished: false,
  };

  onPressSubmit = (payload) => {
    const {
      programSubmitFeedback,
      detail,
      isProgramFinished,
      onMirrorCommunicate,
    } = this.props;
    onMirrorCommunicate(MirrorEvents.SAVE_FEEDBACK_FINISH, {
      classId: detail.id,
    });
    programSubmitFeedback(detail.trainingClassHistory.id, payload, isProgramFinished);
  };

  render() {
    const { detail } = this.props;
    return <FeedbackPanel detail={detail} onPressSubmit={this.onPressSubmit} />;
  }
}

export default connect(
  (state) => ({
    token: state.user.token,
    detail: state.player.detail,
    isProgramFinished: state.program.isProgramFinished,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        programSubmitFeedback: ProgramActions.programSubmitFeedback,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(ProgramFeedbackScreen);
