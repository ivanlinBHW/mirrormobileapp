import { ScaledSheet, Screen } from 'App/Helpers';
import { Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  emptyBox: {
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Screen.scale(266),
  },
  emptyTitle: {
    fontSize: Screen.scale(24),
    color: Colors.titleText.primary,
    textAlign: 'center',
    ...Fonts.style.fontWeight500,
  },
  emptyContent: {
    ...Fonts.style.small500,
    marginTop: '12@vs',
    color: Colors.titleText.third,
    textAlign: 'center',
  },
  saveBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: 0,
    marginRight: '-4@s',
    flex: 1,
  },
  saveText: {
    fontSize: Fonts.size.regular,
    color: Colors.button.primary.text.text,
  },
});
