import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, FlatList, View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseButton, CommunityClassItem } from 'App/Components';
import { Screen } from 'App/Helpers';
import { Colors, Classes } from 'App/Theme';
import { CommunityActions, SearchActions } from 'App/Stores';
import styles from './ClassesSelectedScreenStyle';

class ClassesSelectedScreen extends React.Component {
  static propTypes = {
    multiSelect: PropTypes.bool,
    list: PropTypes.array.isRequired,
    token: PropTypes.string,
    classesSelected: PropTypes.array,
    stashClassesSelected: PropTypes.array,
    routeName: PropTypes.string,
    saveSelectedClasses: PropTypes.func.isRequired,
    getSearchEventClassDetail: PropTypes.func.isRequired,
    addStashSelectedClasses: PropTypes.func.isRequired,
    removeStashSelectedClasses: PropTypes.func.isRequired,
    saveStashSelectedClasses: PropTypes.func.isRequired,

    totalProps: PropTypes.number,
    curPageProps: PropTypes.number,
    pageSizeProps: PropTypes.number,
    searchPayload: PropTypes.object.isRequired,
    communitySearch: PropTypes.func.isRequired,
  };

  static defaultProps = {
    classesSelected: [],
    multiSelect: true,

    totalProps: 0,
    curPageProps: 1,
    pageSizeProps: 10,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log('nextProps.list.length=>', nextProps.list.length);
    console.log('prevState.classesList.length=>', prevState.classesList.length);
    console.log('update=>', nextProps.list.length !== prevState.classesList.length);
    if (
      nextProps.curPageProps !== prevState.curPage ||
      nextProps.pageSizeProps !== prevState.pageSize ||
      nextProps.totalProps !== prevState.total ||
      nextProps.list.length !== prevState.classesList.length ||
      nextProps.stashClassesSelected.length !== prevState.selected.length
    ) {
      const classesList = nextProps.list.map((e) => ({
        ...e,
        isSelect: nextProps.stashClassesSelected.some((s) => s.id === e.id),
      }));
      return {
        ...prevState,
        curPage: nextProps.curPageProps,
        pageSize: nextProps.pageSizeProps,
        total: nextProps.totalProps,
        selected: nextProps.stashClassesSelected,
        classesList,
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    props.saveStashSelectedClasses(props.classesSelected);

    this.state = {
      classesList: [],
      stashClassesSelected: [],
      selected: [],
      isSelect: false,

      curPage: 1,
      pageSize: 10,
      total: null,
    };

    this.endReached = false;
  }

  componentDidMount() {
    __DEV__ && console.log('@ClassesSelectedScreen');

    const { communitySearch, searchPayload } = this.props;
    communitySearch(searchPayload, 1, 10);
  }

  onPressChoose = (data) => {
    const { classesList, selected } = this.state;
    const { saveStashSelectedClasses } = this.props;

    let tempSelected = JSON.parse(JSON.stringify(selected));
    if (
      tempSelected.length > 0 &&
      tempSelected.filter((item) => item.id === data.id).length > 0
    ) {
      let tempSelect = [];
      tempSelected.forEach((select) => {
        if (select.id !== data.id) {
          tempSelect.push(select);
        }
      });
      tempSelected = tempSelect;
    } else {
      tempSelected.push(data);
    }
    let temp = JSON.parse(JSON.stringify(classesList));
    temp.map((item, index) => {
      if (item.id === data.id) {
        temp[index].isSelect = !temp[index].isSelect;
      }
    });
    this.setState({ classesList: temp, selected: tempSelected, isSelect: true });
    saveStashSelectedClasses(tempSelected);
  };

  renderItem = ({ item, index }) => {
    const { selected } = this.state;
    const { getSearchEventClassDetail, multiSelect, stashClassesSelected } = this.props;

    let disabled = !multiSelect && !item.isSelect && selected.length > 0;
    const maxClassCount = 36;

    if (multiSelect && stashClassesSelected) {
      disabled =
        stashClassesSelected &&
        stashClassesSelected.length >= maxClassCount &&
        !item.isSelect;
    }
    return (
      <CommunityClassItem
        {...item}
        key={item.id}
        backgroundColor={index % 2 ? Colors.white_02 : Colors.gray_04}
        onPressDetail={() => getSearchEventClassDetail(item.id, disabled)}
        onPress={() => this.onPressChoose(item)}
        deletable={item.isSelect}
        disabled={disabled}
      />
    );
  };

  renderEmpty = () => {
    return (
      <View style={styles.emptyBox}>
        <Text style={styles.emptyTitle}>{t('see_all_empty_title')}</Text>
        <Text style={styles.emptyContent}>{t('see_all_empty_content')}</Text>
      </View>
    );
  };

  onPressSave = () => {
    const { saveSelectedClasses } = this.props;
    const { selected } = this.state;
    saveSelectedClasses(selected);
    Actions.popTo('CreateEventScreen');
  };

  isShowBackConfirm = () => {
    const { stashClassesSelected = [], classesSelected } = this.props;

    if (stashClassesSelected.length !== classesSelected.length) {
      return true;
    } else {
      if (stashClassesSelected.length > 0) {
        stashClassesSelected.sort((a, b) => {
          return a.id - b.id;
        });

        classesSelected.sort((a, b) => {
          return a.id - b.id;
        });
        let i = 0;
        stashClassesSelected.map((item, index) => {
          if (item.id !== classesSelected[index].id) {
            i += 1;
          }
        });
        if (i !== 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;

    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom
    );
  };

  onLoadData = (event) => {
    if (!this.endReached) {
      return;
    }
    const {
      communitySearch,
      searchPayload,
      curPageProps,
      pageSizeProps,
      totalProps,
    } = this.props;

    console.log('onPressEndReach PageProps=>', {
      totalProps,
      curPageProps,
      pageSizeProps,
    });

    if (totalProps >= 0) {
      communitySearch(searchPayload, curPageProps + 1, pageSizeProps);
    }
    this.endReached = false;
  };

  render() {
    const { classesList } = this.state;
    const { classesSelected, saveStashSelectedClasses } = this.props;

    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          back
          backConfirm={this.isShowBackConfirm()}
          onPressYesBack={() => saveStashSelectedClasses(classesSelected)}
          backConfirmDesc={t('community_back_alert_classes_content')}
          navTitle={`${t('community_choose_class')}`}
          navRightComponent={
            <BaseButton
              style={styles.saveBtn}
              onPress={this.onPressSave}
              throttleTime={0}
              transparent
            >
              <Text style={styles.saveText}>{t('setting_edit_profile_save')}</Text>
            </BaseButton>
          }
        />
        <View style={Classes.fill}>
          <FlatList
            data={classesList}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderEmpty}
            onEndReachedThreshold={0.4}
            onEndReached={() => (this.endReached = true)}
            onMomentumScrollEnd={this.onLoadData}
            keyExtractor={(item) => item.id}
            style={styles.listBox}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    token: state.user.token,
    timezone: state.appState.currentTimeZone,
    routeName: params.name,
    classesSelected: state.community.classesSelected,
    stashClassesSelected: state.community.stashClassesSelected,

    curPageProps: state.search.communitySearch.payload.page,
    pageSizeProps: state.search.communitySearch.payload.pageSize,
    totalProps: state.search.communitySearch.total,
    list: state.search.communitySearch.list,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        saveSelectedClasses: CommunityActions.saveSelectedClasses,
        getSearchEventClassDetail: CommunityActions.getSearchEventClassDetail,
        addStashSelectedClasses: CommunityActions.addStashSelectedClasses,
        removeStashSelectedClasses: CommunityActions.removeStashSelectedClasses,
        saveStashSelectedClasses: CommunityActions.saveStashSelectedClasses,

        communitySearch: SearchActions.communitySearch,
      },
      dispatch,
    ),
)(ClassesSelectedScreen);
