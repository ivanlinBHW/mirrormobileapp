import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import MirrorActions from 'App/Stores/Mirror/Actions';

class MirrorConnector extends React.PureComponent {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    mirrorConnect: PropTypes.func.isRequired,
    lastDevice: PropTypes.object,
    token: PropTypes.string,
    connectedSsidName: PropTypes.string,
  };

  static defaultProps = {
    lastDevice: null,
    token: null,
    connectedSsidName: null,
  };

  componentDidMount() {
    setTimeout(() => {
      __DEV__ && console.log('@Enter MirrorConnector!');
      const { mirrorConnect, token, lastDevice, connectedSsidName } = this.props;
      if (token && lastDevice && connectedSsidName) {
        mirrorConnect(lastDevice, {
          maxRetries: 3,
        });
      }
    }, 200);
  }

  render() {
    return null;
  }
}

export default connect(
  (state) => ({
    isLoading: state.appState.isLoading,
    token: state.user.token,
    lastDevice: state.mirror.lastDevice,
    routeName: state.appRoute.routeName,
    connectedSsidName: state.mirror.connectedSsidName,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mirrorConnect: MirrorActions.onMirrorConnect,
      },
      dispatch,
    ),
)(MirrorConnector);
