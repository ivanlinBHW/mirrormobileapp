import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';

import ClassCard from 'App/Components/ClassCard';
import styles from './ProgressClassStyle';
class ProgressClass extends React.Component {
  componentDidMount() {
    __DEV__ && console.log('@Enter ProgressClass!');
  }

  render() {
    return (
      <View style={styles.layout}>
        <ClassCard size="large" />
      </View>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => ({}),
)(ProgressClass);
