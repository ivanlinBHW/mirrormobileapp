import { ScaledSheet } from 'App/Helpers';
import { Fonts } from 'App/Theme';
import Styles from 'App/Theme/Styles';
import Classes from 'App/Theme/Classes';

export default ScaledSheet.create({
  title: {
    width: '100%',
    textAlign: 'center',
    marginTop: '40@vs',
    marginBottom: '20@vs',
  },
  navBar: {
    ...Classes.mainStart,
    ...Classes.crossStart,
    flex: 0.1,
  },
  headerText: {
    textAlign: 'center',
    width: '100%',
  },
  image: {
    width: '280@s',
    height: '110@vs',
  },
  imageBox: {
    marginLeft: '40@s',
    marginRight: '40@s',
    marginTop: '40@vs',
  },
  footerBox: {
    marginTop: '60@vs',
  },
  footerTextBox: {
    textAlign: 'left',
    marginBottom: '20@vs',
    marginTop: '40@vs',
  },
  footerText: {
    fontSize: '14@vs',
    textAlign: 'left',
  },
  layout: {
    marginLeft: '20@s',
    marginRight: '20@s',
    ...Styles.screen.container,
  },
  btnbox: {
    marginTop: '20@vs',
  },
  loginBtn: {
    backgroundColor: '#853E60',
  },
  titleText: {
    flexDirection: 'row',
    marginTop: '20@vs',
    marginBottom: '20@vs',
  },
  refreshBtn: {
    marginTop: '5@vs',
    marginLeft: '10@s',
  },
  addBtn: {
    borderStyle: 'dashed',
  },
  flexRight: {
    marginRight: 0,
  },
  FAQStyle: {
    ...Fonts.style.underline,
    color: '#2A78E4',
  },
  headerToTop: {
    marginTop: '40@vs',
  },
});
