import * as yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { isArray } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, Keyboard, SafeAreaView, TouchableOpacity } from 'react-native';

import { Dialog, Date as d } from 'App/Helpers';
import { CommunityActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { Classes, Colors, Metrics } from 'App/Theme';
import {
  HeadLine,
  FormHeader,
  SquareButton,
  BouncyCheckbox,
  SecondaryNavbar,
  VerticalTextInput,
  DateTimePickerModal,
} from 'App/Components';

const yString = yup.string();

const validationSchema = yup.object().shape({
  title: yString.required(t('required_field')),
  startDate: yString.required(t('required_field')),
  dueDate: yString.required(t('required_field')),
});

const initialValues = {
  title: '',
  startDate: d.formatDate(new Date()).toString(),
  dueDate: d.formatDate(new Date(), 'HH:mm').toString(),
  eventBeginType: 'now',
};

class ReserveNew1on1Screen extends React.Component {
  static propTypes = {
    userId: PropTypes.number.isRequired,
    eventDetail: PropTypes.array.isRequired,
    createNewEvent: PropTypes.func.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@ReserveNew1on1Screen');
  }

  state = {
    isShowTimePicker: false,
    isValidateChange: true,
    time: new Date(),
  };

  handleSubmit = (values) => {
    const { createNewEvent, eventDetail, userId } = this.props;
    const date = `${values.startDate} ${values.dueDate}`;
    const payload = {
      startDate:
        values.eventBeginType === 'now'
          ? ''
          : d.transformDateToUTC(date, 'UTC', d.FORMAT_YY_MM_DD_HH_MM_SS),
      dueDate:
        values.eventBeginType === 'now'
          ? ''
          : d
              .transformDateToUTC(
                d.moment(date).add(1, 'hours'),
                'UTC',
                d.FORMAT_YY_MM_DD_HH_MM_SS,
              )
              .toString(),
      title: values.title,
      isPublic: false,
      canInvite: false,
      trainingClasses: isArray(eventDetail.trainingClasses)
        ? eventDetail.trainingClasses.map((e) => e.id)
        : [],
      inviteUserIds: isArray(eventDetail.participate)
        ? eventDetail.participate.map((e) => e.id).filter((e) => e !== userId)
        : [],
      usersCount: 100,
      description: eventDetail.description,
      coverImage: eventDetail.coverImage,
      eventType: eventDetail.eventType,
    };
    createNewEvent(payload, Dialog.reserveNew1on1EventSuccessAlert);
  };

  onTriggerTimePicker = (isOpen, mode) => () => {
    Keyboard.dismiss();
    if (this.scrollView) {
      this.scrollView.scrollTo({ y: Screen.height * 0.7 });
    }
    this.setState({
      isShowTimePicker: isOpen,
      timePickerMode: mode,
    });
  };

  onTimeChange = (setFieldValue) => (date) => {
    const { timePickerMode } = this.state;
    if (timePickerMode === 'date') {
      const formattedDate = d.formatDate(date).toString();
      setFieldValue('startDate', formattedDate);
    } else if (timePickerMode === 'time') {
      const formattedDate = d.formatDate(date, 'HH:mm').toString();
      setFieldValue('dueDate', formattedDate);
    }
    this.setState({
      time: date,
    });
  };

  renderRatioButton = ({ handleChange, values }) => {
    return (
      <View style={[Classes.halfFill, Classes.marginBottom]}>
        <TouchableOpacity
          style={[Classes.fillRowCross, Classes.marginLeft]}
          onPress={() => {
            handleChange('eventBeginType')('now');
          }}
        >
          <BouncyCheckbox
            isChecked={values.eventBeginType === 'now'}
            fillColor={Colors.black}
            checkboxSize={22}
            text=""
            borderColor={Colors.gray_03}
          />
          <Text>{t('community_event_1on1_now')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[Classes.fillRowCross, Classes.marginLeft]}
          onPress={() => {
            handleChange('eventBeginType')('reservation');
            handleChange('dueDate')(d.formatDate(new Date(), 'HH:mm').toString());
          }}
        >
          <BouncyCheckbox
            isChecked={values.eventBeginType === 'reservation'}
            fillColor={Colors.black}
            checkboxSize={22}
            text=""
            borderColor={Colors.gray_03}
          />
          <Text>{t('community_event_1on1_reservation')}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderDateInput = ({ errors, values }) => {
    return (
      <View style={Classes.halfFill}>
        <HeadLine
          title={t('community_event_creation_input_choose_date')}
          hrLine={false}
          height={24}
          paddingTop={24}
          paddingHorizontal={0}
        />
        <View style={Classes.fillRow}>
          <View style={Classes.fill}>
            <VerticalTextInput
              title={t('community_event_creation_input_from')}
              placeholder={t('community_event_creation_input_desc_placeholder')}
              onPress={this.onTriggerTimePicker(true, 'date')}
              error={errors.startDate}
              value={values.startDate}
              disabled={values.eventBeginType !== 'reservation'}
              editable={values.eventBeginType === 'reservation'}
              inputAlign="center"
              required
              border
            />
          </View>
          <View style={{ width: Metrics.baseMargin }} />
          <View style={Classes.fill}>
            <VerticalTextInput
              title={t('setting_mii_display_time')}
              placeholder={t('community_event_creation_input_desc_placeholder')}
              onPress={this.onTriggerTimePicker(true, 'time')}
              error={errors.dueDate}
              value={values.dueDate}
              disabled={values.eventBeginType !== 'reservation'}
              editable={values.eventBeginType === 'reservation'}
              inputAlign="center"
              required
              border
            />
          </View>
        </View>
      </View>
    );
  };

  renderForm = (formikProps) => {
    const {
      setFieldValue,
      handleChange,
      handleSubmit,
      values,
      errors,
      dirty,
      isSubmitting,
      isValidating,
      isValid,
    } = formikProps;
    const { eventDetail } = this.props;
    const { isShowTimePicker, timePickerMode, time } = this.state;
    const minDate = new Date();
    const maxDate = new Date(d.moment(minDate).add(30, 'days'));
    return (
      <View style={Classes.fill}>
        <FormHeader text={t('community_event_1on1_next_event')} />

        <View style={[Classes.padding, Classes.fill]}>
          <View style={[Classes.halfFill]}>
            {this.renderRatioButton(formikProps)}

            <View style={Classes.halfFill}>
              <VerticalTextInput
                title={t('community_event_creation_input_event_title')}
                placeholder={eventDetail.title}
                onChangeText={handleChange('title')}
                error={errors.title}
                value={values.title}
                maxLength={24}
                required
                border
                bold
              />
            </View>
            {this.renderDateInput(formikProps)}
          </View>
        </View>

        <SquareButton
          disabled={isSubmitting || isValidating || !isValid}
          color={Colors.primary}
          onPress={handleSubmit}
          text={t('submit')}
        />
        {isShowTimePicker && (
          <DateTimePickerModal
            date={time}
            onDateChange={this.onTimeChange(setFieldValue)}
            onCancelPress={this.onTriggerTimePicker(false)}
            minimumDate={minDate}
            maximumDate={maxDate}
            mode={timePickerMode}
            selectedStartDate={new Date()}
          />
        )}
      </View>
    );
  };

  render() {
    const { isValidateChange } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar navHeight={44} back />
        <Formik
          onSubmit={this.handleSubmit}
          initialValues={initialValues}
          validationSchema={validationSchema}
          validateOnChange={isValidateChange}
        >
          {(formikProps) => this.renderForm(formikProps)}
        </Formik>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    userId: state.user.id,
    eventDetail: state.community.eventDetail,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        createNewEvent: CommunityActions.createNewEvent,
      },
      dispatch,
    ),
)(ReserveNew1on1Screen);
