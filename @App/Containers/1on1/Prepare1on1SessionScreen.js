import React from 'react';
import PropTypes from 'prop-types';
import { isArray, has } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import {
  View,
  SafeAreaView,
  Text,
  ActivityIndicator,
  TouchableWithoutFeedback,
} from 'react-native';
import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';

import { CommunityActions, AppStateActions, MirrorActions } from 'App/Stores';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { Classes, Colors, Images, Fonts, Animations } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d, Screen, Dialog } from 'App/Helpers';
import {
  LottieButton,
  SecondaryNavbar,
  GreenRoundButton,
  RoundButton,
  BaseAvatar,
  BaseImageButton,
} from 'App/Components';

class Prepare1on1SessionScreen extends React.Component {
  static propTypes = {
    classStartAt: PropTypes.string,
    currentState: PropTypes.string,
    user: PropTypes.object.isRequired,
    isReady: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isConnected: PropTypes.bool.isRequired,
    sceneKey: PropTypes.string.isRequired,
    routeName: PropTypes.string.isRequired,
    eventDetail: PropTypes.object.isRequired,
    createNewEvent: PropTypes.func.isRequired,
    eventClassId: PropTypes.string.isRequired,
    startEventClass: PropTypes.func.isRequired,
    sync1on1StartTime: PropTypes.func.isRequired,
    currentTimeZone: PropTypes.string.isRequired,
    fetchGet1on1OnlineStatus: PropTypes.func.isRequired,
    fetchPut1on1OnlineStatus: PropTypes.func.isRequired,
    fetchPost1on1Notification: PropTypes.func.isRequired,
    fetchGet1on1SyncedStartTime: PropTypes.func.isRequired,
    playerDetail: PropTypes.object,
    onMirrorCommunicate: PropTypes.func.isRequired,

    hasDistanceAdjustment: PropTypes.bool.isRequired,
    hasPartnerWorkout: PropTypes.bool.isRequired,
    hasCoachVI: PropTypes.bool.isRequired,
    hasCamera: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    currentState: null,
    classStartAt: null,
  };

  state = {
    countdown: 'GO',
  };

  timer = null;

  countDownTimer = null;

  componentDidMount() {
    __DEV__ && console.log('@Prepare1on1SessionScreen');
    if (this.timer) {
      clearInterval(this.timer);
    }
    if (this.countDownTimer) {
      clearInterval(this.countDownTimer);
    }
    this.handleTimer();

    const { eventDetail, fetchPut1on1OnlineStatus } = this.props;
    fetchPut1on1OnlineStatus(eventDetail.id, 'joined');
  }

  componentDidUpdate(prevProps) {
    const {
      isReady,
      sceneKey,
      routeName,
      eventDetail,
      currentState,
      classStartAt,
      fetchPut1on1OnlineStatus,
    } = this.props;
    if (routeName !== sceneKey) {
      clearInterval(this.timer);
      clearInterval(this.countDownTimer);
    }
    if (has(eventDetail, 'id')) {
      if (routeName !== prevProps.routeName) {
        if (routeName !== sceneKey && !routeName.includes('Player')) {
          fetchPut1on1OnlineStatus(eventDetail.id, 'offline');
        }
        if (routeName === sceneKey) {
          this.handleTimer();
          fetchPut1on1OnlineStatus(eventDetail.id, 'joined');
        }
      }
      if (currentState !== prevProps.currentState && typeof currentState === 'string') {
        if (currentState !== 'active') {
          fetchPut1on1OnlineStatus(eventDetail.id, 'offline');
          if (this.timer) {
            clearInterval(this.timer);
          }
        } else {
          this.handleTimer();
          fetchPut1on1OnlineStatus(eventDetail.id, 'joined');
        }
      }
    }
    if (classStartAt !== prevProps.classStartAt && classStartAt) {
      this.handleStartClass();
    }
    if (this.Lottie && isReady !== prevProps.isReady) {
      this.Lottie.reset();
      this.Lottie.play();
    }
  }

  componentWillUnmount() {
    const { eventDetail, fetchPut1on1OnlineStatus } = this.props;
    if (has(eventDetail, 'id')) {
      fetchPut1on1OnlineStatus(eventDetail.id, 'offline');
    }
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  _getAnimatedButtonRef = (e) => {
    this.Lottie = e;
  };

  handleTimer = () => {
    const {
      fetchGet1on1OnlineStatus,
      fetchPut1on1OnlineStatus,
      eventDetail,
    } = this.props;
    this.timer = setInterval(() => {
      if (has(eventDetail, 'id')) {
        fetchPut1on1OnlineStatus(eventDetail.id, 'joined');
        fetchGet1on1OnlineStatus(eventDetail.id);
      }
    }, 3 * 1000);
  };

  handleCountDownTimer = () => {
    const { classStartAt, currentTimeZone } = this.props;
    clearInterval(this.timer);
    const localClassStartTime = d.transformDate(classStartAt, currentTimeZone);

    this.countDownTimer = setInterval(() => {
      const periodInSeconds = parseInt(
        (d.moment(localClassStartTime) - d.moment()) / 1000,
        10,
      );
      console.log('currentTimeZone=>', currentTimeZone);
      console.log('classStartAt=>', classStartAt);
      console.log('localClassStartTime=>', localClassStartTime);
      console.log('periodInSeconds=>', periodInSeconds);
      if (periodInSeconds <= 3 && periodInSeconds >= 0) {
        this.setState((state) => ({
          countdown: periodInSeconds,
        }));
        if (periodInSeconds <= 0) {
          clearInterval(this.countDownTimer);
          this.handleStartClass();
        }
      }
    }, 1000);
  };

  handleSyncClassStart = () => {
    const { eventDetail, sync1on1StartTime } = this.props;
    clearInterval(this.timer);
    if (has(eventDetail, 'id')) {
      sync1on1StartTime(eventDetail.id);
    }
  };

  handleStartClass = () => {
    clearInterval(this.timer);
    clearInterval(this.countDownTimer);
    const { eventDetail, startEventClass, eventClassId, classStartAt } = this.props;

    if (has(eventDetail, 'id')) {
      startEventClass(eventClassId, eventDetail.id, classStartAt);
    }

    this.setState((state) => ({
      countdown: 'GO',
    }));
  };

  onPressCancel = () => {
    const {
      fetchPut1on1OnlineStatus,
      eventDetail,
      onMirrorCommunicate,
      playerDetail,
    } = this.props;
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS, {
      classId: playerDetail.id,
      classType: 'on-demand',
    });
    fetchPut1on1OnlineStatus(eventDetail.id, 'offline', Actions.pop);
  };

  renderUserAvatar = ({ borderColor, fullName, avatarUrl }) => {
    return (
      <View style={Classes.fillCenter}>
        <BaseAvatar
          size={Screen.scale(120)}
          uri={avatarUrl}
          image={Images.defaultAvatar}
          borderColor={borderColor}
          borderWidth={3}
        />
        <Text style={[Classes.marginTop, { color: borderColor }]}>{fullName}</Text>
      </View>
    );
  };

  render() {
    const {
      user,
      isReady,
      sceneKey,
      routeName,
      isLoading,
      isConnected,
      eventDetail,
      classStartAt,
      currentState,
      fetchPut1on1OnlineStatus,
      fetchPost1on1Notification,
      hasPartnerWorkout,
    } = this.props;
    const { countdown } = this.state;
    const hasMeetingUsers = has(eventDetail, 'meetingSession.meetingUsers');
    const anotherUser =
      hasMeetingUsers && isArray(eventDetail.meetingSession.meetingUsers)
        ? eventDetail.meetingSession.meetingUsers.find((e) => e.user.id !== user.id)
        : null;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          back={isReady}
          backConfirm
          onPressBack={this.onPressBack}
          navRightComponent={
            <View style={[Classes.fillRowCross, Classes.mainEnd]}>
              <ActivityIndicator animating={isLoading} size="small" color="black" />
              <BaseImageButton
                style={Classes.marginLeft}
                source={Images.voice}
                onPress={Actions.AudioModal}
                disabled={!isConnected}
              />
              <BaseImageButton
                style={Classes.marginLeft}
                source={Images.setting}
                onPress={Actions.WorkoutOptionModal}
                disabled={!isConnected}
              />
            </View>
          }
        />
        <View
          style={[Classes.mainEnd, Classes.crossCenter, { marginTop: Screen.scale(48) }]}
        >
          <Text style={Fonts.style.h7_500}>{t('community_1on1_ready_for_workout')}</Text>
        </View>

        <View style={[Classes.fillRowCenter, Classes.padding]}>
          {hasMeetingUsers && (
            <>
              {this.renderUserAvatar({
                fullName: user.fullName,
                avatarUrl: user.avatar,
                borderColor: Colors.green_blue,
              })}
              <View style={Classes.fillColCross}>
                <LottieButton
                  ref={this._getAnimatedButtonRef}
                  disabled
                  source={
                    isReady ? Animations.on_one_one_green : Animations.on_one_one_grey
                  }
                  autoPlay
                  autoSize
                  speed={1}
                  loop
                  style={{
                    position: 'absolute',
                    top: -80,
                    opacity: 1,
                  }}
                  animationStyle={{
                    width: 150,
                  }}
                />
                <Text
                  style={[
                    Classes.marginTop,
                    Fonts.style.medium500,
                    {
                      position: 'absolute',
                      top: 8,
                    },
                  ]}
                >
                  {isReady ? t('community_1on1_matched') : t('community_1on1_waiting')}
                </Text>
              </View>
              {this.renderUserAvatar({
                fullName: anotherUser ? anotherUser.user.fullName : '-',
                avatarUrl: anotherUser ? anotherUser.user.avatar : '',
                borderColor: isReady ? Colors.green_blue : Colors.primary,
              })}
            </>
          )}
        </View>

        <View style={[Classes.fill, Classes.colCenter, Classes.mainStart]}>
          {isReady ? (
            <TouchableWithoutFeedback
              onPress={Dialog.showDeviceNoPartnerWorkoutAlert}
              pointerEvents={hasPartnerWorkout ? 'box-none' : 'box-only'}
            >
              <View
                pointerEvents={hasPartnerWorkout ? 'box-none' : 'box-only'}
                style={{
                  width: '100%',
                }}
              >
                <GreenRoundButton
                  size={188}
                  text={countdown}
                  onPress={this.handleSyncClassStart}
                />
              </View>
            </TouchableWithoutFeedback>
          ) : (
            <>
              <RoundButton
                text={t('community_1on1_push')}
                onPress={() => fetchPost1on1Notification(eventDetail.id)}
                textColor={Colors.button.primary.content.text}
                color={Colors.button.primary.content.background}
                style={{ ...Classes.marginBottom, width: 250 }}
                bold
              />
              <RoundButton
                text={t('community_1on1_cancel')}
                onPress={this.onPressCancel}
                textColor={Colors.button.primary.outline.text}
                color={Colors.button.primary.outline.background}
                style={{ width: 250 }}
                outline
                round
                bold
              />
            </>
          )}
          <Text
            style={{
              ...Fonts.style.small500,
              marginTop: Screen.scale(40),
              color: Colors.gray_02,
            }}
          >
            {t(
              isReady ? 'community_1on1_footer_text_ready' : 'community_1on1_footer_text',
            )}
          </Text>
          {__DEV__ && (
            <>
              <Text>classStartAt: `{classStartAt}`</Text>
              <Text>currentState: `{currentState}`</Text>
              <Text>sceneKey: `{sceneKey}`</Text>
              <Text>routeName: `{routeName}`</Text>
            </>
          )}
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    sceneKey: params.name,
    routeName: state.appRoute.routeName,
    user: state.user,
    eventClassId: state.player.detail.id,
    isLoading: state.appState.isLoading,
    currentState: state.appState.currentState,
    eventDetail: state.community.eventDetail,
    isReady: state.community.isReady,
    isConnected: state.mirror.isConnected,
    classStartAt: state.community.classStartAt,
    currentTimeZone: state.appState.currentTimeZone,
    playerDetail: state.player.detail,
    hasDistanceAdjustment: filterMirrorHasFeatureOption(state, 'distanceAdjustment'),
    hasPartnerWorkout: filterMirrorHasFeatureOption(state, 'partnerWorkout'),
    hasCoachVI: filterMirrorHasFeatureOption(state, 'coachVI'),
    hasCamera: filterMirrorHasFeatureOption(state, 'mirrorCamera'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        createNewEvent: CommunityActions.createNewEvent,
        startEventClass: CommunityActions.startEventClass,
        sync1on1StartTime: CommunityActions.sync1on1StartTime,
        fetchGet1on1SyncedStartTime: CommunityActions.fetchGet1on1SyncedStartTime,
        fetchPost1on1Notification: CommunityActions.fetchPost1on1Notification,
        fetchPut1on1OnlineStatus: CommunityActions.fetchPut1on1OnlineStatus,
        fetchGet1on1OnlineStatus: CommunityActions.fetchGet1on1OnlineStatus,
      },
      dispatch,
    ),
)(Prepare1on1SessionScreen);
