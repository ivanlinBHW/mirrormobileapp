import { ScaledSheet } from 'App/Helpers';
import { Styles, Fonts, Metrics, Colors } from 'App/Theme';

export default ScaledSheet.create({
  layout: {
    ...Styles.layoutScreen,
    flexDirection: 'column',
  },
  flatBox: {
    paddingBottom: Metrics.baseVerticalMargin * 4,
  },
  title: {
    ...Fonts.style.medium500,
    marginTop: Metrics.baseVerticalMargin,
    color: Colors.black,
  },
  content: {
    ...Fonts.style.small500,
    color: Colors.gray_02,
    marginTop: Metrics.baseVerticalMargin / 2,
  },
  collectionCardList: {
    marginHorizontal: Metrics.baseMargin,
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  marginWrapper: {
    margin: 15,
  },
});
