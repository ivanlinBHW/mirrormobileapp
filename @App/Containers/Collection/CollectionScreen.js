import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, ActivityIndicator } from 'react-native';

import { translate as t } from 'App/Helpers/I18n';
import { Classes } from 'App/Theme';
import styles from './CollectionScreenStyle';
import { CollectionCard, AutoHideFlatList } from 'App/Components';
import CollectionActions from 'App/Stores/Collection/Actions';

class Collection extends React.Component {
  static propTypes = {
    sceneKey: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,
    getCollectionList: PropTypes.func.isRequired,
    getCollectionDetail: PropTypes.func.isRequired,
    onLoading: PropTypes.bool,
    list: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
  };

  componentDidMount() {
    const { user, getCollectionList } = this.props;
    getCollectionList(user.token);
  }

  onPressCollectionDetail = (id) => {
    const { user, getCollectionDetail } = this.props;
    getCollectionDetail(id, user.token);
  };

  onPressDetail = (id) => {
    const { getCollectionDetail, user } = this.props;

    getCollectionDetail(id, user.token);
  };

  loadMoreData = () => {
    console.log('======== this.loadMoreData ========');
  };

  renderItem = ({ item }) => {
    return <CollectionCard data={item} onPress={this.onPressDetail} />;
  };

  renderFooter = () => {
    return (
      <View style={styles.footer}>
        <ActivityIndicator color="black" style={styles.marginWrapper} />
      </View>
    );
  };

  render() {
    const { list, isLoading, getCollectionList, user, sceneKey } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <AutoHideFlatList
          ListHeaderComponent={
            <View>
              {/* TODO : en missing translation */}
              <Text style={styles.title}>{t('collection_title')}</Text>
              {/* <Text style={styles.content}>{t('collection_content')}</Text> */}
            </View>
          }
          sceneKey={sceneKey}
          data={list}
          renderItem={this.renderItem}
          style={styles.collectionCardList}
          refreshing={isLoading}
          showRefresh={true}
          onRefresh={() => getCollectionList(user.token)}
          onEndReached={() => this.loadMoreData()}
          onEndReachedThreshold={0.5}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    user: state.user,
    list: state.collection.list,
    isLoading: state.appState.isLoading,
    sceneKey: params.name,
  }),
  (dispatch) => ({
    getCollectionList: (token) => dispatch(CollectionActions.getCollectionList(token)),
    getCollectionDetail: (id, token) =>
      dispatch(CollectionActions.getCollectionDetail(id, token)),
  }),
)(Collection);
