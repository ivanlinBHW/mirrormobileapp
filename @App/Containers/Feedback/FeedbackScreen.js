import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  View,
  Text,
  Platform,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import RNPickerSelect from 'react-native-picker-select';

import ClassActions from 'App/Stores/Class/Actions';
import LiveActions from 'App/Stores/Live/Actions';
import StepIndicator from 'react-native-step-indicator';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { translate as t } from 'App/Helpers/I18n';
import { Colors } from 'App/Theme';
import { Screen } from 'App/Helpers';
import { SquareButton, BaseIcon } from 'App/Components';
import { styles, pickerSelectStyles } from './FeedbackScreenStyle';

const thirdIndicatorStyles = {
  stepIndicatorSize: Platform.isPad ? Screen.scale(8) : Screen.scale(10),
  currentStepIndicatorSize: Platform.isPad ? Screen.scale(12) : Screen.scale(10),
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 2,
  stepStrokeCurrentColor: Colors.fontIcon.primary,
  stepStrokeWidth: 1,
  stepStrokeFinishedColor: Colors.fontIcon.primary,
  stepStrokeUnFinishedColor: Colors.fontIcon.primary,
  separatorFinishedColor: Colors.fontIcon.primary,
  separatorUnFinishedColor: Colors.fontIcon.primary,
  stepIndicatorFinishedColor: Colors.fontIcon.primary,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 0,
  currentStepIndicatorLabelFontSize: 0,
  stepIndicatorLabelCurrentColor: 'transparent',
  stepIndicatorLabelFinishedColor: 'transparent',
  stepIndicatorLabelUnFinishedColor: 'transparent',
  labelColor: '#000000',
  labelSize: Screen.scale(12),
  padding: 5,
  currentStepLabelColor: '#000000',
};

class FeedbackScreen extends React.Component {
  static propTypes = {
    classSubmit: PropTypes.func.isRequired,
    liveSubmit: PropTypes.func.isRequired,
    token: PropTypes.string,
    detailId: PropTypes.string,
    detail: PropTypes.object,
    routeName: PropTypes.string,
    isProgramFinished: PropTypes.bool,
    live: PropTypes.object,
    program: PropTypes.object,
    onMirrorCommunicate: PropTypes.func.isRequired,
  };

  state = {
    selectText: t('feedback_select_select_1'),
    classRate: 5,
    instructorRate: 5,
    feelRate: 3,
    userComment: t('feedback_select_select_1'),
  };

  onQ1StarRatingPress(rating) {
    this.setState({
      classRate: rating,
    });
  }

  onQ2StarRatingPress(rating) {
    this.setState({
      instructorRate: rating,
    });
  }

  onStepPress = (position) => {
    this.setState({ feelRate: position + 1 });
  };

  onPressPicker = (itemValue) => {
    if (itemValue) {
      this.setState({
        selectText: itemValue,
        userComment: itemValue,
      });
    }
  };

  onChangeText = (value) => {
    this.setState({
      userComment: value,
    });
  };

  onPressSubmit = () => {
    const {
      classSubmit,
      liveSubmit,
      token,
      detail,
      routeName,
      isProgramFinished,
      program,
      onMirrorCommunicate,
    } = this.props;
    const { classRate, instructorRate, feelRate, userComment } = this.state;

    const payload = {
      classRate,
      instructorRate,
      feelRate,
      userComment,
    };
    onMirrorCommunicate(MirrorEvents.SAVE_FEEDBACK_FINISH, {
      classId: detail.id,
    });
    if (detail.scheduledAt) {
      liveSubmit(detail.liveClassHistory.id, token, payload);
    } else {
      classSubmit(
        detail.trainingClassHistory.id,
        token,
        payload,
        routeName,
        isProgramFinished,
        detail.id,
        program ? program.id : null,
      );
    }
  };

  render() {
    const { selectText, classRate, instructorRate, feelRate, userComment } = this.state;
    const { detail } = this.props;

    return (
      <>
        <KeyboardAvoidingView
          style={styles.wrapperBox}
          keyboardVerticalOffset={
            Platform.OS === 'ios' ? Screen.scale(48) : Screen.scale(72)
          }
          behavior={Platform.OS === 'ios' ? 'position' : 'height'}
          enable
        >
          <ScrollView>
            <View style={styles.container}>
              <Text style={styles.title}>{t('workout_detail_feedback')}</Text>
              <Text style={styles.question}>{t('feedback_q1')}</Text>
              <StarRating
                disabled={false}
                maxStars={5}
                rating={classRate}
                starStyle={styles.star}
                starSize={25}
                selectedStar={(rating) => this.onQ1StarRatingPress(rating)}
                fullStarColor={Colors.fontIcon.primary}
                emptyStarColor={Colors.fontIcon.primary}
                containerStyle={styles.starBox}
              />
              <Text style={styles.question}>
                {t('feedback_q2', {
                  name: detail.instructor ? detail.instructor.title : '',
                })}
              </Text>
              <StarRating
                disabled={false}
                maxStars={5}
                rating={instructorRate}
                starStyle={styles.star}
                starSize={25}
                selectedStar={(rating) => this.onQ2StarRatingPress(rating)}
                fullStarColor={Colors.fontIcon.primary}
                emptyStarColor={Colors.fontIcon.primary}
                containerStyle={styles.starBox}
              />
              <Text style={styles.question}>{t('feedback_q3')}</Text>
              <View style={styles.stepBox}>
                <StepIndicator
                  stepCount={5}
                  customStyles={thirdIndicatorStyles}
                  currentPosition={feelRate}
                  onPress={this.onStepPress}
                  labels={[
                    t('feedback_step_1'),
                    '',
                    t('feedback_step_3'),
                    '',
                    t('feedback_step_5'),
                  ]}
                />
              </View>
              <Text style={styles.question}>{t('feedback_q4')}</Text>
              <RNPickerSelect
                style={pickerSelectStyles}
                value={selectText}
                Icon={() => <BaseIcon size={12} name="angle-down" />}
                onValueChange={(value) => this.onPressPicker(value)}
                placeholder={{
                  label: t('feedback_select_one'),
                  value: null,
                  disabled: true,
                }}
                useNativeAndroidPickerStyle={false}
                items={[
                  {
                    label: t('feedback_select_select_1'),
                    value: t('feedback_select_select_1'),
                  },
                  {
                    label: t('feedback_select_select_2'),
                    value: t('feedback_select_select_2'),
                  },
                  {
                    label: t('feedback_select_select_3'),
                    value: t('feedback_select_select_3'),
                  },
                ]}
              />
              <TextInput
                style={styles.textInputBox}
                onChangeText={this.onChangeText}
                multiline
                numberOfLines={1}
                value={userComment}
                maxLength={254}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <SquareButton
          text={t('submit')}
          onPress={this.onPressSubmit}
          disabled={!userComment}
        />
      </>
    );
  }
}

export default connect(
  (state) => ({
    currentClassType: state.mirror.currentClassType,
    token: state.user.token,
    classId: state.mirror.currentClassId,
    detailId: state.player.detail.id,
    live: state.live.detail,
    detail: state.player.detail,
    program: state.program.detail,
    routeName: state.player.routeName,
    isProgramFinished: state.program.isProgramFinished,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        classSubmit: ClassActions.submitFeedback,
        liveSubmit: LiveActions.submitLiveFeedback,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(FeedbackScreen);
