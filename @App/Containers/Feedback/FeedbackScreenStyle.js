import { Platform } from 'react-native';
import { ScaledSheet, Screen, isIphoneX } from 'App/Helpers';
import { Colors, Classes, Fonts, Metrics } from 'App/Theme';

export const pickerSelectStyles = ScaledSheet.create({
  inputIOS: {
    fontSize: Fonts.size.small,
    paddingVertical: '10@vs',
    borderWidth: 1,
    borderColor: Colors.black,
    borderRadius: 2,
    color: 'black',
    marginHorizontal: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
  },
  inputAndroid: {
    fontSize: Fonts.size.small,
    paddingVertical: '10@vs',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 2,
    color: 'black',
    marginHorizontal: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
  },
  iconContainer: {
    ...Platform.select({
      ios: {
        right: Platform.isPad ? '32@sr' : isIphoneX() ? '32@sr' : '30@sr',
        top: Platform.isPad ? '10@sr' : isIphoneX() ? '14@sr' : '12@sr',
      },
      android: {
        right: '34@sr',
        top: '18@sr',
      },
    }),
  },
});

export const styles = ScaledSheet.create({
  container: {
    flexDirection: 'column',
    paddingTop: '13@vs',
    flex: 1,
  },
  wrapperBox: {
    ...Classes.fill,
  },
  title: {
    ...Fonts.style.regular500,
    textAlign: 'center',
  },
  question: {
    ...Fonts.style.medium500,
    marginTop: '48.5@vs',
    marginBottom: '24@vs',
    textAlign: 'center',
  },
  star: {
    marginRight: '10@s',
  },
  starBox: {
    width: Platform.isPad
      ? Screen.width - Screen.scale(80)
      : Screen.width - Screen.scale(58),
    marginLeft: Platform.isPad ? '42@sr' : '32@sr',
  },
  stepBox: {
    backgroundColor: '#ffffff',
  },
  pickerBox: {
  },
  pickerItem: {
    width: '100%',
  },
  inputIOS: {
    fontSize: Fonts.size.small,
    paddingVertical: '10@vs',
    borderWidth: 1,
    borderColor: Colors.black,
    borderRadius: 2,
    color: 'black',
    marginHorizontal: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
  },
  inputAndroid: {
    fontSize: Fonts.size.small,
    paddingVertical: '10@vs',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 2,
    color: 'black',
    marginHorizontal: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
  },
  textInputBox: {
    minHeight: '104@sr',
    height: Platform.isPad ? '150@sr' : isIphoneX() ? '130@sr' : '104@sr',
    borderWidth: 1,
    borderColor: '#545454',
    marginTop: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin,
    textAlignVertical: 'top',
    marginHorizontal: Metrics.baseMargin,
    width: Screen.width - Screen.scale(32),
    paddingHorizontal: Metrics.baseMargin,
    paddingTop: Metrics.baseMargin / 2,
  },
  submitStyle: {
    height: '44@vs',
    color: 'white',
    fontSize: '14@vs',
  },
  flexWrap: {
    flex: 1,
  },
  iconStyle: {
    ...Platform.select({
      ios: {
        right: 30,
        top: 6,
      },
      android: {
        right: 32,
        top: 10,
      },
    }),
  },
});
