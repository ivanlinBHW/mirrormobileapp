import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { get, isNil, isString, isEmpty } from 'lodash';
import { View, Text } from 'react-native';

import { Date as d, Dialog, Subscription } from 'App/Helpers';
import { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import {
  SquareButton,
  RoundLabel,
  CachedImage as Image,
  CachedImage as ImageBackground,
} from 'App/Components';
import { Images, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import {
  ClassActions,
  CollectionActions,
  MirrorActions,
  PlayerActions,
  ProgramActions,
  InstructorActions,
} from 'App/Stores';
import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';
import styles from './ClassDetailStyle';
import ClassDetailView from './ClassDetailView';

class ClassDetail extends React.Component {
  static propTypes = {
    playerDetail: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    prevRoute: PropTypes.string,
    startClass: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
    setInstructorId: PropTypes.func.isRequired,
    routeName: PropTypes.string,
    activeTrainingProgramId: PropTypes.string,
    isFinished: PropTypes.bool,
    resumeClass: PropTypes.func.isRequired,
    startProgramClass: PropTypes.func.isRequired,
    updatePlayerStore: PropTypes.func.isRequired,
    programHistoryId: PropTypes.string,
    resumeProgramClass: PropTypes.func.isRequired,
    trainingProgramClassHistoryId: PropTypes.string,
    currentRouteName: PropTypes.string,
    onMirrorCommunicate: PropTypes.func.isRequired,
    isOnlySee: PropTypes.bool,
    isConnected: PropTypes.bool.isRequired,
    mainSubscription: PropTypes.object,
    isLoading: PropTypes.bool.isRequired,
    mirrorOptionMotionCapture: PropTypes.bool.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
    nextShowDistance: PropTypes.bool.isRequired,
    isSubscriptionExpired: PropTypes.bool.isRequired,
    classCanEnableMotionCapture: PropTypes.bool.isRequired,
    isResumable: PropTypes.bool.isRequired,
    shouldButtonDisabled: PropTypes.bool.isRequired,

    hasDistanceAdjustment: PropTypes.bool.isRequired,
    hasPartnerWorkout: PropTypes.bool.isRequired,
    hasCoachVI: PropTypes.bool.isRequired,

    setClassBookmark: PropTypes.func.isRequired,
    removeClassBookmark: PropTypes.func.isRequired,
    setCollectionBookmark: PropTypes.func.isRequired,
    removeCollectionBookmark: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isOnlySee: false,
    routeName: '',
    prevRoute: '',
    mainSubscription: {},
  };

  renderStripesView = (list) =>
    list.map((item) => ({
      leftComponent: <Text style={styles.stripeText}>{item.title}</Text>,
      rightComponent: (
        <View style={styles.stripeRight}>
          {item.captureSetting && (
            <Image source={Images.motion_capture} style={styles.stripeImage} />
          )}
          <Text style={styles.stripeSecondText}>{d.mmss(item.duration)}</Text>
        </View>
      ),
      disabled: true,
    }));

  renderItem = ({ item, index }) => {
    return (
      <View key={index} style={styles.imageBox}>
        <Image
          source={item.signedDeviceCoverImageObj}
          style={styles.equipments}
          resizeMode="contain"
        />
        <Text style={styles.equipmentsText}>{item.title}</Text>
      </View>
    );
  };

  handleStartClass = (startDistanceMeasuring = false) => {
    console.log(
      '=== handleStartClass startDistanceMeasuring ===',
      startDistanceMeasuring,
    );
    const {
      token,
      playerDetail,
      startClass,
      routeName,
      currentRouteName,
      startProgramClass,
      isMotionCaptureEnabled,
      trainingProgramClassHistoryId,
    } = this.props;
    if (routeName === 'ProgramClassDetail' && currentRouteName === 'ProgramClassDetail') {
      startProgramClass(trainingProgramClassHistoryId, token, playerDetail.id, {
        enableMotionCapture: isMotionCaptureEnabled,
        'start-measure-distance': startDistanceMeasuring,
      });
    } else {
      startClass(playerDetail.id, token, currentRouteName, {
        enableMotionCapture: isMotionCaptureEnabled,
        'start-measure-distance': startDistanceMeasuring,
      });
    }
  };

  handlePressStartClass = () => {
    let {
      isMotionCaptureEnabled,
      nextShowDistance,
      classCanEnableMotionCapture,
    } = this.props;
    console.info('=== isMotionCaptureEnabled ===', isMotionCaptureEnabled);
    if (!classCanEnableMotionCapture) isMotionCaptureEnabled = false;
    if (isMotionCaptureEnabled) {
      if (nextShowDistance) {
        Dialog.showConfirmCameraUsageAlert(() =>
          Dialog.requestDistanceAdjustmentWhenEnableMotionCapture(this.handleStartClass),
        );
      } else {
        Dialog.showConfirmCameraUsageAlert(this.handleStartClass);
      }
    } else {
      this.handleStartClass();
    }
  };

  resumeClass = () => {
    const {
      token,
      data,
      resumeClass,
      routeName,
      currentRouteName,
      resumeProgramClass,
      trainingProgramClassHistoryId,
      isMotionCaptureEnabled,
      nextShowDistance,
    } = this.props;

    const doResumeClass = (startDistanceMeasuring = false) => {
      if (
        routeName === 'ProgramClassDetail' &&
        currentRouteName === 'ProgramClassDetail'
      ) {
        resumeProgramClass(trainingProgramClassHistoryId, token, routeName, data.id, {
          enableMotionCapture: isMotionCaptureEnabled,
          'start-measure-distance': startDistanceMeasuring,
        });
      } else {
        console.log('=== resumeClass ===', data.trainingClassHistory);
        resumeClass(data.trainingClassHistory.id, token, data.id, {
          enableMotionCapture: isMotionCaptureEnabled,
          'start-measure-distance': startDistanceMeasuring,
        });
      }
    };
    if (isMotionCaptureEnabled) {
      if (nextShowDistance) {
        Dialog.showConfirmCameraUsageAlert(() =>
          Dialog.requestDistanceAdjustmentWhenEnableMotionCapture(doResumeClass),
        );
      } else {
        Dialog.showConfirmCameraUsageAlert(this.handleStartClass);
      }
    } else {
      doResumeClass();
    }
  };

  onPressInstructor = () => {
    const { data, setInstructorId, token } = this.props;
    setInstructorId(data.instructor.id, token);
  };

  onMotionCaptureChanged = (isMotionCaptureEnabled) => {
    const { updatePlayerStore } = this.props;
    updatePlayerStore({
      isMotionCaptureEnabled,
    });
  };

  renderClassCard = () => {
    const { data } = this.props;
    return (
      <ImageBackground
        resizeMode="cover"
        source={data.signedCoverImageObj}
        style={styles.classImgWidth}
        imageSize="2x"
        imageType="h"
      >
        <View style={styles.classFragment}>
          <View style={styles.classLeftBox}>
            <View style={styles.classLeft}>
              <Text style={styles.classSecondTitle}>{t('__class')}</Text>
              <Text style={styles.classTitle}>
                {isString(data.title) && data.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.classRightBox}>
            <View style={styles.classRight}>
              <RoundLabel text={t(`search_lv${data.level}`)} />
              <RoundLabel text={`${Math.round(data.duration / 60)} ${t('min')}`} />
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  };

  renderStartButton = () => {
    const {
      isResumable,
      playerDetail: data,
      isSubscriptionExpired,
      mainSubscription,
      shouldButtonDisabled,
    } = this.props;
    console.log('=== data ===', data);
    if (isEmpty(mainSubscription) && data.classType === 0) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestAccountSubscriptionCodeAlert(this.handlePressStartClass)}
          disabled={shouldButtonDisabled}
          text={t('class_detail_request_account_subscription_alert_title')}
        />
      );
    } else if (isSubscriptionExpired && data.classType === 0) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestAccountSubscriptionExpiredAlert(
            this.handlePressStartClass,
          )}
          disabled={shouldButtonDisabled}
          text={t('class_detail_request_account_subscription_expired_title')}
        />
      );
    } else if (!data.isSubscribed && data.classType === 0) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestClassSubscriptionAlert(this.handlePressStartClass)}
          disabled={shouldButtonDisabled}
          text={t('class_detail_request_class_subscription_alert_title')}
        />
      );
    } else {
      if (isResumable) {
        return (
          <SquareButton
            color={Colors.black}
            onPress={this.handlePressStartClass}
            disabled={shouldButtonDisabled}
            text={t('class_detail_start_class')}
          />
        );
      }
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={this.handlePressStartClass}
          disabled={shouldButtonDisabled}
          text={t('class_detail_start_class')}
        />
      );
    }
  };

  renderResumeButton = () => {
    const {
      isSubscriptionExpired,
      playerDetail,
      routeName,
      mainSubscription,
      isResumable,
      shouldButtonDisabled,
    } = this.props;

    if (
      (isSubscriptionExpired ||
        isEmpty(mainSubscription) ||
        !playerDetail.isSubscribed) &&
      playerDetail.classType === 0
    ) {
      return null;
    }
    if (routeName === 'ProgramClassDetail') {
      if (isResumable) {
        return (
          <SquareButton
            disabled={shouldButtonDisabled}
            onPress={this.resumeClass}
            color={Colors.button.primary.content.background}
            text={t('class_detail_resume_class')}
          />
        );
      }
    } else {
      if (isResumable) {
        return (
          <SquareButton
            disabled={shouldButtonDisabled}
            onPress={this.resumeClass}
            color={Colors.button.primary.content.background}
            text={t('class_detail_resume_class')}
          />
        );
      }
    }
  };

  onPressBack = () => {
    const { onMirrorCommunicate, data } = this.props;
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS_PREVIEW, {
      classId: data.id,
      classType: 'on-demand',
    });
  };

  getBackToRouteName = () => {
    const { routeName, activeTrainingProgramId } = this.props;
    console.log('=== getBackToRouteName ===', routeName);
    if (routeName === 'Program' || routeName === 'ProgramClassDetail') {
      if (activeTrainingProgramId) {
        return 'Program';
      } else {
        return '';
      }
    } else if (routeName === 'CollectionScreen') {
      return 'CollectionDetail';
    } else if (routeName === 'SearchScreen') {
      return 'ClassScreen';
    } else if (routeName === 'CommunityDetailScreen') {
      return 'CommunityDetailScreen';
    } else {
      return '';
    }
  };

  onPressBookmark = (id, item) => {
    const {
      routeName,
      setClassBookmark,
      removeClassBookmark,
      setCollectionBookmark,
      removeCollectionBookmark,
    } = this.props;

    if (item.isBookmarked) {
      console.log('class removeClassBookmark');

      if (routeName === 'CollectionScreen') {
        removeCollectionBookmark(id);
      } else {
        removeClassBookmark(id);
      }
    } else {
      console.log('class setClassBookmark');

      if (routeName === 'CollectionScreen') {
        setCollectionBookmark(id);
      } else {
        setClassBookmark(id);
      }
    }
  };

  render() {
    const {
      playerDetail,
      isOnlySee = false,
      isConnected,
      isLoading,
      prevRoute,
      mainSubscription,
      setInstructorId,
      updatePlayerStore,
      isMotionCaptureEnabled,
      mirrorOptionMotionCapture,
      isResumable,
      hasCoachVI,
      hasDistanceAdjustment,
    } = this.props;

    return (
      <ClassDetailView
        hasDistanceAdjustment={hasDistanceAdjustment}
        hasCoachVI={hasCoachVI}
        isOnlySee={isOnlySee}
        isLoading={isLoading}
        isConnected={isConnected}
        isMotionCaptureEnabled={isMotionCaptureEnabled}
        setInstructorId={setInstructorId}
        playerDetail={playerDetail}
        prevRouteName={prevRoute}
        mainSubscription={mainSubscription}
        backToRouteName={this.getBackToRouteName()}
        renderResumeButton={this.renderResumeButton}
        renderStartButton={this.renderStartButton}
        mirrorOptionMotionCapture={mirrorOptionMotionCapture}
        onPressBack={this.onPressBack}
        onMotionCaptureChanged={updatePlayerStore}
        isResumable={isResumable}
        onPressBookmark={this.onPressBookmark}
      />
    );
  }
}

export default connect(
  (state, params) => ({
    playerDetail: state.player.detail,
    isMotionCaptureEnabled:
      'isMotionCaptureEnabled' in state.player
        ? state.player.isMotionCaptureEnabled
        : false,
    isConnected: state.mirror.isConnected,
    data: state.class.detail,
    token: state.user.token,
    routeName: state.player.routeName,
    activeTrainingProgramId: state.user.activeTrainingProgramId,
    programHistoryId: state.user.activeTrainingProgramHistoryId,
    trainingProgramClassHistoryId: state.user.trainingProgramClassHistoryId,
    mainSubscription: state.user.mainSubscription,
    connectSuccessResult: state.mirror.connectSuccessResult,
    mirrorOptionMotionCapture: state.mirror.options[MirrorOptions.MOTION_TRACKER],
    prevRoute: state.appRoute.prevRoute,
    isLoading: state.appState.isLoading,
    currentRouteName: params.name,
    nextShowDistance: state.user.isNextShowDistance,
    classCanEnableMotionCapture: state.class.detail.hasMotionCapture,

    isSubscriptionExpired: Subscription.isExpired(
      state.user.mainSubscription,
      state.player.detail,
    ),
    isResumable: !isNil(get(state, 'player.detail.trainingClassHistory.pausedStepId')),
    shouldButtonDisabled:
      state.mirror.isWaitingForDeviceStartClass || state.appState.isLoading,
    hasDistanceAdjustment: filterMirrorHasFeatureOption(state, 'distanceAdjustment'),
    hasPartnerWorkout: filterMirrorHasFeatureOption(state, 'partnerWorkout'),
    hasCoachVI: filterMirrorHasFeatureOption(state, 'coachVI'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        setCollectionBookmark: CollectionActions.setCollectionBookmark,
        removeCollectionBookmark: CollectionActions.removeCollectionBookmark,
        setClassBookmark: ClassActions.setClassBookmark,
        removeClassBookmark: ClassActions.removeClassBookmark,
        startClass: ClassActions.startClass,
        resumeClass: ClassActions.resumeClass,
        startProgramClass: ProgramActions.startProgramClass,
        resumeProgramClass: ProgramActions.resumeProgramClass,
        setInstructorId: InstructorActions.setInstructorId,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        updatePlayerStore: PlayerActions.updatePlayerStore,
      },
      dispatch,
    ),
)(ClassDetail);
