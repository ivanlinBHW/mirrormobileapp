import React from 'react';
import PropTypes from 'prop-types';
import { TouchableWithoutFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { get, isArray, isEmpty, isString } from 'lodash';
import {
  View,
  Text,
  FlatList,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

import {
  CachedImage as ImageBackground,
  CachedImage as Image,
  BaseImageButton,
  SecondaryNavbar,
  SquareButton,
  BaseReadMore,
  BaseAvatar,
  RoundLabel,
  BaseSwitch,
  HeadLine,
  StripeView,
} from 'App/Components';
import { Images, Colors, Classes, Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d, Screen, Dialog } from 'App/Helpers';
import styles from './ClassDetailStyle';
import LinearGradient from 'react-native-linear-gradient';
class ClassDetailView extends React.PureComponent {
  static propTypes = {
    onMotionCaptureChanged: PropTypes.func.isRequired,
    renderResumeButton: PropTypes.func.isRequired,
    renderStartButton: PropTypes.func.isRequired,
    backToRouteName: PropTypes.string,
    setInstructorId: PropTypes.func.isRequired,
    playerDetail: PropTypes.object.isRequired,
    isConnected: PropTypes.bool.isRequired,
    onPressBack: PropTypes.func.isRequired,
    isMotionCaptureEnabled: PropTypes.bool,
    mirrorOptionMotionCapture: PropTypes.bool,
    mainSubscription: PropTypes.object,
    prevRouteName: PropTypes.string,
    isOnlySee: PropTypes.bool,
    isResumable: PropTypes.bool,
    hasCoachVI: PropTypes.bool.isRequired,
    token: PropTypes.string,
    eventType: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onPressBookmark: PropTypes.func.isRequired,
  };

  static defaultProps = {
    backToRouteName: '',
    isMotionCaptureEnabled: false,
    isOnlySee: false,
    requiredEquipments: [],
    mainSubscription: {},
    trainingSteps: [],
    routeName: '',
    prevRouteName: '',
    eventType: '-',
    hasCoachVI: false,
  };
  state = {
    isMotionCaptureEnabled: false,
  };

  constructor(props) {
    super(props);
    let isMotionCaptureEnabled = false;
    let classHasMotionCapture = props.playerDetail.hasMotionCapture;

    if (classHasMotionCapture) {
      if (props.mirrorOptionMotionCapture) isMotionCaptureEnabled = true;
      if (props.isMotionCaptureEnabled) isMotionCaptureEnabled = true;
    }

    this.state = {
      isMotionCaptureEnabled,
    };

    const { onMotionCaptureChanged } = props;
    if (isMotionCaptureEnabled == true) {
      onMotionCaptureChanged({
        isMotionCaptureEnabled,
      });
      this.setState({
        isMotionCaptureEnabled,
      });
    }
  }
  handleMotionCaptureChange = () => {
    const { onMotionCaptureChanged } = this.props;

    let isMotionCaptureEnabled = !this.state.isMotionCaptureEnabled;
    this.setState((state) => ({
      isMotionCaptureEnabled,
    }));
    console.log('=== handleMotionCaptureChange ===', isMotionCaptureEnabled);
    onMotionCaptureChanged({ isMotionCaptureEnabled });
  };
  componentDidMount() {
    console.log('@ClassView entered');
  }
  componentDidUpdate(prevProps) {
    if (prevProps.isMotionCaptureEnabled !== this.props.isMotionCaptureEnabled) {
      this.setState((state) => ({
        isMotionCaptureEnabled: this.props.isMotionCaptureEnabled,
      }));
    }
  }
  onPressInstructor = () => {
    const {
      playerDetail: { instructor = {} },
      setInstructorId,
      token,
    } = this.props;
    setInstructorId(instructor.id, token);
  };

  renderStripesView = (list) =>
    list.map((item) => ({
      leftComponent: <Text style={styles.stripeText}>{item.title}</Text>,
      rightComponent: (
        <View style={styles.stripeRight}>
          {item.captureSetting && (
            <Image source={Images.motion_capture} style={styles.stripeImage} />
          )}
          <Text style={styles.stripeSecondText}>{d.mmss(item.duration)}</Text>
        </View>
      ),
      disabled: true,
    }));

  renderEquipmentItems = ({ item, index }) => {
    return (
      <View key={index} style={styles.imageBox}>
        <Image
          source={{
            uri: get(item, 'signedDeviceCoverImage') || '',
          }}
          style={styles.equipments}
          resizeMode="contain"
          imageSize="thum"
          imageType="s"
        />
        <Text style={styles.equipmentsText}>{item.title}</Text>
      </View>
    );
  };

  renderClassCard = () => {
    const { playerDetail, onPressBookmark } = this.props;
    return (
      <ImageBackground
        resizeMode="cover"
        source={playerDetail.signedCoverImageObj}
        style={styles.classImgWidth}
        imageSize="2x"
        imageType="h"
      >
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
          locations={[0, 1]}
          colors={[`rgba(50,50,50,0)`, `rgba(50,50,50,0.4)`]}
        />

        <View style={styles.classFragment}>
          <View style={styles.classLeftBox}>
            <View style={styles.classLeft}>
              <Text style={styles.classSecondTitle}>{t('__class')}</Text>
              <Text style={styles.classTitle}>
                {isString(playerDetail.title) && playerDetail.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.classRightBox}>
            <View style={styles.classRight}>
              <RoundLabel text={t(`search_lv${playerDetail.level}`)} />
              <RoundLabel
                text={`${Math.round(playerDetail.duration / 60)} ${t('min')}`}
              />
            </View>
            {onPressBookmark && playerDetail.classType === 0 && (
              <BaseImageButton
                style={styles.btnBookmark}
                imageStyle={styles.btnBookmarkImage}
                source={
                  playerDetail.isBookmarked
                    ? Images.bookmark_solid_fixed
                    : Images.bookmark_black
                }
                onPress={() => onPressBookmark(playerDetail.id, playerDetail)}
                throttleTime={100}
              />
            )}
          </View>
        </View>
      </ImageBackground>
    );
  };

  render() {
    const {
      playerDetail: {
        instructor: {
          title = '',
          description: instructorDescription = '',
          signedAvatarImageObj,
        },
        trainingClassHistory,
        requiredEquipments = [],
        trainingSteps = [],
        hasMotionCapture,
        isSubscribed,
        isPublished,
        description,
        calories,
        tags,
      },
      eventType,
      isOnlySee = false,
      isConnected,
      prevRouteName,
      mainSubscription,
      renderResumeButton,
      renderStartButton,
      backToRouteName,
      onPressBack,
      isResumable,
      hasCoachVI,
    } = this.props;

    const { isMotionCaptureEnabled } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <SecondaryNavbar
          back
          backTo={backToRouteName}
          onPressBack={onPressBack}
          navRightComponent={
            <View style={styles.navBox}>
              <BaseImageButton
                style={styles.navVoiceIcon}
                source={Images.mirror}
                onPress={Actions.DeviceModal}
              />
              <BaseImageButton
                style={styles.navVoiceIcon}
                source={Images.voice}
                onPress={Actions.AudioModal}
                disabled={!isConnected}
              />
              <BaseImageButton
                style={styles.navSettingIcon}
                source={Images.setting}
                onPress={Actions.WorkoutOptionModal}
                disabled={!isConnected}
              />
            </View>
          }
        />
        <View style={styles.layoutBox}>
          <ScrollView style={styles.scrollBox}>
            {this.renderClassCard()}
            {eventType !== 1 && hasMotionCapture && (
              <View
                style={[
                  Classes.rowCross,
                  Classes.mainSpaceBetween,
                  Classes.paddingTop,
                  Classes.paddingLeft,
                  Classes.paddingRight,
                ]}
              >
                <View style={[Classes.fill, Classes.rowCross, Classes.mainStart]}>
                  <Image source={Images.motion_capture} style={styles.captureImg} />
                  <Text
                    style={[
                      Fonts.style.medium500,
                      !hasCoachVI && styles.noFeatureTextStyle,
                    ]}
                  >
                    {t('motion_capture')}
                  </Text>
                </View>
                <TouchableWithoutFeedback
                  onPress={Dialog.showDeviceHasNoCameraAlert}
                  pointerEvents={hasCoachVI ? 'box-none' : 'box-only'}
                >
                  <View pointerEvents={hasCoachVI ? 'box-none' : 'box-only'}>
                    <BaseSwitch
                      active={isMotionCaptureEnabled}
                      onValueChange={this.handleMotionCaptureChange}
                      disabled={!hasCoachVI || !isConnected}
                    />
                  </View>
                </TouchableWithoutFeedback>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine
                paddingHorizontal={16}
                title={t('class_detail_about_this_workout')}
              />
              <BaseReadMore text={description} />
            </View>
            <View style={styles.contentBox}>
              <HeadLine paddingHorizontal={16} title={t('class_detail_good_for_tags')} />
              <View style={[Classes.row, Classes.paddingLeft, Classes.paddingRight]}>
                {isArray(tags) &&
                  tags.map((e, i) => (
                    <Text key={`${e.tagId}`} style={styles.txtTags}>
                      {e.name}
                      {i !== tags.length - 1 ? ', ' : '.'}
                    </Text>
                  ))}
              </View>
            </View>
            {(!!calories || calories !== 0) && (
              <View style={styles.contentBox}>
                <HeadLine
                  paddingHorizontal={16}
                  title={t('class_detail_reference_calories')}
                />
                <Text style={Classes.paddingLeft}>
                  {calories} {t('cal')}
                </Text>
                <Text style={[styles.context, styles.txtCaloriesDesc]}>
                  {t('class_detail_calories_description')}
                </Text>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine paddingHorizontal={16} title={t('class_detail_instructor')} />
              <View style={styles.instructorBox}>
                <BaseAvatar
                  uri={signedAvatarImageObj ? signedAvatarImageObj : Images.defaultAvatar}
                  style={styles.avatarImage}
                  size={Screen.scale(49)}
                  imageSize="thum"
                  imageType="s"
                />
                <Text style={styles.instructorText}>{title}</Text>
              </View>
              <Text style={styles.context} numberOfLines={2}>
                {instructorDescription}
              </Text>
              <TouchableOpacity onPress={this.onPressInstructor}>
                <Text style={[styles.expandText, styles.marginBetween]}>
                  {t('view_profile')}
                </Text>
              </TouchableOpacity>
            </View>
            {requiredEquipments.length > 0 && (
              <View style={styles.contentBox}>
                <HeadLine
                  paddingHorizontal={16}
                  title={t('class_detail_equipment_needed')}
                />
                <View style={styles.equipmentBox}>
                  <FlatList
                    horizontal
                    data={requiredEquipments}
                    renderItem={this.renderEquipmentItems}
                  />
                </View>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine
                title={`${trainingSteps.length} ${t('class_detail_exercise')}`}
                hrLineStyle={styles.hrLine}
                paddingHorizontal={16}
              />
              <StripeView
                style={styles.stripe}
                buttonStyle={styles.stripeBtn}
                stripStyle={styles.stripeStyle}
                stripeLightColor={Colors.white}
                stripeDarkColor={Colors.white_02}
                stripes={this.renderStripesView(trainingSteps)}
              />
            </View>

            {__DEV__ && (
              <View style={Classes.backgroundReset}>
                <Text>
                  {'DEV ONLY\n'}
                  {'Account Subscribed: '}
                  {JSON.stringify(!isEmpty(mainSubscription))}
                </Text>
                <Text>
                  {'Class Subscribed: '}
                  {JSON.stringify(isSubscribed)}
                </Text>
                <Text>
                  {'prevRouteName: '}
                  {prevRouteName}
                  {'\nisOnlySee:'}
                  {isOnlySee ? 'yes' : 'no'}
                  {'\neventType:'}
                  {eventType}
                  {'\nisResumable:'}
                  {isResumable ? 'yes' : 'no'}
                  {'\ntrainingClassHistory:'}
                  {JSON.stringify(trainingClassHistory)}
                  {'\nisMotionCaptureEnabled:'}
                  {isMotionCaptureEnabled ? 'yes' : 'no'}
                </Text>
                {__DEV__ && (
                  <SquareButton
                    onPress={Actions.PlayerScreen}
                    text="debug - force start"
                  />
                )}
              </View>
            )}
          </ScrollView>
        </View>

        {isConnected ? (
          <View style={styles.btnBox}>
            {!isOnlySee && renderResumeButton()}
            {!isOnlySee && renderStartButton()}
          </View>
        ) : (
          !isOnlySee &&
          prevRouteName !== 'ClassesSelectedScreen' && (
            <View style={styles.btnBox}>
              <SquareButton
                onPress={Actions.DeviceModal}
                color={Colors.button.primary.content.background}
                text={t('class_detail_connect_device')}
              />
            </View>
          )
        )}
      </SafeAreaView>
    );
  }
}

export default ClassDetailView;
