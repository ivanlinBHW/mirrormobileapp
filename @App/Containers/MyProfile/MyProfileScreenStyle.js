import { ScaledSheet } from 'App/Helpers';
import { Metrics, Classes, Colors, Fonts } from 'App/Theme';
import { Screen } from 'App/Helpers';
export default ScaledSheet.create({
  container: {
    flex: 1,
  },
  navContainer: {
    flex: 1,
  },
  full: {
    width: '100%',
  },
  headerWrapper: {
    flexDirection: 'column',
    marginTop: '24@vs',
    alignItems: 'center',
  },
  rightText: {
    ...Fonts.style.regular500,
    marginTop: '24@vs',
    marginBottom: Metrics.baseMargin,
  },
  disabledText: {
    color: Colors.gray_03,
  },
  btnStyle: {
    height: '24@vs',
    paddingHorizontal: '18@s',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: Colors.button.primary.outline.border,
    marginBottom: '24@vs',
  },
  textStyle: {
    color: Colors.button.primary.outline.text,
    fontSize: Fonts.size.small,
  },
  wrapper: {
    borderTopWidth: 1,
    borderColor: Colors.black_15,
  },
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  flexBox: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  leftTitle: {
    width: '128@s',
    fontSize: Fonts.size.medium,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    letterSpacing: 0,
  },
  inputStyle: {
    fontSize: Fonts.size.medium,
    paddingTop: '2@s',
    textAlignVertical: 'bottom',
  },
  inputContainerStyle: {
    width: '72%',
    borderBottomColor: Colors.black_10,
    marginHorizontal: Metrics.baseMargin / 2,
  },
  rightIcon: {
    ...Classes.paddingRight,
    marginRight: Metrics.baseMargin,
  },
  img: {
    width: '6@s',
    height: '11@vs',
    marginVertical: '10@s',
    marginRight: Metrics.baseMargin,
  },
  lastBox: {
    marginBottom: '11@vs',
  },
  downBtn: {
    width: '30@s',
    height: '30@vs',
  },
  downImg: {
    width: '30@s',
    height: '30@vs',
  },
  box: {
    width: '100%',
    marginVertical: '23@vs',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  navText: {
    fontSize: Fonts.size.regular,
    color: Colors.button.primary.text.text,
  },
  btnMargin: {
    marginTop: Metrics.baseMargin,
  },
  placeholder: {
    marginLeft: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
    color: Colors.gray_02,
    marginVertical: Metrics.baseMargin / 2,
  },
  itemWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_15,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: Metrics.baseMargin,
  },
  itemTitle: {
    color: Colors.black,
    fontSize: Fonts.size.medium,
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
  },
  activityItemTitle: {
    marginLeft: 0,
  },
  activityItemWrapper: {
    width: '86%',
  },
  itemContainer: {
    width: '100%',
    flexDirection: 'row',
  },
  activityImg: {
    height: 35,
    width: '30@vs',
    marginVertical: '9@vs',
    marginHorizontal: Metrics.baseMargin,
  },
  emptyBox: {
    height: Metrics.baseMargin,
    width: '100%',
  },
  scheduleTitle: {
    marginLeft: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
    marginTop: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  picker: {
    marginHorizontal: Metrics.baseMargin,
    height: '40@vs',
    borderWidth: 1,
    borderColor: Colors.gray_01,
    marginBottom: '40@vs',
    paddingLeft: Metrics.baseMargin,
    paddingTop: 0,
    color: Colors.black,
  },
  pickerSmallMarginBottom: {
    marginHorizontal: Metrics.baseMargin,
    height: '40@vs',
    borderWidth: 1,
    borderColor: Colors.gray_01,
    paddingLeft: Metrics.baseMargin,
    paddingTop: 0,
    color: Colors.black,
  },
  iconContainer: {
    top: Metrics.baseMargin / 2,
    right: 24,
  },
  textBoxStyle: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '68%',
    marginTop: Metrics.baseMargin / 2,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  rightEditText: {
    paddingVertical: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
  },
  Editimg: {
    width: '6@s',
    height: '11@s',
    marginTop: '22@s',
    marginRight: Metrics.baseMargin,
  },
  wrapperContainer: {
    width: '100%',
    height: '100%',
  },
  containerStyle: {
    height: '58@s',
  },
  textInputStyle: {
    height: Metrics.baseMargin * 4,
  },
  postfixStyle: {
    ...Classes.paddingRight,
    top: Screen.scale(20),
  },
});
