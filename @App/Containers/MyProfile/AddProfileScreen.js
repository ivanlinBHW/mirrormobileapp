import * as yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Alert,
  Image,
  Platform,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  FlatList,
} from 'react-native';
import { Formik } from 'formik';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import * as ImagePicker from 'react-native-image-picker';

import {
  BaseAvatar,
  RoundButton,
  DismissKeyboard,
  SecondaryNavbar,
  HorizontalTextInput,
  DateTimePickerModal,
  BouncyCheckbox,
  SquareButton,
} from 'App/Components';
import { SettingActions, SearchActions } from 'App/Stores';
import { Images, Classes, Colors, Metrics } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog, Permission } from 'App/Helpers';
import RadioForm from 'react-native-simple-radio-button';
import { Date as d } from 'App/Helpers';

import styles from './MyProfileScreenStyle';

const yString = yup.string();

const validationSchema = yup.object().shape({
  fullName: yString.required(t('required_field')),
  height: yup
    .number()
    .required(t('required_field'))
    .typeError(t('must_be_number'))
    .positive(t('must_be_positive'))
    .min(61, t('must_not_smaller_then', { min: 61 }))
    .max(302, t('must_not_greater_then', { max: 302 })),
  weight: yup
    .number()
    .required(t('required_field'))
    .typeError(t('must_be_number'))
    .positive(t('must_be_positive'))
    .min(30, t('must_not_smaller_then', { min: 30 }))
    .max(227, t('must_not_greater_then', { max: 227 })),
  birth: yString.required(t('register_validation_birth_required')),
  gender: yup.number().required(t('required_field')),
  age: yup
    .number()
    .required(t('required_field'))
    .min(18, t('register_validation_age_min_error')),
});
const initialValues = {
  name: '',
  fullName: '',
  gender: 0,
  birth: new Date(d.moment().subtract(20, 'years')),
  age: 20,
  defaultPrivacy: 1,
};
class AddProfileScreen extends React.Component {
  static propTypes = {
    setting: PropTypes.object,
    editSetting: PropTypes.object,
    updateSetting: PropTypes.func.isRequired,
    updateAvatar: PropTypes.func.isRequired,
    updatePayload: PropTypes.func.isRequired,
    payload: PropTypes.object,
    currentLocales: PropTypes.array.isRequired,
    userSetting: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const { setting = { settings: {} } } = props;

    this.state = {
      payload: {},
      isDisabled: false,
      list: [
        {
          title: 'setting_privacy_everyone',
          key: 1,
        },
        {
          title: 'setting_privacy_friend',
          key: 2,
        },
        {
          title: 'setting_privacy_onlyme',
          key: 3,
        },
      ].map((e) => ({ ...e, selected: e.key === initialValues.defaultPrivacy })),
      selected: initialValues.defaultPrivacy,
      initialValues,
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@AddProfileScreen');
  }

  renderGender = (value) => {
    switch (value) {
      case 0:
        return t('register_input_male');
      case 1:
        return t('register_input_female');
      case 2:
        return 'Unspecified';
      default:
        return 'undefined';
    }
  };

  handleOpenCamera = async () => {
    if (Platform.OS === 'android') {
      const hasPermission = await Permission.checkAndRequestPermission(
        Permission.permissionType.CAMERA,
      );
      if (!hasPermission) {
        Dialog.requestCameraPermissionFromSystemAlert();
      }
    }
    const { updateAvatar } = this.props;
    const options = {
      mediaType: 'image',
      quality: 0.7,
      maxWidth: 1024,
      maxHeight: 1024,
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.error) {
        Dialog.requestCameraPermissionFromSystemAlert();
      } else if (response.uri && !response.didCancel) {
        updateAvatar(response);
      }
    });
  };

  handleImageLibrary = () => {
    const { updateAvatar } = this.props;
    const options = {
      mediaType: 'image',
      quality: 0.7,
      maxWidth: 1024,
      maxHeight: 1024,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.error) {
        Dialog.requestCameraPermissionFromSystemAlert();
      } else if (response.uri && !response.didCancel) {
        updateAvatar(response);
      }
    });
  };

  onPressChangePhoto = () => {
    Alert.alert(t('setting_edit_profile_change_photo'), '', [
      {
        text: t('__cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('setting_edit_profile_select_from_album'),
        onPress: this.handleImageLibrary,
      },
      {
        text: t('setting_edit_profile_take_a_photo'),
        onPress: this.handleOpenCamera,
      },
    ]);
  };

  handleHeight = (value) => {
    if (parseInt(value, 10) > 302) {
      return '302';
    } else if (parseInt(value, 10) < 0) {
      return 0;
    } else {
      return value;
    }
  };

  handleWeight = (value) => {
    if (parseInt(value, 10) > 227) {
      return '227';
    } else if (parseInt(value, 10) < 0) {
      return 0;
    } else {
      return value;
    }
  };

  renderHeader = () => {
    const { setting } = this.props;
    return (
      <View style={styles.headerWrapper}>
        <BaseAvatar
          size={88}
          image={Images.defaultAvatar}
          uri={setting.avatar}
          time={Date.now()}
          active
        />
        <RoundButton
          style={[styles.btnStyle, styles.btnMargin]}
          textStyle={styles.textStyle}
          uppercase={false}
          text={t('setting_edit_profile_change_profile_photo')}
          onPress={this.onPressChangePhoto}
        />
      </View>
    );
  };

  onChangeValue = (key, value) => {
    const { updatePayload } = this.props;
    updatePayload({ [key]: value });
    if (isEmpty(value)) {
      this.setState({ isDisabled: true });
    } else {
      this.setState({ isDisabled: false });
    }
    this.setState({
      [key]: value,
    });
  };
  onDateChange = (setFieldValue) => (date, type) => {
    setFieldValue('birth', date);
    setFieldValue('age', d.moment().diff(d.moment(date), 'years'));
  };
  onTriggerDatePicker = (isOpen) => () => {
    this.setState({
      isShowDatePicker: isOpen,
    });
  };
  renderGeneral = ({ values, errors, handleChange, setFieldValue }) => {
    return (
      <View style={[styles.container, Classes.paddingLeft]}>
        <HorizontalTextInput
          title={t('setting_my_profile_name')}
          onChangeText={handleChange('fullName')}
          error={errors.fullName}
          value={values.fullName}
          containerStyle={styles.containerStyle}
          inputStyle={styles.textInputStyle}
        />

        <HorizontalTextInput
          getRef={(r) => {
            this._textInputRef = r;
          }}
          title={t('register_input_gender_title')}
          component={
            <RadioForm
              radio_props={[
                {
                  label: t('register_input_male'),
                  value: 0,
                },
                {
                  label: t('register_input_female'),
                  value: 1,
                },
              ]}
              buttonSize={10}
              buttonOuterSize={20}
              buttonColor={Colors.radioForm.primary.button}
              selectedButtonColor={Colors.radioForm.primary.selected}
              onPress={(v) => setFieldValue('gender', v)}
              initial={0}
              formHorizontal
              labelStyle={{ paddingRight: Metrics.baseMargin }}
            />
          }
          error={errors.gender}
          value={values.gender}
          paddingVertical={0}
        />
        <HorizontalTextInput
          title={t('register_input_birth_title')}
          onPress={this.onTriggerDatePicker(true)}
          error={errors.age || errors.birth}
          value={d.moment(values.birth).format('YYYY-MM-DD')}
          paddingVertical={0}
        />

        <HorizontalTextInput
          title={t('setting_my_profile_height')}
          onChangeText={handleChange('height')}
          error={errors.height}
          value={values.height}
          keyboardType="numeric"
          postfix={t('setting_my_profile_cm')}
          containerStyle={styles.containerStyle}
          postfixStyle={styles.postfixStyle}
          inputStyle={styles.textInputStyle}
        />
        <HorizontalTextInput
          title={t('setting_my_profile_weight')}
          onChangeText={handleChange('weight')}
          error={errors.weight}
          value={values.weight}
          keyboardType="numeric"
          postfix={t('setting_my_profile_kg')}
          containerStyle={styles.containerStyle}
          postfixStyle={styles.postfixStyle}
          inputStyle={styles.textInputStyle}
        />
      </View>
    );
  };

  onPressItem = (key) => () => {
    this.setState(
      (state) => ({
        list: state.list.map((e) => ({ ...e, selected: e.key === key })),
        selected: key,
      }),
      () => {
        const { updateSetting } = this.props;
        updateSetting({ privacy: key });
      },
    );
  };
  renderItem = ({ item }) => {
    return (
      <View style={styles.itemWrapper}>
        <TouchableOpacity style={styles.item} onPress={this.onPressItem(item.key)}>
          <Text style={styles.itemTitle}>{t(`${item.title}`)}</Text>
          <BouncyCheckbox
            isChecked={item.selected}
            fillColor={Colors.black}
            checkboxSize={22}
            text=""
            borderColor={Colors.gray_03}
            onPress={this.onPressItem(item.key)}
          />
        </TouchableOpacity>
      </View>
    );
  };
  renderPrivacy = () => {
    const { list } = this.state;
    return (
      <View>
        <Text style={styles.placeholder}>{t('1-2_setting_privacy_desc')}</Text>
        <FlatList
          data={list}
          keyExtractor={(item) => item.title}
          renderItem={this.renderItem}
        />
        <Text style={styles.placeholder}>{t('1-2_setting_privacy_notice')}</Text>
      </View>
    );
  };

  onPressNext = (values) => {
    const { updateSetting, payload } = this.props;
    let temp = JSON.parse(JSON.stringify(payload));

    if (temp !== {} && temp.height) {
      temp.height = parseInt(temp.height, 10);
    }
    if (temp !== {} && temp.weight) {
      temp.weight = parseInt(temp.weight, 10);
    }
    if (temp !== {} && temp.maxHeartRate) {
      temp.maxHeartRate = parseInt(temp.maxHeartRate, 10);
    }

    values.birth = d.moment(values.birth).format('YYYY-MM-DD');
    if (temp !== {}) {
      const data = { ...values, ...temp };
      console.log('=== data ===', data);
      updateSetting(data);

      requestAnimationFrame(Actions.AddProfileDetailScreen);
    }
  };

  render() {
    const { currentLocales } = this.props;
    const { isShowDatePicker } = this.state;
    return (
      <Formik
        initialValues={this.state.initialValues}
        validationSchema={validationSchema}
        onSubmit={this.onPressNext}
        validateOnChange={false}
      >
        {(formikProps) => (
          <View style={Classes.fill}>
            <SecondaryNavbar
              back
              style={{ backgroundColor: Colors.white }}
              navHeight={44}
              navTitle={t('1-2_add_profile')}
            />
            <KeyboardAvoidingView
              style={Classes.fill}
              contentContainerStyle={Classes.fill}
              behavior={Platform.OS === 'ios' ? 'position' : 'height'}
            >
              <ScrollView style={styles.container}>
                <DismissKeyboard>
                  <>
                    {this.renderHeader()}
                    <Text style={styles.headerTitle}>
                      {t('setting_my_profile_general')}
                    </Text>
                    {this.renderGeneral(formikProps)}
                    <Text style={styles.headerTitle}>{t('setting_privacy_title')}</Text>
                    {this.renderPrivacy()}
                  </>
                </DismissKeyboard>
              </ScrollView>
              <View style={[Classes.margin, Classes.mainEnd, Classes.marginBottom]}>
                <DismissKeyboard>
                  <SquareButton
                    text={t('1-2_next')}
                    disabledOpacity={0.5}
                    color={Colors.button.primary.content.background}
                    onPress={formikProps.handleSubmit}
                    radius={6}
                  />
                </DismissKeyboard>
              </View>
            </KeyboardAvoidingView>
            {isShowDatePicker && (
              <DateTimePickerModal
                onDateChange={this.onDateChange(formikProps.setFieldValue)}
                onCancelPress={this.onTriggerDatePicker(false)}
                maximumDate={new Date()}
                format="YYYY-MM-DD"
                date={formikProps.values.birth}
                locale={currentLocales[0] && currentLocales[0].languageCode}
              />
            )}
          </View>
        )}
      </Formik>
    );
  }
}

export default connect(
  (state, params) => ({
    setting: state.setting.setting,
    editSetting: state.setting.editSetting,
    routeName: params.routeName,
    payload: state.setting.payload,
    currentLocales: state.appState.currentLocales,
    userSetting: state.setting.setting.settings,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateSetting: SettingActions.updateSetting,
        updateEditSetting: SettingActions.updateEditSetting,
        updateAvatar: SettingActions.updateAvatar,
        updatePayload: SettingActions.updatePayload,
      },
      dispatch,
    ),
)(AddProfileScreen);
