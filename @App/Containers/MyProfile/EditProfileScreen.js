import * as yup from 'yup';

import {
  Alert,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  BaseAvatar,
  DateTimePickerModal,
  DismissKeyboard,
  HorizontalTextInput,
  RoundButton,
  SecondaryNavbar,
} from 'App/Components';
import { Classes, Colors, Images, Metrics } from 'App/Theme';
import { SearchActions, SettingActions } from 'App/Stores';

import { Actions } from 'react-native-router-flux';
import { Dialog, Permission } from 'App/Helpers';
import { Formik } from 'formik';
import * as ImagePicker from 'react-native-image-picker';
import PropTypes from 'prop-types';
import RadioForm from 'react-native-simple-radio-button';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Date as d } from 'App/Helpers';
import { isEmpty, get } from 'lodash';
import styles from './MyProfileScreenStyle';
import { translate as t } from 'App/Helpers/I18n';

const yString = yup.string();

const validationSchema = yup.object().shape({
  fullName: yString.required(t('required_field')),
  birth: yString.required(t('register_validation_birth_required')),
  gender: yup.number().required(t('required_field')),
  maxHeartRate: yup
    .number()
    .required(t('required_field'))
    .typeError(t('must_be_number'))
    .positive(t('must_be_positive'))
    .min(0, t('must_not_smaller_then', { min: 0 }))
    .max(255, t('must_not_greater_then', { max: 255 })),
  height: yup
    .number()
    .required(t('required_field'))
    .typeError(t('must_be_number'))
    .positive(t('must_be_positive'))
    .min(61, t('must_not_smaller_then', { min: 61 }))
    .max(302, t('must_not_greater_then', { max: 302 })),
  weight: yup
    .number()
    .required(t('required_field'))
    .typeError(t('must_be_number'))
    .positive(t('must_be_positive'))
    .min(30, t('must_not_smaller_then', { min: 30 }))
    .max(227, t('must_not_greater_then', { max: 227 })),
});

class EditProfileScreen extends React.Component {
  static propTypes = {
    equipmentList: PropTypes.array,
    setting: PropTypes.object,
    editSetting: PropTypes.object,
    updateSetting: PropTypes.func.isRequired,
    updateAvatar: PropTypes.func.isRequired,
    ability: PropTypes.number,
    goal: PropTypes.number,
    frequency: PropTypes.number,
    duration: PropTypes.number,
    equipments: PropTypes.array,
    injuries: PropTypes.array,
    activity: PropTypes.array,
    updatePayload: PropTypes.func.isRequired,
    updateEquipment: PropTypes.func.isRequired,
    payload: PropTypes.object,
    editEquipment: PropTypes.array,
    genres: PropTypes.array.isRequired,
    currentLocales: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    const { setting = { settings: {} } } = props;

    this.state = {
      payload: {},
      isDisabled: false,
      city: setting.settings.city,
      fullName: setting.fullName,
      height: JSON.stringify(setting.settings.height),
      weight: JSON.stringify(setting.settings.weight),
      maxHeartRate: JSON.stringify(setting.settings.maxHeartRate),
      birth: new Date(d.moment(setting.settings.birth)),
      gender: setting.settings.gender,
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@EditProfileScreen');
  }

  renderGender = (value) => {
    switch (value) {
      case 0:
        return t('register_input_male');
      case 1:
        return t('register_input_female');
      case 2:
        return 'Unspecified';
      default:
        return 'undefined';
    }
  };

  renderAbility = (value) => {
    switch (value) {
      case 0:
        return 'setting_ability_beginner';
      case 1:
        return 'setting_ability_intermediate';
      case 2:
        return 'setting_ability_advanced';
      case 3:
        return 'setting_ability_export';
      default:
        return 'undefined';
    }
  };

  renderGoal = (value) => {
    switch (value) {
      case 0:
        return 'setting_goal_build_muscle';
      case 1:
        return 'setting_goal_de_stress';
      case 2:
        return 'setting_goal_improve_definition';
      case 3:
        return 'setting_goal_improve_health';
      case 4:
        return 'setting_goal_increase_flexibility';
      case 5:
        return 'setting_goal_lose_weight';
      case 6:
        return 'setting_goal_tone_up';
      default:
        return 'setting_goal_build_muscle';
    }
  };

  renderInjuries = (value) => {
    switch (value) {
      case 0:
        return 'setting_injuries_ankle';
      case 1:
        return 'setting_injuries_back';
      case 2:
        return 'setting_injuries_knee';
      case 3:
        return 'setting_injuries_neck';
      case 4:
        return 'setting_injuries_postnatal';
      case 5:
        return 'setting_injuries_pernatal';
      case 6:
        return 'setting_injuries_shoulder';
      case 7:
        return 'setting_injuries_wrist';
      default:
        return 'undefined';
    }
  };

  showInjuries = (injuries) => {
    let temp = '';
    let count = 0;
    if (injuries && injuries.length > 0) {
      injuries.forEach((item, index) => {
        if (index < 4) {
          temp =
            temp +
            t(this.renderInjuries(item)) +
            (index < injuries.length - 1 ? ' / ' : '');
        } else {
          count += 1;
        }
      });
    }
    if (count === 0) {
      return temp;
    } else {
      return temp + '+' + count;
    }
  };

  renderActivity = (value) => {
    const { genres, currentLocales } = this.props;
    const target = genres.find((e) => e.id === value);

    const languageCode = currentLocales[0] && currentLocales[0].languageCode;
    return !target.translations || !languageCode
      ? t(`achievement_list_${target.name}`)
      : get(
          target.translations.find((e) => e.locale.indexOf(languageCode) !== -1),
          'title',
          target.translations[0]
            ? target.translations[0].title
            : t(`achievement_list_${target.name}`),
        );
  };

  showActivity = (activities) => {
    let temp = '';
    let count = 0;
    if (activities && activities.length > 0) {
      activities.forEach((item, index) => {
        if (index < 3) {
          temp =
            temp +
            this.renderActivity(item) +
            (index < activities.length - 1 ? ' / ' : '');
        } else {
          count += 1;
        }
      });
    }
    if (count === 0) {
      return temp;
    }
    return temp + '+' + count;
  };

  renderEquipments = (value) => {
    switch (value) {
      case 'chair':
        return 'setting_equipment_chair';
      case 'dumbbell':
        return 'setting_equipment_dumbbells';
      case 'foam-roller':
        return 'setting_equipment_foam_roller';
      case 'jump-rope':
        return 'setting_equipment_jump_rope';
      case 'kettlebells':
        return 'setting_equipment_kettlebell';
      case 'medicine-ball':
        return 'setting_equipment_medicine_bell';
      case 'yoga-block':
        return 'setting_equipment_yoga_blocks';
      case 'resistance-bands':
        return 'setting_equipment_resistance_bands';
      case 'mini-bands':
        return 'setting_equipment_mini_bands';
      default:
        return 'undefined';
    }
  };

  showEquipment = (value) => {
    let temp = '';
    let count = 0;
    const { equipmentList } = this.props;

    if (value && value.length > 0) {
      value.forEach((item, index) => {
        if (index < 2) {
          temp =
            temp +
            get(equipmentList.find((select) => select.id === item.id), 'title') +
            (index < value.length - 1 ? ' / ' : '');
        } else {
          count += 1;
        }
      });
    }
    if (count === 0) {
      return temp;
    } else {
      return temp + '+' + count;
    }
  };

  handleOpenCamera = async () => {
    if (Platform.OS === 'android') {
      const hasPermission = await Permission.checkAndRequestPermission(
        Permission.permissionType.CAMERA,
      );
      if (!hasPermission) {
        Dialog.requestCameraPermissionFromSystemAlert();
      }
    }
    const { updateAvatar } = this.props;
    const options = {
      mediaType: 'image',
      quality: 0.7,
      maxWidth: 1024,
      maxHeight: 1024,
    };
    ImagePicker.launchCamera(options, (response) => {
      console.log('response=>', response);
      if (response.error) {
        Dialog.requestCameraPermissionFromSystemAlert();
      } else if (response.uri && !response.didCancel) {
        updateAvatar(response);
      }
    });
  };

  handleImageLibrary = () => {
    const { updateAvatar } = this.props;
    const options = {
      includeBase64: false,
      mediaType: 'image',
      quality: 0.7,
      maxWidth: 1024,
      maxHeight: 1024,
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('response=>', response);
      if (response.error) {
        Alert.alert(t('alert_title_oops'), JSON.stringify(response.error));
      } else if (response.uri && !response.didCancel) {
        updateAvatar(response);
      }
    });
  };

  onPressChangePhoto = () => {
    Alert.alert(t('setting_edit_profile_change_photo'), '', [
      {
        text: t('__cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('setting_edit_profile_select_from_album'),
        onPress: this.handleImageLibrary,
      },
      {
        text: t('setting_edit_profile_take_a_photo'),
        onPress: this.handleOpenCamera,
      },
    ]);
  };

  handleHeight = (value) => {
    if (parseInt(value, 10) > 302) {
      return '302';
    } else if (parseInt(value, 10) < 0) {
      return 0;
    } else {
      return value;
    }
  };

  handleWeight = (value) => {
    if (parseInt(value, 10) > 227) {
      return '227';
    } else if (parseInt(value, 10) < 0) {
      return 0;
    } else {
      return value;
    }
  };

  renderHeader = () => {
    const { setting } = this.props;
    return (
      <View style={styles.headerWrapper}>
        <BaseAvatar
          size={88}
          image={Images.defaultAvatar}
          uri={setting.signedAvatarUrl}
          active
        />
        <RoundButton
          style={[styles.btnStyle, styles.btnMargin]}
          textStyle={styles.textStyle}
          uppercase={false}
          text={t('setting_edit_profile_change_profile_photo')}
          onPress={this.onPressChangePhoto}
        />
      </View>
    );
  };

  onChangeValue = (key, value) => {
    const { updatePayload } = this.props;
    updatePayload({ [key]: value });
    if (isEmpty(value)) {
      this.setState({ isDisabled: true });
    } else {
      this.setState({ isDisabled: false });
    }
    this.setState({
      [key]: value,
    });
  };
  onDateChange = (setFieldValue) => (date, type) => {
    setFieldValue('birth', date);
    setFieldValue('age', d.moment().diff(d.moment(date), 'years'));
  };
  onTriggerDatePicker = (isOpen) => () => {
    this.setState({
      isShowDatePicker: isOpen,
    });
  };
  renderGeneral = ({ values, errors, handleChange, setFieldValue }) => {
    const { setting } = this.props;
    return (
      <View style={[styles.container, Classes.paddingLeft]}>
        <HorizontalTextInput
          title={t('setting_my_profile_email')}
          value={setting.email}
          editable={false}
          containerStyle={styles.containerStyle}
          inputStyle={styles.textInputStyle}
        />
        <HorizontalTextInput
          title={t('setting_my_profile_birth')}
          value={d.moment(values.birth).format('YYYY-MM-DD')}
          editable={true}
          onPress={this.onTriggerDatePicker(true)}
          containerStyle={styles.containerStyle}
          inputStyle={styles.textInputStyle}
        />

        <HorizontalTextInput
          getRef={(r) => {
            this._textInputRef = r;
          }}
          title={t('register_input_gender_title')}
          component={
            <RadioForm
              radio_props={[
                {
                  label: t('register_input_male'),
                  value: 0,
                },
                {
                  label: t('register_input_female'),
                  value: 1,
                },
              ]}
              buttonSize={10}
              buttonOuterSize={20}
              buttonColor={Colors.radioForm.primary.button}
              selectedButtonColor={Colors.radioForm.primary.selected}
              onPress={(v) => setFieldValue('gender', v)}
              initial={setting.settings.gender}
              formHorizontal
              labelStyle={{ paddingRight: Metrics.baseMargin }}
            />
          }
          error={errors.gender}
          value={values.gender}
          paddingVertical={0}
        />
        <HorizontalTextInput
          title={t('setting_my_profile_name')}
          onChangeText={handleChange('fullName')}
          error={errors.fullName}
          value={values.fullName}
          containerStyle={styles.containerStyle}
          inputStyle={styles.textInputStyle}
        />
        <HorizontalTextInput
          title={t('setting_my_profile_height')}
          onChangeText={handleChange('height')}
          error={errors.height}
          value={values.height}
          keyboardType="numeric"
          postfix={t('setting_my_profile_cm')}
          containerStyle={styles.containerStyle}
          postfixStyle={styles.postfixStyle}
          inputStyle={styles.textInputStyle}
        />
        <HorizontalTextInput
          title={t('setting_my_profile_weight')}
          onChangeText={handleChange('weight')}
          error={errors.weight}
          value={values.weight}
          keyboardType="numeric"
          postfix={t('setting_my_profile_kg')}
          containerStyle={styles.containerStyle}
          postfixStyle={styles.postfixStyle}
          inputStyle={styles.textInputStyle}
        />
        <HorizontalTextInput
          title={t('setting_my_profile_city')}
          onChangeText={handleChange('city')}
          error={errors.city}
          value={values.city}
          containerStyle={styles.containerStyle}
          inputStyle={styles.textInputStyle}
        />
      </View>
    );
  };

  renderBody = ({ values, errors, handleChange }) => {
    const { ability, goal, injuries } = this.props;
    return (
      <View style={styles.container}>
        <View style={Classes.paddingLeft}>
          <HorizontalTextInput
            title={t('setting_my_profile_max_heart_rate')}
            onChangeText={handleChange('maxHeartRate')}
            error={errors.maxHeartRate}
            value={values.maxHeartRate}
            keyboardType="numeric"
            postfix={t('setting_my_profile_bpm')}
            containerStyle={styles.containerStyle}
            postfixStyle={styles.postfixStyle}
            inputStyle={styles.textInputStyle}
          />
        </View>

        <View style={Classes.paddingRight}>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_ability')}</Text>
            <TouchableOpacity style={styles.full} onPress={Actions.AbilityScreen}>
              <View style={styles.textBoxStyle}>
                <Text style={styles.rightEditText}>{t(this.renderAbility(ability))}</Text>
                <Image source={Images.next} style={styles.Editimg} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_goal')}</Text>
            <TouchableOpacity style={styles.full} onPress={Actions.GoalScreen}>
              <View style={styles.textBoxStyle}>
                <Text style={styles.rightEditText}>{t(this.renderGoal(goal))}</Text>
                <Image source={Images.next} style={styles.Editimg} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_injuries')}</Text>
            <TouchableOpacity style={styles.full} onPress={Actions.InjuriesScreen}>
              <View style={styles.textBoxStyle}>
                <Text style={styles.rightEditText}>{this.showInjuries(injuries)}</Text>
                <Image source={Images.next} style={styles.Editimg} />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  renderWorkout = () => {
    const { activity, frequency, duration, equipments } = this.props;
    return (
      <View style={[styles.container, styles.lastBox, Classes.paddingRight]}>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_workout_type')}</Text>
          <TouchableOpacity style={styles.full} onPress={Actions.ActivityScreen}>
            <View style={styles.textBoxStyle}>
              <Text style={styles.rightEditText}>{this.showActivity(activity)}</Text>
              <Image source={Images.next} style={styles.Editimg} />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_schedule')}</Text>
          <TouchableOpacity style={styles.full} onPress={Actions.ScheduleScreen}>
            <View style={styles.textBoxStyle}>
              <Text style={styles.rightEditText}>
                {t('exercise_cycle', {
                  frequency,
                  duration,
                })}
              </Text>
              <Image source={Images.next} style={styles.Editimg} />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_equipment')}</Text>
          <TouchableOpacity style={styles.full} onPress={Actions.EquipmentScreen}>
            <View style={styles.textBoxStyle}>
              <Text style={styles.rightEditText}>{this.showEquipment(equipments)}</Text>
              <Image source={Images.next} style={styles.Editimg} />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  onPressSave = (values) => {
    const { updateSetting, payload, editEquipment, updateEquipment } = this.props;
    let temp = JSON.parse(JSON.stringify(payload));
    values.birth = d.moment(values.birth).format('YYYY-MM-DD');

    if (temp !== {} && temp.height) {
      temp.height = parseInt(temp.height, 10);
    }
    if (temp !== {} && temp.weight) {
      temp.weight = parseInt(temp.weight, 10);
    }
    if (temp !== {} && temp.maxHeartRate) {
      temp.maxHeartRate = parseInt(temp.maxHeartRate, 10);
    }
    if (temp !== {}) {
      console.log('=== onPressSave ===');
      const data = { ...values, ...temp };
      updateSetting(data);
      Actions.pop();
    }
    if (!isEmpty(editEquipment)) {
      updateEquipment(editEquipment);
    }
  };

  render() {
    const { isShowDatePicker, isDisabled } = this.state;
    const { currentLocales } = this.props;
    return (
      <Formik
        initialValues={this.state}
        validationSchema={validationSchema}
        onSubmit={this.onPressSave}
        validateOnChange={false}
      >
        {(formikProps) => (
          <View style={Classes.fill}>
            <SecondaryNavbar
              back
              backTo="MyProfileScreen"
              style={{ backgroundColor: Colors.white }}
              navHeight={44}
              navTitle={t('setting_edit_profile')}
              backConfirm={formikProps.dirty}
              navRightComponent={
                <TouchableOpacity
                  disabled={isDisabled || formikProps.isSubmitting}
                  onPress={formikProps.handleSubmit}
                >
                  <Text style={[styles.navText, isDisabled && styles.disabledText]}>
                    {t('setting_edit_profile_save')}
                  </Text>
                </TouchableOpacity>
              }
            />
            <KeyboardAvoidingView
              style={Classes.fill}
              contentContainerStyle={Classes.fill}
              behavior={Platform.OS === 'ios' ? 'position' : 'height'}
            >
              <ScrollView style={styles.container}>
                <DismissKeyboard>
                  <>
                    {this.renderHeader()}
                    <Text style={styles.headerTitle}>
                      {t('setting_my_profile_general')}
                    </Text>
                    {this.renderGeneral(formikProps)}
                    <Text style={styles.headerTitle}>{t('setting_my_profile_body')}</Text>
                    {this.renderBody(formikProps)}
                    <Text style={styles.headerTitle}>
                      {t('setting_my_profile_workout')}
                    </Text>
                    {this.renderWorkout()}
                  </>
                </DismissKeyboard>
              </ScrollView>
            </KeyboardAvoidingView>
            {isShowDatePicker && (
              <DateTimePickerModal
                onDateChange={this.onDateChange(formikProps.setFieldValue)}
                onCancelPress={this.onTriggerDatePicker(false)}
                maximumDate={new Date()}
                format="YYYY-MM-DD"
                date={formikProps.values.birth}
                locale={currentLocales[0] && currentLocales[0].languageCode}
              />
            )}
          </View>
        )}
      </Formik>
    );
  }
}

export default connect(
  (state, params) => ({
    equipmentList: state.search.equipmentList,
    genres: state.setting.genres,
    setting: state.setting.setting,
    editSetting: state.setting.editSetting,
    ability: state.setting.editSetting.settings.ability,
    goal: state.setting.editSetting.settings.goal,
    injuries: state.setting.editSetting.settings.injuries,
    activity: state.setting.editSetting.settings.activity,
    frequency: state.setting.editSetting.settings.schedule.frequency,
    duration: state.setting.editSetting.settings.schedule.duration,
    equipments: state.setting.editSetting.equipments,
    routeName: params.routeName,
    payload: state.setting.payload,
    editEquipment: state.setting.editEquipment,
    currentLocales: state.appState.currentLocales,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateSetting: SettingActions.updateSetting,
        updateEditSetting: SettingActions.updateEditSetting,
        getEquipmentList: SearchActions.getEquipmentList,
        updateEquipment: SettingActions.updateEquipment,
        updateAvatar: SettingActions.updateAvatar,
        updatePayload: SettingActions.updatePayload,
      },
      dispatch,
    ),
)(EditProfileScreen);
