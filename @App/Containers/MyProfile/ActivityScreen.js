import {
  BaseNavDoneButton,
  BouncyCheckbox,
  SecondaryNavbar,
} from 'App/Components';
import { Classes, Colors, Images } from 'App/Theme';
import {
  FlatList,
  Image,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import React from 'react';
import SettingActions from 'App/Stores/Setting/Actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './MyProfileScreenStyle';
import { translate as t } from 'App/Helpers/I18n';
import { get } from 'lodash';

class ActivityScreen extends React.Component {
  static propTypes = {
    genres: PropTypes.array.isRequired,
    editSetting: PropTypes.object,
    updateEditSetting: PropTypes.func.isRequired,
    updatePayload: PropTypes.func.isRequired,
    workoutTypeList: PropTypes.array,
    currentLocale: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: props.editSetting.settings.activity,
      changed: false,
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@ActivityScreen');
  }

  onPressItem = (key) => {
    const { selected } = this.state;
    let temp = JSON.parse(JSON.stringify(selected));
    if (temp.length > 0 && temp.filter((item) => item === key).length > 0) {
      const index = temp.indexOf(key);
      temp.splice(index, 1);
    } else {
      temp.push(key);
    }
    this.setState({
      selected: temp,
      changed: true,
    });
  };

  renderItem = ({ item }) => {
    const { currentLocale: { languageCode } = {} } = this.props;
    const { selected } = this.state;

    const rollbackI18nName = t(`achievement_list_${item.name}`);
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: item.deviceCoverImage }} style={styles.activityImg} />
        <View style={[styles.itemWrapper, styles.activityItemWrapper]}>
          <TouchableOpacity style={styles.item} onPress={() => this.onPressItem(item.id)}>
            <Text style={[styles.itemTitle, styles.activityItemTitle]}>
              {!item.translations
                ? rollbackI18nName
                : get(
                    item.translations.find((e) => e.locale.indexOf(languageCode) !== -1),
                    'title',
                    item.translations[0] ? item.translations[0].title : rollbackI18nName,
                  )}
            </Text>
            <BouncyCheckbox
              isChecked={
                selected.filter((selectItem) => selectItem === item.id).length > 0
              }
              fillColor={Colors.black}
              checkboxSize={22}
              text=""
              borderColor={Colors.gray_03}
              onPress={() => this.onPressItem(item.id)}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  onPressSave = () => {
    const { editSetting, updateEditSetting, updatePayload } = this.props;
    const { selected } = this.state;
    let payload = editSetting;
    payload.settings.activity = selected;
    updateEditSetting(payload);
    updatePayload({ activity: selected });
    Actions.pop();
  };

  render() {
    const { workoutTypeList } = this.props;
    const { changed } = this.state;

    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          navHeight={44}
          back
          backConfirm={changed}
          navTitle={t('setting_activity_navtitle')}
          navRightComponent={
            <BaseNavDoneButton
              disabled={this.state.selected.length === 0}
              onPress={this.onPressSave}
            />
          }
        />
        <Text style={styles.headerTitle}>
          {t('setting_activity_your_favorite_activity')}
        </Text>
        <Text style={styles.placeholder}>{t('setting_placeholder_select_more')}</Text>
        <FlatList
          data={workoutTypeList}
          keyExtractor={(item) => `key-${item.id}`}
          renderItem={this.renderItem}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    editSetting: state.setting.editSetting,
    genres: state.setting.genres,
    workoutTypeList: state.search.workoutTypeList,
    currentLocale: state.appState.currentLocales[0],
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateEditSetting: SettingActions.updateEditSetting,
        updatePayload: SettingActions.updatePayload,
      },
      dispatch,
    ),
)(ActivityScreen);
