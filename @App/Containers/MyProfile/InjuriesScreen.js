import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, FlatList, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { Classes, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BouncyCheckbox, BaseNavDoneButton } from 'App/Components';
import SettingActions from 'App/Stores/Setting/Actions';

import styles from './MyProfileScreenStyle';

class InjuriesScreen extends React.Component {
  static propTypes = {
    editSetting: PropTypes.object,
    updateEditSetting: PropTypes.func.isRequired,
    updatePayload: PropTypes.func.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@InjuriesScreen');
    const { editSetting } = this.props;
    this.setState({ selected: editSetting.settings.injuries });
  }

  state = {
    list: [
      {
        title: 'setting_injuries_ankle',
        key: 0,
        selected: false,
      },
      {
        title: 'setting_injuries_back',
        key: 1,
        selected: false,
      },
      {
        title: 'setting_injuries_knee',
        key: 2,
        selected: false,
      },
      {
        title: 'setting_injuries_neck',
        key: 3,
        selected: false,
      },
      {
        title: 'setting_injuries_postnatal',
        key: 4,
        selected: false,
      },
      {
        title: 'setting_injuries_pernatal',
        key: 5,
        selected: false,
      },
      {
        title: 'setting_injuries_shoulder',
        selected: false,
        key: 6,
      },
      {
        title: 'setting_injuries_wrist',
        key: 7,
        selected: false,
      },
    ],
    selected: [],
    changed: false
  };

  onPressItem = (key) => {
    const { selected } = this.state;
    let temp = JSON.parse(JSON.stringify(selected));
    if (temp.length > 0 && temp.filter((item) => item === key).length > 0) {
      const index = temp.indexOf(key);
      temp.splice(index, 1);
    } else {
      temp.push(key);
    }
    this.setState({
      selected: temp,
      changed: true
    });
  };

  renderItem = ({ item }) => {
    const { selected } = this.state;
    return (
      <View style={styles.itemWrapper}>
        <TouchableOpacity style={styles.item} onPress={() => this.onPressItem(item.key)}>
          <Text style={styles.itemTitle}>{t(`${item.title}`)}</Text>
          <BouncyCheckbox
            isChecked={
              selected.filter((selectItem) => selectItem === item.key).length > 0
            }
            fillColor={Colors.black}
            checkboxSize={22}
            text=""
            borderColor={Colors.gray_03}
            onPress={() => this.onPressItem(item.key)}
          />
        </TouchableOpacity>
      </View>
    );
  };

  onPressSave = () => {
    const { editSetting, updateEditSetting, updatePayload } = this.props;
    const { selected } = this.state;
    let payload = editSetting;
    payload.settings.injuries = selected;
    updateEditSetting(payload);
    updatePayload({ injuries: selected });
    Actions.pop();
  };

  render() {
    const { list, changed } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          navHeight={44}
          back
          backConfirm={changed}
          navTitle={t('setting_injuries_nav_title')}
          navRightComponent={
            <BaseNavDoneButton
              onPress={this.onPressSave}
            />
          }
        />
        <Text style={styles.headerTitle}>
          {t('setting_injuries_your_body_limitation')}
        </Text>
        <Text style={styles.placeholder}>{t('setting_placeholder_select_more')}</Text>
        <FlatList
          data={list}
          keyExtractor={(item) => item.title}
          renderItem={this.renderItem}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    editSetting: state.setting.editSetting,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateEditSetting: SettingActions.updateEditSetting,
        updatePayload: SettingActions.updatePayload,
      },
      dispatch,
    ),
)(InjuriesScreen);
