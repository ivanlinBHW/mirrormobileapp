import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, FlatList, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { Classes, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BouncyCheckbox, BaseNavDoneButton } from 'App/Components';
import SettingActions from 'App/Stores/Setting/Actions';

import styles from './MyProfileScreenStyle';

class AblilityScreen extends React.Component {
  static propTypes = {
    editSetting: PropTypes.object,
    updateEditSetting: PropTypes.func.isRequired,
    updatePayload: PropTypes.func.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@AblilityScreen');
    const { editSetting } = this.props;
    const { list } = this.state;
    let temp = JSON.parse(JSON.stringify(list));
    temp.forEach((item, index) => {
      if (index === editSetting.settings.ability) {
        temp[index].selected = true;
      }
    });
    this.setState({ list: temp, select: editSetting.settings.ability });
  }

  state = {
    list: [
      {
        title: 'setting_ability_beginner',
        key: 0,
        selected: false,
      },
      {
        title: 'setting_ability_intermediate',
        key: 1,
        selected: false,
      },
      {
        title: 'setting_ability_advanced',
        key: 2,
        selected: false,
      },
      {
        title: 'setting_ability_export',
        key: 3,
        selected: false,
      },
    ],
    select: 0,
    changed: false
  };

  onPressItem = (title, key) => {
    const { list } = this.state;
    let temp = JSON.parse(JSON.stringify(list));
    temp.forEach((item) => {
      if (item.title === title) {
        item.selected = true;
      } else {
        item.selected = false;
      }
    });
    this.setState({
      list: temp,
      select: key,
      changed: true
    });
  };

  renderItem = ({ item }) => {
    return (
      <View style={styles.itemWrapper}>
        <TouchableOpacity
          style={styles.item}
          onPress={() => this.onPressItem(item.title, item.key)}
        >
          <Text style={styles.itemTitle}>{t(`${item.title}`)}</Text>
          <BouncyCheckbox
            isChecked={item.selected}
            fillColor={Colors.black}
            checkboxSize={22}
            text=""
            borderColor={Colors.gray_03}
            onPress={() => this.onPressItem(item.title, item.key)}
          />
        </TouchableOpacity>
      </View>
    );
  };

  onPressSave = () => {
    const { editSetting, updateEditSetting, updatePayload } = this.props;
    const { select } = this.state;
    let payload = editSetting;
    payload.settings.ability = select;
    updateEditSetting(payload);
    updatePayload({ ability: select });
    Actions.pop();
  };

  render() {
    const { list, changed } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          navHeight={44}
          back
          backConfirm={changed}
          navTitle={t('setting_my_profile_ability')}
          navRightComponent={<BaseNavDoneButton onPress={this.onPressSave} />}
        />
        <Text style={styles.headerTitle}>{t('setting_ability_your_fitness_level')}</Text>
        <Text style={styles.placeholder}>{t('setting_placeholder_select_one')}</Text>
        <FlatList data={list} renderItem={this.renderItem} />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    editSetting: state.setting.editSetting,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateEditSetting: SettingActions.updateEditSetting,
        updatePayload: SettingActions.updatePayload,
      },
      dispatch,
    ),
)(AblilityScreen);
