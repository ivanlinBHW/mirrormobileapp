import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, Text, Image } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { Classes, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseNavDoneButton } from 'App/Components';
import SettingActions from 'App/Stores/Setting/Actions';

import styles from './MyProfileScreenStyle';

class ScheduleScreen extends React.Component {
  static propTypes = {
    editSetting: PropTypes.object,
    updateEditSetting: PropTypes.func.isRequired,
    updatePayload: PropTypes.func.isRequired,
  };

  state = {
    frequency: 1,
    duration: 15,
    frequencyList: [
      { label: '1', value: 1 },
      { label: '2', value: 2 },
      { label: '3', value: 3 },
      { label: '4', value: 4 },
      { label: '5', value: 5 },
      { label: '6', value: 6 },
      { label: '7', value: 7 },
    ],
    durationList: [
      { label: '5', value: 5 },
      { label: '10', value: 10 },
      { label: '15', value: 15 },
      { label: '20', value: 20 },
      { label: '25', value: 25 },
      { label: '30', value: 30 },
      { label: '35', value: 35 },
      { label: '40', value: 40 },
      { label: '45', value: 45 },
      { label: '50', value: 50 },
      { label: '55', value: 55 },
      { label: '60', value: 60 },
    ],
    changed: false,
    count: 0
  };

  componentDidMount() {
    __DEV__ && console.log('@ScheduleScreen');
    const { editSetting } = this.props;
    this.setState({
      frequency: editSetting.settings.schedule.frequency,
      duration: editSetting.settings.schedule.duration,
    });
  }

  onPressSave = () => {
    const { editSetting, updateEditSetting, updatePayload } = this.props;
    const { frequency, duration } = this.state;
    let payload = editSetting;
    payload.settings.schedule.frequency = frequency;
    payload.settings.schedule.duration = duration;
    updateEditSetting(payload);
    updatePayload({ schedule: { frequency, duration } });
    Actions.pop();
  };

  render() {
    const { frequency, duration, frequencyList, durationList, changed } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          navHeight={44}
          back
          backConfirm={changed}
          navTitle={t('setting_schedule_nav_title')}
          navRightComponent={<BaseNavDoneButton onPress={this.onPressSave} />}
        />
        <Text style={styles.headerTitle}>
          {t('setting_schedule_your_weekly_schedule')}
        </Text>
        <Text style={styles.scheduleTitle}>{t('setting_schedule_frequancy')}</Text>
        <RNPickerSelect
          onValueChange={(value) => {
            
            let changed = this.state.frequency != value;
            value && this.setState({ frequency: value, changed })
          }}
          placeholder={{
            label: t('setting_schedule_frequancy_placeholder'),
            value: null,
            disabled: true,
          }}
          useNativeAndroidPickerStyle={false}
          items={frequencyList}
          value={frequency}
          style={{
            inputIOS: styles.picker,
            inputAndroid: styles.picker,
            iconContainer: styles.iconContainer,
          }}
          Icon={() => <Image source={Images.down_black} style={styles.downImg} />}
        />
        <Text style={styles.scheduleTitle}>{t('setting_schedule_duration')}</Text>
        <RNPickerSelect
          onValueChange={(value) => {

            let changed = this.state.duration != value;
            value && this.setState({ duration: value, changed })
          }}          
          placeholder={{
            label: t('setting_schedule_duration_placeholder'),
            value: null,
            disabled: true,
          }}
          useNativeAndroidPickerStyle={false}
          items={durationList}
          value={duration}
          style={{
            inputIOS: styles.picker,
            inputAndroid: styles.picker,
            iconContainer: styles.iconContainer,
          }}
          Icon={() => <Image source={Images.down_black} style={styles.downImg} />}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    editSetting: state.setting.editSetting,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateEditSetting: SettingActions.updateEditSetting,
        updatePayload: SettingActions.updatePayload,
      },
      dispatch,
    ),
)(ScheduleScreen);
