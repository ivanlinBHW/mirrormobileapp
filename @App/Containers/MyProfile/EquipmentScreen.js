import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  View,
  SafeAreaView,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { Classes, Colors, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BouncyCheckbox, BaseNavDoneButton } from 'App/Components';
import SettingActions from 'App/Stores/Setting/Actions';
import SearchActions from 'App/Stores/Search/Actions';

import styles from './MyProfileScreenStyle';

class EquipmentScreen extends React.Component {
  static propTypes = {
    updateEquipment: PropTypes.func.isRequired,
    getEquipmentList: PropTypes.func.isRequired,
    updateEditEquipment: PropTypes.func.isRequired,
    editEquipment: PropTypes.array,
    equipmentList: PropTypes.array,
    editSetting: PropTypes.object,
    updateEditSetting: PropTypes.func.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@EquipmentScreen');
    const { editSetting, getEquipmentList } = this.props;
    getEquipmentList();
    let temp = [];
    editSetting.equipments.forEach((item) => {
      temp.push(item.name);
    });
    this.setState({ selected: temp });
  }

  state = {
    list: [],
    equipmentList: [],
    selected: [],
    changed: false
  };

  componentWillReceiveProps(nextProps) {
    if (this.state.equipmentList !== nextProps.equipmentList) {
      this.setState({
        equipmentList: nextProps.equipmentList,
      });
      const tempList = [];
      nextProps.equipmentList.map((item) => {
        tempList.push({
          img: item.signedDeviceCoverImage,
          title: item.title,
          key: item.name,
        });
      });
      this.setState({
        list: tempList,
      });
    }
  }

  onPressItem = (key) => {
    const { selected } = this.state;
    let temp = JSON.parse(JSON.stringify(selected));
    if (temp.length > 0 && temp.filter((item) => item === key).length > 0) {
      const index = temp.indexOf(key);
      temp.splice(index, 1);
    } else {
      temp.push(key);
    }
    this.setState({
      selected: temp,
      changed: true
    });
  };

  renderItem = ({ item }) => {
    const { selected } = this.state;
    return (
      <View style={styles.itemContainer}>
        <Image
          source={{
            uri: typeof item.img === 'string' ? item.img : '',
          }}
          style={styles.activityImg}
        />
        <View style={[styles.itemWrapper, styles.activityItemWrapper]}>
          <TouchableOpacity
            style={styles.item}
            onPress={() => this.onPressItem(item.key)}
          >
            <Text style={[styles.itemTitle, styles.activityItemTitle]}>{item.title}</Text>
            <BouncyCheckbox
              isChecked={
                selected &&
                selected.filter((selectItem) => selectItem === item.key).length > 0
              }
              fillColor={Colors.black}
              checkboxSize={22}
              text=""
              borderColor={Colors.gray_03}
              onPress={() => this.onPressItem(item.key)}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  onPressSave = () => {
    const {
      updateEditEquipment,
      equipmentList,
      updateEditSetting,
      editSetting,
    } = this.props;
    const { selected } = this.state;
    let temp = [];
    let tempItem = [];
    let payload = editSetting;
    if (selected && selected.length > 0) {
      selected.forEach((select) => {
        let filterItem = [];
        filterItem = equipmentList.filter((item) => item.name === select);
        if (filterItem && filterItem.length > 0) {
          temp.push(filterItem[0].id);
          tempItem.push(filterItem[0]);
        }
      });
    }
    payload.equipments = tempItem;
    updateEditSetting(payload);
    updateEditEquipment({ equipments: temp });
    Actions.pop();
  };

  render() {
    const { list, changed } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          navHeight={44}
          back
          backConfirm={changed}
          navTitle={t('setting_equipment_nav_title')}
          navRightComponent={
            <BaseNavDoneButton
              onPress={this.onPressSave}
            />
          }
        />
        <FlatList
          data={list}
          ListHeaderComponent={
            <View>
              <Text style={styles.headerTitle}>
                {t('setting_equipment_your_equipment')}
              </Text>
              <Text style={styles.placeholder}>
                {t('setting_placeholder_select_more')}
              </Text>
            </View>
          }
          ListFooterComponent={<View style={styles.emptyBox} />}
          keyExtractor={(item) => item.title}
          renderItem={this.renderItem}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    equipmentList: state.search.equipmentList,
    editSetting: state.setting.editSetting,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateEquipment: SettingActions.updateEquipment,
        getEquipmentList: SearchActions.getEquipmentList,
        updateEditSetting: SettingActions.updateEditSetting,
        updateEditEquipment: SettingActions.updateEditEquipment,
      },
      dispatch,
    ),
)(EquipmentScreen);
