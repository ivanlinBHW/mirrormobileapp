import { BaseAvatar, FormHeader, RoundButton, SecondaryNavbar } from 'App/Components';
import { Classes, Images } from 'App/Theme';
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import { Input } from 'react-native-elements';
import PropTypes from 'prop-types';
import React from 'react';
import SearchActions from 'App/Stores/Search/Actions';
import SettingActions from 'App/Stores/Setting/Actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './MyProfileScreenStyle';
import { translate as t } from 'App/Helpers/I18n';
import { get } from 'lodash';

class MyProfileScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object,
    setting: PropTypes.object,
    equipmentList: PropTypes.array,
    genres: PropTypes.array.isRequired,
    getEquipmentList: PropTypes.func.isRequired,
    resetEditSetting: PropTypes.func.isRequired,
    getWorkoutTypeList: PropTypes.func.isRequired,

    currentLocales: PropTypes.array.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@MyProfileScreen');
    const { getEquipmentList, getWorkoutTypeList } = this.props;
    getEquipmentList();
    getWorkoutTypeList();
  }

  state = {
    showAll: false,
  };

  onPressEditProfile = () => {
    const { resetEditSetting } = this.props;
    resetEditSetting();
    Actions.EditProfileScreen();
  };

  renderHeader = () => {
    const { setting } = this.props;
    return (
      <View style={styles.headerWrapper}>
        <BaseAvatar
          size={88}
          image={Images.defaultAvatar}
          uri={setting.signedAvatarUrl}
          active
        />
        <Text style={styles.rightText}>{setting.fullName}</Text>
        <RoundButton
          style={styles.btnStyle}
          textStyle={styles.textStyle}
          uppercase={false}
          text={t('edit')}
          onPress={() => this.onPressEditProfile()}
        />
      </View>
    );
  };

  renderGender = (value) => {
    switch (value) {
      case 0:
        return t('register_input_male');
      case 1:
        return t('register_input_female');
      case 2:
        return 'Unspecified';
      default:
        return 'undefined';
    }
  };

  renderAbility = (value) => {
    switch (value) {
      case 0:
        return 'setting_ability_beginner';
      case 1:
        return 'setting_ability_intermediate';
      case 2:
        return 'setting_ability_advanced';
      case 3:
        return 'setting_ability_export';
      default:
        return 'undefined';
    }
  };

  renderGoal = (value) => {
    switch (value) {
      case 0:
        return 'setting_goal_build_muscle';
      case 1:
        return 'setting_goal_de_stress';
      case 2:
        return 'setting_goal_improve_definition';
      case 3:
        return 'setting_goal_improve_health';
      case 4:
        return 'setting_goal_increase_flexibility';
      case 5:
        return 'setting_goal_lose_weight';
      case 6:
        return 'setting_goal_tone_up';
      default:
        return 'undefined';
    }
  };

  renderInjuries = (value) => {
    switch (value) {
      case 0:
        return 'setting_injuries_ankle';
      case 1:
        return 'setting_injuries_back';
      case 2:
        return 'setting_injuries_knee';
      case 3:
        return 'setting_injuries_neck';
      case 4:
        return 'setting_injuries_postnatal';
      case 5:
        return 'setting_injuries_pernatal';
      case 6:
        return 'setting_injuries_shoulder';
      case 7:
        return 'setting_injuries_wrist';
      default:
        return 'undefined';
    }
  };

  showInjuries = (injuries) => {
    let temp = '';
    let count = 0;
    if (injuries && injuries.length > 0) {
      injuries.forEach((item, index) => {
        if (index < 4) {
          temp =
            temp +
            t(this.renderInjuries(item)) +
            (index < injuries.length - 1 ? ' / ' : '');
        } else {
          count += 1;
        }
      });
    }
    if (count === 0) {
      return temp;
    } else {
      return temp + '+' + count;
    }
  };

  renderActivity = (value) => {
    const { genres, currentLocales } = this.props;
    const target = genres.find((e) => e.id === value);

    const languageCode = currentLocales[0] && currentLocales[0].languageCode;
    return !target.translations || !languageCode
      ? t(`achievement_list_${target.name}`)
      : get(
          target.translations.find((e) => e.locale.indexOf(languageCode) !== -1),
          'title',
          target.translations[0]
            ? target.translations[0].title
            : t(`achievement_list_${target.name}`),
        );
  };

  showActivity = (activities) => {
    let temp = '';
    let count = 0;
    if (activities && activities.length > 0) {
      activities.forEach((item, index) => {
        if (index < 3) {
          temp =
            temp +
            this.renderActivity(item.genreId) +
            (index < activities.length - 1 ? ' / ' : '');
        } else {
          count += 1;
        }
      });
    }
    if (count === 0) {
      return temp;
    } else {
      return temp + '+' + count;
    }
  };

  showEquipment = (value) => {
    let temp = '';
    let count = 0;
    const { equipmentList } = this.props;

    if (value && value.length > 0) {
      value.forEach((item, index) => {
        if (index < 2) {
          temp =
            temp +
            equipmentList.filter((select) => select.id === item.id)[0].title +
            (index < value.length - 1 ? ' / ' : '');
        } else {
          count += 1;
        }
      });
    }
    if (count === 0) {
      return temp;
    } else {
      return temp + '+' + count;
    }
  };

  renderGeneral = () => {
    const { user } = this.props;
    if (user == null) {
      return <View />;
    }
    if (user != null) {
      return (
        <View style={Classes.fullWidth}>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_email')}</Text>
            <Input
              value={user.email}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              editable={false}
            />
          </View>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_age')}</Text>
            <Input
              value={`${user.age}`}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              editable={false}
            />
          </View>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_gender')}</Text>
            <Input
              value={this.renderGender(get(user, 'settings.gender'))}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              editable={false}
            />
          </View>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_height')}</Text>
            <Input
              value={`${get(user, 'settings.height')}`}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              rightIcon={
                <Text style={styles.rightIcon}>{t('setting_my_profile_cm')}</Text>
              }
              editable={false}
            />
          </View>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_weight')}</Text>
            <Input
              value={`${get(user, 'settings.weight')}`}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              rightIcon={
                <Text style={styles.rightIcon}>{t('setting_my_profile_kg')}</Text>
              }
              editable={false}
            />
          </View>
          <View style={styles.flexBox}>
            <Text style={styles.leftTitle}>{t('setting_my_profile_city')}</Text>
            <Input
              value={get(user, 'settings.city')}
              inputStyle={styles.inputStyle}
              inputContainerStyle={styles.inputContainerStyle}
              editable={false}
            />
          </View>
        </View>
      );
    }
  };

  renderBody = () => {
    const { user } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_max_heart_rate')}</Text>
          <Input
            value={`${get(user, 'settings.maxHeartRate')}`}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            rightIcon={
              <Text style={styles.rightIcon}>{t('setting_my_profile_bpm')}</Text>
            }
            editable={false}
          />
        </View>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_ability')}</Text>
          <Input
            value={t(this.renderAbility(user.settings.ability))}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            editable={false}
          />
        </View>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_goal')}</Text>
          <Input
            value={t(this.renderGoal(user.settings.goal))}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            editable={false}
          />
        </View>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_injuries')}</Text>
          <Input
            value={this.showInjuries(user.settings.injuries)}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            editable={false}
          />
        </View>
      </View>
    );
  };

  renderWorkout = () => {
    const { user } = this.props;
    return (
      <View style={[styles.container, styles.lastBox]}>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_workout_type')}</Text>
          <Input
            value={this.showActivity(user.userActivity)}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            editable={false}
          />
        </View>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_schedule')}</Text>
          <Input
            value={t('exercise_cycle', {
              frequency: user.settings.schedule.frequency,
              duration: user.settings.schedule.duration,
            })}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            editable={false}
          />
        </View>
        <View style={styles.flexBox}>
          <Text style={styles.leftTitle}>{t('setting_my_profile_equipment')}</Text>
          <Input
            value={this.showEquipment(user.equipments)}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputContainerStyle}
            editable={false}
          />
        </View>
      </View>
    );
  };

  render() {
    const { showAll } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          backTo="SettingScreen"
          navHeight={44}
          back
          navTitle={t('setting_my_profile')}
        />
        <ScrollView style={styles.container}>
          {this.renderHeader()}
          {/* <Text style={styles.headerTitle}>{t('setting_my_profile_general')}</Text> */}

          <FormHeader text={t('setting_my_profile_general')} />
          {this.renderGeneral()}
          {showAll ? (
            <View>
              <FormHeader text={t('setting_my_profile_body')} />
              {/* <Text style={styles.headerTitle}>{t('setting_my_profile_body')}</Text> */}
              {this.renderBody()}
              <FormHeader text={t('setting_my_profile_workout')} />
              {/* <Text style={styles.headerTitle}>{t('setting_my_profile_workout')}</Text> */}
              {this.renderWorkout()}
            </View>
          ) : (
            <View style={styles.box}>
              <TouchableOpacity
                style={styles.downBtn}
                onPress={() => this.setState({ showAll: true })}
              >
                {/* TODO Change down image color? */}
                <Image source={Images.down} style={styles.downImg} />
              </TouchableOpacity>
            </View>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    genres: state.setting.genres,
    user: state.user,
    setting: state.setting.setting,
    equipmentList: state.search.equipmentList,
    currentLocales: state.appState.currentLocales,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateSetting: SettingActions.updateSetting,
        resetEditSetting: SettingActions.resetEditSetting,
        getEquipmentList: SearchActions.getEquipmentList,
        getWorkoutTypeList: SearchActions.getWorkoutTypeList,
      },
      dispatch,
    ),
)(MyProfileScreen);
