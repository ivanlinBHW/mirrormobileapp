import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Text, ScrollView, TouchableOpacity, FlatList, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { get } from 'lodash';

import { SecondaryNavbar, BouncyCheckbox, SquareButton } from 'App/Components';
import RNPickerSelect from 'react-native-picker-select';
import { SettingActions, SearchActions } from 'App/Stores';
import { Images, Classes, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import styles from './MyProfileScreenStyle';

class AddProfileScreenDetail extends React.Component {
  static propTypes = {
    editSetting: PropTypes.object,
    updateEditSetting: PropTypes.func.isRequired,
    updatePayload: PropTypes.func.isRequired,
    workoutTypeList: PropTypes.array,
    getWorkoutTypeList: PropTypes.func.isRequired,

    editEquipment: PropTypes.array,
    equipmentList: PropTypes.array,
    getEquipmentList: PropTypes.func.isRequired,
    updateEditEquipment: PropTypes.func.isRequired,
    updateEquipment: PropTypes.func.isRequired,
    updateSetting: PropTypes.func.isRequired,
  };

  static getDerivedStateFromProps(props, state) {
    if (state.equipment.equipmentList !== props.equipmentList) {
      const tempList = [];
      props.equipmentList.map((item) => {
        tempList.push({
          img: get(item, 'signedDeviceCoverImage', get(item, 'deviceCoverImage')) || '',
          title: item.name,
          key: item.name,
        });
      });
      return {
        equipment: {
          ...state.equipment,
          equipmentList: props.equipmentList,
          list: tempList,
        },
      };
    }
    if (state.activity.list !== props.workoutTypeList) {
      return {
        activity: {
          ...state.activity,
          list: props.workoutTypeList,
        },
      };
    }
    return null;
  }

  state = {
    ability: {
      list: [
        {
          title: 'setting_ability_beginner',
          key: 0,
          selected: false,
        },
        {
          title: 'setting_ability_intermediate',
          key: 1,
          selected: false,
        },
        {
          title: 'setting_ability_advanced',
          key: 2,
          selected: false,
        },
        {
          title: 'setting_ability_export',
          key: 3,
          selected: false,
        },
      ],
      select: null,
    },
    activity: {
      list: this.props.workoutTypeList,
      selected: [],
    },
    schedule: {
      frequency: 1,
      duration: 15,
      frequencyList: [
        { label: '1', value: 1 },
        { label: '2', value: 2 },
        { label: '3', value: 3 },
        { label: '4', value: 4 },
        { label: '5', value: 5 },
        { label: '6', value: 6 },
        { label: '7', value: 7 },
      ],
      durationList: [
        { label: '5', value: 5 },
        { label: '10', value: 10 },
        { label: '15', value: 15 },
        { label: '20', value: 20 },
        { label: '25', value: 25 },
        { label: '30', value: 30 },
        { label: '35', value: 35 },
        { label: '40', value: 40 },
        { label: '45', value: 45 },
        { label: '50', value: 50 },
        { label: '55', value: 55 },
        { label: '60', value: 60 },
      ],
    },
    equipment: {
      list: [],
      equipmentList: [],
      selected: [],
    },
  };

  ability = {
    onPressItem: (title, key) => {
      const { ability } = this.state;
      let temp = JSON.parse(JSON.stringify(ability.list));
      temp.forEach((item) => {
        if (item.title === title) {
          item.selected = true;
        } else {
          item.selected = false;
        }
      });
      this.setState({
        ability: {
          list: temp,
          select: key,
        },
      });
    },

    renderItem: ({ item }) => {
      return (
        <View style={styles.itemWrapper}>
          <TouchableOpacity
            style={styles.item}
            onPress={() => this.ability.onPressItem(item.title, item.key)}
          >
            <Text style={styles.itemTitle}>{t(`${item.title}`)}</Text>
            <BouncyCheckbox
              isChecked={item.selected}
              fillColor={Colors.black}
              checkboxSize={22}
              text=""
              borderColor={Colors.gray_03}
              onPress={() => this.ability.onPressItem(item.title, item.key)}
            />
          </TouchableOpacity>
        </View>
      );
    },

    render: () => {
      const { ability } = this.state;
      return (
        <>
          <Text style={styles.headerTitle}>
            {t('setting_ability_your_fitness_level')}
          </Text>
          <Text style={styles.placeholder}>{t('setting_placeholder_select_one')}</Text>
          <FlatList data={ability.list} renderItem={this.ability.renderItem} />
        </>
      );
    },
  };

  activity = {
    renderItem: ({ item }) => {
      const { activity } = this.state;
      return (
        <View style={styles.itemContainer}>
          <Image
            source={{
              uri:
                get(item, 'signedDeviceCoverImage', get(item, 'deviceCoverImage')) || '',
            }}
            style={styles.activityImg}
          />
          <View style={[styles.itemWrapper, styles.activityItemWrapper]}>
            <TouchableOpacity
              style={styles.item}
              onPress={() => this.activity.onPressItem(item.id)}
            >
              <Text style={[styles.itemTitle, styles.activityItemTitle]}>
                {item.title.toLowerCase()}
              </Text>
              <BouncyCheckbox
                isChecked={
                  activity.selected.filter((selectItem) => selectItem === item.id)
                    .length > 0
                }
                fillColor={Colors.black}
                checkboxSize={22}
                text=""
                borderColor={Colors.gray_03}
                onPress={() => this.activity.onPressItem(item.id)}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    },
    onPressItem: (key) => {
      const { activity } = this.state;
      let temp = JSON.parse(JSON.stringify(activity.selected));
      if (temp.length > 0 && temp.filter((item) => item === key).length > 0) {
        const index = temp.indexOf(key);
        temp.splice(index, 1);
      } else {
        temp.push(key);
      }
      console.log(temp);
      this.setState({
        activity: {
          list: activity.list,
          selected: temp,
        },
      });
    },
    render: () => {
      const { activity } = this.state;
      return (
        <>
          <Text style={styles.headerTitle}>
            {t('setting_activity_your_favorite_activity')}
          </Text>
          <Text style={styles.placeholder}>{t('setting_placeholder_select_more')}</Text>
          <FlatList
            data={activity.list}
            keyExtractor={(item) => item.id}
            renderItem={this.activity.renderItem}
          />
        </>
      );
    },
  };

  schedule = {
    render: () => {
      const { schedule } = this.state;
      const { frequency, duration, frequencyList, durationList } = schedule;
      return (
        <>
          <Text style={styles.headerTitle}>
            {t('setting_schedule_your_weekly_schedule')}
          </Text>
          <Text style={styles.scheduleTitle}>{t('setting_schedule_frequancy')}</Text>
          <RNPickerSelect
            onValueChange={(value) =>
              value &&
              this.setState({
                schedule: {
                  ...this.state.schedule,
                  frequency: value,
                },
              })
            }
            placeholder={{
              label: t('setting_schedule_frequancy_placeholder'),
              value: null,
              disabled: true,
            }}
            useNativeAndroidPickerStyle={false}
            items={frequencyList}
            value={frequency}
            style={{
              inputIOS: styles.pickerSmallMarginBottom,
              inputAndroid: styles.pickerSmallMarginBottom,
              iconContainer: styles.iconContainer,
            }}
            Icon={() => <Image source={Images.down_black} style={styles.downImg} />}
          />
          <Text style={styles.scheduleTitle}>{t('setting_schedule_duration')}</Text>
          <RNPickerSelect
            onValueChange={(value) =>
              value &&
              this.setState({
                schedule: {
                  ...this.state.schedule,
                  duration: value,
                },
              })
            }
            placeholder={{
              label: t('setting_schedule_duration_placeholder'),
              value: null,
              disabled: true,
            }}
            useNativeAndroidPickerStyle={false}
            items={durationList}
            value={duration}
            style={{
              inputIOS: styles.pickerSmallMarginBottom,
              inputAndroid: styles.pickerSmallMarginBottom,
              iconContainer: styles.iconContainer,
            }}
            Icon={() => <Image source={Images.down_black} style={styles.downImg} />}
          />
        </>
      );
    },
  };

  equipment = {
    renderItem: ({ item }) => {
      const { selected } = this.state.equipment;
      return (
        <View style={styles.itemContainer}>
          <Image
            source={{
              uri: item.img,
            }}
            style={styles.activityImg}
          />
          <View style={[styles.itemWrapper, styles.activityItemWrapper]}>
            <TouchableOpacity
              style={styles.item}
              onPress={() => this.equipment.onPressItem(item.key)}
            >
              <Text style={[styles.itemTitle, styles.activityItemTitle]}>
                {item.title}
              </Text>
              <BouncyCheckbox
                isChecked={
                  selected &&
                  selected.filter((selectItem) => selectItem === item.key).length > 0
                }
                fillColor={Colors.black}
                checkboxSize={22}
                text=""
                borderColor={Colors.gray_03}
                onPress={() => this.equipment.onPressItem(item.key)}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    },
    onPressItem: (key) => {
      const { selected } = this.state.equipment;
      let temp = JSON.parse(JSON.stringify(selected));
      if (temp.length > 0 && temp.filter((item) => item === key).length > 0) {
        const index = temp.indexOf(key);
        temp.splice(index, 1);
      } else {
        temp.push(key);
      }
      this.setState({
        equipment: {
          ...this.state.equipment,
          selected: temp,
        },
      });
    },
    render: () => {
      const { list } = this.state.equipment;
      return (
        <FlatList
          data={list}
          ListHeaderComponent={
            <View>
              <Text style={styles.headerTitle}>
                {t('setting_equipment_your_equipment')}
              </Text>
              <Text style={styles.placeholder}>
                {t('setting_placeholder_select_more')}
              </Text>
            </View>
          }
          ListFooterComponent={<View style={styles.emptyBox} />}
          keyExtractor={(item) => item.title}
          renderItem={this.equipment.renderItem}
        />
      );
    },
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    __DEV__ && console.log('@AddProfileDetailScreen');
    this.props.getWorkoutTypeList();
    this.props.getEquipmentList();
  }

  onPressSave = () => {
    const { editSetting, updateEditSetting, updatePayload } = this.props;
    const { ability, activity } = this.state;
    const { frequency, duration } = this.state.schedule;
    const { updateSetting, editEquipment, updateEquipment } = this.props;
    let payload = editSetting;

    const { updateEditEquipment, equipmentList } = this.props;
    const { equipment } = this.state;
    let temp = [];
    let tempItem = [];

    if (equipment.selected && equipment.selected.length > 0) {
      equipment.selected.forEach((select) => {
        let filterItem = [];
        filterItem = equipmentList.filter((item) => item.name === select);
        if (filterItem && filterItem.length > 0) {
          temp.push(filterItem[0].id);
          tempItem.push(filterItem[0]);
        }
      });
    }

    payload.settings.ability = ability.select;
    payload.settings.activity = activity.selected;
    payload.settings.schedule.frequency = frequency;
    payload.settings.schedule.duration = duration;
    payload.equipments = tempItem;
    updateEditSetting(payload);
    updatePayload({
      schedule: { frequency, duration },
      ability: ability.select,
      activity: activity.select,
    });

    updateSetting({
      schedule: { frequency, duration },
      ability: ability.select,
      activity: activity.selected,
    });
    updateEditEquipment({ equipments: temp });
    updateEquipment({ equipments: temp });
    Actions.UserRegisterSuccessScreen();
  };

  showDoneButton() {
    let showDoneButton = false;
    if (this.state.ability.select != null) showDoneButton = true;

    return showDoneButton;
  }

  onPressSkip = () => {
    Actions.UserRegisterSuccessScreen();
  };
  render() {
    const { ability, activity } = this.state;
    return (
      <View style={Classes.fill}>
        <SecondaryNavbar
          back
          style={{ backgroundColor: Colors.white }}
          navHeight={44}
          navTitle={t('1-2_add_profile')}
        />
        <ScrollView style={styles.container}>
          {this.ability.render()}
          {this.activity.render()}
          {this.schedule.render()}
          {this.equipment.render()}

          <View style={[Classes.margin, Classes.mainEnd, Classes.marginBottom]}>
            {this.showDoneButton() == true && (
              <SquareButton
                text={t('1-2_done')}
                disabledOpacity={0.5}
                onPress={this.onPressSave}
                radius={6}
              />
            )}
            {this.showDoneButton() == false && (
              <SquareButton
                text={t('1-2_skip')}
                disabledOpacity={0.5}
                onPress={this.onPressSkip}
                radius={6}
              />
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  (state, params) => ({
    editSetting: state.setting.editSetting,
    workoutTypeList: state.search.workoutTypeList,
    equipmentList: state.search.equipmentList,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getEquipmentList: SearchActions.getEquipmentList,
        updateEditSetting: SettingActions.updateEditSetting,
        updatePayload: SettingActions.updatePayload,
        getWorkoutTypeList: SearchActions.getWorkoutTypeList,
        updateEditEquipment: SettingActions.updateEditEquipment,
        updateEquipment: SettingActions.updateEquipment,
        updateSetting: SettingActions.updateSetting,
      },
      dispatch,
    ),
)(AddProfileScreenDetail);
