import { StyleSheet } from 'App/Helpers';
import { Classes, Metrics, Fonts, Colors } from 'App/Theme';

export default StyleSheet.create({
  imgBox: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '100@vs',
  },
  closeHotspotImg: {
    width: '60@s',
    height: '60@vs',
    flex: 1,
    resizeMode: 'contain'
  },
  textStyle: {
    marginLeft: Metrics.baseMargin,
  },
  btnTextStyle: {
    ...Fonts.style.medium500,
  },
  text: {
    ...Fonts.style.small500,
    color: Colors.gray_02,
    marginTop: Metrics.baseMargin,
    textAlign: 'center',
  },
  colorGray02: {
    color: Colors.gray_02,
  },
  introTitle: {
    fontSize: Fonts.size.h5,
    fontWeight: 'bold',
    marginBottom: 0,
    textAlign: 'center',
  },
  introContent: {
    ...Fonts.style.medium,
    padding: Metrics.baseMargin,
    color: Colors.gray_01,
    textAlign: 'center',
  },
  bottomView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  }
});
