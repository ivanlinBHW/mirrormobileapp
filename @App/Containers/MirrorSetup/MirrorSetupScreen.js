import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Text, View, Platform, SafeAreaView } from 'react-native';

import { SquareButton, SecondaryNavbar } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog } from 'App/Helpers';
import { Colors } from 'App/Theme';
import styles from './MirrorSetupScreenStyle';

class MirrorSetupScreen extends React.Component {
  static propTypes = {
    systemVersion: PropTypes.string.isRequired,
  };

  componentDidMount() {
    const { systemVersion } = this.props;
    if (
      Platform.OS === 'ios' &&
      typeof systemVersion === 'string' &&
      systemVersion.includes('.')
    ) {
      const osMajorVer = systemVersion.split('.')[0];
      const osMirrorVer = systemVersion.split('.')[1];
      if (systemVersion === '13.3' || (osMajorVer === '13' && osMirrorVer < 5)) {
        Dialog.iosNeedToUpdateAlert(() => Actions.pop());
      }
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <SecondaryNavbar
          title={t('mirror_setup_nav_header')}
          style={styles.navBar}
          back
        />
        <View style={styles.wrapper}>
          <View style={styles.topFlex}>
            <Text style={styles.setupTextLine1}>{t('mirror_setup_line1')}</Text>
            {/* <Text style={styles.setupTextLine2}>{t('mirror_setup_line2')}</Text> */}
          </View>
        </View>
        <SquareButton
          text={t('mirror_setup_pair_continue')}
          onPress={Actions.MirrorWifiInputScreen}
          color={Colors.black}
          textColor={Colors.white}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    systemVersion: state.appState.currentDevice.systemVersion,
  }),
  (dispatch) => bindActionCreators({}, dispatch),
)(MirrorSetupScreen);
