import { Platform } from 'react-native';
import { StyleSheet } from 'App/Helpers';
import { Classes, Metrics, Colors } from 'App/Theme';

export default StyleSheet.create({
  container: {
    ...Classes.fill,
    backgroundColor: Colors.white_02,
  },
  navBar: {
    marginVertical: Metrics.baseVerticalMargin,
  },
  wrapper: {
    paddingHorizontal: Metrics.baseMargin,
    paddingTop: Metrics.baseVerticalMargin,
  },
  listContainer: {
    paddingTop: Metrics.baseMargin,
  },
  btnWifiItem: {
    justifyContent: 'flex-start',
    marginBottom: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin * 4,
  },
  dialogInputWrapper:
    Platform.OS === 'android'
      ? {
          borderBottomWidth: 1,
          borderBottomColor: Colors.black,
        }
      : {},
});
