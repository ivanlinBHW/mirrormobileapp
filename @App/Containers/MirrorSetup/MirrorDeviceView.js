import React from 'react';
import PropTypes from 'prop-types';
import { debounce, isEmpty } from 'lodash';
import {
  Text,
  Alert,
  View,
  FlatList,
  Platform,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import { Badge } from 'react-native-elements';
import Ping from 'react-native-ping';

import { ConnectItemButton, SecondaryNavbar, BaseIconButton } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Classes, Fonts, Colors, Metrics } from 'App/Theme';
import { Screen, StyleSheet } from 'App/Helpers';

const styles = StyleSheet.create({
  container: {
    ...Classes.fill,
  },
  wrapper: {
    paddingHorizontal: Metrics.baseMargin,
  },
  txtFooter: {
    ...Fonts.style.small500,
    marginTop: Metrics.baseMargin * 2.5,
    color: Colors.brownGrey,
  },
  txtConnectOtherDevice: {
    marginBottom: Metrics.baseMargin,
  },
  btnNabBarRight: {
    justifyContent: 'flex-end',
    right: -2,
    position: 'absolute',
  },
  loadingIndicator: {
    width: '20@s',
    height: '20@vs',
    marginTop: '6@vs',
    right: -2,
  },
  badge: {
    width: '12@s',
    height: '12@s',
    borderRadius: '12@s',
    right: 7,
    marginTop: -14,
  },
  txtGray: {
    color: Colors.gray_01,
  },
});

class MirrorDeviceView extends React.PureComponent {
  static propTypes = {
    routeName: PropTypes.string.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsClear: PropTypes.func.isRequired,
    mirrorServices: PropTypes.array.isRequired,
    mirrorInitializedServices: PropTypes.array.isRequired,
    currentNetworkInfo: PropTypes.object.isRequired,
    connectedDevice: PropTypes.object,
    mirrorDiscoverInit: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mirrorConnect: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    lastDeviceInList: PropTypes.object,
    lastDevice: PropTypes.object,
    isPaired: PropTypes.bool.isRequired,
    mirrorCommunicate: PropTypes.func.isRequired,
    mirrorLog: PropTypes.func.isRequired,

    isLoading: PropTypes.bool.isRequired,

    onPressDeviceItem: PropTypes.func,
    onPressSetupNewDevice: PropTypes.func.isRequired,
  };

  state = {
    onPressDeviceItem: undefined,
    discoveredActiveDevices: null,
    lastActiveDeviceInList: null,
    services: {},
    serviceList: [],
    selectedDevice: null,
    connectedDevice: null,

    refreshing: false,
  };

  componentDidMount() {
    console.log('@MirrorDeviceView entered.');
    this.handleDiscoverDevice();
  }

  componentDidUpdate(prevProps) {
    const {
      mirrorServices,
      mirrorInitializedServices,
      lastDeviceInList,
      isPaired,
    } = this.props;
    if (isPaired) {
      if (
        JSON.stringify(prevProps.mirrorInitializedServices) !==
        JSON.stringify(mirrorInitializedServices)
      ) {
        this.handlePingDiscoveredDevices(mirrorInitializedServices);
      }
      if (
        JSON.stringify(prevProps.lastDeviceInList) !== JSON.stringify(lastDeviceInList)
      ) {
        this.handlePingDiscoveredDevices(lastDeviceInList);
      }
    } else {
      if (JSON.stringify(prevProps.mirrorServices) !== JSON.stringify(mirrorServices)) {
        this.handlePingDiscoveredDevices(mirrorServices);
      }
    }
  }

  handlePingDiscoveredDevices = async (services) => {
    const { mirrorLog } = this.props;
    let googleMs = -1;
    try {
      googleMs = await Ping.start('8.8.8.8', {
        timeout: 250,
      });
    } catch (e) {
      console.log('e=>', e);
      googleMs = -1;
    }
    if (googleMs < 0) {
      if (services && services.length) {
        this.setState({
          discoveredActiveDevices: services,
        });
      }
      if (services && services.host) {
        this.setState({
          lastActiveDeviceInList: services,
        });
      }
      return;
    }

    const pingDevice = async (e) => {
      try {
        const ms = await Ping.start(e.host, {
          timeout: 3000,
        });
        mirrorLog(`${e.host}: ${ms}ms`);
        return e;
      } catch (error) {
        mirrorLog(`${e.host}: ${error.message} (${error.code})`);
        return false;
      }
    };

    if (services && services.length) {
      const discoveredActiveDevices = await Promise.all(
        services.map(async (e) => await pingDevice(e)),
      );
      if (discoveredActiveDevices && discoveredActiveDevices.length) {
        this.setState({
          discoveredActiveDevices: discoveredActiveDevices.filter((e) => !!e),
        });
      }
    } else if (services && services.host) {
      const res = await pingDevice(services);
      if (res) {
        this.setState({
          lastActiveDeviceInList: services,
        });
      } else {
        this.setState({
          lastActiveDeviceInList: null,
        });
      }
    }
    this.handleTurnOffLoading();
  };

  handleDiscoverDevice = () => {
    const {
      currentNetworkInfo: { isConnected, type },
      mirrorDiscover,
      onLoading,
      mdnsClear,
      mdnsStop,
    } = this.props;
    if (!isConnected) {
      return Alert.alert(t('alert_need_wifi_title'), t('alert_need_wifi_desc'));
    }
    this.setState({ selectedDevice: null });

    mdnsClear();
    mirrorDiscover();
    onLoading(true);

    setTimeout(() => {
      mdnsStop();
      this.handleTurnOffLoading();
    }, 5000);
  };

  renderDeviceItem = ({ item, index }) => {
    const {
      currentNetworkInfo: { isConnected, type },
      onPressDeviceItem,
      connectedDevice,
    } = this.props;
    const { selectedDevice } = this.state;
    return (
      <ConnectItemButton
        active
        icon="wifi"
        iconColor={Colors.black}
        key={index}
        text={`${item.name} ${__DEV__ ? '(' : ''}${__DEV__ ? item.host : ''}${
          __DEV__ ? ')' : ''
        }`}
        onPress={
          onPressDeviceItem
            ? () => onPressDeviceItem(item)
            : () => this.setState({ selectedDevice: item })
        }
      />
    );
  };

  renderList = () => {
    const {
      currentNetworkInfo: { isConnected, type },
      isLoading,
    } = this.props;
    const { discoveredActiveDevices = [] } = this.state;
    const { refreshing } = this.state;
    return (
      <FlatList
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={this.handleDiscoverDevice} />
        }
        data={discoveredActiveDevices}
        keyExtractor={(item, index) => `device-${index}`}
        renderItem={this.renderDeviceItem}
      />
    );
  };

  renderEmptyView = () => {
    return (
      <View style={[Classes.fill, Classes.mainStart, Classes.padding]}>
        <Text style={styles.txtGray}>{t('mirror_device_empty')}</Text>
      </View>
    );
  };

  handleTurnOffLoading = debounce(() => {
    const { onLoading } = this.props;
    onLoading(false);
  }, 3000);

  render() {
    const {
      isLoading,
      connectedDevice,
      onPressDeviceItem,
      onPressSetupNewDevice,
      currentNetworkInfo: { isConnected, type },
    } = this.props;
    const {
      selectedDevice,
      lastActiveDeviceInList,
      discoveredActiveDevices = [],
    } = this.state;
    return (
      <View style={styles.container}>
        <SecondaryNavbar
          title={t('mirror_setup_wifi_setup')}
          navLeftComponent={
            <View style={Classes.fill}>
              <Text style={Fonts.style.regular500}>
                {t('mirror_setup_pair_pair_devices')}
              </Text>
            </View>
          }
          navRightComponent={
            isLoading ? (
              <ActivityIndicator
                size="small"
                color="black"
                style={styles.loadingIndicator}
              />
            ) : (
              <BaseIconButton
                iconType="FontAwesome5"
                iconName="redo"
                iconSize={Platform.isPad ? Screen.scale(8) : Screen.scale(16)}
                onPress={this.handleDiscoverDevice}
                style={styles.btnNabBarRight}
              />
            )
          }
        />
        <View style={styles.wrapper}>
          {lastActiveDeviceInList && lastActiveDeviceInList.txt.initialized === 'true' && (
            <>
              <View style={Classes.rowCenter}>
                <ConnectItemButton
                  active
                  connect
                  icon="wifi"
                  iconColor={Colors.black}
                  type="outline"
                  text={`${lastActiveDeviceInList.name} ${__DEV__ ? '(' : ''}${
                    __DEV__ ? lastActiveDeviceInList.host : ''
                  }${__DEV__ ? ')' : ''}`}
                  onPress={
                    onPressDeviceItem
                      ? () => onPressDeviceItem(lastActiveDeviceInList)
                      : () => this.setState({ selectedDevice: lastActiveDeviceInList })
                  }
                  disabled={!isConnected}
                />
                {connectedDevice &&
                  connectedDevice.host === lastActiveDeviceInList.host && (
                    <Badge status="success" badgeStyle={styles.badge} />
                  )}
              </View>
              <Text style={[Fonts.style.regular500, styles.txtConnectOtherDevice]}>
                {t('mirror_setup_pair_other_devices')}
              </Text>
            </>
          )}

          {this.renderList()}

          {/* <Text style={styles.txtFooter}>{t('mirror_setup_pair_footer_text')}</Text> */}
        </View>
        {!isLoading && isEmpty(discoveredActiveDevices) && this.renderEmptyView()}
      </View>
    );
  }
}

export default MirrorDeviceView;
