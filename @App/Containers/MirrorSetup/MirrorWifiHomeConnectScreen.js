import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Image, Text, View, SafeAreaView, TouchableOpacity } from 'react-native';
import { Input } from 'react-native-elements';
import { NetworkInfo } from 'react-native-network-info';

import {
  RoundButton,
  DismissKeyboard,
  BaseImageButton,
  SecondaryNavbar,
  SquareButton,
  LinkButton,
} from 'App/Components';
import { MirrorActions, MdnsActions, AppStateActions } from 'App/Stores';
import { filterMirrorService } from 'App/Stores/Mirror/Selectors';
import { translate as t } from 'App/Helpers/I18n';
import { Permission } from 'App/Helpers';
import { Styles, Images, Colors, Classes, Fonts } from 'App/Theme';
import styles from './MirrorWifiHomeConnectScreenStyle';

class MirrorWifiHomeConnectScreen extends React.Component {
  static propTypes = { setCurrentWifiSettingRoute: PropTypes.func.isRequired };

  static defaultProps = {};

  state = {};

  async componentDidMount() {
    __DEV__ && console.log('@MirrorWifiHomeConnectScreen');
  }

  render() {
    return (
      <View style={(Classes.fill, Styles.containerWhite02)}>
        <SecondaryNavbar back navTitle={t('1-3_home_connect_wifi')} />

        <View style={[Classes.fill, Classes.center]}>
          <View style={styles.imgBox}>
            <Image source={Images.connectWifi} style={styles.wifiImg} />
          </View>

          <View style={[Classes.fill, Classes.center]}>
            <Text style={styles.introTitle}>{t('1-3_connect_to_home_wifi')}</Text>
            <Text style={styles.introContent}>
              {t('1-3_connect_to_wifi_description')}
            </Text>
          </View>
        </View>

        <View style={[Classes.fill, Classes.padding]}>
          <TouchableOpacity>
            <SquareButton
              textStyle={styles.btnTextStyle}
              text={t('1-3_connect_to_mirror_device')}
              radius={6}
              onPress={() => {
                this.props.setCurrentWifiSettingRoute('WiFi');
                Actions.MirrorWifiInputScreen();
              }}
            />
          </TouchableOpacity>
          <View style={[Classes.fill, Classes.center]}>
            <LinkButton
              text="Use Mobile hotspot"
              textSize={Fonts.size.small}
              textColor={Colors.primary}
              onPress={Actions.MirrorWifiMobileCheckScreen}
              style={styles.bottomView}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) =>
    bindActionCreators(
      { setCurrentWifiSettingRoute: AppStateActions.setCurrentWifiSettingRoute },
      dispatch,
    ),
)(MirrorWifiHomeConnectScreen);
