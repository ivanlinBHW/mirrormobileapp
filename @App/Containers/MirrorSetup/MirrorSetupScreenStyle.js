import { ScaledSheet } from 'App/Helpers';
import { Styles, Classes, Colors, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    ...Classes.fill,
    backgroundColor: Colors.white_02,
  },
  navBar: {
    backgroundColor: Colors.white_02,
  },
  wrapper: {
    ...Classes.fill,
    ...Styles.layoutScreen,
    paddingHorizontal: Metrics.baseMargin,
    paddingTop: Metrics.baseVerticalMargin,
    flex: 1,
  },
  setupTextLine1: {
    ...Fonts.style.regular500,
    marginBottom: Metrics.baseVerticalMargin,
  },
  setupTextLine2: {
    ...Fonts.style.medium,
    marginBottom: '24@vs',
  },
  setupTextLine3: {
    ...Fonts.style.small,
    color: Colors.gray_02,
  },
});
