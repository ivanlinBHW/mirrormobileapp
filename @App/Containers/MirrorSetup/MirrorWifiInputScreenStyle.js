import { StyleSheet } from 'App/Helpers';
import { Classes, Metrics, Fonts, Colors } from 'App/Theme';

export default StyleSheet.create({
  container: {
    ...Classes.fill,
    backgroundColor: Colors.white_02,
  },
  imgBox: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '64@vs',
    marginBottom: '87@vs',
  },
  wifiImg: {
    width: '60@s',
    height: '60@vs',
  },
  textInputBox: {
    marginHorizontal: Metrics.baseMargin,
    flexDirection: 'column',
  },
  textInputTitle: {
    fontSize: Fonts.size.medium,
  },
  editImg: {
    marginRight: Metrics.baseMargin,
    justifyContent: 'flex-start',
  },
  inputBox: {
    height: '38@vs',
    paddingRight: 0,
    paddingLeft: 0,
    borderRadius: 6,
    backgroundColor: Colors.white,
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin,

    elevation: 2,
    shadowColor: Colors.shadow,
    shadowOpacity: 0.4,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 3,
    },
    justifyContent: 'center',
  },
  wrapper: {
    borderBottomWidth: 0,
  },
  textStyle: {
    marginLeft: Metrics.baseMargin,
  },
  buttonStyle: {
    width: '100%',
    marginTop: '24@vs',
    borderRadius: 6,
  },
  btnTextStyle: {
    ...Fonts.style.medium500,
  },
  text: {
    ...Fonts.style.small500,
    color: Colors.gray_02,
    marginTop: Metrics.baseMargin,
    textAlign: 'center',
  },
  colorGray02: {
    color: Colors.gray_02,
  },
});
