import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Image, Text, View, SafeAreaView, TouchableOpacity, Linking } from 'react-native';
import { Input } from 'react-native-elements';
import { NetworkInfo } from 'react-native-network-info';
import AndroidOpenSettings from 'react-native-android-open-settings';
import AndroidWifi from 'react-native-android-wifi-with-ap-status';

import {
  RoundButton,
  DismissKeyboard,
  BaseImageButton,
  SecondaryNavbar,
  SquareButton,
  LinkButton,
} from 'App/Components';
import { MirrorActions, MdnsActions, AppStateActions } from 'App/Stores';
import { filterMirrorService } from 'App/Stores/Mirror/Selectors';
import { translate as t } from 'App/Helpers/I18n';
import { Permission } from 'App/Helpers';
import { Styles, Images, Colors, Classes, Fonts } from 'App/Theme';
import styles from './MirrorCloseHotspotScreenStyle';

class MirrorCloseHotspotScreen extends React.Component {
  static propTypes = { setCurrentWifiSettingRoute: PropTypes.func.isRequired };

  static defaultProps = {};

  state = {};

  async componentDidMount() {
    __DEV__ && console.log('@MirrorCloseHotspotScreen');
  }

  onPressNext = () => {
    if (Platform.OS === 'android') {
      AndroidWifi.isApOn(
        (isEnabled) => {
          if (isEnabled) {
            console.log('Ap service enabled');
            AndroidOpenSettings.generalSettings();
          } else {
            console.log('Ap service is disabled');
            this.props.setCurrentWifiSettingRoute('HotSpot');
            Actions.MirrorWifiInputScreen();
          }
        },
        (error) => {},
      );
    } else {
      Linking.openURL('App-prefs:root=INTERNET_TETHERING');
      this.props.setCurrentWifiSettingRoute('HotSpot');
      Actions.MirrorWifiInputScreen();
    }
  };

  render() {
    return (
      <View style={(Classes.fill, Styles.containerWhite02)}>
        <SecondaryNavbar
          back
          navTitle={t("1-3_connect_to_mobile_hotspot")} 
        />

        <View style={[Classes.fill, Classes.center]}>
          <View style={styles.imgBox}>
            <Image source={Images.closeHotspot} style={styles.closeHotspotImg} />
          </View>

          <View style={[Classes.fill, Classes.center]}>
            {/* TODO 語系 */}
            <Text style={styles.introTitle}>{t("1-3_request_close_hotspot")}</Text>
            <Text style={styles.introContent}>{t("1-3_confirm_hotspot_closed")}</Text>
          </View>
        </View>

        <View style={[Classes.fill, Classes.padding]}>
          <TouchableOpacity>
            <SquareButton
              textStyle={styles.btnTextStyle}
              text={t('1-2_next')}
              radius={6}
              onPress={() => {
                this.onPressNext();
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) =>
    bindActionCreators(
      { setCurrentWifiSettingRoute: AppStateActions.setCurrentWifiSettingRoute },
      dispatch,
    ),
)(MirrorCloseHotspotScreen);
