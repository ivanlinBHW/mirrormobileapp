import { StyleSheet } from 'App/Helpers';
import { Fonts, Colors } from 'App/Theme';

export default StyleSheet.create({
  txtCurrentWifi: {
    ...Fonts.style.small500,
    color: Colors.gray_01,
  },
});
