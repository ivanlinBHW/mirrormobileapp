import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Image, Text, View, SafeAreaView } from 'react-native';
import { Input } from 'react-native-elements';
import { NetworkInfo } from 'react-native-network-info';

import {
  RoundButton,
  DismissKeyboard,
  BaseImageButton,
  SecondaryNavbar,
} from 'App/Components';
import { MirrorActions, MdnsActions, AppStateActions } from 'App/Stores';
import { filterMirrorService } from 'App/Stores/Mirror/Selectors';
import { translate as t } from 'App/Helpers/I18n';
import { Permission } from 'App/Helpers';
import { Styles, Images, Colors } from 'App/Theme';
import styles from './MirrorWifiInputScreenStyle';
import WifiManager from 'react-native-wifi-reborn';
import { Platform } from 'react-native';

class MirrorWifiInputScreen extends React.Component {
  static propTypes = {
    mirrorSetupSSID: PropTypes.func.isRequired,
    mirrorPreConnect: PropTypes.func.isRequired,
    mirrorSetup: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mirrorIsConnected: PropTypes.bool.isRequired,
    mirrorDiscoveredDevice: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsIsScanning: PropTypes.bool.isRequired,
    onLoading: PropTypes.func.isRequired,
    wifiSettingRoute: PropTypes.string,
  };

  static defaultProps = {
    mirrorDiscoveredDevice: null,
  };

  state = {
    SSID: '',
    password: '',
    disabled: true,
  };

  async componentDidMount() {
    __DEV__ && console.log('@MirrorWifiInputScreen');
    await this.requestPermission();
  }

  getSSID = async () => {
    const SSID = await NetworkInfo.getSSID();
    console.log('SSID=>', SSID);
    this.setState({
      SSID,
    });
    if (typeof SSID === 'string' && SSID.includes('TrunkStudio')) {
      this.setState({
        password: '',
      });
    }
  };

  requestPermission = async () => {
    console.log('=== wifiSettingRoute ===', this.props.wifiSettingRoute);
    if (this.props.wifiSettingRoute == 'WiFi') {
      try {
        const hasPermission = await Permission.checkAndRequestPermission(
          Permission.permissionType.GEOLOCATION_LOW,
        );
        this.setState({
          permission: hasPermission,
        });
        if (hasPermission) {
          await this.getSSID();
        }
      } catch (error) {
        __DEV__ && console.log('@MirrorWifiInputScreen: permission rejected, ', error);
      }
    }
  };

  handleNextScreen = async () => {
    const { mirrorSetupSSID } = this.props;
    const { SSID, password } = this.state;

    try {
      const currentSSID = await WifiManager.getCurrentWifiSSID();


      if(currentSSID != null){
        if(currentSSID.startsWith("mirror-")){
          if (Platform.OS === 'android') {
            await WifiManager.disconnect();
          }else {
            await WifiManager.disconnectFromSSID(currentSSID);
          }
        }
      }
        
    } catch (error) {
      console.log("=== mirror wifi init disconnect prve wifi error ===", error);
    }
    

    mirrorSetupSSID(SSID, password);
    Actions.MirrorSetupWifiInitScreen({
      SSID,
      PW: password,
    });


  };

  renderEditIconButton = () => {
    return (
      <BaseImageButton
        transparent
        source={Images.edit}
        imageHeight={30}
        imageWidth={30}
        imageStyle={styles.editImg}
        onPress={() => {
          this.setState((state) => ({
            disabled: false,
          }));
          if (this.ssidInput) {
            this.ssidInput.focus();
          }
        }}
      />
    );
  };

  render() {
    const { SSID, password, disabled } = this.state;
    return (
      <DismissKeyboard>
        <SafeAreaView style={Styles.containerWhite02}>
          <SecondaryNavbar back />
          <View style={styles.imgBox}>
            <Image source={Images.connectWifi} style={styles.wifiImg} />
          </View>
          <View style={styles.textInputBox}>
            <Text style={styles.textInputTitle}>
              {this.props.wifiSettingRoute == 'WiFi'
                ? t('wifi_wifi_input_title')
                : t('wifi_hotSpot_input_title')}
            </Text>
            <Input
              ref={(el) => {
                this.ssidInput = el;
              }}
              inputContainerStyle={styles.wrapper}
              containerStyle={styles.inputBox}
              inputStyle={styles.textStyle}
              value={SSID}
              disabled={this.props.wifiSettingRoute == 'WiFi' ? disabled : !disabled}
              onChangeText={(value) => this.setState({ SSID: value })}
              placeholder={
                this.props.wifiSettingRoute == 'WiFi'
                  ? t('wifi_wifi_input_title')
                  : t('wifi_hotSpot_input_title')
              }
              rightIcon={
                this.props.wifiSettingRoute == 'WiFi'
                  ? disabled && this.renderEditIconButton()
                  : !this.renderEditIconButton()
              }
            />
            <Input
              inputContainerStyle={styles.wrapper}
              containerStyle={styles.inputBox}
              value={password}
              onChangeText={(value) => this.setState({ password: value })}
              inputStyle={styles.textStyle}
              placeholder={t('mirror_setup_wifi_password')}
            />
            <Text style={styles.colorGray02}>
              {this.props.wifiSettingRoute == 'WiFi'
                ? t('mirror_setup_wifi_hint')
                : t('mirror_setup_hotSpot_hint')}
            </Text>
            <RoundButton
              uppercase={false}
              style={styles.buttonStyle}
              textStyle={styles.btnTextStyle}
              text={t('connect')}
              color={Colors.button.primary.content.background}
              onPress={this.handleNextScreen}
              disabled={!SSID}
            />
          </View>
        </SafeAreaView>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({
    mdnsIsScanning: state.mdns.isScanning,
    mdnsServices: filterMirrorService(state),
    mirrorDiscoveredDevice: state.mirror.discoveredDevice,
    mirrorIsConnected: state.mirror.isConnected,
    wifiSettingRoute: state.appState.wifiSettingRoute,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        mdnsStop: MdnsActions.onMdnsStop,
        mirrorDiscover: MirrorActions.onMirrorDiscover,
        mirrorSetup: MirrorActions.onMirrorSetup,
        mirrorPreConnect: MirrorActions.onMirrorPreConnect,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
        mirrorSetupSSID: MirrorActions.onMirrorSetupSSID,
      },
      dispatch,
    ),
)(MirrorWifiInputScreen);
