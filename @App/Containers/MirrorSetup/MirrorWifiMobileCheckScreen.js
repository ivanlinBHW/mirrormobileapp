import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Image, Text, View, SafeAreaView, TouchableOpacity, Linking } from 'react-native';
import { Input } from 'react-native-elements';
import { NetworkInfo } from 'react-native-network-info';
import AndroidOpenSettings from 'react-native-android-open-settings';
import AndroidWifi from 'react-native-android-wifi-with-ap-status';

import {
  RoundButton,
  DismissKeyboard,
  BaseImageButton,
  SecondaryNavbar,
  SquareButton,
  LinkButton,
} from 'App/Components';
import { MirrorActions, MdnsActions, AppStateActions } from 'App/Stores';
import { filterMirrorService } from 'App/Stores/Mirror/Selectors';
import { translate as t } from 'App/Helpers/I18n';
import { Permission } from 'App/Helpers';
import { Styles, Images, Colors, Classes, Fonts } from 'App/Theme';
import styles from './MirrorWifiMobileCheckScreenStyle';

class MirrorWifiMobileCheckScreen extends React.Component {
  static propTypes = {};

  static defaultProps = {};

  state = {};

  async componentDidMount() {
    __DEV__ && console.log('@MirrorWifiMobileCheckScreen');
  }

  onPressNext = () => {
    if (Platform.OS === 'android') {
      AndroidWifi.isEnabled((isEnabled) => {
        if (isEnabled) {
          console.log('wifi service enabled');
          Actions.MirrorCloseHotspotScreen();
        } else {
          console.log('wifi service is disabled');
          AndroidOpenSettings.wifiSettings();
        }
      });
    } else {
      Linking.openURL('App-prefs:root=WIFI');
      Actions.MirrorCloseHotspotScreen();
    }
  };

  render() {
    return (
      <View style={(Classes.fill, Styles.containerWhite02)}>
        <SecondaryNavbar back navTitle={t('1-3_connect_to_mobile_hotspot')} />

        <View style={[Classes.fill, Classes.center]}>
          <View style={styles.imgBox}>
            <Image source={Images.openWifi} style={styles.openWifiImg} />
          </View>

          <View style={[Classes.fill, Classes.center]}>
            <Text style={styles.introTitle}>{t('1-3_request_open_wifi')}</Text>
            <Text style={styles.introContent}>{t('1-3_confirm_open_wifi')}</Text>
          </View>
        </View>

        <View style={[Classes.fill, Classes.padding]}>
          <TouchableOpacity>
            <SquareButton
              textStyle={styles.btnTextStyle}
              text={t('1-2_next')}
              radius={6}
              onPress={() => {
                this.onPressNext();
              }}
            />
          </TouchableOpacity>
          <View style={[Classes.fill, Classes.center]}>
            <LinkButton
              text={t('1-3_connect_to_home_wifi')}
              textSize={Fonts.size.small}
              textColor={Colors.primary}
              onPress={Actions.MirrorWifiHomeConnectScreen}
              style={styles.bottomView}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => bindActionCreators({}, dispatch),
)(MirrorWifiMobileCheckScreen);
