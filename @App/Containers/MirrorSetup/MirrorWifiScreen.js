import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FlatList, RefreshControl, SafeAreaView, Text, View } from 'react-native';
import { bindActionCreators } from 'redux';
import Dialog from 'react-native-dialog';

import { AppStateActions, MirrorActions } from 'App/Stores';
import {
  ConnectItemButton,
  BaseIconButton,
  BaseNavBar,
  NavBackButton,
} from 'App/Components';
import { Fonts } from 'App/Theme';

import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { translate as t } from 'App/Helpers/I18n';
import styles from './MirrorWifiScreenStyle';

class MirrorWifiScreen extends React.Component {
  static propTypes = {
    searchedSsid: PropTypes.array,
    device: PropTypes.object.isRequired,
    mirrorClearWifiList: PropTypes.func.isRequired,
    mirrorCommunicate: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
  };

  static defaultProps = {
    searchedSsid: [],
  };

  state = {
    refreshing: false,
    password: '',
  };

  handleGetWiFiList = () => {
    const { mirrorCommunicate, mirrorClearWifiList, onLoading } = this.props;
    onLoading(true);
    mirrorClearWifiList();
    mirrorCommunicate(MirrorEvents.GET_WIFI_LIST);
  };

  handleConnectWiFi = () => {
    this.setState({ dialogVisible: false });
    const { password, currentWiFi } = this.state;
    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.SET_WIFI_PASS, {
      SSID: currentWiFi.SSID,
      PW: password,
    });
  };

  renderWifiItem = ({ item, index }) => {
    return (
      <ConnectItemButton
        key={index}
        text={item.SSID}
        onPress={() => this.setState({ dialogVisible: true, currentWiFi: item })}
      />
    );
  };

  renderModal = () => {
    const { dialogVisible, currentWiFi, password } = this.state;
    const handleOnCancel = () => this.setState({ dialogVisible: false });
    const handleOnTetChange = (text) => this.setState({ password: text });
    const getBtnStyle = () => ({ opacity: password ? 1 : 0.2 });
    return (
      currentWiFi && (
        <Dialog.Container visible={dialogVisible}>
          <Dialog.Title style={Fonts.style.input600}>
            {t('mirror_setup_wifi_enter_wifi_password')}
          </Dialog.Title>
          <Dialog.Description>
            {t('mirror_setup_wifi_password_for', { ssid: currentWiFi.SSID })}
          </Dialog.Description>
          <Dialog.Input
            placeholder={t('mirror_setup_wifi_password')}
            wrapperStyle={styles.dialogInputWrapper}
            onChangeText={handleOnTetChange}
            maxLength={63}
            secureTextEntry
          />
          <Dialog.Button label={t('__cancel')} onPress={handleOnCancel} />
          <Dialog.Button
            label={t('__ok')}
            onPress={this.handleConnectWiFi}
            style={getBtnStyle()}
            disabled={!password}
            maxLength
            bold
          />
        </Dialog.Container>
      )
    );
  };

  renderList = () => {
    const { searchedSsid = [] } = this.props;
    const { refreshing } = this.state;
    return (
      <View style={styles.wrapper}>
        <Text style={Fonts.style.regular500}>
          {t('mirror_setup_wifi_select_wifi_ap')}
        </Text>
        <FlatList
          contentContainerStyle={styles.listContainer}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.handleGetWiFiList}
              enabled
            />
          }
          data={searchedSsid}
          keyExtractor={(item, index) => `ssid-${index}`}
          renderItem={this.renderWifiItem}
        />
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <BaseNavBar
          title={t('mirror_setup_wifi_setup')}
          style={styles.navBar}
          leftComponent={<NavBackButton />}
          rightComponent={
            <BaseIconButton
              iconType="FontAwesome5"
              iconName="redo"
              onPress={this.handleGetWiFiList}
            />
          }
        />
        {this.renderList()}
        {this.renderModal()}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    isConnected: state.websocket.isConnected,
    lastReceivedMessage: state.websocket.lastReceivedMessage,
    lastSentData: state.websocket.lastSentData,
    device: state.mirror.discoveredDevice,
    searchedSsid: state.mirror.discoveredSsid,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mirrorClearWifiList: MirrorActions.onMirrorClearWifiList,
        mirrorConnect: MirrorActions.onMirrorConnect,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
        onLoading: AppStateActions.onLoading,
      },
      dispatch,
    ),
)(MirrorWifiScreen);
