import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Text, View, Platform, Image, TouchableOpacity } from 'react-native';

import { SquareButton, SecondaryNavbar, DismissKeyboard } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog } from 'App/Helpers';
import { Colors, Images, Classes, Fonts, Metrics } from 'App/Theme';
import { ScaledSheet } from 'App/Helpers';
const styles = ScaledSheet.create({
  container: {
    marginHorizontal: Metrics.baseMargin,
    height: '100%',
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '80@vs',
  },
  imageMain: {
    width: '60@s',
    height: '60@vs',
  },
  txtWrapper: {
    marginVertical: '40@vs',
    alignItems: 'center',
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: Metrics.baseMargin / 2,
  },
  txtFooter: {
    textAlign: 'center',
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'center',
    color: Colors.gray_01,
    textAlign: 'center',
    alignItems: 'center',
  },
});

class MirrorFirstSetupScreen extends React.Component {
  static propTypes = {};

  componentDidMount() {}

  render() {
    return (
      <DismissKeyboard>
        <View style={Classes.fill}>
          <SecondaryNavbar navTitle={t('1-3_home_connect_wifi')} />
          <View style={[Classes.fill]}>
            <View style={styles.container}>
              <View style={styles.imageWrapper}>
                <Image source={Images.connectWifi} style={styles.imageMain} />
              </View>
              <View style={styles.txtWrapper}>
                <Text style={styles.txtHeader}>{t('1-3_connect_to_home_wifi')}</Text>
                <Text>{t('1-3_connect_to_wifi_description')}</Text>
              </View>
              <TouchableOpacity style={{ padding: 15 }}>
                <SquareButton
                  textStyle={styles.loginStyle}
                  text={t('1-3_connect_to_mirror_device')}
                  radius={6}
                  accessible={true}
                  accessibilityLabel={'btn_login'}
                  onPress={Actions.MiiFirstSettingScreen}
                />
              </TouchableOpacity>
              <TouchableOpacity style={{ padding: 15 }}>
                <SquareButton
                  borderOutline
                  textStyle={styles.loginStyle}
                  text={t('__skip')}
                  radius={6}
                  onPress={Actions.ClassScreen}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({

  }),
  (dispatch) => bindActionCreators({}, dispatch),
)(MirrorFirstSetupScreen);
