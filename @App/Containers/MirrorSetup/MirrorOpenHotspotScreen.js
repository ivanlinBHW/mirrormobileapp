import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Image, Text, View, SafeAreaView, TouchableOpacity, Linking } from 'react-native';
import { Input } from 'react-native-elements';
import { NetworkInfo } from 'react-native-network-info';

import AndroidOpenSettings from 'react-native-android-open-settings';
import AndroidWifi from 'react-native-android-wifi-with-ap-status';

import {
  RoundButton,
  DismissKeyboard,
  BaseImageButton,
  SecondaryNavbar,
  SquareButton,
  LinkButton,
} from 'App/Components';
import { MirrorActions, MdnsActions, AppStateActions } from 'App/Stores';
import { filterMirrorService } from 'App/Stores/Mirror/Selectors';
import { translate as t } from 'App/Helpers/I18n';
import { Permission } from 'App/Helpers';
import { Styles, Images, Colors, Classes, Fonts } from 'App/Theme';
import styles from './MirrorOpenHotspotScreenStyle';
import { AppStore, AppStateActions as AppActions } from 'App/Stores';
class MirrorOpenHotspotScreen extends React.Component {
  static propTypes = { isLoading: PropTypes.bool.isRequired };

  static defaultProps = {};

  state = {};

  async componentDidMount() {
    __DEV__ && console.log('@MirrorOpenHotspotScreen');
    AppStore.dispatch(AppActions.onLoading(true));
  }

  onPressNext = () => {
    if (Platform.OS === 'android') {
      AndroidWifi.isApOn(
        (isEnabled) => {
          if (isEnabled) {
            console.log('Ap service enabled');
            Actions.popTo('MiiSettingScreen');
          } else {
            console.log('Ap service is disabled');
            AndroidOpenSettings.generalSettings();
          }
        },
        (error) => {},
      );
    } else {
      Linking.openURL('App-prefs:root=INTERNET_TETHERING');
      Actions.popTo('MiiSettingScreen');
    }
  };

  render() {
    return (
      <View style={(Classes.fill, Styles.containerWhite02)}>
        <SecondaryNavbar back navTitle={t('1-3_connect_to_mobile_hotspot')} />

        <View style={[Classes.fill, Classes.center]}>
          <View style={styles.imgBox}>
            <Image source={Images.openHotspot} style={styles.openHotspotImg} />
          </View>

          <View style={[Classes.fill, Classes.center]}>
            {/* TODO 語系 */}
            <Text style={styles.introTitle}>{t('1-3_request_open_hotspot')}</Text>
            <Text style={styles.introContent}>{t('1-3_confirm_hotspot_opened')}</Text>
          </View>
        </View>

        <View style={[Classes.fill, Classes.padding]}>
          <TouchableOpacity>
            <SquareButton
              textStyle={styles.btnTextStyle}
              text={t('1-2_next')}
              radius={6}
              onPress={() => {
                this.onPressNext();
              }}
              disabled={this.props.isLoading}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => ({ isLoading: state.appState.isLoading }),
  (dispatch) => bindActionCreators({}, dispatch),
)(MirrorOpenHotspotScreen);
