import { StyleSheet } from 'App/Helpers';
import { Classes, Metrics, Fonts, Colors } from 'App/Theme';

export default StyleSheet.create({
  container: {
    ...Classes.fill,
    backgroundColor: Colors.white_02,
  },
  wrapper: {
    paddingHorizontal: Metrics.baseMargin,
    paddingTop: Metrics.baseVerticalMargin / 2,
  },
  txtFooter: {
    ...Fonts.style.small500,
    marginTop: Metrics.baseMargin * 2.5,
    color: Colors.brownGrey,
  },
  txtConnectOtherDevice: {
    marginBottom: Metrics.baseMargin,
  },
  btnNabBarRight: {
    justifyContent: 'flex-end',
    position: 'absolute',
    right: 0,
  },
});
