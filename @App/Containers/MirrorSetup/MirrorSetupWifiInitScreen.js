import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  SafeAreaView,
  FlatList,
  Text,
  View,
  Alert,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { NetworkInfo } from 'react-native-network-info';
import { Permissions } from 'react-native-unimodules';

import { MirrorActions, MdnsActions, AppStateActions } from 'App/Stores';
import { SquareButton, SecondaryNavbar } from 'App/Components';
import { Fonts, Classes, Styles, Colors, Images, Metrics } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet } from 'App/Helpers';
const styles = ScaledSheet.create({
  container: {
    marginHorizontal: Metrics.baseMargin,
    height: '100%',
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '80@vs',
  },
  imageMain: {
    width: '60@s',
    height: '60@vs',
  },
  txtWrapper: {
    marginVertical: '40@vs',
    alignItems: 'center',
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: Metrics.baseMargin / 2,
  },
  txtFooter: {
    textAlign: 'center',
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'center',
    color: Colors.gray_01,
    textAlign: 'center',
    alignItems: 'center',
  },
});
class MirrorSetupWifiInitScreen extends React.Component {
  static propTypes = {
    mirrorLogs: PropTypes.array,
    mirrorPreConnect: PropTypes.func.isRequired,
    mirrorSetup: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mirrorIsConnected: PropTypes.bool.isRequired,
    mirrorDiscoveredDevice: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
    mdnsScanClear: PropTypes.func.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsInit: PropTypes.func.isRequired,
    mdnsIsScanning: PropTypes.bool.isRequired,
    onLoading: PropTypes.func.isRequired,
    currentNetworkInfo: PropTypes.object,
    currentState: PropTypes.string,

    SSID: PropTypes.string.isRequired,
    PW: PropTypes.string.isRequired,
    wifiSettingRoute: PropTypes.string,
  };

  static defaultProps = {
    mirrorLogs: [],
    mirrorDiscoveredDevice: null,
    currentNetworkInfo: {},
  };

  state = {
    currentSSID: '',
    currentState: '',
  };

  async componentDidMount() {
    const { mirrorSetup } = this.props;
    mirrorSetup();

    await this.requestPermission();
  }

  componentDidUpdate(prevProps, prevState) {
    const { currentNetworkInfo: state, currentState } = this.props;
    if (state !== prevProps.currentNetworkInfo && state.type === 'wifi') {
      this.handleConnect();
    }
    if (currentState !== prevProps.currentState && currentState === 'active') {
      this.handleConnect();
    }
  }

  componentWillUnmount() {
    const { mdnsStop } = this.props;
    mdnsStop();
  }

  handleConnect = async () => {
    const ssid = await NetworkInfo.getSSID();
    const isMirrorSsid =
      typeof ssid === 'string' && ssid.toLowerCase().includes('mirror-');
    if (isMirrorSsid) {
      this.handleConnectWifi();
    }
    this.setState({ currentSSID: ssid });
  };

  requestPermission = async () => {
    try {
      const { status } = await Permissions.askAsync(Permissions.LOCATION);
      this.setState({
        permission: status,
      });
      if (status !== 'granted') {
        throw new Error('@MirrorWifiInputScreen: Location permission not granted.');
      } else {
        const { currentNetworkInfo: { details: { ssid } = {} } = {} } = this.props;
        this.handleConnect(ssid);
      }
    } catch (error) {
      __DEV__ && console.log('@MirrorWifiInputScreen: permission rejected, ', error);
      Alert.alert(
        'Oops',
        'We need to LOCATION permission to get your connected Wifi SSID in order to pre-filled for you.',
      );
    }
  };

  handleConnectWifi = (isForce) => {
    const {
      mirrorPreConnect,
      mirrorDiscover,
      mdnsScanClear,
      mdnsInit,
      mdnsStop,
      onLoading,
    } = this.props;
    onLoading(true);
    mdnsStop();
    mdnsInit();
    mdnsScanClear();
    mirrorDiscover({
      onSuccess: (device) => {
        mdnsStop();
        const { PW, SSID } = this.props;
        mirrorPreConnect({
          device,
          wifiOptions: {
            SSID,
            PW,
          },
          isForce,
        });
      },
    });

    setTimeout(() => {
      mdnsStop();
      const { mirrorDiscoveredDevice } = this.props;
      if (!mirrorDiscoveredDevice) {
        Alert.alert(t('alert_title_oops'), t('alert_content_discover_mirror_failed'));
        onLoading(false);
      }
    }, 30 * 1000);
  };

  renderLogs() {
    const renderItem = ({ item }) => <Text style={Fonts.style.small}>{item}</Text>;
    return (
      <View style={Classes.fill}>
        {__DEV__ && (
          <FlatList
            style={[Styles.marginHorizontal, Styles.border]}
            data={this.props.mirrorLogs}
            renderItem={renderItem}
            keyExtractor={(item, index) => index.toString()}
          />
        )}
        <View style={Styles.marginHorizontal}>
          <SquareButton
            text={'Force Connect'}
            onPress={() => this.handleConnectWifi(true)}
            color={Colors.transparent}
            textColor={Colors.gray_12}
          />
        </View>
      </View>
    );
  }

  render() {
    const { PW, SSID, wifiSettingRoute } = this.props;
    const { currentSSID } = this.state;
    return (
      <SafeAreaView style={Styles.containerWhite02}>
        <SecondaryNavbar navTitle={t('mirror_setup_wifi_init_nav_title')} back />

        <View style={[Classes.fill]}>
          <View style={styles.container}>
            <View style={styles.imageWrapper}>
              <Image source={Images.qrcodeIcon} style={styles.imageMain} />
            </View>
            <View style={styles.txtWrapper}>
              <Text style={styles.txtHeader}>{t('1-4_scan_qr_code')}</Text>
              <Text>{t('mirror_setup_wifi_init_description')}</Text>
            </View>
            <TouchableOpacity style={{ padding: 15 }}>
              <SquareButton
                textStyle={styles.loginStyle}
                text={t('1-4_scan')}
                radius={6}
                accessible={true}
                accessibilityLabel={'btn_login'}
                onPress={() => {
                  Actions.WifiBarCodeScanScreen({ wifiSettingRoute: wifiSettingRoute });
                }}
              />
            </TouchableOpacity>
          </View>
        </View>

        {/* {this.renderLogs()} */}

        {this.props.wifiSettingRoute == 'WiFi' ? (
          <View style={[Styles.fullMargin, Classes.center]}>
            <Text style={styles.txtCurrentWifi}>
              {t('mirror_setup_wifi_init_current_wifi')} {currentSSID}
            </Text>
            <Text style={styles.txtCurrentWifi}>
              {t('wifi_wifi_input_title')}/{t('mirror_setup_wifi_password')}: {SSID}/{PW}
            </Text>
          </View>
        ) : null}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    mdnsIsScanning: state.mdns.isScanning,
    mirrorDiscoveredDevice: state.mirror.discoveredDevice,
    mirrorIsConnected: state.mirror.isConnected,
    mirrorLogs: state.mirror.logs,
    currentNetworkInfo: state.appState.currentNetworkInfo,
    currentState: state.appState.currentState,
    wifiSettingRoute: state.appState.wifiSettingRoute,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mdnsScanClear: MdnsActions.onMdnsScanClear,
        onLoading: AppStateActions.onLoading,
        mdnsStop: MdnsActions.onMdnsStop,
        mdnsInit: MdnsActions.onMdnsInit,
        mirrorSetup: MirrorActions.onMirrorSetup,
        mirrorDiscover: MirrorActions.onMirrorDiscover,
        mirrorPreConnect: MirrorActions.onMirrorPreConnect,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(MirrorSetupWifiInitScreen);
