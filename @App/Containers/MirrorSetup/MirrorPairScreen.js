import React from 'react';
import PropTypes from 'prop-types';
import { debounce, throttle } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Text,
  Alert,
  SafeAreaView,
  View,
  FlatList,
  Platform,
  RefreshControl,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Ping from 'react-native-ping';

import {
  ConnectItemButton,
  SecondaryNavbar,
  BaseIconButton,
  SquareButton,
} from 'App/Components';
import {
  MdnsActions,
  MirrorActions,
  WebsocketActions,
  AppStateActions as AppActions,
} from 'App/Stores';
import {
  filterMirrorService,
  filterLastRecordMirrorService,
  filterInitializedMirrorService,
} from 'App/Stores/Mirror/Selectors';
import { translate as t } from 'App/Helpers/I18n';
import { Classes, Fonts, Colors } from 'App/Theme';
import { Screen } from 'App/Helpers';
import styles from './MirrorPairScreenStyle';

class MirrorPairScreen extends React.Component {
  static propTypes = {
    routeName: PropTypes.string.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsClear: PropTypes.func.isRequired,
    mirrorServices: PropTypes.array.isRequired,
    mirrorInitializedServices: PropTypes.array.isRequired,
    currentNetworkInfo: PropTypes.object.isRequired,
    mirrorDiscoverInit: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mirrorConnect: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    lastDeviceInList: PropTypes.object,
    lastDevice: PropTypes.object,
    isPaired: PropTypes.bool.isRequired,
    mirrorCommunicate: PropTypes.func.isRequired,
    mirrorLog: PropTypes.func.isRequired,
  };

  state = {
    discoveredActiveDevices: null,
    lastActiveDeviceInList: null,
    services: {},
    serviceList: [],
    selectedDevice: null,

    refreshing: false,
  };

  componentDidMount() {
    console.log('@MirrorPairScreen entered.');
    setTimeout(() => {
      this.handleDiscoverDevice();
    }, 1000);
  }

  componentDidUpdate(prevProps) {
    const { routeName } = this.props;
    if (routeName === 'MirrorPairScreen' && prevProps !== this.props) {
      const {
        mirrorServices,
        mirrorInitializedServices,
        lastDeviceInList,
        isPaired,
      } = this.props;
      if (isPaired) {
        if (
          JSON.stringify(prevProps.mirrorInitializedServices) !==
          JSON.stringify(mirrorInitializedServices)
        ) {
          this.handlePingDiscoveredDevices(mirrorInitializedServices);
        }
        if (
          JSON.stringify(prevProps.lastDeviceInList) !== JSON.stringify(lastDeviceInList)
        ) {
          this.handlePingDiscoveredDevices(lastDeviceInList);
        }
      } else {
        if (JSON.stringify(prevProps.mirrorServices) !== JSON.stringify(mirrorServices)) {
          this.handlePingDiscoveredDevices(mirrorServices);
        }
      }
    }
  }

  handlePingDiscoveredDevices = async (services) => {
    const { mirrorLog } = this.props;
    let googleMs = -1;
    try {
      googleMs = await Ping.start('8.8.8.8', {
        timeout: 250,
      });
    } catch (e) {
      console.log('e=>', e);
      googleMs = -1;
    }
    if (googleMs < 0) {
      if (services && services.length) {
        this.setState({
          discoveredActiveDevices: services,
        });
      }
      if (services && services.host) {
        this.setState({
          lastActiveDeviceInList: services,
        });
      }
      return;
    }

    const pingDevice = async (e) => {
      try {
        const ms = await Ping.start(e.host, {
          timeout: 3000,
        });
        mirrorLog(`${e.host}: ${ms}ms`);
        return e;
      } catch (error) {
        mirrorLog(`${e.host}: ${error.message} (${error.code})`);
        return false;
      }
    };

    if (services && services.length) {
      const discoveredActiveDevices = await Promise.all(
        services.map(async (e) => await pingDevice(e)),
      );
      if (discoveredActiveDevices && discoveredActiveDevices.length) {
        this.setState({
          discoveredActiveDevices: discoveredActiveDevices.filter((e) => !!e),
        });
      }
    } else if (services && services.host) {
      const res = await pingDevice(services);
      if (res) {
        this.setState({
          lastActiveDeviceInList: services,
        });
      } else {
        this.setState({
          lastActiveDeviceInList: null,
        });
      }
    }
    this.handleTurnOffLoading();
  };

  handleBackToConnectScreen = () => {
    Actions.popTo('MirrorWifiInputScreen');
  };

  handleDiscoverDevice = () => {
    const {
      currentNetworkInfo: { isConnected, type },
      mirrorDiscover,
      onLoading,
      mdnsClear,
      mdnsStop,
    } = this.props;
    if (!isConnected || type !== 'wifi') {
      return Alert.alert(t('alert_need_wifi_title'), t('alert_need_wifi_desc'));
    }
    this.setState({ selectedDevice: null });

    mdnsClear();
    mirrorDiscover();
    onLoading(true);

    setTimeout(() => {
      mdnsStop();
      this.handleTurnOffLoading();
    }, 5000);
  };

  handleConnectDevice = (item) => () => {
    const { mirrorConnect, onLoading } = this.props;
    onLoading(true);
    mirrorConnect(
      item,
      {
        maxRetries: 5,
      },
      () => {
        Actions.ClassScreen();
      },
    );
  };

  renderDeviceItem = ({ item, index }) => {
    const {
      currentNetworkInfo: { isConnected, type },
    } = this.props;
    const { selectedDevice } = this.state;
    return (
      <ConnectItemButton
        key={index}
        text={`${item.name} ${__DEV__ ? '(' : ''}${__DEV__ ? item.host : ''}${
          __DEV__ ? ')' : ''
        }`}
        active={selectedDevice && item.name === selectedDevice.name}
        onPress={() => this.setState({ selectedDevice: item })}
        disabled={!isConnected || type !== 'wifi'}
      />
    );
  };

  renderList = () => {
    const {
      currentNetworkInfo: { isConnected, type },
    } = this.props;
    const { discoveredActiveDevices = [] } = this.state;
    const { refreshing } = this.state;
    return (
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={this.handleDiscoverDevice}
            enabled={isConnected && type === 'wifi'}
          />
        }
        data={discoveredActiveDevices}
        keyExtractor={(item, index) => `device-${index}`}
        renderItem={this.renderDeviceItem}
      />
    );
  };

  handleTurnOffLoading = debounce(() => {
    const { onLoading } = this.props;
    onLoading(false);
  }, 3000);

  render() {
    const {
      currentNetworkInfo: { isConnected, type },
    } = this.props;
    const { selectedDevice, lastActiveDeviceInList } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <SecondaryNavbar
          title={t('mirror_setup_wifi_setup')}
          navLeftComponent={
            <Text style={Fonts.style.regular500}>
              {t('mirror_setup_nav_left_connect_to')}
            </Text>
          }
          navRightComponent={
            <BaseIconButton
              iconType="FontAwesome5"
              iconName="redo"
              iconSize={Platform.isPad ? Screen.scale(8) : Screen.scale(20)}
              onPress={this.handleDiscoverDevice}
              style={styles.btnNabBarRight}
            />
          }
        />
        <View style={styles.wrapper}>
          {lastActiveDeviceInList && lastActiveDeviceInList.txt.initialized === 'true' && (
            <>
              <ConnectItemButton
                type="outline"
                text={`${lastActiveDeviceInList.name} ${__DEV__ ? '(' : ''}${
                  __DEV__ ? lastActiveDeviceInList.host : ''
                }${__DEV__ ? ')' : ''}`}
                active={selectedDevice === lastActiveDeviceInList}
                onPress={() => this.setState({ selectedDevice: lastActiveDeviceInList })}
                disabled={!isConnected || type !== 'wifi'}
              />
              <Text style={[Fonts.style.regular500, styles.txtConnectOtherDevice]}>
                {t('mirror_setup_pair_connect_other_mirror')}
              </Text>
            </>
          )}

          {this.renderList()}

          <ConnectItemButton
            text={t('mirror_setup_pair_setup_new_mirror')}
            onPress={Actions.MirrorSetupScreen}
            disabled={type !== 'wifi'}
          />
          <Text style={styles.txtFooter}>{t('mirror_setup_pair_footer_text')}</Text>
        </View>

        <View style={[Classes.fill, Classes.mainEnd]}>
          <SquareButton
            text={t('mirror_setup_pair_continue')}
            onPress={this.handleConnectDevice(selectedDevice)}
            color={Colors.black}
            textColor={Colors.white}
            disabled={!selectedDevice}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    routeName: state.appRoute.routeName,
    lastDevice: state.mirror.lastDevice,
    isPaired: state.mirror.isPaired,
    mdServices: state.mdns.services,
    lastDeviceInList: filterLastRecordMirrorService(state),
    mirrorServices: filterMirrorService(state),
    mirrorInitializedServices: filterInitializedMirrorService(state),
    currentNetworkInfo: state.appState.currentNetworkInfo,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppActions.onLoading,
        initConnection: WebsocketActions.initConnection,
        sendMessage: WebsocketActions.onSendMessage,
        onDeviceConnected: MirrorActions.onDeviceConnected,
        mirrorLog: MirrorActions.onMirrorLog,
        mirrorConnect: MirrorActions.onMirrorConnect,
        mirrorDiscover: MdnsActions.onMdnsScan,
        mirrorDiscoverInit: MdnsActions.onMdnsInit,
        mdnsClear: MdnsActions.onMdnsScanClear,
        mdnsStop: MdnsActions.onMdnsStop,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(MirrorPairScreen);
