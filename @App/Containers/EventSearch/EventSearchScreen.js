import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, FlatList, View, Text, Image } from 'react-native';
import { bindActionCreators } from 'redux';

import { translate as t } from 'App/Helpers/I18n';
import {
  SecondaryNavbar,
  BaseButton,
  BaseInput,
  EventList,
  FlatListEmptyLoading,
} from 'App/Components';
import { Images, Classes, Colors } from 'App/Theme';
import styles from './EventSearchScreenStyle';
import { CommunityActions } from 'App/Stores/index';

class EventSearchScreen extends React.Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    token: PropTypes.string,
    timezone: PropTypes.string.isRequired,
    getSearchEvents: PropTypes.func.isRequired,
    getTrainingEvent: PropTypes.func.isRequired,
    getUserDetail: PropTypes.func.isRequired,
    joinEvent: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    selected: [],
  };

  state = {
    text: '',
  };

  componentDidMount() {
    __DEV__ && console.log('@EventSearchScreen');
    const { getSearchEvents } = this.props;
    getSearchEvents('');
  }

  onPressSearch = (value) => {
    const { getSearchEvents } = this.props;
    this.setState({ text: value });
    getSearchEvents(value);
  };

  renderRightBtn = (item) => {
    const { text } = this.state;
    if (item.friendStatus === 2 && text !== '') {
      return (
        <BaseButton
          transparent
          text={t('__friend')}
          style={[styles.baseRightBtn]}
          textStyle={styles.baseTextStyle}
          textColor={Colors.button.primary.outline.text}
        />
      );
    }
  };

  renderItem = ({ item, index }) => {
    const { timezone, getTrainingEvent, joinEvent } = this.props;
    return (
      <EventList
        data={item}
        timezone={timezone}
        backgroundColor={index % 2 ? Colors.white_02 : Colors.gray_04}
        onPressAdd={() => joinEvent(item.id)}
        onPressDetail={() => getTrainingEvent(item.id)}
      />
    );
  };

  renderEmpty = () => {
    const { isLoading } = this.props;
    return (
      <FlatListEmptyLoading
        title={t('see_all_empty_title')}
        content={t('see_all_empty_content')}
        isLoading={isLoading}
      />
    );
  };

  render() {
    const { text } = this.state;
    const { list } = this.props;
    return (
      <SafeAreaView style={styles.wrapper}>
        <SecondaryNavbar back />

        <View style={styles.searchWrapper}>
          <BaseInput
            leftComponent={<Image source={Images.search_gray} style={styles.searchImg} />}
            leftIconContainerStyle={styles.leftIconContainerStyle}
            inputContainerStyle={styles.inputContainerStyle}
            containerStyle={styles.inputStyle}
            placeholder={t('search_title')}
            value={text}
            onChangeText={this.onPressSearch}
          />
        </View>
        {text === '' ? (
          <Text style={styles.textStyle}>{t('community_search_event_recommended')}</Text>
        ) : (
          <Text style={styles.textStyle}>{t('community_search_event_result')}</Text>
        )}
        <View style={Classes.fill}>
          <FlatList
            data={list}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderEmpty}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    list: state.community.events,
    selected: state.community.friendSelected,
    timezone: state.appState.currentTimeZone,
    isLoading: state.appState.isLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getSearchEvents: CommunityActions.getSearchEvents,
        getUserDetail: CommunityActions.getUserDetail,
        getTrainingEvent: CommunityActions.getTrainingEvent,
        joinEvent: CommunityActions.joinEvent,
        addFriend: CommunityActions.addFriend,
      },
      dispatch,
    ),
)(EventSearchScreen);
