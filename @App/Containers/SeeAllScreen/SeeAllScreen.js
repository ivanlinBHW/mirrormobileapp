import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { SeeAllView } from 'App/Components';
import { ClassActions, LiveActions, SearchActions, SeeAllActions } from 'App/Stores';

const initialSearchData = {
  text: '',
  isCompleted: false,
  isBookmarked: false,
  isNewArrival: false,
  isPopular: false,
  isMotionCapture: false,
  equipments: [],
  channel: [],
  genre: [],
  tags: [],
  level: [],
  duration: [],
  instructors: [],
};
class SeeAllScreen extends React.Component {
  static propTypes = {
    appRoute: PropTypes.object.isRequired,
    apiError: PropTypes.object,
    list: PropTypes.array.isRequired,
    listLoading: PropTypes.bool.isRequired,
    getClassDetail: PropTypes.func.isRequired,
    getLiveDetail: PropTypes.func.isRequired,
    timezone: PropTypes.string.isRequired,

    getSeeAllList: PropTypes.func.isRequired,
    setClassBookmark: PropTypes.func.isRequired,
    removeClassBookmark: PropTypes.func.isRequired,
    setClassLiveBookmark: PropTypes.func.isRequired,
    removeClassLiveBookmark: PropTypes.func.isRequired,
    isChannel: PropTypes.bool,
    hasNext: PropTypes.bool.isRequired,
    curPage: PropTypes.number,
    perPage: PropTypes.number,
    pageSize: PropTypes.number,
    total: PropTypes.number,
    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,
    searchData: PropTypes.object,
    filter: PropTypes.object,
    loginRoute: PropTypes.string,
    title: PropTypes.string,
    equipmentList: PropTypes.array,
    instructorList: PropTypes.array,

    setSearchData: PropTypes.func.isRequired,
    resetList: PropTypes.func.isRequired,

    showFilter: PropTypes.bool,
    mode: PropTypes.string,
  };

  static defaultProps = {
    showFilter: false,
    isChannel: false,
    apiError: null,
    searchData: initialSearchData,
    loginRoute: 'seeAll',
    mode: undefined,
  };

  state = {};

  componentDidMount() {
    __DEV__ && console.log('@SeeAllScreen');
    const { getSeeAllList, mode } = this.props;

    if (mode) {
      getSeeAllList(mode, 1);
    }
  }

  componentDidUpdate(prevProps) {
    const { routeName, prevRoute, type } = this.props.appRoute;

    if (routeName !== prevRoute && routeName == 'SeeAllScreen') {
      console.log('appRoute', 'SeeAllScreen');
    }
  }

  componentWillUnmount() {
    const { setSearchData, setPage } = this.props;
    setSearchData(initialSearchData);
    setPage(1);
  }

  onPressClassDetail = (id, data) => {
    const { getClassDetail, getLiveDetail, isChannel } = this.props;
    if (data.scheduledAt) {
      getLiveDetail(id);
    } else {
      getClassDetail(id, isChannel ? 'ChannelClassDetailScreen' : 'SearchScreen');
    }
  };

  onPressBookmark = (item) => {
    const {
      setClassBookmark,
      removeClassBookmark,
      setClassLiveBookmark,
      removeClassLiveBookmark,
    } = this.props;

    if (item.scheduledAt) {
      if (item.isBookmarked) {
        removeClassLiveBookmark(item.id);
      } else {
        setClassLiveBookmark(item.id);
      }
    } else {
      if (item.isBookmarked) {
        removeClassBookmark(item.id);
      } else {
        setClassBookmark(item.id);
      }
    }
  };

  onPressEndReach = (showLoadingIndicator = false) => {
    console.log('onPressEndReach');
    const {
      search,
      searchData,
      curPage,
      perPage,
      total,
      getSeeAllList,
      mode,
      hasNext,
      pageSize,
    } = this.props;

    console.log('onPressEndReach PageProps=>', {
      hasNext,
      total,
      curPage,
      perPage,
    });

    if (mode) {
      hasNext && getSeeAllList(mode, curPage + 1, perPage);
    } else {
      if (total >= 0 && pageSize * curPage - total <= 1) {
        search(
          searchData,
          {
            page: curPage + 1,
            pageSize: perPage,
            filter: [],
            type: 'page',
          },
          showLoadingIndicator,
        );
      }
    }
  };

  onPressClear = () => {
    const { search, searchFilter, perPage, resetList, searchData } = this.props;

    resetList();

    const payload = {
      text: '',
      isCompleted: false,
      isBookmarked: false,
      isMotionCapture: false,
      isNewArrival: false,
      isPopular: false,
      equipments: [],
      channel: [],
      genre: [searchData.genre[0]],
      tags: [],
      level: [],
      duration: [],
      instructors: [],
    };
    searchFilter(payload);
    search(
      payload,
      {
        page: 1,
        pageSize: perPage,
        filter: [],
        type: 'page',
      },
      'seeAll',
    );
  };

  render() {
    const {
      list,
      timezone,
      loginRoute,
      title,
      searchData,
      equipmentList,
      instructorList,
      setSearchData,
      listLoading,
      appRoute,
      apiError,
      showFilter,
    } = this.props;
    return (
      <SeeAllView
        showFilter={showFilter}
        apiError={apiError}
        list={list}
        timezone={timezone}
        title={title}
        searchData={searchData}
        loginRoute={loginRoute}
        onPressBookmark={this.onPressBookmark}
        onPressClassDetail={this.onPressClassDetail}
        onPressEndReach={this.onPressEndReach}
        equipmentList={equipmentList}
        instructorList={instructorList}
        setSearchData={setSearchData}
        onPressClear={this.onPressClear}
        listLoading={listLoading}
        appRoute={appRoute}
      />
    );
  }
}

export default connect(
  (state) => ({
    appRoute: state.appRoute,
    timezone: state.appState.currentTimeZone,
    list: state.seeAll.list,
    filter: state.seeAll.filter,
    curPage: state.seeAll.curPage,
    perPage: state.seeAll.perPage,
    hasNext: state.seeAll.hasNext,
    total: state.seeAll.total,
    listLoading: state.seeAll.listLoading,
    searchData: state.search.searchData,
    equipmentList: state.search.equipmentList,
    instructorList: state.search.instructorList,
    apiError: state.appApi.error,
    pageSize: state.seeAll.pageSize,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        setPage: SeeAllActions.setPage,
        getSeeAllList: SeeAllActions.getSeeAllList,
        getClassDetail: ClassActions.getClassDetail,
        getLiveDetail: LiveActions.getLiveDetail,
        setClassBookmark: ClassActions.setClassBookmark,
        removeClassBookmark: ClassActions.removeClassBookmark,
        setClassLiveBookmark: ClassActions.setClassLiveBookmark,
        removeClassLiveBookmark: ClassActions.removeClassLiveBookmark,
        search: SearchActions.search,
        searchFilter: SearchActions.searchFilter,
        setSearchData: SearchActions.setSearchData,
        resetList: SeeAllActions.resetList,
      },
      dispatch,
    ),
)(SeeAllScreen);
