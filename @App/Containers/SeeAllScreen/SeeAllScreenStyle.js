import { ScaledSheet, Screen } from 'App/Helpers';
import { Styles, Metrics, Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  layout: {
    ...Styles.screenPaddingLeft,
  },
  navBar: {
    ...Styles.navBar,
    marginBottom: Metrics.baseMargin,
  },
  button: {
    width: '44@s',
    height: '44@vs',
    borderWidth: 0,
  },
  back: {
    width: '10@s',
    height: '18@vs',
  },
  flex: {
  },
  wrapper: {
    flex: 1,
    height: '100%',
    marginBottom: Metrics.baseMargin * 3,
  },
  listBox: {
    height: '100%',
    paddingBottom: Metrics.baseMargin,
  },
  emptyBox: {
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Screen.scale(266),
  },
  emptyTitle: {
    fontSize: Screen.scale(24),
    color: Colors.black,
    textAlign: 'center',
    ...Fonts.style.fontWeight500,
  },
  emptyContent: {
    ...Fonts.style.small500,
    marginTop: '12@vs',
    color: Colors.gray_01,
    textAlign: 'center',
  },
  itemBox: {
    marginBottom: Metrics.baseMargin,
  },
});
