import {
  ActivityIndicator,
  Alert,
  FlatList,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  BaseReadMore,
  HeadLine,
  RoundLabel,
  SecondaryNavbar,
  SquareButton,
  CachedImage as Image,
  CachedImage as ImageBackground,
} from 'App/Components';
import { Classes, Colors, Images, Metrics } from 'App/Theme';
import { isArray, isEmpty, isString } from 'lodash';

import Emitter from 'tiny-emitter/instance';
import LinearGradient from 'react-native-linear-gradient';
import { ProgramActions } from 'App/Stores';
import ProgramTabList from './ProgramTabList';
import PropTypes from 'prop-types';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './ProgramDetailStyle';
import { translate as t } from 'App/Helpers/I18n';

class ProgramDetail extends React.Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    isStandalone: PropTypes.bool,
    data: PropTypes.object,
    getProgramClassDetail: PropTypes.func.isRequired,
    startProgram: PropTypes.func.isRequired,
    leaveProgram: PropTypes.func.isRequired,
    getProgramDetail: PropTypes.func.isRequired,
    token: PropTypes.string,
    programId: PropTypes.string,
    isProgramFinished: PropTypes.bool,
    programHistoryId: PropTypes.string,
    setProgramBookmark: PropTypes.func.isRequired,
    removeProgramBookmark: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isStandalone: true,
    data: {},
  };

  constructor(props) {
    super(props);
    const weekSchedules = props.data.weekSchedules || [];
    const notEmptyList = weekSchedules.find((e) => e && e.length > 0);
    const index = weekSchedules.indexOf(notEmptyList);
    this.state = {
      index: index,
      routes: [],
    };
  }

  componentDidMount() {
    const { programId, getProgramDetail, token, data } = this.props;
    if (programId) {
      getProgramDetail(programId, token);
    }
    if (data && typeof data.weekSchedules === 'object' && data.weekSchedules.length > 0) {
      this.setState({
        routes: data.weekSchedules.map((item, index) => ({
          key: index,
          title: t(`week_${index + 1}`),
        })),
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { data } = this.props;
    if (
      typeof data.weekSchedules === 'object' &&
      data.weekSchedules.length > 0 &&
      prevProps.data.weekSchedules !== data.weekSchedules
    ) {
      this.setState({
        routes: data.weekSchedules.map((item, index) => ({
          key: index,
          title: t(`week_${index + 1}`),
        })),
      });
    }
  }

  onPressDetail = (id, trainingProgramClassHistory, isFinished) => {
    const { getProgramClassDetail, token, programHistoryId } = this.props;
    getProgramClassDetail(
      id,
      token,
      trainingProgramClassHistory,
      isFinished,
      programHistoryId,
      'ProgramClassDetail',
    );
  };

  onPressBookmark = (id, bool) => {
    const { setProgramBookmark, removeProgramBookmark } = this.props;
    if (bool) {
      removeProgramBookmark(id);
    } else {
      setProgramBookmark(id);
    }
  };

  onPressTabBar = (itemIndex) => () => {
    this.setState({ index: itemIndex });
    if (this.scrollView) {
      setTimeout(() => {
        this.scrollView.scrollToEnd();
      }, 250);
    }
  };

  renderPureTabBar = () => {
    const { index, routes } = this.state;
    return (
      <View style={styles.baseTab}>
        {routes.map((element, itemIndex) => (
          <TouchableOpacity
            style={index === itemIndex ? styles.focusTab : styles.tab}
            key={element.key}
            onPress={this.onPressTabBar(itemIndex)}
          >
            <Text
              style={[
                index === itemIndex ? styles.focusText : styles.text,
              ]}
            >
              {element.title}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  renderPureTabView = () => {
    const { data } = this.props;
    const { index } = this.state;
    return (
      !!data.weekSchedules[index] && (
        <ProgramTabList
          list={data.weekSchedules[index]}
          onPressDetail={this.onPressDetail}
          onPressBookmark={this.onPressBookmark}
        />
      )
    );
  };

  renderItem = ({ item, index }) => {
    return (
      <View key={index} style={styles.imageBox}>
        <Image
          source={item.signedDeviceCoverImageObj}
          style={styles.equipments}
          resizeMode="contain"
        />
        <Text style={styles.equipmentsText}>{item.title}</Text>
      </View>
    );
  };

  renderProgramCard = () => {
    const { data } = this.props;
    return (
      <ImageBackground
        source={data.signedCoverImageObj}
        style={styles.programImgWidth}
        imageSize="2x"
        imageType="h"
      >
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
          locations={[0, 1]}
          colors={[`rgba(50,50,50,0)`, `rgba(50,50,50,0.4)`]}
        />
        <View style={styles.programFragment}>
          <View style={styles.programLeftBox}>
            <View style={styles.programLeft}>
              <Text style={styles.programSecondTitle}>{t('__program')}</Text>
              <Text style={styles.programTitle}>
                {isString(data.title) && data.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.programRightBox}>
            <View style={styles.programRight}>
              <RoundLabel text={t(`program_card_level_${data.level}`)} />
              <RoundLabel text={`${data.weeks} ${t('weeks')}`} />
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  };

  renderHeader = (data) => {
    return (
      <View style={styles.toolBox}>
        <View style={styles.flexBox}>
          <Image source={Images.difficulty} style={styles.flexImg} />
          <Text style={styles.flexText}>{t(`program_detail_level_${data.level}`)}</Text>
        </View>
        {/* <View style={styles.flexBox}>
          <Text style={styles.flexTextIcon}>{data.days}</Text>
          <View style={styles.flexSecondBox}>
            <Text style={styles.flexText}>{t('program_detail_days_a_week')}</Text>
          </View>
        </View> */}
      </View>
    );
  };

  resetHomeNavBarPosition = () => {
    setTimeout(() => {
      requestAnimationFrame(() => {
        Emitter.emit('onListReset');
      });
    }, 5);
  };

  startProgram = () => {
    const { data, startProgram, token } = this.props;
    startProgram(data.id, token);
    this.resetHomeNavBarPosition();
  };

  leaveProgram = () => {
    const { programHistoryId, leaveProgram, token } = this.props;

    Alert.alert(t('program_detail_alert_title'), t('player_screen_content'), [
      {
        text: t('__cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('__quit'),
        onPress: () => {
          leaveProgram(programHistoryId, token);

          this.resetHomeNavBarPosition();
        },
      },
    ]);
  };

  render() {
    const { data, isStandalone, programId, isLoading } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        {isEmpty(programId) && <SecondaryNavbar backTo="Program" back />}
        <ScrollView
          ref={(ref) => (this.scrollView = ref)}
          isStandalone={isStandalone}
          nestedScrollEnabled
        >
          {this.renderProgramCard()}
          {/* {this.renderHeader(data)} */}
          <View style={styles.contentBox}>
            <HeadLine
              paddingHorizontal={16}
              title={t('progrma_detail_about_this_program')}
              rightComponent={
                <ActivityIndicator animating={isLoading} size="small" color="black" />
              }
            />
            <BaseReadMore text={data.description} />
          </View>
          <View style={styles.contentBox}>
            <HeadLine paddingHorizontal={16} title={t('class_detail_good_for_tags')} />
            <View style={[Classes.row, Classes.paddingLeft, Classes.paddingRight]}>
              {isArray(data.tags) &&
                data.tags.map((e, i) => (
                  <Text key={`${e.tagId}`} style={styles.txtTags}>
                    {e.name}
                    {i !== data.tags.length - 1 ? ', ' : '.'}
                  </Text>
                ))}
            </View>
          </View>
          {data.requiredEquipments && data.requiredEquipments.length > 0 && (
            <View style={styles.contentBox}>
              <HeadLine
                paddingHorizontal={16}
                title={t('program_detail_equipment_needed')}
                rightComponent={
                  <ActivityIndicator animating={isLoading} size="small" color="black" />
                }
              />
              <View style={styles.equipmentBox}>
                <FlatList
                  horizontal
                  data={data.requiredEquipments}
                  showsHorizontalScrollIndicator={false}
                  renderItem={this.renderItem}
                />
                {/* {data.requiredEquipments.map((e, i) => this.renderItem(e, i))} */}
              </View>
            </View>
          )}

          <View
            style={[
              styles.contentBox,
              !isStandalone && {
                paddingBottom: Metrics.homeNavBarHeight + Metrics.baseVerticalMargin,
              },
            ]}
          >
            <HeadLine paddingHorizontal={16} title={t('setting_schedule_nav_title')} />
            {this.renderPureTabBar()}
            {this.renderPureTabView()}
            {!isEmpty(programId) && (
              <SquareButton
                onPress={this.leaveProgram}
                color="transparent"
                style={styles.leaveBtn}
                textColor={Colors.button.primary.text.text}
                text={t('program_detail_leave')}
              />
            )}
          </View>
        </ScrollView>

        {isEmpty(programId) && (
          <SquareButton
            onPress={this.startProgram}
            text={t('program_detail_start_this_program')}
          />
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    isLoading: state.appState.isLoading,
    data: state.program.detail,
    token: state.user.token,
    programId: state.user.activeTrainingProgramId,
    isProgramFinished: state.program.isProgramFinished,
    programHistoryId: state.user.activeTrainingProgramHistoryId,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getProgramClassDetail: ProgramActions.getProgramClassDetail,
        startProgram: ProgramActions.startProgram,
        leaveProgram: ProgramActions.leaveProgram,
        setProgramBookmark: ProgramActions.setProgramBookmark,
        removeProgramBookmark: ProgramActions.removeProgramBookmark,
        getProgramDetail: ProgramActions.getProgramDetail,
      },
      dispatch,
    ),
)(ProgramDetail);
