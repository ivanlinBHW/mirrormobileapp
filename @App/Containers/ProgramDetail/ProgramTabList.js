import { ClassCard, HeadLine } from 'App/Components';
import { Text, View, FlatList } from 'react-native';

import { Classes } from 'App/Theme';
import PropTypes from 'prop-types';
import React from 'react';
import { isEmpty } from 'lodash';
import styles from './ProgramDetailStyle';
import { translate as t } from 'App/Helpers/I18n';

export default class ProgramTabList extends React.PureComponent {
  static propTypes = {
    list: PropTypes.array.isRequired,
    onPressDetail: PropTypes.func.isRequired,
    onPressBookmark: PropTypes.func.isRequired,
  };

  renderItem = ({ item }) => {
    const { onPressBookmark } = this.props;
    return (
      <View style={styles.cardMargin}>
        <ClassCard
          size="full"
          data={item}
          isHover={item.isFinished}
          onPress={() =>
            this.props.onPressDetail(
              item.id,
              item.trainingProgramClassHistory
                ? item.trainingProgramClassHistory.id
                : null,
              item.isFinished,
            )
          }
          onPressBookmark={() => onPressBookmark(item.id, item.isBookmarked)}
        />
      </View>
    );
  };

  render() {
    const { list = [] } = this.props;
    return (
      <View
        style={[!isEmpty(list) && Classes.fill, !isEmpty(list) && styles.flatBox]}
      >
        <HeadLine
          paddingHorizontal={16}
          title={`${list.length} ${t('workout')}`}
        />
        {isEmpty(list) ? (
          <View style={[Classes.fillCenter, Classes.marginBottom]}>
            <Text style={Classes.fillCenter}>{t('program_empty_week')}</Text>
          </View>
        ) : (
          <FlatList
            data={list}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => item.id}
          />
        )}
      </View>
    );
  }
}
