import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Fonts, Styles, Colors, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    height: '100%',
  },
  navBar: {
    ...Styles.navBar,
  },
  imgStyle: {
    width: '100%',
    height: '300@vs',
  },
  contentBox: {
    marginTop: Metrics.baseMargin,
  },
  context: {
    paddingHorizontal: '20@s',
  },
  equipmentBox: {
    marginHorizontal: Metrics.baseMargin,
    marginTop: Metrics.baseMargin,
    flexDirection: 'row',
  },
  titleBox: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title: {
    fontSize: 36,
  },
  toolBox: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin,
  },
  flexBox: {
    width: '64@s',
    height: '62@vs',
    marginLeft: Metrics.baseMargin,
    flexDirection: 'column',
    alignItems: 'center',
  },
  flexImg: {
    width: '34@s',
    height: '34@vs',
    marginBottom: Metrics.baseMargin / 4,
  },
  flexText: {
    ...Fonts.style.extraSmall500,
    textAlign: 'center',
  },
  flexSecondBox: {
    width: '40@s',
  },
  flexTextIcon: {
    ...Fonts.style.h4_500,
  },
  flatBox: {
    marginBottom: Metrics.baseMargin,
    justifyContent: 'flex-start',
  },
  equipmentIcon: {
    marginRight: Metrics.baseMargin,
  },
  programImgWidth: {
    width: '100%',
    height: Platform.isPad ? Screen.verticalScale(270) : Screen.verticalScale(230),
    overflow: 'hidden',
  },
  programTitle: {
    ...Fonts.style.regular500,
    color: Colors.bright_light_blue,
  },
  programBoxStyle: {
    borderRadius: 7.5,
    backgroundColor: Colors.black,
    marginTop: Metrics.baseMargin / 4,
    alignItems: 'center',
  },
  programText: {
    ...Fonts.style.extraSmall500,
    paddingVertical: Metrics.baseMargin / 8,
    paddingHorizontal: Metrics.baseMargin / 2,
    color: Colors.white,
  },
  programLiveBoxStyle: {
    borderRadius: 7,
    width: '55@s',
    height: '15@vs',
    backgroundColor: Colors.white,
    marginBottom: 4,
    alignItems: 'center',
  },
  programLiveText: {
    fontSize: 10,
    fontWeight: '500',
    letterSpacing: 1,

    color: Colors.primary,
  },
  programFragment: {
    flexDirection: 'row',
    flex: 1,
  },
  programLeftBox: {
    flex: 1,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  programRightBox: {
    marginRight: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  programLeft: {
    flexDirection: 'column',
  },
  programRight: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },
  programTime: {
    fontSize: '11@vs',
    color: '#f0f0f0',
  },
  programSecondTitle: {
    ...Fonts.style.extraSmall500,
    color: Colors.gray_04,
  },
  programSecondIcon: {
    width: '50@vs',
    backgroundColor: 'blue',
    color: Colors.white,
    flexDirection: 'row',
  },
  programEquipments: {
    width: '20@vs',
    height: '20@vs',
  },
  programImageBox: {
    width: '40@vs',
    height: '40@vs',
    borderRadius: '40@vs',
    marginRight: '8@s',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  programEquipmentsBox: {
    flexDirection: 'row',
  },
  programTimeBox: {
    width: '100%',
    height: '13@vs',
    marginBottom: '12@vs',
  },
  imageBox: {
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: '16@s',
  },
  equipments: {
    width: Platform.isPad ? '35@s' : '30@s',
    height: Platform.isPad ? '35@s' : '30@s',
  },
  equipmentsText: {
    fontSize: '10@vs',
    fontWeight: '500',
    letterSpacing: 1,
    width: '60@s',
    textAlign: 'center',
  },

  baseTab: {
    flexDirection: 'row',
    marginLeft: '16@s',
    marginRight: '16@s',
    backgroundColor: '#f8f8f8',
    borderRadius: '40@s',
    marginTop: Metrics.baseMargin / 2,
  },
  tab: {
    flex: 1,
    alignItems: 'center',
  },
  focusTab: {
    ...Styles.BaseTab.tab,
    backgroundColor: Colors.tabBar.focus,
    borderRadius: 40,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    elevation: 4,
  },
  text: {
    ...Fonts.style.small500,
    marginVertical: '6@vs',
    lineHeight: '30@vs',
  },
  textGray02: {
    color: Colors.tabBar.text,
  },
  focusText: {
    ...Fonts.style.small500,
    marginVertical: '6@vs',
    lineHeight: '30@vs',
    color: 'white',
  },
  bar: {
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
    borderBottomWidth: 1,
    marginTop: '16@vs',
  },
  leaveBtn: {
    marginTop: Metrics.baseMargin,
  },
  achievementImge: {
    width: '270@s',
    height: '270@s',
  },
  containerLightPink: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  achievementBox: {
    width: '300@s',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  achievementTitle: {
    marginTop: '40@vs',
    fontSize: '34@s',
    ...Fonts.style.bold,
  },
  achievementContent: {
    fontSize: Fonts.size.medium,
    width: '254@s',
    color: Colors.gray_01,
    marginTop: Metrics.baseMargin * 4,
    textAlign: 'center',
  },
  cardMargin: {
    marginBottom: Metrics.baseMargin,
    marginTop: '12@vs',
  },
  classCardList: {
    flex: 1,
  },
});
