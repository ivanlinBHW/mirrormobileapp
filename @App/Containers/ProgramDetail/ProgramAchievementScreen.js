import React from 'react';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import { Text, View, SafeAreaView, Image, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Images } from 'App/Theme';
import { Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import UserActions from 'App/Stores/User/Actions';
import ProgramActions from 'App/Stores/Program/Actions';

import { BaseIconButton, SecondaryNavbar } from 'App/Components';
import styles from './ProgramDetailStyle';

class ProgramAchievementScreen extends React.PureComponent {
  static propTypes = {
    achievement: PropTypes.string,
    date: PropTypes.string,
    resetProgram: PropTypes.func.isRequired,
    title: PropTypes.string,
    description: PropTypes.string,
    getProgramList: PropTypes.func.isRequired,
    token: PropTypes.string,
  };

  static defaultProps = {
    achievement: '',
    date: '',
  };

  onPressProgramList = () => {
    const { resetProgram, token, getProgramList } = this.props;
    resetProgram();
    getProgramList(token);
    Actions.Program();
  };

  render() {
    return (
      <SafeAreaView style={styles.containerLightPink}>
        <SecondaryNavbar
          navHeight={80}
          navRightComponent={
            <BaseIconButton
              iconType="FontAwesome5"
              iconName="times"
              iconSize={Platform.isPad ? Screen.scale(12) : Screen.scale(24)}
              onPress={this.onPressProgramList}
            />
          }
        />
        <View style={styles.achievementBox}>
          <Image source={Images.program_achievement} style={styles.achievementImge} />
          <Text style={styles.achievementTitle}>{t('program_achievement_title')}</Text>
          <Text style={styles.achievementContent}>
            {t('program_achievement_content')}
          </Text>
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    token: state.user.token,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        resetProgram: UserActions.resetProgram,
        getProgramList: ProgramActions.getProgramList,
      },
      dispatch,
    ),
)(ProgramAchievementScreen);
