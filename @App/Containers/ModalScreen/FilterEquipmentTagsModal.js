import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, ScrollView } from 'react-native';

import { translate as t } from 'App/Helpers/I18n';
import { BaseModal, SelectedTagsButton, BaseSearchButton } from 'App/Components';
import { StyleSheet } from 'App/Helpers';

import { isArray } from 'lodash';
import { Images, Colors, Classes, Metrics } from 'App/Theme';
import { SeeAllActions, SearchActions } from 'App/Stores';
import { get } from 'lodash';

let styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.gray_01,
  },
  model: {
    padding: 16,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {},
  scrollContainer: {
    alignSelf: 'stretch',
  },
});

const NO_EQUIPMENT = 'no-equipment';

class FilterEquipmentTagsModal extends React.PureComponent {
  static propTypes = {
    equipmentList: PropTypes.array,
    searchData: PropTypes.object,
    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,
    resetList: PropTypes.func.isRequired,
    pageSize: PropTypes.number,
    searchableFilter: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    const equipmentList = get(props, 'equipmentList', [])
      .filter((equipment) =>
        get(props, 'searchableFilter.equipments', []).includes(equipment.id),
      )
      .map((equipment) => ({
        ...equipment,
        selected: get(props, 'searchData.equipments', []).includes(equipment.id),
      }));
    const countSelectedTags = isArray(equipmentList)
      ? equipmentList.filter((e) => e.selected).length
      : 0;

    const noEquipment = props.searchData.equipments.includes(NO_EQUIPMENT);
    this.state = {
      noEquipment,
      equipmentList,
      countSelectedTags: noEquipment ? countSelectedTags + 1 : countSelectedTags,
    };
  }

  _mounted = false;

  _updated = false;

  componentDidUpdate(prevProps, prevState) {
    this._updated = this.state.countSelectedTags !== prevState.countSelectedTags;
  }

  componentWillUnmount() {
    this.onPressFinish();
  }

  getSelectedTags = () => {
    return this.state.equipmentList.filter((e) => e.selected);
  };

  onPressEquipment = (id) => {
    const { equipmentList = [] } = this.state;

    this.setState({
      equipmentList: equipmentList.map((e) => ({
        ...e,
        selected: e.id === id ? !e.selected : e.selected,
      })),
    });

    if (this.state.noEquipment) {
      this.setState({
        countSelectedTags: this.getSelectedTags().length + 1,
      });
    } else {
      this.setState({
        countSelectedTags: this.getSelectedTags().length,
      });
    }
  };

  onPressNoEquipment = () => {
    const { noEquipment } = this.state;
    const noEquipmentTemp = !noEquipment;

    this.setState({
      noEquipment: noEquipmentTemp,
      countSelectedTags: this.getSelectedTags().length + (noEquipmentTemp ? 1 : 0),
    });
  };

  onPressFinish = () => {
    if (!this._updated) {
      return false;
    }
    const { searchData, resetList, search, searchFilter, pageSize } = this.props;
    const { equipmentList = [], noEquipment } = this.state;

    const tempEquipment = equipmentList.filter((e) => e.selected).map((e) => e.id);
    if (noEquipment) {
      tempEquipment.push(NO_EQUIPMENT);
    }

    const payload = {
      ...searchData,
      equipments: tempEquipment,
    };

    console.log('search payload ->', payload);
    resetList();
    search(payload, {
      page: 1,
      pageSize: pageSize,
      filter: [],
      type: 'page',
    });
  };

  renderSelectedNoEquipment() {
    return (
      <SelectedTagsButton
        text={t('search_no_equipment')}
        onPressCancelFilter={this.onPressNoEquipment}
      />
    );
  }

  render() {
    const { noEquipment, equipmentList, countSelectedTags } = this.state;

    return (
      <BaseModal
        style={Classes.fillCenter}
        title={t('search_equipment')}
        navRightComponent={{ onPress: this.onPressFinish, text: t('search_apply') }}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.model}>
            <Text style={styles.text}>{`${countSelectedTags} item(s) selected.`}</Text>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              {noEquipment && this.renderSelectedNoEquipment()}
              {isArray(this.getSelectedTags()) &&
                this.getSelectedTags().map((item, index) => {
                  return (
                    <SelectedTagsButton
                      key={item.id}
                      text={item.title}
                      onPressCancelFilter={() => {
                        this.onPressEquipment(item.id);
                      }}
                    />
                  );
                })}
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              <BaseSearchButton
                image={
                  noEquipment ? Images.no_equipment_white_1 : Images.no_equipment_black
                }
                text={t('search_no_equipment')}
                isSelected={noEquipment}
                onPress={this.onPressNoEquipment}
              />
              {isArray(equipmentList) &&
                equipmentList.map((item) => {
                  return (
                    <BaseSearchButton
                      key={item.id}
                      image={{
                        uri: item.selected ? item.coverImage : item.deviceCoverImage,
                      }}
                      text={item.title}
                      isSelected={item.selected}
                      onPress={() => this.onPressEquipment(item.id)}
                    />
                  );
                })}
            </View>
          </View>
        </ScrollView>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    equipmentList: state.search.equipmentList,
    searchData: state.search.searchData,
    searchableFilter: state.seeAll.filter,
    pageSize: state.seeAll.pageSize,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        search: SearchActions.search,
        resetList: SeeAllActions.resetList,
        searchFilter: SearchActions.searchFilter,
      },
      dispatch,
    ),
)(FilterEquipmentTagsModal);
