import React from 'react';
import { connect } from 'react-redux';
import { throttle, isEmpty, isString, has } from 'lodash';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { TabView } from 'react-native-tab-view';
import { Actions } from 'react-native-router-flux';
import Slider from 'react-native-slider';

import MusicModal from './MusicModalContent';
import OutputDeviceModal from './OutputDeviceModalContent';

import { translate as t } from 'App/Helpers/I18n';
import { BaseModal } from 'App/Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { StripeView } from '@ublocks-react-native/component';
import MirrorActions, { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import { Colors, Images } from 'App/Theme';
import styles from './AudioModalStyle';

import SystemSetting from 'react-native-system-setting';

class AudioModal extends React.Component {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
    music_volume: PropTypes.number,
    instructor_volume: PropTypes.number,
    output_audio_device: PropTypes.string,
    activeDevice: PropTypes.string,
    mode: PropTypes.string,
    isLive: PropTypes.bool,
    playerDetail: PropTypes.object,
    spotifyPlayLists: PropTypes.array.isRequired,
  };

  static defaultProps = {
    music_volume: 0,
    instructor_volume: 0,
    activeDevice: 'mirror-music',
    mode: 'playScreen',
    isLive: false,
    playerDetail: {},
  };

  state = {
    index: 0,
    routes: [
      { key: 'audio', title: t('audio_audio') },
      { key: 'music', title: t('music_music') },
      { key: 'output', title: t('output_device_output_device') },
    ],
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter AudioModal!');
    this.props.onMirrorCommunicate(MirrorEvents.GET_AUDIO_SETTING);
  }

  onBackPress = () => {
    const { index } = this.state;
    if (index > 0) {
      this.setState((state) => ({
        index: 0,
      }));
      this.props.onMirrorCommunicate(MirrorEvents.GET_AUDIO_SETTING);
    } else if (index === 0) {
      Actions.pop();
    }
  };

  onTabIndexChange = (index) => {
    this.setState({ index });
  };

  onMusicValueChange = (volumeType, volume, save) => {

    this.props.onMirrorCommunicate(MirrorEvents.CHANGE_AUDIO_VOLUME, {
      type: volumeType,
      volume: Math.round(volume),
      save,
    });
  };

  renderStripesView = (jumpTo) => {
    const { output_audio_device, activeDevice, spotifyPlayLists } = this.props;
    const isPlayingSpotify = spotifyPlayLists.some((e) => e.isPlaying);
    return [
      {
        leftComponent: <Text style={styles.leftTitle}>{t('audio_now_playing')}</Text>,
        rightComponent: (
          <View style={styles.rightWrapper}>
            {!isEmpty(activeDevice) &&
            has(activeDevice, 'data.type') &&
            isString(activeDevice.data.type) &&
            isPlayingSpotify ? (
              <TouchableOpacity style={styles.box} onPress={() => jumpTo('music')}>
                <Icon style={styles.spIcon} name="spotify" size={16} />
                <Text style={styles.rightText}>{t('audio_spotify_music')}</Text>
                <Image source={Images.next} resizeMode="contain" style={styles.img} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={styles.box} onPress={() => jumpTo('music')}>
                <Text style={styles.rightText}>{t('audio_mirror_music')}</Text>
                <Image source={Images.next} resizeMode="contain" style={styles.img} />
              </TouchableOpacity>
            )}
          </View>
        ),
        onPress: () => jumpTo('music'),
      },
      {
        leftComponent: (
          <Text style={styles.leftTitle}>{t('output_device_output_device')}</Text>
        ),
        rightComponent: (
          <TouchableOpacity style={styles.box} onPress={() => jumpTo('output')}>
            {/* <Image source={Images.next} style={styles.img} /> */}
            <Text style={styles.rightText}>
              {output_audio_device === 'mirror-speaker'
                ? t('audio_mirror_speaker')
                : output_audio_device}
            </Text>
            <Image source={Images.next} resizeMode="contain" style={styles.img} />
          </TouchableOpacity>
        ),
        onPress: () => jumpTo('output'),
      },
    ];
  };

  renderAudioSettings = (jumpTo) => {
    const {
      spotifyPlayLists,
      activeDevice,
      playerDetail,
      music_volume,
      instructor_volume,
      mode,
    } = this.props;

    const isSpotify =
      !isEmpty(activeDevice) &&
      has(activeDevice, 'data.type') &&
      isString(activeDevice.data.type) &&
      spotifyPlayLists instanceof Array &&
      spotifyPlayLists.some((e) => e.isPlaying);

    const hasMusic = playerDetail && !!playerDetail.mainTrainingMusicUrl;
    return (
      <>
        <StripeView
          stripeLightColor="white"
          stripeDarkColor="white"
          stripStyle={styles.stripStyle}
          stripes={this.renderStripesView(jumpTo)}
        />
        <View style={styles.itemBox}>
          <View style={styles.item}>
            <Text style={styles.headTitle}>{t('audio_music_volume')}</Text>
            <View style={styles.sliderbox}>
              <Image source={Images.voice} style={styles.voiceImg} />
              <Slider
                style={styles.slider}
                minimumValue={0}
                disabled={mode === 'livePlayScreen' || (!hasMusic && !isSpotify)}
                maximumValue={100}
                value={music_volume}
                onSlidingComplete={(value) =>
                  this.onMusicValueChange('music-volume', value, true)
                }
                minimumTrackTintColor={
                  mode === 'livePlayScreen' ? Colors.gray_03 : Colors.slider.primary.track
                }
                maximumTrackTintColor={
                  mode === 'livePlayScreen' ? Colors.gray_03 : Colors.slider.primary.track
                }
                thumbTintColor={Colors.white}
                thumbStyle={styles.thumbStyle}
                animationConfig={{ useNativeDriver: true }}
              />
            </View>
          </View>
        </View>
        <View style={styles.itemBox}>
          <View style={styles.item}>
            <Text style={styles.headTitle}>{t('audio_instructor_volume')}</Text>
            <View style={styles.sliderbox}>
              <Image source={Images.voice} style={styles.voiceImg} />
              <Slider
                style={styles.slider}
                minimumValue={0}
                maximumValue={100}
                value={instructor_volume}
                onSlidingComplete={throttle(
                  (value) => this.onMusicValueChange('instructor-volume', value, true),
                  1000,
                )}
                minimumTrackTintColor={Colors.slider.primary.track}
                maximumTrackTintColor={Colors.slider.primary.track}
                thumbTintColor={Colors.white}
                thumbStyle={styles.thumbStyle}
                animationConfig={{ useNativeDriver: true }}
              />
            </View>
          </View>
        </View>
      </>
    );
  };

  renderMusicSettings = () => {
    return <View />;
  };

  renderScene = ({ route, jumpTo }) => {
    const { mode } = this.props;
    switch (route.key) {
      case 'audio':
        return this.renderAudioSettings(jumpTo);
      case 'music':
        return <MusicModal mode={mode} jumpTo={jumpTo} />;
      case 'output':
        return <OutputDeviceModal jumpTo={jumpTo} />;
    }
  };

  render() {
    const { index, routes } = this.state;
    return (
      <BaseModal
        title={routes[index].title}
        isShowBack={index > 0}
        onBackPress={this.onBackPress}
      >
        <View style={styles.wrapperBox}>
          <TabView
            navigationState={this.state}
            swipeEnabled={false}
            renderScene={this.renderScene}
            onIndexChange={this.onTabIndexChange}
            tabBarPosition="none"
            lazy
          />
        </View>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    spotifyPlayLists: state.spotify.playlists,
    appRoute: state.appRoute,
    isLoading: state.appState.isLoading,
    activeDevice: state.mirror.activeList,
    music_volume: state.mirror.options[MirrorOptions.MUSIC_VOLUME],
    instructor_volume: state.mirror.options[MirrorOptions.INSTROCTOR_VOLUME],
    output_audio_device: state.mirror.options[MirrorOptions.OUTPUT_AUDIO_DEVICE],
    playerDetail: state.player.detail,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(AudioModal);
