import React, { useState, useEffect } from 'react';
import Permissions from 'react-native-permissions';
import WifiManager from 'react-native-wifi-reborn';
import { Actions } from 'react-native-router-flux';
import { BarCodeScanner } from 'expo-barcode-scanner';
import {
  Text,
  View,
  Alert,
  StyleSheet,
  Platform,
  SafeAreaView,
  Image,
} from 'react-native';

import { Screen } from 'App/Helpers';
import { Classes, Colors, Metrics, Fonts, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { BaseIconButton, SquareButton, RoundButton } from 'App/Components';
import { AppStore, AppStateActions } from 'App/Stores';
import { Camera } from 'expo-camera';
const styles = StyleSheet.create({
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
  },
  imageMain: {
    width: 100,
    height: 100,
  },
  qrcodeDescriptionText: {
    color: Colors.white,
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.black,
  },
  layerTop: {
    flex: 2,
    backgroundColor: Colors.black,
  },
  layerCenter: {
    flex: 4,
    flexDirection: 'row',
  },
  layerLeft: {
    flex: 0.5,
    backgroundColor: Colors.black,
  },
  focused: {
    flex: 10,
  },
  layerRight: {
    flex: 1,
    backgroundColor: Colors.black,
  },
  layerBottom: {
    flex: 2,
    backgroundColor: Colors.black,
  },
  btnCancel: {
    position: 'absolute',
    left: Metrics.baseMargin,
    top: Metrics.baseMargin,
  },
  containerRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  containerColumn: {
    flexDirection: 'column',
    backgroundColor: 'black',
    flex: 1,
  },
  fixedRatio: {
    backgroundColor: 'rgba(52, 52, 52, 0.0)',
    flex: 1,
    aspectRatio: 1,
    borderRadius: 12,
    borderStyle: 'solid',
    borderWidth: 6,
    borderColor: '#ffffff',
  },
  fixedRatioBoard: {
    backgroundColor: 'rgba(52, 52, 52, 0.0)',
    flex: 1,
    aspectRatio: 1,
    borderRadius: 12,
    borderStyle: 'solid',
    borderWidth: 6,
    borderColor: '#ffffff',
    margin: -5,
    marginLeft: -5,
  },
});

let timer = null;

export default function WifiBarCodeScanScreen({ wifiSettingRoute }) {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ type, data }) => {
    if (data.startsWith('WIFI:') && data.endsWith(';;')) {
      const config = data
        .replace('WIFI:', '')
        .replace(';;', '')
        .split(';');
      const ssid = config[0]
        .replace('S:', '')
        .replace('<', '')
        .replace('>', '');
      const password = config[2]
        .replace('P:', '')
        .replace('<', '')
        .replace('>', '');

      AppStore.dispatch(AppStateActions.onDeviceWifiInit(ssid));
      AppStore.dispatch(AppStateActions.onLoading(true));

      WifiManager.connectToProtectedSSID(ssid, password, false).then(
        (res) => {
          if (wifiSettingRoute == 'HotSpot') {
            Actions.MirrorOpenHotspotScreen();
          } else {
            requestAnimationFrame(Actions.pop);
          }
        },
        (e) => {
          console.warn('WifiBarCodeScanScreen error=>', e);
          requestAnimationFrame(Actions.pop);
          requestAnimationFrame(() =>
            __DEV__
              ? Alert.alert(e.message, JSON.stringify(e))
              : Alert.alert(
                  t('alert_title_oops'),
                  `${t('alert_wifi_auto_connect_failed_desc')}(${e.message})`,
                ),
          );
          AppStore.dispatch(AppStateActions.onLoading(false));
        },
      );
      setScanned(true);
    } else {
      if (timer) {
        clearTimeout(timer);
      }
      timer = setTimeout(() => {
        __DEV__ && alert(`Bar code with type ${type} and data ${data} has been scanned!`);
      }, 1000);
    }
  };

  if (hasPermission === null) {
    return (
      <View style={Classes.fillCenter}>
        <Text>{t('permission_request_camera')}</Text>
      </View>
    );
  }
  if (hasPermission === false) {
    return (
      <View style={Classes.fillCenter}>
        <BaseIconButton
          iconType="FontAwesome5"
          iconName="times"
          iconSize={Platform.isPad ? Screen.scale(10) : Screen.scale(24)}
          style={styles.btnCancel}
          onPress={() => Actions.pop()}
        />
        <Text style={Fonts.style.h2}>{t('permission_no_camera_title')}</Text>
        <Text style={[Classes.margin, Classes.center, Fonts.style.h7]}>
          {t('permission_request_desc_camera')}
        </Text>
        <RoundButton
          style={Classes.marginTop}
          text={t('permission_request_open_setting')}
          onPress={Permissions.openSettings}
        />
      </View>
    );
  }
  return (
    <View style={styles.containerColumn}>
      <View style={styles.imageWrapper}>
        <Image source={Images.scanQrcodeIcon} style={styles.imageMain} />
      </View>
      <View style={styles.containerRow}>
        <Camera
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          barCodeScannerSettings={{
            barCodeTypes: [BarCodeScanner.Constants.BarCodeType.qr],
          }}
          style={styles.fixedRatio}
        >
          <View style={styles.fixedRatioBoard} />
        </Camera>
      </View>
      <View style={{ alignItems: 'center' }}>
        <Text style={styles.qrcodeDescriptionText}>{t('1-4-1_qrcode_description')}</Text>
      </View>

      <View style={[Classes.margin, Classes.mainEnd, Classes.marginBottom]}>
        <SquareButton
          text={t('__cancel')}
          onPress={Actions.pop}
          color={Colors.deepBlack}
          textColor={Colors.white}
        />
      </View>
    </View>
  );
}
