import React from 'react';
import PropTypes from 'prop-types';
import Geolocation from '@react-native-community/geolocation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import { BaseModal } from 'App/Components';
import {
  UserActions,
  MdnsActions,
  MirrorActions,
  WebsocketActions,
  AppStateActions as AppActions,
} from 'App/Stores';
import {
  filterMirrorService,
  filterLastRecordMirrorService,
  filterInitializedMirrorService,
} from 'App/Stores/Mirror/Selectors';
import { Permission } from 'App/Helpers';

import MirrorDeviceView from '../MirrorSetup/MirrorDeviceView';

class DeviceModal extends React.Component {
  static propTypes = {
    routeName: PropTypes.string.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsClear: PropTypes.func.isRequired,
    mirrorServices: PropTypes.array.isRequired,
    mirrorInitializedServices: PropTypes.array.isRequired,
    currentNetworkInfo: PropTypes.object.isRequired,
    mirrorDiscoverInit: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mirrorConnect: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    lastDeviceInList: PropTypes.object,
    lastDevice: PropTypes.object,
    isPaired: PropTypes.bool.isRequired,
    mirrorCommunicate: PropTypes.func.isRequired,
    mirrorLog: PropTypes.func.isRequired,
    updateUserStore: PropTypes.func.isRequired,
  };

  state = {};

  _mounted = false;

  async componentDidMount() {
    __DEV__ && console.log('@Enter DeviceModal!');

    const hasPermission = await Permission.checkAndRequestPermission(
      Permission.permissionType.GEOLOCATION_LOW,
    );
    if (hasPermission) {
      Geolocation.getCurrentPosition(({ coords }) => {
        if (this._mounted) {
          const { longitude, latitude } = coords;
          const { updateUserStore } = this.props;
          updateUserStore({
            coordinate: {
              longitude,
              latitude,
            },
          });
        }
      });
    }
    this._mounted = true;
  }

  componentWillUnmount() {
    Geolocation.stopObserving();

    this._mounted = false;
  }

  handleSetupNewDevice = () => {
    const { onLoading } = this.props;
    onLoading(false);
    Actions.pop();
    Actions.MirrorSetupScreen();
  };

  handleConnectDevice = (device) => {
    const { mirrorConnect, onLoading } = this.props;
    onLoading(true);
    mirrorConnect(
      device,
      {
        maxRetries: 5,
      },
      () => {
        Actions.pop();
      },
    );
  };

  render() {
    const { mdnsStop } = this.props;
    return (
      <BaseModal
        title={t('mirror_device')}
        height="80%"
        showLoadingIndicator={false}
        onCloseModal={mdnsStop}
      >
        <MirrorDeviceView
          {...this.props}
          onPressDeviceItem={this.handleConnectDevice}
          onPressSetupNewDevice={this.handleSetupNewDevice}
        />
      </BaseModal>
    );
  }
}

export default connect(
  (state, params) => ({
    sceneKey: params.name,
    isLoading: state.appState.isLoading,
    routeName: state.appRoute.routeName,
    lastDevice: state.mirror.lastDevice,
    isPaired: state.mirror.isPaired,
    mdServices: state.mdns.services,
    connectedDevice: state.mirror.connectedDevice,
    lastDeviceInList: filterLastRecordMirrorService(state),
    mirrorServices: filterMirrorService(state),
    mirrorInitializedServices: filterInitializedMirrorService(state),
    currentNetworkInfo: state.appState.currentNetworkInfo,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateUserStore: UserActions.updateUserStore,
        onLoading: AppActions.onLoading,
        initConnection: WebsocketActions.initConnection,
        sendMessage: WebsocketActions.onSendMessage,
        onDeviceConnected: MirrorActions.onDeviceConnected,
        mirrorLog: MirrorActions.onMirrorLog,
        mirrorConnect: MirrorActions.onMirrorConnect,
        mirrorDiscover: MdnsActions.onMdnsScan,
        mirrorDiscoverInit: MdnsActions.onMdnsInit,
        mdnsClear: MdnsActions.onMdnsScanClear,
        mdnsStop: MdnsActions.onMdnsStop,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(DeviceModal);
