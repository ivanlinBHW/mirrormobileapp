import { Platform } from 'react-native';
import { StyleSheet, isIphoneX, Screen } from 'App/Helpers';
import { Fonts, Colors, Metrics } from 'App/Theme';

export default StyleSheet.create({
  leftTitle: {
    ...Fonts.style.medium500,
  },
  rightWrapper: {
    flexDirection: 'row',
    zIndex: 12222,
  },
  box: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  spIcon: {
    marginRight: Metrics.baseMargin,
  },
  rightText: {
    ...Fonts.style.small500,
    marginRight: Metrics.baseMargin / 2,
  },
  rightIcon: {
    justifyContent: 'flex-end',
  },
  stripStyle: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
    zIndex: 122,
    paddingLeft: Metrics.baseMargin,
    paddingRight: Metrics.baseMargin,
  },
  item: {
    height: '76@vs',
    marginLeft: '16@vs',
  },
  headTitle: {
    ...Fonts.style.medium500,
    lineHeight: '48@vs',
  },
  sliderbox: {
    flexDirection: 'row',
  },
  slider: {
    flex: 2,
    justifyContent: 'flex-start',
    marginTop: '10@vs',
    marginLeft: Metrics.baseMargin / 2,
    marginRight: Metrics.baseMargin,
  },
  itemBox: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
    paddingBottom: '16@vs',
  },
  img: {
    width: '10@s',
    height: '16@vs',
    ...Platform.select({
      ios: {
        marginTop: Platform.isPad ? 4 : 2,
      },
      android: {
        marginTop: 2,
      },
    }),
  },
  voiceImg: {
    width: '20@s',
    height: '20@vs',
  },
  marginTop: '1@vs',
  thumbStyle: {
    width: '32@sr',
    height: '32@sr',
    borderRadius: '16@sr',
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        marginTop: Platform.isPad ? -Metrics.baseMargin / 4 : '-2@sr',
        shadowColor: Colors.black5,
        shadowOffset: { height: '0@vs', width: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
      },
      android: {
        marginTop: '-4@sr',
        borderColor: Colors.shadow,
        borderWidth: Screen.onePixel,
        elevation: 2,
      },
    }),

    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    padding: 0,
    ...Platform.select({
      ios: {
        top: Platform.isPad ? '-14@sr' : isIphoneX ? '-14@sr' : '-12@sr',
      },
      android: {
        top: '-14@sr',
      },
    }),
  },
  wrapperBox: {
    zIndex: 12,
    flex: 1,
  },
});
