import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, ScrollView } from 'react-native';

import { BaseModal, BaseSearchButton } from 'App/Components';
import { Metrics, Colors, Images, Classes } from 'App/Theme';
import { SeeAllActions, SearchActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { StyleSheet, Screen } from 'App/Helpers';

let styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.gray_01,
  },
  model: {
    padding: 16,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {},
  imgStyle: {
    width: '19@s',
    height: '32@s',
  },
  scrollContainer: {
    alignSelf: 'stretch',
  },
});

const countSelected = (prop) => {
  const { isCompleted, isBookmarked, isMotionCapture } = prop;

  const countSelectedTags = [isCompleted, isBookmarked, isMotionCapture].reduce(
    (prev, curr) => (curr ? prev + 1 : prev),
    0,
  );

  return countSelectedTags;
};

class FilterFilterTagsModal extends React.PureComponent {
  static propTypes = {
    searchData: PropTypes.object,
    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,
    resetList: PropTypes.func.isRequired,
    pageSize: PropTypes.number,
    searchableFilter: PropTypes.object.isRequired,
    filterLoading: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    const {
      isCompleted,
      isBookmarked,
      isMotionCapture,
      isNewArrival,
      isPopular,
    } = props.searchData;

    this.state = {
      isCompleted,
      isBookmarked,
      isMotionCapture,
      isNewArrival,
      isPopular,
      countSelectedTags: countSelected(props.searchData),
    };
  }

  _mounted = false;

  _updated = false;

  componentDidUpdate(prevProps, prevState) {
    const {
      isCompleted,
      isBookmarked,
      isMotionCapture,
      isNewArrival,
      isPopular,
    } = this.state;
    if (
      isCompleted !== prevState.isCompleted ||
      isBookmarked !== prevState.isBookmarked ||
      isMotionCapture !== prevState.isMotionCapture ||
      isNewArrival !== prevState.isNewArrival ||
      isPopular !== prevState.isPopular
    ) {
      this.setState({
        countSelectedTags: countSelected(this.state),
      });
      this._updated = true;
    }
  }

  componentWillUnmount() {
    this.onPressFinish();
  }

  onPressFilter() {
    const { searchData, searchFilter } = this.props;
    const { isCompleted, isBookmarked, isMotionCapture } = this.state;

    const payload = {
      ...searchData,
      isCompleted,
      isBookmarked,
      isMotionCapture,
    };
  }

  onPressFinish = () => {
    if (!this._updated) {
      return false;
    }
    const { searchData, resetList, search, searchFilter, pageSize } = this.props;
    const {
      isCompleted,
      isBookmarked,
      isMotionCapture,
      isNewArrival,
      isPopular,
    } = this.state;

    const payload = {
      ...searchData,
      isCompleted,
      isBookmarked,
      isMotionCapture,
      isNewArrival,
      isPopular,
    };

    console.log('search payload ->', payload);
    resetList();
    search(payload, {
      page: 1,
      pageSize: pageSize,
      filter: [],
      type: 'page',
    });
  };

  render() {
    const {
      isCompleted,
      isBookmarked,
      isMotionCapture,
      isPopular,
      isNewArrival,
      countSelectedTags,
    } = this.state;

    const { searchableFilter, filterLoading } = this.props;

    return (
      <BaseModal
        style={Classes.fillCenter}
        title={t('search_filter')}
        navRightComponent={{ onPress: this.onPressFinish, text: t('search_apply') }}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.model}>
            <Text style={styles.text}>{`${countSelectedTags} item(s) selected.`}</Text>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              <BaseSearchButton
                image={isCompleted ? Images.finsih_white : Images.finsih_black}
                text={t('search_completed')}
                isSelected={isCompleted}
                disabled={filterLoading}
                onPress={() => {
                  this.setState({ isCompleted: !isCompleted });
                  this.onPressFilter();
                }}
              />
              <BaseSearchButton
                image={isBookmarked ? Images.bookmark : Images.bookmark_black}
                text={t('search_bookmarked')}
                isSelected={isBookmarked}
                imageStyle={styles.imgStyle}
                disabled={filterLoading}
                onPress={() => {
                  this.setState({ isBookmarked: !isBookmarked });
                  this.onPressFilter();
                }}
              />
              <BaseSearchButton
                iconName="star"
                iconSize={Screen.scale(24)}
                iconColor={isNewArrival ? Colors.white : Colors.gray_01}
                iconStyle={{ marginVertical: Metrics.baseMargin / 2 }}
                text={t('search_new_arrival')}
                isSelected={isNewArrival}
                disabled={filterLoading}
                onPress={() => {
                  this.setState({ isNewArrival: !isNewArrival });
                  this.onPressFilter();
                }}
              />
              <BaseSearchButton
                iconName="thumbs-up"
                iconSize={Screen.scale(24)}
                iconColor={isPopular ? Colors.white : Colors.gray_01}
                iconActiveColor={Colors.white}
                iconStyle={{ marginVertical: Metrics.baseMargin / 2 }}
                text={t('search_popular_class')}
                isSelected={isPopular}
                disabled={filterLoading}
                onPress={() => {
                  this.setState({ isPopular: !isPopular });
                  this.onPressFilter();
                }}
              />
              {/* <BaseSearchButton
                image={
                  isMotionCapture ? Images.motion_capture_white : Images.motion_capture
                }
                text={t('motion_capture')}
                isSelected={isMotionCapture}
                disabled={filterLoading}
                onPress={() => {
                  this.setState({ isMotionCapture: !isMotionCapture });
                  this.onPressFilter();
                }}
              /> */}
            </View>
          </View>
        </ScrollView>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    searchData: state.search.searchData,
    searchableFilter: state.seeAll.filter,
    pageSize: state.seeAll.pageSize,
    filterLoading: state.seeAll.filterLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        search: SearchActions.search,
        resetList: SeeAllActions.resetList,
        searchFilter: SearchActions.searchFilter,
      },
      dispatch,
    ),
)(FilterFilterTagsModal);
