import { Platform } from 'react-native';
import { StyleSheet } from 'App/Helpers';
import { Metrics, Fonts } from 'App/Theme';
export default StyleSheet.create({
  itemBox: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
  },
  flexBox: {
    flexDirection: 'row',
    height: '48@vs',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: '14@vs',
    letterSpacing: 1,
    lineHeight: '48@vs',
    marginLeft: '16@vs',
  },
  switch: {
    alignItems: 'flex-end',
  },
  flex: {
    flexDirection: 'row',
    paddingTop: '8@vs',
    paddingBottom: '8@vs',
  },
  image: {
    width: Metrics.baseMargin * 2,
    height: Metrics.baseMargin * 2,
    marginLeft: Metrics.baseMargin,
  },
  text: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
    marginVertical: '7@vs',
  },
  chevron: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: '18@s',
  },
  listBox: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
  },
  centerItem: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rightItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: '12@s',
  },
  sizeItemText: {
    fontSize: Fonts.size.medium,
    marginLeft: Metrics.baseMargin,
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  itemText: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
    marginVertical: Metrics.baseMargin,
  },

  badge: {
    width: '12@vs',
    height: '12@vs',
    marginRight: Metrics.baseMargin,
    borderRadius: '12@vs',
  },
  header: {
    paddingLeft: '16@vs',
    fontSize: '14@vs',
    fontWeight: '500',
    letterSpacing: 1,
    lineHeight: '48@vs',
    backgroundColor: '#f8f8f8',
  },
  scrollIndex: {
    zIndex: 2,
  },
  accordionStyle: {
    paddingTop: 0,
    paddingBottom: 0,
    marginTop: 0,
    marginBottom: 0,
  },
});
