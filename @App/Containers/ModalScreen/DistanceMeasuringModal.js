import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Image, View, Text, Platform, BackHandler, Alert } from 'react-native';

import { Images, Colors, Classes, Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SquareButton, BaseSwitch, SecondaryNavbar } from 'App/Components';

import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';

import { PlayerActions, ClassActions, AppStateActions } from 'App/Stores';

import styles from './DistanceMeasuringModalStyle';

class DistanceMeasuringModal extends React.Component {
  static propTypes = {
    onLoading: PropTypes.func.isRequired,
    mirrorCommunicate: PropTypes.func.isRequired,
    onSuccess: PropTypes.func,
    getClassDetail: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    exitDistanceMeasuringModal: PropTypes.func.isRequired,
    routeStack: PropTypes.array.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter DistanceMeasuringModal!');

    if (Platform.OS === 'android') {
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => true);
    }

    this.props.onLoading(false);
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      this.backHandler.remove();
    }
  }
  handleMotionCaptureChange = (value) => {
    const { updatePlayerStore } = this.props;
    updatePlayerStore({ isMotionCaptureEnabled: value });
  };
  render() {
    const {
      mirrorCommunicate,
      onSuccess,
      data,
      routeStack,
      exitDistanceMeasuringModal,
    } = this.props;
    return (
      <View style={styles.container}>
        <SecondaryNavbar
          back
          onPressBack={async () => {
            if (routeStack[0] !== 'MiiSettingScreen') exitDistanceMeasuringModal(data.id);
          }}
          navTitle=""
        />
        <View style={styles.imageWrapper}>
          <Image source={Images.human} style={styles.imageMain} />
        </View>
        <View style={styles.txtWrapper}>
          <Text style={styles.txtHeader}>{t('modal_distance_measuring')}</Text>
          <Text style={styles.txtContent}>{t('modal_distance_measuring_desc')}</Text>
        </View>
        {/* <View style={[styles.switchContent]}>
          <View style={{ marginRight: 15 }}>
            <Image source={Images.motion_capture} style={styles.captureImg} />
          </View>
          <Text style={{ marginTop: 5 }}>{t('motion_capture')}</Text>
          <BaseSwitch
            style={{ marginLeft: 10 }}
            active={this.props.isMotionCaptureEnabled}
            onValueChange={this.handleMotionCaptureChange}
          />
        </View> */}
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: 0,
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'flex-end',
            }}
          >
            <SquareButton
              color={Colors.button.primary.content.background}
              text={t('3-0c_skip_distance_adjustment')}
              onPress={async () => {
                mirrorCommunicate(MirrorEvents.END_DISTANCE_MEASURING);
                if (typeof onSuccess === 'function') {
                  await onSuccess();
                }
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => ({
    isMotionCaptureEnabled:
      'isMotionCaptureEnabled' in state.player
        ? state.player.isMotionCaptureEnabled
        : false,
    data: state.class.detail,
    routeStack: state.appRoute.stack,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
        updatePlayerStore: PlayerActions.updatePlayerStore,
        getClassDetail: ClassActions.getClassDetail,
        exitDistanceMeasuringModal: ClassActions.exitDistanceMeasuringModal,
      },
      dispatch,
    ),
)(DistanceMeasuringModal);
