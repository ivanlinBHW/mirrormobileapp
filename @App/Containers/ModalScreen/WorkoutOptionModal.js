import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, FlatList, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Accordion from 'react-native-collapsible/Accordion';
import { Badge } from 'react-native-elements';
import { RoundButton } from '@ublocks-react-native/component';

import MirrorActions, { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import { translate as t } from 'App/Helpers/I18n';
import { BaseModal, BaseSwitch } from 'App/Components';
import styles from './WorkoutOptionModalStyle';

class WorkoutOptionModal extends React.PureComponent {
  static propTypes = {
    mirrorCommunicate: PropTypes.func.isRequired,
    options: PropTypes.object.isRequired,
    videoSize: PropTypes.string,
    appRoute: PropTypes.object,
  };

  static defaultProps = {
    videoSize: 'large',
  };

  constructor(props) {
    super(props);
    this.state = {
      activeSections: [],
      sections: [
        {
          key: 'video-size',
          title: t('workout_option_instructor_video_size'),
          data: [
            {
              title: t('workout_option_dynamic'),
              key: 'dynamic',
              active: false,
            },
            {
              title: t('workout_option_large'),
              key: 'large',
              active: false,
            },
            {
              title: t('workout_option_small'),
              key: 'small',
              active: false,
            },
          ],
        },
      ],
    };
  }

  componentDidMount() {
    const { mirrorCommunicate, videoSize } = this.props;
    mirrorCommunicate(MirrorEvents.GET_WORKOUT_SETTING);
    if (videoSize) {
      let temp = JSON.parse(JSON.stringify(this.state.sections));
      temp[0].data.forEach((section, index) => {
        if (section.key === this.props.videoSize) {
          temp[0].data[index].active = true;
        } else {
          temp[0].data[index].active = false;
        }
      });
      this.setState({ sections: temp });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.videoSize !== this.props.videoSize) {
      let temp = JSON.parse(JSON.stringify(this.state.sections));
      temp[0].data.forEach((section, index) => {
        if (section.key === this.props.videoSize) {
          temp[0].data[index].active = true;
        } else {
          temp[0].data[index].active = false;
        }
      });
      this.setState({ sections: temp });
    }
  }

  onSwitchChange = (optionName) => () => {
    const { mirrorCommunicate, options } = this.props;
    mirrorCommunicate(MirrorEvents.SET_WORKOUT_SETTING, {
      optionName,
      value: !options[optionName],
    });
  };

  updateSections = (activeSections) => {
    this.setState({ activeSections });
  };

  updateVideoSizeOption = (item) => () => {
    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.SET_WORKOUT_SETTING, {
      optionName: MirrorOptions.VIDEO_SIZE,
      value: item.key,
    });
  };

  renderVideoSizeItem = ({ item }) => {
    return (
      <View style={styles.listBox}>
        <RoundButton
          style={styles.centerItem}
          onPress={this.updateVideoSizeOption(item)}
          transparent
        >
          <Text style={styles.sizeItemText}>{item.title}</Text>
          {item.active && <Badge status="success" badgeStyle={styles.badge} />}
        </RoundButton>
      </View>
    );
  };

  renderAccordionHeader = (section, index, isActive, sections) => {
    return (
      <View style={[styles.flex, styles.itemBox]}>
        <Text style={styles.text}>{section.title}</Text>
        <View style={styles.chevron}>
          {isActive ? (
            <Icon name="chevron-up" solid />
          ) : (
              <Icon name="chevron-down" solid />
            )}
        </View>
      </View>
    );
  };

  renderAccordionContent = (section) => {
    return <FlatList data={section.data} renderItem={this.renderVideoSizeItem} />;
  };

  render() {
    const { appRoute, options } = this.props;
    const { sections } = this.state;
    return (
      <BaseModal title={t('workout_option_workout_option')}>
        <ScrollView style={styles.scrollIndex}>
          <Text style={styles.header}>{t('workout_option_display')}</Text>
          <Accordion
            sections={sections}
            activeSections={this.state.activeSections}
            renderHeader={this.renderAccordionHeader}
            renderContent={this.renderAccordionContent}
            onChange={this.updateSections}
            containerStyle={styles.accordionStyle}
            underlayColor="white"
          />
          <Text style={styles.header}>{t('workout_option_metrics')}</Text>

          <View style={styles.listBox}>
            <Text style={styles.itemText}>{t('workout_option_heart_rate')}</Text>
            <View style={styles.rightItem}>
              <BaseSwitch
                active={options[MirrorOptions.HEART_RATE_DISPLAY]}
                onValueChange={this.onSwitchChange(MirrorOptions.HEART_RATE_DISPLAY)}
              />
            </View>
          </View>
          <View style={styles.listBox}>
            <Text style={styles.itemText}>{t('workout_option_calories')}</Text>
            <View style={styles.rightItem}>
              <BaseSwitch
                active={options[MirrorOptions.CALORIES_DISPLAY]}
                onValueChange={this.onSwitchChange(MirrorOptions.CALORIES_DISPLAY)}
              />
            </View>
          </View>
          <Text style={styles.header}>{t('workout_option_general')}</Text>

          <View style={styles.listBox}>
            <Text style={styles.itemText}>{t('workout_option_workout_name')}</Text>
            <View style={styles.rightItem}>
              <BaseSwitch
                active={options[MirrorOptions.EXERCISE_NAME_DISPLAY]}
                onValueChange={this.onSwitchChange(MirrorOptions.EXERCISE_NAME_DISPLAY)}
              />
            </View>
          </View>
          <View style={styles.listBox}>
            <Text style={styles.itemText}>{t('workout_option_timers')}</Text>
            <View style={styles.rightItem}>
              <BaseSwitch
                active={options[MirrorOptions.TIMERS_DISPLAY]}
                onValueChange={this.onSwitchChange(MirrorOptions.TIMERS_DISPLAY)}
              />
            </View>
          </View>
          <View style={styles.listBox}>
            <Text style={styles.itemText}>{t('workout_option_Community')}</Text>
            <View style={styles.rightItem}>
              <BaseSwitch
                active={options[MirrorOptions.COMMUNITY_DISPLAY]}
                onValueChange={this.onSwitchChange(MirrorOptions.COMMUNITY_DISPLAY)}
              />
            </View>
          </View>
        </ScrollView>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    appRoute: state.appRoute,
    isLoading: state.appState.isLoading,
    options: state.mirror.options,
    videoSize: state.mirror.options[MirrorOptions.VIDEO_SIZE],
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(WorkoutOptionModal);
