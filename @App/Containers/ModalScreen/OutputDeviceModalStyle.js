import { StyleSheet } from 'App/Helpers';
import { Metrics } from 'App/Theme';

export default StyleSheet.create({
  flatBox: {
    flexDirection: 'row',
    paddingTop: '8@vs',
    paddingBottom: '8@vs',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
  },
  icon: {
    marginRight: '40@vs',
    marginLeft: '16@vs',
  },
  title: {
    fontSize: '14@vs',
    letterSpacing: 1,
    lineHeight: '30@vs',
  },
  badge: {
    width: '12@s',
    height: '12@vs',
    borderRadius: '12@vs',
  },
  badgeBox: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    lineHeight: '30@vs',
    height: '30@vs',
    width: '30@s',
    marginRight: '17@vs',
  },
  header: {
    marginLeft: '16@vs',
    fontSize: '14@vs',
    marginTop: '8@vs',
  },
  iconbtn: {
    width: '20@vs',
    height: '20@s',
    lineHeight: '20@vs',
    marginRight: '13@vs',
    marginTop: '6@vs',
    alignSelf: 'center',
  },
  headerBox: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: '13@s',
  },
  button: {
    width: '20@s',
    height: '20@vs',
  },
  imgStyle: {
    width: '20@s',
    height: '20@vs',
    marginTop: '6@vs',
  },
  wrapper: {
    flex: 1,
  },
  img: {
    width: '32@s',
    height: '32@vs',
    marginLeft: Metrics.baseMargin,
    marginRight: '40@s',
  },
});
