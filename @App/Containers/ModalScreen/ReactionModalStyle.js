import { StyleSheet, isIphoneX } from 'App/Helpers';
import { Metrics } from 'App/Theme';
import { Platform } from 'react-native';

export default StyleSheet.create({
  flatBox: {
    flexDirection: 'row',
    paddingTop: Metrics.baseMargin / 2,
    paddingBottom: Metrics.baseMargin / 2,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
  },
  icon: {
    marginRight: 40,
    marginLeft: Metrics.baseMargin,
  },
  title: {
    fontSize: 14,
    letterSpacing: 1,
    lineHeight: 30,
  },
  badge: {
    width: 12,
    height: 12,
    borderRadius: 12,
  },
  badgeBox: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    lineHeight: 30,
    height: 30,
    width: 30,
    marginRight: 17,
  },
  itemBox: {
    flexWrap: 'wrap',
    flex: 1,
    height: Platform.isPad ? '280@sr' : '248@sr',
    flexDirection: 'row',
    marginLeft: Metrics.baseMargin * 2,
    marginTop: Metrics.baseMargin,
  },
  itemStyle: {
    ...Platform.select({
      ios: {
        width: Platform.isPad ? '58@sr' : isIphoneX() ? '58@sr' : '50@sr',
        height: Platform.isPad ? '58@sr' : isIphoneX() ? '58@sr' : '50@sr',
      },
      android: {
        width: '50@sr',
        height: '50@sr',
      },
    }),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: Metrics.baseMargin,
  },
  itemMargin: {
    marginRight: Platform.isPad ? '64@sr' : isIphoneX() ? '36@sr' : '49@sr',
  },
  imgStyle: {
    ...Platform.select({
      ios: {
        width: Platform.isPad ? '58@sr' : isIphoneX() ? '58@sr' : '50@sr',
        height: Platform.isPad ? '58@sr' : isIphoneX() ? '58@sr' : '50@sr',
      },
      android: {
        width: '50@sr',
        height: '50@sr',
      },
    }),
  },
  dotStyle: {
    width: '7@s',
    height: '7@s',
    borderRadius: 5,
    marginHorizontal: 4.5,
    backgroundColor: 'black',
    marginBottom: Metrics.baseMargin / 2,
  },
  carouselBox: {
    ...Platform.select({
      ios: {
        height: Platform.isPad ? '305@sr' : isIphoneX() ? '330@sr' : '290@sr',
      },
      android: {
        height: '275@sr',
      },
    }),
    justifyContent: 'center',
  },
  paginationStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'relative',
    top: -20,
  },
});
