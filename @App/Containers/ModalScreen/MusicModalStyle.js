import { Platform } from 'react-native';
import { StyleSheet, ifIphoneX } from 'App/Helpers';
import { Fonts, Colors, Metrics } from 'App/Theme';

export default StyleSheet.create({
  itemBox: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
  },
  flexBox: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: Metrics.baseMargin / 2,
  },
  badge: {
    width: '12@s',
    height: '12@s',
    borderRadius: '6@s',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  badgeBox: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: -Metrics.baseMargin * 1.5,
  },
  mirrorBox: {
  },
  sourceWrapper: {
    width: '55@s',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: Metrics.baseMargin / 4,
  },
  title: {
    ...Fonts.style.medium500,
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
  },
  switch: {
    marginRight: Metrics.baseMargin / 1.5,
    marginVertical: Metrics.baseMargin,
  },
  flex: {
    flexDirection: 'row',
  },
  miiBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image: {
    justifyContent: 'center',
    width: '32@s',
    height: '32@vs',
    marginLeft: Metrics.baseMargin,
  },
  rightImage: {
    width: '14@s',
    height: '8@vs',
    marginTop: Metrics.baseMargin,
    marginRight: '6@s',
  },
  text: {
    ...Fonts.style.medium500,
    marginLeft: '40@vs',
    marginVertical: Metrics.baseMargin,
  },
  spotifyText: {
    ...Fonts.style.medium500,
    marginLeft: '40@vs',
  },
  textGray01: {
    ...Fonts.style.medium500,
    marginLeft: '40@vs',
    marginVertical: Metrics.baseMargin,
    color: Colors.gray_01,
  },
  spotifybox: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: Platform.select({
      ios: Platform.isPad
        ? Metrics.baseMargin * 1.6
        : ifIphoneX(Metrics.baseMargin * 1.5, Metrics.baseMargin * 1.4),
      android: Metrics.baseMargin * 1.5,
    }),
  },
  listBox: {
    flexDirection: 'row',
    height: '48@vs',
    paddingTop: '8@vs',
    paddingBottom: '8@vs',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
  },
  centerItem: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rightItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: Platform.select({
      ios: Platform.isPad
        ? Metrics.baseMargin * 1.4
        : ifIphoneX(Metrics.baseMargin * 1.5, Metrics.baseMargin * 1.4),
      android: Metrics.baseMargin * 1.5,
    }),
  },
  itemText: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
    lineHeight: '32@vs',
  },
  miibadge: {
    width: '12@vs',
    height: '12@vs',
    borderRadius: '6@vs',
    alignSelf: 'center',
    marginRight: Platform.isPad ? Metrics.baseMargin * 3 : Metrics.baseMargin * 1.5,
    marginTop: Platform.isPad ? '30@vs' : '20@vs',
  },
  spotify: {
    paddingLeft: '19@s',
    marginVertical: '11@vs',
  },
  miiImage: {
    width: '32@s',
    height: '32@s',
    marginLeft: Metrics.baseMargin,
    marginVertical: Metrics.baseMargin / 2,
  },
});
