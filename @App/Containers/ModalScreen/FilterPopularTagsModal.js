import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView } from 'react-native';
import { isArray, get } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { BaseModal, TagsButton, SelectedTagsButton } from 'App/Components';
import { SeeAllActions, SearchActions } from 'App/Stores';
import { Metrics, Classes, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { StyleSheet } from 'App/Helpers';

let styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.gray_01,
  },
  model: {
    padding: 16,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {},
  scrollContainer: {
    alignSelf: 'stretch',
  },
});

class FilterPopularTagsModal extends React.PureComponent {
  static propTypes = {
    popularTagList: PropTypes.array,
    searchData: PropTypes.object,
    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,
    resetList: PropTypes.func.isRequired,
    pageSize: PropTypes.number,
    searchableFilter: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    const tagList = get(props, 'searchableFilter.tags', []).map((tag, index) => ({
      name: tag,
      tagId: index,
    }));
    const popularTagList = tagList.map((tag) => ({
      ...tag,
      selected: props.searchData.tags.includes(tag.name),
    }));
    const countSelectedTags = isArray(popularTagList)
      ? popularTagList.filter((e) => e.selected).length
      : 0;

    this.state = {
      popularTagList,
      countSelectedTags,
    };
  }

  _mounted = false;

  _updated = false;

  componentDidUpdate(prevProps, prevState) {
    this._updated = this.state.countSelectedTags !== prevState.countSelectedTags;
  }

  componentWillUnmount() {
    this.onPressFinish();
  }

  getSelectedTags = () => {
    return this.state.popularTagList.filter((e) => e.selected);
  };

  onPressPopularTag = (tagId) => {
    const { popularTagList } = this.state;
    this.setState({
      popularTagList: popularTagList.map((e) => ({
        ...e,
        selected: e.tagId === tagId ? !e.selected : e.selected,
      })),
    });

    this.setState({
      countSelectedTags: this.getSelectedTags().length,
    });
  };

  onPressFinish = () => {
    const { searchData, resetList, search, searchFilter, pageSize } = this.props;
    const { popularTagList = [] } = this.state;

    if (this._updated) {
      const payload = {
        ...searchData,
        tags: popularTagList.filter((e) => e.selected).map((e) => e.name),
      };

      console.log('search payload ->', payload);
      resetList();
      search(payload, {
        page: 1,
        pageSize: pageSize,
        filter: [],
        type: 'page',
      });
    }
  };

  render() {
    const { popularTagList, countSelectedTags } = this.state;

    return (
      <BaseModal
        style={Classes.fillCenter}
        title={t('search_popular_tags')}
        navRightComponent={{ onPress: this.onPressFinish, text: t('search_apply') }}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.model}>
            <Text style={styles.text}>{`${countSelectedTags} item(s) selected.`}</Text>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              {isArray(this.getSelectedTags()) &&
                this.getSelectedTags().map((item, index) => {
                  return (
                    <SelectedTagsButton
                      key={item.tagId}
                      text={item.name}
                      onPressCancelFilter={() => {
                        this.onPressPopularTag(item.tagId);
                      }}
                    />
                  );
                })}
            </View>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              {isArray(popularTagList) &&
                popularTagList.map((item, index) => {
                  return (
                    <TagsButton
                      key={item.tagId}
                      size="normal"
                      text={item.name}
                      isSelected={item.selected}
                      onPress={() => this.onPressPopularTag(item.tagId)}
                    />
                  );
                })}
            </View>
          </View>
        </ScrollView>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    popularTagList: state.search.popularTagList,
    searchData: state.search.searchData,
    searchableFilter: state.seeAll.filter,
    pageSize: state.seeAll.pageSize,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        search: SearchActions.search,
        resetList: SeeAllActions.resetList,
        searchFilter: SearchActions.searchFilter,
      },
      dispatch,
    ),
)(FilterPopularTagsModal);
