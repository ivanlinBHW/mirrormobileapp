import { ScaledSheet } from 'App/Helpers';
import { Metrics, Fonts, Colors, Classes } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.shadow,
    paddingHorizontal: Metrics.baseMargin * 2,
  },
  modelWrapper: {
    ...Classes.marginBottomHalf,
    shadowColor: Colors.black,
    shadowOpacity: 1,
    shadowRadius: 20,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    elevation: 20,

    borderWidth: '1@sr',
    borderColor: Colors.shadow_70,
    borderBottomColor: Colors.primary,
    borderTopLeftRadius: Metrics.baseMargin / 4,
    borderTopRightRadius: Metrics.baseMargin / 4,
    maxHeight: '70%',
    height: '50%',
    width: '100%',
    flex: 1,
    backgroundColor: Colors.white,
  },
  headerWrapper: {
    ...Classes.paddingHorizontal,
    ...Classes.crossStart,
    ...Classes.marginTopHalf,
  },
  htmlWrapper: {
    ...Classes.paddingHorizontal,
    ...Classes.marginVertical,
    ...Classes.fill,
  },
  footerWrapper: {
    marginBottom: '-1@sr',
    ...Classes.rowMain,
    ...Classes.crossEnd,
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: -Metrics.baseMargin,
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    justifyContent: 'center',
    color: Colors.gray_01,
  },
});
