import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { translate as t } from 'App/Helpers/I18n';
import { BaseModal, SelectedTagsButton, BaseAvatar } from 'App/Components';
import { View, Text, ScrollView, Platform } from 'react-native';
import { StyleSheet } from 'App/Helpers';
import { Metrics, Colors, Fonts } from 'App/Theme';

import { isArray, get } from 'lodash';
import { Classes } from 'App/Theme';
import { RoundButton } from '@ublocks-react-native/component';
import { SeeAllActions, SearchActions } from 'App/Stores';

let styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.gray_01,
  },
  model: {
    padding: 16,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {},
  scrollContainer: {
    alignSelf: 'stretch',
  },
  instructorBox: {
    width: Platform.isPad ? '98@s' : '83@s',
    height: '87@vs',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.transparent,
    marginLeft: Metrics.baseMargin,
    marginTop: '10@vs',
    borderWidth: 0,
  },
  avatar: {
    width: '49@s',
    height: '49@s',
    borderRadius: '24.5@s',
    marginBottom: Metrics.baseMargin / 2,
  },
  instructorText: {
    fontSize: Fonts.size.small,
    textAlign: 'center',
    flexWrap: 'wrap',
  },
  activeText: {
    color: Colors.primary,
  },
  contentAvatar: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentText: {
    flex: 1,
    marginTop: '10@vs',
    flexDirection: 'row',
  },
});

class FilterInstructorTagsModal extends React.PureComponent {
  static propTypes = {
    instructorList: PropTypes.array,
    searchData: PropTypes.object,
    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,
    resetList: PropTypes.func.isRequired,
    pageSize: PropTypes.number,
    searchableFilter: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    const instructorList = get(props, 'instructorList', [])
      .filter((instructor) =>
        get(props, 'searchableFilter.instructor', []).includes(instructor.id),
      )
      .map((instructor) => ({
        ...instructor,
        selected: get(props, 'searchData.instructors', []).includes(instructor.id),
      }));

    const countSelectedTags = instructorList.filter((e) => e.selected).length;

    this.state = {
      instructorList,
      countSelectedTags,
    };
  }

  _mounted = false;

  _updated = false;

  componentDidUpdate(prevProps, prevState) {
    this._updated = this.state.countSelectedTags !== prevState.countSelectedTags;
  }

  componentWillUnmount() {
    this.onPressFinish();
  }

  getSelectedTags = () => {
    return this.state.instructorList.filter((e) => e.selected);
  };

  onPressInstructorTag = (id) => {
    const { instructorList } = this.state;
    this.setState({
      instructorList: instructorList.map((e) => ({
        ...e,
        selected: e.id === id ? !e.selected : e.selected,
      })),
    });

    this.setState({
      countSelectedTags: this.getSelectedTags().length,
    });
  };

  onPressFinish = () => {
    if (!this._updated) {
      return false;
    }
    const { searchData, resetList, search, searchFilter, pageSize } = this.props;
    const { instructorList = [] } = this.state;

    const payload = {
      ...searchData,
      instructors: instructorList.filter((e) => e.selected).map((e) => e.id),
    };

    resetList();
    search(payload, {
      page: 1,
      pageSize: pageSize,
      filter: [],
      type: 'page',
    });
  };

  render() {
    const { instructorList, showAll, countSelectedTags } = this.state;

    return (
      <BaseModal
        style={Classes.fillCenter}
        title={t('search_instructor')}
        navRightComponent={{ onPress: this.onPressFinish, text: t('search_apply') }}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.model}>
            <Text style={styles.text}>{`${countSelectedTags} item(s) selected.`}</Text>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              {isArray(this.getSelectedTags()) &&
                this.getSelectedTags().map((item, index) => {
                  console.log('SelectedTagsButton', item);
                  return (
                    <SelectedTagsButton
                      key={item.id}
                      text={item.title}
                      onPressCancelFilter={() => {
                        this.onPressInstructorTag(item.id);
                      }}
                    />
                  );
                })}
            </View>
          </View>
          <View style={styles.box}>
            {/* <View style={styles.titleBox}>
              <TouchableOpacity onPress={() => this.setState({ showAll: !showAll })}>
                <Text style={styles.rightText}>
                  {t(showAll ? 'search_viewless' : 'search_viewall')}
                </Text>
              </TouchableOpacity>
            </View> */}
            <View style={styles.flexBox}>
              {isArray(instructorList) &&
                instructorList.map((item, index) => {
                  return (
                    <RoundButton
                      onPress={() => this.onPressInstructorTag(item.id)}
                      key={index}
                      style={styles.instructorBox}
                      throttleTime={0}
                    >
                      <View style={styles.contentAvatar}>
                        <BaseAvatar
                          active={item.selected}
                          size={49}
                          uri={item.signedAvatarImageObj}
                          style={styles.avatar}
                          cache
                        />
                      </View>
                      <View style={styles.contentText}>
                        <Text
                          style={[
                            styles.instructorText,
                            item.selected && styles.activeText,
                          ]}
                          ellipsizeMode="tail"
                          numberOfLines={2}
                        >
                          {item.title}
                        </Text>
                      </View>
                    </RoundButton>
                  );
                })}
            </View>
          </View>
        </ScrollView>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    instructorList: state.search.instructorList,
    searchData: state.search.searchData,
    searchableFilter: state.seeAll.filter,
    pageSize: state.seeAll.pageSize,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        search: SearchActions.search,
        resetList: SeeAllActions.resetList,
        searchFilter: SearchActions.searchFilter,
      },
      dispatch,
    ),
)(FilterInstructorTagsModal);
