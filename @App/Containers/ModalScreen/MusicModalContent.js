import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Accordion from 'react-native-collapsible/Accordion';
import { Image, View, Text, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import { bindActionCreators } from 'redux';
import { Badge } from 'react-native-elements';
import { Spotify } from 'App/Helpers';

import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { SpotifyActions, MirrorActions } from 'App/Stores';
import { BaseSwitch } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Screen } from 'App/Helpers';
import { Colors, Classes, Images } from 'App/Theme';
import styles from './MusicModalStyle';

const SECTIONS = [
  {
    title: 'First',
    data: [
      {
        title: 'Playlist 01',
        isPlay: true,
      },
      {
        title: 'Playlist 02',
        isPlay: false,
      },
      {
        title: 'Playlist 03',
        isPlay: false,
      },
      {
        title: 'Playlist 04',
        isPlay: false,
      },
      {
        title: 'Playlist 05',
        isPlay: false,
      },
      {
        title: 'Playlist 06',
        isPlay: false,
      },
    ],
  },
];
class MusicModal extends React.Component {
  static propTypes = {
    shuffle: PropTypes.bool,
    onMirrorCommunicate: PropTypes.func.isRequired,

    spotifyAccessToken: PropTypes.string,
    spotifyIsLogin: PropTypes.bool.isRequired,
    spotifySession: PropTypes.object.isRequired,
    spotifyPlayLists: PropTypes.array.isRequired,
    onSpotifyUpdatePlayLists: PropTypes.func.isRequired,
    mode: PropTypes.string,
    activeList: PropTypes.object,
    detail: PropTypes.object,
  };

  static defaultProps = {
    shuffle: false,
    spotifyAccessToken: '',
    mode: 'playScreen',
    activeList: {
      data: '',
    },
  };

  state = {
    isShuffle: false,
    activeSections: [],
    expand: true,
  };

  async componentDidMount() {
    __DEV__ && console.log('@Enter MusicModal!');

    const { spotifyIsLogin, onMirrorCommunicate, spotifySession } = this.props;
    if (spotifyIsLogin) {
      await Spotify.initializeIfNeeded();

      await Spotify.getMyPlaylists(spotifySession.accessToken);
    }
  }

  onSwitchChange = (value) => {
    const { onMirrorCommunicate, shuffle } = this.props;
    onMirrorCommunicate(MirrorEvents.UPDATE_MUSIC_PLAY_SETTING, {
      optionName: 'shuffle-mode',
      value: !shuffle,
    });
  };

  onChangToPlaySpotifyMusic = (playlist) => () => {
    const {
      spotifyPlayLists,
      onMirrorCommunicate,
      onSpotifyUpdatePlayLists,
      spotifyAccessToken,
    } = this.props;
    onMirrorCommunicate(MirrorEvents.CHANGE_PLAYLIST, {
      type: 'spotify-music',
      playlist: {
        id: playlist.id,
        name: playlist.name,
        uri: playlist.uri,
        token: spotifyAccessToken,
      },
    });
    onSpotifyUpdatePlayLists(
      spotifyPlayLists.map((e) => ({ ...e, isPlaying: e.id === playlist.id })),
    );
  };

  onChangeToPlayInternalMusic = () => {
    this.setState({ expand: false });
    const {
      spotifyPlayLists,
      onMirrorCommunicate,
      onSpotifyUpdatePlayLists,
    } = this.props;
    onMirrorCommunicate(MirrorEvents.CHANGE_PLAYLIST, {
      type: 'mirror-music',
    });
    onSpotifyUpdatePlayLists(spotifyPlayLists.map((e) => ({ ...e, isPlaying: false })));
  };

  updateSections = async (activeSections) => {
    const { spotifySession } = this.props;
    if (spotifySession && spotifySession.accessToken) {
      await Spotify.getMyPlaylists(spotifySession.accessToken);
      this.setState({ expand: true, activeSections });
    } else {
      await Spotify.initializeIfNeeded();
      await Spotify.login();
    }
  };

  onPressSpotify = async (event) => {
    const { spotifyIsLogin } = this.props;
    if (!spotifyIsLogin) {
      await Spotify.login();
    }
  };

  renderAccordionHeader = (section, index, isActive, sections) => {
    const { spotifyIsLogin } = this.props;
    return (
      <TouchableOpacity
        style={[styles.flex, styles.itemBox, Classes.crossCenter]}
        onPress={this.onPressSpotify}
        disabled={spotifyIsLogin}
      >
        <View style={styles.sourceWrapper}>
          <Icon
            style={styles.spotify}
            name="spotify"
            size={Screen.scale(30)}
            color={spotifyIsLogin ? Colors.deepBlack : Colors.gray_02}
          />
        </View>
        <Text style={spotifyIsLogin ? styles.text : styles.textGray01}>
          {t('audio_spotify_music')}
        </Text>
        {spotifyIsLogin && (
          <View style={styles.spotifybox}>
            {isActive ? (
              <Icon name="chevron-up" size={14} solid />
            ) : (
              <Icon name="chevron-down" size={14} solid />
            )}
          </View>
        )}
      </TouchableOpacity>
    );
  };

  renderItem = ({ item, index }) => {
    console.log('=== item ===', item);
    return (
      <TouchableOpacity
        style={styles.listBox}
        key={index}
        onPress={this.onChangToPlaySpotifyMusic(item)}
      >
        <View style={Classes.rowCross}>
          <View style={Classes.rowCenter}>
            {item != null &&
              (item.images.length > 1 ? (
                <Image
                  source={{
                    uri: item.images[0].url,
                  }}
                  style={[styles.image]}
                />
              ) : (
                <Image style={[styles.image]} />
              ))}
          </View>
          <View style={Classes.crossStart}>
            <Text style={styles.spotifyText}>{item.name}</Text>
          </View>
        </View>

        <View style={styles.rightItem}>
          {item.isPlaying && <Badge status="success" badgeStyle={styles.badge} />}
        </View>
      </TouchableOpacity>
    );
  };

  renderAccordionContent = () => {
    const { spotifyPlayLists } = this.props;
    return <FlatList data={spotifyPlayLists} renderItem={this.renderItem} />;
  };

  render() {
    const { shuffle, mode, spotifyPlayLists } = this.props;
    const { expand } = this.state;

    const isPlayingSpotify = spotifyPlayLists.some((e) => e.isPlaying);
    return (
      <ScrollView style={Classes.fill}>
        <View style={styles.itemBox}>
          <View style={styles.flexBox}>
            <Text style={styles.title}>{t('music_shuffle_songs')}</Text>
            <BaseSwitch
              style={styles.switch}
              active={shuffle}
              onValueChange={this.onSwitchChange}
            />
          </View>
        </View>
        <View style={styles.itemBox}>
          <TouchableOpacity
            style={styles.miiBox}
            onPress={this.onChangeToPlayInternalMusic}
          >
            <View style={styles.sourceWrapper}>
              <Image source={Images.music} style={styles.miiImage} />
            </View>
            <Text style={styles.text}>{t('audio_mirror_music')}</Text>
            <View style={styles.badgeBox}>
              {/* {!isEmpty(activeList) &&
                has(activeList, 'data.type') &&
                isString(activeList.data.type) &&
                activeList.data.type === 'mirror-music' && (
                  <Badge status="success" badgeStyle={styles.miibadge} />
                )} */}
              {!isPlayingSpotify && (
                <Badge status="success" badgeStyle={styles.miibadge} />
              )}
            </View>
          </TouchableOpacity>
        </View>
        {mode !== 'livePlayScreen' && (
          <Accordion
            sections={SECTIONS}
            activeSections={expand ? this.state.activeSections : []}
            renderHeader={this.renderAccordionHeader}
            renderContent={this.renderAccordionContent}
            onChange={this.updateSections}
            underlayColor="white"
          />
        )}
      </ScrollView>
    );
  }
}

export default connect(
  (state) => ({
    appRoute: state.appRoute,
    isLoading: state.appState.isLoading,
    shuffle: state.mirror.shuffle,
    spotifyIsLogin: state.spotify.isLogin,
    spotifyPlayLists: state.spotify.playlists,
    spotifySession: state.spotify.currentSession,
    spotifyAccessToken: state.user.spotifyTokens.accessToken,
    activeList: state.mirror.activeList,
    detail: state.player.detail,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        onSpotifyUpdatePlayLists: SpotifyActions.onSpotifyUpdatePlayLists,
        resetActivityList: MirrorActions.resetActivityList,
      },
      dispatch,
    ),
)(MusicModal);
