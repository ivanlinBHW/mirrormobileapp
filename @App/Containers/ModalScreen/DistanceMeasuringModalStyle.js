import { ScaledSheet } from 'App/Helpers';
import { Metrics, Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    height: '100%',
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '60@vs',
  },
  imageMain: {
    width: '353@s',
    height: 380,
    resizeMode: 'cover',
  },
  txtWrapper: {
    marginTop: '20@vs',
    alignItems: 'center',
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: Metrics.baseMargin / 2,
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    textAlign: 'center',
    color: Colors.gray_01,
    marginHorizontal: Metrics.baseMargin,
  },
  switchContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
