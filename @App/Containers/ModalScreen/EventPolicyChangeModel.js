import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Accordion from 'react-native-collapsible/Accordion';
import { Badge } from 'react-native-elements';
import { RoundButton } from '@ublocks-react-native/component';

import SettingActions from 'App/Stores/Setting/Actions';
import { translate as t } from 'App/Helpers/I18n';
import { BaseModal, BaseSwitch, BouncyCheckbox, SquareButton } from 'App/Components';
import { Platform } from 'react-native';
import { StyleSheet } from 'App/Helpers';
import { Metrics, Fonts, Colors } from 'App/Theme';

let styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    color: '#545454',
  },
  model: {
    padding: 16,
  },
});

class EventPolicyChangeModel extends React.PureComponent {
  static propTypes = {
    updateSetting: PropTypes.func.isRequired,
    privacy: PropTypes.number,
    finishCallback: PropTypes.func,
    acceptCreateEventPolicy: PropTypes.boolean,
  };

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      acceptCreateEventPolicy: false,
    };
  }

  componentDidMount() {
    console.log(
      '=== this.props.acceptCreateEventPolicy ===',
      this.props.acceptCreateEventPolicy,
    );
    this.setState({
      acceptCreateEventPolicy: this.props.acceptCreateEventPolicy,
    });
  }

  componentDidUpdate(prevProps) {}

  render() {
    const { appRoute, options, finishCallback } = this.props;
    const { sections, acceptCreateEventPolicy } = this.state;
    return (
      <BaseModal
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        title={t('4-3e1_change_policy_setting')}
      >
        <ScrollView style={styles.scrollIndex}>
          <View>
            <View style={styles.model}>
              <Text style={styles.text}>
                {t('4-3e1_change_policy_setting_content_1')}
              </Text>
              <Text style={styles.text} />
              <Text style={styles.text}>
                {t('4-3e1_change_policy_setting_content_2')}
              </Text>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: 0,
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'flex-end',
              padding: 12,
            }}
          >
            <BouncyCheckbox
              isChecked={this.state.acceptCreateEventPolicy}
              checkboxSize={20}
              onPress={() => {
                this.setState({
                  acceptCreateEventPolicy: !this.state.acceptCreateEventPolicy,
                });
              }}
            />
            <Text style={{ fontSize: 14 }}>{t('4-3e1_change_policy_setting_agree')}</Text>
          </TouchableOpacity>
          <SquareButton
            disabled={!this.state.acceptCreateEventPolicy}
            color={Colors.button.primary.content.background}
            onPress={() => {
              console.log(finishCallback);
              finishCallback(acceptCreateEventPolicy);
            }}
            text={t('4-3e1_change_policy_setting_agree')}
            style={{
              height: 44,
            }}
          />
        </View>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    privacy: state.setting.setting.settings.privacy,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateSetting: SettingActions.updateSetting,
      },
      dispatch,
    ),
)(EventPolicyChangeModel);
