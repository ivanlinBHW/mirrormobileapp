import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { translate as t } from 'App/Helpers/I18n';
import { BaseModal, BaseSearchButton } from 'App/Components';
import { View, Text, ScrollView } from 'react-native';
import { StyleSheet } from 'App/Helpers';
import { Metrics, Colors, Classes } from 'App/Theme';

import { isArray } from 'lodash';

import { SeeAllActions, SearchActions } from 'App/Stores';
import { get } from 'lodash';

let styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.gray_01,
  },
  model: {
    padding: 16,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {},
  scrollContainer: {
    alignSelf: 'stretch',
  },
});

const initialDurationList = [
  {
    text: 'search_duration_15',
    selected: false,
    key: [0, 900],
  },
  {
    text: 'search_duration_30',
    selected: false,
    key: [901, 1800],
  },
  {
    text: 'search_duration_45',
    selected: false,
    key: [1801, 2700],
  },
  {
    text: 'search_duration_60',
    selected: false,
    key: [2701, 3600],
  },
];

class FilterDurationTagsModal extends React.PureComponent {
  static propTypes = {
    durationList: PropTypes.array,
    searchData: PropTypes.object,
    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,
    resetList: PropTypes.func.isRequired,
    pageSize: PropTypes.number,
    searchableFilter: PropTypes.object.isRequired,
    filterLoading: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    const durationList = initialDurationList
      .map((duration) => {
        let inBetween = false;

        props.searchableFilter.duration.forEach((classDuration) => {
          if (classDuration > duration.key[0] && classDuration <= duration.key[1]) {
            inBetween = true;
          }
        });

        return {
          ...duration,
          disabled: !inBetween,
        };
      })
      .map((duration) => {
        let hasKey = false;
        props.searchData.duration.forEach((classDuration) => {
          if (classDuration[0] === duration.key[0]) {
            hasKey = true;
          }
        });
        return {
          ...duration,
          selected: hasKey,
        };
      });

    const countSelectedTags = durationList.filter((e) => e.selected).length;

    this.state = {
      durationList,
      countSelectedTags,
      initSearchData: props.searchData,
    };
  }

  _mounted = false;

  _updated = false;

  componentDidUpdate(prevProps, prevState) {
    this._updated = this.state !== prevState;

    if (prevProps.searchableFilter.duration !== this.props.searchableFilter.duration) {
      const newDurationList = get(prevState, 'durationList', []).map((duration) => {
        let inBetween = false;

        this.props.searchableFilter.duration.forEach((classDuration) => {
          if (classDuration > duration.key[0] && classDuration <= duration.key[1]) {
            inBetween = true;
          }
        });

        return {
          ...duration,
          disabled: !inBetween,
        };
      });

      this.setState({
        durationList: newDurationList,
      });
    }
  }

  componentWillUnmount() {
    this.onPressFinish();
  }

  getSelectedTags = () => {
    return this.state.durationList.filter((e) => e.selected);
  };

  onPressDuration = (key) => {
    const { durationList } = this.state;
    const { searchData, searchFilter, pageSize } = this.props;

    let temp = durationList;
    temp.forEach((item) => {
      if (item.key === key) {
        item.selected = !item.selected;
      }
    });

    this.setState({
      durationList: temp,
    });

    this.setState({
      countSelectedTags: this.getSelectedTags().length,
    });
    const payload = {
      ...searchData,
      duration: durationList.filter((e) => e.selected).map((e) => e.key),
    };
  };

  onPressFinish = () => {
    if (!this._updated) {
      return false;
    }
    const { searchData, resetList, search, pageSize, searchFilter } = this.props;
    const { durationList = [] } = this.state;

    const payload = {
      ...searchData,
      duration: durationList.filter((e) => e.selected).map((e) => e.key),
    };

    console.log('search payload ->', payload);
    resetList();
    search(
      payload,
      {
        page: 1,
        pageSize: pageSize,
        filter: [],
        type: 'page',
      },
      'seeAll',
    );
  };

  render() {
    const { durationList, countSelectedTags } = this.state;
    const { filterLoading } = this.props;
    return (
      <BaseModal
        style={Classes.fillCenter}
        title={t('search_duration')}
        navRightComponent={{ onPress: this.onPressFinish, text: t('search_apply') }}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.model}>
            <Text style={styles.text}>{`${countSelectedTags} item(s) selected.`}</Text>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              {isArray(durationList) &&
                durationList.map((item, index) => {
                  return (
                    <BaseSearchButton
                      key={item.key}
                      size="normal"
                      text={t(item.text)}
                      isSelected={item.selected}
                      onPress={() => this.onPressDuration(item.key)}
                      hasBorder
                      disabled={item.disabled || filterLoading}
                    />
                  );
                })}
            </View>
          </View>
        </ScrollView>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    searchData: state.search.searchData,
    searchableFilter: state.seeAll.filter,
    pageSize: state.seeAll.pageSize,
    filterLoading: state.seeAll.filterLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        search: SearchActions.search,
        resetList: SeeAllActions.resetList,
        setClassList: SeeAllActions.setClassList,
        searchFilter: SearchActions.searchFilter,
      },
      dispatch,
    ),
)(FilterDurationTagsModal);
