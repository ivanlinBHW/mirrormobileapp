import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { ScrollView, View, Text } from 'react-native';
import HTML from 'react-native-render-html';

import { AppBulletinActions } from 'App/Stores';
import { Metrics, Classes } from 'App/Theme';
import { Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { SquareButton, AndroidBackKey } from 'App/Components';

import styles from './BulletinModelStyle';

class BulletinModel extends React.Component {
  static propTypes = {
    content: PropTypes.string,
    version: PropTypes.string,
    skippedVersion: PropTypes.string,
    getBulletinContent: PropTypes.func.isRequired,
    setSkipBulletinVersion: PropTypes.func.isRequired,
  };

  static defaultProps = {
    content: undefined,
    version: undefined,
    skippedVersion: undefined,
  };

  state = {};

  CONTENT_WIDTH = Screen.width - Metrics.baseMargin * 2;

  componentDidMount() {
    __DEV__ && console.log('@Enter BulletinModel!');

    this.props.getBulletinContent();
  }

  handleSkipVersion = () => {
    const { setSkipBulletinVersion, version } = this.props;

    setSkipBulletinVersion(version);

    requestAnimationFrame(Actions.pop);
  };

  render() {
    const { content: html } = this.props;
    return (
      <View style={styles.container}>
        <AndroidBackKey hasBackToHandler={() => {}} />
        <View style={styles.modelWrapper}>
          <View style={styles.headerWrapper}>
            <Text style={styles.txtHeader}>{t('bulletin_title')}</Text>
          </View>

          <ScrollView style={styles.htmlWrapper}>
            <HTML source={{ html }} contentWidth={this.CONTENT_WIDTH} />
          </ScrollView>

          <View style={styles.footerWrapper}>
            <SquareButton
              borderOutline
              radius={0}
              style={Classes.fill}
              text={t('bulletin_skip_this_version')}
              onPress={this.handleSkipVersion}
            />
            <SquareButton
              radius={0}
              style={Classes.fill}
              text={t('__ok')}
              onPress={() => requestAnimationFrame(Actions.pop)}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => ({
    isInternetReachable: state.appState.currentNetworkInfo.isInternetReachable,
    connectionType: state.appState.currentNetworkInfo.type,
    netInfoDetails: state.appState.currentNetworkInfo.details,
    content: state.appBulletin.content,
    version: state.appBulletin.version,
    skippedVersion: state.appBulletin.skippedVersion,
  }),

  (dispatch) =>
    bindActionCreators(
      {
        getBulletinContent: AppBulletinActions.getBulletinContent,
        setSkipBulletinVersion: AppBulletinActions.setSkipBulletinVersion,
      },
      dispatch,
    ),
)(BulletinModel);
