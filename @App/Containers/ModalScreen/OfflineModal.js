import React from 'react';
import PropTypes from 'prop-types';
import { has } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Image, View, Text } from 'react-native';

import { Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SquareButton, AndroidBackKey } from 'App/Components';

import styles from './OfflineModalStyle';

class OfflineModal extends React.Component {
  static propTypes = {
    description: PropTypes.string,
    activeRouteName: PropTypes.string,
    isInternetReachable: PropTypes.bool,
    connectionType: PropTypes.string,
    netInfoDetails: PropTypes.object,
  };

  static defaultProps = {
    description: t('modal_offline_desc'),
    netInfoDetails: { ssid: null },
    isInternetReachable: false,
    connectionType: '',
  };

  state = {};

  async componentDidMount() {
    __DEV__ && console.log('@Enter OfflineModal!');
  }

  render() {
    const {
      description,
      isInternetReachable,
      netInfoDetails = {},
      activeRouteName,
      connectionType,
    } = this.props;

    if (isInternetReachable && connectionType === 'wifi') {
      return Actions.pop();
    }
    return (
      <View style={styles.container}>
        <AndroidBackKey hasBackToHandler={() => {}} />
        <View style={styles.imageWrapper}>
          <Image source={Images.offline} style={styles.imageMain} />
        </View>
        <View style={styles.txtWrapper}>
          <Text style={styles.txtHeader}>{t('modal_offline')}</Text>
          <Text style={styles.txtContent}>{description}</Text>
          <Text style={styles.txtContent}>
            {has(netInfoDetails, 'ssid') ? `SSID: ${netInfoDetails.ssid}` : ''}
          </Text>
          <Text style={styles.txtContent}>
            {!!isInternetReachable ? '' : t('modal_offline_desc')}
          </Text>
        </View>
        <SquareButton
          borderOutline
          radius={6}
          textStyle={styles.loginStyle}
          text={t('__ok')}
          onPress={() => {
            if (activeRouteName != null && activeRouteName == 'PlayerScreen')
              Actions.ClassScreen();
            else Actions.pop();
          }}
          disabled={!isInternetReachable}
        />
      </View>
    );
  }
}

export default connect(
  (state) => ({
    isInternetReachable: state.appState.currentNetworkInfo.isInternetReachable,
    connectionType: state.appState.currentNetworkInfo.type,
    netInfoDetails: state.appState.currentNetworkInfo.details,
  }),

  (dispatch) => bindActionCreators({}, dispatch),
)(OfflineModal);
