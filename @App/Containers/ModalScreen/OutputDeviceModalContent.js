import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import { BluetoothAudioDevicePanel } from 'App/Components';
import MirrorActions, { MirrorOptions } from 'App/Stores/Mirror/Actions';
import styles from './OutputDeviceModalStyle';

class OutputDeviceModal extends React.Component {
  static propTypes = {
    isLoading: PropTypes.bool.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    outputOtherDevices: PropTypes.array,
    outputPairedDevices: PropTypes.array,
    outputDeviceType: PropTypes.string,
    targetOutputAudioDevice: PropTypes.object,
    routeName: PropTypes.string,
    isScanning: PropTypes.bool,
  };

  static defaultProps = {
    targetOutputAudioDevice: {},
    outputPairedDevices: [],
    outputOtherDevices: [],
    outputDeviceType: '',
    isScanning: false,
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter OutputDeviceModal!');
  }

  render() {
    const { isScanning } = this.props;
    return (
      <View style={styles.wrapper}>
        <BluetoothAudioDevicePanel isScanning={isScanning} {...this.props} />
      </View>
    );
  }
}

export default connect(
  (state) => ({
    isLoading: state.appState.isLoading,
    routeName: state.appRoute.routeName,
    outputPairedDevices: state.mirror.outputPairedDevices,
    outputOtherDevices: state.mirror.outputOtherDevices,
    outputDeviceType: state.mirror.options[MirrorOptions.OUTPUT_AUDIO_DEVICE],
    outputDeviceData: state.mirror.options[MirrorOptions.OUTPUT_AUDIO_DEVICE_DATA],
    activeDevice: state.mirror.options[MirrorOptions.OUTPUT_AUDIO_DEVICE],
    isScanning: state.bleDevice.isScanning,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        onMirrorChangeAudioOutputDevice: MirrorActions.onMirrorChangeAudioOutputDevice,
      },
      dispatch,
    ),
)(OutputDeviceModal);
