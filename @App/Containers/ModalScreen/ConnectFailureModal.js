import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Image, View, Text } from 'react-native';

import { Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SquareButton } from 'App/Components';

import styles from './ConnectFailureModalModalStyle';

class ConnectFailureModal extends React.Component {
  static propTypes = {};

  state = {};

  async componentDidMount() {
    __DEV__ && console.log('@Enter ConnectFailureModal!');
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageWrapper}>
          <Image source={Images.warning} style={styles.imageMain} />
        </View>
        <View style={styles.txtWrapper}>
          <Text style={styles.txtHeader}>{t('modal_connect_failure')}</Text>
          <Text style={styles.txtContent}>{t('modal_connect_failure_desc')}</Text>
        </View>
        <SquareButton
          borderOutline
          textStyle={styles.loginStyle}
          text={t('modal_connect_failure_button_retry')}
          radius={6}
          onPress={() => Actions.pop}
        />
      </View>
    );
  }
}

export default ConnectFailureModal;
