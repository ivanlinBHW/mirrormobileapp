import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { BaseModal, BleDevicePanel } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';

class HeartRateModal extends React.Component {
  static propTypes = {
    routeName: PropTypes.string.isRequired,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { shouldScanning } = prevState;
    const { routeName } = nextProps;
    if (routeName === 'HeartRateModal' && !shouldScanning) {
      return {
        shouldScanning: true,
      };
    }
    if (routeName !== 'HeartRateModal' && shouldScanning) {
      return {
        shouldScanning: false,
      };
    }
    return null;
  }

  state = {
    shouldScanning: true,
  };

  componentDidMount() {
    __DEV__ && console.log('@HeartRateModal');
  }

  render() {
    const { shouldScanning } = this.state;
    return (
      <BaseModal title={t('heart_rate_heart_rate_monitor')}>
        <BleDevicePanel scanning={shouldScanning} {...this.props} />
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    routeName: state.appRoute.routeName,
  }),
  (dispatch) => bindActionCreators({}, dispatch),
)(HeartRateModal);
