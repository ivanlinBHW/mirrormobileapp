import { ScaledSheet } from 'App/Helpers';
import { Metrics, Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    marginHorizontal: Metrics.baseMargin,
    height: '100%',
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '184@vs',
  },
  imageMain: {
    width: '60@s',
    height: '60@vs',
  },
  txtWrapper: {
    marginVertical: '40@vs',
    alignItems: 'center',
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: Metrics.baseMargin / 2,
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'center',
    color: Colors.gray_01,
  },
});
