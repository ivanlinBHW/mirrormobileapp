import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Text, ScrollView } from 'react-native';
import { isArray } from 'lodash';
import { bindActionCreators } from 'redux';

import { translate as t } from 'App/Helpers/I18n';
import { BaseModal, BaseSearchButton } from 'App/Components';
import { StyleSheet } from 'App/Helpers';
import { Metrics, Colors, Classes } from 'App/Theme';
import { SeeAllActions, SearchActions } from 'App/Stores';

let styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    color: Colors.gray_01,
  },
  model: {
    padding: 16,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {},
  scrollContainer: {
    alignSelf: 'stretch',
  },
});

const initialDifficultyList = [
  {
    text: 'search_lv1',
    selected: false,
    key: 1,
  },
  {
    text: 'search_lv2',
    selected: false,
    key: 2,
  },
  {
    text: 'search_lv3',
    selected: false,
    key: 3,
  },
  {
    text: 'search_lv4',
    selected: false,
    key: 4,
  },
];

class FilterDifficultyTagsModal extends React.PureComponent {
  static propTypes = {
    difficultyList: PropTypes.array,
    searchData: PropTypes.object,
    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,

    resetList: PropTypes.func.isRequired,
    pageSize: PropTypes.number,
    searchableFilter: PropTypes.object.isRequired,
    filterLoading: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    const difficultyList = initialDifficultyList
      .map((level) => ({
        ...level,
        disabled: !props.searchableFilter.level.includes(level.key),
      }))
      .map((level) => ({
        ...level,
        selected: props.searchData.level.includes(level.key),
      }));

    const countSelectedTags = difficultyList.filter((e) => e.selected).length;

    this.state = {
      difficultyList,
      countSelectedTags,
    };
  }

  _mounted = false;

  _updated = false;

  componentDidUpdate(prevProps, prevState) {
    this._updated = this.state !== prevState;

    console.log('this.props.searchableFilter', this.props.searchableFilter);

    if (prevProps.searchableFilter.level !== this.props.searchableFilter.level) {
      this.setState({
        difficultyList: prevState.difficultyList.map((level) => ({
          ...level,
          disabled: !this.props.searchableFilter.level.includes(level.key),
        })),
      });
    }
  }

  componentWillUnmount() {
    this.onPressFinish();
  }

  getSelectedTags = () => {
    return this.state.difficultyList.filter((e) => e.selected);
  };

  onPressLevel = (key) => {
    const { difficultyList } = this.state;
    const { searchData, searchFilter } = this.props;

    let temp = difficultyList;
    temp.forEach((item) => {
      if (item.key === key) {
        item.selected = !item.selected;
      }
    });
    this.setState({
      difficultyList: temp,
    });

    this.setState({
      countSelectedTags: this.getSelectedTags().length,
    });
    const payload = {
      ...searchData,
      level: difficultyList.filter((e) => e.selected).map((e) => e.key),
    };

    console.log('payload =>', payload);
  };

  onPressFinish = () => {
    if (!this._updated) {
      return false;
    }
    const { searchData, resetList, search, searchFilter, pageSize } = this.props;
    const { difficultyList = [] } = this.state;

    const payload = {
      ...searchData,
      level: difficultyList.filter((e) => e.selected).map((e) => e.key),
    };

    console.log('search payload ->', payload);
    resetList();
    search(payload, {
      page: 1,
      pageSize: pageSize,
      filter: [],
      type: 'page',
    });
  };

  render() {
    const { difficultyList, countSelectedTags } = this.state;
    const { filterLoading } = this.props;

    return (
      <BaseModal
        style={Classes.fillCenter}
        title={t('search_difficulty')}
        navRightComponent={{ onPress: this.onPressFinish, text: t('search_apply') }}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.model}>
            <Text style={styles.text}>{`${countSelectedTags} item(s) selected.`}</Text>
          </View>
          <View style={styles.box}>
            <View style={styles.flexBox}>
              {isArray(difficultyList) &&
                difficultyList.map((item, index) => {
                  return (
                    <BaseSearchButton
                      key={item.key}
                      size="normal"
                      text={t(item.text)}
                      isSelected={item.selected}
                      onPress={() => this.onPressLevel(item.key)}
                      hasBorder
                      disabled={item.disabled || filterLoading}
                    />
                  );
                })}
            </View>
          </View>
        </ScrollView>
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    searchData: state.search.searchData,
    searchableFilter: state.seeAll.filter,
    pageSize: state.seeAll.pageSize,
    filterLoading: state.seeAll.filterLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        search: SearchActions.search,
        resetList: SeeAllActions.resetList,
        searchFilter: SearchActions.searchFilter,
      },
      dispatch,
    ),
)(FilterDifficultyTagsModal);
