import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { View, Text, Platform } from 'react-native';
import { Badge } from 'react-native-elements';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { translate as t } from 'App/Helpers/I18n';
import { ImageButton, BaseModal } from 'App/Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { ClassActions, LiveActions } from 'App/Stores';
import styles from './ReactionModalStyle';
import { Images } from 'App/Theme';
import { Screen } from 'App/Helpers';

const select = 1;
const list = [
  [
    { img: Images.reaction_1, key: 'like' },
    { img: Images.reaction_2, key: 'angry' },
    { img: Images.reaction_3, key: 'sad' },
    { img: Images.reaction_4, key: 'lol' },
    { img: Images.reaction_5, key: 'happy' },
    { img: Images.reaction_6, key: 'handsome' },
    { img: Images.reaction_7, key: 'joy' },
    { img: Images.reaction_8, key: 'cry' },
    { img: Images.reaction_9, key: 'sleep' },
    { img: Images.reaction_10, key: 'drip' },
    { img: Images.reaction_11, key: 'fire' },
    { img: Images.reaction_12, key: 'rocket' },
    { img: Images.reaction_13, key: 'good' },
    { img: Images.reaction_14, key: 'rock' },
    { img: Images.reaction_15, key: 'clap' },
    { img: Images.reaction_16, key: 'strong' },
  ],
  [
    { img: Images.reaction_17, key: 'cake' },
    { img: Images.reaction_18, key: 'love' },
    { img: Images.reaction_19, key: 'messy' },
    { img: Images.reaction_20, key: 'die' },
    { img: Images.reaction_21, key: 'excited' },
    { img: Images.reaction_22, key: 'power' },
  ],
];
class ReactionModal extends React.Component {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
    sendClassReaction: PropTypes.func.isRequired,
    sendLiveReaction: PropTypes.func.isRequired,
    detail: PropTypes.object,
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter ReactionModal!');
  }

  state = {
    activeSlide: 0,
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.flatBox}>
        <Icon style={styles.icon} name={item.iconName} size={32} />
        <Text style={styles.title}>{item.title}</Text>
        {index === select && (
          <View style={styles.badgeBox}>
            <Badge status="success" badgeStyle={styles.badge} />
          </View>
        )}
      </View>
    );
  };

  onPressReaction = (item) => {
    const {
      onMirrorCommunicate,
      detail,
      sendClassReaction,
      sendLiveReaction,
    } = this.props;
    if (detail.scheduledAt) {
      console.log("=== onPressReaction ===", detail.id, item.key);
      sendLiveReaction(detail.id, item.key);
    } else {
      sendClassReaction(detail.id, item.key);
      
    }
    onMirrorCommunicate(MirrorEvents.SET_REACTION, {
      icon: item.key,
    });
  };

  _renderItem = ({ item }) => (
    <View style={styles.itemBox}>
      {item.map((reaction, indexs) => (
        <ImageButton
          key={`reaction_${indexs}`}
          style={[styles.itemStyle, indexs % 4 !== 3 && styles.itemMargin]}
          imageStyle={styles.imgStyle}
          image={reaction.img}
          onPress={() => this.onPressReaction(reaction)}
        />
      ))}
    </View>
  );

  pagination = () => {
    const { activeSlide } = this.state;
    return (
      <View style={styles.paginationStyle}>
        <Pagination
          dotsLength={2}
          activeDotIndex={activeSlide}
          dotStyle={styles.dotStyle}
          inactiveDotOpacity={0.4}
          inactiveDotScale={1}
        />
      </View>
    );
  };

  render() {
    return (
      <BaseModal
        title={t('reation_send_a_reaction')}
        height={Platform.isPad ? '58%' : '52%'}
      >
        <View style={styles.carouselBox}>
          <Carousel
            data={list}
            renderItem={this._renderItem}
            onSnapToItem={(index) => this.setState({ activeSlide: index })}
            sliderWidth={Screen.width - Screen.scale(32)}
            itemWidth={Screen.width - Screen.scale(32)}
            inactiveSlideScale={1}
            useScrollView
          />
        </View>
        {this.pagination()}
      </BaseModal>
    );
  }
}

export default connect(
  (state) => ({
    appRoute: state.appRoute,
    isLoading: state.appState.isLoading,
    detail: state.player.detail,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        sendClassReaction: ClassActions.sendClassReaction,
        sendLiveReaction: LiveActions.sendLiveReaction,
      },
      dispatch,
    ),
)(ReactionModal);
