import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, FlatList, View, Text, Image } from 'react-native';
import { bindActionCreators } from 'redux';

import { translate as t } from 'App/Helpers/I18n';
import {
  SecondaryNavbar,
  BaseButton,
  BaseInput,
  BaseStatusAvatar,
  FlatListEmptyLoading,
} from 'App/Components';
import { Images, Classes, Colors } from 'App/Theme';
import { Date as d } from 'App/Helpers';
import styles from './FriendSearchScreenStyle';
import { CommunityActions } from 'App/Stores/index';

class FriendSearchScreen extends React.Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    token: PropTypes.string,
    timezone: PropTypes.string.isRequired,
    getSearchFriends: PropTypes.func.isRequired,
    getSearchUser: PropTypes.func.isRequired,
    saveSelectedFriends: PropTypes.func.isRequired,
    getUserDetail: PropTypes.func.isRequired,
    selected: PropTypes.array,
    addFriend: PropTypes.func.isRequired,
    userId: PropTypes.string,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    selected: [],
    userId: '',
  };

  state = {
    text: '',
  };

  componentDidMount() {
    __DEV__ && console.log('@FriendSearchScreen');
    const { getSearchFriends } = this.props;
    getSearchFriends({
      text: '',
    });
  }

  onPressSearch = (value) => {
    const { getSearchUser, getSearchFriends } = this.props;
    this.setState({ text: value });
    if (value !== '') {
      getSearchUser(value);
    } else {
      getSearchFriends({
        text: '',
      });
    }
  };

  renderRightBtn = (item) => {
    const { text } = this.state;
    const { userId } = this.props;
    if (item.friendStatus === 2 && text !== '' && userId !== item.id) {
      return (
        <BaseButton
          transparent
          text={t('__friend')}
          style={[styles.baseRightBtn]}
          textStyle={styles.baseTextStyle}
          textColor={Colors.button.primary.outline.text}
        />
      );
    }
  };

  renderItem = ({ item, index }) => {
    const { getUserDetail } = this.props;
    const { lastOnlineMinuteAgo = 0 } = item;
    return (
      <BaseButton
        transparent
        style={styles.itemWrapper}
        onPress={() => getUserDetail(item.id)}
      >
        <View
          style={[
            styles.itemWrapper,
            { backgroundColor: item.isSelect ? Colors.white_02 : Colors.white },
          ]}
        >
          <View style={styles.leftItem}>
            <View style={styles.avatarBox}>
              <BaseStatusAvatar uri={item.avatar} isOnline={item.currentStatus === 1} />
            </View>
            <View style={styles.itemTitleBox}>
              <Text style={styles.itemTitle}>{item.fullName}</Text>
              <Text style={styles.itemSecondTitle}>
                {item.currentStatus === 1
                  ? 'online'
                  : d.transformLastOnlineDiff(lastOnlineMinuteAgo)}
              </Text>
            </View>
          </View>
          <View style={styles.rightItem}>{this.renderRightBtn(item)}</View>
        </View>
      </BaseButton>
    );
  };

  renderEmpty = () => {
    const { isLoading } = this.props;
    return (
      <FlatListEmptyLoading
        isLoading={isLoading}
        title={t('see_all_empty_title')}
        content={t('see_all_empty_content')}
      />
    );
  };

  render() {
    const { text } = this.state;
    const { list } = this.props;
    return (
      <SafeAreaView style={styles.wrapper}>
        <SecondaryNavbar back navTitle={`${t('__members')}`} />

        <View style={styles.searchWrapper}>
          <BaseInput
            leftComponent={<Image source={Images.search_gray} style={styles.searchImg} />}
            leftIconContainerStyle={styles.leftIconContainerStyle}
            inputContainerStyle={styles.inputContainerStyle}
            containerStyle={styles.inputStyle}
            placeholder={t('community_search_input_placeholder')}
            value={text}
            onChangeText={(value) => this.onPressSearch(value)}
          />
        </View>
        {text === '' && <Text style={styles.textStyle}>{t('community_friends')}</Text>}
        <View style={Classes.fill}>
          <FlatList
            data={list}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderEmpty}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    list: state.community.friends,
    selected: state.community.friendSelected,
    timezone: state.appState.currentTimeZone,
    userId: state.user.id,
    isLoading: state.appState.isLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getSearchFriends: CommunityActions.getSearchFriends,
        getSearchUser: CommunityActions.getSearchUser,
        saveSelectedFriends: CommunityActions.saveSelectedFriends,
        getUserDetail: CommunityActions.getUserDetail,
        addFriend: CommunityActions.addFriend,
      },
      dispatch,
    ),
)(FriendSearchScreen);
