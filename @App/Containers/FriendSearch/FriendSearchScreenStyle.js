import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Metrics, Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  wrapper: {
    flex: 1,
    height: '100%',
  },
  listBox: {
    height: '100%',
  },
  emptyBox: {
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Screen.scale(266),
  },
  emptyTitle: {
    fontSize: Screen.scale(24),
    color: Colors.black,
    textAlign: 'center',
    ...Fonts.style.fontWeight500,
  },
  emptyContent: {
    ...Fonts.style.small500,
    marginTop: '12@vs',
    color: Colors.gray_01,
    textAlign: 'center',
  },
  cardWrapper: {
    marginBottom: Metrics.baseMargin,
  },
  saveBtn: {
    width: '35@s',
    height: '19@s',
  },
  saveText: {
    fontSize: Fonts.size.regular,
    color: Colors.primary,
  },

  searchImg: {
    width: '20@s',
    height: '16@vs',
  },
  searchWrapper: {
    marginHorizontal: Metrics.baseMargin,
    marginBottom: '40@vs',
  },
  leftIconContainerStyle: {
    marginLeft: 0,
    position: 'relative',
    marginTop: Platform.select({
      ios: '8@s',
      android: '-6@s',
    }),
  },
  inputContainerStyle: {
    height: '22@vs',
    marginTop: Platform.select({
      ios: '-8@s',
      android: '8@s',
    }),
    justifyContent: 'center',
  },
  inputStyle: {
    backgroundColor: Colors.gray_12,
    height: '36@vs',
    borderRadius: '10@vs',
    justifyContent: 'center',
  },
  itemWrapper: {
    width: '100%',
    height: '72@s',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: Metrics.baseMargin,
  },
  avatarBox: {
    flexDirection: 'row',
    alignItems: 'center',
    height: '72@s',
  },
  leftItem: {
    flex: 1,
    height: '72@s',
    flexDirection: 'row',
  },
  rightItem: {
    width: '83@s',
  },
  itemTitleBox: {
    height: '72@s',
    flexDirection: 'column',
    marginLeft: Metrics.baseMargin,
    justifyContent: 'center',
  },
  itemTitle: {
    ...Fonts.style.medium500,
    marginBottom: Metrics.baseMargin / 2,
  },
  itemSecondTitle: {
    fontSize: Fonts.size.small,
    color: Colors.gray_02,
  },
  bottomWrapper: {
    width: '100%',
    height: '76@vs',
    position: 'relative',
    bottom: 0,
    backgroundColor: Colors.gray_04,
    flexDirection: 'row',
    alignItems: 'center',
  },
  editAvatarBox: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selectedWrapper: {
    width: '64@s',
    flexDirection: 'column',
    marginLeft: Metrics.baseMargin,
  },
  selectedText: {
    width: '64@s',
    textAlign: 'center',
    fontSize: Fonts.size.small,
  },
  baseRightBtn: {
    width: '83@s',
    height: '24@vs',
    borderWidth: 1,
    borderColor: Colors.button.primary.outline.border,
    borderRadius: '6@vs',
  },
  baseTextStyle: {
    fontSize: Fonts.size.small,
    color: Colors.button.primary.text.border,
  },
  textStyle: {
    ...Fonts.style.medium500,
    marginBottom: Metrics.baseMargin / 2,
    marginLeft: Metrics.baseMargin,
  },
});
