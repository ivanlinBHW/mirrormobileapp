import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, FlatList, View } from 'react-native';
import { bindActionCreators } from 'redux';

import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, CommunityCard } from 'App/Components';
import { CommunityActions } from 'App/Stores';
import { Classes } from 'App/Theme';
import styles from './MyEventScreenStyle';

class MyEventScreen extends React.Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    token: PropTypes.string,
    timezone: PropTypes.string.isRequired,
    getTrainingEvent: PropTypes.func.isRequired,
    navTitle: PropTypes.string,
  };

  static defaultProps = {
    navTitle: t('community_my_event'),
  };

  componentDidMount() {
    __DEV__ && console.log('@MyEventScreen');
  }

  renderItem = ({ item, index }) => {
    const { getTrainingEvent, timezone } = this.props;
    return (
      <View style={styles.cardWrapper}>
        <CommunityCard
          size="full"
          data={item}
          eventType={item.eventType}
          timezone={timezone}
          onPress={() => getTrainingEvent(item.id)}
        />
      </View>
    );
  };

  render() {
    const { list, navTitle } = this.props;
    return (
      <SafeAreaView style={styles.wrapper}>
        <SecondaryNavbar back navTitle={navTitle} />
        <View style={Classes.fill}>
          <FlatList data={list} renderItem={this.renderItem} />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    list: state.community.allMyEventList,
    timezone: state.appState.currentTimeZone,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getTrainingEvent: CommunityActions.getTrainingEvent,
      },
      dispatch,
    ),
)(MyEventScreen);
