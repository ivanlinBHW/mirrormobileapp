import { ScaledSheet } from 'App/Helpers';

export default ScaledSheet.create({
  cardListFooter: {
    paddingBottom: 128,
    marginBottom: '-16@vs',
  },
});
