import { AutoHideFlatList, BaseSearchButton, ClassTitleCard } from 'App/Components';
import { ClassActions, LiveActions, SearchActions, SeeAllActions } from 'App/Stores';
import { FlatList, SafeAreaView, Text, View } from 'react-native';
import { get, isEmpty } from 'lodash';
import { Fonts, Metrics, Colors } from 'App/Theme';
import PropTypes from 'prop-types';

import { Actions } from 'react-native-router-flux';
import { Classes } from 'App/Theme';
import React from 'react';
import { ScaledSheet } from 'App/Helpers';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { translate as t } from 'App/Helpers/I18n';

class ClassScreen extends React.Component {
  static propTypes = {
    appRoute: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    getClassList: PropTypes.func.isRequired,
    getClassDetail: PropTypes.func.isRequired,
    getLiveDetail: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    list: PropTypes.object.isRequired,
    filter: PropTypes.object,
    timezone: PropTypes.string,
    sceneKey: PropTypes.string,

    setClassBookmark: PropTypes.func.isRequired,
    removeClassBookmark: PropTypes.func.isRequired,
    setClassLiveBookmark: PropTypes.func.isRequired,
    removeClassLiveBookmark: PropTypes.func.isRequired,
    getSeeAllList: PropTypes.func.isRequired,
    getTrainingClassList: PropTypes.func.isRequired,
    workoutTypeList: PropTypes.array,

    search: PropTypes.func.isRequired,
    searchFilter: PropTypes.func.isRequired,
    pageSize: PropTypes.number,
    resetList: PropTypes.func.isRequired,
  };

  static defaultProps = {
    timezone: 'Asia/Taipei',
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.workoutTypeList.length !== prevState.workoutTypeList.length) {
      const workoutTypeList = nextProps.workoutTypeList.map((item) => ({
        ...item,
        selected: false,
      }));
      return {
        workoutTypeList,
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    const { list, workoutTypeList } = props;
    let count = 0;
    Object.values(list).forEach((item) => {
      if (item instanceof Array && item.length > 0) {
        count += 1;
      }
    });

    let workoutTypes = workoutTypeList.map((item) => ({ ...item, selected: false }));

    this.state = {
      titleName: {
        newArrival: t('class_new_arrival'),
        hottest: t('class_hottest_this_week'),
        recommended: t('class_recommended_for_you_this_week'),
        upcomingLive: t('class_upcomming_live_class'),

        bookmarked: t('class_bookmarked'),
        bookedLive: t('class_booked_live_classes'),
      },
      count,
      workoutTypeList: workoutTypes,
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@ClassScreen');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list !== this.props.list) {
      let count = 0;
      Object.values(nextProps.list).forEach((item) => {
        if (item instanceof Array && item.length > 0) {
          count += 1;
        }
      });
      this.setState({ count });
    }
  }

  onPressClassDetail = (id, data) => {
    const { user, getClassDetail, getLiveDetail } = this.props;
    if (data.scheduledAt) {
      getLiveDetail(id);
    } else {
      getClassDetail(id, user.token, 'ClassDetail');
    }
  };

  onPressBookmark = (id, item) => {
    const {
      setClassBookmark,
      removeClassBookmark,
      setClassLiveBookmark,
      removeClassLiveBookmark,
    } = this.props;

    if (item.scheduledAt) {
      console.log('live bookmark');
      if (item.isBookmarked) {
        removeClassLiveBookmark(id);
      } else {
        setClassLiveBookmark(id);
      }
    } else {
      if (item.isBookmarked) {
        console.log('class removeClassBookmark');
        removeClassBookmark(id);
      } else {
        console.log('class setClassBookmark');
        setClassBookmark(id);
      }
    }
  };

  getDataToSeeAll = (mode) => {
    Actions.SeeAllScreen({ loginRoute: 'seeAll', mode });
  };
  onPressGenre = (workoutType) => {
    const { search, pageSize, resetList, searchFilter } = this.props;

    resetList();

    const payload = {
      text: '',
      isCompleted: false,
      isBookmarked: false,
      isMotionCapture: false,
      equipments: [],
      channel: [],
      genre: [workoutType.id],
      tags: [],
      level: [],
      duration: [],
      instructors: [],
    };

    console.log('search payload ->', payload);
    console.log('search pageSize ->', pageSize);
    searchFilter(payload);
    search(
      payload,
      {
        page: 1,
        pageSize: pageSize,
        filter: [],
        type: 'page',
      },
      'seeAll',
    );

    Actions.SeeAllScreen({
      loginRoute: 'seeAll',
      title: workoutType.title,
      showFilter: true,
    });
  };
  renderGenre = (workoutTypeList) => {
    return (
      <View tyle={styles.boxs}>
        <View style={styles.titleBox}>
          <Text
            style={{
              ...Fonts.style.medium500,
              color: Colors.black,
            }}
          >
            {t('search_workout_type')}
          </Text>
        </View>

        <FlatList
          horizontal
          data={workoutTypeList}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => {
            return (
              <BaseSearchButton
                image={{
                  uri: item.selected
                    ? item.signedCoverImage || ''
                    : item.signedDeviceCoverImage || '',
                }}
                textStyle={Fonts.style.small500}
                text={item.title.toLowerCase()}
                isSelected={item.selected}
                key={item.id}
                onPress={() => this.onPressGenre(item)}
              />
            );
          }}
          keyExtractor={(item, i) => `class_title_${i}`}
        />
        <View style={styles.hrLine} />
      </View>
    );
  };
  renderItem = ({ item, index }) => {
    const { titleName, count, workoutTypeList } = this.state;
    const { timezone, list } = this.props;

    if (item.key == 'workoutType') {
      return this.renderGenre(workoutTypeList);
    } else {
      return (
        <ClassTitleCard
          size="large"
          index={index}
          type={item.key}
          data={item.items}
          timezone={timezone}
          title={titleName[item.key]}
          hrLine={index < count}
          onPressBookmark={this.onPressBookmark}
          getDataToSeeAll={this.getDataToSeeAll}
          showMore={(() => {
            switch (item.key) {
              case 'bookmarked':
                return get(list, 'bookmarked', []).length >= 6;
              case 'bookedLive':
                return get(list, 'bookedLive', []).length >= 6;
              case 'newArrival':
                return get(list, 'newArrival', []).length >= 6;
              default:
                return item.items.length >= 6;
            }
          })()}
          onPressClassDetail={this.onPressClassDetail}
        />
      );
    }
  };

  render() {
    const { list, getClassList, user, sceneKey } = this.props;

    if (list == null) {
      return <View />;
    }

    const data = Object.keys(list)
      .filter((e) => !e.includes('Count'))
      .map((e) => {
        if (e == 'workoutType') {
          return {
            items: list[e] || [],
            key: e,
          };
        }
        return {
          items: list[e] || [],
          key: e,
        };
      });
    return (
      <SafeAreaView style={Classes.fill}>
        <AutoHideFlatList
          data={data}
          showRefresh={true}
          sceneKey={sceneKey}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => `class_${index}`}
          onRefresh={() => getClassList(user.token)}
        />

        {/* <View style={{ paddingBottom: 50}} /> */}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    appRoute: state.appRoute,
    user: state.user,
    list: state.class.list,
    isLoading: state.appState.isLoading,
    timezone: state.appState.currentTimeZone,
    sceneKey: params.name,
    workoutTypeList: state.search.workoutTypeList,
    pageSize: state.seeAll.perPage,
    filter: state.seeAll.filter,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getClassList: ClassActions.getClassList,
        getLiveDetail: LiveActions.getLiveDetail,
        getSeeAllList: SeeAllActions.getSeeAllList,
        getTrainingClassList: SeeAllActions.getTrainingClassList,
        getClassDetail: ClassActions.getClassDetail,
        setClassBookmark: ClassActions.setClassBookmark,
        removeClassBookmark: ClassActions.removeClassBookmark,
        setClassLiveBookmark: ClassActions.setClassLiveBookmark,
        removeClassLiveBookmark: ClassActions.removeClassLiveBookmark,
        search: SearchActions.search,
        searchFilter: SearchActions.searchFilter,
        resetList: SeeAllActions.resetList,
      },
      dispatch,
    ),
)(ClassScreen);

const styles = ScaledSheet.create({
  title: {
    ...Fonts.style.medium500,
    color: '#323232',
  },

  hrLine: {
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
    borderBottomWidth: 1,
    marginTop: Metrics.baseMargin,
  },

  titleBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
});
