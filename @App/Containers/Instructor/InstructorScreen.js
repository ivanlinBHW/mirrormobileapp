import React from 'react';
import PropTypes from 'prop-types';
import Video from 'react-native-video';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isString, isArray, isEmpty } from 'lodash';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { FlatList, View, Text, ScrollView, ActivityIndicator } from 'react-native';

import {
  HeadLine,
  BaseReadMore,
  SecondaryNavbar,
  CachedImage as Image,
} from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { InstructorActions } from 'App/Stores';
import { Images, Classes } from 'App/Theme';
import { Screen } from 'App/Helpers';

import styles from './InstructorScreenStyle';
import ConnectFailureModalModalStyle from '../ModalScreen/ConnectFailureModalModalStyle';

class InstructorScreen extends React.Component {
  static propTypes = {
    token: PropTypes.string,
    instructor: PropTypes.object,
    getInstructor: PropTypes.func.isRequired,
    genres: PropTypes.array.isRequired,
  };

  static defaultProps = {
    instructor: {},
  };

  state = {
    entries: [
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
    ],
    activeSlide: 0,
  };

  componentWillMount() {
    __DEV__ && console.log('@InstructorScreen');
  }

  renderCarouselItem = ({ item }) => {
    const { isVideoLoading } = this.state;
    return item && item.type === 'video' ? (
      item.url && (
        <View style={Classes.fill}>
          <Video
            source={{ uri: item.url || '' }}
            ref={(ref) => {
              this.player = ref;
            }}
            onLoadStart={() =>
              this.setState({
                isVideoLoading: true,
              })
            }
            onLoad={() =>
              this.setState({
                isVideoLoading: false,
              })
            }
            style={Classes.fill}
            controls={false}
            allowsExternalPlayback={false}
          />
          <View style={styles.instructorVideoLoadingIndicator}>
            <ActivityIndicator animating={isVideoLoading} size="large" color="black" />
          </View>
        </View>
      )
    ) : (
      <View style={styles.slide}>
        <Image
          source={{ uri: item }}
          style={styles.img}
          resizeMode="contain"
          imageSize="2x"
          imageType="v"
        />
      </View>
    );
  };

  renderItem = ({ item, index }) => {
    const { genres } = this.props;
    let typeItem = genres.find((e) => e.id === item);
    if(typeItem){
      return (
        <View key={index} style={styles.imageBox}>
          <Image source={Images[typeItem.name]} style={styles.equipments} />
          <Text style={styles.equipmentsText}>{typeItem.title.toLowerCase()}</Text>
        </View>
      );    
    }


    typeItem = genres.find((e) => e.id === item.id);

    return typeItem ? (
      <View key={index} style={styles.imageBox}>
        <Image source={{ uri: item.signedDeviceCoverImage }} style={styles.equipments} />
        <Text style={styles.equipmentsText}>{typeItem.title.toLowerCase()}</Text>
      </View>
    ) : null;
  };

  pagination = () => {
    const { activeSlide } = this.state;
    const { instructor } = this.props;
    return (
      <Pagination
        dotsLength={
          isArray(instructor.signedCoverImage)
            ? this.getInstructorCoverAndVideoMerged().length
            : 0
        }
        activeDotIndex={activeSlide}
        containerStyle={styles.dotBackground}
        dotStyle={styles.dot}
        inactiveDotOpacity={0.3}
        inactiveDotScale={1}
      />
    );
  };

  getInstructorCoverAndVideoMerged = () => {
    const { instructor } = this.props;
    const arr = [];
    if (!isEmpty(instructor.originVideoUrl)) {
      arr.push({
        type: 'video',
        url: instructor.originVideoUrl,
      });
    }
    return arr.concat(instructor.signedCoverImage);
  };

  render() {
    const { instructor } = this.props;
    return (
      <ScrollView style={styles.container}>
        <SecondaryNavbar back navTitle={t('class_detail_instructor')} />
        <View>
          <Carousel
            data={this.getInstructorCoverAndVideoMerged()}
            renderItem={this.renderCarouselItem}
            onSnapToItem={(index) => this.setState({ activeSlide: index })}
            sliderWidth={Screen.width}
            itemWidth={Screen.width}
            inactiveSlideScale={1}
          />
          {this.pagination()}
        </View>
        <View style={styles.wrapper}>
          <HeadLine
            paddingHorizontal={16}
            style={styles.headerStyle}
            title={t('instructor_about_instructor')}
          />
          <View style={styles.aboutBox}>
            <BaseReadMore showLine={5} text={instructor.description} />
          </View>
        </View>
        <View style={styles.wrapper}>
          <HeadLine
            paddingHorizontal={16}
            style={styles.headerStyle}
            title={t('instructor_expert_in')}
          />
          <View style={styles.flexBox}>
            <FlatList
              keyExtractor={(item, index) => `expert_${index}`}
              renderItem={this.renderItem}
              data={instructor.expertise}
              showsHorizontalScrollIndicator={false}
              horizontal
            />
          </View>
        </View>
        <View style={styles.wrapper}>
          <HeadLine
            paddingHorizontal={16}
            style={styles.headerStyle}
            title={t('instructor_coaching_style')}
          />
          <View
            style={[
              Classes.row,
              Classes.paddingLeft,
              Classes.paddingRight,
              Classes.marginBottom,
            ]}
          >
            {isArray(instructor.tags) &&
              instructor.tags.map((e, i) => (
                <Text key={`${e.tagId}`} style={styles.txtTags}>
                  {e.name}
                  {i !== instructor.tags.length - 1 ? ', ' : '.'}
                </Text>
              ))}
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default connect(
  (state) => ({
    token: state.user.token,
    genres: state.setting.genres,
    instructor: state.instructor,
    isLoading: state.appState.isLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getInstructor: InstructorActions.getInstructor,
      },
      dispatch,
    ),
)(InstructorScreen);
