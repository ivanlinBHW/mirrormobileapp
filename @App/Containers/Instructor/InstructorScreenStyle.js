import { ScaledSheet } from 'App/Helpers';
import { Styles, Metrics, Colors, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    flex: 1,
    height: '100%',
  },
  navBar: {
    ...Styles.modalNavBar,
  },
  slide: {
    width: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: '28@vs',
  },
  img: {
    width: '263@s',
    height: '230@s',
  },
  dotBackground: {
    backgroundColor: Colors.white,
    marginBottom: Metrics.baseMargin,
  },
  dot: {
    width: '7@s',
    height: '7@s',
    borderRadius: '3.5@s',
    marginLeft: 0,
    marginRight: '9@s',
    backgroundColor: 'black',
  },
  wrapper: {
    minHeight: '60@vs',
  },
  equipments: {
    width: '30@s',
    height: '30@s',
  },
  equipmentsText: {
    ...Fonts.style.extraSmall,
    width: '60@s',
    textAlign: 'center',
  },
  imageBox: {
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: Metrics.baseMargin,
    marginTop: Metrics.baseMargin,
  },
  flexBox: {
    paddingHorizontal: '8@s',
  },
  aboutBox: {
  },
  headerStyle: {
    marginBottom: 0,
  },
  instructorVideoLoadingIndicator: {
    position: 'absolute',
    top: '45%',
    left: '45%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
