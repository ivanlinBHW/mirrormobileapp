import React from 'react';
import PropTypes from 'prop-types';
import { get, isArray } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import {
  View,
  Image,
  Text,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import { RoundButton } from '@ublocks-react-native/component';

import { translate as t } from 'App/Helpers/I18n';
import {
  RoundButton as BaseRoundButton,
  BaseSearchButton,
  SecondaryNavbar,
  DismissKeyboard,
  SquareButton,
  BaseAvatar,
  BaseInput,
} from 'App/Components';
import { SearchActions, SettingActions } from 'App/Stores';
import { Images } from 'App/Theme';
import styles from './CommunitySearchScreenStyle';

const initialState = {
  text: '',
  isCompleted: false,
  isBookmarked: false,
  noEquipment: false,
  genre: [],
  instructor: [],
  equipment: [],
  difficulty: [],
  duration: [],
  genreList: [
    {
      img: 'expertise_0',
      activeImg: 'expertise_0_active',
      text: 'search_boxing',
      key: 'boxing',
      selected: false,
    },
    {
      img: 'expertise_1',
      activeImg: 'expertise_1_active',
      text: 'search_cardio',
      key: 'cardio',
      selected: false,
    },
    {
      img: 'expertise_2',
      activeImg: 'expertise_2_active',
      text: 'search_strength',
      key: 'strength',
      selected: false,
    },
    {
      img: 'expertise_3',
      activeImg: 'expertise_3_active',
      text: 'search_stretching',
      key: 'stretching',
      selected: false,
    },
    {
      img: 'expertise_4',
      activeImg: 'expertise_4_active',
      text: 'search_yoga',
      key: 'yoga',
      selected: false,
    },
  ],
  difficultyList: [
    {
      text: 'search_lv1',
      selected: false,
      key: 1,
    },
    {
      text: 'search_lv2',
      selected: false,
      key: 2,
    },
    {
      text: 'search_lv3',
      selected: false,
      key: 3,
    },
    {
      text: 'search_lv4',
      selected: false,
      key: 4,
    },
  ],
  durationList: [
    {
      text: 'search_duration_15',
      selected: false,
      key: 900,
    },
    {
      text: 'search_duration_30',
      selected: false,
      key: 1800,
    },
    {
      text: 'search_duration_45',
      selected: false,
      key: 2700,
    },
    {
      text: 'search_duration_60',
      selected: false,
      key: 3600,
    },
  ],
  genres: [],
  showAll: false,
};

class CommunitySearchScreen extends React.Component {
  static propTypes = {
    multiSelect: PropTypes.bool,
    getInstructorList: PropTypes.func.isRequired,
    getEquipmentList: PropTypes.func.isRequired,
    getGenre: PropTypes.func.isRequired,
    genres: PropTypes.array.isRequired,
    communitySearch: PropTypes.func.isRequired,
    instructorList: PropTypes.array,
    equipmentList: PropTypes.array,
    token: PropTypes.string,
  };

  static defaultProps = {
    multiSelect: true,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.genres !== prevState.genres && nextProps.genres) {
      return {
        genres: nextProps.genres,
        genreList: nextProps.genres.map((e) => ({
          ...e,
          selected: false,
        })),
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    this.state = JSON.parse(JSON.stringify(initialState));
  }

  componentDidMount() {
    __DEV__ && console.log('@SearchScreen');
    const { getEquipmentList, getInstructorList, getGenre, token } = this.props;
    getEquipmentList(token);
    getInstructorList(token);
    getGenre();
  }

  renderNavbar = () => (
    <SecondaryNavbar
      style={styles.navBar}
      navTitle={t('search_title')}
      back
      navRightComponent={
        <View>
          <BaseRoundButton
            style={styles.btnStyle}
            textStyle={styles.textStyle}
            uppercase={false}
            text={t('search_clear_all')}
            onPress={this.onPressClear}
          />
        </View>
      }
    />
  );

  onPressClear = () => {
    const { genres } = this.props;
    this.setState(JSON.parse(JSON.stringify(initialState)));

    if (genres) {
      this.setState({
        genreList: genres.map((e) => ({
          ...e,
          selected: false,
        })),
        text: '',
      });
    }
  };

  renderSearchInput = () => {
    const { text } = this.state;
    return (
      <View style={styles.wrapper}>
        <BaseInput
          leftComponent={<Image source={Images.search_gray} style={styles.searchImg} />}
          leftIconContainerStyle={styles.leftIconContainerStyle}
          inputContainerStyle={styles.inputContainerStyle}
          containerStyle={styles.inputStyle}
          placeholder={t('search_title')}
          value={text}
          onChangeText={(value) => this.setState({ text: value })}
        />
      </View>
    );
  };

  renderFirst = () => {
    const { isCompleted, isBookmarked } = this.state;
    return (
      <View style={styles.flexBox}>
        <BaseSearchButton
          image={isCompleted ? Images.finsih_white : Images.finsih_black}
          text={t('search_completed')}
          isSelected={isCompleted}
          onPress={() => this.setState({ isCompleted: !isCompleted })}
        />
        <BaseSearchButton
          image={isBookmarked ? Images.bookmark : Images.bookmark_black}
          text={t('search_bookmarked')}
          isSelected={isBookmarked}
          imageStyle={styles.imgStyle}
          onPress={() => this.setState({ isBookmarked: !isBookmarked })}
        />
      </View>
    );
  };

  renderGenre = () => {
    const { genreList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_workout_type')}</Text>
        <View style={styles.flexBox}>
          {genreList &&
            genreList.length > 0 &&
            genreList.map((item) => {
              return (
                <BaseSearchButton
                  image={{ uri: item.selected ? item.coverImage : item.deviceCoverImage }}
                  text={item.title}
                  isSelected={item.selected}
                  key={item.id}
                  onPress={() => this.onPressGenre(item.id)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderDifficulty = () => {
    const { difficultyList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_difficulty')}</Text>
        <View style={styles.flexBox}>
          {difficultyList &&
            difficultyList.length > 0 &&
            difficultyList.map((item, index) => {
              return (
                <BaseSearchButton
                  size="normal"
                  text={t(item.text)}
                  isSelected={item.selected}
                  key={index}
                  onPress={() => this.onPressLevel(item.key)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderDuration = () => {
    const { durationList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_duration')}</Text>
        <View style={styles.flexBox}>
          {durationList &&
            durationList.length > 0 &&
            durationList.map((item, index) => {
              return (
                <BaseSearchButton
                  size="normal"
                  text={t(item.text)}
                  isSelected={item.selected}
                  key={index}
                  onPress={() => this.onPressDuration(item.key)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderInstructor = () => {
    const { instructorList } = this.props;
    const { instructor, showAll } = this.state;
    return (
      <View style={styles.box}>
        <View style={styles.titleBox}>
          <Text style={styles.title}>{t('search_instructor')}</Text>
          <TouchableOpacity onPress={() => this.setState({ showAll: !showAll })}>
            <Text style={styles.rightText}>
              {t(showAll ? 'search_viewless' : 'search_viewall')}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.flexBox}>
          {instructorList.map((item, index) => {
            return (
              (index < 8 || showAll) && (
                <RoundButton
                  onPress={() => this.onPressInstructor(item.id)}
                  key={index}
                  style={styles.instructorBox}
                  throttleTime={0}
                >
                  <BaseAvatar
                    active={instructor.filter((select) => select === item.id).length > 0}
                    size={49}
                    uri={{
                      uri: get(item, 'signedAvatarImage') || '',
                    }}
                    style={styles.avatar}
                  />
                  <Text
                    style={[
                      styles.instructorText,
                      instructor.filter((select) => select === item.id).length > 0 &&
                        styles.activeText,
                    ]}
                  >
                    {item.title}
                  </Text>
                </RoundButton>
              )
            );
          })}
        </View>
      </View>
    );
  };

  renderEquipments = () => {
    const { equipmentList } = this.props;
    const { equipment, noEquipment } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_equipment')}</Text>
        <View style={styles.flexBox}>
          <BaseSearchButton
            image={noEquipment ? Images.no_equipment_white_1 : Images.no_equipment_black}
            text={t('search_no_equipment')}
            isSelected={noEquipment}
            onPress={() => this.setState({ noEquipment: !noEquipment })}
          />
          {isArray(equipmentList) &&
            equipmentList.map((item) => {
              return (
                <BaseSearchButton
                  image={{ uri: item.selected ? item.coverImage : item.deviceCoverImage }}
                  text={item.title}
                  isSelected={equipment.filter((select) => select === item.id).length > 0}
                  key={item.id}
                  onPress={() => this.onPressEquipment(item.id)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  onPressInstructor = (id) => {
    const { instructor } = this.state;
    const temp = instructor;
    if (temp.filter((item) => item === id).length > 0) {
      const index = temp.indexOf(id);
      temp.splice(index, 1);
    } else {
      temp.push(id);
    }
    this.setState({
      instructor: temp,
    });
  };

  onPressEquipment = (id) => {
    const { equipment } = this.state;
    let temp = equipment;
    if (temp.filter((item) => item === id).length > 0) {
      const index = temp.indexOf(id);
      temp.splice(index, 1);
    } else {
      temp.push(id);
    }
    this.setState({
      equipment: temp,
    });
  };

  onPressGenre = (id) => {
    const { genreList } = this.state;
    let temp = genreList;
    temp.forEach((item) => {
      if (item.id === id) {
        item.selected = !item.selected;
      }
    });
    this.setState({
      genreList: temp,
    });
  };

  onPressLevel = (key) => {
    const { difficultyList } = this.state;
    let temp = difficultyList;
    temp.forEach((item) => {
      if (item.key === key) {
        item.selected = !item.selected;
      }
    });
    this.setState({
      difficultyList: temp,
    });
  };

  onPressDuration = (key) => {
    const { durationList } = this.state;
    let temp = durationList;
    temp.forEach((item) => {
      if (item.key === key) {
        item.selected = !item.selected;
      }
    });
    this.setState({
      durationList: temp,
    });
  };

  onPressFilter = () => {
    const {
      isBookmarked,
      isCompleted,
      genreList,
      instructor,
      equipment,
      durationList,
      difficultyList,
      noEquipment,
      text,
    } = this.state;
    const { multiSelect, communitySearch } = this.props;
    let genre = [];
    let level = [];
    let duration = [];
    let tempEquipment = equipment;
    if (noEquipment) {
      tempEquipment.push('no-equipment');
    }
    genreList.forEach((item, index) => {
      if (item.selected) {
        genre.push(item.id);
      }
    });
    durationList.forEach((item, index) => {
      if (item.selected) {
        duration.push(item.key);
      }
    });
    difficultyList.forEach((item, index) => {
      if (item.selected) {
        level.push(item.key);
      }
    });
    const payload = {
      isBookmarked,
      isCompleted,
      genre,
      instructors: instructor,
      equipments: tempEquipment,
      level,
      duration,
      text,
    };
    Actions.ClassesSelectedScreen({ multiSelect, searchPayload: payload });
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        {this.renderNavbar()}
        <View style={styles.scrollBox}>
          <ScrollView style={styles.scrollBox}>
            <DismissKeyboard>
              <>
                {this.renderSearchInput()}
                <View style={styles.line} />
                {this.renderFirst()}
                <View style={styles.line} />
                {this.renderGenre()}
                <View style={styles.line} />
                {this.renderInstructor()}
                <View style={styles.line} />
                {this.renderEquipments()}
                <View style={styles.line} />
                {this.renderDifficulty()}
                <View style={styles.line} />
                {this.renderDuration()}
              </>
            </DismissKeyboard>
          </ScrollView>
        </View>
        <View style={styles.applyBtn}>
          <SquareButton text={t('search_apply')} onPress={this.onPressFilter} />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  (state) => ({
    token: state.user.token,
    genres: state.setting.genres,
    instructorList: state.search.instructorList,
    equipmentList: state.search.equipmentList,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getInstructorList: SearchActions.getInstructorList,
        getEquipmentList: SearchActions.getEquipmentList,
        communitySearch: SearchActions.communitySearch,
        getGenre: SettingActions.getGenre,
      },
      dispatch,
    ),
)(CommunitySearchScreen);
