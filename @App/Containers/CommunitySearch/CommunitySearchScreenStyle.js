import { Platform } from 'react-native';
import { ScaledSheet } from 'App/Helpers';
import { Styles, Colors, Metrics, Fonts, Classes } from 'App/Theme';

export default ScaledSheet.create({
  layout: {
    ...Styles.screenPaddingLeft,
  },
  navBar: {
    ...Styles.navBar,
  },
  searchImg: {
    width: '20@s',
    height: '16@vs',
  },
  btnStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: 0,
    marginRight: -4,
    flex: 1,
    backgroundColor: 'transparent',
  },
  textStyle: {
    fontSize: Fonts.size.regular,
    color: Colors.button.primary.text.text,
  },
  inputStyle: {
    backgroundColor: Colors.gray_12,
    height: '36@vs',
    borderRadius: '10@vs',
    justifyContent: 'center',
  },
  wrapper: {
    marginHorizontal: Metrics.baseMargin,
    marginBottom: '40@vs',
  },
  leftIconContainerStyle: {
    marginLeft: 0,
    position: 'relative',
    marginTop: Platform.select({
      ios: '8@s',
      android: '-6@s',
    }),
  },
  inputContainerStyle: {
    height: '22@vs',
    marginTop: Platform.select({
      ios: '-8@s',
      android: '8@s',
    }),
    justifyContent: 'center',
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: Colors.horizontalLine.primary,
    marginBottom: Metrics.baseMargin,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {
  },
  title: {
    ...Fonts.style.small500,
    marginLeft: Metrics.baseMargin,
  },
  titleBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: Metrics.baseMargin,
  },
  instructorBox: {
    width: '83@s',
    height: '87@vs',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    marginLeft: Metrics.baseMargin,
    marginTop: '10@vs',
    borderWidth: 0,
  },
  avatar: {
    width: '49@s',
    height: '49@s',
    borderRadius: '24.5@s',
    marginBottom: Metrics.baseMargin / 2,
  },
  instructorText: {
    fontSize: Fonts.size.small,
    width: '83@s',
    textAlign: 'center',
  },

  activeText: {
    color: Colors.titleText.active,
  },
  rightText: {
    color: Colors.button.primary.text.text,
  },
  scrollBox: {
    flex: 1,
  },
  container: {
    ...Classes.fill,
  },
  imgStyle: {
    width: '19@s',
    height: '32@vs',
  },
});
