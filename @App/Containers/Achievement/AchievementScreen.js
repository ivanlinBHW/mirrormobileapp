import React from 'react';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import { Text, View, SafeAreaView, Platform } from 'react-native';

import { Classes } from 'App/Theme';
import { Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { AchievementItem, BaseIconButton, SecondaryNavbar } from 'App/Components';
import styles from './AchievementStyle';

export default class AchievementScreen extends React.PureComponent {
  static propTypes = {
    achievement: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    date: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
  };

  static defaultProps = {
    date: '',
    title: '',
    description:
      'Awarded for completing your first Strength workout. This is just the start! ',
  };

  renderImage = () => {
    const { achievement, active } = this.props;
    return (
      <View style={[Classes.center]}>
        <AchievementItem active={active} large name={achievement} disabled />
      </View>
    );
  };

  renderDescription = () => {
    const { date, title, description, achievement } = this.props;
    return (
      <View style={[styles.descriptionWrapper, Classes.center]}>
        <Text style={styles.txtAchievementDate}>{date}</Text>
        <Text style={styles.txtAchievementTitle} numberOfLines={2}>
          {t(achievement)}
        </Text>
        {/* <Text style={styles.txtAchievementDescription}>{description}</Text> */}
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.containerLightPink}>
        <SecondaryNavbar
          navHeight={80}
          navRightComponent={
            <BaseIconButton
              iconType="FontAwesome5"
              iconName="times"
              iconSize={Platform.isPad ? Screen.scale(8) : Screen.scale(24)}
              onPress={() => Actions.pop()}
            />
          }
        />
        {this.renderImage()}
        {this.renderDescription()}
      </SafeAreaView>
    );
  }
}
