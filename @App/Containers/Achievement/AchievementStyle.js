import { StyleSheet } from 'App/Helpers';
import { Colors, Fonts, Metrics, Classes } from 'App/Theme';

export default StyleSheet.create({
  container: {
    ...Classes.fill,
    backgroundColor: Colors.white_02,
  },
  containerLightPink: {
    ...Classes.fill,
    backgroundColor: Colors.very_light_pink,
  },
  marginLeft: {
    marginLeft: Metrics.baseMargin,
  },
  layout: {
    marginLeft: 16,
    flexDirection: 'column',
  },

  descriptionWrapper: {},
  txtAchievementDate: {
    ...Fonts.style.small,
    color: Colors.black,
    marginBottom: Metrics.baseVerticalMargin * 1.5,
  },
  txtAchievementTitle: {
    ...Fonts.style.h1_500,
    color: Colors.black,
    width: '70%',
    textAlign: 'center',
  },
  txtAchievementDescription: {
    ...Fonts.style.medium,
    marginVertical: Metrics.baseVerticalMargin,
    width: '70%',
    textAlign: 'center',
  },
  achievementItemWrapper: {
    marginHorizontal: Metrics.baseMargin / 2,
    alignItems: 'center',
  },
  txtAchievementItem: {
    ...Fonts.style.small500,
    width: '64@s',
    textAlign: 'center',
  },
  emptyBox: {
    height: Metrics.baseMargin,
  },
});
