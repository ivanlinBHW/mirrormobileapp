import React from 'react';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import { Text, View, SafeAreaView, Platform } from 'react-native';

import { Screen } from 'App/Helpers';
import { Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { AchievementItem, BaseIconButton, SecondaryNavbar } from 'App/Components';
import styles from './AchievementStyle';

export default class AwesomeScreen extends React.PureComponent {
  static propTypes = {
    achievement: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    title: PropTypes.string,
    description: PropTypes.string,
  };

  static defaultProps = {
    title: 'First Stretching Workout',
    description:
      'Awarded for completing your first Strength workout. This is just the start! ',
  };

  componentDidMount() {}

  renderImage = () => {
    const { achievement } = this.props;
    return (
      <View style={[Classes.center]}>
        <AchievementItem active large name={achievement} />
      </View>
    );
  };

  renderDescription = () => {
    return (
      <View style={[styles.descriptionWrapper, Classes.center]}>
        <Text style={styles.txtAchievementTitle} numberOfLines={2}>
          {t('awesome_title')}
        </Text>
        <Text style={styles.txtAchievementDescription}>{t('awesome_description')}</Text>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.containerLightPink}>
        <SecondaryNavbar
          navHeight={80}
          navRightComponent={
            <BaseIconButton
              iconType="FontAwesome5"
              iconName="times"
              iconSize={Platform.isPad ? Screen.scale(10) : Screen.scale(40)}
              onPress={() => Actions.pop()}
            />
          }
        />
        {this.renderImage()}
        {this.renderDescription()}
      </SafeAreaView>
    );
  }
}
