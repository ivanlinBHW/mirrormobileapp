import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, ScrollView, SafeAreaView, FlatList } from 'react-native';

import { Classes } from 'App/Theme';
import { Date as d } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { AchievementItem, HeadLine, SecondaryNavbar } from 'App/Components';
import styles from './AchievementStyle';

class AchievementListScreen extends React.PureComponent {
  static propTypes = {
    allAchievements: PropTypes.array.isRequired,
  };

  renderAchievementItem = ({ item, index }) => {
    return (
      <View style={styles.achievementItemWrapper}>
        <AchievementItem
          key={index}
          name={item.achievement.name}
          active={item.isAchieved}
          date={
            item.isAchieved
              ? d.formatDate(item.createdAt, d.ACHIEVEMENT_DATE_FORMAT)
              : null
          }
        />
        <Text numberOfLines={3} style={styles.txtAchievementItem}>
          {t(item.achievement.name)}
        </Text>
      </View>
    );
  };

  render() {
    const {
      allAchievements: {
        boxing = [],
        stretching = [],
        cardio = [],
        strength = [],
        yoga = [],
        daysLastFor = [],
      },
    } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar navTitle={t('achievement_list_nav_title')} back />
        <ScrollView style={styles.container} nestedScrollEnabled>
          <HeadLine
            title={t('achievement_list_stretching')}
            hrLine
            paddingTop={0}
            paddingHorizontal={16}
          />
          <FlatList
            data={stretching}
            contentContainerStyle={[Classes.mainStart, Classes.fullHeight]}
            renderItem={this.renderAchievementItem}
            style={styles.marginLeft}
            showsHorizontalScrollIndicator={false}
            horizontal
          />

          <HeadLine
            title={t('achievement_list_boxing')}
            hrLine
            paddingTop={0}
            paddingHorizontal={16}
          />
          <FlatList
            data={boxing}
            renderItem={this.renderAchievementItem}
            style={styles.marginLeft}
            horizontal
            showsHorizontalScrollIndicator={false}
          />

          <HeadLine
            title={t('achievement_list_strength')}
            hrLine
            paddingTop={0}
            paddingHorizontal={16}
          />
          <FlatList
            data={strength}
            renderItem={this.renderAchievementItem}
            style={styles.marginLeft}
            horizontal
            showsHorizontalScrollIndicator={false}
          />

          <HeadLine
            title={t('achievement_list_yoga')}
            hrLine
            paddingTop={0}
            paddingHorizontal={16}
          />
          <FlatList
            data={yoga}
            renderItem={this.renderAchievementItem}
            style={styles.marginLeft}
            horizontal
            showsHorizontalScrollIndicator={false}
          />

          <HeadLine
            title={t('achievement_list_cardio')}
            hrLine
            paddingTop={0}
            paddingHorizontal={16}
          />
          <FlatList
            data={cardio}
            renderItem={this.renderAchievementItem}
            style={styles.marginLeft}
            horizontal
            showsHorizontalScrollIndicator={false}
          />

          <HeadLine
            title={t('achievement_list_days_for')}
            hrLine
            paddingTop={0}
            paddingHorizontal={16}
          />
          <FlatList
            data={daysLastFor}
            renderItem={this.renderAchievementItem}
            style={styles.marginLeft}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
          <View style={styles.emptyBox} />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    allAchievements: state.progress.allAchievements,
  }),
  (dispatch) =>
    bindActionCreators(
      {
      },
      dispatch,
    ),
)(AchievementListScreen);
