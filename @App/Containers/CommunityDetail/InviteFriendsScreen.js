import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, FlatList, View, Text, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import {
  SecondaryNavbar,
  BaseButton,
  BaseInput,
  BaseStatusAvatar,
  BouncyCheckbox,
  BaseEditAvatar,
} from 'App/Components';
import { Images, Classes, Colors } from 'App/Theme';
import styles from './InviteFriendsScreenStyle';
import { CommunityActions } from 'App/Stores/index';

class InviteFriendsScreen extends React.Component {
  static propTypes = {
    list: PropTypes.array.isRequired,
    token: PropTypes.string,
    timezone: PropTypes.string.isRequired,
    searchEventFriend: PropTypes.func.isRequired,
    inviteFriends: PropTypes.func.isRequired,
    selected: PropTypes.array,
    detail: PropTypes.object,
    trainingEventId: PropTypes.string,
  };

  static defaultProps = {
    selected: [],
  };

  state = {
    text: '',
    friendList: [],
    selected: [],
  };

  componentDidMount() {
    __DEV__ && console.log('@InviteFriendsScreen');
    const { searchEventFriend, detail, trainingEventId } = this.props;
    searchEventFriend(detail.id, '', trainingEventId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list !== this.props.list) {
      this.setState({
        friendList: nextProps.list,
      });
    }
  }

  onPressSelected = (data) => {
    const { selected, friendList } = this.state;
    let temp = JSON.parse(JSON.stringify(selected));
    if (temp.length > 0 && temp.filter((item) => item.id === data.id).length > 0) {
      let tempSelected = [];
      temp.forEach((select) => {
        if (select.id !== data.id) {
          tempSelected.push(select);
        }
      });
      temp = tempSelected;
    } else {
      temp.push(data);
    }
    let tempFriendList = JSON.parse(JSON.stringify(friendList));
    tempFriendList.map((item, index) => {
      if (item.id === data.id) {
        tempFriendList[index].isSelect = !tempFriendList[index].isSelect;
      }
    });
    this.setState({
      selected: temp,
      friendList: tempFriendList,
    });
  };

  onPressSearch = (value) => {
    const { searchEventFriend, detail, trainingEventId } = this.props;
    this.setState({ text: value });
    searchEventFriend(detail.id, value, trainingEventId);
  };

  renderItem = ({ item, index }) => {
    return (
      <BaseButton
        transparent
        style={styles.itemWrapper}
        onPress={() => this.onPressSelected(item)}
      >
        <View
          style={[
            styles.itemWrapper,
            { backgroundColor: item.isSelect ? Colors.white_02 : Colors.white },
          ]}
        >
          <View style={styles.leftItem}>
            <View style={styles.avatarBox}>
              <BaseStatusAvatar
                isOnline={item.currentStatus === 1}
              />
            </View>
            <View style={styles.itemTitleBox}>
              <Text style={styles.itemTitle}>{item.fullName}</Text>
              <Text style={styles.itemSecondTitle}>
                {item.currentStatus === 1 ? 'online' : 'offline'}
              </Text>
            </View>
          </View>
          <View style={styles.rightItem}>
            <BouncyCheckbox
              isChecked={item.isSelect}
              fillColor={Colors.black}
              checkboxSize={22}
              text=""
              borderColor={Colors.gray_03}
              onPress={() => this.onPressSelected(item)}
            />
          </View>
        </View>
      </BaseButton>
    );
  };

  onPressRemove = (data) => {
    const { selected, friendList } = this.state;
    let tempSelected = JSON.parse(JSON.stringify(selected));
    let tempFriendList = JSON.parse(JSON.stringify(friendList));
    let select = [];
    tempSelected.forEach((item) => {
      if (item.id !== data.id) {
        select.push(item);
      }
    });
    tempFriendList.map((item, index) => {
      if (item.id === data.id) {
        tempFriendList[index].isSelect = !tempFriendList[index].isSelect;
      }
    });
    this.setState({
      selected: select,
      friendList: tempFriendList,
    });
  };

  renderSelectedItem = ({ item, index }) => {
    return (
      <View style={styles.selectedWrapper}>
        <View style={styles.editAvatarBox}>
          <BaseEditAvatar
            onPress={() => this.onPressRemove(item)}
          />
        </View>
        <Text numberOfLines={1} style={styles.selectedText}>
          {item.fullName}
        </Text>
      </View>
    );
  };

  onPressSend = () => {
    const { inviteFriends, detail } = this.props;
    const { selected } = this.state;

    let payload = [];
    selected.map((item) => {
      payload.push(item.id);
    });
    console.log('payload ->', payload);
    inviteFriends(detail.id, payload);
    Actions.pop();
  };

  renderEmpty = () => {
    return (
      <View style={styles.emptyBox}>
        <Text style={styles.emptyTitle}>{t('see_all_empty_title')}</Text>
        <Text style={styles.emptyContent}>{t('see_all_empty_content')}</Text>
      </View>
    );
  };
  render() {
    const { text, friendList, selected = [] } = this.state;
    return (
      <SafeAreaView style={styles.wrapper}>
        <SecondaryNavbar
          back
          navTitle={
            selected.length > 0
              ? `${t('__selected', { count: selected.length })}`
              : `${t('community_friends')}`
          }
          navRightComponent={
            <BaseButton
              disabled={selected.length === 0}
              transparent
              style={styles.saveBtn}
              onPress={this.onPressSend}
            >
              {/* TODO Missing English translation */}
              <Text style={styles.saveText}>{t('__send')}</Text>
            </BaseButton>
          }
        />

        <View style={styles.searchWrapper}>
          <BaseInput
            leftComponent={<Image source={Images.search_gray} style={styles.searchImg} />}
            leftIconContainerStyle={styles.leftIconContainerStyle}
            inputContainerStyle={styles.inputContainerStyle}
            containerStyle={styles.inputStyle}
            placeholder={t('search_title')}
            value={text}
            onChangeText={(value) => this.onPressSearch(value)}
          />
        </View>
        <View style={Classes.fill}>
          <FlatList
            data={friendList}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderEmpty()}
          />
        </View>
        {selected.length > 0 && (
          <View style={styles.bottomWrapper}>
            <FlatList
              horizontal
              data={selected}
              showsHorizontalScrollIndicator={false}
              renderItem={this.renderSelectedItem}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    list: state.community.eventFriends,
    selected: state.community.friendSelected,
    timezone: state.appState.currentTimeZone,
    detail: state.community.eventDetail,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        searchEventFriend: CommunityActions.searchEventFriend,
        inviteFriends: CommunityActions.inviteFriends,
      },
      dispatch,
    ),
)(InviteFriendsScreen);
