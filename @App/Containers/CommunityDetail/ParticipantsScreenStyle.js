import { ScaledSheet } from 'App/Helpers';
import { Colors, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  headerBox: {
    backgroundColor: Colors.white_02,
    paddingHorizontal: Metrics.baseMargin,
  },
  headerTitle: {
    ...Fonts.style.medium500,
    marginVertical: Metrics.baseMargin,
  },
  wrapperBox: {
    flexDirection: 'row',
    minHeight: '56@s',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_15,
  },
  itemBox: {
    flexDirection: 'row',
    minHeight: '56@s',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemImg: {
    width: '40@s',
    height: '40@s',
    borderRadius: '20@s',
    marginLeft: Metrics.baseMargin,
  },
  itemName: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
    textAlign: 'left',
  },
  friendBox: {
    width: '83@s',
    height: '24@vs',
    borderWidth: 1,
    borderColor: Colors.primary,
    borderRadius: 6,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: Metrics.baseMargin,
  },
  friendTag: {
    fontSize: Fonts.size.small,
    color: Colors.primary,
  },
  leftBox: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
