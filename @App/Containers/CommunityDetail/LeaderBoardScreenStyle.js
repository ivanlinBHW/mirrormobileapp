import { ScaledSheet, Screen } from 'App/Helpers';
import { Colors, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  headerBox: {
    backgroundColor: Colors.white_02,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: Metrics.baseMargin,
  },
  headerTitle: {
    ...Fonts.style.medium500,
    marginVertical: Metrics.baseMargin,
  },
  itemBox: {
    flexDirection: 'row',
    minHeight: '56@s',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_15,
  },
  itemIndex: {
    ...Fonts.style.medium500,
    width: '60@s',
    textAlign: 'center',
    marginLeft: Metrics.baseMargin,
  },
  itemImg: {
    width: '40@s',
    height: '40@s',
    borderRadius: '20@s',
    marginLeft: Metrics.baseMargin,
  },
  itemName: {
    ...Fonts.style.medium500,
    width: '114@s',
    marginLeft: Metrics.baseMargin,
    textAlign: 'left',
  },
  itemTime: {
    ...Fonts.style.medium500,
    marginLeft: '10@s',
    color: Colors.gray_02,
  },
  emptyHeight: {
    height: Screen.height - Screen.scale(92),
  },
});
