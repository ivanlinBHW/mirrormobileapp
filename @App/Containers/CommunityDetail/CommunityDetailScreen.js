import React from 'react';
import PropTypes from 'prop-types';
import Share from 'react-native-share';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { ScrollView, SafeAreaView, View, Text, Alert } from 'react-native';

import { Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d, Screen } from 'App/Helpers';
import {
  SecondaryNavbar,
  HorizontalLine,
  BaseButton,
  RoundButton,
  HeadLine,
  ClassCard,
  CachedImage as Image,
} from 'App/Components';
import { CommunityActions } from 'App/Stores';
import styles from './CommunityDetailScreenStyle';
import { Colors } from 'App/Theme/index';
import { Config } from 'App/Config';

class CommunityDetailScreen extends React.Component {
  static propTypes = {
    timezone: PropTypes.string,
    detail: PropTypes.object,
    getCommunityClassDetail: PropTypes.func.isRequired,
    onlyGetTrainingEvent: PropTypes.func.isRequired,
    token: PropTypes.string,
    setCommunityClassBookmark: PropTypes.func.isRequired,
    removeCommunityClassBookmark: PropTypes.func.isRequired,
    joinEvent: PropTypes.func.isRequired,
    leaveEvent: PropTypes.func.isRequired,
    userId: PropTypes.string,
    getEventLeaderBoard: PropTypes.func.isRequired,
    deleteTrainingEvent: PropTypes.func.isRequired,
    rejectTrainingEvent: PropTypes.func.isRequired,
    acceptTrainingEvent: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    sceneName: PropTypes.string.isRequired,
  };

  static defaultProps = {
    timezone: 'Asia/Taipei',
  };

  state = {};

  componentDidMount() {
    __DEV__ && console.log('@CommunityDetailScreen');
  }

  renderInviteOrJoinButton = () => {
    const { detail, joinEvent, userId, isLoading, sceneName } = this.props;
    if (detail.wasFullyBooked) {
      return (
        <BaseButton
          transparent
          height={Screen.scale(32)}
          text={t('community_detail_btn_fully_booked')}
          style={styles.baseGrayBtn}
          textColor={Colors.button.secondary.content.text}
          textStyle={styles.btnText}
        />
      );
    } else if (!detail.wasFullyBooked && !detail.wasJoined) {
      return (
        <BaseButton
          transparent
          height={Screen.scale(32)}
          text={t('join')}
          style={[styles.baseBtn]}
          textColor={Colors.button.primary.content.text}
          textStyle={styles.btnText}
          onPress={() => joinEvent(detail.id)}
          disabled={isLoading || detail.wasFullyBooked || detail.wasJoined}
          throttleTime={2000}
        />
      );
    } else if (detail.wasJoined && (detail.canInvite || detail.ownerId === userId)) {
      console.log('detail=>', detail);
      return (
        <BaseButton
          transparent
          height={Screen.scale(32)}
          text={t('invite')}
          style={[styles.baseBtn]}
          textColor={Colors.button.primary.content.text}
          textStyle={styles.btnText}
          onPress={() =>
            sceneName.includes('Notification')
              ? Actions.NotificationInviteFriendsScreen({ trainingEventId: detail.id })
              : Actions.InviteFriendsScreen({ trainingEventId: detail.id })
          }
          disabled={isLoading}
          throttleTime={1500}
        />
      );
    }
  };

  renderInviteAndShareBlock = () => {
    const {
      detail: { dueDate },
      timezone,
    } = this.props;
    const isSameOrAfterDate = d.isSameOrAfterDate(
      d.transformDate(dueDate, timezone),
      new Date(),
    );
    return (
      isSameOrAfterDate && (
        <View style={styles.btnBox}>
          {this.renderInviteOrJoinButton()}
          {/* <BaseButton
            transparent
            height={Screen.scale(32)}
            text={t('__share')}
            onPress={this.onPressShare}
            textColor={Colors.primary}
            textStyle={styles.btnText}
            style={[styles.baseBtn, styles.borderBtn]}
          /> */}
        </View>
      )
    );
  };

  renderAcceptAndRejectBlock = () => {
    const { detail, acceptTrainingEvent, rejectTrainingEvent, isLoading } = this.props;
    return (
      <View style={styles.btnBoxAcceptAndReject}>
        <RoundButton
          style={{ margin: 5 }}
          onPress={() => acceptTrainingEvent(detail.id)}
          text={t('community_detail_btn_accept')}
          textColor={Colors.button.primary.content.text}
          color={Colors.button.primary.content.background}
          disabled={isLoading}
          throttleTime={2000}
          bold
        />
        <RoundButton
          style={{ margin: 5 }}
          onPress={() => rejectTrainingEvent(detail.id)}
          text={t('community_detail_btn_reject')}
          textColor={Colors.button.primary.text.text}
          color={Colors.button.primary.text.background}
          disabled={isLoading}
          outline
          round
          bold
        />
      </View>
    );
  };

  render1on1EventDetail = () => {
    const { detail, timezone, userId } = this.props;
    const participate = (detail.participate || []).find((e) => e.id !== userId);
    return (
      <View style={styles.detailWrapper}>
        <View style={styles.detailBox}>
          <Text style={styles.detailCount}>
            {participate ? participate.fullName : '-'}
          </Text>
          <Text style={styles.detailTitle}>
            {t('community_leaderboard_participant_lower')}
          </Text>
        </View>
        <View style={styles.detailBox}>
          <Text style={styles.detailCount}>{`${d.transformDate(
            detail.startDate,
            timezone,
            'MM/DD',
          )}`}</Text>
          <Text style={styles.detailTitle}>{t('__date')}</Text>
        </View>
        <View style={styles.detailBox}>
          <Text style={styles.detailCount}>{`${d.transformDate(
            detail.startDate,
            timezone,
            'HH:mm',
          )}`}</Text>
          <Text style={styles.detailTitle}>{t('__time')}</Text>
        </View>
      </View>
    );
  };

  renderGroupEventDetail = () => {
    const { detail, timezone } = this.props;
    return (
      <View style={styles.detailWrapper}>
        <View style={styles.detailBox}>
          <Text style={styles.detailCount}>{detail.participateCount}</Text>
          <Text style={styles.detailTitle}>
            {t('community_leaderboard_participant_lower')}
          </Text>
        </View>
        <View style={styles.detailBox}>
          <Text style={styles.detailCount}>{detail.classesCount}</Text>
          <Text style={styles.detailTitle}>{t('__class')}</Text>
        </View>
        <View style={styles.detailBox}>
          <Text style={styles.detailCount}>{`${d.transformDate(
            detail.startDate,
            timezone,
            'MM/DD',
          )} - ${d.transformDate(detail.dueDate, timezone, 'MM/DD')}`}</Text>
          <Text style={styles.detailTitle}>{t('__period')}</Text>
        </View>
      </View>
    );
  };

  onPressBookmark = (id, item) => {
    const { setCommunityClassBookmark, removeCommunityClassBookmark } = this.props;
    if (item.isBookmarked) {
      removeCommunityClassBookmark(id);
    } else {
      setCommunityClassBookmark(id);
    }
  };

  renderItem = (item, index) => {
    const { getCommunityClassDetail, onlyGetTrainingEvent, detail } = this.props;
    return (
      <View style={styles.cardWrapper} key={index}>
        <ClassCard
          data={item}
          size="full"
          onPress={() => {
            if (detail.eventType === 1) {
              onlyGetTrainingEvent(detail.id);
            }
            getCommunityClassDetail(
              item.id,
              detail.id,
              !detail.wasJoined,
              detail.isAccepted,
            );
          }}
          onPressBookmark={() => this.onPressBookmark(item.id, item)}
          isHover={item.isFinished}
        />
      </View>
    );
  };

  showDeleteBtn = () => {
    const { detail, userId } = this.props;
    if (detail.ownerId === userId && d.moment() < d.moment(detail.startDate)) {
      return true;
    }
    return false;
  };

  showLeaveBtn = () => {
    const { detail, userId } = this.props;
    if (detail.ownerId !== userId && detail.wasJoined && !detail.userIsFinish) {
      return true;
    } else {
      return false;
    }
  };

  onPressLeave = () => {
    Alert.alert(t('community_detail_leave_title'), t('community_detail_leave_content'), [
      {
        text: t('player_screen_cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('player_screen_quit'),
        onPress: this.onPressLeaveAction,
      },
    ]);
  };

  onPressDelete = () => {
    const { detail, deleteTrainingEvent } = this.props;
    Alert.alert(
      t('community_detail_delete_title'),
      t('community_detail_delete_content'),
      [
        {
          text: t('player_screen_cancel'),
          onPress: () => console.log('onPressDelete Pressed'),
          style: 'cancel',
        },
        {
          text: t('__delete'),
          onPress: () => deleteTrainingEvent(detail.id),
        },
      ],
    );
  };

  onPressLeaveAction = () => {
    const { leaveEvent, detail } = this.props;
    leaveEvent(detail.id);
  };

  onPressShare = () => {
    const {
      detail: { title },
    } = this.props;
    const shareOptions = {
      title: t('app_name'),
      message: t('share_event_message', { title, link: Config.SHOP_LINK }),
      url: 'https://johnsonfitnesslive.com/',
      subject: 'Share from Johnson@mirror',
    };
    Share.open(shareOptions);
  };

  render() {
    const { userId, detail, getEventLeaderBoard, sceneName } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back />
        <ScrollView>
          <Image
            source={detail.signedCoverImageObj || { uri: detail.signedCoverImage }}
            style={styles.headerImg}
          />
          <Text style={styles.title}>{detail.title}</Text>
          <Text style={styles.description}>{detail.description}</Text>

          {detail.eventType === 0 && this.renderInviteAndShareBlock()}

          {detail.eventType === 1 &&
            detail.ownerId !== userId &&
            !detail.isAccepted &&
            this.renderAcceptAndRejectBlock()}

          <HorizontalLine style={styles.hrLine} />

          {detail.eventType === 0 && this.renderGroupEventDetail()}
          {detail.eventType === 1 && this.render1on1EventDetail()}

          {detail.eventType === 0 && (
            <>
              <HeadLine
                title={t('community_detail_view_participant')}
                hrLine={false}
                style={styles.hrStyle}
                paddingHorizontal={16}
                onPress={() =>
                  sceneName.includes('Notification')
                    ? Actions.NotificationParticipantsScreen({ hideTabBar: true })
                    : Actions.ParticipantsScreen({ hideTabBar: true })
                }
              />
              <HeadLine
                title={t('community_detail_view_leaderboard')}
                hrLine={false}
                paddingTop={0}
                style={styles.hrStyle}
                paddingHorizontal={16}
                onPress={() => getEventLeaderBoard(detail.id)}
              />
            </>
          )}

          <HeadLine
            title={t('all_class')}
            marginTop={30}
            paddingHorizontal={16}
            style={styles.bottomTitle}
            hrLineStyle={styles.hrStyle}
          />

          {detail.trainingClasses &&
            detail.trainingClasses.map((e, i) => this.renderItem(e, i))}

          {this.showDeleteBtn() && (
            <BaseButton
              text={t('community_detail_btn_delete_this_event')}
              transparent
              height={Screen.scale(44)}
              textColor={Colors.button.primary.text.text}
              style={styles.deleteBtn}
              textStyle={styles.deleteTextStyle}
              onPress={this.onPressDelete}
            />
          )}
          {this.showLeaveBtn() && (
            <BaseButton
              text={t('community_detail_btn_leave_this_event')}
              transparent
              height={Screen.scale(44)}
              textColor={Colors.button.primary.text.text}
              style={styles.deleteBtn}
              textStyle={styles.deleteTextStyle}
              onPress={this.onPressLeave}
            />
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    sceneName: params.name,
    timezone: state.appState.currentTimeZone,
    detail: state.community.eventDetail,
    token: state.user.token,
    userId: state.user.userId,
    isLoading: state.appState.isLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onlyGetTrainingEvent: CommunityActions.onlyGetTrainingEvent,
        getCommunityClassDetail: CommunityActions.getCommunityClassDetail,
        setCommunityClassBookmark: CommunityActions.setCommunityClassBookmark,
        removeCommunityClassBookmark: CommunityActions.removeCommunityClassBookmark,
        joinEvent: CommunityActions.joinEvent,
        leaveEvent: CommunityActions.leaveEvent,
        getEventLeaderBoard: CommunityActions.getEventLeaderBoard,
        deleteTrainingEvent: CommunityActions.deleteTrainingEvent,
        rejectTrainingEvent: CommunityActions.rejectTrainingEvent,
        acceptTrainingEvent: CommunityActions.acceptTrainingEvent,
      },
      dispatch,
    ),
)(CommunityDetailScreen);
