import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Colors, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  headerImg: {
    width: '100%',
    height: '230@s',
    resizeMode: 'cover',
    backgroundColor: Colors.very_light_pink,
  },
  title: {
    ...Fonts.style.regular500,
    marginVertical: Metrics.baseMargin,
    width: '100%',
    textAlign: 'center',
  },
  description: {
    width: '100%',
    paddingHorizontal: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
    textAlign: 'center',
  },
  hrLine: {
    marginTop: '40@vs',
    marginBottom: Metrics.baseMargin,
  },
  btnBox: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  btnBoxAcceptAndReject: {
    width: Screen.width,
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  baseBtn: {
    width: Platform.isPad ? '214@s' : '182@s',
    height: '32@sr',
    backgroundColor: Colors.button.primary.content.background,
    borderRadius: 22,
  },
  baseGrayBtn: {
    width: '182@s',
    height: '32@s',
    backgroundColor: Colors.button.secondary.content.background,
    borderRadius: 22,
  },
  btnText: {
    ...Fonts.style.medium500,
  },
  borderBtn: {
    borderWidth: 2,
    borderColor: Colors.button.primary.outline.border,
    backgroundColor: Colors.button.primary.outline.background,
  },
  detailWrapper: {
    width: '100%',
    flexDirection: 'row',
    marginHorizontal: Metrics.baseMargin,
  },
  detailBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '126@s',
  },
  detailCount: {
    fontSize: Fonts.size.medium,
    fontWeight: 'bold',
    marginBottom: Metrics.baseMargin / 2,
  },
  detailTitle: {
    ...Fonts.style.extraSmall500,
  },
  bottomTitle: {
    paddingTop: 0,
    marginBottom: Metrics.baseMargin,
    paddingBottom: 0,
  },
  cardWrapper: {
    marginBottom: Metrics.baseMargin,
  },
  hrStyle: {
    marginBottom: 0,
  },
  deleteBtn: {
    marginBottom: Metrics.baseMargin,
    marginTop: '40@vs',
  },
  deleteTextStyle: {
    ...Fonts.style.medium500,
    textAlign: 'center',
  },
});
