import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { SafeAreaView, View, Text, FlatList, Image } from 'react-native';
import { bindActionCreators } from 'redux';

import { Classes, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseButton } from 'App/Components';
import { CommunityActions } from 'App/Stores';

import styles from './ParticipantsScreenStyle';

class ParticipantsScreen extends React.Component {
  static propTypes = {
    timezone: PropTypes.string,
    list: PropTypes.array,
    getUserDetail: PropTypes.func.isRequired,
  };

  static defaultProps = {
    timezone: 'Asia/Taipei',
    list: [],
  };

  state = {};

  componentDidMount() {
    __DEV__ && console.log('@ParticipantsScreen');
  }

  renderItem = ({ item, index }) => {
    const { getUserDetail } = this.props;
    console.log('item =>', item);
    return (
      <View style={styles.wrapperBox}>
        <BaseButton
          transparent
          style={styles.itemBox}
          onPress={() => getUserDetail(item.id)}
        >
          <View style={styles.leftBox}>
            <Image
              source={!isEmpty(item.avatar) ? { uri: item.avatar } : Images.defaultAvatar}
              style={styles.itemImg}
            />
            <Text style={styles.itemName}>{item.fullName}</Text>
          </View>
          {item.currentStatus === 2 && (
            <View style={styles.friendBox}>
              <Text style={styles.friendTag}>{t('__friend')}</Text>
            </View>
          )}
        </BaseButton>
      </View>
    );
  };

  render() {
    const { list } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar navTitle={t('community_leaderboard_participant_lower')} back />
        <View style={styles.headerBox}>
          <Text style={styles.headerTitle}>{t('members')}</Text>
        </View>
        <FlatList data={list} renderItem={this.renderItem} />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    timezone: state.appState.currentTimeZone,
    list: state.community.eventDetail.participate,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getUserDetail: CommunityActions.getUserDetail,
      },
      dispatch,
    ),
)(ParticipantsScreen);
