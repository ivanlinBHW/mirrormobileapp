import React from 'react';
import PropTypes from 'prop-types';
import { has, isString } from 'lodash';
import { connect } from 'react-redux';
import { SafeAreaView, View, Text, FlatList } from 'react-native';
import { bindActionCreators } from 'redux';

import { Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d } from 'App/Helpers';
import {
  SecondaryNavbar,
  FlatListEmptyLoading,
  CachedImage as Image,
} from 'App/Components';

import styles from './LeaderBoardScreenStyle';
class LeaderBoardScreen extends React.Component {
  static propTypes = {
    timezone: PropTypes.string,
    list: PropTypes.array,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    timezone: 'Asia/Taipei',
    isLoading: false,
  };

  state = {};

  componentDidMount() {
    __DEV__ && console.log('@LeaderBoardScreen');
  }

  getAvatarUri = (item) => {
    console.log('item =>', item);
    if (has(item, 'user.avatar') && isString(item.user.avatar)) {
      return item.user.avatar;
    }
    return '';
  };

  renderItem = ({ item, index }) => {
    const { timezone } = this.props;
    return (
      <View style={styles.itemBox}>
        <Text style={styles.itemIndex}>{index + 1}</Text>
        <Image
          source={{ uri: this.getAvatarUri(item) }}
          style={styles.itemImg}
          imageSize="2x"
          imageType="s"
        />
        <Text style={styles.itemName}>{item.user.fullName}</Text>
        <Text style={styles.itemTime}>
          {d
            .moment(d.transformDate(item.completedAt, timezone))
            .format('MMM DD [at] HH:mm a')}
        </Text>
      </View>
    );
  };

  renderEmpty = () => {
    const { isLoading } = this.props;
    return (
      <View style={styles.emptyHeight}>
        <FlatListEmptyLoading
          content={t('community_detail_leader_board_content')}
          isLoading={isLoading}
        />
      </View>
    );
  };

  render() {
    const { list } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar navTitle={t('community_leaderboard')} back />
        <View style={styles.headerBox}>
          <Text style={styles.headerTitle}>
            {`${t('community_leaderboard_participant_lower')}`.toLocaleUpperCase()}
          </Text>
          <Text style={styles.headerTitle}>
            {t('community_leaderboard_completed_time')}
          </Text>
        </View>
        <FlatList
          data={list}
          renderItem={this.renderItem}
          ListEmptyComponent={this.renderEmpty()}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    timezone: state.appState.currentTimeZone,
    list: state.community.leaderBoard,
    isLoading: state.appState.isLoading,
  }),
  (dispatch) => bindActionCreators({}, dispatch),
)(LeaderBoardScreen);
