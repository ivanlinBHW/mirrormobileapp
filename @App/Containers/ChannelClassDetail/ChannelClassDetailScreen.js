import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEmpty, isString } from 'lodash';
import { bindActionCreators } from 'redux';
import { View, Text } from 'react-native';

import {
  InstructorActions,
  MirrorActions,
  ClassActions,
  PlayerActions,
} from 'App/Stores';
import { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import { translate as t } from 'App/Helpers/I18n';
import { SquareButton, CachedImage as ImageBackground } from 'App/Components';
import { Dialog, Subscription } from 'App/Helpers';
import { Colors } from 'App/Theme';

import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';
import styles from './ChannelClassDetailScreenStyle';
import ClassDetailView from '../ClassDetail/ClassDetailView';

class ChannelClassDetailScreen extends React.Component {
  static propTypes = {
    playerDetail: PropTypes.object.isRequired,
    startClass: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
    setInstructorId: PropTypes.func.isRequired,
    routeName: PropTypes.string.isRequired,
    resumeClass: PropTypes.func.isRequired,
    currentRouteName: PropTypes.string,
    onMirrorCommunicate: PropTypes.func.isRequired,
    mirrorOptionMotionCapture: PropTypes.bool.isRequired,

    isConnected: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isOnlySee: PropTypes.bool,
    prevRoute: PropTypes.string,
    mainSubscription: PropTypes.object,
    updatePlayerStore: PropTypes.func.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
    isSubscriptionExpired: PropTypes.bool.isRequired,
    hasCoachVI: PropTypes.bool.isRequired,
    hasDistanceAdjustment: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    isOnlySee: false,
  };

  startClass = () => {
    const {
      token,
      playerDetail,
      startClass,
      currentRouteName,
      isMotionCaptureEnabled,
    } = this.props;

    if (isMotionCaptureEnabled) {
      Dialog.requestDistanceAdjustmentWhenEnableMotionCapture(() =>
        startClass(playerDetail.id, token, currentRouteName),
      );
    } else {
      startClass(playerDetail.id, token, currentRouteName);
    }
  };

  resumeClass = () => {
    const { token, playerDetail, resumeClass } = this.props;
    resumeClass(playerDetail.trainingClassHistory.id, token, playerDetail.id);
  };

  onPressInstructor = () => {
    const {
      playerDetail: { instructor },
      setInstructorId,
      token,
    } = this.props;
    setInstructorId(instructor.id, token);
  };

  renderClassCard = () => {
    const { playerDetail } = this.props;
    return (
      <ImageBackground
        resizeMode="cover"
        source={playerDetail.signedCoverImageObj}
        style={styles.classImgWidth}
      >
        <View style={styles.classFragment}>
          <View style={styles.classLeftBox}>
            <View style={styles.classLeft}>
              <Text style={styles.classSecondTitle}>{t('__class')}</Text>
              <Text style={styles.classTitle}>
                {isString(playerDetail.title) && playerDetail.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.classRightBox}>
            <View style={styles.classRight}>
              <View style={styles.classBoxStyle}>
                <Text style={styles.classText}>
                  {t(`search_lv${playerDetail.level}`)}
                </Text>
              </View>

              <View style={styles.classBoxStyle}>
                <Text style={styles.classText}>
                  {playerDetail.duration / 60} {t('min')}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  };

  renderStartButton = () => {
    const {
      isSubscriptionExpired,
      playerDetail,
      isLoading,
      mainSubscription,
    } = this.props;

    if (isEmpty(mainSubscription)) {
      return (
        <SquareButton
          color={Colors.primary}
          onPress={Dialog.requestAccountSubscriptionCodeAlert(this.startClass)}
          disabled={isLoading}
          text={t('class_detail_request_account_subscription_alert_title')}
        />
      );
    } else if (isSubscriptionExpired) {
      return (
        <SquareButton
          color={Colors.primary}
          onPress={Dialog.requestAccountSubscriptionExpiredAlert(this.startClass)}
          disabled={isLoading}
          text={t('class_detail_request_account_subscription_expired_title')}
        />
      );
    } else if (!playerDetail.isSubscribed) {
      return (
        <SquareButton
          color={Colors.primary}
          onPress={Dialog.requestClassSubscriptionAlert(this.startClass)}
          disabled={isLoading}
          text={t('class_detail_request_class_subscription_alert_title')}
        />
      );
    }

    if (
      playerDetail.trainingClassHistory &&
      playerDetail.trainingClassHistory.pausedStepId
    ) {
      return (
        <SquareButton
          color={Colors.black}
          onPress={this.startClass}
          text={t('class_detail_start_class')}
        />
      );
    } else {
      return (
        <SquareButton
          color={Colors.primary}
          onPress={() => Dialog.showConfirmCameraUsageAlert(this.startClass)}
          text={t('class_detail_start_class')}
        />
      );
    }
  };

  renderResumeButton = () => {
    const { isSubscriptionExpired, playerDetail, mainSubscription } = this.props;

    if (
      isSubscriptionExpired ||
      isEmpty(mainSubscription) ||
      !playerDetail.isSubscribed
    ) {
      return null;
    }
    if (
      !isEmpty(playerDetail.trainingClassHistory) &&
      isString(playerDetail.trainingClassHistory.pausedStepId)
    ) {
      return (
        <SquareButton
          onPress={() => Dialog.showConfirmCameraUsageAlert(this.resumeClass)}
          color={Colors.primary}
          text={t('class_detail_resume_class')}
        />
      );
    }
  };

  onPressBack = () => {
    const { onMirrorCommunicate, playerDetail } = this.props;
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS_PREVIEW, {
      classId: playerDetail.id,
      classType: 'on-demand',
    });
  };

  render() {
    const {
      playerDetail,
      isLoading,
      isConnected,
      isOnlySee = false,
      prevRoute,
      mainSubscription,
      updatePlayerStore,
      isMotionCaptureEnabled,
      setInstructorId,
      mirrorOptionMotionCapture,
      hasCoachVI,
      hasDistanceAdjustment,
    } = this.props;
    return (
      <ClassDetailView
        hasCoachVI={hasCoachVI}
        setInstructorId={setInstructorId}
        playerDetail={playerDetail}
        isOnlySee={isOnlySee}
        isLoading={isLoading}
        isConnected={isConnected}
        prevRouteName={prevRoute}
        mainSubscription={mainSubscription}
        backToRouteName={'ChannelDetailScreen'}
        renderResumeButton={this.renderResumeButton}
        renderStartButton={this.renderStartButton}
        onPressBack={this.onPressBack}
        isMotionCaptureEnabled={isMotionCaptureEnabled}
        onMotionCaptureChanged={updatePlayerStore}
        mirrorOptionMotionCapture={mirrorOptionMotionCapture}
      />
    );
  }
}

export default connect(
  (state, params) => ({
    playerDetail: state.player.detail,
    isConnected: state.mirror.isConnected,
    isLoading: state.appState.isLoading,

    prevRoute: state.appRoute.prevRoute,
    mainSubscription: state.user.mainSubscription,
    isMotionCaptureEnabled:
      'isMotionCaptureEnabled' in state.player
        ? state.player.isMotionCaptureEnabled
        : false,
    mirrorOptionMotionCapture: state.mirror.options[MirrorOptions.MOTION_TRACKER],

    isSubscriptionExpired: Subscription.isExpired(
      state.user.mainSubscription,
      state.player.detail,
    ),
    hasDistanceAdjustment: filterMirrorHasFeatureOption(state, 'distanceAdjustment'),
    hasCoachVI: filterMirrorHasFeatureOption(state, 'coachVI'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        startClass: ClassActions.startClass,
        resumeClass: ClassActions.resumeClass,
        setInstructorId: InstructorActions.setInstructorId,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        updatePlayerStore: PlayerActions.updatePlayerStore,
      },
      dispatch,
    ),
)(ChannelClassDetailScreen);
