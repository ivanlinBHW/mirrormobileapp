import React from 'react';
import PropTypes from 'prop-types';
import { isString } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  View,
  Text,
  Alert,
  ScrollView,
  SafeAreaView,
  ImageBackground,
} from 'react-native';

import { I18n, Date as d } from 'App/Helpers';
import { Classes, Images, Colors } from 'App/Theme';
import {
  BaseIcon,
  HeadLine,
  StripeView,
  RoundLabel,
  RatingSelect,
  LayeredText,
  SquareButton,
  BaseImageButton,
  SecondaryNavbar,
} from 'App/Components';
import ClassActions from 'App/Stores/Class/Actions';
import LiveActions from 'App/Stores/Live/Actions';
import { ProgressActions } from 'App/Stores';
import styles from './WorkoutDetailStyle';

const t = I18n.translate;

class WorkoutDetailScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    data: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired,
    deleteClassHistory: PropTypes.func.isRequired,
    setWorkoutDetailBookmark: PropTypes.func.isRequired,
    deleteWorkoutDetailBookmark: PropTypes.func.isRequired,
    getClassDetail: PropTypes.func.isRequired,
    getLiveDetail: PropTypes.func.isRequired,
    workoutType: PropTypes.string.isRequired,
    currentTimeZone: PropTypes.string.isRequired,
  };

  handleDeleteWorkout = (id, workoutType) => () => {
    const { deleteClassHistory } = this.props;
    Alert.alert(
      t('workout_detail_confirm_delete_title'),
      t('workout_detail_confirm_delete_desc'),
      [
        {
          text: t('__cancel'),
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: t('__delete'),
          onPress: () => deleteClassHistory({ id, workoutType }),
          style: 'destructive',
        },
      ],
    );
  };

  handleBookmarkClick = ({ id, isBookmarked, workoutType }) => () => {
    const { setWorkoutDetailBookmark, deleteWorkoutDetailBookmark } = this.props;
    return isBookmarked
      ? deleteWorkoutDetailBookmark(id, workoutType)
      : setWorkoutDetailBookmark(id, workoutType);
  };

  renderNavBar = ({ id, isBookmarked, workoutType }) => {
    return (
      <SecondaryNavbar
        navTitle={t('workout_detail_nav_title')}
        navRightComponent={
          <BaseImageButton
            source={isBookmarked ? Images.bookmark_solid_fixed : Images.bookmark_black}
            imageHeight={32}
            height={32}
            color={Colors.black}
            onPress={this.handleBookmarkClick({ id, isBookmarked, workoutType })}
          />
        }
        back
      />
    );
  };

  renderHeader = ({
    endedAt,
    currentTimeZone,
    title,
    level,
    originDuration,
    instructor,
  }) => {
    return (
      <View style={Classes.rowMain}>
        <View style={[styles.fill]}>
          <Text style={styles.workoutName}>{isString(title) && title.toUpperCase()}</Text>
          <Text style={styles.workoutInstructorName}>{instructor.title}</Text>
          <Text style={styles.workoutPerformedAt}>
            <BaseIcon name="check-circle" color={Colors.fontIcon.primary} size={12} />{' '}
            {t('workout_detail_completed_in')}
            {d.formatDate(d.transformDate(endedAt, currentTimeZone), d.FULL_DATE_FORMAT)}
          </Text>
        </View>
        <View style={[Classes.fillColMain, Classes.crossEnd, Classes.mainStart]}>
          <RoundLabel text={this.renderDifficulty(level)} />
          <RoundLabel text={`${d.mm(originDuration)} ${t('min')}`} />
        </View>
      </View>
    );
  };

  renderWorkoutDetail = (calories, duration) => {
    return (
      <View style={Classes.fill}>
        <HeadLine
          title={t('workout_detail_stats')}
          paddingTop={8}
          paddingHorizontal={16}
        />
        <View style={[Classes.rowMain]}>
          <LayeredText title={d.hhmmss(duration)} subtitle={t('duration')} />
          <LayeredText title={calories} subtitle={t('calories')} />
        </View>
      </View>
    );
  };

  renderFeedback = (feedback) => {
    console.log('feedback=>', feedback);
    return (
      <View style={Classes.fill}>
        <HeadLine
          title={t('workout_detail_feedback')}
          paddingTop={8}
          paddingHorizontal={16}
        />
        <StripeView
          stripeHeight={64}
          stripeLightColor={Colors.white}
          stripeDarkColor={Colors.white}
          stripes={this.renderStripesView(feedback)}
          stripStyle={styles.stripStyle}
        />
      </View>
    );
  };

  renderDifficulty = (feelRate) => {
    switch (feelRate) {
      case 1:
        return t('setting_ability_beginner');
      case 2:
        return t('setting_ability_intermediate');
      case 3:
        return t('setting_ability_advanced');
      case 4:
        return t('setting_ability_export');
      default:
        return feelRate;
    }
  };

  renderStripesView = ({ classRate, instructorRate, feelRate, userComment }) => {
    console.log('userComment ->', userComment);
    return [
      {
        leftComponent: (
          <Text style={styles.box}>{t('workout_detail_workout_rating')}</Text>
        ),
        rightComponent: (
          <RatingSelect style={styles.selectStyle} rating={classRate} starSize={16} />
        ),
        disabled: true,
      },
      {
        leftComponent: (
          <Text style={styles.box}>{t('workout_detail_instructor_rating')}</Text>
        ),
        rightComponent: (
          <RatingSelect
            style={styles.selectStyle}
            rating={instructorRate}
            starSize={16}
          />
        ),
        disabled: true,
      },
      {
        leftComponent: <Text style={styles.box}>{t('workout_detail_difficulty')}</Text>,
        rightComponent: (
          <Text style={styles.txtComment}>{this.renderDifficulty(feelRate)}</Text>
        ),
        disabled: true,
      },
      {
        leftComponent: <Text style={styles.box}>{t('workout_detail_comments')}</Text>,
        rightComponent: (
          <Text style={styles.txtComment} ellipsizeMode="tail" numberOfLines={3}>
            {userComment}
          </Text>
        ),
        disabled: true,
      },
    ];
  };

  onPressDetail = () => {
    const { data, token, workoutType, getClassDetail, getLiveDetail } = this.props;
    if (workoutType === 'live-class') {
      getLiveDetail(data.liveClassId);
    }
    if (workoutType === 'training-class') {
      getClassDetail(data.trainingClass.id, token);
    }
  };

  render() {
    const source =
      this.props.workoutType === 'training-class' ? 'trainingClass' : 'liveClass';
    console.log('this.props=>', this.props);
    console.log('source=>', source);
    const {
      currentTimeZone,
      workoutType,
      data: {
        id: historyId,
        feedback,
        endedAt,
        duration,
        totalDuration,
        totalCalories,
        [source]: {
          id = '',
          isBookmarked = false,
          signedCoverImage = 'https://via.placeholder.com/368x207',
          coverImage,
          calories,
          duration: originDuration,
          instructor,
          title,
          level,
        } = {},
      },
    } = this.props;
    const calcCalories = parseInt(calories * (duration / originDuration), 10);
    return (
      <SafeAreaView style={Classes.fill}>
        {this.renderNavBar({ id, isBookmarked, workoutType })}

        <ScrollView style={Classes.fill}>
          <ImageBackground
            source={{
              uri: signedCoverImage || coverImage,
            }}
            style={styles.imgStyle}
          />

          <View style={[styles.contentBox]}>
            {this.renderHeader({
              currentTimeZone,
              instructor,
              endedAt,
              title,
              level,
              originDuration,
            })}
            <SquareButton
              style={styles.btnResume}
              onPress={this.onPressDetail}
              text={t('workout_detail_goto_workout')}
            />
          </View>

          {this.renderWorkoutDetail(totalCalories, totalDuration)}

          {this.renderFeedback(feedback)}

          <View style={styles.footer}>
            <SquareButton
              outline
              transparent
              text={t('workout_detail_delete_workout')}
              textColor={Colors.button.primary.text.text}
              style={styles.btnDelete}
              onPress={this.handleDeleteWorkout(historyId, workoutType)}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    currentTimeZone: state.appState.currentTimeZone,
    user: state.user,
    data: state.progress.detail,
    token: state.user.token,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        deleteClassHistory: ProgressActions.deleteClassHistory,
        setWorkoutDetailBookmark: ProgressActions.setWorkoutDetailBookmark,
        deleteWorkoutDetailBookmark: ProgressActions.deleteWorkoutDetailBookmark,
        getClassDetail: ClassActions.getClassDetail,
        getLiveDetail: LiveActions.getLiveDetail,
      },
      dispatch,
    ),
)(WorkoutDetailScreen);
