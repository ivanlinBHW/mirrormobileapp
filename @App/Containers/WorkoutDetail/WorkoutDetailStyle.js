import { Platform } from 'react-native';
import { StyleSheet, Screen } from 'App/Helpers';
import { Classes, Colors, Styles, Metrics, Fonts } from 'App/Theme';

export default StyleSheet.create({
  container: {
    ...Classes.fill,
  },
  baseMargin: {
    marginHorizontal: Metrics.baseMargin,
  },
  btnResume: {
    borderRadius: '6@s',
    marginVertical: Metrics.baseVerticalMargin,
  },
  workoutName: {
    ...Fonts.style.regular500,
    color: Colors.black,
  },
  workoutInstructorName: {
    ...Fonts.style.regular500,
    color: Colors.brownGrey,
  },
  workoutPerformedAt: {
    ...Fonts.style.small,
    color: Colors.titleText.active,
    marginTop: '3@vs',
  },
  txtComment: {
    color: Colors.black,
    fontSize: Fonts.size.small,
  },
  navBar: {
    ...Styles.navBar,
  },
  navBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  layoutBox: {
    flex: 1,
  },
  footer: {
    ...Classes.center,
    paddingVertical: Metrics.baseVerticalMargin / 2,
    width: '100%',
  },
  navVoiceIcon: {
    width: '24@s',
    height: '24@vs',
    marginRight: '21@s',
  },
  navSettingIcon: {
    width: '30@s',
    height: '30@vs',
    marginRight: '13@s',
  },
  marginBetween: {
    marginRight: '16@s',
    marginLeft: '16@s',
  },
  imgStyle: {
    width: '100%',
    height: Platform.isPad ? Screen.verticalScale(270) : Screen.verticalScale(230),
  },
  contentBox: {
    marginVertical: Metrics.baseVerticalMargin,
    marginHorizontal: Metrics.baseHorizontalMargin,
  },
  stripStyle: {
    paddingLeft: Metrics.baseMargin,
    paddingRight: Metrics.baseMargin,
  },
  selectStyle: {
    marginLeft: Metrics.baseMargin / 2,
  },
});
