import { Colors, Fonts, Images } from 'App/Theme';
import { Screen, Date as d } from 'App/Helpers';

import { BaseCalendar } from 'App/Components';
import { Platform } from 'react-native';
import { ProgressActions } from 'App/Stores';
import PropTypes from 'prop-types';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { debounce } from 'lodash';
import { translate as t } from 'App/Helpers/I18n';

class CalendarView extends React.PureComponent {
  static propTypes = {
    historyDate: PropTypes.array.isRequired,
    getProgressDate: PropTypes.func.isRequired,
    getProgressOverview: PropTypes.func.isRequired,
    currentTimeZone: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isCalenderExpanded: PropTypes.bool.isRequired,
    locale: PropTypes.string.isRequired,
  };

  static defaultProps = {
    lastStartDate: undefined,
    lastEndDate: undefined,
    isCalenderExpanded: false,
  };

  state = {
    current: new Date(),
    currentDate: new Date(),
    calendarWeeklyTitle: '',
    calendarMonthlyTitle: '',
    calendarTheme: {
      dayHeaderTextStyle: {
        textTransform: 'uppercase',
      },
      textDayHeaderFontFamily: Fonts.fontFamily.default,
      textDayHeaderFontSize: Fonts.style.small500.fontSize,
      textDayHeaderFontWeight: Fonts.style.small500.fontWeight,
      textSectionTitleColor: Colors.black,
      todayBackgroundColor: Colors.white,
      todayTextColor: Colors.primary,
      selectedDayBackgroundColor: Colors.white,
      selectedDayTextColor: Colors.black,
      dotColor: Colors.goldenYellow,
      dotTextColor: Colors.white,
      selectedDotTextColor: Colors.white,
      selectedDotColor: Colors.goldenYellow,
      dotStyle: {
        position: 'absolute',
        top: Platform.select({
          ios: Screen.verticalScale(1),
          android: Screen.verticalScale(-1),
        }),
        height: Platform.select({
          ios: Platform.isPad ? Screen.scale(18) : Screen.verticalScale(30),
          android: Platform.isPad ? Screen.scale(18) : Screen.verticalScale(30),
        }),
        width: Platform.select({
          ios: Platform.isPad ? Screen.scale(18) : Screen.verticalScale(30),
          android: Platform.isPad ? Screen.scale(18) : Screen.verticalScale(30),
        }),
        borderRadius: Platform.isPad ? Screen.scale(9) : Screen.verticalScale(15),
        zIndex: -10,
      },
      zIndex: 999999,
    },
    calendarStyle: {
      backgroundColor: Colors.white,
      display: 'flex',
      zIndex: 999999,
    },
    shouldDisableLeftArrow: false,
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter CalendarView!');
    this.onMonthChanged();
    this.onDateChanged();
  }

  onExtendCalender = () => {};

  shouldDisableLeftArrow = () => {
    const { isCalenderExpanded } = this.props;
    const { currentDate } = this.state;
    const minDate = d.moment();
    const lastDate = d
      .moment(currentDate, 'YYYY/MM/DD')
      .subtract(1, isCalenderExpanded ? 'month' : 'week')
      .endOf(isCalenderExpanded ? 'month' : 'week');

    const result = d.moment(lastDate).isBefore(minDate, 'year');
    return result;
  };

  shouldDisableRightArrow = () => {
    const { isCalenderExpanded } = this.props;
    const { currentDate } = this.state;

    if (isCalenderExpanded) {
      const maxDate = d.moment().endOf('month');
      const nextDate = d.moment(currentDate, 'YYYY/MM/DD').add(1, 'month');

      return d.moment(maxDate).isBefore(nextDate, 'month');
    } else {
      const maxDate = d.moment().endOf('week');
      const nextDate = d.moment(currentDate, 'YYYY/MM/DD').add(1, 'week');
      return d.moment(maxDate).isBefore(nextDate, 'week');
    }
  };

  getCalendarTitle = () => {
    const { isCalenderExpanded } = this.props;
    const { currentDate } = this.state;

    if (isCalenderExpanded) {
      const mmmm = t(d.formatDate(currentDate, 'MMMM'));
      const yyyy = d.formatDate(currentDate, 'YYYY');
      return `${mmmm} ${yyyy}`;
    } else {
      const { startDate, endDate } = d.getWeekRangeByDate(currentDate);
      const month = d.moment(startDate, 'YYYY/MM/DD').format('MMMM');
      const mmmm = t(month);
      return `${mmmm} ${startDate.split('/')[2]}-${endDate.split('/')[2]}`;
    }
  };

  onMonthChanged = (date = new Date()) => {
    const currentDate = date.dateString ? new Date(date.dateString) : date;

    const { startDate, endDate } = d.getMonthRangeByDate(currentDate);
    const { startDate: lastStartDate, endDate: lastEndDate } = this.state;
    this.setState(
      {
        currentDate: date.dateString ? new Date(date.dateString) : date,
        startDate,
        endDate,
      },
      () => {
        if (lastStartDate !== startDate || lastEndDate !== endDate) {
          this.handleFetchDate(startDate, endDate);
        }
      },
    );
  };

  onDateChanged = (date = new Date()) => {
    const today = d.moment();
    const isAfterDate = d.isAfterDate(d.moment(date, 'YYYY/MM/DD'), today);

    if (!isAfterDate) {
      const { startDate: lastStartDate, endDate: lastEndDate } = this.state;
      const { startDate, endDate } = d.getWeekRangeByDate(date);
      this.setState(
        {
          currentDate: date.dateString ? new Date(date.dateString) : date,
          startDate,
          endDate,
        },
        () => {
          if (lastStartDate !== startDate || lastEndDate !== endDate) {
            this.handleFetchOverview(startDate, endDate);
          }
        },
      );
    }
  };

  handleFetchDate = debounce((startDate, endDate) => {
    const { getProgressDate } = this.props;
    getProgressDate(startDate, endDate);
  }, 400);

  handleFetchOverview = debounce((startDate, endDate) => {
    const { getProgressOverview } = this.props;
    getProgressOverview(startDate, endDate);
  }, 400);

  getMarkedDates = () => {
    const marked = {};
    const { isCalenderExpanded } = this.props;
    const { historyDate, currentTimeZone } = this.props;
    const today = d.moment();
    historyDate.forEach((e) => {
      marked[d.toTz(d.fromUTC(e), currentTimeZone, 'YYYY-MM-DD')] = {
        marked: true,
      };
    });
    d.generateDaysByNow(new Date(), isCalenderExpanded ? 30 : 7).forEach((e) => {
      const isDisabled = d.isAfterDate(d.fromUTC(e), today);
      marked[d.toTz(d.fromUTC(e), currentTimeZone, 'YYYY-MM-DD')] = {
        disabled: isDisabled,
      };
    });
    return marked;
  };

  render() {
    const { isLoading, isCalenderExpanded, locale } = this.props;
    const {
      currentDate,
      calendarStyle,
      calendarTheme,
    } = this.state;
    return (
      <>
        <BaseCalendar
          locale={locale}
          current={currentDate}
          calendarHeight={Screen.verticalScale(500)}
          calendarTheme={calendarTheme}
          calendarStyle={calendarStyle}
          onDateChanged={this.onDateChanged}
          onMonthChanged={this.onMonthChanged}
          onExpandStateChange={this.onExtendCalender}
          displayLoadingIndicator={isLoading}
          leftArrowImageSource={Images.nav_back}
          rightArrowImageSource={Images.nav_next}
          expanded={isCalenderExpanded}
          dateTitle={this.getCalendarTitle()}
          markedDates={this.getMarkedDates()}
          disableRightArrow={this.shouldDisableRightArrow()}
          allowShadow
          hideKnob
        />
      </>
    );
  }
}

export default connect(
  (state, params) => ({
    currentTimeZone: state.appState.currentTimeZone,
    isLoading: state.appState.isLoading,
    historyDate: state.progress.historyDate,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getProgressDate: ProgressActions.getProgressDate,
        getProgressOverview: ProgressActions.getProgressOverview,
      },
      dispatch,
    ),
)(CalendarView);
