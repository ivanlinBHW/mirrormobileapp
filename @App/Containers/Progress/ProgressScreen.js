import {
  AchievementItem,
  AutoHeaderScrollView,
  BaseButton,
  BaseImageButton,
  HeadLine,
  LayeredText,
  PrimaryNavbar,
  ProgressBarChart,
} from 'App/Components';
import { Classes, Colors, Images } from 'App/Theme';
import { FlatList, Text, View } from 'react-native';
import { Screen, Date as d } from 'App/Helpers';

import CalendarView from './CalendarView';
import { ProgressActions } from 'App/Stores';
import PropTypes from 'prop-types';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isEqual, first } from 'lodash';
import styles from './ProgressScreenStyle';
import { translate as t } from 'App/Helpers/I18n';

class ProgressScreen extends React.Component {
  static propTypes = {
    sceneKey: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,
    historyDate: PropTypes.array.isRequired,
    overview: PropTypes.object.isRequired,
    getAchievements: PropTypes.func.isRequired,
    getProgressDetail: PropTypes.func.isRequired,
    currentTimeZone: PropTypes.string.isRequired,
    currentLocale: PropTypes.object.isRequired,
    lastStartDate: PropTypes.string,
    lastEndDate: PropTypes.string,
    routeName: PropTypes.string.isRequired,
    prevRoute: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
  };

  static defaultProps = {};

  state = {
    current: d.moment().format('YYYY-MM-DD'),
    currentDate: new Date(),
    isCalenderExpanded: false,
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter ProgressScreen!');
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { sceneKey, routeName } = this.props;
    return (
      routeName === sceneKey &&
      (!isEqual(this.props, nextProps) || !isEqual(this.state, nextState))
    );
  }

  onExtendCalender = () => {
    this.setState((state) => ({
      isCalenderExpanded: !state.isCalenderExpanded,
    }));
  };

  onGoToWorkoutDetail = (item) => () => {
    const { getProgressDetail, user } = this.props;
    getProgressDetail({ id: item.id, token: user.token, workoutType: item.type });
  };

  renderChart = (data, lastStartDate) => {
    const { weekdays } = d.getWeekRangeByDate(
      d.moment(lastStartDate, 'YYYY/MM/DD'),
      'MM/DD',
    );
    const chartHeight = Screen.verticalScale(235);
    const chartData = data.map((e) => Math.ceil(e / 60));
    return (
      <View style={styles.chartWrapper}>
        <ProgressBarChart data={chartData} height={chartHeight} weekdays={weekdays} />
      </View>
    );
  };

  renderWeekProgress = ({ amount = 0, duration = 0, calories = 0 }) => {
    return (
      <View style={Classes.rowMain}>
        <LayeredText title={amount} subtitle={t('workouts')} />
        <LayeredText title={d.hhmmss(duration)} subtitle={t('duration')} />
        <LayeredText title={calories} subtitle={t('calories')} />
      </View>
    );
  };

  renderAchievement = (achievements) => {
    const renderItem = ({ item }) => {
      return (
        <View style={styles.achievementWrapper}>
          <AchievementItem
            name={item.name}
            date={d.formatDate(`${item.createdAt}`, d.ACHIEVEMENT_DATE_FORMAT)}
            title={item.title}
            description={item.description}
            active
          />
        </View>
      );
    };
    return (
      <View style={[Classes.rowMain, styles.achievementList]}>
        <FlatList
          horizontal
          data={achievements}
          renderItem={renderItem}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  };

  renderWorkoutActivity = (activities, currentTimeZone) => {
    const renderItem = ({ item }) => {
      return (
        <BaseButton
          style={[Classes.fillRow, Classes.fullHeight, styles.borderBottom]}
          key={item.id}
          onPress={this.onGoToWorkoutDetail(item)}
          throttleTime={500}
          transparent
        >
          <View style={styles.workoutActivityWrapper}>
            <Text style={styles.workoutActivityTextDate}>
              {d.formatDate(d.transformDate(item.endedAt, currentTimeZone))}
            </Text>
            <Text style={styles.workoutActivityTextTitle}>{item.title}</Text>
            <View style={styles.workoutActivityTextWrapper}>
              <Text style={styles.workoutActivityTextEndTime}>
                {d.formatDate(
                  d.transformDate(item.endedAt, currentTimeZone),
                  d.MINUTE_FORMAT,
                )}
              </Text>
              <Text style={styles.workoutActivityTextDuration}>
                {d.mmss(item.duration)} {t('min')}
              </Text>
              <Text style={styles.workoutActivityTextCalories}>
                {item.calories} {t('cal')}
              </Text>
            </View>
          </View>
          <View style={styles.arrowWrapper}>
            <BaseImageButton source={Images.next} imageHeight={12} color={Colors.black} />
          </View>
        </BaseButton>
      );
    };
    return (
      <View style={[styles.workoutActivityList]}>
        {activities && activities.map((item) => renderItem({ item }))}
      </View>
    );
  };

  render() {
    const {
      overview: {
        summary = {},
        activities = [],
        achievements = [],
        history = [1, 1, 1, 1, 1, 1, 1, 1],
      },
      getAchievements,
      currentTimeZone,
      lastStartDate,
      currentLocale,
    } = this.props;
    const { isCalenderExpanded } = this.state;
    return (
      <View style={styles.container}>
        <AutoHeaderScrollView
          headerComponent={
            <View style={styles.navBarWrapper}>
              <PrimaryNavbar
                navText={t('progress')}
                navComponent={
                  <View style={[Classes.mainEnd, Classes.crossEnd]}>
                    <BaseImageButton
                      source={Images.schedule}
                      imageHeight={32}
                      color={Colors.black}
                      onPress={this.onExtendCalender}
                      throttleTime={150}
                    />
                  </View>
                }
              />
            </View>
          }
          fixedComponent={
            <CalendarView
              isCalenderExpanded={isCalenderExpanded}
              locale={currentLocale.languageCode}
            />
          }
        >
          <View style={styles.marginForPad} />
          <HeadLine paddingHorizontal={16} title={t('progress_weekly_records')} />
          {this.renderWeekProgress(summary)}
          <HeadLine paddingHorizontal={16} title={t('progress_weekly_records')} />
          {this.renderChart(history, lastStartDate)}
          {/* <HeadLine
            paddingHorizontal={16}
            title={t('progress_achievement')}
            onViewAll={getAchievements}
          />
          {this.renderAchievement(achievements)} */}
          <HeadLine paddingHorizontal={16} title={t('progress_workout_history')} />
          {this.renderWorkoutActivity(activities, currentTimeZone)}
        </AutoHeaderScrollView>
      </View>
    );
  }
}

export default connect(
  (state, params) => ({
    user: state.user,
    sceneKey: params.name,
    overview: state.progress.overview,
    lastEndDate: state.progress.endDate,
    isLoading: state.appState.isLoading,
    prevRoute: state.appRoute.prevRoute,
    routeName: state.appRoute.routeName,
    lastStartDate: state.progress.startDate,
    historyDate: state.progress.historyDate,
    currentTimeZone: state.appState.currentTimeZone,
    currentLocale: first(state.appState.currentLocales) || {},
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getAchievements: ProgressActions.getAchievements,
        getProgressDetail: ProgressActions.getProgressDetail,
      },
      dispatch,
    ),
)(ProgressScreen);
