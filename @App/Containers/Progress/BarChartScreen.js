import React from 'react';
import { StyleSheet, Text, View, processColor } from 'react-native';

import { BarChart } from 'react-native-charts-wrapper';
import { Classes, Colors } from 'App/Theme';

class BarChartScreen extends React.Component {
  constructor() {
    super();

    this.state = {
      legend: {
        enabled: true,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5,
      },
      data: {
        dataSets: [
          {
            values: [
              { y: 77 },
              { y: 10 },
              { y: 10 },
              { y: 40 },
              { y: 60 },
              { y: 10 },
              { y: 10 },
            ],
            label: 'Bar dataSet',
            config: {
              color: processColor('teal'),
              barShadowColor: processColor('lightgrey'),
              highlightAlpha: 90,
              highlightColor: processColor('red'),
              drawGridLines: false,
            },
          },
        ],
        config: {
          barWidth: 0.7,
          drawGridLines: false,
        },
      },
      highlights: [{ x: 3 }, { x: 6 }],
      xAxis: {
        enabled: false,
        drawAxisLine: false,
        drawGridLines: false,
        drawLabels: false,
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
        granularityEnabled: true,
        granularity: 1,
      },
    };
  }

  handleSelect(event) {
    let entry = event.nativeEvent;
    if (entry == null) {
      this.setState({ ...this.state, selectedEntry: null });
    } else {
      this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) });
    }
  }

  render() {
    return (
      <View style={Classes.fill}>
        <View style={styles.textHeight}>
          <Text> selected entry</Text>
          <Text> {this.state.selectedEntry}</Text>
        </View>

        <View style={styles.container}>
          <BarChart
            style={styles.chart}
            data={this.state.data}
            xAxis={this.state.xAxis}
            animation={{ durationX: 2000 }}
            legend={this.state.legend}
            gridBackgroundColor={processColor(Colors.white)}
            visibleRange={{ x: { min: 5, max: 5 } }}
            drawBarShadow={false}
            drawValueAboveBar={true}
            drawHighlightArrow={true}
            onSelect={this.handleSelect.bind(this)}
            highlights={this.state.highlights}
            onChange={(event) => console.log(event.nativeEvent)}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white_03,
  },
  chart: {
    flex: 1,
  },
});

export default BarChartScreen;
