import { Platform } from 'react-native';
import { StyleSheet } from 'App/Helpers';
import { Classes, Colors, Fonts, Metrics } from 'App/Theme';
import { Screen } from 'App/Helpers';

export default StyleSheet.create({
  container: {
    ...Classes.fill,
    marginTop: Screen.scale(6),
  },
  marginForPad: {
    marginTop: Platform.select({
      ios: Platform.isPad ? Metrics.baseVerticalMargin * 2 : 0,
      android: 0,
    }),
  },
  navBarWrapper: { position: 'absolute', top: 0 },
  chartWrapper: {
    marginTop: Metrics.baseVerticalMargin,
    marginBottom: Metrics.baseVerticalMargin,
  },
  chart: {
    flex: 1,
  },
  textHeight: {
    height: 80,
  },
  achievementList: {
    paddingHorizontal: Metrics.baseMargin,
  },
  achievementWrapper: {
    paddingHorizontal: Platform.isPad ? Metrics.baseMargin / 2 : Metrics.baseMargin / 4,
  },
  workoutActivityList: {
    marginTop: '-8@vs',
  },
  workoutActivityWrapper: {
    ...Classes.colMain,
    paddingLeft: Metrics.baseMargin,
    paddingTop: Metrics.baseVerticalMargin / 2,
  },
  workoutActivityTextDate: {
    ...Fonts.style.medium500,
    marginBottom: Metrics.baseVerticalMargin / 2,
  },
  workoutActivityTextTitle: {
    ...Fonts.style.medium,
    color: Colors.black,
  },
  workoutActivityTextWrapper: {
    ...Classes.row,
    paddingVertical: Metrics.baseVerticalMargin / 2,
  },
  workoutActivityTextEndTime: {
    ...Fonts.style.medium,
    color: Colors.black,
  },
  workoutActivityTextDuration: {
    ...Fonts.style.medium,
    color: Colors.black,
    paddingHorizontal: Metrics.baseMargin,
  },
  workoutActivityTextCalories: {
    ...Fonts.style.medium,
    color: Colors.black,
  },
  arrowWrapper: {
    ...Classes.fillCenter,
    ...Classes.crossEnd,
    marginRight: Metrics.baseMargin / 4,
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_15,
  },
});
