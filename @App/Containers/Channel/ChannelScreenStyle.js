import { ScaledSheet } from 'App/Helpers';
import { Metrics, Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  flatBox: {
    marginBottom: Metrics.baseMargin,
    paddingBottom: Metrics.baseMargin,
  },
  borderWidth: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_15,
  },
  title: {
    ...Fonts.style.medium500,
    marginBottom: Metrics.baseMargin / 2,
    marginLeft: Metrics.baseMargin,
  },
  firstMargin: {
    marginTop: Metrics.baseMargin,
  },
  box: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: 0,
  },
});
