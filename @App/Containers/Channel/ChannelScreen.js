import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, SafeAreaView } from 'react-native';

import { AutoHideFlatList, ChannelCard } from 'App/Components';
import { ChannelActions, SearchActions } from 'App/Stores';
import { Classes } from 'App/Theme';
import { Actions } from 'react-native-router-flux';
import styles from './ChannelScreenStyle';

class ChannelScreen extends React.Component {
  static propTypes = {
    search: PropTypes.func.isRequired,
    getChannelList: PropTypes.func.isRequired,
    getChannelDetail: PropTypes.func.isRequired,
    list: PropTypes.array,
    isLoading: PropTypes.bool,
    sceneKey: PropTypes.string.isRequired,
  };

  componentDidMount() {
    const { getChannelList } = this.props;
    getChannelList();
  }

  renderItem = ({ item, index }) => {
    const { getChannelDetail, list, search } = this.props;
    return (
      <View style={[styles.flatBox, index < list.length - 1 && styles.borderWidth]}>
        <Text style={[styles.title, index === 0 && styles.firstMargin]}>
          {item.title}
        </Text>
        <ChannelCard
          size="full"
          data={item}
          onPress={() => {
            console.log('channel item=>', item);
            if (item.isFreeTrial) {
              const payload = {
                text: null,
                equipments: null,
                ids: item.ids,
              };
              console.log('search payload ->', payload);
              search(payload, {
                page: 1,
                pageSize: 10,
                filter: [],
                type: 'page',
              });
              Actions.SeeAllScreen({ loginRoute: 'freeTrial' });
            } else {
              const toDetailPage = true;
              getChannelDetail(item.id, toDetailPage);
            }
          }}
          onPressBookmark={() => {}}
        />
      </View>
    );
  };

  render() {
    const { list, isLoading, getChannelList, sceneKey } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <AutoHideFlatList
          keyExtractor={(item, index) => `${index}`}
          data={list}
          sceneKey={sceneKey}
          renderItem={this.renderItem}
          refreshing={isLoading}
          onRefresh={getChannelList}
          showRefresh
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    list: state.channel.channelList,
    isLoading: state.appState.isLoading,
    sceneKey: params.name,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        search: SearchActions.search,
        getChannelList: ChannelActions.getChannelList,
        getChannelDetail: ChannelActions.getChannelDetail,
      },
      dispatch,
    ),
)(ChannelScreen);
