import React from 'react';
import { isNumber, isEmpty, has } from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Platform, View, SafeAreaView } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import * as ImagePicker from 'react-native-image-picker';

import { AppStateActions, MirrorActions } from 'App/Stores';
import { RoundButton, AppendPhotoView } from 'App/Components';
import { Colors, Images, Classes } from 'App/Theme';
import { Screen, Photo, Date as d, Dialog, Permission } from 'App/Helpers';
import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';

import { translate as t } from 'App/Helpers/I18n';
import styles from './SaveActivityScreenStyle';

class WorkoutRecord extends React.Component {
  static propTypes = {
    onLoading: PropTypes.func.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    onMirrorSetPictureSuccess: PropTypes.func.isRequired,
    detail: PropTypes.object.isRequired,
    image: PropTypes.string,
    routeName: PropTypes.string,
    currentSceneName: PropTypes.string.isRequired,
    eventDetail: PropTypes.object,
    currentClassType: PropTypes.string,
    hasMirrorCamera: PropTypes.bool.isRequired,
  };

  static defaultProps = {};

  state = {
    isCapturing: false,
  };

  handleOpenCamera = async () => {
    if (Platform.OS === 'android') {
      const hasPermission = await Permission.checkAndRequestPermission(
        Permission.permissionType.CAMERA,
      );
      if (!hasPermission) {
        Dialog.requestCameraPermissionFromSystemAlert();
      }
    }
    const { onMirrorSetPictureSuccess } = this.props;
    const options = {
      includeBase64: true,
      mediaType: 'image',
      quality: 0.98,
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.error) {
        Dialog.requestCameraPermissionFromSystemAlert();
      }
      console.log('launchCamera options=>', options);
      console.log('launchCamera response=>', response);
      if (response.base64) {
        onMirrorSetPictureSuccess({ thumbnail: response.base64 });
      }
    });
  };

  onPressShareImage = async () => {
    const { onLoading } = this.props;
    try {
      onLoading(true);
      this.setState({ isCapturing: true });
      const uri = await this.viewShot.capture();
      this.setState({ isCapturing: false });
      onLoading(false);
      await Photo.shareImage(uri);
    } catch (error) {
      onLoading(false);
    }
  };

  onPressCalories = () => {
    const { detail, routeName } = this.props;
    if (detail.scheduledAt) {
      if (
        !isEmpty(detail.liveClassHistory) &&
        isNumber(detail.liveClassHistory.totalCalories)
      ) {
        return detail.liveClassHistory.totalCalories;
      }
      return detail.duration;
    } else {
      if (routeName === 'ProgramClassDetail') {
        if (
          !isEmpty(detail.trainingProgramClassHistory) &&
          isNumber(detail.trainingProgramClassHistory.totalCalories)
        ) {
          return detail.trainingProgramClassHistory.totalCalories;
        }
        return detail.duration;
      } else {
        if (
          !isEmpty(detail.trainingClassHistory) &&
          isNumber(detail.trainingClassHistory.totalCalories)
        ) {
          return detail.trainingClassHistory.totalCalories;
        }
        return detail.duration;
      }
    }
  };

  onPressSaveImage = async () => {
    const { onLoading } = this.props;
    onLoading(true);
    this.setState({ isCapturing: true });
    const uri = await this.viewShot.capture();
    await Photo.saveToGallery(uri);
    this.setState({ isCapturing: false });
    onLoading(false);
  };

  renderDuration = () => {
    const { detail } = this.props;
    if (detail.scheduledAt) {
      if (
        !isEmpty(detail.liveClassHistory) &&
        isNumber(detail.liveClassHistory.totalDuration)
      ) {
        return detail.liveClassHistory.totalDuration;
      }
      return detail.duration;
    } else {
      if (
        has(detail, 'trainingProgramClassHistory') &&
        !isEmpty(detail.trainingProgramClassHistory)
      ) {
        return detail.trainingProgramClassHistory.totalDuration;
      } else if (
        has(detail, 'trainingClassHistory') &&
        !isEmpty(detail.trainingClassHistory)
      ) {
        return detail.trainingClassHistory.totalDuration;
      } else {
        return detail.duration;
      }
    }
  };

  render() {
    const { isCapturing } = this.state;
    const {
      onMirrorSetPictureSuccess,
      currentClassType,
      currentSceneName,
      eventDetail,
      hasMirrorCamera,
      detail,
      image,
    } = this.props;
    let eventType = -1;
    if (eventDetail && eventDetail.eventType) {
      eventType = eventDetail.eventType;
    }
    const is1on1 = currentClassType === 'event' && eventType === 1;
    return (
      <SafeAreaView style={styles.tabBox}>
        <AppendPhotoView
          onMirrorSetPictureSuccess={onMirrorSetPictureSuccess}
          hasMirrorCamera={hasMirrorCamera}
          getRef={(r) => (this.viewShot = r)}
          isCapturing={isCapturing}
          routeName={currentSceneName}
          detail={detail}
          image={image}
        />
        <View style={styles.bottomBtn}>
          <RoundButton
            image={Images.wordRecord_share}
            text={t('__share')}
            onPress={this.onPressShareImage}
            style={styles.bottomBtnStyle}
          />
          <RoundButton
            image={Images.wordRecord_save}
            imageStyle={styles.imgStyle}
            onPress={this.onPressSaveImage}
            text={t('__save')}
          />
        </View>
        {is1on1 && (
          <View style={[Classes.fill, Classes.marginTop]}>
            <RoundButton
              onPress={Actions.ReserveNew1on1Screen}
              text={t('community_1on1_do_again')}
              textColor={Colors.primary}
              style={{ width: Screen.width - Screen.scale(32) }}
              width={Screen.width - Screen.scale(32)}
              outline
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, param) => ({
    eventDetail: state.community.eventDetail,
    currentClassType: state.mirror.currentClassType,
    image: state.mirror.thumbnail,
    detail: state.player.detail,
    routeName: state.player.routeName,
    currentSceneName: state.appRoute.routeName,
    hasMirrorCamera: filterMirrorHasFeatureOption(state, 'mirrorCamera'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        onMirrorSetPictureSuccess: MirrorActions.onMirrorSetPictureSuccess,
      },
      dispatch,
    ),
)(WorkoutRecord);
