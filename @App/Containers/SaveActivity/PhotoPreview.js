import React from 'react';
import PropTypes from 'prop-types';
import { last } from 'lodash';
import { connect } from 'react-redux';
import { View, SafeAreaView, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import { RoundButton, SecondaryNavbar, BaseButton } from 'App/Components';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';
import styles from './SaveActivityScreenStyle';
import { Colors, Images } from 'App/Theme';

class PhotoPreView extends React.Component {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
    resetThumbnail: PropTypes.func.isRequired,
    thumbnail: PropTypes.string,
    routeStack: PropTypes.array,
  };

  static defaultProps = {
    thumbnail: '',
  };

  onPressAccept = () => {
    const { onMirrorCommunicate, routeStack } = this.props;
    onMirrorCommunicate(MirrorEvents.PICTURE_ACCEPT);

    if (last(routeStack).includes('Program')) {
      Actions.ProgramSharePreview();
    } else {
      Actions.SharePreview();
    }
  };

  onPressCancel = () => {
    const { resetThumbnail } = this.props;
    resetThumbnail();
    this.props.onMirrorCommunicate(MirrorEvents.CANCEL_TAKE_PICTURE);
  };

  onPressPop = () => {
    this.props.onMirrorCommunicate(MirrorEvents.ACTIVITY_CAM);

    const { routeStack } = this.props;

    console.log('PhotoPreView routeStack=>', routeStack);

    if (last(routeStack).includes('Program')) {
      Actions.popTo('ProgramFeedbackScreen');
    } else {
      Actions.pop();
    }
  };

  render() {
    const { thumbnail, routeStack } = this.props;
    return (
      <SafeAreaView>
        <SecondaryNavbar
          cancelTo={
            routeStack[0].includes('ProgramDetail')
              ? 'ProgramFeedbackScreen'
              : 'SaveActivityScreen'
          }
          cancel
          onPressCancel={this.onPressCancel}
          navTitle={t('photo_preview_photo')}
          navRightComponent={
            <BaseButton
              transparent
              text={t('__ok')}
              textColor={Colors.primary}
              textStyle={styles.rightNav}
              onPress={this.onPressAccept}
            />
          }
        />
        <View style={styles.previewBox}>
          <Image
            source={{ uri: typeof thumbnail === 'string' ? thumbnail : '' }}
            style={styles.previewImg}
          />
        </View>
        <View style={styles.previewBottomBox}>
          <RoundButton
            image={Images.preview_camera}
            text={t('re_take')}
            onPress={this.onPressPop}
            style={styles.bottomBtnStyle}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    routeStack: state.appRoute.stack,
    thumbnail: state.mirror.thumbnail,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        resetThumbnail: MirrorActions.resetThumbnail,
      },
      dispatch,
    ),
)(PhotoPreView);
