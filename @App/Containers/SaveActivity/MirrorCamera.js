import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseButton } from 'App/Components';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';
import styles from './SaveActivityScreenStyle';
import { Colors } from 'App/Theme';

class MirrorCamera extends React.Component {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.onMirrorCommunicate(MirrorEvents.ACTIVITY_CAM);
  }

  render() {
    return (
      <SafeAreaView>
        <SecondaryNavbar
          navTitle={t('mirror_camera_mirror_camera')}
          navLeftComponent={
            <BaseButton
              transparent
              text={t('mirror_camera_cancel')}
              textColor={Colors.primary}
              textStyle={styles.rightNav}
              style={styles.cancelcBtn}
              onPress={() =>
                this.props.onMirrorCommunicate(MirrorEvents.CANCEL_TAKE_PICTURE)
              }
            />
          }
        />
        <View style={styles.cameraBox}>
          <Text style={styles.cameraText}>{t('mirror_camera_description')}</Text>
        </View>
        <View style={styles.pressBox}>
          <View style={styles.btnBox}>
            <TouchableOpacity
              style={styles.pressBtn}
              onPress={() => this.props.onMirrorCommunicate(MirrorEvents.TAKE_PICTURE)}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(MirrorCamera);
