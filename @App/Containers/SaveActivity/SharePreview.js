import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import {
  RoundButton,
  SecondaryNavbar,
  BaseButton,
  AppendPhotoView,
} from 'App/Components';
import { AppStateActions, MirrorActions } from 'App/Stores';
import { Photo } from 'App/Helpers';
import styles from './SaveActivityScreenStyle';
import { Colors, Images } from 'App/Theme';

class SharePreview extends React.PureComponent {
  static propTypes = {
    onLoading: PropTypes.func.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    onMirrorSetPictureSuccess: PropTypes.func.isRequired,
    detail: PropTypes.object.isRequired,
    image: PropTypes.string,
    routeName: PropTypes.string,
    currentSceneName: PropTypes.string.isRequired,
    eventDetail: PropTypes.object,
    currentClassType: PropTypes.string,
  };

  static defaultProps = {
    thumbnail: '',
  };

  state = {
    isCapturing: false,
  };

  onPressShareImage = async () => {
    const { onLoading } = this.props;
    try {
      onLoading(true);
      this.setState({ isCapturing: true });
      const uri = await this.viewShot.capture();
      this.setState({ isCapturing: false });
      onLoading(false);
      await Photo.shareImage(uri);
    } catch (error) {
      onLoading(false);
    }
  };

  onPressSaveImage = async () => {
    const { onLoading } = this.props;
    onLoading(true);
    this.setState({ isCapturing: true });
    const uri = await this.viewShot.capture();
    await Photo.saveToGallery(uri);
    this.setState({ isCapturing: false });
    onLoading(false);
  };

  render() {
    const { isCapturing } = this.state;
    const {
      detail,
      image,
      routeName,
      currentSceneName,
      onMirrorSetPictureSuccess,
    } = this.props;
    return (
      <SafeAreaView>
        <SecondaryNavbar
          backTo={
            currentSceneName === 'ProgramSharePreview'
              ? 'ProgramPhotoPreview'
              : 'PhotoPreview'
          }
          back
          navTitle={t('save_activity_save_activity')}
          navRightComponent={
            <BaseButton
              transparent
              text={t('__ok')}
              textColor={Colors.primary}
              textStyle={styles.rightNav}
              onPress={
                currentSceneName === 'ProgramSharePreview'
                  ? Actions.ProgramSaveActivityScreen
                  : Actions.SaveActivityScreen
              }
            />
          }
        />
        <AppendPhotoView
          onMirrorSetPictureSuccess={onMirrorSetPictureSuccess}
          getRef={(r) => (this.viewShot = r)}
          isCapturing={isCapturing}
          routeName={routeName}
          detail={detail}
          image={image}
          showAddButton={false}
        />
        <View style={styles.previewBottomBox}>
          <RoundButton
            image={Images.wordRecord_share}
            text={t('__share')}
            onPress={this.onPressShareImage}
            style={styles.bottomBtnStyle}
          />
          <RoundButton
            image={Images.wordRecord_save}
            imageStyle={styles.imgStyle}
            onPress={this.onPressSaveImage}
            text={t('__save')}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    eventDetail: state.community.eventDetail,
    currentClassType: state.mirror.currentClassType,
    image: state.mirror.thumbnail,
    detail: state.player.detail,
    routeName: state.player.routeName,
    currentSceneName: state.appRoute.routeName,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        onMirrorSetPictureSuccess: MirrorActions.onMirrorSetPictureSuccess,
      },
      dispatch,
    ),
)(SharePreview);
