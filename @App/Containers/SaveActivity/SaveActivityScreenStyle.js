import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Styles, Colors, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  navBar: {
    ...Styles.navBar,
    marginBottom: '26@s',
  },
  navBarTitle: {
    ...Fonts.style.regular500,
  },
  rightNav: {
    fontSize: Fonts.size.regular,
  },
  container: {
    ...Styles.screen.container,
  },
  tabBox: {
    marginTop: '17.5@vs',
    flex: 1,
    ...Styles.marginContainer,
  },
  tabMotionBox: {
    marginTop: Metrics.baseMargin,
    flex: 1,
  },
  horizontal: {
    marginLeft: Metrics.baseMargin,
  },
  hiddenForCapturing: {
    opacity: 0,
  },
  tabImage: {
    height: '380@s',
    flexDirection: 'column',
    alignItems: 'center',
  },
  firstTitle: {
    fontSize: Fonts.size.medium,
    letterSpacing: 0.88,
    fontWeight: '500',
    color: Colors.secondary,
    marginTop: '24@vs',
  },
  secondTitle: {
    fontSize: Fonts.size.input,
    fontWeight: '500',
    color: 'white',
    marginTop: '4@vs',
  },
  textStyle: {
    fontSize: Fonts.size.regular,
    fontWeight: '500',
    color: Colors.secondary,
    letterSpacing: 1,
  },
  btnStyle: {
    width: '180@s',
    height: '44@vs',
    borderRadius: '22@vs',
    borderWidth: 2,
    borderColor: Colors.secondary,
    marginTop: '103@vs',
    marginBottom: '128@vs',
  },
  bootomBox: {
    ...Styles.container,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
  },
  logoBox: {
    width: '60@s',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logoImage: {
    width: '36@s',
    height: '36@s',
    marginBottom: Metrics.baseMargin,
  },
  motionLogoImage: {
    width: '24@s',
    height: '24@s',
    marginBottom: Metrics.baseMargin,
  },
  bottomText: {
    ...Fonts.style.medium500,
    color: Colors.secondary,
    textAlign: 'center',
    marginBottom: Metrics.baseMargin,
    width: '70@s',
  },
  imgStyle: {
    width: '30@s',
    height: '30@vs',
  },
  bottomBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '16@s',
  },
  bottomBtnStyle: {
    marginRight: '20@vs',
  },
  tabBarBackground: {
    backgroundColor: 'white',
  },
  indicatorColor: {
    backgroundColor: Colors.tab.selected,
    height: '3.9@vs',
  },
  noMotionIndicatorColor: {
    backgroundColor: Colors.tab.selected,
    height: '3.9@vs',
    width: '50%',
    marginLeft: '25%',
  },
  tabStyle: {
    color: 'black',
  },
  activeColor: {
    color: 'red',
  },
  text: {
    fontSize: '16@vs',
  },
  carouselImage: {
    width: Screen.width - Screen.scale(48),
    height: Screen.width - Screen.scale(48),
    flexDirection: 'column',
    alignItems: 'center',
    zIndex: 900,
  },
  slide: {},
  slideTitle: {
    ...Fonts.style.medium500,
    color: Colors.secondary,
    marginTop: '24@vs',
    textAlign: 'center',
  },
  sliderBox: {
    marginTop: Screen.width - 138,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: Metrics.baseMargin,
  },
  slideAccuration: {
    ...Fonts.style.medium500,
    color: Colors.secondary,
    alignItems: 'flex-end',
  },
  cameraBox: {
    width: '100%',
    height: '412@vs',
    backgroundColor: Colors.black,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraText: {
    fontSize: '22@vs',
    fontWeight: '500',
    color: 'white',
    ...Platform.select({
      ios: {
        width: '260@s',
      },
      android: {
        width: '235@s',
      },
    }),
  },
  pressBox: {
    width: '100%',
    marginTop: '32@vs',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  pressImg: {
    width: '168@s',
    height: '168@vs',
  },
  previewBox: {
    width: '100%',
    height: '535@vs',
  },
  previewBottomBox: {
    marginTop: '40@vs',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  previewImg: {
    width: '100%',
    height: '535@vs',
  },
  btnBox: {
    width: '188@s',
    height: '188@s',
    backgroundColor: Colors.secondary,
    borderRadius: '94@s',
  },
  pressBtn: {
    width: '134@s',
    height: '134@s',
    borderRadius: '67@s',
    backgroundColor: Colors.white,
    margin: '27@s',
  },
  wrapperBox: {
    minHeight: '362.6@vs',
    flexDirection: 'row',
    justifyContent: 'center',
    opacity: 1,
  },
  cancelcBtn: {
    width: '88@s',
  },
});
