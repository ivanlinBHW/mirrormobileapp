import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, BackHandler, Platform, Text } from 'react-native';
import { NavBar, RoundButton } from '@ublocks-react-native/component';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { bindActionCreators } from 'redux';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';

import { translate as t } from 'App/Helpers/I18n';
import { Colors, Fonts } from 'App/Theme';
import styles from './SaveActivityScreenStyle';
import WorkoutRecord from './WorkoutRecord';
import MotionCapture from './MotionCapture';

class SaveActivityScreen extends React.Component {
  static propTypes = {
    detail: PropTypes.object.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
  };

  state = {
    index: 0,
    hasMotion: {
      index: 0,
      routes: [
        {
          key: 'WorkoutRecord',
          title: t('save_activity_workout_record'),
        },
        {
          key: 'MotionCapture',
          title: t('save_activity_motion_tracker'),
          disabled: true,
        },
      ],
    },
    noMotion: {
      index: 0,
      routes: [
        {
          key: 'WorkoutRecord',
          title: t('save_activity_workout_record'),
        },
      ],
    },
  };

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        this.onAndroidBackButtonPressed,
      );
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      this.backHandler.remove();
    }
  }

  onAndroidBackButtonPressed = () => {
    return true;
  };

  render() {
    const { hasMotion, noMotion } = this.state;
    const { detail, onMirrorCommunicate, isMotionCaptureEnabled } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <NavBar
          style={styles.navBar}
          backIconColor="black"
          title={t('save_activity_workout_summary')}
          leftComponent={null}
          titleStyle={styles.navBarTitle}
          rightComponent={
            <RoundButton
              transparent
              text={t('save_activity_finish')}
              textColor={Colors.button.primary.text.text}
              textStyle={styles.rightNav}
              onPress={() =>
                onMirrorCommunicate(MirrorEvents.SAVE_ACTIVITY_FINISH, {
                  classId: detail.id,
                })
              }
            />
          }
        />
        <TabView
          lazy
          navigationState={
            isMotionCaptureEnabled && !detail.scheduledAtUTC ? hasMotion : noMotion
          }
          renderScene={SceneMap({
            WorkoutRecord,
            MotionCapture,
          })}
          swipeEnabled={false}
          onIndexChange={(index) => {
            if (isMotionCaptureEnabled) {
              this.setState({
                hasMotion: {
                  ...this.state.hasMotion,
                  index,
                },
              });
              onMirrorCommunicate(MirrorEvents.SWITCH_SUMMARY, {
                pageType: index === 0 ? 'general' : 'motion',
              });
              setTimeout(() => {
                if (index === 1) {
                  onMirrorCommunicate(MirrorEvents.GET_MOTION_CAPTURE_PICTURE_PREVIEW, {
                    classId: detail.id,
                  });
                }
              }, 2000);
            } else {
              this.setState({
                noMotion: {
                  ...this.state.noMotion,
                  index,
                },
              });
            }
          }}
          renderTabBar={(props) => (
            <TabBar
              {...props}
              indicatorStyle={
                isMotionCaptureEnabled
                  ? styles.indicatorColor
                  : styles.noMotionIndicatorColor
              }
              style={styles.tabBarBackground}
              labelStyle={styles.tabStyle}
              disabled={props.disabled}
              renderLabel={({ route, focused, color }) => (
                <Text style={Fonts.style.regular}>{route.title}</Text>
              )}
            />
          )}
          tabBarPosition="top"
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    detail: state.class.detail,
    routeName: state.appRoute.routeName,
    isMotionCaptureEnabled: state.player.isMotionCaptureEnabled,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(SaveActivityScreen);
