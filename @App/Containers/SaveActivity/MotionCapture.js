import React from 'react';
import PropTypes from 'prop-types';
import { omit, get } from 'lodash';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, ImageBackground, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import Carousel from 'react-native-snap-carousel';
import ViewShot from 'react-native-view-shot';

import { AppStateActions, MirrorActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { Photo, Screen } from 'App/Helpers';
import { RoundButton } from 'App/Components';
import { Images, Styles } from 'App/Theme';
import styles from './SaveActivityScreenStyle';

class MotionCapture extends React.Component {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    classId: PropTypes.string.isRequired,
    thumbnails: PropTypes.array.isRequired,
    title: PropTypes.object,
  };

  static defaultProps = {
    thumbnails: [],
    title: '',
  };

  state = {
    entries: [
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
      { uri: 'https://via.placeholder.com/364X362' },
    ],
    activeSlide: 0,
  };

  componentDidMount() {
    const { onMirrorCommunicate, classId } = this.props;
    this.setState({
      activeSlide: 0,
    });
  }

  onPressShareImage = async () => {
    const { onLoading } = this.props;
    onLoading(true);
    this.setState({ isCapturing: true });
    const uri = await this.viewShot.capture();
    onLoading(false);
    await Photo.shareImage(uri);
    this.setState({ isCapturing: false });
  };

  onPressSaveImage = async () => {
    const { onLoading } = this.props;
    onLoading(true);
    this.setState({ isCapturing: true });
    const uri = await this.viewShot.capture();
    await Photo.saveToGallery(uri);
    this.setState({ isCapturing: false });
    onLoading(false);
  };

  renderCaptureView = () => {
    const { thumbnails = [] } = this.props;
    const { activeSlide } = this.state;
    const styleHidden = { opacity: 0 };
    const isSavePhotoAvailable =
      thumbnails &&
      activeSlide < thumbnails.length &&
      thumbnails[activeSlide] &&
      thumbnails[activeSlide].thumbnail;
    return (
      isSavePhotoAvailable && (
        <View style={styleHidden}>
          <ViewShot
            ref={(r) => (this.viewShot = r)}
            options={{ format: 'png', quality: 1 }}
          >
            {this.renderAppendPhotoView({ uri: thumbnails[activeSlide].thumbnail })}
          </ViewShot>
        </View>
      )
    );
  };

  renderAppendPhotoView = ({ uri }) => {
    const { thumbnails = [], title } = this.props;
    const { activeSlide } = this.state;
    return (
      <ImageBackground source={{ uri }} style={styles.carouselImage}>
        <Text style={styles.slideTitle}>{title}</Text>
        <View style={styles.sliderBox}>
          <Text style={styles.slideAccuration}>{`${t('save_activity_workout_score')}\n${
            thumbnails[activeSlide].accuracy
          }`}</Text>
          <Image source={Images.logo_small} style={styles.motionLogoImage} />
        </View>
      </ImageBackground>
    );
  };

  renderItem = ({ item, index }) => {
    const { title } = this.props;
    return (
      <View style={styles.slide}>
        <ImageBackground
          source={{ uri: item.thumbnail }}
          resizeMode="cover"
          style={styles.carouselImage}
        >
          <Text style={styles.slideTitle}>{title}</Text>
          <View style={styles.sliderBox}>
            <Text style={styles.slideAccuration}>{`${t('save_activity_workout_score')}\n${
              item.accuracy
            }`}</Text>
            {/* <Text style={styles.slideAccuration}>{item.accuration}</Text> */}
            <Image source={Images.logo_small} style={styles.logoImage} />
          </View>
        </ImageBackground>
      </View>
    );
  };

  render() {
    const { thumbnails = [] } = this.props;
    const { activeSlide } = this.state;
    const isSavePhotoAvailable =
      thumbnails &&
      activeSlide < thumbnails.length &&
      thumbnails[activeSlide] &&
      thumbnails[activeSlide].thumbnail;

    return (
      <SafeAreaView style={styles.tabMotionBox}>
        <View style={styles.wrapperBox}>
          <Carousel
            data={thumbnails}
            renderItem={this.renderItem}
            onSnapToItem={(index) => this.setState({ activeSlide: index || 0 })}
            sliderWidth={Screen.width}
            itemWidth={Screen.width - Screen.scale(32)}
            sliderHeight={Screen.width - Screen.scale(32)}
            itemHeight={Screen.width - Screen.scale(32)}
            inactiveSlideScale={1}
            firstItem={1}
            layout="default"
            useScrollView
          />
        </View>
        <View style={[styles.bottomBtn, Styles.marginHorizontal]}>
          <RoundButton
            image={Images.wordRecord_share}
            text={t('__share')}
            onPress={this.onPressShareImage}
            style={styles.bottomBtnStyle}
            disabled={!isSavePhotoAvailable}
          />
          <RoundButton
            image={Images.wordRecord_save}
            imageStyle={styles.imgStyle}
            onPress={this.onPressSaveImage}
            text={t('__save')}
            disabled={!isSavePhotoAvailable}
          />
        </View>
        {this.renderCaptureView()}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, param) => ({
    thumbnails: state.mirror.captureThumbnails,
    classId: state.mirror.currentClassId,
    title: state.player.detail.title,
    routeName: state.appRoute.routeName,
    sceneKey: param.name,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(MotionCapture);
