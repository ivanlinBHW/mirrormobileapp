import React from 'react';
import PropTypes from 'prop-types';
import { get, has, isNil, isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { Text } from 'react-native';
import { bindActionCreators } from 'redux';

import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';
import { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import { translate as t } from 'App/Helpers/I18n';
import { SquareButton } from 'App/Components';
import { Dialog, Date as d, Subscription } from 'App/Helpers';
import { Colors } from 'App/Theme';
import {
  MirrorActions,
  PlayerActions,
  ProgramActions,
  InstructorActions,
} from 'App/Stores';

import ClassDetailView from '../ClassDetail/ClassDetailView';

class ProgramClassDetailScreen extends React.Component {
  static propTypes = {
    playerDetail: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired,
    setInstructorId: PropTypes.func.isRequired,
    routeName: PropTypes.string.isRequired,
    activeTrainingProgramId: PropTypes.string,
    isFinished: PropTypes.bool,
    startProgramClass: PropTypes.func.isRequired,
    programHistoryId: PropTypes.string,
    resumeProgramClass: PropTypes.func.isRequired,
    trainingProgramClassHistoryId: PropTypes.string,
    onMirrorCommunicate: PropTypes.func.isRequired,
    isConnected: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    mirrorCompare: PropTypes.func.isRequired,
    isSubscriptionExpired: PropTypes.bool.isRequired,
    mirrorOptionMotionCapture: PropTypes.bool.isRequired,

    isOnlySee: PropTypes.bool,
    prevRoute: PropTypes.string,
    mainSubscription: PropTypes.object,
    updatePlayerStore: PropTypes.func.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
    nextShowDistance: PropTypes.bool.isRequired,
    classCanEnableMotionCapture: PropTypes.bool.isRequired,
    isResumable: PropTypes.bool.isRequired,
    isProgramResumable: PropTypes.bool.isRequired,
    shouldButtonDisabled: PropTypes.bool.isRequired,

    hasDistanceAdjustment: PropTypes.bool.isRequired,
    hasPartnerWorkout: PropTypes.bool.isRequired,
    hasCoachVI: PropTypes.bool.isRequired,
    setProgramBookmark: PropTypes.func.isRequired,
    removeProgramBookmark: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isOnlySee: false,
    prevRoute: '',
    mainSubscription: null,
  };

  componentDidMount() {
    console.log('@ProgramClassDetailScreen entered');
  }

  startClass = () => {
    const {
      token,
      playerDetail,
      startProgramClass,
      trainingProgramClassHistoryId,
      isMotionCaptureEnabled,
      nextShowDistance,
    } = this.props;

    if (isMotionCaptureEnabled) {
      if (nextShowDistance) {
        Dialog.showConfirmCameraUsageAlert(() =>
          Dialog.requestDistanceAdjustmentWhenEnableMotionCapture(
            (startDistanceMeasuring = false) =>
              startProgramClass(trainingProgramClassHistoryId, token, playerDetail.id, {
                enableMotionCapture: isMotionCaptureEnabled,
                'start-measure-distance': startDistanceMeasuring,
              }),
          ),
        );
      } else {
        Dialog.showConfirmCameraUsageAlert((startDistanceMeasuring = false) =>
          startProgramClass(trainingProgramClassHistoryId, token, playerDetail.id, {
            enableMotionCapture: isMotionCaptureEnabled,
            'start-measure-distance': startDistanceMeasuring,
          }),
        );
      }
    } else {
      startProgramClass(trainingProgramClassHistoryId, token, playerDetail.id, {
        enableMotionCapture: isMotionCaptureEnabled,
      });
    }
  };

  resumeClass = () => {
    const {
      token,
      playerDetail,
      routeName,
      resumeProgramClass,
      trainingProgramClassHistoryId,
      isMotionCaptureEnabled,
      nextShowDistance,
    } = this.props;

    if (isMotionCaptureEnabled) {
      if (nextShowDistance) {
        Dialog.showConfirmCameraUsageAlert(() =>
          Dialog.requestDistanceAdjustmentWhenEnableMotionCapture(
            (startDistanceMeasuring = false) =>
              resumeProgramClass(
                trainingProgramClassHistoryId,
                token,
                routeName,
                playerDetail.id,
                {
                  enableMotionCapture: isMotionCaptureEnabled,
                  'start-measure-distance': startDistanceMeasuring,
                },
              ),
          ),
        );
      } else {
        Dialog.showConfirmCameraUsageAlert(() =>
          resumeProgramClass(
            trainingProgramClassHistoryId,
            token,
            routeName,
            playerDetail.id,
            {
              enableMotionCapture: isMotionCaptureEnabled,
            },
          ),
        );
      }
    } else {
      resumeProgramClass(
        trainingProgramClassHistoryId,
        token,
        routeName,
        playerDetail.id,
        {
          enableMotionCapture: isMotionCaptureEnabled,
          'start-measure-distance': false,
        },
      );
    }
  };

  onPressInstructor = () => {
    const {
      playerDetail: { instructor },
      setInstructorId,
      token,
    } = this.props;
    setInstructorId(instructor.id, token);
  };

  onPressBack = () => {
    const { onMirrorCommunicate, playerDetail } = this.props;
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS_PREVIEW, {
      classId: playerDetail.id,
      classType: 'on-demand',
    });
  };

  renderStartButton = () => {
    const {
      isResumable,
      isProgramResumable,
      playerDetail,
      routeName,
      activeTrainingProgramId,
      mirrorCompare,
      mainSubscription,
      shouldButtonDisabled,
      isSubscriptionExpired,
      trainingProgramClassHistoryId,
    } = this.props;
    if (isEmpty(mainSubscription)) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestAccountSubscriptionCodeAlert(() =>
            mirrorCompare({ onPressNo: this.startClass }),
          )}
          disabled={shouldButtonDisabled}
          text={t('class_detail_request_account_subscription_alert_title')}
        />
      );
    } else if (isSubscriptionExpired) {
      return (
        <SquareButton
          color={Colors.primary}
          onPress={Dialog.requestAccountSubscriptionExpiredAlert(this.startClass)}
          disabled={shouldButtonDisabled}
          text={t('class_detail_request_account_subscription_expired_title')}
        />
      );
    } else if (!playerDetail.isSubscribed) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestClassSubscriptionAlert(() =>
            mirrorCompare({ onPressNo: this.startClass }),
          )}
          disabled={shouldButtonDisabled}
          text={t('class_detail_request_class_subscription_alert_title')}
        />
      );
    }

    if (routeName === 'ProgramClassDetail') {
      if (isProgramResumable) {
        return (
          <SquareButton
            color={Colors.button.third.content.background}
            disabled={shouldButtonDisabled}
            onPress={() => mirrorCompare({ onPressNo: this.startClass })}
            text={t('class_detail_start_class')}
          />
        );
      }
      if (
        (activeTrainingProgramId || trainingProgramClassHistoryId) &&
        playerDetail.trainingProgramClassHistory &&
        playerDetail.trainingProgramClassHistory.status !== 1 &&
        playerDetail.trainingProgramClassHistory.pausedStepId === null
      ) {
        return (
          <SquareButton
            color={Colors.button.primary.content.background}
            disabled={shouldButtonDisabled}
            onPress={() => mirrorCompare({ onPressNo: this.startClass })}
            text={t('class_detail_start_class')}
          />
        );
      }
    } else {
      if (isResumable) {
        return (
          <SquareButton
            color={Colors.button.third.content.background}
            disabled={shouldButtonDisabled}
            onPress={() => mirrorCompare({ onPressNo: this.startClass })}
            text={t('class_detail_start_class')}
          />
        );
      } else {
        return (
          <SquareButton
            color={Colors.button.primary.content.background}
            disabled={shouldButtonDisabled}
            onPress={() => mirrorCompare({ onPressNo: this.startClass })}
            text={t('class_detail_start_class')}
          />
        );
      }
    }
  };

  renderResumeButton = () => {
    const {
      isSubscriptionExpired,
      playerDetail,
      routeName,
      mirrorCompare,
      mainSubscription,
      isProgramResumable,
      isResumable,
      shouldButtonDisabled,
    } = this.props;

    if (
      isSubscriptionExpired ||
      isEmpty(mainSubscription) ||
      !playerDetail.isSubscribed
    ) {
      return null;
    }

    if (routeName === 'ProgramClassDetail') {
      if (isProgramResumable) {
        return (
          <SquareButton
            disabled={shouldButtonDisabled}
            onPress={() => mirrorCompare({ onPressNo: this.resumeClass })}
            color={Colors.button.primary.content.background}
            text={t('class_detail_resume_class')}
          />
        );
      }
    } else {
      if (isResumable) {
        return (
          <SquareButton
            disabled={shouldButtonDisabled}
            onPress={() => mirrorCompare({ onPressNo: this.resumeClass })}
            color={Colors.button.primary.content.background}
            text={t('class_detail_resume_class')}
          />
        );
      }
    }
  };

  checkSubscriptionExpire(mainSubscription) {
    let isSubscriptionExpire = true;

    let { playerDetail } = this.props;
    let { code } = playerDetail.channel;
    console.log('=== training class data ===', playerDetail.channel);

    if (!isEmpty(mainSubscription) && mainSubscription.items) {
      mainSubscription.items.forEach((item) => {
        console.log('=== training class data ===', item);
        if (
          code == item.product.subscriptionTypeId &&
          d.moment() <= d.moment(item.expireAt)
        ) {
          isSubscriptionExpire = false;
        }
      });
    }

    return isSubscriptionExpire;
  }

  onPressBookmark = (id, item) => {
    const { setProgramBookmark, removeProgramBookmark } = this.props;
    if (item.isBookmarked) {
      removeProgramBookmark(id);
    } else {
      setProgramBookmark(id);
    }
  };

  render() {
    const {
      playerDetail,
      isLoading,
      isConnected,
      isOnlySee = false,
      prevRoute,
      mainSubscription,
      updatePlayerStore,
      isMotionCaptureEnabled,
      setInstructorId,
      mirrorOptionMotionCapture,
      activeTrainingProgramId,
      hasCoachVI,
      hasDistanceAdjustment,
      isSubscriptionExpired,
      routeName,
    } = this.props;
    return (
      <>
        {__DEV__ && (
          <>
            <Text>
              trainingProgramClassHistory.status:{' '}
              {get(playerDetail, 'trainingProgramClassHistory.status')}
            </Text>
            <Text>
              isSubscriptionExpired:
              {JSON.stringify(isSubscriptionExpired)}
            </Text>
            <Text>player routeName: {routeName}</Text>
          </>
        )}
        <ClassDetailView
          hasDistanceAdjustment={hasDistanceAdjustment}
          hasCoachVI={hasCoachVI}
          setInstructorId={setInstructorId}
          playerDetail={playerDetail}
          isOnlySee={isOnlySee}
          isLoading={isLoading}
          isConnected={isConnected}
          prevRouteName={prevRoute}
          mainSubscription={mainSubscription}
          backToRouteName={activeTrainingProgramId ? 'Program' : 'ProgramDetail'}
          renderResumeButton={this.renderResumeButton}
          renderStartButton={this.renderStartButton}
          onPressBack={this.onPressBack}
          isMotionCaptureEnabled={isMotionCaptureEnabled}
          onMotionCaptureChanged={updatePlayerStore}
          mirrorOptionMotionCapture={mirrorOptionMotionCapture}
          onPressBookmark={this.onPressBookmark}
        />
      </>
    );
  }
}

export default connect(
  (state) => ({
    playerDetail: state.player.detail,
    isConnected: state.mirror.isConnected,
    isLoading: state.appState.isLoading,
    token: state.user.token,
    routeName: state.player.routeName,
    prevRoute: state.appRoute.prevRoute,
    activeTrainingProgramId: state.user.activeTrainingProgramId,
    programHistoryId: state.user.activeTrainingProgramHistoryId,
    trainingProgramClassHistoryId: state.user.trainingProgramClassHistoryId,
    mainSubscription: state.user.mainSubscription,
    mirrorOptionMotionCapture: state.mirror.options[MirrorOptions.MOTION_TRACKER],
    isMotionCaptureEnabled:
      'isMotionCaptureEnabled' in state.player
        ? state.player.isMotionCaptureEnabled
        : false,
    nextShowDistance: state.user.isNextShowDistance,
    classCanEnableMotionCapture: state.class.detail.hasMotionCapture,

    isSubscriptionExpired: Subscription.isExpired(
      state.user.mainSubscription,
      state.player.detail,
    ),
    isProgramResumable: !isNil(
      get(state, 'player.detail.trainingProgramClassHistory.pausedStepId'),
    ),
    isResumable: !isNil(get(state, 'player.detail.trainingClassHistory.pausedStepId')),
    shouldButtonDisabled:
      state.mirror.isWaitingForDeviceStartClass || state.appState.isLoading,

    hasDistanceAdjustment: filterMirrorHasFeatureOption(state, 'distanceAdjustment'),
    hasPartnerWorkout: filterMirrorHasFeatureOption(state, 'partnerWorkout'),
    hasCoachVI: filterMirrorHasFeatureOption(state, 'coachVI'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        startProgramClass: ProgramActions.startProgramClass,
        resumeProgramClass: ProgramActions.resumeProgramClass,
        setInstructorId: InstructorActions.setInstructorId,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        mirrorCompare: MirrorActions.mirrorCompare,
        updatePlayerStore: PlayerActions.updatePlayerStore,
        setProgramBookmark: ProgramActions.setProgramBookmark,
        removeProgramBookmark: ProgramActions.removeProgramBookmark,
      },
      dispatch,
    ),
)(ProgramClassDetailScreen);
