import {
  ActivityIndicator,
  Alert,
  FlatList,
  Image,
  Linking,
  SafeAreaView,
  Text,
  View,
} from 'react-native';
import {
  BaseAvatar,
  BaseButton,
  BaseIcon,
  PrimaryNavbar,
  RoundButton,
  SquareButton,
} from 'App/Components';
import { Classes, Colors, Fonts, Images } from 'App/Theme';
import { Permission, Screen } from 'App/Helpers';

import { Actions } from 'react-native-router-flux';
import AutoHideViewHoc from 'App/Components/AutoHideViewHoc';
import { Config } from 'App/Config';
import Geolocation from '@react-native-community/geolocation';
import PropTypes from 'prop-types';
import React from 'react';
import SettingActions from 'App/Stores/Setting/Actions';
import UserActions from 'App/Stores/User/Actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './SettingScreenStyle';
import { translate as t } from 'App/Helpers/I18n';

const data = [
  {
    title: 'setting_mii_setting',
    key: 'MiiSettingScreen',
  },
  {
    title: 'setting_heart_rate_monitor',
    key: 'HeartRateScreen',
  },
  {
    title: 'setting_music',
    key: 'MusicScreen',
  },
  {
    title: 'setting_audio_output',
    key: 'AudioScreen',
  },
  {
    title: 'change_password',
    key: 'ResetPasswordScreen',
  },
  {
    title: 'setting_notification',
    key: 'NotificationSettingScreen',
  },
  {
    title: 'setting_privacy',
    key: 'PrivacySettingScreen',
  },
  {
    title: 'setting_account',
    key: 'AccountSettingScreen',
  },
  {
    title: 'setting_privacy_policy',
    key: 'PrivacyScreen',
  },
  {
    title: 'setting_terms',
    key: 'TermScreen',
  },
  {
    title: 'setting_faq',
    key: '',
  },
  {
    title: 'setting_contact_us',
    key: '',
  },
];

const SettingNavBar = AutoHideViewHoc(() => <PrimaryNavbar navText={t('setting')} />);

class SettingScreen extends React.Component {
  static propTypes = {
    getSetting: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    setting: PropTypes.object.isRequired,
    userLogout: PropTypes.func.isRequired,
    isConnected: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isPaired: PropTypes.bool.isRequired,
    updateUserStore: PropTypes.func.isRequired,
  };

  state = {
    engineeringModeCount: 1,
  };

  _mounted = false;

  async componentDidMount() {
    __DEV__ && console.log('@SettingScreen');
    const { getSetting } = this.props;
    getSetting();

    const hasPermission = await Permission.checkAndRequestPermission(
      Permission.permissionType.GEOLOCATION_LOW,
    );
    if (hasPermission) {
      Geolocation.getCurrentPosition(({ coords }) => {
        if (this._mounted) {
          const { longitude, latitude } = coords;
          const { updateUserStore } = this.props;
          updateUserStore({
            coordinate: {
              longitude,
              latitude,
            },
          });
        }
      });
      this._mounted = true;
    }
  }

  componentWillUnmount() {
    Geolocation.stopObserving();
    this._mounted = false;
  }

  handleLogout = () => {
    this.props.userLogout();

    Actions.UserLoginScreen({
      type: 'replace',
      panHandlers: null,
    });
  };

  onPressLogout = () => {
    Alert.alert(t('setting_logout_modal_title'), t('setting_logout_modal_content'), [
      {
        text: t('__cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('__ok'),
        onPress: this.handleLogout,
      },
    ]);
  };

  onTriggerEngineeringMode = () => {
    const { engineeringModeCount } = this.state;

    if (engineeringModeCount < 10) {
      this.setState((state) => ({
        ...state,
        engineeringModeCount: state.engineeringModeCount + 1,
      }));
    } else if (engineeringModeCount >= 10) {
      this.setState(
        {
          engineeringModeCount: 0,
        },
        () => Actions.DeveloperModeScreen(),
      );
    }
  };

  renderHeader = () => {
    const { setting, isLoading } = this.props;
    return (
      <View style={{ ...styles.headerWrapper, backgroundColor: '#f8f8f8' }}>
        <View style={styles.leftBox}>
          <BaseAvatar
            size={Screen.verticalScale(56)}
            uri={`${setting.signedAvatarUrl}`}
            active
          />
        </View>
        <View style={styles.rightBox}>
          <View style={[Classes.rowCross, Classes.marginBottomHalf]}>
            <Text style={Fonts.style.regular500}>{setting.fullName}</Text>
            <ActivityIndicator
              animating={isLoading}
              size="small"
              color="black"
              style={Classes.marginLeft}
            />
          </View>
          <RoundButton
            style={styles.btnStyle}
            textStyle={styles.textStyle}
            uppercase={false}
            text={t('setting_view_profile')}
            onPress={Actions.MyProfileScreen}
          />
        </View>
      </View>
    );
  };

  renderItem = ({ item, index }) => {
    const handleOnPress = () => {
      if (['setting_faq'].includes(item.title)) {
        Linking.openURL(`${Config.SHOP_LINK}?action=contact`);
      } else if (['setting_contact_us'].includes(item.title)) {
        Linking.openURL(`${Config.SHOP_LINK}?action=contact_form`);
      } else {
        Actions[item.key]();
      }
    };
    const { isConnected } = this.props;
    const disabled = ['setting_music', 'setting_audio_output'].includes(item.title);
    return (
      <View style={{ ...styles.wrapper, backgroundColor: '#f8f8f8' }}>
        <BaseButton
          transparent
          style={styles.itemStyle}
          onPress={handleOnPress}
          disabled={isConnected ? false : disabled}
        >
          <Text style={styles.itemText}>{t(`${item.title}`)}</Text>
          <Image source={Images.next} style={styles.itemImg} />
        </BaseButton>
        {index === 5 && <View style={styles.emptyBox} />}
      </View>
    );
  };

  renderFooter = () => {
    const { isPaired, isConnected } = this.props;
    return (
      <View style={{ ...Classes.fill, backgroundColor: '#f8f8f8' }}>
        <View
          style={[
            Classes.fill,
            Classes.rowMain,
            Classes.mainEnd,
            Classes.mainSpaceBetween,
          ]}
        >
          <View>
            {__DEV__ && (
              <BaseButton
                style={{ width: Screen.scale(36) }}
                transparent
                onPress={Actions.DEBUG}
                throttleTime={0}
              >
                <BaseIcon
                  size={12}
                  name="bug"
                  color={isConnected ? 'green' : 'darkred'}
                />
                {isPaired && (
                  <BaseIcon
                    name="circle"
                    size={24}
                    color={isConnected ? 'green' : 'darkred'}
                    style={Classes.absolute}
                  />
                )}
              </BaseButton>
            )}
          </View>
          <View style={styles.engineeringModeWrapper}>
            <BaseButton
              style={styles.btnEngineeringMode}
              onPress={this.onTriggerEngineeringMode}
              throttleTime={0}
              transparent
            >
              <Text style={styles.versionText}>
                v{Config.APP_VERSION}
                {__DEV__ && Config.BUILD_VERSION && `(${Config.BUILD_VERSION})`}
              </Text>
            </BaseButton>
          </View>
        </View>
        <SquareButton
          onPress={this.onPressLogout}
          color="transparent"
          style={styles.logoutBtn}
          textColor={Colors.button.primary.text.text}
          text={t('setting_logout')}
        />
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <SettingNavBar />
        <View style={styles.mainWrapper}>
          {this.renderHeader()}
          <FlatList
            data={data}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => `setting_${index}`}
            ListFooterComponent={this.renderFooter}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    setting: state.setting.setting,
    user: state.user,
    isConnected: __DEV__ ? true : state.mirror.isConnected,
    isLoading: state.appState.isLoading,
    isPaired: state.mirror.isPaired,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateUserStore: UserActions.updateUserStore,
        getSetting: SettingActions.getSetting,
        userLogout: UserActions.onUserLogout,
      },
      dispatch,
    ),
)(SettingScreen);
