import { ScaledSheet } from 'App/Helpers';
import { Metrics, Colors, Fonts, Styles } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    ...Styles.screen.container,
    backgroundColor: Colors.white,
    flex: 1,
  },
  mainWrapper: {
    flex: 1,
    marginTop: '75@vs',
    backgroundColor: Colors.white,
  },

  navContainer: {
    flex: 1,
  },
  headerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: '28@vs',
    paddingBottom: '28@vs',
  },
  rightBox: {
    marginLeft: Metrics.baseMargin,
  },
  leftBox: {
    marginLeft: Metrics.baseMargin,
  },
  btnStyle: {
    height: '24@vs',
    width: '110@s',
    backgroundColor: 'transparent',
    borderWidth: 1,
    borderColor: Colors.button.primary.outline.border,
  },
  textStyle: {
    color: Colors.button.primary.outline.text,
    fontSize: Fonts.size.small,
  },
  wrapper: {
    borderTopWidth: 1,
    borderColor: Colors.black_15,
  },
  itemStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: '48@vs',
    alignItems: 'center',
  },
  itemText: {
    fontSize: Fonts.size.medium,
    lineHeight: '48@vs',
    marginLeft: Metrics.baseMargin,
  },
  itemImg: {
    width: '6@s',
    height: '11@s',
    marginRight: '20@s',
  },
  emptyBox: {
    height: '24@vs',
    backgroundColor: Colors.white,
  },
  versionText: {
    fontSize: Fonts.size.small,
  },
  btnEngineeringMode: {
    width: 'auto',
    marginHorizontal: Metrics.baseMargin,
    alignSelf: 'center',
  },
  engineeringModeWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 0,
  },
  logoutBtn: {
    marginBottom: Metrics.baseMargin,
  },
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  disabled: {
    opacity: 0.2,
  },
  txtGray: {
    color: Colors.gray_02,
  },
  bgWhite02: {
    backgroundColor: Colors.white_02,
  },
  bgWhite: {
    backgroundColor: Colors.white,
  },
  subscriptionCodeContainer: {
    position: 'absolute',
    top: '33%',
    width: '100%',
    alignSelf: 'center',
  },
  accountInfoWrapper: {
    backgroundColor: Colors.white_02,
    paddingBottom: Metrics.baseMargin,
  },
  marginTop80: {
    marginTop: '80@s',
  },
  listWrapper: {
    flex: 1,
  },
});
