import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, SafeAreaView, Picker, Alert } from 'react-native';

import { Actions } from 'react-native-router-flux';

import { Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseButton } from 'App/Components';
import { MirrorActions, AppStateActions } from 'App/Stores';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { Config } from 'App/Config';
import styles from './DeveloperModeScreenStyle';

class DeveloperModeScreen extends React.Component {
  static propTypes = {
    mirrorCommunicate: PropTypes.func.isRequired,
    deviceAppVersion: PropTypes.string,
    deviceHardwareVersion: PropTypes.string,
    env: PropTypes.string,
    doEnvChange: PropTypes.func.isRequired,
    isConnected: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    deviceAppVersion: '',
    deviceHardwareVersion: '',
  };

  state = {
    env: '',
    envList: [],
  };

  componentDidMount() {
    __DEV__ && console.log('@DeveloperModeScreen enter');
    this.setState({ envList: Config.API_BASE_URL, env: this.props.env });
    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.GET_DEVICE_INFO);
  }

  componentWillUnmount() {
    __DEV__ && console.log('@DeveloperModeScreen leave');
    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.END_ENGINEERING_MODE);
  }

  render() {
    const {
      mirrorCommunicate,
      deviceAppVersion,
      deviceHardwareVersion,
      doEnvChange,
      isConnected,
    } = this.props;
    const { env } = this.state;
    const envList = Object.keys(this.state.envList).map((key) => {
      return <Picker.Item label={key} value={key} />;
    });
    return (
      <SafeAreaView style={styles.container}>
        <SecondaryNavbar back navTitle={t('developer_mode_title')} />

        <View style={styles.btnWrapper}>
          <BaseButton
            text={t('developer_mode_save_log_to_usb')}
            style={styles.button}
            textStyle={Fonts.style.medium}
            onPress={() =>
              mirrorCommunicate(MirrorEvents.ENGINEERING_MODE, {
                action: 'save-log-to-usb-device',
              })
            }
            disabled={!isConnected}
          />
          <BaseButton
            text={t('developer_mode_display_on_device')}
            style={styles.button}
            textStyle={Fonts.style.medium}
            onPress={() =>
              mirrorCommunicate(MirrorEvents.ENGINEERING_MODE, {
                action: 'display-log-on-screen',
              })
            }
            disabled={!isConnected}
          />
          <BaseButton
            text={'Debug Menu'}
            style={styles.button}
            textStyle={Fonts.style.medium}
            onPress={Actions.DebugMenuScreen}
          />
          {/* <View>
            <Picker
              selectedValue={env}
              onValueChange={(itemValue, itemIndex) => {
                this.setState({ env: itemValue });
                Alert.alert(
                  t('compare_alert_title'),
                  `確定切換環境為 ${itemValue} app 將登出`,
                  [
                    {
                      text: t('compare_alert_no'),
                      onPress: () => {
                        this.setState({ env });
                      },
                    },
                    {
                      text: t('compare_alert_yes'),
                      onPress: () => {
                        doEnvChange(itemValue);
                      },
                    },
                  ],
                );
              }}
            >
              {envList}
            </Picker>
          </View> */}
        </View>

        <Text style={styles.txtVersion}>
          {t('developer_mode_device_version')} {deviceAppVersion}
          {`\n`}
          {t('developer_mode_device_hardware_version')} {deviceHardwareVersion}
        </Text>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    deviceAppVersion: state.mirror.deviceAppVersion,
    deviceHardwareVersion: state.mirror.deviceHardwareVersion,
    env: state.appState.env,
    isConnected: state.mirror.isConnected,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
        doEnvChange: AppStateActions.doEnvChange,
      },
      dispatch,
    ),
)(DeveloperModeScreen);
