import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, SafeAreaView, Text } from 'react-native';

import { UserActions } from 'App/Stores';
import { Permission } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseSwitch } from 'App/Components';

import styles from './NotificationSettingScreenStyle';

class NotificationSettingScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    notificationEnable: PropTypes.bool.isRequired,
    hasNotificationsPermission: PropTypes.bool.isRequired,
    fetchPutNotificationEnable: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isNotificationEnabled: false,
    };
  }

  async componentDidMount() {
    __DEV__ && console.log('@NotificationSettingScreen');

    const { hasNotificationsPermission } = this.props;
    if (!hasNotificationsPermission) {
      await Permission.requestFcmPermission();
    }
  }

  onValueChange = (value) => {
    const { fetchPutNotificationEnable } = this.props;

    fetchPutNotificationEnable(value);
  };

  render() {
    const {
      user: { notificationEnable },
      hasNotificationsPermission,
    } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <SecondaryNavbar navHeight={44} back navTitle={t('setting_notification_title')} />

        <View style={styles.listBox}>
          <Text style={styles.itemText}>
            {t('setting_notification_option_all_notification')}
          </Text>
          <View style={styles.rightItem}>
            <BaseSwitch
              active={notificationEnable}
              disabled={!hasNotificationsPermission}
              onValueChange={this.onValueChange}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    user: state.user,
    notificationEnable: state.user.notificationEnable,
    hasNotificationsPermission: state.user.hasNotificationsPermission,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        fetchPutNotificationEnable: UserActions.fetchPutNotificationEnable,
      },
      dispatch,
    ),
)(NotificationSettingScreen);
