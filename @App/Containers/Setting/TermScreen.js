import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { SafeAreaView, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { WebView } from 'react-native-webview';
import { Actions } from 'react-native-router-flux';

import { Metrics, Classes, Colors, Fonts } from 'App/Theme';
import { SecondaryNavbar, SquareButton, BouncyCheckbox } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { SettingActions } from 'App/Stores';

const styles = StyleSheet.create({
  txtDate: {
    ...Fonts.style.bold,
    ...Fonts.style.small,
    ...Classes.marginTop,
    color: Colors.gray_02,
  },
  divider: {
    borderBottomColor: Colors.gray_02,
    borderBottomWidth: 1,
  },
  itemTitle: {
    fontSize: Fonts.size.medium,
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
  },
  activityItemTitle: {
    marginLeft: 0,
  },
});

class TermScreen extends React.Component {
  static propTypes = {
    currentLocales: PropTypes.array,
    isRegisterFlow: PropTypes.bool,

    usagePolicy: PropTypes.object,
    fetchGetUsagePolicy: PropTypes.func.isRequired,
  };

  static defaultProps = {
    usagePolicy: {},
  };

  state = {
    over18YearsOld: false,
  };

  componentDidMount() {
    __DEV__ && console.log('@TermScreen');

    const { fetchGetUsagePolicy } = this.props;
    fetchGetUsagePolicy();
  }

  accept = () => {
    Actions.UserRegisterScreen();
  };

  onPressOver18 = () => {
    this.setState({
      over18YearsOld: !this.state.over18YearsOld,
    });
  };

  render() {
    let { isRegisterFlow, usagePolicy } = this.props;
    let paddingBottom = 100;
    if (!isRegisterFlow) paddingBottom = 0;

    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back navTitle={t('setting_terms')} />

        <View style={{ flex: 1, justifyContent: 'center' }}>
          <View style={Classes.padding}>
            <Text style={[Fonts.style.h7, Fonts.style.bold, Classes.mainEnd]}>
              {t('setting_terms')}
            </Text>
            <Text style={styles.txtDate}>
              {t('setting_privacy_effective_date')}
              {' : '}
              {'November 15, 2020'}
            </Text>
          </View>

          <View style={styles.divider} />
          <View style={[Classes.padding, Classes.fill, { paddingBottom }]}>
            <WebView source={{ html: usagePolicy.body }} />
          </View>
        </View>

        {isRegisterFlow == true && (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              position: 'absolute',
              bottom: 0,
            }}
          >
            <View style={{ flexDirection: 'row', paddingLeft: 15, paddingRight: 15 }}>
              <BouncyCheckbox
                isChecked={this.state.over18YearsOld}
                fillColor={Colors.black}
                checkboxSize={22}
                text=""
                borderColor={Colors.gray_03}
                onPress={() => this.onPressOver18()}
              />
              <Text style={[styles.itemTitle, styles.activityItemTitle]}>
                {t('register_terms_over_18_years_old')}
              </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                style={{
                  paddingLeft: 10,
                  paddingRight: 10,
                  paddingBottom: 10,
                  width: '50%',
                }}
              >
                <SquareButton
                  textStyle={styles.loginStyle}
                  text={t('register_terms_back')}
                  borderOutline
                  radius={6}
                  accessible={true}
                  style={[{ height: 32 }]}
                  onPress={() => Actions.pop()}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  paddingLeft: 10,
                  paddingRight: 10,
                  paddingBottom: 10,
                  width: '50%',
                }}
              >
                <SquareButton
                  textStyle={styles.loginStyle}
                  text={t('register_terms_accept')}
                  radius={6}
                  disabled={!this.state.over18YearsOld}
                  style={[{ height: 32 }]}
                  onPress={() => this.accept()}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    currentLocales: state.appState.currentLocales,
    usagePolicy: state.setting.usagePolicy,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        fetchGetUsagePolicy: SettingActions.fetchGetUsagePolicy,
      },
      dispatch,
    ),
)(TermScreen);
