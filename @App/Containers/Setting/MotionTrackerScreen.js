import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text } from 'react-native';
import { bindActionCreators } from 'redux';

import { MirrorOptions, MirrorEvents } from 'App/Stores/Mirror/Actions';
import { MirrorActions } from 'App/Stores';
import { Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseSwitch } from 'App/Components';

import { StyleSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

class MotionTrackerScreen extends React.Component {
  static propTypes = {
    mirrorOptions: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
  };

  static defaultProps = {
    mirrorOptions: {
      [MirrorOptions.MOTION_TRACKER]: false,
    },
  };

  componentDidMount() {
    __DEV__ && console.log('@MotionTrackerScreen');

    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.GET_WORKOUT_SETTING);
  }

  handleSwitchValueChange = (key) => () => {
    const { mirrorCommunicate, mirrorOptions } = this.props;

    mirrorCommunicate(MirrorEvents.SET_WORKOUT_SETTING, {
      optionName: key,
      value: !mirrorOptions[key],
    });
  };

  render() {
    const { mirrorOptions } = this.props;
    return (
      <SafeAreaView style={[Classes.fill, Classes.white_02_background]}>
        <SecondaryNavbar back navTitle={t('setting_mii_motion_tracker_nav_title')} />

        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_motion_tracker_nav_title')}</Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={mirrorOptions[MirrorOptions.MOTION_TRACKER]}
            onValueChange={this.handleSwitchValueChange(MirrorOptions.MOTION_TRACKER)}
          />
        </View>
        <Text style={styles.contentText}>{t('setting_mii_motion_tracker_content')}</Text>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    mirrorOptions: state.mirror.options,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(MotionTrackerScreen);

const styles = StyleSheet.create({
  itemBox: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    justifyContent: 'space-between',
    borderBottomColor: Colors.black_15,
    borderBottomWidth: 1,
  },
  itemText: {
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
  },
  itemSwitch: {
    marginVertical: Metrics.baseMargin,
    marginRight: '10@s',
  },
  contentText: {
    ...Fonts.style.small500,
    padding: Metrics.baseMargin,
    color: Colors.gray_02,
  },
});
