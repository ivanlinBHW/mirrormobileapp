import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { has, isEmpty, isArray } from 'lodash';
import { Actions } from 'react-native-router-flux';
import {
  View,
  Text,
  Linking,
  Platform,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

import {
  HeadLine,
  BaseAvatar,
  LinkButton,
  HorizontalLine,
  BaseIconButton,
  SecondaryNavbar,
  BaseButton,
} from 'App/Components';
import { Classes, Images, Metrics, Colors, Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { UserActions } from 'App/Stores';
import { Screen, Date as d, Dialog } from 'App/Helpers';
import { Config } from 'App/Config';

import styles from './SettingScreenStyle';

class AccountInfoScreen extends React.Component {
  static propTypes = {
    getUserAccount: PropTypes.func.isRequired,
    user: PropTypes.object,
    deleteAccountSaga: PropTypes.func.isRequired,
  };

  static defaultProps = {
    user: {},
  };

  componentDidMount() {
    __DEV__ && console.log('@AccountInfoScreen');
    const { getUserAccount } = this.props;
    getUserAccount();
  }

  renderHeader = () => {
    let {
      user: {
        signedAvatarUrl,
        updatedAt,
        fullName,
        mainSubscription = {},
        subscriptionMember = {},
      } = {},
    } = this.props;
    if (mainSubscription == null) mainSubscription = {};

    return (
      <View style={styles.headerWrapper}>
        <View style={[styles.leftBox, Classes.rowCenter]}>
          <BaseAvatar
            size={56}
            uri={signedAvatarUrl}
            image={Images.defaultAvatar}
            cache={false}
            active
          />
        </View>

        <View style={[styles.rightBox]}>
          <View style={[Classes.mainSpaceAround]}>
            <Text style={Fonts.style.medium500}>{fullName}</Text>

            <Text
              style={[
                Fonts.style.small500,
                styles.txtGray,
                { marginTop: Metrics.baseMargin / 2 },
              ]}
            >
              {isEmpty(mainSubscription)
                ? '—'
                : `${
                    mainSubscription.identity === 'Manager'
                      ? t('setting_account_info_account_manager')
                      : t('setting_account_info_account_member')
                  }`}
            </Text>

            <View style={Classes.rowCross}>
              <Text style={[Fonts.style.small500, styles.txtGray]}>
                {`${t('setting_account_info_subscription_code')}`}
                {' : '}
              </Text>

              <Text style={[Fonts.style.small500, styles.txtGray]}>
                {isEmpty(mainSubscription)
                  ? '—'
                  : mainSubscription.identity === 'Manager'
                  ? mainSubscription.subscriptionCode
                  : subscriptionMember.subscriptionCode}
              </Text>
              {mainSubscription.identity === 'Manager' ? null : (
                <BaseIconButton
                  iconType="FontAwesome5"
                  iconName="edit"
                  iconSize={Platform.isPad ? Screen.scale(8) : Screen.scale(12)}
                  onPress={Actions.EnterSubscriptionScreen}
                />
              )}
            </View>
          </View>
        </View>
      </View>
    );
  };

  renderPurchasesItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={Actions[item.key]}
        style={[Classes.mainCenter, Classes.paddingHorizontal, Classes.paddingBottom]}
      >
        <Text style={Fonts.style.medium500}>{item.title}</Text>
        <Text style={[Fonts.style.small, styles.txtGray]}>
          {t('setting_another_purchase_valid_until')} : {d.formatDate(item.expireAt)}
        </Text>
      </TouchableOpacity>
    );
  };

  renderAnotherPurchases = () => {
    const {
      user: { mainSubscription = {} },
    } = this.props;
    const channels = mainSubscription.items
      .filter((e) => e.product.type === 1)
      .map((e) => ({
        ...e.product.channel,
        expireAt: e.expireAt,
      }));
    const services = mainSubscription.items
      .filter((e) => e.product.type === 0)
      .map((e) => ({
        ...e.product,
        expireAt: e.expireAt,
      }));
    return (
      <>
        <FlatList
          data={channels}
          style={{ flexGrow: 0, flex: 0 }}
          keyExtractor={(item) => item.id}
          renderItem={this.renderPurchasesItem}
          ItemSeparatorComponent={HorizontalLine}
          ListHeaderComponent={
            <HeadLine
              title={t('setting_another_purchase_my_channel')}
              titleColor={Colors.gray_02}
              titleStyle={Classes.paddingLeft}
              paddingTop={Metrics.baseMargin}
              height={40}
            />
          }
        />
        <FlatList
          data={services}
          containerStyle={Classes.fill}
          keyExtractor={(item) => item.id}
          renderItem={this.renderPurchasesItem}
          ItemSeparatorComponent={HorizontalLine}
          ListHeaderComponent={
            <HeadLine
              title={t('setting_another_purchase_service')}
              titleColor={Colors.gray_02}
              titleStyle={Classes.paddingLeft}
            />
          }
        />
      </>
    );
  };

  renderAccountInfo = () => {
    const {
      user: { email, mainSubscription = {} },
    } = this.props;
    const rowStyle = [
      Classes.row,
      Classes.marginLeft,
      Classes.marginRight,
      Classes.mainSpaceBetween,
    ];
    return (
      <View styles={Classes.paddingHorizontal}>
        <View style={rowStyle}>
          <Text style={Fonts.style.medium}>
            {`${t('setting_account_info_account_number')}`}
          </Text>
          <Text style={[Fonts.style.medium, styles.txtGray]}>{email}</Text>
        </View>

        <View style={[rowStyle, Classes.marginTop]}>
          <Text style={Fonts.style.medium}>
            {`${t('setting_account_info_account_type')}`}
          </Text>
          <Text style={[Fonts.style.medium, styles.txtGray]}>
            {mainSubscription.identity === 'Manager'
              ? t('setting_account_info_account_manager')
              : t('setting_account_info_account_member')}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    const {
      user: { mainSubscription = {} },
      deleteAccountSaga,
    } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back navTitle={t('setting_account_info_title')} />

        <View style={styles.accountInfoWrapper}>
          {this.renderHeader()}
          {!isEmpty(mainSubscription) &&
            has(mainSubscription, 'items') &&
            this.renderAccountInfo()}
        </View>

        {!isEmpty(mainSubscription) &&
          has(mainSubscription, 'items') &&
          isArray(mainSubscription.items) &&
          this.renderAnotherPurchases()}

        <View style={Classes.rowCenter}>
          <BaseButton
            transparent
            textStyle={styles.btnText}
            textColor={Colors.button.primary.text.text}
            onPress={() => {
              Dialog.showConfirmDeleteAcclountAlert(() => deleteAccountSaga());
            }}
            text={`${t('delete_this_account')}`}
            textSize={Fonts.size.small}
          />
        </View>
        <View style={Classes.rowCenter}>
          <Text style={[styles.txtGray, Fonts.style.small]}>
            {t('setting_account_more_info')}
          </Text>
        </View>
        <View style={Classes.rowCenter}>
          <LinkButton
            onPress={() => Linking.openURL(Config.SHOP_LINK)}
            text={`${t('setting_account_more_info_shop')}`}
            textSize={Fonts.size.small}
            textColor={Colors.gray_01}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    user: state.user,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getUserAccount: UserActions.getUserAccount,
        deleteAccountSaga: UserActions.deleteAccountSaga,
      },
      dispatch,
    ),
)(AccountInfoScreen);
