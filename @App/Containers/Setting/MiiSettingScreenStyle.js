import { StyleSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default StyleSheet.create({
  container: {
    marginHorizontal: Metrics.baseMargin,
    height: '100%',
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '40@vs',
  },
  imageMain: {
    width: '60@s',
    height: '60@vs',
  },
  txtWrapper: {
    marginVertical: '40@vs',
    alignItems: 'center',
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: Metrics.baseMargin / 2,
  },
  txtFooter: {
    textAlign: 'center',
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'center',
    color: Colors.gray_01,
    textAlign: 'center',
    alignItems: 'center',
  },

  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  title: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin,
  },
  activeBox: {
    backgroundColor: Colors.white,
    marginHorizontal: Metrics.baseMargin,
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    marginTop: Metrics.baseMargin,
    alignItems: 'center',
  },
  badge: {
    width: '12.8@s',
    height: '12.8@s',
    borderRadius: '6.2@s',
    marginLeft: '25.6@s',
  },
  headerText: {
    ...Fonts.style.medium500,
    marginLeft: '25.6@s',
    marginVertical: Metrics.baseMargin,
  },
  imageBox: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  menuBox: {
    width: '30@s',
    height: 'auto',
    top: 0,
    marginRight: Metrics.baseMargin,
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  hamburger: {
    width: '30@s',
    height: '30@vs',
  },
  itemBox: {
    alignItems: 'flex-start',
    justifyContent: 'center',

    marginHorizontal: Metrics.baseMargin,
    backgroundColor: Colors.white,
    marginBottom: Metrics.baseMargin,
  },
  itemText: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin * 4,
  },
  footerText: {
    ...Fonts.style.small500,
    marginTop: '24@vs',
    marginHorizontal: Metrics.baseMargin,
    color: Colors.gray_02,
  },
  optionStyle: {
    width: '100%',
  },
  optionText: {
    marginVertical: '14@vs',
    marginLeft: '4@s',
  },
  btnNabBarRight: {
    justifyContent: 'flex-end',
    position: 'absolute',
    right: 0,
  },
});
