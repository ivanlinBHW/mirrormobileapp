import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import PickerModal from 'react-native-picker-modal-view';

import { MirrorOptions, MirrorEvents } from 'App/Stores/Mirror/Actions';
import { AppStateActions, MirrorActions } from 'App/Stores';
import { Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseSwitch } from 'App/Components';

import styles from './WeatherScreenStyle';

import location from 'App/Assets/Location';

class WeatherScreen extends React.Component {
  static propTypes = {
    mirrorSlideshowOptions: PropTypes.object,
    mirrorDisplayOptions: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
  };

  static defaultProps = {
    mirrorDisplayOptions: {
      [MirrorOptions.DISPLAY_TEMPERATURE]: true,
      [MirrorOptions.DISPLAY_HUMIDITY]: true,
      [MirrorOptions.DISPLAY_WEATHER_ICON]: true,
      [MirrorOptions.DISPLAY_LOCATION]: '----------',
    },
  };

  state = {
    selectedItem: [],
  };

  componentDidMount() {
    __DEV__ && console.log('@WeatherScreen');

    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.GET_DEVICE_DISPLAY_SETTING);
  }

  handleSwitchValueChange = (key) => () => {
    const { mirrorCommunicate, mirrorDisplayOptions } = this.props;

    mirrorCommunicate(MirrorEvents.UPDATE_DEVICE_DISPLAY_SETTING, {
      optionName: key,
      value: !mirrorDisplayOptions[key],
    });
  };

  onSelected = (value) => {
    const { mirrorCommunicate } = this.props;
    if (!isEmpty(value)) {
      mirrorCommunicate(MirrorEvents.UPDATE_DEVICE_DISPLAY_SETTING, {
        optionName: MirrorOptions.DISPLAY_LOCATION,
        value: value.Name,
      });
    }
  };

  render() {
    const { mirrorDisplayOptions } = this.props;
    const { selectedItem } = this.state;
    return (
      <SafeAreaView style={[Classes.fill, Classes.white_02_background]}>
        <SecondaryNavbar back navTitle={t('setting_mii_display_weather')} />

        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_display_temperature')}</Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={mirrorDisplayOptions[MirrorOptions.DISPLAY_TEMPERATURE]}
            onValueChange={this.handleSwitchValueChange(
              MirrorOptions.DISPLAY_TEMPERATURE,
            )}
          />
        </View>

        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_display_humidity')}</Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={mirrorDisplayOptions[MirrorOptions.DISPLAY_HUMIDITY]}
            onValueChange={this.handleSwitchValueChange(MirrorOptions.DISPLAY_HUMIDITY)}
          />
        </View>

        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_display_weather_icon')}</Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={mirrorDisplayOptions[MirrorOptions.DISPLAY_WEATHER_ICON]}
            onValueChange={this.handleSwitchValueChange(
              MirrorOptions.DISPLAY_WEATHER_ICON,
            )}
          />
        </View>

        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_weather_location')}</Text>
          <View style={styles.usbText}>
            <PickerModal
              renderSelectView={(disabled, selected, showModal) => (
                <TouchableOpacity
                  style={styles.locationBox}
                >
                  <Text style={styles.usbText}>
                    {mirrorDisplayOptions[MirrorOptions.DISPLAY_LOCATION]}
                  </Text>
                </TouchableOpacity>
              )}
              onSelected={(value) => this.onSelected(value)}
              items={location}
              sortingLanguage={'tr'}
              selected={selectedItem}
              selectPlaceholderText={'Choose one...'}
              onEndReached={() => console.log('list ended...')}
              searchPlaceholderText={'Search...'}
              requireSelection={false}
              autoGenerateAlphabeticalIndex
              showAlphabeticalIndex
              showToTopButton
              autoSort
            />
          </View>
        </View>
        <Text style={styles.contentText}>{t('setting_mii_weather_content')}</Text>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    mirrorDisplayOptions: state.mirror.displayOptions,
    mirrorSlideshowOptions: state.mirror.slideshowOptions,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(WeatherScreen);
