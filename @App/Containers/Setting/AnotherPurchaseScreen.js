import React from 'react';
import PropTypes from 'prop-types';
import { isArray, has } from 'lodash';
import { connect } from 'react-redux';
import { SafeAreaView, Text, FlatList, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { Classes, Fonts, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d } from 'App/Helpers';
import { SecondaryNavbar, HeadLine, HorizontalLine } from 'App/Components';
import SettingActions from 'App/Stores/Setting/Actions';
import UserActions from 'App/Stores/User/Actions';
import { MirrorActions } from 'App/Stores';

import styles from './SettingScreenStyle';

class AnotherPurchaseScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object,
  };

  static defaultProps = {
    user: {},
  };

  state = {
    engineeringModeCount: 1,
  };

  componentDidMount() {
    __DEV__ && console.log('@AnotherPurchaseScreen');
  }

  renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={Actions[item.key]}
        style={[Classes.paddingLeft, Classes.paddingRight]}
      >
        <Text style={Fonts.style.medium500}>
          {has(item, 'product.channel.title') ? item.product.channel.title : ''}
        </Text>
        <Text style={[Fonts.style.small, styles.txtGray]}>
          {t('setting_another_purchase_valid_until')} : {d.formatDate(item.expireAt)}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {
      user: { mainSubscription = {} },
    } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back navTitle={t('setting_account_item_another_purchase')} />
        {isArray(mainSubscription.items) && (
          <FlatList
            data={mainSubscription.items}
            keyExtractor={(item) => item.id}
            renderItem={this.renderItem}
            ItemSeparatorComponent={<HorizontalLine />}
            ListHeaderComponent={
              <HeadLine
                title={t('setting_another_purchase_my_channel')}
                titleColor={Colors.gray_02}
                titleStyle={Classes.paddingLeft}
              />
            }
            ListFooterComponent={
              <FlatList
                data={mainSubscription.items}
                keyExtractor={(item) => item.id}
                renderItem={this.renderItem}
                ItemSeparatorComponent={<HorizontalLine />}
                ListHeaderComponent={
                  <HeadLine
                    title={t('setting_another_purchase_service')}
                    titleColor={Colors.gray_02}
                    titleStyle={Classes.paddingLeft}
                  />
                }
              />
            }
          />
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    user: state.user,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getSetting: SettingActions.getSetting,
        userLogout: UserActions.onUserLogout,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(AnotherPurchaseScreen);
