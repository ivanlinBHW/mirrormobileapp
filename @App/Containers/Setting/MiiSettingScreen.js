import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ActivityIndicator, SafeAreaView, Platform, Image, View } from 'react-native';
import { bindActionCreators } from 'redux';

import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { Classes, Images } from 'App/Theme';
import { MdnsActions, AppStateActions as AppActions, MirrorActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { Screen } from 'App/Helpers';
import {
  filterMirrorService,
  filterInitializedMirrorService,
} from 'App/Stores/Mirror/Selectors';
import { SecondaryNavbar, BaseIconButton } from 'App/Components';
import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';

import styles from './MiiSettingScreenStyle';
import MiiSettingDeviceView from './MiiSettingDeviceView';

class MiiSettingScreen extends React.Component {
  static propTypes = {
    mirrorCommunicate: PropTypes.func.isRequired,
    lastDevice: PropTypes.object,
    isPaired: PropTypes.bool.isRequired,
    mirrorServices: PropTypes.array.isRequired,
    mirrorInitializedServices: PropTypes.array.isRequired,
    mirrorLastInitializedServices: PropTypes.array.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsClear: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    connectedDevice: PropTypes.object,
    mirrorDisconnect: PropTypes.func.isRequired,
    isScanning: PropTypes.bool.isRequired,
    isConnected: PropTypes.bool.isRequired,
    deviceWifiSSID: PropTypes.string,
    currentSceneName: PropTypes.string,
  };

  static defaultProps = {};

  optionStyle = {
    optionWrapper: styles.optionStyle,
    optionText: styles.optionText,
  };

  componentDidMount() {
    __DEV__ && console.log('@MiiSettingScreen');

    this.handleDiscoverDevice();

    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.GET_DEVICE_INFO);
  }

  componentDidUpdate(prevProps) {
    const {
      mirrorInitializedServices = [],
      connectedDevice,
      mirrorLastInitializedServices,
      deviceWifiSSID,
      currentSceneName,
    } = this.props;

    console.log('=== currentSceneName ===', currentSceneName);
    if (
      !connectedDevice &&
      !this.state.refreshing &&
      deviceWifiSSID != null &&
      deviceWifiSSID != '' &&
      currentSceneName == 'MiiFirstSettingScreen'
    ) {
      this.handleDiscoverDevice();
    }
  }

  state = {
    refreshing: false,
  };

  handleDiscoverDevice = () => {
    this.setState({ selectedDevice: null, refreshing: true });
    const { mirrorDiscover, mdnsStop, mdnsClear, onLoading } = this.props;
    onLoading(true, null, { hide: true });
    mdnsClear();
    mirrorDiscover();

    setTimeout(() => {
      mdnsStop();
      onLoading(false);
      this.setState({ refreshing: false });
    }, 15000);
  };

  render() {
    const { isScanning } = this.props;
    return (
      <SafeAreaView style={[Classes.fill, Classes.white_02_background]}>
        <SecondaryNavbar
          back
          navTitle={t('setting_mii_nav_title')}
          navRightComponent={
            isScanning ? (
              <ActivityIndicator size="small" color="black" />
            ) : (
              <BaseIconButton
                iconType="FontAwesome5"
                iconName="redo"
                iconSize={Platform.select({
                  ios: Platform.isPad ? Screen.scale(8) : Screen.scale(16),
                  android: Screen.scale(16),
                })}
                onPress={this.handleDiscoverDevice}
                style={Classes.colCenter}
              />
            )
          }
        />
        <View style={styles.imageWrapper}>
          <Image source={Images.connectWifi} style={styles.imageMain} />
        </View>

        <MiiSettingDeviceView {...this.props} />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    isConnected: state.mdns.isConnected,
    isScanning: state.mdns.isScanning,
    lastDevice: state.mirror.lastDevice,
    isPaired: state.mirror.isPaired,
    mirrorServices: filterMirrorService(state),
    connectedDevice: state.mirror.connectedDevice,
    mirrorInitializedServices: filterInitializedMirrorService(state, false),
    mirrorLastInitializedServices: filterInitializedMirrorService(state),
    deviceWifiSSID: state.appState.deviceWifiSSID,
    currentSceneName: state.appRoute.routeName,
    hasDistanceAdjustment: filterMirrorHasFeatureOption(state, 'distanceAdjustment'),
    hasScreenCasting: filterMirrorHasFeatureOption(state, 'screenCasting'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mdnsClear: MdnsActions.onMdnsScanClear,
        mdnsStop: MdnsActions.onMdnsStop,
        onLoading: AppActions.onLoading,
        mirrorDiscover: MdnsActions.onMdnsScan,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
        mirrorDisconnect: MirrorActions.onMirrorDisconnectByUser,
        mirrorConnect: MirrorActions.onMirrorConnect,
      },
      dispatch,
    ),
)(MiiSettingScreen);
