import { StyleSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default StyleSheet.create({
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  itemBox: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: Colors.black_15,
    borderBottomWidth: 1,
  },
  itemText: {
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
  },
  itemSwitch: {
    marginVertical: Metrics.baseMargin,
    marginRight: '10@s',
  },
  contentText: {
    ...Fonts.style.small500,
    padding: Metrics.baseMargin,
    color: Colors.gray_02,
  },
  itemImg: {
    width: '8@s',
    height: '14@vs',
    marginRight: Metrics.baseMargin,
    marginVertical: '17@vs',
  },
  usbText: {
    fontSize: Fonts.size.medium,
    marginRight: '10@s',
    color: Colors.gray_02,
  },
  badge: {
    width: '12.8@s',
    height: '12.8@s',
    borderRadius: '6.4@s',
    marginRight: '25.6@s',
    marginTop: '22@vs',
  },
  locationBox: {
    height: '16@vs',
    minWidth: '39@s',
  },
});
