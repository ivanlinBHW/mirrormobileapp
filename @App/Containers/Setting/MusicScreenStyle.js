import { StyleSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default StyleSheet.create({
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  musicImg: {
    width: '32@s',
    marginVertical: '11.2@s',
    marginLeft: Metrics.baseMargin,
    marginRight: '40@s',
  },
  itemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  sourceWrapper: {
    width: '80@s',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  itemText: {
    ...Fonts.style.medium500,
    alignSelf: 'flex-start',
    textAlign: 'left',
    marginVertical: Metrics.baseMargin,
  },
  spotify: {
    marginVertical: '11.2@s',
    marginLeft: '19@s',
    marginRight: '40@s',
    width: '32@s',
  },
  wrapper: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_10,
  },
  badge: {
    width: '12@s',
    height: '12@s',
    borderRadius: '6@s',
    borderWidth: 1,
    borderColor: Colors.transparent,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: Metrics.baseMargin,
  },
  badgeBox: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-end',
  },
  standalone: {
    marginTop: 30,
    marginBottom: 30,
  },
  standaloneRowFront: {
    backgroundColor: Colors.white,
  },
  standaloneRowBack: {
    alignItems: 'center',
    backgroundColor: Colors.primary,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: Metrics.baseMargin,
  },
  backTextWhite: {
    ...Fonts.style.medium500,
    color: Colors.white,
  },
});
