import { ScaledSheet } from 'App/Helpers';
import { Metrics, Colors, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white_02,
  },
  listBox: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
    backgroundColor: Colors.white,
  },
  rightItem: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginRight: '12@s',
  },
  itemText: {
    ...Fonts.style.medium,
    marginLeft: Metrics.baseMargin,
    marginVertical: Metrics.baseMargin,
  },
});
