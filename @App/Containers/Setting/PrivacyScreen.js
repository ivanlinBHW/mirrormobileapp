import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { SafeAreaView, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { WebView } from 'react-native-webview';

import { SettingActions } from 'App/Stores';
import { Classes, Colors, Fonts } from 'App/Theme';
import { SecondaryNavbar, SquareButton } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Actions } from 'react-native-router-flux';

const styles = StyleSheet.create({
  txtDate: {
    ...Fonts.style.bold,
    ...Fonts.style.small,
    ...Classes.marginTop,
    color: Colors.gray_02,
  },
  divider: {
    borderBottomColor: Colors.gray_02,
    borderBottomWidth: 1,
  },
});

class PrivacyScreen extends React.Component {
  static propTypes = {
    currentLocales: PropTypes.array,
    isRegisterFlow: PropTypes.bool,

    privacyPolicy: PropTypes.object,
    fetchGetPrivacyPolicy: PropTypes.func.isRequired,
  };

  static defaultProps = {
    privacyPolicy: {},
  };

  componentDidMount() {
    __DEV__ && console.log('@PrivacyScreen');

    const { fetchGetPrivacyPolicy } = this.props;
    fetchGetPrivacyPolicy();
  }

  understand = () => {
    Actions.UserRegisterTermScreen({ isRegisterFlow: true });
  };

  render() {
    let { isRegisterFlow, privacyPolicy } = this.props;
    let paddingBottom = 60;
    if (!isRegisterFlow) paddingBottom = 0;

    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back navTitle={t('setting_privacy_policy')} />

        <View style={{ flex: 1, justifyContent: 'center' }}>
          <View style={Classes.padding}>
            <Text style={[Fonts.style.h7, Fonts.style.bold, Classes.mainEnd]}>
              {t('setting_privacy_policy')}
            </Text>
            <Text style={styles.txtDate}>
              {t('setting_privacy_effective_date')}
              {' : '}
              {'November 15, 2020'}
            </Text>
          </View>

          <View style={styles.divider} />
          <View style={[Classes.padding, Classes.fill, { paddingBottom }]}>
            <WebView source={{ html: privacyPolicy.body }} />
          </View>
        </View>

        {isRegisterFlow == true && (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              position: 'absolute',
              bottom: 0,
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity style={{ padding: 15, width: '100%' }}>
                <SquareButton
                  textStyle={styles.loginStyle}
                  text={t('register_privacy_policy_understand')}
                  radius={6}
                  accessible={true}
                  style={[{ height: 32 }]}
                  onPress={() => this.understand()}
                />
              </TouchableOpacity>
            </View>
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    currentLocales: state.appState.currentLocales,
    privacyPolicy: state.setting.privacyPolicy,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        fetchGetPrivacyPolicy: SettingActions.fetchGetPrivacyPolicy,
      },
      dispatch,
    ),
)(PrivacyScreen);
