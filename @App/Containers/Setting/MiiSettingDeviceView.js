import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  FlatList,
  Image,
  RefreshControl,
  TouchableWithoutFeedback,
} from 'react-native';
import { debounce } from 'lodash';
import { Actions } from 'react-native-router-flux';
import { Badge } from 'react-native-elements';
import { MenuProvider } from 'react-native-popup-menu';
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import { RoundButton } from '@ublocks-react-native/component';

import { Classes, Images } from 'App/Theme';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog } from 'App/Helpers';

import styles from './MiiSettingScreenStyle';

class MiiSettingDeviceView extends React.PureComponent {
  static propTypes = {
    lastDevice: PropTypes.object,
    isPaired: PropTypes.bool.isRequired,
    mirrorServices: PropTypes.array.isRequired,
    mirrorInitializedServices: PropTypes.array.isRequired,
    mirrorLastInitializedServices: PropTypes.array.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsClear: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mirrorConnect: PropTypes.func.isRequired,
    mirrorDisconnect: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    connectedDevice: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
    deviceWifiSSID: PropTypes.string,
    currentSceneName: PropTypes.string,
    hasDistanceAdjustment: PropTypes.bool.isRequired,
    hasScreenCasting: PropTypes.bool.isRequired,
    isCasting: PropTypes.bool.isRequired,
    isConnected: PropTypes.bool.isRequired,
  };

  static defaultProps = {};

  optionStyle = {
    optionWrapper: styles.optionStyle,
    optionText: styles.optionText,
  };

  componentDidMount() {
    __DEV__ && console.log('@MiiSettingDeviceView');
  }

  state = {
    refreshing: false,
  };

  handleDiscoverDevice = () => {
    this.setState({ selectedDevice: null });
    const { mirrorDiscover, mdnsStop, mdnsClear, onLoading } = this.props;
    onLoading(true);
    mdnsClear();
    mirrorDiscover();

    setTimeout(() => {
      mdnsStop();
      onLoading(false);
    }, 10000);
  };

  handleConnectDevice = (item) => () => {
    const { mirrorConnect, onLoading, mirrorDisconnect } = this.props;
    onLoading(true);
    mirrorDisconnect();
    setTimeout(() => {
      mirrorConnect(item, {
        maxRetries: 5,
      });
    }, 1000);
  };

  renderItem = ({ item, index }) => {
    return (
      <RoundButton
        key={`${index}`}
        style={styles.itemBox}
        onPress={this.handleConnectDevice(item)}
        transparent
        text={item.name}
        textStyle={styles.itemText}
      />
    );
  };

  onPressSetting = (value) => {
    const { mirrorDisconnect, mirrorCommunicate } = this.props;

    switch (value) {
      case 'display_setting':
        Actions.DisplayScreen();
        break;

      case 'distance_measuring':
        mirrorCommunicate(MirrorEvents.START_DISTANCE_MEASURING);
        break;

      case 'disconnect':
        mirrorDisconnect();
        break;

      case 'motion_tracker':
        Actions.MotionTrackerScreen();
        break;

      case 'device_update':
        mirrorCommunicate(MirrorEvents.DEVICE_CHECK_UPDATE_REQUEST, {
          updateIfHasNewVersion: false,
        });
        break;

      case 'cast_to_mirror':
        Actions.MirrorCastScreen();
        break;

      default:
        break;
    }
  };
  componentDidUpdate(prevProps) {
    const {
      mirrorInitializedServices = [],
      lastDevice,
      connectedDevice,
      mirrorLastInitializedServices,
      deviceWifiSSID,
      currentSceneName,
    } = this.props;

    console.log('=== currentSceneName ===', currentSceneName);
    if (
      !connectedDevice &&
      deviceWifiSSID != null &&
      deviceWifiSSID != '' &&
      currentSceneName == 'MiiFirstSettingScreen'
    ) {
      if (
        prevProps.mirrorLastInitializedServices.length !=
        mirrorLastInitializedServices.length
      ) {
        mirrorLastInitializedServices.forEach((device) => {
          if (deviceWifiSSID == device.name) {
            console.log('=== can connect 1===');
            setTimeout(() => this.handleConnectDevice(device)(), 3000);
          }
        });
      }
      if (
        prevProps.mirrorInitializedServices.length != mirrorInitializedServices.length
      ) {
        mirrorInitializedServices.forEach((device) => {
          if (deviceWifiSSID == device.name) {
            console.log('=== can connect 2===');
            setTimeout(() => this.handleConnectDevice(device)(), 3000);
          }
        });
      }
    }

    if (
      currentSceneName == 'MiiFirstSettingScreen' &&
      connectedDevice &&
      prevProps.connectedDevice != connectedDevice
    ) {
      setTimeout(() => Actions.ClassScreen(), 1000);
    }
  }
  render() {
    const {
      mirrorInitializedServices = [],
      lastDevice,
      connectedDevice,
      mirrorLastInitializedServices,
      hasDistanceAdjustment,
      hasScreenCasting,
      mirrorCommunicate,
    } = this.props;
    const { refreshing } = this.state;

    console.log(connectedDevice);
    console.log(mirrorInitializedServices);

    return (
      <MenuProvider style={Classes.fill}>
        {connectedDevice && (
          <View style={styles.activeBox}>
            <Badge status="success" badgeStyle={styles.badge} />
            <Text style={styles.headerText}>{lastDevice ? lastDevice.name : ''}</Text>

            <View style={[Classes.fillRowCenter, Classes.mainEnd]}>
              <View style={styles.menuBox}>
                <Menu
                  onSelect={debounce((value) => this.onPressSetting(value), 250)}
                  onOpen={debounce(() =>
                    mirrorCommunicate(MirrorEvents.GET_DEVICE_INFO, 250),
                  )}
                >
                  <MenuTrigger
                  >
                    <Image source={Images.hamburger_menu} style={styles.hamburger} />
                  </MenuTrigger>
                  <MenuOptions>
                    <MenuOption
                      customStyles={this.optionStyle}
                      value="display_setting"
                      text={t('setting_mii_display_setting')}
                    />
                    <TouchableWithoutFeedback
                      onPress={Dialog.showDeviceHasNoCameraAlert}
                      pointerEvents={hasDistanceAdjustment ? 'box-none' : 'box-only'}
                    >
                      <MenuOption
                        customStyles={this.optionStyle}
                        value="distance_measuring"
                        text={t('setting_mii_distance_mesuring')}
                        disabled={!hasDistanceAdjustment}
                      />
                    </TouchableWithoutFeedback>
                    <MenuOption
                      customStyles={this.optionStyle}
                      value="disconnect"
                      text={t('setting_mii_disconnect')}
                    />
                    <TouchableWithoutFeedback
                      onPress={Dialog.showDeviceHasNoCameraAlert}
                      pointerEvents={hasDistanceAdjustment ? 'box-none' : 'box-only'}
                    >
                      <MenuOption
                        customStyles={this.optionStyle}
                        value="motion_tracker"
                        text={t('setting_mii_motion_tracker')}
                        disabled={!hasDistanceAdjustment}
                      />
                    </TouchableWithoutFeedback>
                    <MenuOption
                      customStyles={this.optionStyle}
                      value="device_update"
                      text={t('setting_mii_device_update')}
                    />
                    <TouchableWithoutFeedback
                      onPress={Dialog.showDeviceHasNoCameraAlert}
                      pointerEvents={hasScreenCasting ? 'box-none' : 'box-only'}
                    >
                      <MenuOption
                        customStyles={this.optionStyle}
                        value="cast_to_mirror"
                        text={t('cast_to_mirror')}
                        disabled={!hasScreenCasting}
                      />
                    </TouchableWithoutFeedback>
                  </MenuOptions>
                </Menu>
              </View>
            </View>
          </View>
        )}
        <Text style={styles.title}>{t('setting_mii_title')}</Text>
        <FlatList
          data={
            connectedDevice ? mirrorLastInitializedServices : mirrorInitializedServices
          }
          keyExtractor={(item) => item.host}
          renderItem={this.renderItem}
          ListFooterComponent={
            <View>
              <RoundButton
                style={styles.itemBox}
                onPress={Actions.MirrorWifiHomeConnectScreen}
                transparent
              >
                <Text style={styles.itemText}>
                  {t('mirror_setup_pair_setup_new_mirror')}
                </Text>
              </RoundButton>
              {connectedDevice == null && mirrorInitializedServices.length === 0 && (
                <Text style={styles.footerText}>{t('setting_mii_footer')}</Text>
              )}
            </View>
          }
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this.handleDiscoverDevice}
              enabled
            />
          }
        />
      </MenuProvider>
    );
  }
}

export default MiiSettingDeviceView;
