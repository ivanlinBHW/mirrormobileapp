import { StyleSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default StyleSheet.create({
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  itemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.white,
    justifyContent: 'space-between',
    borderBottomColor: Colors.black_15,
    borderBottomWidth: 1,
  },
  itemText: {
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
  },
  itemSwitch: {
    marginVertical: Metrics.baseMargin,
    marginRight: '10@s',
  },
  contentText: {
    ...Fonts.style.small500,
    padding: Metrics.baseMargin,
    color: Colors.gray_02,
  },
  itemImg: {
    width: '14@s',
    marginRight: Metrics.baseMargin * 2,
  },
});
