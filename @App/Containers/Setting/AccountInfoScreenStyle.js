import { StyleSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors, Classes } from 'App/Theme';

export default StyleSheet.create({
  container: {
    ...Classes.fill,
    backgroundColor: Colors.white_02,
  },
  btnWrapper: {
    flex: 0.9,
  },
  button: {
    alignItems: 'flex-start',
    paddingLeft: Metrics.baseMargin,
    borderWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: Colors.gray_12,
    backgroundColor: Colors.white,
    borderRadius: 0,
  },
  txtVersion: {
    ...Fonts.style.small,
    flex: 0.1,
    paddingLeft: Metrics.baseMargin,
    color: Colors.gray_02,
  },
  txtGray: {
    color: Colors.gray_02,
  },
});
