import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, SafeAreaView, Text } from 'react-native';

import { MirrorOptions, MirrorEvents } from 'App/Stores/Mirror/Actions';
import { SecondaryNavbar, BaseSwitch } from 'App/Components';
import { AppStateActions, MirrorActions } from 'App/Stores';
import { Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Dialog } from 'App/Helpers';
import { INITIAL_STATE as MirrorInitialState } from 'App/Stores/Mirror/InitialState';
import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';
import styles from './DisplayScreenStyle';

class MirrorCastScreen extends React.Component {
  static propTypes = {
    featureOptions: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    hasScreenCasting: PropTypes.bool.isRequired,
    isCasting: PropTypes.bool.isRequired,
    isConnected: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    featureOptions: MirrorInitialState.featureOptions,
  };

  componentDidMount() {
    __DEV__ && console.log('@DisplayScreen');
  }

  handleSwitchValueChange = (key) => () => {
    const { mirrorCommunicate, hasScreenCasting } = this.props;

    if (hasScreenCasting) {
      mirrorCommunicate(MirrorEvents.UPDATE_DEVICE_SETTING, {
        optionName: key,
        value: true,
      });

      mirrorCommunicate(MirrorEvents.GET_DEVICE_INFO);
    } else {
      Dialog.showDeviceHasNoCameraAlert();
    }
  };

  render() {
    const { isCasting, isConnected, hasScreenCasting } = this.props;
    console.log('isCasting=>', isCasting);
    return (
      <SafeAreaView style={[Classes.fill, Classes.white_02_background]}>
        <SecondaryNavbar back navTitle={t('mirror_cast')} />
        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('start_casting')}</Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={isCasting}
            disabled={!hasScreenCasting || !isConnected}
            onValueChange={this.handleSwitchValueChange(MirrorOptions.CASTING)}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    isConnected: state.mirror.isConnected,
    isCasting: state.mirror.isCasting,
    hasScreenCasting: filterMirrorHasFeatureOption(state, 'screenCasting'),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(MirrorCastScreen);
