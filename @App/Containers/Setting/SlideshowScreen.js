import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, FlatList, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { Badge } from 'react-native-elements';

import { MirrorOptions, MirrorEvents } from 'App/Stores/Mirror/Actions';
import { AppStateActions, MirrorActions } from 'App/Stores';
import { Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BaseSwitch } from 'App/Components';

import styles from './SlideshowScreenStyle';

class SlideshowScreen extends React.Component {
  static propTypes = {
    mirrorSlideshowOptions: PropTypes.object,
    mirrorDisplayOptions: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
  };

  static defaultProps = {
    mirrorDisplayOptions: {
      [MirrorOptions.DISPLAY_SLIDE_SHOW]: true,
    },
    mirrorSlideshowOptions: {
      [MirrorOptions.SLIDE_SHOW_USB_NAME]: 'Johndoe',
      [MirrorOptions.SLIDE_SHOW_FOLDER_LIST]: [
        { name: 'folder1', selected: true },
        { name: 'folder2', selected: false },
        { name: 'folder3', selected: false },
        { name: 'folder4', selected: false },
      ],
    },
  };

  componentDidMount() {
    __DEV__ && console.log('@SlideshowScreen');

    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.GET_DEVICE_DISPLAY_SETTING);
  }

  handleSwitchValueChange = (key) => () => {
    const {
      mirrorCommunicate,
      mirrorDisplayOptions,
      mirrorSlideshowOptions,
    } = this.props;

    mirrorCommunicate(MirrorEvents.UPDATE_DEVICE_DISPLAY_SETTING, {
      optionName: key,
      value: !mirrorDisplayOptions[key],
      folders: mirrorSlideshowOptions[MirrorOptions.SLIDE_SHOW_FOLDER_LIST],
    });

    mirrorCommunicate(MirrorEvents.GET_DEVICE_DISPLAY_SETTING);
  };

  handleSelectValueChange = (selectedName) => () => {
    const {
      mirrorCommunicate,
      mirrorDisplayOptions,
      mirrorSlideshowOptions,
    } = this.props;

    mirrorCommunicate(MirrorEvents.UPDATE_DEVICE_DISPLAY_SETTING, {
      optionName: MirrorOptions.DISPLAY_SLIDE_SHOW,
      value: mirrorDisplayOptions[MirrorOptions.DISPLAY_SLIDE_SHOW],
      folders: mirrorSlideshowOptions[MirrorOptions.SLIDE_SHOW_FOLDER_LIST].map((e) =>
        e.name === selectedName
          ? { name: e.name, selected: true }
          : { name: e.name, selected: false },
      ),
    });

    mirrorCommunicate(MirrorEvents.GET_DEVICE_DISPLAY_SETTING);
  };

  renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        key={`${index}`}
        style={styles.itemBox}
        onPress={this.handleSelectValueChange(item.name)}
      >
        <Text style={styles.itemText}>{item.name}</Text>
        {item.selected && <Badge status="success" badgeStyle={styles.badge} />}
      </TouchableOpacity>
    );
  };

  render() {
    const { mirrorDisplayOptions, mirrorSlideshowOptions } = this.props;
    return (
      <SafeAreaView style={[Classes.fill, Classes.white_02_background]}>
        <SecondaryNavbar back navTitle={t('setting_mii_slideshow_nav_title')} />

        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_display_slideshow')}</Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={mirrorDisplayOptions[MirrorOptions.DISPLAY_SLIDE_SHOW]}
            onValueChange={this.handleSwitchValueChange(MirrorOptions.DISPLAY_SLIDE_SHOW)}
          />
        </View>
        <Text style={styles.contentText}>{t('setting_mii_slideshow_content')}</Text>

        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_slideshow_usb_name')}</Text>
          <Text style={styles.usbText}>
            {mirrorSlideshowOptions[MirrorOptions.SLIDE_SHOW_USB_NAME]}
          </Text>
        </View>

        <FlatList
          data={mirrorSlideshowOptions[MirrorOptions.SLIDE_SHOW_FOLDER_LIST]}
          renderItem={this.renderItem}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    mirrorDisplayOptions: state.mirror.displayOptions,
    mirrorSlideshowOptions: state.mirror.slideShowOptions,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(SlideshowScreen);
