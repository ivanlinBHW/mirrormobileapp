import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, FlatList, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';

import { Classes, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { SecondaryNavbar, BouncyCheckbox, HorizontalLine } from 'App/Components';
import SettingActions from 'App/Stores/Setting/Actions';

import styles from './PrivacySettingScreenStyle';

class PrivacySettingScreen extends React.Component {
  static propTypes = {
    userSetting: PropTypes.object,
    updateSetting: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      list: [
        {
          title: 'setting_privacy_everyone',
          key: 1,
        },
        {
          title: 'setting_privacy_friend',
          key: 2,
        },
        {
          title: 'setting_privacy_onlyme',
          key: 3,
        },
      ].map((e) => ({ ...e, selected: e.key === props.userSetting.privacy })),
      selected: props.userSetting.privacy,
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@PrivacySettingScreen');
  }

  onPressItem = (key) => () => {
    this.setState(
      (state) => ({
        list: state.list.map((e) => ({ ...e, selected: e.key === key })),
        selected: key,
      }),
      () => {
        const { updateSetting } = this.props;
        updateSetting({ privacy: key });
      },
    );
  };

  renderItem = ({ item }) => {
    return (
      <View style={styles.itemWrapper}>
        <TouchableOpacity style={styles.item} onPress={this.onPressItem(item.key)}>
          <Text style={styles.itemTitle}>{t(`${item.title}`)}</Text>
          <BouncyCheckbox
            isChecked={item.selected}
            fillColor={Colors.black}
            checkboxSize={22}
            text=""
            borderColor={Colors.gray_03}
            onPress={this.onPressItem(item.key)}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { list } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar navHeight={44} back navTitle={t('setting_privacy_title')} />
        <Text style={styles.placeholder}>{t('setting_privacy_desc')}</Text>
        <HorizontalLine />
        <FlatList
          data={list}
          keyExtractor={(item) => item.title}
          renderItem={this.renderItem}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    userSetting: state.setting.setting.settings,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateSetting: SettingActions.updateSetting,
      },
      dispatch,
    ),
)(PrivacySettingScreen);
