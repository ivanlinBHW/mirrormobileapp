import React from 'react';
import { isEmpty, isString, get } from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, Image, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { SwipeRow } from 'react-native-swipe-list-view';
import { Badge } from 'react-native-elements';

import { Spotify, Screen } from 'App/Helpers';
import { Classes, Images } from 'App/Theme';
import { SecondaryNavbar } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';

import styles from './MusicScreenStyle';

class MusicScreen extends React.Component {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
    activeList: PropTypes.string,
    spotifyIsLogin: PropTypes.bool.isRequired,

    clientID: PropTypes.string,
    clientSecret: PropTypes.string,
    scopes: PropTypes.array,
    redirectUrl: PropTypes.string,
    tokenSwapUrl: PropTypes.string,
    tokenRefreshUrl: PropTypes.string,
    tokenRefreshEarliness: PropTypes.number,
    sessionUserDefaultsKey: PropTypes.string,
    setActiveListType: PropTypes.func.isRequired,
    resetActivityList: PropTypes.func.isRequired,
  };

  static defaultProps = {
    activeList: {
      data: {
        type: 'mirror-music',
      },
    },
  };

  state = {
    activeMode: '',
    isLogin: false,
  };

  componentDidMount() {
    __DEV__ && console.log('@MusicScreen');

    Spotify.initializeIfNeeded();
  }

  componentDidUpdate(prevProps) {
    const { activeList } = this.props;
    if (prevProps.spotifyIsLogin !== this.props.spotifyIsLogin) {
      if (this.props.spotifyIsLogin) {
        this.props.setActiveListType('spotify-music');
      } else if (isString(get(activeList, 'data.type'))) {
        if (activeList.data.type !== 'mirror-music') {
          this.props.resetActivityList();
        }
      }
    }
  }

  onPressChangeList = (type) => {
    const { spotifyIsLogin } = this.props;
    if (type === 'mirror-music') {
      this.props.onMirrorCommunicate(MirrorEvents.CHANGE_PLAYLIST, {
        type,
      });
    } else {
      if (!spotifyIsLogin) {
        Spotify.login();
      } else {
        this.props.setActiveListType('spotify-music');
      }
    }
  };

  render() {
    const { activeList, spotifyIsLogin } = this.props;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back navTitle={t('setting_music_nav_title')} />
        <Text style={styles.headerTitle}>{t('setting_music_title')}</Text>
        <View style={styles.wrapper}>
          <TouchableOpacity
            style={styles.itemBox}
            disabled
          >
            <View style={styles.sourceWrapper}>
              <Image
                source={Images.music}
                style={styles.musicImg}
                size={Screen.scale(25.6)}
              />
            </View>
            <Text style={styles.itemText}>{t('setting_music_mii_music')}</Text>
            <View style={styles.badgeBox}>
              {!isEmpty(activeList) &&
                !isEmpty(activeList.data) &&
                isString(activeList.data.type) &&
                activeList.data.type !== 'spotify-music' && (
                  <Badge status="success" badgeStyle={styles.badge} />
                )}
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.wrapper}>
          <SwipeRow
            rightOpenValue={-88}
            disableRightSwipe
            disableLeftSwipe={!spotifyIsLogin}
            swipeToClosePercent={0}
            closeOnRowPress={!spotifyIsLogin}
            preview={spotifyIsLogin}
          >
            <TouchableOpacity
              style={styles.standaloneRowBack}
              onPress={spotifyIsLogin ? Spotify.logout : Spotify.login}
            >
              <Text style={styles.backTextWhite}>
                {t(
                  spotifyIsLogin
                    ? 'setting_music_spotify_logout'
                    : 'setting_music_spotify_login',
                )}
              </Text>
            </TouchableOpacity>
            <View style={styles.standaloneRowFront}>
              <TouchableOpacity
                style={styles.itemBox}
                onPress={() => {
                  if (!spotifyIsLogin) Spotify.login();
                }}
                disabled={spotifyIsLogin}
              >
                <View style={styles.sourceWrapper}>
                  <Icon style={styles.spotify} name="spotify" size={Screen.scale(25.6)} />
                </View>
                <Text style={styles.itemText}>{t('audio_spotify_music')}</Text>
                <View style={styles.badgeBox}>
                  {!isEmpty(activeList) &&
                    !isEmpty(activeList.data) &&
                    isString(activeList.data.type) &&
                    activeList.data.type === 'spotify-music' && (
                      <Badge status="success" badgeStyle={styles.badge} />
                    )}
                </View>
              </TouchableOpacity>
            </View>
          </SwipeRow>
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    activeList: state.mirror.activeList,
    clientID: state.spotify.clientID,
    scopes: state.spotify.scopes,
    redirectUrl: state.spotify.redirectUrl,
    tokenSwapUrl: state.spotify.tokenSwapUrl,
    tokenRefreshUrl: state.spotify.tokenRefreshUrl,
    tokenRefreshEarliness: state.spotify.tokenRefreshEarliness,
    sessionUserDefaultsKey: state.spotify.sessionUserDefaultsKey,
    spotifyIsLogin: state.spotify.isLogin,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        setActiveListType: MirrorActions.setActiveListType,
        resetActivityList: MirrorActions.resetActivityList,
      },
      dispatch,
    ),
)(MusicScreen);
