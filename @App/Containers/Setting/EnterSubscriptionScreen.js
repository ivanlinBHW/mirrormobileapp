import * as Yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, Linking } from 'react-native';
import {
  VerticalTextInput,
  SecondaryNavbar,
  DismissKeyboard,
  SquareButton,
  LinkButton,
} from 'App/Components';
import { Config } from 'App/Config';
import { Dialog } from 'App/Helpers';
import { UserActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { Classes, Fonts, Colors } from 'App/Theme';

import styles from './SettingScreenStyle';

const yString = Yup.string();

const validationSchema = Yup.object().shape({
  subscriptionCode: yString
    .required(t('subscription_validation_code_required'))
    .min(5, t('subscription_validation_code_min_error', { value: 5 })),
});

class EnterSubscriptionScreen extends React.Component {
  static propTypes = {
    fetchPutSubscriptionCode: PropTypes.func.isRequired,
    updateUserStore: PropTypes.func.isRequired,
    mainSubscription: PropTypes.object,
    subscriptionMember: PropTypes.object,
    errorCode: PropTypes.string,
  };

  static defaultProps = {
    mainSubscription: null,
  };

  constructor(props) {
    super(props);
    let subscriptionCode = '';
    if (props.mainSubscription && props.mainSubscription.identity === 'Manager') {
      if (props.mainSubscription && props.mainSubscription.subscriptionCode) {
        subscriptionCode = props.mainSubscription.subscriptionCode;
      }
    } else {
      if (props.subscriptionMember && props.subscriptionMember.subscriptionCode) {
        subscriptionCode = props.subscriptionMember.subscriptionCode;
      }
    }
    console.log('props.mainSubscription=>', props.mainSubscription);
    console.log('props.subscriptionMember=>', props.subscriptionMember);
    this.state = {
      subscriptionCode,
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@EnterSubscriptionScreen');

    this.props.updateUserStore({ errorCode: '' });
  }

  renderForm = () => {
    const { fetchPutSubscriptionCode, errorCode } = this.props;
    return (
      <Formik
        initialValues={this.state}
        validationSchema={validationSchema}
        onSubmit={(values) => fetchPutSubscriptionCode(values.subscriptionCode)}
        validateOnChange
      >
        {({
          dirty,
          isValid,
          isSubmitting,
          isValidating,
          values,
          errors,
          touched,
          setFieldValue,
          handleSubmit,
          handleChange,
          setFieldTouched,
        }) => (
          <View style={Classes.fill}>
            <VerticalTextInput
              onChangeText={handleChange('subscriptionCode')}
              error={
                errors.subscriptionCode ||
                (errorCode && !isEmpty(errorCode) ? t(errorCode) : '')
              }
              value={values.subscriptionCode}
              disabled={isSubmitting || isValidating}
              autoCapitalize="none"
              border
            />
            <SquareButton
              text={t('submit')}
              style={[Classes.padding, Classes.marginTop]}
              onPress={() => {
                Dialog.showConfirmChangeSubscriptionCodeAlert(handleSubmit);
              }}
              throttleTime={0}
              disabled={!dirty}
              radius={6}
            />
          </View>
        )}
      </Formik>
    );
  };

  render() {
    return (
      <DismissKeyboard>
        <View style={Classes.fill}>
          <SecondaryNavbar back />
          <View style={[Classes.fill, Classes.padding]}>
            <View style={styles.subscriptionCodeContainer}>
              <View style={Classes.crossCenter}>
                <Text style={Fonts.style.h5_500}>
                  {t('setting_account_new_subscription_code')}
                </Text>
                <Text style={[Fonts.style.medium500, styles.txtGray]}>
                  {t('setting_account_enter_subscription_code')}
                </Text>
              </View>
              {this.renderForm()}
            </View>
          </View>

          <View
            style={[
              Classes.halfFill,
              Classes.mainEnd,
              Classes.crossCenter,
              Classes.marginBottom,
            ]}
          >
            <LinkButton
              text={`${t('setting_account_no_subscription_code')}`}
              textSize={Fonts.size.medium}
              textColor={Colors.button.primary.text.text}
              onPress={() => Linking.openURL(Config.SHOP_LINK)}
            />
          </View>
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({
    errorCode: state.user.errorCode,
    mainSubscription: state.user.mainSubscription,
    subscriptionMember: state.user.subscriptionMember,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        fetchPutSubscriptionCode: UserActions.fetchPutSubscriptionCode,
        updateUserStore: UserActions.updateUserStore,
      },
      dispatch,
    ),
)(EnterSubscriptionScreen);
