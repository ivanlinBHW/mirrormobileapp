import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { View, SafeAreaView, Text, Image } from 'react-native';

import { MirrorOptions, MirrorEvents } from 'App/Stores/Mirror/Actions';
import { SecondaryNavbar, BaseSwitch, BaseButton } from 'App/Components';
import { AppStateActions, MirrorActions } from 'App/Stores';
import { Classes, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';

import styles from './DisplayScreenStyle';

class DisplayScreen extends React.Component {
  static propTypes = {
    mirrorDisplayOptions: PropTypes.object,
    mirrorCommunicate: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
  };

  static defaultProps = {
    mirrorDisplayOptions: {
      [MirrorOptions.DISPLAY_STANDBY_MODE_DISPLAY]: true,
      [MirrorOptions.DISPLAY_TEMPERATURE]: true,
      [MirrorOptions.DISPLAY_AIR_QUALITY]: true,
      [MirrorOptions.DISPLAY_SLIDE_SHOW]: true,
      [MirrorOptions.DISPLAY_HUMIDITY]: true,
      [MirrorOptions.DISPLAY_TIME]: true,
    },
  };

  componentDidMount() {
    __DEV__ && console.log('@DisplayScreen');

    const { mirrorCommunicate } = this.props;
    mirrorCommunicate(MirrorEvents.GET_DEVICE_DISPLAY_SETTING);
  }

  handleSwitchValueChange = (key) => () => {
    const { mirrorCommunicate, mirrorDisplayOptions } = this.props;

    mirrorCommunicate(MirrorEvents.UPDATE_DEVICE_DISPLAY_SETTING, {
      optionName: key,
      value: !mirrorDisplayOptions[key],
    });

    mirrorCommunicate(MirrorEvents.GET_DEVICE_DISPLAY_SETTING);
  };

  render() {
    const { mirrorDisplayOptions } = this.props;
    return (
      <SafeAreaView style={[Classes.fill, Classes.white_02_background]}>
        <SecondaryNavbar back navTitle={t('setting_mii_display__nav_title')} />
        <View style={styles.itemBox}>
          <Text style={styles.itemText}>
            {t('setting_mii_display_standby_mode_display')}
          </Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={mirrorDisplayOptions[MirrorOptions.DISPLAY_STANDBY_MODE_DISPLAY]}
            onValueChange={this.handleSwitchValueChange(
              MirrorOptions.DISPLAY_STANDBY_MODE_DISPLAY,
            )}
          />
        </View>

        <Text style={styles.contentText}>{t('setting_mii_display_content')}</Text>
        <View style={styles.itemBox}>
          <Text style={styles.itemText}>{t('setting_mii_display_time')}</Text>
          <BaseSwitch
            style={styles.itemSwitch}
            active={mirrorDisplayOptions[MirrorOptions.DISPLAY_TIME]}
            onValueChange={this.handleSwitchValueChange(MirrorOptions.DISPLAY_TIME)}
            disabled={!mirrorDisplayOptions[MirrorOptions.DISPLAY_STANDBY_MODE_DISPLAY]}
          />
        </View>

        <BaseButton
          style={styles.itemBox}
          onPress={Actions.WeatherScreen}
          disabled={!mirrorDisplayOptions[MirrorOptions.DISPLAY_STANDBY_MODE_DISPLAY]}
          transparent
        >
          <Text style={styles.itemText}>{t('setting_mii_display_weather')}</Text>
          <Image resizeMode="contain" source={Images.next} style={styles.itemImg} />
        </BaseButton>

        <BaseButton
          style={styles.itemBox}
          onPress={Actions.SlideshowScreen}
          disabled={!mirrorDisplayOptions[MirrorOptions.DISPLAY_STANDBY_MODE_DISPLAY]}
          transparent
        >
          <Text style={styles.itemText}>{t('setting_mii_display_slideshow')}</Text>
          <Image resizeMode="contain" source={Images.next} style={styles.itemImg} />
        </BaseButton>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    mirrorDisplayOptions: state.mirror.displayOptions,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
        mirrorCommunicate: MirrorActions.onMirrorCommunicate,
      },
      dispatch,
    ),
)(DisplayScreen);
