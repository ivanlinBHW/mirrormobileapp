import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { BaseAvatar, SecondaryNavbar } from 'App/Components';
import { Classes, Images, Fonts, Metrics } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { UserActions } from 'App/Stores';

import styles from './SettingScreenStyle';

class AccountSettingScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object,
    getUserAccount: PropTypes.func.isRequired,
    routeName: PropTypes.string.isRequired,
    sceneKey: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      engineeringModeCount: 1,
      menu: [
        {
          title: 'setting_account_item_account_info',
          key: 'AccountInfoScreen',
        },
        {
          title:
            !isEmpty(props.user.mainSubscription) &&
            props.user.mainSubscription.identity === 'Manager'
              ? 'setting_account_info_account_manager'
              : 'setting_account_item_family_member',
          key: 'FamilyMemberScreen',
        },
      ],
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@AccountSettingScreen');
    const { getUserAccount } = this.props;
    getUserAccount();
  }

  componentDidUpdate(prevProps) {
    const { routeName, sceneKey, getUserAccount } = this.props;
    if (prevProps.routeName !== routeName && routeName === sceneKey) {
      getUserAccount();
    }
  }

  renderHeader = () => {
    const {
      user: { signedAvatarUrl, updatedAt, fullName, mainSubscription = {} },
      isLoading,
    } = this.props;
    return (
      <View style={styles.headerWrapper}>
        <View style={styles.leftBox}>
          <BaseAvatar
            size={56}
            uri={signedAvatarUrl}
            image={Images.defaultAvatar}
            cache={false}
            active
          />
        </View>
        <View style={[styles.rightBox, Classes.mainCenter]}>
          <View style={[Classes.rowCross, Classes.marginBottom]}>
            <Text style={Fonts.style.regular500}>{fullName} </Text>
            <ActivityIndicator animating={isLoading} size="small" color="black" />
          </View>

          <Text
            style={[
              Fonts.style.small500,
              styles.txtGray,
              { marginTop: -Metrics.baseMargin / 2 },
            ]}
          >
            {!isEmpty(mainSubscription)
              ? `${
                  mainSubscription.identity === 'Manager'
                    ? t('setting_account_info_account_manager')
                    : t('setting_account_info_account_member')
                }`
              : ''}
          </Text>
        </View>
      </View>
    );
  };

  renderItem = ({ item, index }) => {
    const {
      user: { mainSubscription },
    } = this.props;
    const isDisabled = item.key !== 'AccountInfoScreen' && !mainSubscription;
    return (
      <View style={styles.wrapper}>
        <TouchableOpacity
          style={styles.itemStyle}
          onPress={Actions[item.key]}
          disabled={isDisabled}
        >
          <Text style={[styles.itemText, isDisabled && styles.txtGray]}>
            {t(`${item.title}`)}
          </Text>
          <Image source={Images.next} style={styles.itemImg} />
        </TouchableOpacity>
        {index === 5 && <View style={styles.emptyBox} />}
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back navTitle={t('setting_account_title')} />
        <View style={styles.bgWhite02}>
          {this.renderHeader()}
          <FlatList
            data={this.state.menu}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => `setting_${index}`}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, props) => ({
    isLoading: state.appState.isLoading,
    routeName: state.appRoute.routeName,
    sceneKey: props.name,
    user: state.user,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getUserAccount: UserActions.getUserAccount,
      },
      dispatch,
    ),
)(AccountSettingScreen);
