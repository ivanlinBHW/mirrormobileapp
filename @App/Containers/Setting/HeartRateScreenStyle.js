import { StyleSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default StyleSheet.create({
  flatBox: {
    flexDirection: 'row',
    paddingTop: '8@vs',
    paddingBottom: '8@vs',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
  },
  img: {
    width: '32@s',
    height: '32@vs',
    marginLeft: Metrics.baseMargin,
    marginRight: '40@s',
  },
  title: {
    fontSize: Fonts.size.medium500,
    lineHeight: '30@vs',
  },
  badge: {
    width: '12@s',
    height: '12@vs',
    borderRadius: '12@vs',
  },
  badgeBox: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    lineHeight: '30@vs',
    height: '30@vs',
    width: '30@s',
    marginRight: '17@vs',
  },
  header: {
    fontSize: Fonts.size.medium,
    marginLeft: Metrics.baseMargin,
    marginVertical: Metrics.baseMargin,
    justifyContent: 'flex-start',
  },
  iconbtn: {
    width: '20@vs',
    height: '20@s',
    lineHeight: '20@vs',
    marginRight: '13@vs',
    marginTop: '6@vs',
    alignSelf: 'center',
  },
  headerBox: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: '13@s',
    marginTop: Metrics.baseMargin,
  },
  button: {
    width: '20@s',
    height: '20@vs',
  },
  imgStyle: {
    width: '20@s',
    height: '20@vs',
    marginTop: '6@vs',
  },
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  headerContent: {
    fontSize: Fonts.size.medium,
    margin: Metrics.baseMargin,
  },
  emptyBox: {
    width: '100%',
    height: 1,
    backgroundColor: Colors.black_15,
  },
});
