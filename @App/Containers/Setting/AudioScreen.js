import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, Platform } from 'react-native';
import { bindActionCreators } from 'redux';

import { Classes } from 'App/Theme';
import { Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import MirrorActions, { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import {
  SecondaryNavbar,
  BaseIconButton,
  BluetoothAudioDevicePanel,
} from 'App/Components';

class AudioScreen extends React.Component {
  static propTypes = {
    isScanning: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    onMirrorChangeAudioOutputDevice: PropTypes.func.isRequired,
    outputOtherDevices: PropTypes.array,
    outputPairedDevices: PropTypes.array,
    outputDeviceType: PropTypes.string,
    targetOutputAudioDevice: PropTypes.object,
    activeBtAudioDevice: PropTypes.object,
  };

  static defaultProps = {
    targetOutputAudioDevice: {},
    outputPairedDevices: [],
    outputOtherDevices: [],
    activeBtAudioDevice: null,
    outputDeviceType: '',
  };

  componentDidMount() {
    __DEV__ && console.log('@AudioScreen');
  }

  onRefreshBluetoothDevice = () => {
    this.props.onMirrorCommunicate(MirrorEvents.REQUEST_BLUETOOTH_AUDIO_DEVICES);
  };

  render() {
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          back
          navTitle={t('setting_audio_output')}
          navRightComponent={
            <BaseIconButton
              iconSize={Platform.isPad ? Screen.scale(8) : Screen.scale(24)}
              iconType="FontAwesome5"
              iconName="redo"
              onPress={this.onRefreshBluetoothDevice}
              throttleTime={0}
            />
          }
        />
        <BluetoothAudioDevicePanel {...this.props} />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    isLoading: state.appState.isLoading,
    outputPairedDevices: state.mirror.outputPairedDevices,
    outputOtherDevices: state.mirror.outputOtherDevices,
    outputDeviceType: state.mirror.options[MirrorOptions.OUTPUT_AUDIO_DEVICE],
    outputDeviceData: state.mirror.options[MirrorOptions.OUTPUT_AUDIO_DEVICE_DATA],
    activeDevice: state.mirror.activeBtAudioDevice,
    isScanning: state.bleDevice.isScanning,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        onMirrorChangeAudioOutputDevice: MirrorActions.onMirrorChangeAudioOutputDevice,
      },
      dispatch,
    ),
)(AudioScreen);
