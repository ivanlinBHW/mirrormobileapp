import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text } from 'react-native';
import { bindActionCreators } from 'redux';

import { SecondaryNavbar, BleDevicePanel } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Classes } from 'App/Theme';

import styles from './HeartRateScreenStyle';

class HeartRateScreen extends React.Component {
  static propTypes = {
    routeName: PropTypes.string.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@HeartRateScreen');
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { shouldScanning } = prevState;
    const { routeName } = nextProps;
    if (!shouldScanning || (routeName === 'HeartRateScreen' && !shouldScanning)) {
      return {
        shouldScanning: true,
      };
    }
    if (routeName !== 'HeartRateScreen' && shouldScanning) {
      return {
        shouldScanning: false,
      };
    }
    return null;
  }

  state = {
    shouldScanning: true,
  };

  render() {
    const { shouldScanning } = this.state;
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar back navTitle={t('setting_heart_rate_monitor')} />
        <Text style={styles.headerTitle}>{t('setting_heart_rate_title')}</Text>
        <Text style={styles.headerContent}>{t('setting_heart_rate_content')}</Text>
        <View style={styles.emptyBox} />
        <BleDevicePanel scanning={shouldScanning} {...this.props} />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    routeName: state.appRoute.routeName,
  }),
  (dispatch) => bindActionCreators({}, dispatch),
)(HeartRateScreen);
