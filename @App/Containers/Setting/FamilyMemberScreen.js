import React from 'react';
import PropTypes from 'prop-types';
import Share from 'react-native-share';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isEmpty, isArray, get } from 'lodash';
import { Actions } from 'react-native-router-flux';
import { View, Text, Image, FlatList, SafeAreaView } from 'react-native';

import {
  BaseAvatar,
  BaseButton,
  ImageButton,
  SquareButton,
  BaseIconButton,
  SecondaryNavbar,
  FullWidthBarButton,
} from 'App/Components';
import { Classes, Images, Fonts, Metrics, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Screen, Dialog } from 'App/Helpers';
import { UserActions } from 'App/Stores';

import styles from './SettingScreenStyle';

class FamilyMemberScreen extends React.Component {
  static propTypes = {
    fetchAddMemberSubscriptionCode: PropTypes.func.isRequired,
    fetchDelMemberSubscriptionCode: PropTypes.func.isRequired,
    user: PropTypes.object,
    mainSubscription: PropTypes.object,
  };

  static defaultProps = {
    user: {},
    mainSubscription: {},
  };

  state = {
    isEditing: false,
    memberAccountLimited: true,
  };

  componentDidMount() {
    __DEV__ && console.log('@FamilyMemberScreen');

    if (this.props.user != null && this.props.user.email == 'admin@example.com') {
      this.setState({ memberAccountLimited: false });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { isEditing } = this.state;
    if (prevState.isEditing !== isEditing) {
      Actions.refresh({
        panHandlers: isEditing ? null : undefined,
      });
    }
  }

  onPressShare = (subscriptionCode) => () => {
    const shareOptions = {
      title: t('setting_account_family_share_with_friend'),
      message: subscriptionCode,
      subject: t('setting_account_family_share_with_friend'),
    };
    Share.open(shareOptions);
  };

  renderHeader = () => {
    const { mainSubscription, user: { signedAvatarUrl, fullName } = {} } = this.props;
    return (
      <View style={styles.headerWrapper}>
        <View style={[styles.leftBox, Classes.mainCenter]}>
          <BaseAvatar
            size={56}
            uri={signedAvatarUrl}
            image={Images.defaultAvatar}
            cache={false}
          />
        </View>

        <View style={[styles.rightBox, Classes.mainCenter]}>
          <Text style={Fonts.style.regular500}>{fullName}</Text>

          <Text
            style={[
              Fonts.style.small500,
              styles.txtGray,
              { marginTop: Metrics.baseMargin / 2 },
            ]}
          >
            {mainSubscription
              ? `${
                  get(mainSubscription, 'identity') === 'Manager'
                    ? t('setting_account_info_account_manager')
                    : t('setting_account_info_account_member')
                }`
              : ''}
          </Text>
        </View>
      </View>
    );
  };

  renderItem = ({ item: { member, subscriptionCode, id } = {}, index }) => {
    const {
      user: { mainSubscription: { members = [] } = {} },
    } = this.props;
    const { isEditing } = this.state;
    return (
      <View
        style={[
          Classes.fill,
          Classes.row,
          Classes.mainEnd,
          Classes.crossCenter,
          styles.bgWhite,
          index < members.length - 1 && {
            borderBottomColor: Colors.gray_02,
            borderBottomWidth: 1 / 2,
          },
        ]}
      >
        <View style={Classes.fill}>
          <View style={[Classes.row, Classes.paddingLeft, Classes.paddingRight]}>
            {isEditing && (
              <View style={[Classes.mainCenter, Classes.paddingRight]}>
                <BaseIconButton
                  iconType="FontAwesome5"
                  iconName="minus-circle"
                  iconColor={Colors.error}
                  iconSize={Screen.scale(25.6)}
                  onPress={Dialog.delSubscriptionCodeWithMemberAlert(id)}
                />
              </View>
            )}
            <View style={[Classes.fill, Classes.crossStart, Classes.mainCenter]}>
              <Text style={Classes.mainEnd}>{subscriptionCode}</Text>
            </View>

            <View style={[Classes.fill, Classes.center]}>
              <Text style={styles.itemText}>
                {member ? member.name || member.email : '---'}
              </Text>
            </View>

            {!isEditing && (
              <View style={Classes.halfFill}>
                <ImageButton
                  style={Classes.crossEnd}
                  image={Images.share_dark}
                  imageWidth={32}
                  imageHeight={32}
                  onPress={this.onPressShare(subscriptionCode)}
                />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  };

  render() {
    const { mainSubscription, fetchAddMemberSubscriptionCode } = this.props;
    const { isEditing, memberAccountLimited } = this.state;

    let disabledAddMember = true;
    if (memberAccountLimited == false) {
      disabledAddMember = false;
    } else if (
      mainSubscription == null ||
      (isArray(mainSubscription.members) && mainSubscription.members.length < 5)
    ) {
      disabledAddMember = false;
    }

    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar
          back={!isEditing}
          navTitle={t('setting_account_item_family_member')}
          navRightComponent={
            !isEmpty(mainSubscription) && mainSubscription.identity === 'Manager' ? (
              <BaseButton
                transparent
                style={[Classes.row, Classes.mainSpaceBetween, Classes.mainEnd]}
                text={isEditing ? t('__done') : t('edit')}
                textColor={Colors.button.primary.text.text}
                throttleTime={0}
                onPress={() => {
                  this.setState((state) => ({
                    isEditing: !state.isEditing,
                  }));
                }}
              />
            ) : null
          }
        />
        <View style={[styles.bgWhite02, styles.listWrapper]}>
          {this.renderHeader()}

          {!isEmpty(mainSubscription) && mainSubscription.identity === 'Manager' && (
            <>
              <FullWidthBarButton
                onPress={fetchAddMemberSubscriptionCode}
                text={t('setting_account_family_create_family_code')}
                backgroundColor={Colors.black}
                disabled={disabledAddMember}
              />
              <View style={Classes.padding}>
                <Text style={[Fonts.style.small, styles.txtGray]}>
                  {t('setting_account_invite_member', { count: 5 })}
                </Text>
              </View>
              <FlatList
                data={mainSubscription.members}
                renderItem={this.renderItem}
                keyExtractor={(item) => `${item.id}`}
              />
            </>
          )}
        </View>
        {!isEmpty(mainSubscription) && mainSubscription.identity === 'Member' && (
          <View
            style={[
              Classes.padding,
              Classes.rowCross,
              {
                borderTop: 1,
                borderBottom: 1,
                borderTopWidth: 0.5,
                borderBottomWidth: 0.5,
                borderColor: Colors.gray_02,
              },
            ]}
          >
            <Image source={Images.members} style={Classes.marginRight} />
            <Text style={Fonts.style.medium}>{mainSubscription.activeUser.fullName}</Text>
          </View>
        )}
        {!isEmpty(mainSubscription) && mainSubscription.identity === 'Member' && (
          <SquareButton
            text={t('setting_account_info_quit_member')}
            onPress={Dialog.exitFamilyAccountAlert}
            color={Colors.black}
            textColor={Colors.white}
            style={{ position: 'absolute', bottom: 0 }}
          />
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    user: state.user,
    mainSubscription: state.user.mainSubscription,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        fetchAddMemberSubscriptionCode: UserActions.fetchAddMemberSubscriptionCode,
        fetchDelMemberSubscriptionCode: UserActions.fetchDelMemberSubscriptionCode,
      },
      dispatch,
    ),
)(FamilyMemberScreen);
