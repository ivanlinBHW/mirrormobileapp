// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Text, View, FlatList, Modal } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

import Actions from 'App/Stores/BleDevice/Actions';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { ConnectionState } from 'App/Stores/BleDevice/InitialState';
import { SensorTagTests } from 'App/Sagas/BleDevice/BleDeviceTestSaga';
import styles from './MdnsDebugScreenStyle';
import * as DebugMode from 'App/Components/DebugMode';

const HEAR_RATE_SERVICE_GUID = '180D';
const HEART_RATE_MEASUREMENT_CHARACTERISTIC_GUID = '2A37';

class BleDeviceDebugScreen extends Component {
  static propTypes = {
    connect: PropTypes.func.isRequired,
    onBleMonitorCharacteristic: PropTypes.func.isRequired,
    onBleWriteCharacteristic: PropTypes.func.isRequired,
    disconnect: PropTypes.func.isRequired,
    forgetDevice: PropTypes.func.isRequired,
    clearLogs: PropTypes.func.isRequired,
    executeTest: PropTypes.func.isRequired,
    scan: PropTypes.func.isRequired,
    stopScan: PropTypes.func.isRequired,

    connectionState: PropTypes.string.isRequired,
    isScanning: PropTypes.bool.isRequired,
    devices: PropTypes.array.isRequired,
    currentTest: PropTypes.string,
    logs: PropTypes.array,
    wsIsConnected: PropTypes.bool,
    onWsSendMessage: PropTypes.func.isRequired,
  };

  static defaultProps = {
    logs: [],
    currentTest: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      showTestModal: false,
      showMonitorModal: false,
      activeDevice: null,
    };
  }

  componentDidMount() {
  }

  sensorTagStatus() {
    const { connectionState, isScanning } = this.props;
    switch (connectionState) {
      case ConnectionState.CONNECTING:
        if (this.state.activeDevice) {
          const {
            activeDevice: { id, name },
          } = this.state;
          return `Connecting to ${id}(${name})...`;
        }
        return 'Connecting...';
      case ConnectionState.DISCOVERING:
        return 'Discovering...';
      case ConnectionState.CONNECTED:
        if (this.state.activeDevice) {
          const {
            activeDevice: { id, name },
          } = this.state;
          return `Connected ${id}(${name}).`;
        }
        return 'Connected';
      case ConnectionState.DISCONNECTED:
      case ConnectionState.DISCONNECTING:
        if (this.state.activeDevice) {
          const {
            activeDevice: { id },
          } = this.state;
          return 'Select ' + id;
        }
    }

    return isScanning ? 'Searching...' : 'wait for command';
  }

  isSensorTagReadyToConnect() {
    return (
      this.state.activeDevice != null &&
      this.props.connectionState === ConnectionState.DISCONNECTED
    );
  }

  isSensorTagReadyToDisconnect() {
    return this.props.connectionState === ConnectionState.CONNECTED;
  }

  isSensorTagReadyToExecuteTests() {
    return (
      this.props.connectionState === ConnectionState.CONNECTED &&
      this.props.currentTest == null
    );
  }

  renderHeader() {
    return (
      <View>
        <DebugMode.Title numberOfLines={1}>
          Device: {this.sensorTagStatus()}
        </DebugMode.Title>
        <DebugMode.ButtonContainer>
          <DebugMode.Button
            onPress={() => {
              this.props.isScanning ? this.props.stopScan() : this.props.scan();
            }}
            text={this.props.isScanning ? 'Stop...' : 'Scan'}
          />
          <DebugMode.Button
            disabled={!this.isSensorTagReadyToConnect()}
            onPress={() => {
              if (this.state.activeDevice != null) {
                this.props.stopScan();
                this.props.connect(this.state.activeDevice);
              }
            }}
            text={'Connect'}
          />
          <DebugMode.Button
            disabled={!this.isSensorTagReadyToDisconnect()}
            onPress={() => {
              this.props.stopScan();
              this.props.disconnect();
            }}
            text={'Disconnect'}
          />
        </DebugMode.ButtonContainer>
        <DebugMode.ButtonContainer>
          <DebugMode.Button
            disabled={!this.isSensorTagReadyToExecuteTests()}
            onPress={() => {

              this.props.onBleMonitorCharacteristic(
                HEAR_RATE_SERVICE_GUID,
                HEART_RATE_MEASUREMENT_CHARACTERISTIC_GUID,
              );
            }}
            text={'Monitor'}
          />
          <DebugMode.Button
            disabled={!this.isSensorTagReadyToExecuteTests()}
            onPress={() => {
              this.setState({ showTestModal: true });
            }}
            text={'Execute test'}
          />
          <DebugMode.Button
            disabled={this.props.devices.length === 0}
            onPress={() => {
              this.props.forgetDevice();
              this.setState({ activeDevice: null });
            }}
            text={'Forget'}
          />
        </DebugMode.ButtonContainer>
        <DebugMode.ButtonContainer>
          <Text>Command: </Text>
          <RNPickerSelect
            placeholder={{
              label: 'Click and send the selected command',
              value: null,
            }}
            style={{
              inputIOS: styles.inputIOS,
              inputAndroid: styles.inputAndroid,
            }}
            useNativeAndroidPickerStyle={false}
            disabled={!this.props.wsIsConnected}
            onValueChange={(value) => {
              if (value) {
                this.setState({ isDebugEnable: true });
                this.props.onWsSendMessage({
                  event: value,
                });
              }
            }}
            items={Object.keys(MirrorEvents)
              .filter((e) => !e.includes('SUCCESS'))
              .map((e) => ({ label: e, value: MirrorEvents[e] }))}
          />
        </DebugMode.ButtonContainer>
      </View>
    );
  }

  renderDevices() {
    return (
      <DebugMode.LogList
        data={this.props.devices}
        renderItem={({ item }) => (
          <DebugMode.Button
            onPress={() => {
              this.setState({ activeDevice: item });
            }}
            text={item.name ? `${item.id}(${item.name})` : item.id}
          />
        )}
      />
    );
  }

  renderLogs() {
    return (
      <View style={styles.fill}>
        <DebugMode.LogList data={this.props.logs} />
        <DebugMode.Button onPress={this.props.clearLogs} text={'Clear logs'} />
      </View>
    );
  }

  renderTestModal() {
    const tests = Object.values(SensorTagTests);

    const modalTextStyle = [styles.textStyle, { paddingBottom: 10, alignSelf: 'center' }];
    const btnItemStyle = { paddingBottom: 5 };
    const btnCancelStyle = { paddingTop: 5 };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showTestModal}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalHeader}>
            <Text style={modalTextStyle}>Select test to execute:</Text>
            <FlatList
              data={tests}
              renderItem={({ item }) => (
                <DebugMode.Button
                  style={btnItemStyle}
                  disabled={!this.isSensorTagReadyToExecuteTests()}
                  onPress={() => {
                    this.props.executeTest(item.id);
                    this.setState({ showTestModal: false });
                  }}
                  text={item.title}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
            />
            <DebugMode.Button
              style={btnCancelStyle}
              onPress={() => {
                this.setState({ showTestModal: false });
              }}
              text={'Cancel'}
            />
          </View>
        </View>
      </Modal>
    );
  }

  renderMonitorModal() {
    const tests = Object.values(SensorTagTests);
    const modalTextStyle = [styles.textStyle, { paddingBottom: 10, alignSelf: 'center' }];
    const btnItemStyle = { paddingBottom: 5 };
    const btnCancelStyle = { paddingTop: 5 };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showMonitorModal}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalHeader}>
            <Text style={modalTextStyle}>Select test to execute:</Text>
            <FlatList
              data={tests}
              renderItem={({ item }) => (
                <DebugMode.Button
                  style={btnItemStyle}
                  disabled={!this.isSensorTagReadyToExecuteTests()}
                  onPress={() => {
                    this.props.executeTest(item.id);
                    this.setState({ showMonitorModal: false });
                  }}
                  text={item.title}
                />
              )}
              keyExtractor={(item, index) => index.toString()}
            />
            <DebugMode.Button
              style={btnCancelStyle}
              onPress={() => {
                this.setState({ showMonitorModal: false });
              }}
              text={'Cancel'}
            />
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    return (
      <DebugMode.Container>
        {this.renderHeader()}
        {this.renderDevices()}
        {this.renderLogs()}
        {this.renderTestModal()}
        {this.renderMonitorModal()}
      </DebugMode.Container>
    );
  }
}

export default connect(
  (state) => ({
    isScanning: state.bleDevice.isScanning,
    devices: state.bleDevice.searchDevices,
    logs: state.bleDevice.logs,
    connectionState: state.bleDevice.connectionState,
    currentTest: state.bleDevice.currentTest,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onBleMonitorCharacteristic: Actions.onBleMonitorCharacteristic,
        onBleWriteCharacteristic: Actions.onBleWriteCharacteristic,
        clearLogs: Actions.onClearLogs,
        connect: Actions.onConnect,
        scan: Actions.onScan,
        stopScan: Actions.onStopScan,
        disconnect: Actions.onDisconnect,
        forgetDevice: Actions.onForgetDevices,
        executeTest: Actions.onExecuteTest,
      },
      dispatch,
    ),
)(BleDeviceDebugScreen);
