// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Platform,
  Text,
  SafeAreaView,
  View,
  FlatList,
  Alert,
  TouchableOpacity,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import WifiManager from 'react-native-wifi-reborn';
import wifi from 'react-native-android-wifi-with-ap-status';
import Ping from 'react-native-ping';

import { SimpleInputDialog } from 'App/Components';
import { Permission } from 'App/Helpers';
import { Config } from 'App/Config';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { ConnectionState } from 'App/Stores/Websocket/Actions';
import { MdnsActions, WebsocketActions as WsActions, MirrorActions } from 'App/Stores';
import styles from './MdnsDebugScreenStyle';

const Button = (props) => {
  const { onPress, title, ...restProps } = props;
  return (
    <TouchableOpacity onPress={onPress} {...restProps}>
      <Text
        style={[
          styles.buttonStyle,
          restProps.disabled ? styles.disabledButtonStyle : null,
        ]}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

class MdnsDebugScreen extends Component {
  static propTypes = {
    mdLogs: PropTypes.array.isRequired,
    mdIsScanning: PropTypes.bool.isRequired,
    mdServices: PropTypes.array.isRequired,
    mdError: PropTypes.any,
    wsLogs: PropTypes.array.isRequired,
    wsError: PropTypes.any,
    wsMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    wsIsConnected: PropTypes.bool.isRequired,
    wsConnectState: PropTypes.string.isRequired,
    closeConnection: PropTypes.func.isRequired,
    onMdnsInit: PropTypes.func.isRequired,
    onMdnsScan: PropTypes.func.isRequired,
    onMdnsScanClear: PropTypes.func.isRequired,
    onMdnsLogClear: PropTypes.func.isRequired,
    onWsLogClear: PropTypes.func.isRequired,
    onMdnsStop: PropTypes.func.isRequired,
    onWsConnect: PropTypes.func.isRequired,
    onWsDisconnect: PropTypes.func.isRequired,
    onWsSendMessage: PropTypes.func.isRequired,
    onWsMessageReceived: PropTypes.func.isRequired,
    onMirrorConnected: PropTypes.func.isRequired,
    onMirrorConnect: PropTypes.func.isRequired,
    onWsLog: PropTypes.func.isRequired,
  };

  static defaultProps = {
    logs: [],
    currentTest: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      activeDevice: null,
      isDebugEnable: false,
      isWifiPasswordPrompt: false,
      isWss: Config.WEBSOCKET_MODE_PREFIX === 'wss',
    };
  }

  componentDidMount() {
    __DEV__ && console.log('MdnsDebugScreen componentDidMount');
    if (Platform.OS === 'android') {
      wifi.isEnabled((isEnabled) => {
        wifi.forceWifiUsage(true);
      });
    }

    const { onMdnsInit } = this.props;
    onMdnsInit();
  }

  componentDidUpdate(prevProps) {
    const { onMirrorConnected, wsIsConnected } = this.props;
    const { activeDevice } = this.state;
    if (wsIsConnected && wsIsConnected !== prevProps.wsIsConnected && activeDevice) {
      onMirrorConnected(activeDevice);
    }
  }

  componentWillUnmount() {
    __DEV__ && console.log('MdnsDebugScreen componentWillUnmount');
    if (this.state.isDebugEnable) {
      this.props.onMdnsStop();
    }
    if (Platform.OS === 'android') {
      wifi.forceWifiUsage(false);
    }
  }

  handleClearLogs = () => {
    this.props.onMdnsStop();
    this.props.onMdnsLogClear();
    this.props.onWsLogClear();
  };

  renderHeader() {
    const { onWsLog } = this.props;
    const { isWss } = this.state;
    const getConnectTitle = () => {
      switch (this.props.wsConnectState) {
        case ConnectionState.CONNECTED:
          return 'Connect✔︎';
        case ConnectionState.CONNECTING:
          return 'Connecting...';
        default:
          return 'Connect';
      }
    };
    return (
      <View>
        <Text style={styles.textStyle} numberOfLines={1}>
          Device:{' '}
          {this.state.activeDevice ? this.state.activeDevice.name : 'Not selected'}
        </Text>
        <View style={styles.row}>
          <Button
            disabled={this.props.wsIsConnected}
            style={styles.button}
            onPress={() => {
              this.setState({ isWss: !isWss });
            }}
            title={isWss ? 'WSS' : 'WS'}
          />
          <Button
            style={styles.button}
            onPress={() => {
              this.props.mdIsScanning ? this.props.onMdnsStop() : this.props.onMdnsScan();
            }}
            title={this.props.mdIsScanning ? 'Stop Scanning' : 'Scan'}
          />
          <Button
            style={styles.button}
            disabled={this.props.mdServices.length === 0}
            onPress={() => {
              this.props.onMdnsScanClear();
              this.setState({ activeDevice: null, isDebugEnable: true });
            }}
            title={'Clear'}
          />
          <Button
            style={styles.button}
            onPress={async () => {
              await Permission.requestLocationPermission();
              WifiManager.connectToProtectedSSID('', '', false).then(
                (res) => {
                  Alert.alert('connect', res);
                },
                (e) => {
                  Alert.alert(e.message, JSON.stringify(e));
                },
              );
            }}
            title={'CONNECT WIFI'}
          />
        </View>
        <View style={styles.row}>
          <Button
            disabled={!this.state.activeDevice || this.props.wsIsConnected}
            style={styles.button}
            onPress={() => {
              if (this.state.activeDevice != null) {
                this.props.onMdnsStop();
                if (isWss) {
                  const { host, port } = this.state.activeDevice;
                  this.props.onWsConnect(`wss://${host}:${port}`);
                } else {
                  this.props.onMirrorConnect(this.state.activeDevice);
                }
                this.setState({ isDebugEnable: true });
              }
            }}
            title={getConnectTitle()}
          />
          <Button
            disabled={!this.state.activeDevice || !this.props.wsIsConnected}
            style={styles.button}
            onPress={() => {
              this.props.onMdnsStop();
              this.props.closeConnection();
              this.setState({ isDebugEnable: true });
            }}
            title={'Disconnect'}
          />
          <Button
            disabled={!this.state.activeDevice}
            style={styles.button}
            onPress={async () => {
              try {
                const ms = await Ping.start(this.state.activeDevice.host, {
                  timeout: 1000,
                });
                Alert.alert('PING', `${ms}`);
              } catch (e) {
                Alert.alert('PING Error', `${e.message}(${e.code})`);
              }
            }}
            title={'PING'}
          />
        </View>
        <View style={styles.row}>
          <Text>Command: </Text>
          <RNPickerSelect
            placeholder={{
              label: 'Click and send the selected command',
              value: null,
            }}
            style={{
              inputIOS: styles.inputIOS,
              inputAndroid: styles.inputAndroid,
            }}
            useNativeAndroidPickerStyle={false}
            disabled={!this.props.wsIsConnected}
            onValueChange={(value) => {
              if (value && value !== MirrorEvents.SET_WIFI_PASS) {
                this.setState({ isDebugEnable: true });
                this.props.onWsSendMessage({
                  event: value,
                });
              } else {
                this.setState({ isWifiPasswordPrompt: true });
              }
            }}
            items={Object.keys(MirrorEvents)
              .filter((e) => !e.includes('SUCCESS'))
              .map((e) => ({ label: e, value: MirrorEvents[e] }))}
          />
        </View>
      </View>
    );
  }

  renderWifiPasswordDialog = () => {
    const { isWifiPasswordPrompt, ssid, password } = this.state;
    const handleOnCancel = () => this.setState({ isWifiPasswordPrompt: false });
    return (
      <SimpleInputDialog
        visible={isWifiPasswordPrompt}
        title="Enter SSID and its passwod"
        description=""
        inputFields={[
          {
            placeholder: 'SSID',
            onChangeText: (v) => this.setState({ ssid: v }),
          },
          {
            placeholder: 'Password',
            onChangeText: (v) => this.setState({ password: v }),
          },
        ]}
        onConfirmPress={() => {
          this.props.onWsSendMessage({
            event: MirrorEvents.SET_WIFI_PASS,
            data: { SSID: ssid, PW: password },
          });
          handleOnCancel();
        }}
        onCancelPress={handleOnCancel}
        isConfirmDisabled={!password}
      />
    );
  };

  renderDevices() {
    return (
      <View style={styles.fill}>
        <FlatList
          style={styles.border}
          data={this.props.mdServices}
          renderItem={({ item }) => (
            <Button
              onPress={() => {
                this.setState({ activeDevice: item });
              }}
              title={`${item.name}(${item.host})`}
            />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }

  renderWsLogs() {
    return (
      <View style={styles.fill}>
        <FlatList
          style={styles.border}
          data={this.props.wsLogs}
          renderItem={({ item }) => <Text style={styles.logTextStyle}> {item} </Text>}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }

  renderMdLogs() {
    return (
      <View style={styles.fill}>
        <FlatList
          style={styles.border}
          data={this.props.mdLogs}
          renderItem={({ item }) => <Text style={styles.logTextStyle}> {item} </Text>}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }

  render() {
    const btnStyle = { paddingTop: 4 };
    return (
      <SafeAreaView style={styles.container}>
        {this.renderHeader()}
        <Button style={btnStyle} onPress={this.handleClearLogs} title={'Clear logs'} />
        {this.renderDevices()}
        {this.renderMdLogs()}
        {this.renderWsLogs()}
        {this.renderWifiPasswordDialog()}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    mdLogs: state.mdns.logs,
    mdIsScanning: state.mdns.isScanning,
    mdServices: state.mdns.services,
    mdError: state.mdns.error,
    wsLogs: state.websocket.logs,
    wsConnectState: state.websocket.connectionState,
    wsError: state.websocket.error,
    wsIsConnected: state.websocket.isConnected,
    wsMessage: state.websocket.lastReceivedMessage,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorConnect: MirrorActions.onMirrorConnect,
        onMirrorConnected: MirrorActions.onMirrorConnected,
        onMdnsScan: MdnsActions.onMdnsScan,
        onMdnsInit: MdnsActions.onMdnsInit,
        onMdnsStop: MdnsActions.onMdnsStop,
        onMdnsScanClear: MdnsActions.onMdnsScanClear,
        onMdnsLogClear: MdnsActions.onMdnsLogClear,
        onWsLog: WsActions.onWsLog,
        onWsLogClear: WsActions.onWsLogClear,
        onWsConnect: WsActions.onWsConnect,
        onWsDisconnect: WsActions.onWsDisconnect,
        closeConnection: WsActions.closeConnection,
        onWsSendMessage: WsActions.onWsSendMessage,
        onWsMessageReceived: WsActions.onWsMessageReceived,
      },
      dispatch,
    ),
)(MdnsDebugScreen);
