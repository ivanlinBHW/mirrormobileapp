import { ScaledSheet } from 'App/Helpers';
import { Styles, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    ...Styles.screen.container,
    margin: 30,
    flex: 1,
  },
  title: {
    ...Fonts.style.h4,
    textAlign: 'left',
    marginBottom: Metrics.baseMargin,
  },
  content: {
    ...Fonts.style.normal,
    marginBottom: Metrics.baseMargin / 2,
    color: 'gray',
  },
  message: {
    ...Fonts.style.normal,
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2,
    color: 'blue',
  },
  image: {
    width: '100%',
    height: '200@s',
    marginBottom: '15@vs',
  },
});
