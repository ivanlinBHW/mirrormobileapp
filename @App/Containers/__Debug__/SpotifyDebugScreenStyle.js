import { StyleSheet } from 'App/Helpers';
import { Metrics } from 'App/Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    marginHorizontal: Metrics.baseMargin / 2,
    width: '100%',
    marginBottom: Metrics.baseMargin / 2,
  },
  loadMessage: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  spotifyLoginButton: {
    justifyContent: 'center',
    backgroundColor: 'green',
    overflow: 'hidden',
  },
  greeting: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  border: {
    borderWidth: 1,
    borderColor: 'black',
  },
  marginTop: {
    marginTop: Metrics.baseMargin / 4,
  },
});
