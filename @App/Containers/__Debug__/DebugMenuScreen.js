import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RNRestart from 'react-native-restart';
import { ScrollView, FlatList, Text, View, Button, Platform } from 'react-native';
import Ping from 'react-native-ping';

import { MirrorActions, UserActions } from 'App/Stores';
import { Fonts, Classes, Styles } from 'App/Theme';
import { Separator } from 'App/Components';
import { Config } from 'App/Config';

import styles from './DebugMenuScreenStyle';
import { getBaseUrl } from 'App/Helpers/ApiHandler/ApiHandler';

class DebugMenuScreen extends React.Component {
  static propTypes = {
    hasNotificationsPermission: PropTypes.bool.isRequired,
    notificationEnable: PropTypes.bool.isRequired,
    currentNetworkInfo: PropTypes.object.isRequired,
    currentCountryCode: PropTypes.string.isRequired,
    appRoute: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
    userLogout: PropTypes.func.isRequired,
    userReset: PropTypes.func.isRequired,
    mirrorReset: PropTypes.func.isRequired,
    mirrorLogs: PropTypes.array.isRequired,
    mirrorLogClear: PropTypes.func.isRequired,
    isConnected: PropTypes.bool.isRequired,
    isConnecting: PropTypes.bool.isRequired,
    isScanning: PropTypes.bool.isRequired,
    isPaired: PropTypes.bool.isRequired,
    connectedDevice: PropTypes.object,
    acitiveBle: PropTypes.object,
    lastDevice: PropTypes.object,
    fcmToken: PropTypes.string,
    appState: PropTypes.object,
    featureOptions: PropTypes.array,
  };

  static defaultProps = {
    currentCountryCode: '',
    connectedDevice: {},
    acitiveBle: {},
    lastDevice: {},
    fcmToken: '',
  };

  constructor(props) {
    super(props);

    this.state = {
      pingApiServer: -1,
      pingDevice: -1,
      pingGoogle: -1,
    };
  }

  async componentDidMount() {
    __DEV__ && console.log('@Mount DebugMenuScreen!');

    const { currentCountryCode } = this.props;
    const hostUrl = getBaseUrl();

    const { lastDevice } = this.props;
    const apiUrl = hostUrl.replace('https://', '').replace('http://', '');
    const pingConfig = {
      timeout: 1000,
    };
    Ping.start('8.8.8.8', pingConfig).then((pingGoogle) => {
      this.setState({
        pingGoogle,
      });

      Ping.start(apiUrl, pingConfig).then((pingApiServer) => {
        this.setState({
          pingApiServer,
        });
      });

      if (lastDevice) {
        Ping.start(lastDevice.host, pingConfig).then((pingDevice) => {
          this.setState({
            pingDevice,
          });
        });
      }
    });
  }

  renderLogs = () => {
    const { mirrorLogs } = this.props;
    const renderItem = ({ item }) => <Text style={Fonts.style.small}>{item}</Text>;
    return (
      <View style={[Classes.fill]}>
        <FlatList
          style={[Classes.fill, Styles.border, styles.logList]}
          data={mirrorLogs}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  renderMirrorState = () => {
    const {
      appRoute: { prevRoute },
      appState,
      acitiveBle,
      lastDevice,
      connectedDevice,
      isConnected,
      isConnecting,
      isScanning,
      isPaired,
      fcmToken,
      hasNotificationsPermission,
      notificationEnable,
      currentCountryCode,
      featureOptions,
      currentNetworkInfo: { isInternetReachable, details },
      currentNetworkInfo,
    } = this.props;
    const { pingApiServer, pingDevice, pingGoogle } = this.state;
    return (
      <ScrollView style={styles.infoWrapper}>
        <Text>
          app version: <Text style={Fonts.style.extraSmall500}>{Config.APP_VERSION}</Text>
        </Text>
        <Text>
          hasNotificationsPermission:
          <Text style={Fonts.style.extraSmall500}>
            {hasNotificationsPermission ? 'yes' : 'no'}
          </Text>
        </Text>
        <Text>
          notificationEnable:
          <Text style={Fonts.style.extraSmall500}>
            {notificationEnable ? 'yes' : 'no'}
          </Text>
        </Text>
        <Text>
          Wifi SSID:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {details ? details.ssid : 'not connected'}
          </Text>
        </Text>
        <Text>
          Internet Reachable:
          <Text style={Fonts.style.extraSmall500}>
            {isInternetReachable ? 'YES' : 'NO'}
          </Text>
        </Text>
        <Text>
          currentNetworkInfo
          <Text style={Fonts.style.extraSmall500}>
            {JSON.stringify(currentNetworkInfo, null, 2)}
          </Text>
        </Text>
        <Text>
          Google DNS Ping response:
          <Text style={Fonts.style.extraSmall500}>{pingGoogle}</Text>
        </Text>
        <Text>
          API Server Ping response:
          <Text style={Fonts.style.extraSmall500}>{pingApiServer}</Text>
        </Text>
        <Text>
          Device Ping response:
          <Text style={Fonts.style.extraSmall500}>{pingDevice}</Text>
        </Text>
        <Text>
          prevRouteName:
          <Text style={Fonts.style.extraSmall500}>{prevRoute}</Text>
        </Text>
        <Text>
          HOST:{' '}
          <Text style={Fonts.style.extraSmall500} numberOfLines={3}>
            {JSON.stringify(Config.API_BASE_URL, null, 2)}
          </Text>
        </Text>
        <Text>
          CURRENT COUNTRY:{' '}
          <Text style={Fonts.style.extraSmall500} numberOfLines={3}>
            {Config.IS_ENABLE_AUTO_REGION_SWITCH
              ? currentCountryCode
              : 'not enable auto region.'}
          </Text>
        </Text>
        <Text>
          last Device:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {lastDevice ? lastDevice.host : 'none'}
          </Text>
        </Text>
        <Text>
          connected Device:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {connectedDevice ? connectedDevice.host : 'none'}
          </Text>
        </Text>
        <Text>
          initialized:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {connectedDevice ? connectedDevice.txt.initialized : 'none'}
          </Text>
        </Text>
        <Text>
          wifiSignalLevel:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {connectedDevice ? connectedDevice.txt.wifiSignalLevel : 'none'}
          </Text>
        </Text>
        <Text>
          active Ble:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {acitiveBle ? acitiveBle.name : 'none'}
          </Text>
        </Text>
        <Text>
          isConnected:{' '}
          <Text style={Fonts.style.extraSmall500}>{JSON.stringify(isConnected)}</Text>
        </Text>
        <Text>
          isConnecting:{' '}
          <Text style={Fonts.style.extraSmall500}>{JSON.stringify(isConnecting)}</Text>
        </Text>
        <Text>
          isScanning:{' '}
          <Text style={Fonts.style.extraSmall500}>{JSON.stringify(isScanning)}</Text>
        </Text>
        <Text>
          isPaired:{' '}
          <Text style={Fonts.style.extraSmall500}>{JSON.stringify(isPaired)}</Text>
        </Text>
        <Text>
          fcmToken: <Text style={Fonts.style.extraSmall500}>{fcmToken}</Text>
        </Text>
        <Text>
          locals:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {JSON.stringify(appState.currentLocales, null, 2)}
          </Text>
        </Text>
        <Text>
          timeZone:{' '}
          <Text style={Fonts.style.extraSmall500}>
            {JSON.stringify(appState.currentTimeZone)}
          </Text>
        </Text>
        <Text>
          isPad:{' '}
          <Text style={Fonts.style.extraSmall500}>{Platform.isPad ? 'Yes' : 'No'}</Text>
        </Text>
        <Text>
          featureOptions:{`\n`}
          {featureOptions
            ? featureOptions.map((e, i) => (
                <Text
                  key={`t-${i}`}
                  style={Fonts.style.extraSmall500}
                >{`    ${e.optionName}: ${e.value}\n`}</Text>
              ))
            : '{}'}
        </Text>
      </ScrollView>
    );
  };

  render() {
    const { userLogout, userReset, mirrorReset, mirrorLogClear } = this.props;
    return (
      <View style={styles.container}>
        {this.renderMirrorState()}

        <View style={[Classes.row, Classes.mainSpaceAround]}>
          <Button
            style={styles.button}
            onPress={() => {
              mirrorReset();
              userLogout();
              userReset();

              setTimeout(RNRestart.Restart, 1000);
            }}
            title={'Logout and restart'}
            color="red"
          />
          <Button style={styles.button} onPress={mirrorLogClear} title={'Clear log'} />
        </View>
        <Separator />
        {this.renderLogs()}
      </View>
    );
  }
}

export default connect(
  (state) => ({
    appState: state.appState,
    appRoute: state.appRoute,
    isLoading: state.appState.isLoading,
    fcmToken: state.user.fcmToken,
    mirrorLogs: state.mirror.logs,
    lastDevice: state.mirror.lastDevice,
    connectedSsidName: state.mirror.connectedSsidName,
    connectedDevice: state.mirror.connectedDevice,
    isConnected: state.mirror.isConnected,
    isConnecting: state.mirror.isConnected,
    isScanning: state.mirror.isScanning,
    acitiveBle: state.mirror.acitiveBle,
    isPaired: state.mirror.isPaired,
    options: state.mirror.options,
    featureOptions: state.mirror.featureOptions,
    currentCountryCode: state.user.currentCountryCode,
    currentNetworkInfo: state.appState.currentNetworkInfo,
    notificationEnable: state.user.notificationEnable,
    hasNotificationsPermission: state.user.hasNotificationsPermission,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        userReset: UserActions.onUserReset,
        userLogout: UserActions.onUserLogout,
        mirrorReset: MirrorActions.onMirrorReset,
        mirrorLogClear: MirrorActions.onMirrorLogClear,
      },
      dispatch,
    ),
)(DebugMenuScreen);
