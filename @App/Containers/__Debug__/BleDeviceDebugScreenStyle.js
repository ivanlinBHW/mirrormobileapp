import { StyleSheet } from 'App/Helpers';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#a92a35',
    padding: 5,
  },
  textStyle: {
    color: 'white',
    fontSize: 20,
  },
  logTextStyle: {
    color: 'white',
    fontSize: 9,
  },
  buttonStyle: {
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
    backgroundColor: '#af3c46',
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
  },
  disabledButtonStyle: {
    backgroundColor: '#614245',
    color: '#919191',
  },
  modalContainer: {
    backgroundColor: '#00000060',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalHeader: {
    backgroundColor: '#a92a35',
    borderRadius: 10,
    height: '50%',
    padding: 5,
    shadowColor: 'black',
    shadowRadius: 20,
    shadowOpacity: 0.9,
    elevation: 20,
  },
  logContainer: { flex: 1, padding: 10, paddingTop: 0 },
  deviceContainer: { flex: 1, padding: 10, paddingTop: 0 },
  fill: { flex: 1 },
  row: { flexDirection: 'row', paddingTop: 5 },
});
