import React from 'react';
import {
  Clipboard,
  ScrollView,
  Platform,
  Alert,
  Text,
  View,
  Image,
  Button,
} from 'react-native';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import { Permissions } from 'react-native-unimodules';

import { getCircularReplacer } from 'App/Helpers';
import style from './FcmDebugScreenStyle';

class FcmExampleScreen extends React.Component {
  channelId = 'test-channel';

  channelName = 'Test channel';

  state = {
    permission: null,
    state: '',
    message: '',
    token: 'no token yet',
  };
  async componentDidMount() {
    __DEV__ && console.log('@Mount FcmExampleScreen!');

    this.handleNotificationChannel();
    this.handleNotificationListeners();
    await this.handleRequestPermission();
  }

  handleNotificationChannel = () => {
    const channel = new firebase.notifications.Android.Channel(
      this.channelId,
      this.channelName,
      firebase.notifications.Android.Importance.Max,
    ).setDescription('My apps test channel');
    firebase.notifications().android.createChannel(channel);
  };

  handleNotificationListeners = async () => {
    this.messageListener = firebase.messaging().onMessage(this.onMessageReceiveListener);
    this.notificationListener = firebase
      .notifications()
      .onNotification(this.onMessageReceiveListener);
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen) =>
        this.onMessageReceiveListener(notificationOpen.notification),
      );
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const notification = notificationOpen.notification;
      console.log('Initial ', notification);

      this.onMessageReceiveListener(notification);
    }
  };

  handleRequestPermission = async () => {
    try {
      if (Platform.OS === 'ios') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        this.setState({
          permission: status,
        });
        if (status !== 'granted') {
          throw new Error('@FCM: Notification permission not granted.');
        }
      }
      await this.onPressGetToken();
    } catch (error) {
      Alert.alert(
        '@FCM: permission rejected',
        `${error.message}\n\n${JSON.stringify(error, null, 2)}`,
      );
      if (error.message.includes('MISSING_INSTANCE_SERVICE')) {
        Alert.alert(
          'Oops',
          'You are probably running this example within a simulator. You will need to install Google Play Service to using FCM.',
        );
      }
    }
  };

  onMessageReceiveListener = (message) => {
    console.log('onMessageReceiveListener=>', message);
    console.log('onMessageReceiveListener _data=>', message._data);
    console.log('onMessageReceiveListener _data.default=>', message._data.default);
    console.log(typeof message._data.default);

    this.setState({
      message,
    });
    if (typeof message._data.default === 'string') {
    }
  };

  onPressGetToken = async () => {
    const token = await firebase.messaging().getToken();
    this.setState({
      token,
    });
    Clipboard.setString(token);
    __DEV__ && console.log('@FCM: Token=>', token);
  };

  render() {
    const { message, token, permission } = this.state;
    return (
      <View style={style.container}>
        <ScrollView>
          <Image
            style={style.image}
            source={{
              uri: 'https://i.ytimg.com/vi/sioEY4tWmLI/maxresdefault.jpg',
            }}
            resizeMode={'contain'}
          />
          <Text style={style.title}>FCM Example</Text>
          <Text style={style.content}>
            This example will shows push notifications message when it arrives.
          </Text>

          {Platform.OS === 'ios' && (
            <>
              <Text style={style.title}>Permission Status</Text>
              <Text style={style.content}>{JSON.stringify(permission)}</Text>
            </>
          )}

          <Text style={style.title}>Device Token</Text>
          <Text style={style.content}>{token}</Text>

          <Text style={style.title}>Receive Message</Text>
          <Text style={style.message}>
            {JSON.stringify(message, getCircularReplacer(), 2)}
          </Text>

          <Button onPress={this.onPressGetToken} title="Copy FCM Token" />
        </ScrollView>
      </View>
    );
  }
}

FcmExampleScreen.propTypes = {};

export default connect(
  (state) => ({}),
  (dispatch) => ({}),
)(FcmExampleScreen);
