import { StyleSheet } from 'App/Helpers';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#a92a35',
    padding: '5@s',
  },
  textStyle: {
    color: 'white',
    fontSize: '20@s',
  },
  logTextStyle: {
    color: 'white',
    fontSize: 9,
  },
  buttonStyle: {
    borderWidth: 1,
    borderRadius: '5@s',
    padding: '5@s',
    backgroundColor: '#af3c46',
    color: 'white',
    textAlign: 'center',
    fontSize: '20@s',
  },
  disabledButtonStyle: {
    backgroundColor: '#614245',
    color: '#919191',
  },
  fill: { flex: 1, marginBottom: 2 },
  inputIOS: {
    fontSize: '16@s',
    paddingVertical: '12@vs',
    paddingHorizontal: '5@s',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 4,
    color: 'black',
  },
  inputAndroid: {
    fontSize: '16@s',
    paddingHorizontal: '5@s',
    paddingVertical: '12@vs',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 4,
    color: 'black',
  },
  button: {
    flex: 1,
    paddingRight: '5@s',
  },
  row: {
    flexDirection: 'row',
    paddingVertical: '5@s',
    justifyContent: 'center',
    alignItems: 'center',
  },
  border: { borderWidth: 1, borderColor: 'black' },

  modalContainer: {
    backgroundColor: '#00000060',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalHeader: {
    backgroundColor: '#a92a35',
    borderRadius: 10,
    height: '50%',
    padding: 5,
    shadowColor: 'black',
    shadowRadius: 20,
    shadowOpacity: 0.9,
    elevation: 20,
  },
});
