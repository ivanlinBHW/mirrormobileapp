import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Alert, Text, ScrollView } from 'react-native';
import firebase from 'react-native-firebase';
import { Classes } from 'App/Theme';
const Spotify = {
  isInitializedAsync: () => {},
  initialize: () => {},
  loginWithSession: () => {},
  getSessionAsync: () => {},
  renewSession: () => {},
  authenticate: () => {},
  logout: () => {},
  sendRequest: () => {},
};

import * as DebugMode from 'App/Components/DebugMode';
import { Separator, BaseButton, SimpleInputDialog } from 'App/Components';
import { UserActions, SpotifyActions } from 'App/Stores';
import { Spotify as SpotifyHelper } from 'App/Helpers';
import { Colors, Fonts } from 'App/Theme';
import { Config } from 'App/Config';
import styles from './SpotifyDebugScreenStyle';

class SpotifyDebugScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    userReset: PropTypes.func.isRequired,
    userLogin: PropTypes.func.isRequired,
    userLogout: PropTypes.func.isRequired,
    userSpotifyToken: PropTypes.object.isRequired,
    logs: PropTypes.array,
    currentSession: PropTypes.object.isRequired,
    isLogin: PropTypes.bool.isRequired,
    cleanLogs: PropTypes.func.isRequired,

    clientID: PropTypes.string,
    clientSecret: PropTypes.string,
    scopes: PropTypes.array,
    redirectUrl: PropTypes.string,
    tokenSwapUrl: PropTypes.string,
    tokenRefreshUrl: PropTypes.string,
    tokenRefreshEarliness: PropTypes.number,
    sessionUserDefaultsKey: PropTypes.string,
    spotifyChangeSettings: PropTypes.func,
  };

  static defaultProps = {
    logs: [],
  };

  constructor(props) {
    super(props);
    this.state = {
      spotifyInitialized: false,
      isLoggedIn: false,
      isSpotifySearchDialogVisible: false,
      isSpotifyUpdateURLDialogVisible: false,
      spotifyRefreshUrl: props.tokenRefreshUrl,
      spotifyRedirectUrl: props.redirectUrl,
      spotifySwapUrl: props.tokenSwapUrl,
      spotifyClientID: props.clientID,
      spotifyClientSecret: props.clientSecret,
      spotifySession: null,
      spotifyMe: null,
      spotifySearchResult: [],
      spotifySearchType: 'album,artist,playlist,track',
      spotifySearchQuery: 'Muse',
      spotifySearchLimit: '10',
      userEmail: '',
      userPassword: '',
      isUserDialogVisible: false,
    };
  }

  async componentDidMount() {
    const isLogin = await SpotifyHelper.initializeIfNeeded();

    console.log('componentDidMount isLogin=>', isLogin);
    if (isLogin) {
      this.setState({
        spotifyMe: await SpotifyHelper.getMe(this.props.currentSession.accessToken),
      });
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    const { isLogin } = this.props;
    if (prevProps.isLogin !== isLogin) {
      this.setState({
        spotifyMe: isLogin
          ? await SpotifyHelper.getMe(this.props.currentSession.accessToken)
          : {},
      });
    }
  }

  onSpotifyLoginButtonWasPressed = async () => {
    const {
      clientID,
      scopes,
      redirectUrl,
      tokenSwapUrl,
      tokenRefreshUrl,
      tokenRefreshEarliness,
      sessionUserDefaultsKey,
    } = this.props;
    await SpotifyHelper.login();
  };

  onSpotifyLogoutButtonWasPressed = () => {
    SpotifyHelper.logout();
  };

  onLoginPressed = async () => {
    const { userEmail, userPassword } = this.state;
    const { userLogin } = this.props;
    const payload = {
      email: userEmail,
      password: userPassword,
      deviceType: 0,
      platform: 1,
      deviceAddress: await firebase.messaging().getToken(),
    };
    userLogin(payload);
    this.onDialogTriggerPressed('isUserDialogVisible')();
  };

  onDialogTriggerPressed = (dialogKey, state = false) => () =>
    this.setState({ [dialogKey]: state });

  renderSpotifyLoginControl = () => {
    const { isLogin } = this.props;
    const { spotifyMe } = this.state;
    if (isLogin) {
      return (
        <>
          {spotifyMe ? (
            <Text style={styles.greeting}>
              You are logged in as {spotifyMe.display_name}
            </Text>
          ) : (
            <Text style={styles.greeting}>Getting user info...</Text>
          )}
          <BaseButton onPress={this.onSpotifyLogoutButtonWasPressed} text="Logout" />
        </>
      );
    }
    return (
      <View style={[Classes.container]}>
        <Text style={styles.greeting}>Hey! You! Log into your spotify</Text>
        <BaseButton
          onPress={this.onSpotifyLoginButtonWasPressed}
          style={styles.spotifyLoginButton}
          text="Spotify Login"
          textColor={Colors.white}
        />
      </View>
    );
  };

  renderSpotifyBackendControls = () => {
    const { spotifyChangeSettings } = this.props;
    const { isLoggedIn } = this.state;
    return (
      <DebugMode.ButtonContainer>
        <DebugMode.Button
          onPress={this.onDialogTriggerPressed('isSpotifyUpdateURLDialogVisible', true)}
          text="Change Settings"
          disabled={isLoggedIn}
        />
        <DebugMode.Button
          onPress={() => {
            spotifyChangeSettings({
              id: Config.SPOTIFY_CLIENT_ID,
              scopes: Config.SPOTIFY_SCOPES,
              secret: Config.SPOTIFY_CLIENT_SECRET,
              redirectUrl: Config.SPOTIFY_REDIRECT_URL,
              tokenSwapUrl: Config.SPOTIFY_TOKEN_SWAP_URL,
              tokenRefreshUrl: Config.SPOTIFY_TOKEN_REFRESH_URL,
              tokenRefreshEarliness: Config.SPOTIFY_TOKEN_REFRESH_EARLINESS,
            });
            this.setState({
              spotifyRedirectUrl: Config.SPOTIFY_REDIRECT_URL,
              spotifyRefreshUrl: Config.SPOTIFY_TOKEN_REFRESH_URL,
              spotifySwapUrl: Config.SPOTIFY_TOKEN_SWAP_URL,
              spotifyClientID: Config.SPOTIFY_CLIENT_ID,
              spotifyClientSecret: Config.SPOTIFY_CLIENT_SECRET,
            });
          }}
          text="Reset Settings"
          disabled={isLoggedIn}
        />
      </DebugMode.ButtonContainer>
    );
  };

  renderSpotifySessionControls = () => {
    const { isLogin, currentSession } = this.props;
    return (
      <DebugMode.ButtonContainer>
        <DebugMode.Button
          onPress={() => {
            const status = SpotifyHelper.checkExpiredStatus();
            Alert.alert('Success', JSON.stringify(status, null, 2));
          }}
          text={'Check Session'}
          disabled={!isLogin}
        />
        <DebugMode.Button
          onPress={async () => {
            const session = await SpotifyHelper.refreshLogin();
            Alert.alert('Success', JSON.stringify(session, null, 2));
          }}
          text={'Renew Session'}
          disabled={!isLogin}
        />
      </DebugMode.ButtonContainer>
    );
  };

  renderSpotifyMetadataControls = () => {
    const { isLogin } = this.props;
    return (
      <DebugMode.ButtonContainer>
        <DebugMode.Button
          onPress={async () => {
            try {
              const me = await SpotifyHelper.getMe(this.props.currentSession.accessToken);
              if (me) {
                this.setState({
                  spotifyMe: me,
                });
              }
              Alert.alert('Success', JSON.stringify(me, null, 2));
            } catch (e) {
              console.log(e);
              Alert.alert('Error', JSON.stringify(e, null, 2));
            }
          }}
          text="Get Me"
          disabled={!isLogin}
        />
        <DebugMode.Button
          onPress={async () => {
            try {
              const res = await SpotifyHelper.getMyPlaylists(
                this.props.currentSession.accessToken,
              );
              console.log('result=>', JSON.stringify(res, null, 2));
              Alert.alert('Success', JSON.stringify(res, null, 2));
              if (res.items) {
                this.setState({
                  spotifySearchResult: res.items,
                });
              }
            } catch (e) {
              Alert.alert('Error', JSON.stringify(e, null, 2));
            }
          }}
          text="Get playlists"
          disabled={!isLogin}
        />
        <DebugMode.Button
          onPress={this.onDialogTriggerPressed('isSpotifySearchDialogVisible', true)}
          text="Search..."
          disabled
        />
      </DebugMode.ButtonContainer>
    );
  };

  renderSpotifyPlaybackControls = () => {
    const { isLogin } = this.props;
    return (
      <DebugMode.ButtonContainer>
        <DebugMode.Button
          onPress={async () => await Spotify.setPlaying()}
          text="Play URI"
          disabled={!isLogin}
        />
      </DebugMode.ButtonContainer>
    );
  };

  renderButton = () => {
    const { userLogout, userReset, user, isLogin } = this.props;
    return (
      <>
        {this.renderSpotifyLoginControl()}
        {/* {this.renderSpotifyBackendControls()} */}
        {this.renderSpotifySessionControls()}
        {this.renderSpotifyMetadataControls()}
        {/* {this.renderSpotifyPlaybackControls()} */}
        {/* <DebugMode.ButtonContainer>
          <DebugMode.Button
            onPress={
              !isLogin
                ? this.onDialogTriggerPressed('isUserDialogVisible', true)
                : userLogout
            }
            text={!isLogin ? 'User Login' : 'User Logout'}
          />
          <DebugMode.Button
            onPress={userReset}
            text={'User Reset'}
            disabled={!user.token}
          />
        </DebugMode.ButtonContainer> */}
      </>
    );
  };

  renderSearchResult = () => {
    const { spotifySearchResult } = this.state;
    return (
      <DebugMode.LogList
        data={spotifySearchResult}
        renderItem={({ item }) => (
          <DebugMode.Button
            onPress={async () => await Spotify.playURI(item.uri, 0, 0)}
            text={`${item.type} - ${item.name}`}
          />
        )}
      />
    );
  };

  renderInfo = () => {
    const {
      user,
      userSpotifyToken,
      clientID,
      clientSecret,
      scopes,
      redirectUrl,
      tokenSwapUrl,
      tokenRefreshUrl,
      tokenRefreshEarliness,
      sessionUserDefaultsKey,
      currentSession,
    } = this.props;
    const { spotifySession, spotifyMe } = this.state;
    return (
      <View style={[Classes.container, Classes.center, styles.container]}>
        <View style={[Classes.crossStart]}>
          <Text style={styles.marginTop}>
            userSpotifyToken:{`\n`}
            <Text style={Fonts.style.small500}>
              {JSON.stringify(userSpotifyToken, null, 2)}
            </Text>
          </Text>
          <Text style={styles.marginTop}>
            currentSession:{`\n`}
            <Text style={Fonts.style.small500}>
              {JSON.stringify(currentSession, null, 2)}
            </Text>
          </Text>
          <Separator width={300} />
          <Text style={styles.marginTop}>
            spotifyMe:{`\n`}
            <Text style={Fonts.style.small500}>{JSON.stringify(spotifyMe, null, 2)}</Text>
          </Text>
          <Separator width={300} />
          {/* <Text style={styles.marginTop}>
            User name:{`\n`}
            <Text style={Fonts.style.small500}>{user.name}</Text>
          </Text> */}
          {/* <Text style={styles.marginTop}>
            User token:{`\n`}
            <Text style={Fonts.style.small500}>{user.token}</Text>
          </Text> */}
          {/* <Separator width={300} /> */}
          <Text style={styles.marginTop}>
            clientID:{`\n`}
            <Text style={Fonts.style.small500}>{clientID}</Text>
          </Text>
          {/* <Text style={styles.marginTop}>
            clientSecret:{`\n`}
            <Text style={Fonts.style.small500}>{clientSecret}</Text>
          </Text> */}
          <Text style={styles.marginTop}>
            tokenRefreshEarliness:{`\n`}
            <Text style={Fonts.style.small500}>{tokenRefreshEarliness}</Text>
          </Text>
          <Text style={styles.marginTop}>
            sessionUserDefaultsKey:{`\n`}
            <Text style={Fonts.style.small500}>{sessionUserDefaultsKey}</Text>
          </Text>
          <Text style={styles.marginTop}>
            scopes:{`\n`}
            <Text style={Fonts.style.small500}>{JSON.stringify(scopes, null, 2)}</Text>
          </Text>
          <Text style={styles.marginTop}>
            redirectUrl:{`\n`}
            <Text style={Fonts.style.small500}>{redirectUrl}</Text>
          </Text>
          <Text style={styles.marginTop}>
            tokenSwapUrl:{`\n`}
            <Text style={Fonts.style.small500}>{tokenSwapUrl}</Text>
          </Text>
          <Text style={styles.marginTop}>
            tokenRefreshUrl:{`\n`}
            <Text style={Fonts.style.small500}>{tokenRefreshUrl}</Text>
          </Text>
        </View>
      </View>
    );
  };

  renderUserLoginDialog = () => {
    const { isUserDialogVisible, userEmail, userPassword } = this.state;
    return (
      <SimpleInputDialog
        visible={isUserDialogVisible}
        title="Enter Email and Password"
        description="you can login with any user account here, this will effects all user credentials in the app."
        inputFields={[
          {
            placeholder: 'Email',
            value: userEmail,
            onChangeText: (v) => this.setState({ userEmail: v }),
          },
          {
            placeholder: 'Password',
            value: userPassword,
            onChangeText: (v) => this.setState({ userPassword: v }),
          },
        ]}
        onConfirmPress={this.onLoginPressed}
        onCancelPress={this.onDialogTriggerPressed('isUserDialogVisible')}
        isConfirmDisabled={!userEmail || !userPassword}
      />
    );
  };

  renderChangeSpotifyUrlDialog = () => {
    const { spotifyChangeSettings, tokenRefreshEarliness } = this.props;
    const {
      isSpotifyUpdateURLDialogVisible,
      spotifyRedirectUrl,
      spotifyRefreshUrl,
      spotifySwapUrl,
      spotifyClientID,
      spotifyClientSecret,
    } = this.state;
    return (
      <SimpleInputDialog
        visible={isSpotifyUpdateURLDialogVisible}
        title="Spotify connection settings"
        description="Settings will effects after"
        inputFields={[
          {
            label: 'Client ID',
            value: spotifyClientID,
            onChangeText: (v) => this.setState({ spotifyClientID: v }),
          },
          {
            label: 'Client Secret',
            value: spotifyClientSecret,
            onChangeText: (v) => this.setState({ spotifyClientSecret: v }),
          },
          {
            label: 'Redirect URL',
            value: spotifyRedirectUrl,
            onChangeText: (v) => this.setState({ spotifyRedirectUrl: v }),
          },
          {
            label: 'Toke Swap URL',
            value: spotifySwapUrl,
            onChangeText: (v) => this.setState({ spotifySwapUrl: v }),
          },
          {
            label: 'Token Refresh URL',
            value: spotifyRefreshUrl,
            onChangeText: (v) => this.setState({ spotifyRefreshUrl: v }),
          },
        ]}
        onConfirmPress={() => {
          spotifyChangeSettings({
            id: spotifyClientID,
            secret: spotifyClientSecret,
            redirectUrl: spotifyRedirectUrl,
            tokenSwapUrl: spotifySwapUrl,
            tokenRefreshUrl: spotifyRefreshUrl,
            tokenRefreshEarliness: tokenRefreshEarliness,
          });
          this.onDialogTriggerPressed('isSpotifyUpdateURLDialogVisible')();
        }}
        onCancelPress={this.onDialogTriggerPressed('isSpotifyUpdateURLDialogVisible')}
        isConfirmDisabled={!spotifyRedirectUrl || !spotifyRefreshUrl || !spotifySwapUrl}
      />
    );
  };

  renderSpotifySearchDialog = () => {
    const {
      isSpotifySearchDialogVisible,
      spotifySearchType,
      spotifySearchLimit,
      spotifySearchQuery,
    } = this.state;
    return (
      <SimpleInputDialog
        visible={isSpotifySearchDialogVisible}
        title="Spotify Search"
        inputFields={[
          {
            type: 'SELECT',
            value: spotifySearchType,
            items: [
              { value: 'album' },
              { value: 'artist' },
              { value: 'playlist' },
              { value: 'track' },
            ],
            onValueChange: (v) => this.setState({ spotifySearchType: v }),
          },
          {
            type: 'INPUT',
            placeholder: 'Arrival',
            label: 'Search Query',
            value: spotifySearchQuery,
            onChangeText: (v) => this.setState({ spotifySearchQuery: v }),
          },
          {
            placeholder: '10 - Search Limit',
            value: spotifySearchLimit,
            label: 'Search Limit',
            keyboardType: 'numeric',
            onChangeText: (v) => this.setState({ spotifySearchLimit: v }),
          },
        ]}
        onConfirmPress={async () => {
          try {
            const result = await SpotifyHelper.search(
              this.props.currentSession.accessToken,
              spotifySearchQuery,
              [spotifySearchType],
              {
                limit: parseInt(spotifySearchLimit, 10),
              },
            );
            Alert.alert('Success', JSON.stringify(result, null, 2));
            if (result) {
              this.setState({
                spotifySearchResult: result[`${spotifySearchType}s`].items,
              });
            }
            this.onDialogTriggerPressed('isSpotifySearchDialogVisible')();
          } catch (e) {
            Alert.alert('Error', e.message || JSON.stringify(e, null, 2));
          } finally {
          }
        }}
        onCancelPress={this.onDialogTriggerPressed('isSpotifySearchDialogVisible')}
      />
    );
  };

  renderLogs() {
    return (
      <View style={Classes.fill}>
        <DebugMode.LogList title={'Logs'} data={this.props.logs} />
        <DebugMode.ButtonContainer>
          <DebugMode.Button onPress={this.props.cleanLogs} text={'Clear logs'} />
        </DebugMode.ButtonContainer>
      </View>
    );
  }

  render() {
    return (
      <DebugMode.Container>
        {this.renderButton()}
        {this.renderLogs()}
        <ScrollView style={Classes.fill}>
          {/* {this.renderSearchResult()} */}
          {this.renderInfo()}
        </ScrollView>
        {this.renderUserLoginDialog()}
        {this.renderSpotifySearchDialog()}
        {this.renderChangeSpotifyUrlDialog()}
      </DebugMode.Container>
    );
  }
}

export default connect(
  (state) => ({
    user: state.user,
    isPaired: state.mirror.isPaired,

    currentSession: state.spotify.currentSession,
    isLogin: state.spotify.isLogin,
    logs: state.spotify.logs,

    userSpotifyToken: state.user.spotifyTokens,
    clientID: state.spotify.clientID,
    clientSecret: state.spotify.clientSecret,
    scopes: state.spotify.scopes,
    redirectUrl: state.spotify.redirectUrl,
    tokenSwapUrl: state.spotify.tokenSwapUrl,
    tokenRefreshUrl: state.spotify.tokenRefreshUrl,
    tokenRefreshEarliness: state.spotify.tokenRefreshEarliness,
    sessionUserDefaultsKey: state.spotify.sessionUserDefaultsKey,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        cleanLogs: SpotifyActions.onSpotifyCleanLogs,
        userReset: UserActions.onUserReset,
        userLogin: UserActions.fetchUserLogin,
        userLogout: UserActions.onUserLogout,
        spotifyChangeSettings: SpotifyActions.onChangeSettings,
      },
      dispatch,
    ),
)(SpotifyDebugScreen);
