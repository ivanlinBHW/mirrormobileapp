import { ScaledSheet } from 'App/Helpers';
import Colors from 'App/Theme/Colors';
import Styles from 'App/Theme/Styles';
import Fonts from 'App/Theme/Fonts';
import Classes from 'App/Theme/Classes';

export default ScaledSheet.create({
  container: {
    ...Styles.screen.container,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  navBar: {
    ...Classes.mainStart,
    ...Classes.crossStart,
    flex: 0.1,
  },
  infoWrapper: {
    ...Styles.screen.container,
    marginLeft: 8,
  },
  title: {
    ...Fonts.style.h2,
    marginVertical: '15@vs',
    color: Colors.text,
  },
  greeting: {
    ...Fonts.style.h4,
    marginBottom: '15@vs',
    color: Colors.primary,
  },
  description: {
    ...Fonts.style.normal,
    marginBottom: '5@s',
    color: Colors.text,
    fontStyle: 'italic',
  },
  text: {
    ...Fonts.style.normal,
    textAlign: 'center',
    marginBottom: '5@s',
    color: Colors.text,
  },
  logList: {
    width: 350,
    height: 200,
  },
});
