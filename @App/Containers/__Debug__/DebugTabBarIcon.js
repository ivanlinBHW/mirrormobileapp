import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Screen } from 'App/Helpers';

export default class TabBarIcon extends React.PureComponent {
  static propTypes = {
    iconName: PropTypes.string.isRequired,
  };
  render() {
    const style = { marginBottom: -3 };
    return (
      <Icon
        name={this.props.iconName}
        size={Screen.scale(20)}
        style={style}
      />
    );
  }
}
