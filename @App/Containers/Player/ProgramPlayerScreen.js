import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isEmpty, isEqual, has } from 'lodash';
import { Alert, BackHandler } from 'react-native';

import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { ProgramActions, MirrorActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';

import PlayerView from './PlayerView';

class ProgramPlayerScreen extends React.Component {
  static propTypes = {
    detail: PropTypes.object.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    finishProgramClass: PropTypes.func.isRequired,
    pauseProgramClass: PropTypes.func.isRequired,
    currentDuration: PropTypes.number,
    currentStepId: PropTypes.string,
    token: PropTypes.string,
    user: PropTypes.object.isRequired,
    sceneKey: PropTypes.string.isRequired,
    currentSceneName: PropTypes.string.isRequired,
    activeTrainingProgramId: PropTypes.string,
    isProgramFinished: PropTypes.bool,
    programHistoryId: PropTypes.string,
    trainingProgramScheduleId: PropTypes.string,
    trainingProgramClassHistoryId: PropTypes.string,
    setCurrentStepId: PropTypes.func.isRequired,
    stashProgramStepId: PropTypes.func.isRequired,
    setCurrentPosition: PropTypes.func.isRequired,
    resetCurrentStep: PropTypes.func.isRequired,
    setActiveEvent: PropTypes.func.isRequired,
    setCarouselDuration: PropTypes.func.isRequired,
    updateMirrorStore: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    eventDetail: PropTypes.object.isRequired,
    currentClassType: PropTypes.string.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    currentDuration: 0,
    isLoading: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      detail: { trainingSteps = [] },
    } = nextProps;
    trainingSteps.forEach((item, index) => {
      if (item.id === nextProps.currentStepId) {
        return {
          count: index,
        };
      }
    });
    return null;
  }

  state = {
    isPlay: true,
    count: 0,
    isNext: false,
  };

  componentDidMount() {
    console.log('======= ProgramPlayerScreen ========');
    const {
      detail,
      resetCurrentStep,
      setActiveEvent,
      setCarouselDuration,
      setCurrentStepId,
    } = this.props;
    const { trainingSteps } = detail;

    setActiveEvent('play');
    if (
      has(detail, 'trainingProgramClassHistory') &&
      !isEmpty(detail.trainingProgramClassHistory.pausedStepId)
    ) {
      trainingSteps.forEach((item, index) => {
        if (item.id === detail.trainingProgramClassHistory.pausedStepId) {
          this.setState({ count: index });
        }
      });
    } else {
      resetCurrentStep();
      console.log('=== trainingSteps[0].id ===', trainingSteps[0].id);
      setCurrentStepId(trainingSteps[0].id);
      this.setState({
        count: 0,
      });
    }
    setCarouselDuration();
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.onPressExit,
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { sceneKey, currentSceneName } = this.props;
    return (
      (sceneKey === currentSceneName ||
        ['AudioModal', 'ReactionModal', 'HeartRateModal', 'WorkoutOptionModal'].includes(
          currentSceneName,
        )) &&
      (!isEqual(this.props, nextProps) || !isEqual(this.state, nextState))
    );
  }

  componentDidUpdate(prevProps) {
    const { currentStepId, stashProgramStepId, detail, user } = this.props;
    const { trainingSteps } = detail;
    if (currentStepId !== prevProps.currentStepId && !this.state.isNext) {
      trainingSteps.forEach((item, index) => {
        if (item.id === currentStepId) {
          stashProgramStepId(
            user.trainingProgramClassHistoryId,
            currentStepId !== '' ? currentStepId : trainingSteps[index].id,
          );
        }
      });
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onPressControlPause = () => {
    const {
      onMirrorCommunicate,
      setActiveEvent,
      setCarouselDuration,
      detail,
    } = this.props;
    setActiveEvent('pause');
    setCarouselDuration();
    onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
      action: 'pause',
      stepId: detail.trainingSteps[this.state.count].id,
    });
  };

  onPressControlPlay = () => {
    const {
      onMirrorCommunicate,
      setActiveEvent,
      setCarouselDuration,
      detail,
    } = this.props;
    const { count } = this.state;
    setActiveEvent('play');
    setCarouselDuration();
    onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
      action: 'play',
      stepId: detail.trainingSteps[count].id,
    });
  };

  onPressControl = (type) => {
    const {
      onMirrorCommunicate,
      detail,
      token,
      user,
      finishProgramClass,
      activeTrainingProgramId,
      setCurrentStepId,
      stashProgramStepId,
      setCurrentPosition,
      setActiveEvent,
      currentDuration,
      updateMirrorStore,
    } = this.props;
    const { isPlay, count } = this.state;
    const { trainingSteps } = detail;
    let tempDuration = 0;
    let nowStepDuration = null;
    trainingSteps.forEach((item, index) => {
      if (this.state.count > index) {
        nowStepDuration += item.duration;
      }
    });
    switch (type) {
      case 'prev':
        if (count > 0) {
          setActiveEvent('play');
          if (this.state.count > 0) {
            if (currentDuration - nowStepDuration > 2) {
              onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                action: 'prev-step',
                stepId: trainingSteps[this.state.count].id,
              });
              setCurrentStepId(trainingSteps[this.state.count].id);
              setCurrentPosition(nowStepDuration);
              updateMirrorStore({ isSameStepChange: true });
            } else {
              this.setState({ count: this.state.count - 1 }, () => {
                onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                  action: 'prev-step',
                  stepId: trainingSteps[this.state.count].id,
                });
                setCurrentStepId(trainingSteps[this.state.count].id);
                trainingSteps.forEach((item, index) => {
                  if (this.state.count > index) {
                    tempDuration += item.duration;
                  }
                });
                setCurrentPosition(tempDuration);
                updateMirrorStore({ isDiffStepChange: true });
              });
            }
            stashProgramStepId(
              user.trainingProgramClassHistoryId,
              trainingSteps[count].id,
            );
          }
        } else {
          setCurrentPosition(0);
          setCurrentStepId(trainingSteps[0].id);
          onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
            action: 'prev-step',
            stepId: trainingSteps[0].id,
          });
        }
        break;
      case 'play':
        isPlay ? this.onPressControlPause() : this.onPressControlPlay();
        this.setState({ isPlay: !isPlay });
        break;
      case 'next':
        if (count < trainingSteps.length - 1) {
          setActiveEvent('play');
          this.setState({ count: this.state.count + 1 }, () => {
            onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
              action: 'next-step',
              stepId: trainingSteps[this.state.count].id,
            });
            setCurrentStepId(trainingSteps[this.state.count].id);
            trainingSteps.forEach((item, index) => {
              if (this.state.count > index) {
                tempDuration += item.duration;
              }
            });
            setCurrentPosition(tempDuration);
            updateMirrorStore({ isDiffStepChange: true });
          });
          stashProgramStepId(
            user.trainingProgramClassHistoryId,
            trainingSteps[this.state.count].id,
          );
        } else {
          updateMirrorStore({ isPlayingClass: false });
          if (!this.state.isNext) {
            this.setState(
              (state) => ({ ...state, isNext: true }),
              () => {
                const body = {
                  trainingProgramClassHistoryId: user.trainingProgramClassHistoryId,
                  trainingProgramHistoryId:
                    detail.trainingProgramClassHistory.trainingProgramHistoryId,
                };
                finishProgramClass(detail.id, token, body, activeTrainingProgramId);
                onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                  action: 'next-step',
                });
              },
            );
          }
        }
        break;
      case 'force-stop':
        updateMirrorStore({ isPlayingClass: false });
        if (!this.state.isNext) {
          this.setState({ isNext: true });
        }
        const body = {
          trainingProgramClassHistoryId: user.trainingProgramClassHistoryId,
          trainingProgramHistoryId:
            detail.trainingProgramClassHistory.trainingProgramHistoryId,
        };
        finishProgramClass(detail.id, token, body, activeTrainingProgramId);
        onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
          action: 'next-step',
        });
        break;
      default:
        break;
    }
  };

  onPressExit = () => {
    Alert.alert(t('player_screen_title'), t('player_screen_content'), [
      {
        text: t('player_screen_cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('player_screen_quit'),
        onPress: this.onPressExitAction,
      },
    ]);
    return true;
  };

  onPressExitAction = () => {
    const {
      detail,
      onMirrorCommunicate,
      pauseProgramClass,
      token,
      trainingProgramScheduleId,
      isProgramFinished,
      trainingProgramClassHistoryId,
      updateMirrorStore,
      currentStepId,
    } = this.props;
    const { trainingSteps } = detail;
    const { count } = this.state;
    pauseProgramClass(
      trainingProgramClassHistoryId,
      token,
      currentStepId,
      trainingProgramScheduleId,
      isProgramFinished,
      detail.id,
    );
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS, {
      classId: detail.id,
      programClassHistoryId: trainingProgramClassHistoryId,
    });
    updateMirrorStore({ isPlayingClass: false });
  };

  render() {
    const {
      isLoading,
      detail: playerDetail,
      currentStepId,
      currentClassType,
      onMirrorCommunicate,
      isMotionCaptureEnabled,
      updateMirrorStore,
    } = this.props;
    const { isPlay } = this.state;
    return (
      <PlayerView
        isPlaying={isPlay}
        isLoading={isLoading}
        isMotionCaptureEnabled={isMotionCaptureEnabled}
        playerDetail={playerDetail}
        currentStepId={currentStepId}
        currentClassType={currentClassType}
        onPressExit={this.onPressExit}
        onPressControl={this.onPressControl}
        onMirrorCommunicate={onMirrorCommunicate}
        updateMirrorStore={updateMirrorStore}
      />
    );
  }
}

export default connect(
  (state, param) => ({
    isLoading: state.appState.isLoading,
    sceneKey: param.name,
    detail: state.player.detail,
    currentStepId: state.mirror.currentStepId,
    currentDuration: state.mirror.currentDuration,
    token: state.user.token,
    user: state.user,
    currentSceneName: state.appRoute.routeName,
    activeTrainingProgramId: state.user.activeTrainingProgramId,
    isProgramFinished: state.program.isProgramFinished,
    programHistoryId: state.user.activeTrainingProgramHistoryId,
    trainingProgramClassHistoryId: state.user.trainingProgramClassHistoryId,
    isMotionCaptureEnabled: state.player.isMotionCaptureEnabled,

    eventDetail: state.community.eventDetail,
    currentClassType: state.mirror.currentClassType,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        setCurrentStepId: MirrorActions.setCurrentStepId,
        setCurrentPosition: MirrorActions.setCurrentPosition,
        stashProgramStepId: ProgramActions.stashProgramStepId,
        finishProgramClass: ProgramActions.finishProgramClass,
        pauseProgramClass: ProgramActions.pauseProgramClass,
        resetCurrentStep: MirrorActions.resetCurrentStep,
        setActiveEvent: MirrorActions.setActiveEvent,
        setCarouselDuration: MirrorActions.setCarouselDuration,
        updateMirrorStore: MirrorActions.updateMirrorStore,
      },
      dispatch,
    ),
)(ProgramPlayerScreen);
