import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isEmpty, isEqual, has, get } from 'lodash';
import { Alert, BackHandler } from 'react-native';

import { activateKeepAwake, deactivateKeepAwake } from 'App/Helpers';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { ClassActions, MirrorActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';

import PlayerView from './PlayerView';

class PlayerScreen extends React.Component {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired,
    playerDetail: PropTypes.object.isRequired,
    eventDetail: PropTypes.object.isRequired,
    currentClassType: PropTypes.string.isRequired,
    currentStepId: PropTypes.string,
    sceneKey: PropTypes.string.isRequired,
    currentSceneKey: PropTypes.string.isRequired,

    pauseClass: PropTypes.func.isRequired,
    finishClass: PropTypes.func.isRequired,
    currentDuration: PropTypes.number,
    token: PropTypes.string,
    stashClassStepId: PropTypes.func.isRequired,
    setCurrentStepId: PropTypes.func.isRequired,
    resetCurrentPosition: PropTypes.func.isRequired,
    setCurrentPosition: PropTypes.func.isRequired,
    resetCurrentStep: PropTypes.func.isRequired,
    setActiveEvent: PropTypes.func.isRequired,
    setCarouselDuration: PropTypes.func.isRequired,
    updateMirrorStore: PropTypes.func.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
    playerState: PropTypes.string,
  };

  static defaultProps = {
    currentDuration: 0,
    isLoading: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      playerDetail: { trainingSteps = [] },
    } = nextProps;
    trainingSteps.forEach((item, index) => {
      if (item && item.id === nextProps.currentStepId) {
        return {
          count: index,
        };
      }
    });
    return null;
  }

  state = {
    isPlay: true,
    count: 0,
    isNext: false,
  };

  componentDidMount() {
    const {
      playerDetail,
      resetCurrentStep,
      setActiveEvent,
      setCarouselDuration,
      playerState,
      setCurrentStepId,
    } = this.props;
    const { trainingSteps } = playerDetail;

    console.log('=== playerState ===', playerState);

    if (playerState === 'idle') this.setState({ isPlay: false });

    setActiveEvent('play');
    if (
      has(playerDetail, 'trainingClassHistory') &&
      !isEmpty(get(playerDetail, 'trainingClassHistory.pausedStepId'))
    ) {
      trainingSteps.forEach((item, index) => {
        if (item.id === playerDetail.trainingClassHistory.pausedStepId) {
          this.setState({ count: index });
        }
      });
    } else {
      resetCurrentStep();
      console.log('=== trainingSteps[0].id ===', trainingSteps[0].id);
      setCurrentStepId(trainingSteps[0].id);
      this.setState({
        count: 0,
      });
    }
    setCarouselDuration();
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.onPressExit,
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { sceneKey, currentSceneKey } = this.props;
    return (
      (sceneKey === currentSceneKey ||
        [
          'AudioModal',
          'ReactionModal',
          'HeartRateModal',
          'WorkoutOptionModal',
          'NotificationScreen',
        ].includes(currentSceneKey)) &&
      (!isEqual(this.props, nextProps) || !isEqual(this.state, nextState))
    );
  }

  componentDidUpdate(prevProps) {
    const { currentStepId, stashClassStepId, playerDetail, playerState } = this.props;
    const { trainingSteps } = playerDetail;

    if (playerState !== prevProps.playerState) {
      this.setState({ isPlay: playerState !== 'idle' });
    }

    if (currentStepId !== prevProps.currentStepId && !this.state.isNext) {
      trainingSteps.forEach((item, index) => {
        if (item && currentStepId && item.id === currentStepId) {
          stashClassStepId(
            playerDetail.trainingClassHistory.id,
            currentStepId !== '' ? currentStepId : trainingSteps[index].id,
          );
        }
      });
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onPressControlPause = () => {
    const {
      onMirrorCommunicate,
      setActiveEvent,
      setCarouselDuration,
      playerDetail,
    } = this.props;
    setActiveEvent('pause');
    setCarouselDuration();
    onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
      action: 'pause',
      stepId: playerDetail.trainingSteps[this.state.count].id,
    });
  };

  onPressControlPlay = () => {
    const {
      onMirrorCommunicate,
      setActiveEvent,
      setCarouselDuration,
      playerDetail,
    } = this.props;
    const { count } = this.state;
    setActiveEvent('play');
    setCarouselDuration();
    onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
      action: 'play',
      stepId: playerDetail.trainingSteps[count].id,
    });
  };

  onPressControl = (type) => {
    const {
      onMirrorCommunicate,
      playerDetail,
      finishClass,
      user,
      stashClassStepId,
      setCurrentStepId,
      setCurrentPosition,
      setActiveEvent,
      currentDuration,
      updateMirrorStore,
    } = this.props;
    const { isPlay, count } = this.state;
    const { trainingSteps } = playerDetail;
    let tempDuration = 0;
    let nowStepDuration = null;
    trainingSteps.forEach((item, index) => {
      if (this.state.count > index) {
        nowStepDuration += item.duration;
      }
    });
    switch (type) {
      case 'prev':
        if (count > 0) {
          setActiveEvent('play');
          if (this.state.count > 0) {
            if (currentDuration - nowStepDuration > 2) {
              onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                action: 'prev-step',
                stepId: trainingSteps[this.state.count].id,
              });
              setCurrentStepId(trainingSteps[this.state.count].id);
              setCurrentPosition(nowStepDuration);
              updateMirrorStore({ isSameStepChange: true });
            } else {
              this.setState({ count: this.state.count - 1 }, () => {
                onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                  action: 'prev-step',
                  stepId: trainingSteps[this.state.count].id,
                });
                setCurrentStepId(trainingSteps[this.state.count].id);
                trainingSteps.forEach((item, index) => {
                  if (this.state.count > index) {
                    tempDuration += item.duration;
                  }
                });
                setCurrentPosition(tempDuration);
                updateMirrorStore({ isDiffStepChange: true });
              });
            }
            stashClassStepId(
              playerDetail.trainingClassHistory.id,
              trainingSteps[count].id,
            );
          }
        } else {
          setCurrentPosition(0);
          setCurrentStepId(trainingSteps[0].id);
          onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
            action: 'prev-step',
            stepId: trainingSteps[0].id,
          });
        }
        break;
      case 'play':
        isPlay ? this.onPressControlPause() : this.onPressControlPlay();
        this.setState({ isPlay: !isPlay });
        break;
      case 'next':
        if (count < trainingSteps.length - 1) {
          setActiveEvent('play');
          this.setState({ count: this.state.count + 1 }, () => {
            onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
              action: 'next-step',
              stepId: trainingSteps[this.state.count].id,
            });
            setCurrentStepId(trainingSteps[this.state.count].id);
            trainingSteps.forEach((item, index) => {
              if (this.state.count > index) {
                tempDuration += item.duration;
              }
            });
            setCurrentPosition(tempDuration);
            updateMirrorStore({ isDiffStepChange: true });
          });
          stashClassStepId(
            playerDetail.trainingClassHistory.id,
            trainingSteps[this.state.count].id,
          );
        } else {
          updateMirrorStore({ isPlayingClass: false });
          if (!this.state.isNext) {
            this.setState({ isNext: true }, () => {
              finishClass(playerDetail.trainingClassHistory.id, user.token);
              onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                action: 'next-step',
              });
            });
          }
        }
        break;
      case 'force-stop':
        updateMirrorStore({ isPlayingClass: false });
        if (!this.state.isNext) {
          this.setState({ isNext: true });
        }
        finishClass(playerDetail.trainingClassHistory.id, user.token);
        onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
          action: 'next-step',
        });
        break;
      default:
        break;
    }
  };

  onPressExit = () => {
    Alert.alert(t('player_screen_title'), t('player_screen_content'), [
      {
        text: t('player_screen_cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('player_screen_quit'),
        onPress: this.onPressExitAction,
      },
    ]);
    return true;
  };

  onPressExitAction = () => {
    const {
      playerDetail,
      onMirrorCommunicate,
      pauseClass,
      currentStepId,
      token,
      updateMirrorStore,
    } = this.props;
    pauseClass(
      playerDetail.trainingClassHistory.id,
      token,
      currentStepId,
      playerDetail.id,
    );
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS, {
      classId: playerDetail.id,
      trainingClassHistoryId: playerDetail.trainingClassHistory.id,
    });
    updateMirrorStore({ isPlayingClass: false });
  };

  render() {
    const {
      isLoading,
      playerDetail,
      currentStepId,
      currentClassType,
      onMirrorCommunicate,
      isMotionCaptureEnabled,
      updateMirrorStore,
    } = this.props;
    const { isPlay } = this.state;
    return (
      <PlayerView
        isPlaying={isPlay}
        isLoading={isLoading}
        isMotionCaptureEnabled={isMotionCaptureEnabled}
        playerDetail={playerDetail}
        currentStepId={currentStepId}
        currentClassType={currentClassType}
        onPressExit={this.onPressExit}
        onPressControl={this.onPressControl}
        onMirrorCommunicate={onMirrorCommunicate}
        updateMirrorStore={updateMirrorStore}
      />
    );
  }
}

export default connect(
  (state, param) => ({
    currentDuration: state.mirror.currentDuration,
    sceneKey: param.name,
    currentSceneKey: state.appRoute.routeName,
    isLoading: state.appState.isLoading,
    playerDetail: state.player.detail,
    isMotionCaptureEnabled: state.player.isMotionCaptureEnabled,
    eventDetail: state.community.eventDetail,
    currentStepId: state.mirror.currentStepId,
    currentClassType: state.mirror.currentClassType,
    user: state.user,
    playerState: state.mirror.playerState,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        setCurrentStepId: MirrorActions.setCurrentStepId,
        setCurrentPosition: MirrorActions.setCurrentPosition,
        resetCurrentPosition: MirrorActions.resetCurrentPosition,
        finishClass: ClassActions.finishClass,
        pauseClass: ClassActions.pauseClass,
        stashClassStepId: ClassActions.stashClassStepId,
        resetCurrentStep: MirrorActions.resetCurrentStep,
        setActiveEvent: MirrorActions.setActiveEvent,
        setCarouselDuration: MirrorActions.setCarouselDuration,
        updateMirrorStore: MirrorActions.updateMirrorStore,
      },
      dispatch,
    ),
)(PlayerScreen);
