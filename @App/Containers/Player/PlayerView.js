import {
  ActivityIndicator,
  Platform,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {
  BaseIconButton,
  BaseModalSelector,
  BaseNavBar,
  CachedImage as ImageBackground,
  ImageButton,
  StripeView,
} from 'App/Components';
import { Classes, Colors, Fonts, Images, Metrics, Styles } from 'App/Theme';
import {
  activateKeepAwake,
  Date as d,
  deactivateKeepAwake,
  isIphoneX,
  Screen,
} from 'App/Helpers';

import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import PropTypes from 'prop-types';
import React from 'react';
import { padStart } from 'lodash';
import styles from './PlayerViewStyle';
import { translate as t } from 'App/Helpers/I18n';

class PlayerView extends React.Component {
  static propTypes = {
    currentClassType: PropTypes.string.isRequired,
    currentStepId: PropTypes.string,
    playerDetail: PropTypes.object.isRequired,
    isPlaying: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isMotionCaptureEnabled: PropTypes.bool,
    eventDetail: PropTypes.object,
    onPressExit: PropTypes.func.isRequired,
    onPressControl: PropTypes.func.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    updateMirrorStore: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isMotionCaptureEnabled: false,
    eventDetail: null,
    currentStepId: null,
  };

  state = {
    isPlayerButtonPause: true,
    is1on1ModalShow: false,
    isMotionTrackerModalShow: false,
    oneOnOneMode: 'mode-1',
    modeCount: 0,
    oneOnOneOptions: [
      {
        key: 'switch',
        label: t('player_screen_switch_view'),
      },
    ],
    motionTrackerOptions: [
      {
        key: 'large',
        label: t('player_screen_large_view'),
      },
      {
        key: 'normal',
        label: t('player_screen_normal_view'),
      },
      {
        key: 'small',
        label: t('player_screen_small_view'),
      },
    ],
  };

  componentDidMount() {
    activateKeepAwake();

    const { updateMirrorStore, isMotionCaptureEnabled } = this.props;
    updateMirrorStore({
      isWaitingForDeviceStartClass: false,
    });

    setTimeout(
      () => {
        this.setState({ isPlayerButtonPause: false });
      },
      isMotionCaptureEnabled ? 3000 : 0,
    );
  }

  componentWillUnmount() {
    deactivateKeepAwake();
  }

  handleMotionTrackerSizeChanged = (item) => {
    const { onMirrorCommunicate } = this.props;
    onMirrorCommunicate(MirrorEvents.MOTION_TRACKER_VIEW_SWITCH, {
      type: item.key,
    });
  };

  handle1on1ModeChanged = () => {
    const { onMirrorCommunicate } = this.props;
    this.setState(
      (state) => ({
        modeCount: state.modeCount + 1,
      }),
      () => {
        const mode = `mode-${(this.state.modeCount % 2) + 1}`;
        console.log('=== handle1on1ModeChanged mode ===', mode);
        this.setState(
          (state) => ({
            oneOnOneMode: mode,
          }),
          () => {
            onMirrorCommunicate(MirrorEvents.DISPLAY_MODE_SWITCH, {
              type: this.state.oneOnOneMode,
            });
          },
        );
      },
    );
  };

  renderUpperComponent = () => {
    const {
      onPressExit,
      playerDetail: { signedCoverImageObj, title, instructor = {} },
      playerDetail,
      isLoading,
    } = this.props;
    const { trainingSteps = [] } = playerDetail;
    return (
      <ImageBackground
        source={signedCoverImageObj}
        style={[styles.image, isIphoneX() && styles.XImageStyle, Classes.fill]}
      >
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={[styles.image, isIphoneX() && styles.XImageStyle]}
          locations={[0, 0.98]}
          colors={[`${Colors.black0}`, `${Colors.black}`]}
        >
          <BaseNavBar
            statusbarStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}
            style={styles.navBar}
            backIconColor="black"
            leftComponent={
              <ImageButton
                image={Images.back_white}
                style={styles.leftButton}
                imageStyle={styles.img}
                onPress={onPressExit}
                resizeMode="contain"
              />
            }
            rightComponent={
              <View style={[Classes.fill, Classes.crossEnd]}>
                <ImageButton
                  image={Images.setting_white}
                  style={styles.rightButton}
                  imageStyle={styles.img}
                  onPress={Actions.WorkoutOptionModal}
                />
              </View>
            }
          />
          <View style={styles.upperWrapper}>
            <Text style={styles.titleText}>{title}</Text>
            <Text style={styles.secondTitle}>{instructor.title}</Text>
          </View>
        </LinearGradient>

        <View style={styles.playerControlWrapper}>
          {this.renderPlayerControlComponent()}
        </View>

        <View style={styles.panelHeader}>
          <View style={[Classes.row]}>
            <Text
              style={[Fonts.style.regular500, Styles.colorWhite, Classes.marginRight]}
            >
              {trainingSteps.length} {`${t('class_detail_exercise')}`.toLowerCase()}
            </Text>
            <ActivityIndicator animating={isLoading} size="small" color="white" />
          </View>
        </View>
      </ImageBackground>
    );
  };

  renderFooterButtons = () => {
    const { currentClassType, eventDetail } = this.props;
    return (
      <View style={styles.panelFooter}>
        <ImageButton
          image={Images.playScreen_heart_ratio}
          style={styles.footerButton}
          imageStyle={styles.footerIcon}
          onPress={Actions.HeartRateModal}
        />
        <ImageButton
          image={Images.playScreen_voice}
          style={styles.footerButton}
          imageStyle={styles.footerIcon}
          onPress={Actions.AudioModal}
        />
        <ImageButton
          image={Images.playScreen_like}
          style={styles.footerButton}
          imageStyle={styles.footerIcon}
          onPress={Actions.ReactionModal}
        />
        {currentClassType === 'event' &&
        eventDetail &&
        eventDetail.eventType &&
        eventDetail.eventType === 1
          ? this.render1on1Modal()
          : false}
      </View>
    );
  };

  renderMotionTrackerModal = () => {
    const { motionTrackerOptions, isMotionTrackerModalShow } = this.state;
    return (
      <View style={styles.footerButton}>
        <BaseModalSelector
          visible={isMotionTrackerModalShow}
          data={motionTrackerOptions}
          onModalClose={this.handleMotionTrackerSizeChanged}
          cancelStyle={styles.modalSelectorCancelStyle}
          cancelTextStyle={Styles.colorSkyBlue}
        >
          <ImageButton
            image={Images.motion_capture_white}
            imageStyle={styles.footerIcon}
            onPress={() =>
              this.setState({
                isMotionTrackerModalShow: true,
              })
            }
          />
        </BaseModalSelector>
      </View>
    );
  };

  render1on1Modal = () => {
    const { oneOnOneOptions, is1on1ModalShow } = this.state;
    return (
      <View style={styles.footerButton}>
        <BaseModalSelector
          visible={is1on1ModalShow}
          data={oneOnOneOptions}
          onModalClose={this.handle1on1ModeChanged}
          cancelStyle={styles.modalSelectorCancelStyle}
          cancelTextStyle={Styles.colorSkyBlue}
        >
          <ImageButton
            image={Images.icon_switch}
            imageStyle={styles.footerIcon}
            onPress={() =>
              this.setState({
                is1on1ModalShow: true,
              })
            }
          />
        </BaseModalSelector>
      </View>
    );
  };

  renderPlayerControlComponent = () => {
    const { eventDetail, isLoading, isPlaying, onPressControl } = this.props;
    const { isPlayerButtonPause } = this.state;
    return (
      <View style={styles.playerControl} pointerEvents="box-none">
        {__DEV__ && (
          <BaseIconButton
            iconType="FontAwesome5"
            iconName="stop"
            iconSize={32}
            iconColor={Colors.white}
            style={{
              marginRight: Metrics.baseMargin * 4,
            }}
            onPress={() => onPressControl('force-stop')}
          />
        )}
        {(!eventDetail || (eventDetail && eventDetail.eventType !== 1)) && (
          <ImageButton
            image={isPlaying ? Images.playScreen_pause : Images.playScreen_play}
            style={styles.btnPlayAndPause}
            imageStyle={styles.playIcon}
            onPress={() => onPressControl('play')}
            disabled={isPlayerButtonPause || isLoading}
            hitSlop={{
              top: Screen.scale(8),
              bottom: Screen.scale(8),
              left: Screen.scale(8),
              right: Screen.scale(8),
            }}
          />
        )}
      </View>
    );
  };

  renderLowerComponent = () => {
    const { eventDetail = null, playerDetail } = this.props;
    const { trainingSteps = [] } = playerDetail;
    return (
      <View style={styles.bottomWrapper}>
        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 0, y: 0 }}
          style={[Classes.fill, Classes.mainEnd]}
          locations={[0, 0.26]}
          colors={[`${Colors.black_20}`, `${Colors.transparent}`]}
        >
          <ScrollView style={Classes.fill}>
            <StripeView
              style={styles.stripe}
              buttonStyle={styles.stripeBtn}
              stripStyle={styles.stripeStyle}
              stripeLightColor={Colors.black}
              stripeDarkColor={Colors.black}
              stripeDividerColor={Colors.gray_12}
              stripeHighlightColor={Colors.gray_01}
              stripeDividerWidth={1}
              stripeRightStyle={{ flex: 4 }}
              stripeLeftStyle={{ flex: 1 }}
              stripes={this.renderTrainingStep(trainingSteps, eventDetail)}
            />
          </ScrollView>
          {this.renderFooterButtons()}
        </LinearGradient>
      </View>
    );
  };

  renderTrainingStep = (list, eventDetail) =>
    list.map((item, index) => {
      const { currentStepId, onMirrorCommunicate } = this.props;
      const highlight = currentStepId === item.id;
      return {
        leftComponent: (
          <Text
            style={[
              Fonts.style.medium500,
              Styles.colorWhite,
              highlight && Styles.colorGreen,
            ]}
          >
            {padStart(index + 1, 2, '0')}
          </Text>
        ),
        rightComponent: (
          <View style={Classes.rowCross}>
            <View style={[Classes.crossStart, Classes.fill]}>
              <View style={Classes.rowCenter}>
                {/* {!item.captureSetting && (
                  <Image
                    source={Images.motion_capture_white}
                    style={styles.stripeImage}
                  />
                )} */}
                <Text
                  style={[
                    Classes.crossCenter,
                    Fonts.style.medium500,
                    Styles.colorWhite,
                    highlight && Styles.colorGreen,
                  ]}
                >
                  {item.title}
                </Text>
              </View>
            </View>
            <View style={[Classes.crossEnd, Classes.fill]}>
              <Text
                style={[
                  Fonts.size.small500,
                  Styles.colorWhite,
                  highlight && Styles.colorGreen,
                ]}
              >
                {d.mmss(item.duration)}
              </Text>
            </View>
          </View>
        ),
        highlight,
        disabled: eventDetail && eventDetail.eventType === 1,
        onPress:
          !eventDetail || (eventDetail && eventDetail.eventType !== 1)
            ? () =>
                onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                  action: 'goto-step',
                  stepId: item.id,
                })
            : undefined,
      };
    });

  render() {
    return (
      <SafeAreaView style={Classes.fill} pointerEvents="box-none">
        {this.renderUpperComponent()}
        {this.renderLowerComponent()}
      </SafeAreaView>
    );
  }
}

export default PlayerView;
