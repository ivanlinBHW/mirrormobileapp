import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Alert, BackHandler } from 'react-native';

import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { MirrorActions, CommunityActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';

import PlayerView from './PlayerView';

class EventPlayerScreen extends React.Component {
  static propTypes = {
    detail: PropTypes.object.isRequired,
    onMirrorPicturePreview: PropTypes.func.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    currentDuration: PropTypes.number,
    currentStepId: PropTypes.string,
    token: PropTypes.string,
    user: PropTypes.object.isRequired,
    sceneKey: PropTypes.string.isRequired,
    currentClassType: PropTypes.string.isRequired,
    currentSceneKey: PropTypes.string.isRequired,
    routeName: PropTypes.string.isRequired,
    setCurrentStepId: PropTypes.func.isRequired,
    setCurrentPosition: PropTypes.func.isRequired,
    resetCurrentStep: PropTypes.func.isRequired,
    setActiveEvent: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    stashStepEventClass: PropTypes.func.isRequired,
    finishEventClass: PropTypes.func.isRequired,
    pauseEventClass: PropTypes.func.isRequired,
    fetchPut1on1OnlineStatus: PropTypes.func.isRequired,
    updateMirrorStore: PropTypes.func.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
    currentState: PropTypes.string.isRequired,
    eventDetail: PropTypes.object,
    instructor: PropTypes.object,
  };

  static defaultProps = {
    currentDuration: 0,
    instructor: {
      title: '',
    },
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      detail: { trainingSteps = [] },
    } = nextProps;
    trainingSteps.forEach((item, index) => {
      if (item.id === nextProps.currentStepId) {
        return {
          count: index,
        };
      }
    });
    return null;
  }

  state = {
    isPlay: true,
    count: 0,
    isNext: false,
  };

  componentDidMount() {
    console.log('Event Player Screen');
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.onPressExit,
    );

    const {
      detail,
      resetCurrentStep,
      setActiveEvent,
      setCurrentPosition,
      onMirrorPicturePreview,
      setCurrentStepId,
    } = this.props;
    setActiveEvent('play');

    onMirrorPicturePreview(detail.signedCoverImage);

    if (
      _.has(detail, 'trainingEventClassHistory') &&
      !_.isEmpty(detail.trainingEventClassHistory.pausedStepId)
    ) {
      let nowIndex = 0;
      detail.trainingSteps.forEach((item, index) => {
        if (item.id === detail.trainingEventClassHistory.pausedStepId) {
          nowIndex = index;
          this.setState({ count: index });
        }
      });
      let tempClassDuration = 0;
      detail.trainingSteps.forEach((item, index) => {
        if (index < nowIndex) {
          tempClassDuration += item.duration;
        }
      });
      setCurrentPosition(tempClassDuration);
    } else {
      resetCurrentStep();
      setCurrentStepId(detail.trainingSteps[0].id);
      this.setState({
        count: 0,
      });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { sceneKey, currentSceneKey } = this.props;
    return (
      (sceneKey === currentSceneKey ||
        ['AudioModal', 'ReactionModal', 'HeartRateModal', 'WorkoutOptionModal'].includes(
          currentSceneKey,
        )) &&
      (!_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState))
    );
  }

  componentDidUpdate(prevProps) {
    const {
      currentStepId,
      stashStepEventClass,
      detail,
      eventDetail,
      currentState,
      fetchPut1on1OnlineStatus,
    } = this.props;
    const { trainingSteps } = detail;
    if (
      trainingSteps != null &&
      currentStepId !== prevProps.currentStepId &&
      !this.state.isNext
    ) {
      trainingSteps.forEach((item, index) => {
        if (item.id === currentStepId) {
          stashStepEventClass(detail.id, eventDetail.id, currentStepId);
        }
      });
    }
    if (currentState !== prevProps.currentState && typeof currentState === 'string') {
      if (eventDetail.eventType === 1) {
        fetchPut1on1OnlineStatus(
          eventDetail.id,
          currentState === 'active' ? 'readyToGo' : 'offline',
        );
      }
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();

    const { fetchPut1on1OnlineStatus, eventDetail } = this.props;
    if (eventDetail.eventType === 1) {
      fetchPut1on1OnlineStatus(eventDetail.id, 'offline');
    }
  }

  onPressControlPause = () => {
    const { onMirrorCommunicate, setActiveEvent, detail } = this.props;
    setActiveEvent('pause');
    onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
      action: 'pause',
      stepId: detail.trainingSteps[this.state.count].id,
    });
  };

  onPressControlPlay = () => {
    const { onMirrorCommunicate, setActiveEvent, detail } = this.props;
    setActiveEvent('play');
    onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
      action: 'play',
      stepId: detail.trainingSteps[this.state.count].id,
    });
  };

  onPressControl = (type) => {
    const {
      onMirrorCommunicate,
      detail,
      stashStepEventClass,
      setCurrentStepId,
      setCurrentPosition,
      setActiveEvent,
      eventDetail,
      finishEventClass,
      currentDuration,
      updateMirrorStore,
    } = this.props;
    const { isPlay, count } = this.state;
    const { trainingSteps = [] } = detail;
    let tempDuration = 0;
    let nowStepDuration = null;
    trainingSteps.forEach((item, index) => {
      if (this.state.count > index) {
        nowStepDuration += item.duration;
      }
    });
    switch (type) {
      case 'prev':
        if (count > 0) {
          setActiveEvent('play');
          if (this.state.count > 0) {
            if (currentDuration - nowStepDuration > 2) {
              onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                action: 'prev-step',
                stepId: trainingSteps[this.state.count].id,
              });
              setCurrentStepId(trainingSteps[this.state.count].id);
              setCurrentPosition(nowStepDuration);
              updateMirrorStore({ isSameStepChange: true });
            } else {
              this.setState({ count: this.state.count - 1 }, () => {
                onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                  action: 'prev-step',
                  stepId: trainingSteps[this.state.count].id,
                });
                setCurrentStepId(trainingSteps[this.state.count].id);
                trainingSteps.forEach((item, index) => {
                  if (this.state.count > index) {
                    tempDuration += item.duration;
                  }
                });
                setCurrentPosition(tempDuration);
                updateMirrorStore({ isSameStepChange: true });
              });
            }
            stashStepEventClass(detail.id, eventDetail.id, trainingSteps[count].id);
          }
        } else {
          setCurrentPosition(0);
          setCurrentStepId(trainingSteps[0].id);
          onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
            action: 'prev-step',
            stepId: trainingSteps[0].id,
          });
        }
        break;
      case 'play':
        isPlay ? this.onPressControlPause() : this.onPressControlPlay();
        this.setState({ isPlay: !isPlay });
        break;
      case 'next': {
        if (count < trainingSteps.length - 1) {
          setActiveEvent('play');
          this.setState({ count: this.state.count + 1 }, () => {
            onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
              action: 'next-step',
              stepId: trainingSteps[this.state.count].id,
            });
            setCurrentStepId(trainingSteps[this.state.count].id);
            trainingSteps.forEach((item, index) => {
              if (this.state.count > index) {
                tempDuration += item.duration;
              }
            });
            setCurrentPosition(tempDuration);
            updateMirrorStore({ isSameStepChange: true });
          });

          stashStepEventClass(detail.id, eventDetail.id, trainingSteps[count].id);
        } else {
          updateMirrorStore({ isPlayingClass: false });
          if (!this.state.isNext) {
            this.setState({ isNext: true }, () => {
              onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
                action: 'next-step',
              });
              finishEventClass(detail.id, eventDetail.id);
            });
          }
        }
        break;
      }
      case 'force-stop': {
        updateMirrorStore({ isPlayingClass: false });
        if (!this.state.isNext) {
          this.setState({ isNext: true }, () => {
            onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, {
              action: 'next-step',
            });
            finishEventClass(detail.id, eventDetail.id);
          });
        }
        break;
      }
      default:
        break;
    }
  };

  onPressExit = () => {
    Alert.alert(t('player_screen_title'), t('player_screen_content'), [
      {
        text: t('player_screen_cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('player_screen_quit'),
        onPress: this.onPressExitAction,
      },
    ]);
    return true;
  };

  onPressExitAction = () => {
    const {
      detail = {},
      onMirrorCommunicate,
      pauseEventClass,
      eventDetail = {},
      currentStepId,
      updateMirrorStore,
    } = this.props;
    updateMirrorStore({ isPlayingClass: false });
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS, {
      classId: detail.id,
      eventClassHistoryId:
        detail && detail.trainingEventClassHistory
          ? detail.trainingEventClassHistory.id
          : null,
    });
    pauseEventClass(detail.id, eventDetail.id, currentStepId);
  };

  render() {
    const {
      isLoading,
      eventDetail,
      detail: playerDetail,
      currentStepId,
      currentClassType,
      onMirrorCommunicate,
      isMotionCaptureEnabled,
      updateMirrorStore,
    } = this.props;
    const { isPlay } = this.state;
    return (
      <PlayerView
        isPlaying={isPlay}
        isLoading={isLoading}
        isMotionCaptureEnabled={isMotionCaptureEnabled}
        playerDetail={playerDetail}
        eventDetail={eventDetail}
        currentStepId={currentStepId}
        currentClassType={currentClassType}
        onPressExit={this.onPressExit}
        onPressControl={this.onPressControl}
        onMirrorCommunicate={onMirrorCommunicate}
        updateMirrorStore={updateMirrorStore}
      />
    );
  }
}

export default connect(
  (state, param) => ({
    currentState: state.appState.currentState,
    isLoading: state.appState.isLoading,
    sceneKey: param.name,
    detail: state.community.eventClass,
    instructor: state.community.eventClass.instructor,
    currentStepId: state.mirror.currentStepId,
    currentDuration: state.mirror.currentDuration,
    token: state.user.token,
    user: state.user,
    routeName: state.player.routeName,
    currentSceneKey: state.appRoute.routeName,
    eventDetail: state.community.eventDetail,
    isMotionCaptureEnabled: state.player.isMotionCaptureEnabled,
    currentClassType: state.mirror.currentClassType,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onMirrorPicturePreview: MirrorActions.onMirrorPicturePreview,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        setCurrentStepId: MirrorActions.setCurrentStepId,
        setCurrentPosition: MirrorActions.setCurrentPosition,
        resetCurrentStep: MirrorActions.resetCurrentStep,
        setActiveEvent: MirrorActions.setActiveEvent,
        stashStepEventClass: CommunityActions.stashStepEventClass,

        finishEventClass: CommunityActions.finishEventClass,
        pauseEventClass: CommunityActions.pauseEventClass,
        updateMirrorStore: MirrorActions.updateMirrorStore,
        fetchPut1on1OnlineStatus: CommunityActions.fetchPut1on1OnlineStatus,
      },
      dispatch,
    ),
)(EventPlayerScreen);
