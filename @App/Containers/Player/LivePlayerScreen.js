import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Slider from 'react-native-slider';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  SafeAreaView,
  Text,
  BackHandler,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { get } from 'lodash';

import { BaseNavBar, CachedImage as ImageBackground } from 'App/Components';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';
import LiveActions from 'App/Stores/Live/Actions';
import ImageButton from 'App/Components/ImageButton';
import styles from './PlayerScreenStyle';
import { Actions } from 'react-native-router-flux';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d, activateKeepAwake, deactivateKeepAwake } from 'App/Helpers';
import { Images, Colors } from 'App/Theme';

class LivePlayerScreen extends React.Component {
  static propTypes = {
    detail: PropTypes.object.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    updateMirrorStore: PropTypes.func.isRequired,
    finishLiveClass: PropTypes.func.isRequired,
    currentDuration: PropTypes.number,
    currentStepId: PropTypes.string,
    token: PropTypes.string,
    setCountDown: PropTypes.func.isRequired,
    time: PropTypes.string,
    resetCountDown: PropTypes.func.isRequired,
  };

  static defaultProps = {
    currentDuration: 0,
    time: '00:00',
  };

  state = {
    isPlay: true,
    count: 0,
  };

  componentDidMount() {
    const { detail, setCountDown, resetCountDown } = this.props;
    resetCountDown();
    activateKeepAwake();
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.onPressExit,
    );
  }

  componentWillUnmount() {
    deactivateKeepAwake();
    this.backHandler.remove();
  }

  onPressControl = (type) => {
    const { onMirrorCommunicate, updateMirrorStore } = this.props;
    const { isPlay } = this.state;
    switch (type) {
      case 'play':
        isPlay
          ? onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, { action: 'pause' })
          : onMirrorCommunicate(MirrorEvents.PLAYER_CONTROL, { action: 'play' });
        this.setState({ isPlay: !isPlay });
        break;
      case 'next': {
        updateMirrorStore({ isPlayingClass: false });
        requestAnimationFrame(Actions.SaveActivityScreen);
        break;
      }
      default:
        break;
    }
  };

  onPressExit = () => {
    Alert.alert(t('player_screen_title'), t('player_screen_content'), [
      {
        text: t('__cancel'),
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: t('__quit'),
        onPress: this.onPressExitAction,
      },
    ]);
    return true;
  };

  onPressExitAction = () => {
    const { detail, onMirrorCommunicate, exitLiveClass } = this.props;
    let liveClassId = detail.id;
    console.log('=== liveClass detail ===', detail);
    console.log('=== liveClass detail liveClassHistory ===', detail.liveClassHistory);
    let liveClassHistoryId = get(detail, 'liveClassHistory.id');
    console.log('=== liveClassHistoryId ===', liveClassHistoryId);
    exitLiveClass({ liveClassId, liveClassHistoryId });
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS, { classId: detail.id }, Actions.pop);
  };

  renderTrainingStep = (id) => {
    const { detail } = this.props;
    const { count } = this.state;
    return detail.exercises[count].title;
  };

  onPressFinish = () => {
    if (__DEV__) {
      const { onMirrorCommunicate, finishLiveClass, detail } = this.props;
      finishLiveClass(detail.liveClassHistory.id);
      onMirrorCommunicate(MirrorEvents.EXIT_CLASS, { classId: detail.id });
    }
  };

  render() {
    const { detail, currentStepId } = this.props;
    return (
      <SafeAreaView>
        <ImageBackground source={detail.signedCoverImageObj} style={styles.image}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0, y: 1 }}
            style={styles.image}
            locations={[0, 0.98]}
            colors={[`${Colors.black0}`, `${Colors.black}`]}
          >
            <BaseNavBar
              style={styles.navBar}
              backIconColor="black"
              leftComponent={
                <ImageButton
                  image={Images.back}
                  style={styles.leftButton}
                  imageStyle={styles.back}
                  onPress={this.onPressExit}
                />
              }
              rightComponent={
                <ImageButton
                  image={Images.setting}
                  style={styles.rightButton}
                  imageStyle={styles.img}
                  onPress={Actions.WorkoutOptionModal}
                />
              }
            />
            <View style={styles.timeBox}>
              <Text style={styles.titleText}>{detail.title}</Text>
              <Text style={styles.secondTitle}>{detail.instructor.title}</Text>

              <View style={styles.textBox}>
                <Text style={styles.text}>{''}</Text>
                <Text style={styles.introtext}>
                  {this.renderTrainingStep(currentStepId)}
                </Text>
              </View>
              {/* {d.transformDiffSecondTime(detail.scheduledAt) >= 0 && time !== '' ? (
                <View style={styles.countDownBox}>
                  <Text style={styles.introtext}>{`${t(
                    'live_detail_workout_begins_in',
                  )}：${time}`}</Text>
                </View>
              ) : (
                <View style={styles.textBox}>
                  <Text style={styles.text}>{d.transformMinute(currentDuration)}</Text>
                  <Text style={styles.introtext}>
                    {this.renderTrainingStep(currentStepId)}
                  </Text>
                </View>
              )} */}
              <Slider
                style={styles.slider}
                thumbTintColor="transparent"
                minimumTrackTintColor={Colors.secondary}
                maximumTrackTintColor={Colors.secondary}
                onValueChange={() => {}}
              />
            </View>
          </LinearGradient>
        </ImageBackground>
        <View style={styles.box}>
          <TouchableOpacity style={styles.playLiveBox} onPress={this.onPressFinish}>
            <Text style={styles.liveText}>LIVE</Text>
          </TouchableOpacity>
          <View style={styles.flexBox}>
            <ImageButton
              image={Images.playScreen_heart_ratio}
              style={styles.footerButton}
              imageStyle={styles.footerIcon}
              onPress={Actions.HeartRateModal}
            />
            <ImageButton
              image={Images.playScreen_voice}
              style={styles.footerButton}
              imageStyle={styles.footerIcon}
              onPress={() => Actions.AudioModal({ mode: 'livePlayScreen' })}
            />
            <ImageButton
              image={Images.playScreen_like}
              style={styles.footerButton}
              imageStyle={styles.footerIcon}
              onPress={Actions.ReactionModal}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    detail: state.live.detail,
    currentStepId: state.mirror.currentStepId,
    currentDuration: state.mirror.currentDuration,
    currnetAction: state.mirror.currnetAction,
    token: state.user.token,
    time: state.live.time,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateMirrorStore: MirrorActions.updateMirrorStore,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        finishLiveClass: LiveActions.finishLiveClass,
        setCountDown: LiveActions.setCountDown,
        resetCountDown: LiveActions.resetCountDown,
        exitLiveClass: LiveActions.exitLiveClass,
      },
      dispatch,
    ),
)(LivePlayerScreen);
