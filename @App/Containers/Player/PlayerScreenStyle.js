import { ScaledSheet } from 'App/Helpers';
import { Platform } from 'react-native';
import { Styles, Colors, Classes, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  box: {
    backgroundColor: Colors.black,
    paddingTop: Platform.isPad ? Metrics.baseMargin * 4 : 0,
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  img: {
    width: '24@s',
    height: '24@s',
  },
  back: {
    width: '10@s',
    height: '18@s',
  },
  leftButton: {
    width: '44@s',
    height: '44@vs',
    borderWidth: 0,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginLeft: Metrics.baseMargin,
  },
  rightButton: {
    width: '44@s',
    height: '44@vs',
    borderWidth: 0,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginRight: Metrics.baseMargin,
  },
  slider: {
    marginBottom: '31@vs',
    marginHorizontal: Metrics.baseMargin,
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: '32@s',
    paddingLeft: '32@s',
    width: '100%',
    marginBottom: '42@vs',
    paddingTop: Metrics.baseMargin * 2,
  },
  navBar: {
    ...Styles.navBar,
    backgroundColor: Colors.transparent,
    position: 'absolute',
    top: Metrics.baseMargin,
  },
  footerIcon: {
    width: '29@s',
    height: '29@vs',
  },
  footerButton: {
    flex: 1,
    height: '48@vs',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  playBox: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: '52@vs',
    width: '100%',
  },
  XPlayBoxStyle: {
    marginBottom: '35@vs',
  },
  playLiveBox: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '78@s',
    width: '80@s',
    height: '38@vs',
    borderRadius: '5@vs',
    backgroundColor: Colors.button.primary.content.background,
  },
  liveText: {
    fontSize: '24@vs',
    ...Fonts.style.fontWeight500,
    color: Colors.white,
  },
  playIcon: {
    width: '64@s',
    height: '64@vs',
    resizeMode: 'contain',
  },
  middleIcon: {
    width: '64@s',
    height: '64@vs',
  },
  playButton: {
    width: '64@s',
    height: '64@vs',
    borderRadius: '64@vs',
    borderWidth: 0,
  },
  playMiddleButton: {
    width: '64@s',
    height: '64@vs',
    borderRadius: '64@vs',
    borderWidth: 0,
    marginHorizontal: '36@s',
  },
  textBox: {
    ...Styles.container,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginBottom: '16@vs',
  },
  countDownBox: {
    ...Styles.container,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginBottom: '16@vs',
  },
  text: {
    color: 'white',
    fontSize: '24@vs',
    fontWeight: '500',
  },
  introtext: {
    fontSize: Fonts.size.regular,
    color: 'white',
  },
  image: {
    width: '100%',
    height: '478@vs',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  XImageStyle: {
    height: '460@vs',
  },
  titleText: {
    fontSize: '48@vs',
    letterSpacing: 2,
    color: Colors.secondary,
    flexWrap: 'wrap',
    width: '380@s',
    textTransform: 'uppercase',
    ...Fonts.style.fontWeight500,
    ...Styles.container,
  },
  secondTitle: {
    ...Fonts.style.h1_500,
    letterSpacing: 2,
    color: Colors.white,
    textTransform: 'uppercase',
    marginBottom: '40@vs',
    ...Styles.container,
  },
  loadingWrapper: {
    ...Classes.mainCenter,
    marginLeft: Metrics.baseMargin,
  },
});
