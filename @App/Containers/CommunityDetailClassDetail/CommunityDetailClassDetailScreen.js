import React from 'react';
import _ from 'lodash';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { get, isNil } from 'lodash';
import { View, Text, TouchableWithoutFeedback } from 'react-native';

import {
  MirrorActions,
  CommunityActions,
  InstructorActions,
  PlayerActions,
} from 'App/Stores';
import { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import {
  SquareButton,
  RoundLabel,
  CachedImage as Image,
  CachedImage as ImageBackground,
} from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d, Dialog, Subscription } from 'App/Helpers';
import { Images, Colors } from 'App/Theme';
import styles from './CommunityDetailClassDetailScreenStyle';
import { filterMirrorHasFeatureOption } from 'App/Stores/Mirror/Selectors';

import ClassDetailView from '../ClassDetail/ClassDetailView';

class CommunityDetailClassDetail extends React.Component {
  static propTypes = {
    timezone: PropTypes.any.isRequired,
    eventType: PropTypes.number.isRequired,
    isConnected: PropTypes.bool.isRequired,
    isAccepted: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired,
    startEventClass: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
    setInstructorId: PropTypes.func.isRequired,
    routeName: PropTypes.string.isRequired,
    activeTrainingProgramId: PropTypes.string,
    isFinished: PropTypes.bool,
    instructor: PropTypes.object,
    requiredEquipments: PropTypes.array,
    trainingSteps: PropTypes.array,
    resumeEventClass: PropTypes.func.isRequired,
    programHistoryId: PropTypes.string,
    currentRouteName: PropTypes.string,
    prevRoute: PropTypes.string,
    onMirrorCommunicate: PropTypes.func.isRequired,
    startDate: PropTypes.string,
    eventId: PropTypes.string,
    isOnlySee: PropTypes.bool,
    isChooseClass: PropTypes.bool,
    addSelectedClasses: PropTypes.func.isRequired,
    removeSelectedClasses: PropTypes.func.isRequired,
    addStashSelectedClasses: PropTypes.func.isRequired,
    removeStashSelectedClasses: PropTypes.func.isRequired,
    classesSelected: PropTypes.array,
    isSearchCreate: PropTypes.bool,
    isClassSelectable: PropTypes.bool,
    stashClassesSelected: PropTypes.array,
    dueDate: PropTypes.string,
    eventMeetingSession: PropTypes.object,
    mainSubscription: PropTypes.object,

    isLoading: PropTypes.bool.isRequired,
    playerDetail: PropTypes.object.isRequired,
    updatePlayerStore: PropTypes.func.isRequired,
    isMotionCaptureEnabled: PropTypes.bool.isRequired,
    nextShowDistance: PropTypes.bool.isRequired,
    classCanEnableMotionCapture: PropTypes.bool.isRequired,
    isSubscriptionExpired: PropTypes.bool.isRequired,
    hasDistanceAdjustment: PropTypes.bool.isRequired,
    hasPartnerWorkout: PropTypes.bool.isRequired,
    hasCoachVI: PropTypes.bool.isRequired,
    hasCamera: PropTypes.bool.isRequired,
    isResumable: PropTypes.bool.isRequired,
    setCommunityClassBookmark: PropTypes.func.isRequired,
    removeCommunityClassBookmark: PropTypes.func.isRequired,
  };

  static defaultProps = {
    prevRoute: '',
    instructor: {},
    requiredEquipments: [],
    trainingSteps: [],
    isOnlySee: false,
    isChooseClass: false,
    classesSelected: [],
    isSearchCreate: false,
    eventMeetingSession: null,
    isClassSelectable: true,
  };

  componentDidMount() {
    console.log('@CommunityDetailClassDetail entered');
  }

  renderStripesView = (list) =>
    list.map((item) => ({
      leftComponent: <Text style={styles.stripeText}>{item.title}</Text>,
      rightComponent: (
        <View style={styles.stripeRight}>
          {item.captureSetting && (
            <Image source={Images.motion_capture} style={styles.stripeImage} />
          )}
          <Text style={styles.stripeSecondText}>{d.mmss(item.duration)}</Text>
        </View>
      ),
      disabled: true,
    }));

  renderItem = ({ item, index }) => {
    return (
      <View key={index} style={styles.imageBox}>
        <Image
          source={item.signedDeviceCoverImageObj}
          style={styles.equipments}
          resizeMode="contain"
        />
        <Text style={styles.equipmentsText}>{item.title}</Text>
      </View>
    );
  };

  startClass = () => {
    let { isMotionCaptureEnabled } = this.props;
    const {
      data,
      startEventClass,
      eventId,
      nextShowDistance,
      eventType,
      classCanEnableMotionCapture,
    } = this.props;

    if (!classCanEnableMotionCapture) isMotionCaptureEnabled = false;
    if (isMotionCaptureEnabled) {
      if (nextShowDistance) {
        Dialog.showConfirmCameraUsageAlert(() =>
          Dialog.requestDistanceAdjustmentWhenEnableMotionCapture(() =>
            startEventClass(data.id, eventId, null, {
              enableMotionCapture: isMotionCaptureEnabled,
            }),
          ),
        );
      } else {
        Dialog.showConfirmCameraUsageAlert(() =>
          startEventClass(data.id, eventId, null, {
            enableMotionCapture: isMotionCaptureEnabled,
          }),
        );
      }
    } else {
      startEventClass(data.id, eventId, null, {
        enableMotionCapture: isMotionCaptureEnabled,
      });
    }
  };

  resumeClass = () => {
    const {
      data,
      resumeEventClass,
      eventId,
      isMotionCaptureEnabled,
      nextShowDistance,
    } = this.props;

    if (isMotionCaptureEnabled) {
      if (nextShowDistance) {
        Dialog.showConfirmCameraUsageAlert(() =>
          Dialog.requestDistanceAdjustmentWhenEnableMotionCapture(() =>
            resumeEventClass(
              data.id,
              eventId,
              data.trainingEventClassHistory.id,
              data.trainingEventClassHistory.pausedStepId,
              {
                enableMotionCapture: isMotionCaptureEnabled,
              },
            ),
          ),
        );
      } else {
        Dialog.showConfirmCameraUsageAlert(() =>
          resumeEventClass(
            data.id,
            eventId,
            data.trainingEventClassHistory.id,
            data.trainingEventClassHistory.pausedStepId,
            {
              enableMotionCapture: isMotionCaptureEnabled,
            },
          ),
        );
      }
    } else {
      resumeEventClass(
        data.id,
        eventId,
        data.trainingEventClassHistory.id,
        data.trainingEventClassHistory.pausedStepId,
        {
          enableMotionCapture: isMotionCaptureEnabled,
        },
      );
    }
  };

  onPressInstructor = () => {
    const { instructor, setInstructorId, token } = this.props;
    setInstructorId(instructor.id, token);
  };

  renderClassCard = () => {
    const { data } = this.props;
    return (
      <ImageBackground
        resizeMode="cover"
        source={data.signedCoverImageObj}
        style={styles.classImgWidth}
        imageSize="2x"
        imageType="h"
      >
        <View style={styles.classFragment}>
          <View style={styles.classLeftBox}>
            <View style={styles.classLeft}>
              <Text style={styles.classSecondTitle}>{t('__class')}</Text>
              <Text style={styles.classTitle}>
                {_.isString(data.title) && data.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.classRightBox}>
            <View style={styles.classRight}>
              <RoundLabel text={t(`search_lv${data.level}`)} />
              <RoundLabel text={`${Math.round(data.duration / 60)} ${t('min')}`} />
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  };

  renderStartButton = () => {
    const {
      isResumable,
      data,
      isLoading,
      startDate,
      dueDate,
      eventType,
      isAccepted,
      eventMeetingSession,
      onMirrorCommunicate,
      mainSubscription,
      timezone,
      isSubscriptionExpired,
      hasPartnerWorkout,
    } = this.props;

    if (isEmpty(mainSubscription)) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestAccountSubscriptionCodeAlert(this.startClass)}
          text={t('class_detail_request_account_subscription_alert_title')}
        />
      );
    } else if (isSubscriptionExpired) {
      return (
        <SquareButton
          color={Colors.primary}
          onPress={Dialog.requestAccountSubscriptionExpiredAlert(
            this.handlePressStartClass,
          )}
          disabled={isLoading}
          text={t('class_detail_request_account_subscription_expired_title')}
        />
      );
    } else if (!data.isSubscribed) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestClassSubscriptionAlert(this.startClass)}
          text={t('class_detail_request_class_subscription_alert_title')}
        />
      );
    }
    const now = d.transformDate(d.moment(), timezone);
    const localStartDate = d.transformDate(startDate, timezone);
    const localDueDate = d.transformDate(dueDate, timezone);

    if (
      d.isSameOrAfterDate(now, localStartDate) &&
      d.isSameOrAfterDate(localDueDate, now)
    ) {
      if (eventType === 1) {
        const { trainingEventClassHistory } = data;
        if (
          !_.isEmpty(eventMeetingSession) &&
          trainingEventClassHistory &&
          trainingEventClassHistory.status !== 1 &&
          !isResumable
        ) {
          return (
            <TouchableWithoutFeedback
              onPress={Dialog.showDeviceNoPartnerWorkoutAlert}
              pointerEvents={hasPartnerWorkout ? 'box-none' : 'box-only'}
            >
              <View>
                <SquareButton
                  disabled={isLoading || !isAccepted || !hasPartnerWorkout}
                  color={Colors.button.primary.content.background}
                  onPress={() => {
                    Dialog.showConfirmCameraUsageAlert(() =>
                      onMirrorCommunicate(MirrorEvents.PREPARE_1ON1_CLASS, {
                        classId: data.id,
                        eventClassHistoryId: data.trainingEventClassHistory.id,
                      }),
                    );
                  }}
                  text={t('community_1on1_ready')}
                />
              </View>
            </TouchableWithoutFeedback>
          );
        }
      } else if (
        _.has(data, 'trainingEventClassHistory') &&
        !_.isEmpty(data.trainingEventClassHistory) &&
        _.isString(data.trainingEventClassHistory.pausedStepId)
      ) {
        return (
          <SquareButton
            color={Colors.black}
            onPress={this.startClass}
            text={t('class_detail_start_class')}
          />
        );
      } else {
        if (
          data.trainingEventClassHistory &&
          _.isObject(data, 'trainingEventClassHistory') &&
          _.get(data, 'trainingEventClassHistory.status') !== 1
        ) {
          return (
            <SquareButton
              color={Colors.button.primary.content.background}
              onPress={this.startClass}
              text={t('class_detail_start_class')}
            />
          );
        }
      }
    }
  };

  renderResumeButton = () => {
    const {
      isResumable,
      isSubscriptionExpired,
      startDate,
      data,
      dueDate,
      playerDetail,
      mainSubscription,
    } = this.props;

    if (
      isSubscriptionExpired ||
      isEmpty(mainSubscription) ||
      !playerDetail.isSubscribed
    ) {
      return null;
    }

    if (isResumable) {
      return (
        <SquareButton
          onPress={this.resumeClass}
          color={Colors.button.primary.content.background}
          text={t('class_detail_resume_class')}
        />
      );
    }
  };

  renderChooseClass = () => {
    const {
      isClassSelectable,
      classesSelected,
      stashClassesSelected,
      data,
      addSelectedClasses,
      removeSelectedClasses,
      addStashSelectedClasses,
      removeStashSelectedClasses,
      isSearchCreate,
    } = this.props;
    if (isSearchCreate) {
      if (
        stashClassesSelected.length > 0 &&
        stashClassesSelected.some((item) => item.id === data.id)
      ) {
        return (
          <SquareButton
            onPress={() => {
              removeStashSelectedClasses(data);
              Actions.pop();
            }}
            color={Colors.button.primary.content.background}
            text={t('community_remove_class')}
            disabled={!isClassSelectable}
          />
        );
      } else {
        return (
          <SquareButton
            onPress={() => {
              addStashSelectedClasses(data);
              Actions.pop();
            }}
            color={Colors.button.primary.content.background}
            text={t('community_choose_class')}
            disabled={!isClassSelectable}
          />
        );
      }
    }
  };

  onPressBack = () => {
    const { onMirrorCommunicate, data } = this.props;
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS_PREVIEW, {
      classId: data.id,
      classType: 'on-demand',
    });
  };

  onPressBookmark = (id, item) => {
    const { setCommunityClassBookmark, removeCommunityClassBookmark } = this.props;

    if (item.isBookmarked) {
      console.log('class removeClassBookmark');
      removeCommunityClassBookmark(id);
    } else {
      console.log('class setClassBookmark');
      setCommunityClassBookmark(id);
    }
  };

  render() {
    const {
      playerDetail,
      isOnlySee = false,
      isConnected,
      isLoading,
      prevRoute,
      mainSubscription,
      setInstructorId,
      updatePlayerStore,
      isMotionCaptureEnabled,
      eventType,
      isSearchCreate,
      hasDistanceAdjustment,
      hasCoachVI,
      classCanEnableMotionCapture,
    } = this.props;
    return (
      <>
        {__DEV__ && (
          <>
            <Text>
              (DEV) event status:{_.get(playerDetail, 'trainingEventClassHistory.status')}
            </Text>
            <Text>
              classCanEnableMotionCapture:{classCanEnableMotionCapture ? 'y' : 'n'}
            </Text>
          </>
        )}
        <ClassDetailView
          hasDistanceAdjustment={hasDistanceAdjustment}
          hasCoachVI={hasCoachVI}
          setInstructorId={setInstructorId}
          playerDetail={playerDetail}
          isOnlySee={isOnlySee}
          isLoading={isLoading}
          isConnected={isConnected}
          prevRouteName={prevRoute}
          mainSubscription={mainSubscription}
          renderResumeButton={this.renderResumeButton}
          renderStartButton={this.renderStartButton}
          onPressBack={this.onPressBack}
          isMotionCaptureEnabled={isMotionCaptureEnabled}
          onMotionCaptureChanged={updatePlayerStore}
          eventType={eventType}
          hideConnectDeviceButton={isSearchCreate}
          onPressBookmark={this.onPressBookmark}
        />
        {this.renderChooseClass()}
      </>
    );
  }
}

export default connect(
  (state, params) => ({
    playerDetail: state.player.detail,
    classCanEnableMotionCapture: state.player.detail.hasMotionCapture,
    prevRoute: state.appRoute.prevRoute,
    isConnected: state.mirror.isConnected,
    data: state.player.detail,
    instructor: state.player.detail.instructor,
    trainingSteps: state.player.detail.trainingSteps,
    requiredEquipments: state.player.detail.requiredEquipments,
    token: state.user.token,
    routeName: state.player.routeName,
    currentRouteName: params.name,
    activeTrainingProgramId: state.user.activeTrainingProgramId,
    programHistoryId: state.user.activeTrainingProgramHistoryId,
    trainingProgramClassHistoryId: state.user.trainingProgramClassHistoryId,
    eventMeetingSession: state.community.eventDetail.meetingSession,
    startDate: state.community.eventDetail.startDate,
    dueDate: state.community.eventDetail.dueDate,
    eventId: state.community.eventDetail.id,
    eventType: state.community.eventDetail.eventType,
    classesSelected: state.community.classesSelected,
    stashClassesSelected: state.community.stashClassesSelected,
    mainSubscription: state.user.mainSubscription,
    isLoading: state.appState.isLoading,
    timezone: state.appState.currentTimeZone,
    mirrorOptionMotionCapture: state.mirror.options[MirrorOptions.MOTION_TRACKER],
    isMotionCaptureEnabled:
      'isMotionCaptureEnabled' in state.player
        ? state.player.isMotionCaptureEnabled
        : false,
    nextShowDistance: state.user.isNextShowDistance,

    isSubscriptionExpired: Subscription.isExpired(
      state.user.mainSubscription,
      state.player.detail,
    ),
    hasDistanceAdjustment: filterMirrorHasFeatureOption(state, 'distanceAdjustment'),
    hasPartnerWorkout: filterMirrorHasFeatureOption(state, 'partnerWorkout'),
    hasCoachVI: filterMirrorHasFeatureOption(state, 'coachVI'),
    hasCamera: filterMirrorHasFeatureOption(state, 'mirrorCamera'),
    isResumable:
      (d.moment().isSameOrAfter(state.community.eventDetail.startDate) ||
        d.moment().isSameOrBefore(state.community.eventDetail.dueDate)) &&
      _.has(state.player.detail, 'trainingEventClassHistory') &&
      !_.isEmpty(state.player.detail.trainingEventClassHistory) &&
      _.isString(state.player.detail.trainingEventClassHistory.pausedStepId),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        setCommunityClassBookmark: CommunityActions.setCommunityClassBookmark,
        removeCommunityClassBookmark: CommunityActions.removeCommunityClassBookmark,
        startEventClass: CommunityActions.startEventClass,
        resumeEventClass: CommunityActions.resumeEventClass,
        setInstructorId: InstructorActions.setInstructorId,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        addSelectedClasses: CommunityActions.addSelectedClasses,
        removeSelectedClasses: CommunityActions.removeSelectedClasses,
        addStashSelectedClasses: CommunityActions.addStashSelectedClasses,
        removeStashSelectedClasses: CommunityActions.removeStashSelectedClasses,
        updatePlayerStore: PlayerActions.updatePlayerStore,
      },
      dispatch,
    ),
)(CommunityDetailClassDetail);
