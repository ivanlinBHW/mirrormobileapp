import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Text, Image } from 'react-native-elements';

import { translate as t } from 'App/Helpers/I18n';
import { Images } from 'App/Theme';
import styles from './IntroScreenStyle';

class Step1 extends React.Component {
  static propTypes = {
    appRoute: PropTypes.object.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@Enter Step1!');
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={Images.intro_step1_yoga} style={styles.introImg} />
        <Text style={styles.introTitle}>{t('intro_step1_title')}</Text>
        <View style={styles.introStep5Content} />
      </View>
    );
  }
}

export default connect(
  (state) => ({
    appRoute: state.appRoute,
  }),
  (dispatch) => ({}),
)(Step1);
