import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { TabView, SceneMap } from 'react-native-tab-view';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Metrics, Colors, Fonts, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Screen, ScaledSheet } from 'App/Helpers';
import { BaseButton } from 'App/Components';

import IntroStep1 from './IntroScreenStep1';
import IntroStep2 from './IntroScreenStep2';
import IntroStep3 from './IntroScreenStep3';
import IntroStep4 from './IntroScreenStep4';
import IntroStep5 from './IntroScreenStep5';

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
  tabBar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tabItem: {
    alignItems: 'center',
    padding: '9@s',
  },
  navBar: {
    minHeight: 46,
  },
  wrapper: {
    backgroundColor: Colors.white_02,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: '34@vs',
  },
  btn: {
    width: '83@s',
    height: '32@vs',
  },
  btnText: {
    ...Fonts.style.medium500,
  },
  nextStyle: {
    ...Fonts.style.medium500,
    color: Colors.white,
  },
  nextImg: {
    width: '8@s',
    height: '15@vs',
    marginLeft: '13@s',
  },
  nextBox: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
class IntroScreenTab extends React.Component {
  static propTypes = {
    appRoute: PropTypes.object.isRequired,
  };

  state = {
    index: 0,
    routes: [
      { key: 'IntroStep1', title: 'IntroStep1' },
      { key: 'IntroStep2', title: 'IntroStep2' },
      { key: 'IntroStep3', title: 'IntroStep3' },
      { key: 'IntroStep4', title: 'IntroStep4' },
      { key: 'IntroStep5', title: 'IntroStep5' },
    ],
  };

  renderTabBar = (props) => {
    const {
      navigationState: { index: currentIndex },
    } = props;
    const { index } = this.state;
    return (
      <View style={styles.wrapper}>
        {index !== 4 ? (
          <BaseButton
            transparent
            textStyle={styles.btnText}
            textColor={Colors.button.primary.text.text}
            style={styles.btn}
            onPress={Actions.UserLoginScreen}
            testID="btn_skip"
            throttleTime={0}
            accessible={true}
            accessibilityLabel={'btn_skip'}
          >
            <View style={styles.nextBox}>
              <Text
                style={[styles.nextStyle, { color: Colors.button.primary.text.text }]}
                testID="txt_skip"
                accessible={true}
                accessibilityLabel={'txt_skip'}
              >
                {t('__skip')}
              </Text>
            </View>
          </BaseButton>
        ) : (
          <View style={styles.btn} />
        )}

        <View style={styles.tabBar}>
          {props.navigationState.routes.map((route, i) => {
            return currentIndex === i ? (
              <Icon style={styles.tabItem} name="circle" key={i} color="#333" regular />
            ) : (
              <Icon style={styles.tabItem} name="circle" key={i} color="#DDD" solid />
            );
          })}
        </View>
        {index !== 4 ? (
          <BaseButton
            style={styles.btn}
            color={Colors.button.primary.text.text}
            throttleTime={0}
            onPress={this.onPressNextStep}
          >
            <View style={styles.nextBox}>
              <Text style={styles.nextStyle}>{t('__next')}</Text>
              {/* <Image
                source={Images.next_white}
                style={styles.nextImg}
                resizeMode="contain"
              /> */}
            </View>
          </BaseButton>
        ) : (
          <View style={styles.btn} />
        )}
      </View>
    );
  };

  onPressNextStep = () => {
    const { index } = this.state;
    this.setState({ index: index + 1 });
  };

  onPressUserLoginScreen = () => {
    Actions.UserLoginScreen();
  };

  renderRightComponent = () => {
    const { index } = this.state;
    if (index <= 1) {
      return (
        <Button
          style={styles.text}
          onPress={this.onPressUserLoginScreen}
          title="Skip"
          type="clear"
        />
      );
    } else {
      return null;
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TabView
          navigationState={this.state}
          renderScene={SceneMap({
            IntroStep1,
            IntroStep2,
            IntroStep3,
            IntroStep4,
            IntroStep5,
          })}
          onIndexChange={(index) => this.setState({ index })}
          initialLayout={{
            width: Screen.width,
          }}
          renderTabBar={this.renderTabBar}
          tabBarPosition="bottom"
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    appRoute: state.appRoute,
  }),
  (dispatch) => ({}),
)(IntroScreenTab);
