import React from 'react';
import { View } from 'react-native';
import { Text, Image } from 'react-native-elements';

import { translate as t } from 'App/Helpers/I18n';
import { Images } from 'App/Theme';
import styles from './IntroScreenStyle';

export default class Step4 extends React.Component {
  componentDidMount() {
    __DEV__ && console.log('@Enter Step4!');
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={Images.intro_step4_motion_capture} style={styles.introImg} />
        <Text style={styles.introTitle}>{t('intro_step4_title')}</Text>
        <View style={styles.introStep5Content} />
      </View>
    );
  }
}
