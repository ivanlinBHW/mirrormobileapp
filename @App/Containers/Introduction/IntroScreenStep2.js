import React from 'react';
import { View } from 'react-native';
import { Text, Image } from 'react-native-elements';

import { translate as t } from 'App/Helpers/I18n';
import { Images } from 'App/Theme';
import styles from './IntroScreenStyle';

export default class Step2 extends React.Component {
  componentDidMount() {
    __DEV__ && console.log('@Enter Step2!');
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={Images.intro_step2_members} style={styles.introImg} />
        <Text style={styles.introTitle}>{t('intro_step2_title')}</Text>
        <View style={styles.introStep5Content} />
      </View>
    );
  }
}
