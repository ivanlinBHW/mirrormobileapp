import React from 'react';
import { View } from 'react-native';
import { Text, Image } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import { Images } from 'App/Theme';
import { SquareButton } from 'App/Components';
import styles from './IntroScreenStyle';

export default class Step5 extends React.Component {
  componentDidMount() {
    __DEV__ && console.log('@Enter Step5!');
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <Image source={Images.intro_step5_weather} style={styles.introImg} />
        <Text style={styles.introTitle}>{t('intro_step5_title')}</Text>
        <View style={styles.introStep5Content} />
        <SquareButton
          text={t('intro_step5_get_start')}
          radius={0}
          style={styles.startBtn}
          onPress={Actions.UserLoginScreen}
        />
      </View>
    );
  }
}
