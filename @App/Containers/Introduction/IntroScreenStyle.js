import { ScaledSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default ScaledSheet.create({
  wrapper: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    paddingTop: '90@s',
    backgroundColor: Colors.white_02,
  },
  introImg: {
    width: '182@s',
    height: '182@s',
    marginBottom: '40@s',
  },
  introTitle: {
    fontSize: Fonts.size.h1,
    fontWeight: 'bold',
    marginBottom: Metrics.baseMargin,
    textAlign: 'center',
  },
  introContent: {
    ...Fonts.style.regular500,
    color: Colors.gray_02,
    textAlign: 'center',
    marginBottom: '126@vs',
  },
  introStep5Content: {
    marginBottom: '66@vs',
  },
  startBtn: {
    marginBottom: Metrics.baseMargin,
  },
});
