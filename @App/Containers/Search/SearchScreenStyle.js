import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Styles, Colors, Metrics, Fonts, Classes } from 'App/Theme';

export default ScaledSheet.create({
  title: {
    ...Fonts.style.medium500,
    color: '#323232',
  },
  hrLine: {
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
    borderBottomWidth: 1,
    marginTop: Metrics.baseMargin,
  },
  titleBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  layout: {
    ...Styles.screenPaddingLeft,
  },
  navBar: {
    ...Styles.navBar,
  },
  searchImg: {
    width: '20@s',
    height: '16@vs',
  },
  btnStyle: {
    backgroundColor: 'transparent',
  },
  rightStyle: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  textStyle: {
    fontSize: Fonts.size.regular,
    color: Colors.primary,
    textAlign: 'right',
  },
  wrapper: {
    marginHorizontal: Metrics.baseMargin,
    marginBottom: '40@vs',
    zIndex: 100,
  },
  leftIconContainerStyle: {
    marginLeft: 0,
    position: 'relative',
  },
  inputStyle: {
    height: '22@vs',
    marginTop: Platform.select({
      ios: -Metrics.baseMargin / 2,
      android: '8@s',
    }),
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainerStyle: {
    backgroundColor: Colors.gray_12,
    height: '36@vs',
    borderRadius: '10@vs',
    width: '100%',

    justifyContent: 'center',
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: Colors.black_15,
    marginBottom: Metrics.baseMargin,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  box: {},
  title: {
    ...Fonts.style.small500,
    marginLeft: Metrics.baseMargin,
  },
  titleBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: Metrics.baseMargin,
  },
  instructorBox: {
    width: Platform.isPad ? '98@s' : '83@s',
    height: '87@vs',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: 'transparent',
    marginLeft: Metrics.baseMargin,
    marginTop: Platform.isPad ? Metrics.baseMargin * 2 : Metrics.baseMargin * 1.25,
    padding: Metrics.baseMargin / 2,
    borderWidth: 0,
  },
  avatar: {
    width: '49@s',
    height: '49@s',
    borderRadius: '24.5@s',
    marginBottom: Metrics.baseMargin / 2,
  },
  instructorText: {
    fontSize: Fonts.size.small,
    width: '83@s',
    textAlign: 'center',
  },
  activeText: {
    color: Colors.titleText.active,
  },
  rightText: {
    color: Colors.button.primary.text.text,
  },
  scrollBox: {
    flex: 1,
    position: 'relative',
  },
  container: {
    ...Classes.fill,
  },
  imgStyle: {
    width: '19@s',
    height: '32@s',
  },
  saveText: {
    fontSize: Fonts.size.regular,
    color: Colors.button.primary.text.text,
  },
  listContainerStyle: {
    top: '36@vs',
    position: 'absolute',
  },
  inputItemsContainerStyle: {
    maxHeight: Screen.height / 2,
    width: Screen.width,
    top: Metrics.baseMargin / 4,
    left: -Metrics.baseMargin,
    backgroundColor: Colors.white,
    zIndex: 10,
  },
  inputItemsTextStyle: {
    color: Colors.black,
    marginLeft: Metrics.baseMargin / 2,
  },
  footerShadowStyle: {
    height: Metrics.baseMargin / 2,
    width: Screen.width,
    left: -Metrics.baseMargin,
    bottom: -10,
    zIndex: 100,
    position: 'absolute',
  },
  inputItemsStyle: {
    padding: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin * 1.5,
  },
});
