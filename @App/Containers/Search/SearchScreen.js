import {
  BaseAvatar,
  BaseButton,
  BaseIcon,
  BaseInput,
  BaseSearchButton,
  DismissKeyboard,
  CachedImage as Image,
  SecondaryNavbar,
  SquareButton,
  SearchableInput,
} from 'App/Components';
import {
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { SearchActions, SettingActions } from 'App/Stores';
import { isArray, isEmpty, get } from 'lodash';

import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import { Images } from 'App/Theme';
import PropTypes from 'prop-types';
import React from 'react';
import { RoundButton } from '@ublocks-react-native/component';
import { SeeAllActions } from 'App/Stores';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './SearchScreenStyle';
import { translate as t } from 'App/Helpers/I18n';

import { Colors, Classes } from 'App/Theme';
import { Screen } from 'App/Helpers';

const initialState = {
  text: '',
  isCompleted: false,
  isBookmarked: false,
  isMotionCapture: false,
  noEquipment: false,
  duration: [],
  difficulty: [],
  channelList: [],
  equipmentList: [],
  popularTagList: [],
  instructorList: [],
  workoutTypeList: [],
  genreList: [
    {
      img: 'expertise_0',
      activeImg: 'expertise_0_active',
      text: 'search_boxing',
      key: 'boxing',
      selected: false,
    },
    {
      img: 'expertise_1',
      activeImg: 'expertise_1_active',
      text: 'search_cardio',
      key: 'cardio',
      selected: false,
    },
    {
      img: 'expertise_2',
      activeImg: 'expertise_2_active',
      text: 'search_strength',
      key: 'strength',
      selected: false,
    },
    {
      img: 'expertise_3',
      activeImg: 'expertise_3_active',
      text: 'search_stretching',
      key: 'stretching',
      selected: false,
    },
    {
      img: 'expertise_4',
      activeImg: 'expertise_4_active',
      text: 'search_yoga',
      key: 'yoga',
      selected: false,
    },
  ],
  difficultyList: [
    {
      text: 'search_lv1',
      selected: false,
      key: 1,
    },
    {
      text: 'search_lv2',
      selected: false,
      key: 2,
    },
    {
      text: 'search_lv3',
      selected: false,
      key: 3,
    },
    {
      text: 'search_lv4',
      selected: false,
      key: 4,
    },
  ],
  durationList: [
    {
      text: 'search_duration_15',
      selected: false,
      key: [0, 900],
    },
    {
      text: 'search_duration_30',
      selected: false,
      key: [901, 1800],
    },
    {
      text: 'search_duration_45',
      selected: false,
      key: [1801, 2700],
    },
    {
      text: 'search_duration_60',
      selected: false,
      key: [2701, 3600],
    },
  ],
  showAll: false,
};

const stateArrayUpdater = (props, key) => {
  return isArray(props)
    ? props.map((e) => ({
        ...e,
        selected: false,
      }))
    : [];
};
class SearchScreen extends React.Component {
  static propTypes = {
    getInstructorList: PropTypes.func.isRequired,
    getEquipmentList: PropTypes.func.isRequired,
    getWorkoutTypeList: PropTypes.func.isRequired,
    getSearchableData: PropTypes.func.isRequired,
    search: PropTypes.func.isRequired,
    token: PropTypes.string,
    genres: PropTypes.array.isRequired,
    channelList: PropTypes.array,
    equipmentList: PropTypes.array,
    popularTagList: PropTypes.array,
    instructorList: PropTypes.array,
    workoutTypeList: PropTypes.array,
    pageSize: PropTypes.number,
    resetList: PropTypes.func.isRequired,
    searchHistory: PropTypes.array,
    hotKeywords: PropTypes.array,
    getSearchHotKeyword: PropTypes.func.isRequired,
  };

  static defaultProps = {
    searchHistory: [],
    hotKeywords: [],
    channelList: [],
    equipmentList: [],
    popularTagList: [],
    instructorList: [],
    workoutTypeList: [],
    pageSize: 10,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const stateUpdater = (key) => ({
      [key]: isArray(nextProps[key])
        ? nextProps[key].map((e) => ({
            ...e,
            selected: false,
          }))
        : [],
    });
    if (nextProps.workoutTypeList.length !== prevState.workoutTypeList.length) {
      return stateUpdater('workoutTypeList');
    }
    if (nextProps.equipmentList.length !== prevState.equipmentList.length) {
      return stateUpdater('equipmentList');
    }
    if (nextProps.popularTagList.length !== prevState.popularTagList.length) {
      return stateUpdater('popularTagList');
    }
    if (nextProps.channelList.length !== prevState.channelList.length) {
      return stateUpdater('channelList');
    }
    if (nextProps.instructorList.length !== prevState.instructorList.length) {
      return stateUpdater('instructorList');
    }
    return null;
  }

  constructor(props) {
    super(props);

    this.state = {
      ...JSON.parse(JSON.stringify(initialState)),
      channelList: stateArrayUpdater(props.channelList),
      equipmentList: stateArrayUpdater(props.equipmentList),
      popularTagList: stateArrayUpdater(props.popularTagList),
      instructorList: stateArrayUpdater(props.instructorList),
      workoutTypeList: stateArrayUpdater(props.workoutTypeList),

      isInputFocus: false,
      inputSelectableItemCount: 0,
    };
  }

  componentDidMount() {
    __DEV__ && console.log('@SearchScreen');
    const {
      getSearchableData,
      channelList,
      popularTagList,
      equipmentList,
      instructorList,
      workoutTypeList,
      getSearchHotKeyword,
    } = this.props;
    if (
      isEmpty(channelList) ||
      isEmpty(popularTagList) ||
      isEmpty(equipmentList) ||
      isEmpty(instructorList) ||
      isEmpty(workoutTypeList)
    ) {
      getSearchableData();
    }
    getSearchHotKeyword();
  }

  onPressClear = () => {
    const {
      channelList,
      popularTagList,
      equipmentList,
      instructorList,
      workoutTypeList,
    } = this.props;
    this.setState({
      ...JSON.parse(JSON.stringify(initialState)),
      channelList: channelList,
      equipmentList: equipmentList,
      popularTagList: popularTagList,
      instructorList: instructorList,
      workoutTypeList: workoutTypeList,
    });
  };

  onPressInstructor = (id) => {
    const { instructorList = [] } = this.state;
    this.setState({
      instructorList: instructorList.map((e) => ({
        ...e,
        selected: e.id === id ? !e.selected : e.selected,
      })),
    });
  };

  onPressEquipment = (id) => {
    const { equipmentList = [] } = this.state;
    this.setState({
      equipmentList: equipmentList.map((e) => ({
        ...e,
        selected: e.id === id ? !e.selected : e.selected,
      })),
    });
  };

  onPressGenre = (id) => {
    const { workoutTypeList = [] } = this.state;
    this.setState({
      workoutTypeList: workoutTypeList.map((e) => ({
        ...e,
        selected: e.id === id ? !e.selected : e.selected,
      })),
    });
  };

  onPressPopularTag = (tagId) => {
    const { popularTagList = [] } = this.state;
    this.setState({
      popularTagList: popularTagList.map((e) => ({
        ...e,
        selected: e.tagId === tagId ? !e.selected : e.selected,
      })),
    });
    console.log('press popularTagList===', popularTagList);
  };

  onPressChannel = (id) => {
    const { channelList = [] } = this.state;
    this.setState({
      channelList: channelList.map((e) => ({
        ...e,
        selected: e.id === id ? !e.selected : e.selected,
      })),
    });
  };

  onPressLevel = (key) => {
    const { difficultyList } = this.state;
    let temp = difficultyList;
    temp.forEach((item) => {
      if (item.key === key) {
        item.selected = !item.selected;
      }
    });
    this.setState({
      difficultyList: temp,
    });
  };

  onPressDuration = (key) => {
    const { durationList } = this.state;
    let temp = durationList;
    temp.forEach((item) => {
      if (item.key === key) {
        item.selected = !item.selected;
      }
    });
    this.setState({
      durationList: temp,
    });
  };

  onPressFilter = () => {
    const { search, pageSize, resetList } = this.props;
    const {
      isMotionCapture,
      isBookmarked,
      isCompleted,
      channelList = [],
      equipmentList = [],
      popularTagList = [],
      instructorList = [],
      workoutTypeList = [],
      durationList,
      difficultyList,
      noEquipment,
      text,
    } = this.state;
    resetList();
    const tempEquipment = equipmentList.filter((e) => e.selected).map((e) => e.id);
    if (noEquipment) {
      tempEquipment.push('no-equipment');
    }
    const payload = {
      text,
      isCompleted,
      isBookmarked,
      isMotionCapture,
      equipments: tempEquipment,
      channel: channelList.filter((e) => e.selected).map((e) => e.id),
      genre: workoutTypeList.filter((e) => e.selected).map((e) => e.id),
      tags: popularTagList.filter((e) => e.selected).map((e) => e.name),
      level: difficultyList.filter((e) => e.selected).map((e) => e.key),
      duration: durationList.filter((e) => e.selected).map((e) => e.key),
      instructors: instructorList.filter((e) => e.selected).map((e) => e.id),
    };
    console.log('search payload ->', payload);
    search(payload, {
      page: 1,
      pageSize: 10,
      filter: [],
      type: 'page',
    });
    Actions.SeeAllScreen({ loginRoute: 'search' });
  };

  renderNavbar = () => (
    <SecondaryNavbar
      navTitle={t('search_title')}
      back
      navRightComponent={
        <BaseButton
          transparent
          throttleTime={0}
          style={styles.btnStyle}
          onPress={this.onPressClear}
        >
          <Text style={styles.saveText}>{t('search_clear_all')}</Text>
        </BaseButton>
      }
    />
  );

  selectableItemRender = (item, i) => {
    return (
      <View style={Classes.rowCross}>
        <BaseIcon
          color={Colors.primary}
          name={item.type === 'history' ? 'clock' : 'search'}
          size={Screen.scale(16)}
        />
        <Text style={styles.inputItemsTextStyle}>{item.name}</Text>
      </View>
    );
  };

  renderSearchInput = () => {
    const { searchHistory = [], hotKeywords = [] } = this.props;
    const { text } = this.state;
    const filterItems = searchHistory
      .map((e) => ({
        id: e,
        name: e,
        type: 'history',
      }))
      .concat(
        (hotKeywords || []).map((e) => ({
          id: e,
          name: e,
          type: 'hot',
        })),
      );
    return (
      <View style={styles.wrapper}>
        <SearchableInput
          textInputComponent={BaseInput}
          textInputProps={{
            placeholder: t('search_title'),
            underlineColorAndroid: 'transparent',
            inputStyle: styles.inputStyle,
            onChangeText: (value) => this.setState({ text: value }),
            leftComponent: <Image source={Images.search_gray} style={styles.searchImg} />,
            leftIconContainerStyle: styles.leftIconContainerStyle,
            returnKeyType: 'search',
            defaultValue: text,
            ref: (ref) => (this.input = ref),
            unControllered: true,
          }}
          onFocusChanges={(isFocus) => {
            this.setState({
              isInputFocus: isFocus,
            });
          }}
          onListItemsChanges={(items) => {
            this.setState({
              inputSelectableItemCount: items.length,
            });
          }}
          onItemSelect={(item) => {
            console.log('onItemSelect item=>', item);
            this.setState({ text: item.name });

            console.log('this.input=>', this.input);
            if (this.input) {
              this.input.setValue(item.name);
            }
          }}
          onRemoveItem={(item, index) => {
            console.log('onRemoveItem=>', item, index);
            this.setState({ text: '' });
          }}
          listContainerStyle={styles.listContainerStyle}
          listProps={{
            nestedScrollEnabled: true,
          }}
          renderListFooter={() =>
            this.state.inputSelectableItemCount > 0 && (
              <LinearGradient
                style={styles.footerShadowStyle}
                colors={[Colors.shadow, Colors.transparent]}
              />
            )
          }
          items={filterItems}
          itemRender={this.selectableItemRender}
          itemStyle={styles.inputItemsStyle}
          itemTextStyle={styles.inputItemsTextStyle}
          itemsContainerStyle={styles.inputItemsContainerStyle}
          inputContainerStyle={styles.inputContainerStyle}
          containerStyle={styles.inputContainerStyle}
        />
      </View>
    );
  };

  renderFirst = () => {
    const { isCompleted, isBookmarked, isMotionCapture } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_filter')}</Text>
        <View style={styles.flexBox}>
          <BaseSearchButton
            image={isCompleted ? Images.finsih_white : Images.finsih_black}
            text={t('search_completed')}
            isSelected={isCompleted}
            onPress={() => this.setState({ isCompleted: !isCompleted })}
          />
          <BaseSearchButton
            image={isBookmarked ? Images.bookmark : Images.bookmark_black}
            text={t('search_bookmarked')}
            isSelected={isBookmarked}
            imageStyle={styles.imgStyle}
            onPress={() => this.setState({ isBookmarked: !isBookmarked })}
          />
          {__DEV__ && (
            <BaseSearchButton
              image={
                isMotionCapture ? Images.motion_capture_white : Images.motion_capture
              }
              text={`DEV:${t('motion_capture')}`}
              isSelected={isMotionCapture}
              onPress={() => this.setState({ isMotionCapture: !isMotionCapture })}
            />
          )}
        </View>
      </View>
    );
  };

  renderPopularTags = () => {
    const { popularTagList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_popular_tags')}</Text>
        <View style={styles.flexBox}>
          {isArray(popularTagList) &&
            popularTagList.map((item, index) => {
              return (
                <BaseSearchButton
                  key={item.tagId}
                  size="normal"
                  text={item.name}
                  isSelected={item.selected}
                  onPress={() => this.onPressPopularTag(item.tagId)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderChannel = () => {
    const { channelList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_channel')}</Text>
        <View style={styles.flexBox}>
          {isArray(channelList) &&
            channelList.map((item) => {
              return (
                <BaseSearchButton
                  size="normal"
                  text={item.title}
                  isSelected={item.selected}
                  key={item.id}
                  onPress={() => this.onPressChannel(item.id)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderGenre = () => {
    const { workoutTypeList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_workout_type')}</Text>
        <View style={styles.flexBox}>
          {isArray(workoutTypeList) &&
            workoutTypeList.map((item) => {
              return (
                <BaseSearchButton
                  image={{ uri: item.selected ? item.coverImage : item.deviceCoverImage }}
                  text={item.title.toLowerCase()}
                  isSelected={item.selected}
                  key={item.id}
                  onPress={() => this.onPressGenre(item.id)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderDifficulty = () => {
    const { difficultyList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_difficulty')}</Text>
        <View style={styles.flexBox}>
          {isArray(difficultyList) &&
            difficultyList.map((item, index) => {
              return (
                <BaseSearchButton
                  size="normal"
                  text={t(item.text)}
                  isSelected={item.selected}
                  key={index}
                  onPress={() => this.onPressLevel(item.key)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderDuration = () => {
    const { durationList } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_duration')}</Text>
        <View style={styles.flexBox}>
          {isArray(durationList) &&
            durationList.map((item, index) => {
              return (
                <BaseSearchButton
                  size="normal"
                  text={t(item.text)}
                  isSelected={item.selected}
                  key={index}
                  onPress={() => this.onPressDuration(item.key)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  renderInstructor = () => {
    const { instructorList, showAll } = this.state;
    return (
      <View style={styles.box}>
        <View style={styles.titleBox}>
          <Text style={styles.title}>{t('search_instructor')}</Text>
          <TouchableOpacity onPress={() => this.setState({ showAll: !showAll })}>
            <Text style={styles.rightText}>
              {t(showAll ? 'search_viewless' : 'search_viewall')}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.flexBox}>
          {isArray(instructorList) &&
            instructorList.map((item, index) => {
              return (
                (index < 8 || showAll) && (
                  <RoundButton
                    onPress={() => this.onPressInstructor(item.id)}
                    key={index}
                    style={styles.instructorBox}
                    throttleTime={0}
                  >
                    <BaseAvatar
                      active={item.selected}
                      size={49}
                      uri={item.signedAvatarImageObj}
                      style={styles.avatar}
                      cache
                    />
                    <Text
                      style={[styles.instructorText, item.selected && styles.activeText]}
                    >
                      {item.title}
                    </Text>
                  </RoundButton>
                )
              );
            })}
        </View>
      </View>
    );
  };

  renderEquipments = () => {
    const { equipmentList, noEquipment } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.title}>{t('search_equipment')}</Text>
        <View style={styles.flexBox}>
          <BaseSearchButton
            image={noEquipment ? Images.no_equipment_white_1 : Images.no_equipment_black}
            text={t('search_no_equipment')}
            isSelected={noEquipment}
            onPress={() => this.setState({ noEquipment: !noEquipment })}
          />
          {isArray(equipmentList) &&
            equipmentList.map((item) => {
              const image = {
                uri: item.selected ? item.coverImage : item.deviceCoverImage,
              };
              return (
                <BaseSearchButton
                  key={item.id}
                  image={image}
                  text={item.title}
                  isSelected={item.selected}
                  onPress={() => this.onPressEquipment(item.id)}
                />
              );
            })}
        </View>
      </View>
    );
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.container}>
        {this.renderNavbar()}

        {this.renderSearchInput()}
        <View style={styles.scrollBox}>
          <ScrollView style={styles.scrollBox} keyboardShouldPersistTaps="handled">
            <DismissKeyboard>
              <>
                {this.renderPopularTags()}
                <View style={styles.line} />
                {this.renderChannel()}
                <View style={styles.line} />
                {this.renderFirst()}
                <View style={styles.line} />
                {this.renderGenre()}
                <View style={styles.line} />
                {this.renderInstructor()}
                <View style={styles.line} />
                {this.renderEquipments()}
                <View style={styles.line} />
                {this.renderDifficulty()}
                <View style={styles.line} />
                {this.renderDuration()}
              </>
            </DismissKeyboard>
          </ScrollView>
        </View>
        <View style={styles.applyBtn}>
          <SquareButton text={t('search_apply')} onPress={this.onPressFilter} />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  (state) => ({
    token: state.user.token,
    genres: state.setting.genres,
    searchHistory: state.search.searchHistory,
    hotKeywords: state.search.hotKeywords,

    channelList: state.search.channelList,
    equipmentList: state.search.equipmentList,
    popularTagList: state.search.popularTagList,
    instructorList: state.search.instructorList,
    workoutTypeList: state.search.workoutTypeList,
    pageSize: state.seeAll.pageSize,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getInstructorList: SearchActions.getInstructorList,
        getEquipmentList: SearchActions.getEquipmentList,
        getWorkoutTypeList: SearchActions.getWorkoutTypeList,
        getSearchHotKeyword: SearchActions.getSearchHotKeyword,
        search: SearchActions.search,
        getGenre: SettingActions.getGenre,
        getSearchableData: SearchActions.getSearchableData,
        resetList: SeeAllActions.resetList,
      },
      dispatch,
    ),
)(SearchScreen);
