import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { isString, isEmpty, isArray } from 'lodash';
import { StripeView } from '@ublocks-react-native/component';
import { View, Text, ScrollView, SafeAreaView, FlatList } from 'react-native';
import { Date as d, Screen, Dialog } from 'App/Helpers';
import LiveAction from 'App/Stores/Live/Actions';
import InstructorAction from 'App/Stores/Instructor/Actions';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { ClassActions } from 'App/Stores';
import {
  RoundLabel,
  SquareButton,
  BaseButton,
  HeadLine,
  SecondaryNavbar,
  BaseReadMore,
  BaseImageButton,
  CachedImage as Image,
  CachedImage as ImageBackground,
} from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Classes, Images, Colors } from 'App/Theme';
import styles from './LiveDetailStyle';

class LiveDetail extends React.Component {
  static propTypes = {
    isConnected: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired,
    joinLive: PropTypes.func.isRequired,
    bookLive: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
    timezone: PropTypes.string.isRequired,
    bookedLive: PropTypes.array,
    setInstructorId: PropTypes.func.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    instructor: PropTypes.object,
    mirrorCompare: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    mainSubscription: PropTypes.object,
    routeName: PropTypes.string.isRequired,
    onlyGetLiveDetail: PropTypes.func.isRequired,
    setClassLiveBookmark: PropTypes.func.isRequired,
    removeClassLiveBookmark: PropTypes.func.isRequired,
  };

  static defaultProps = {
    bookedLive: [],
    instructor: {
      title: '',
      description: '',
      avatarImage: '',
    },
  };

  state = {
    isPrimary: false,
  };

  counter = null;

  componentDidMount() {
    this.startForceUpdateTimer();
  }

  componentDidUpdate(prevProps) {
    const { routeName } = this.props;
    if (routeName !== prevProps.routeName && routeName === 'LiveDetail') {
      this.startForceUpdateTimer();
    }
  }

  componentWillUnmount() {
    clearInterval(this.counter);
    this.counter = null;
  }

  startForceUpdateTimer = () => {
    if (!this.counter) {
      this.counter = setInterval(() => {
        if (this.props.routeName === 'LiveDetail') {
          this.forceUpdate();
        }
      }, 2 * 1000);
    }
  };

  renderStripesView = (list) => {
    if (list && list.length > 0) {
      return list.map((item) => ({
        leftComponent: <Text style={styles.stripeText}>{item.title}</Text>,
        rightComponent: (
          <Text style={styles.stripeSecondText}>{d.transformMinute(item.duration)}</Text>
        ),
      }));
    }
  };

  renderItem = ({ item, index }) => {
    return (
      <View key={index} style={styles.imageBox}>
        <Image
          source={item.signedDeviceCoverImageObj}
          style={styles.equipments}
          resizeMode="contain"
        />
        <Text style={styles.equipmentsText}>{item.title}</Text>
      </View>
    );
  };

  onPressInstructor = () => {
    const { data, setInstructorId, token } = this.props;
    setInstructorId(data.instructor.id, token);
  };

  renderClassCard = () => {
    const { data, timezone } = this.props;
    return (
      <ImageBackground
        source={data.signedCoverImageObj}
        style={styles.liveImgWidth}
        imageSize="2x"
        imageType="h"
      >
        <View style={styles.liveFragment}>
          <View style={styles.liveLeftBox}>
            <View style={styles.liveLeft}>
              <Text style={styles.liveSecondTitle}>{`${t('__live')}：${moment(
                d.transformDate(data.scheduledAt, timezone),
              ).format('ddd MM/DD [@]hh:mm A')}`}</Text>
              <Text style={styles.liveTitle}>
                {isString(data.title) && data.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.liveRightBox}>
            <View style={styles.liveRight}>
              <RoundLabel text={t(`search_lv${data.level}`)} />
              <RoundLabel text={`${data.duration / 60}${t('min')}`} />
            </View>
            <BaseImageButton
              style={styles.btnBookmark}
              imageStyle={styles.btnBookmarkImage}
              source={
                data.isBookmarked ? Images.bookmark_solid_fixed : Images.bookmark_black
              }
              onPress={() => this.onPressBookmark(data.id, data)}
              throttleTime={100}
            />
          </View>
        </View>
      </ImageBackground>
    );
  };

  onPressBtnDisabled = () => {
    const { data } = this.props;
    let minutes = d.transformDiffMinuteTime(data.scheduledAt);
    if (minutes < 0 && minutes * -60 > data.duration) {
      return true;
    }
    return false;
  };

  onPressBack = () => {
    const { onMirrorCommunicate, data } = this.props;
    onMirrorCommunicate(MirrorEvents.EXIT_CLASS_PREVIEW, {
      classId: data.id,
      classType: 'live',
    });
  };

  onJoinLiveClass = () => {
    const { data, token, joinLive } = this.props;

    joinLive(data.id, token);
  };

  checkSubscriptionExpire(mainSubscription) {
    let isSubscriptionExpire = true;

    if (!isEmpty(mainSubscription) && mainSubscription.items) {
      mainSubscription.items.forEach((item) => {
        if (item.product.mainChannel && d.moment() <= d.moment(item.expireAt)) {
          isSubscriptionExpire = false;
        }
      });
    }

    return isSubscriptionExpire;
  }
  showLiveBtn = () => {
    const {
      data,
      isLoading,
      timezone,
      bookLive,
      isConnected,
      token,
      mainSubscription,
    } = this.props;
    const minutes = d.transformDiffMinuteTime(data.scheduledAt);

    const isSubscriptionExpire = this.checkSubscriptionExpire(mainSubscription);
    console.log('=== isSubscriptionExpire ===', isSubscriptionExpire);
    if (isEmpty(mainSubscription)) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestAccountSubscriptionCodeAlert(this.onJoinLiveClass)}
          disabled={isLoading}
          text={t('class_detail_request_account_subscription_alert_title')}
        />
      );
    } else if (isSubscriptionExpire) {
      return (
        <SquareButton
          color={Colors.button.primary.content.background}
          onPress={Dialog.requestAccountSubscriptionExpiredAlert(this.onJoinLiveClass)}
          disabled={isLoading}
          text={t('class_detail_request_account_subscription_expired_title')}
        />
      );
    } else if (minutes >= 0 || (minutes < 0 && data.duration / 60 + minutes > 2)) {
      if (minutes > 2) {
        if (data.isBooked) {
          return (
            <SquareButton
              disabled
              color={Colors.black}
              text={moment(d.transformDate(data.scheduledAt, timezone)).format(
                'MM/DD [at] hh:mm A',
              )}
            />
          );
        }
        return (
          <SquareButton
            color={Colors.button.primary.content.background}
            onPress={() => bookLive(data.id, token)}
            text={t('live_detail_book_this_class')}
            disabled={isLoading}
            throttleTime={5000}
          />
        );
      } else if (minutes <= 2) {
        return isConnected ? (
          <SquareButton
            color={Colors.button.primary.content.background}
            onPress={this.onJoinLiveClass}
            text={t('live_detail_join_this_class')}
            disabled={isLoading}
          />
        ) : (
          <SquareButton
            onPress={Actions.DeviceModal}
            color={Colors.button.primary.content.background}
            text={t('class_detail_connect_device')}
          />
        );
      }
    }
  };

  onPressBookmark = (id, item) => {
    const { setClassLiveBookmark, removeClassLiveBookmark } = this.props;

    if (item.isBookmarked) {
      removeClassLiveBookmark(id);
    } else {
      setClassLiveBookmark(id);
    }
  };

  render() {
    const { data, instructor, isConnected } = this.props;
    const {
      title = '',
      description = '',
      signedAvatarImageObj,
      signedAvatarImage = '',
    } = instructor;

    const minutes = d.transformDiffMinuteTime(data.scheduledAt);
    return (
      <SafeAreaView style={styles.container}>
        <SecondaryNavbar
          back
          onPressBack={this.onPressBack}
          navRightComponent={
            <View style={styles.navBox}>
              <BaseImageButton
                style={styles.navVoiceIcon}
                source={Images.mirror}
                onPress={Actions.DeviceModal}
              />
              <BaseImageButton
                style={styles.navVoiceIcon}
                source={Images.voice}
                onPress={Actions.AudioModal}
                disabled={!isConnected}
              />
              <BaseImageButton
                style={styles.navSettingIcon}
                source={Images.setting}
                onPress={Actions.WorkoutOptionModal}
                disabled={!isConnected}
              />
            </View>
          }
        />
        <View style={styles.layoutBox}>
          <ScrollView style={styles.scrollBox}>
            {this.renderClassCard()}
            {data.hasMotionCapture && (
              <View style={styles.captureBox}>
                <Image source={Images.motion_capture} style={styles.captureImg} />
                <Text style={styles.captureText}>{t('motion_capture')}</Text>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine
                paddingHorizontal={16}
                title={t('class_detail_about_this_workout')}
              />
              <BaseReadMore text={data.description} />
            </View>
            <View style={styles.contentBox}>
              <HeadLine paddingHorizontal={16} title={t('class_detail_instructor')} />
              <View style={styles.instructorBox}>
                <Image
                  source={signedAvatarImageObj}
                  errorImage={Images.defaultAvatar}
                  style={styles.avatarImage}
                />
                <Text style={styles.instructorText}>{title}</Text>
              </View>
              <Text style={styles.context} numberOfLines={2}>
                {description}
              </Text>
              <View style={[styles.roundBox]}>
                <BaseButton
                  transparent
                  height={Screen.scale(18)}
                  text={t('view_profile')}
                  uppercase={false}
                  onPress={this.onPressInstructor}
                  style={styles.btnStyle}
                  textColor={Colors.button.primary.text.text}
                  textStyle={styles.expandText}
                />
              </View>
            </View>
            <View style={styles.contentBox}>
              <HeadLine paddingHorizontal={16} title={t('class_detail_good_for_tags')} />
              <View style={[Classes.row, Classes.paddingLeft, Classes.paddingRight]}>
                {isArray(data.tags) &&
                  data.tags.map((e, i) => (
                    <Text key={`${e.tagId}`} style={styles.txtTags}>
                      {e.name}
                      {i !== data.tags.length - 1 ? ', ' : '.'}
                    </Text>
                  ))}
              </View>
            </View>
            {data.requiredEquipments.length > 0 && (
              <View style={styles.contentBox}>
                <HeadLine
                  paddingHorizontal={16}
                  title={t('class_detail_equipment_needed')}
                />
                <View style={styles.equipmentBox}>
                  <FlatList
                    horizontal
                    data={data.requiredEquipments}
                    renderItem={this.renderItem}
                  />
                </View>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine
                paddingHorizontal={16}
                title={`${data.exercises.length} ${t('class_detail_exercise')}`}
                hrLineStyle={styles.hrStyle}
              />
              {!isEmpty(data.exercises) && (
                <StripeView
                  style={styles.stripe}
                  buttonStyle={styles.stripeBtn}
                  stripStyle={styles.stripeStyle}
                  stripeLightColor={Colors.white}
                  stripeDarkColor={Colors.white_02}
                  stripes={this.renderStripesView(data.exercises)}
                />
              )}
            </View>
          </ScrollView>
        </View>
        {__DEV__ && <Text>start in minutes: {minutes}</Text>}
        {__DEV__ && <Text>total duration: {data.duration}</Text>}
        <View style={styles.btnBox}>{this.showLiveBtn()}</View>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    isConnected: state.mirror.isConnected,
    isLoading: state.appState.isLoading,
    data: state.player.detail,
    instructor: state.player.detail.instructor,
    token: state.user.token,
    bookedLive: state.class.list.bookedLive,
    timezone: state.appState.currentTimeZone,
    mainSubscription: state.user.mainSubscription,
    routeName: state.appRoute.routeName,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        setClassLiveBookmark: ClassActions.setClassLiveBookmark,
        removeClassLiveBookmark: ClassActions.removeClassLiveBookmark,
        onlyGetLiveDetail: LiveAction.onlyGetLiveDetail,
        joinLive: LiveAction.joinLive,
        bookLive: LiveAction.bookLive,
        setInstructorId: InstructorAction.setInstructorId,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        mirrorCompare: MirrorActions.mirrorCompare,
      },
      dispatch,
    ),
)(LiveDetail);
