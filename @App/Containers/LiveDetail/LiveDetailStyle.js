import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Styles, Colors, Classes, Metrics, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  navBar: {
    ...Styles.navBar,
  },
  navBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  layoutBox: {
    flex: 1,
  },
  container: {
    ...Classes.fill,
  },
  btnBox: {
    width: '100%',
  },
  navVoiceIcon: {
    width: '30@s',
    height: '30@s',
    marginRight: '21@s',
  },
  navSettingIcon: {
    width: '30@s',
    height: '30@s',
  },
  marginBetween: {
    marginHorizontal: Metrics.baseMargin,
  },
  imgStyle: {
    width: '100%',
    height: '300@vs',
  },
  contentBox: {
    marginTop: Metrics.baseMargin,
  },
  context: {
    paddingHorizontal: '20@s',
    marginTop: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
  },
  equipmentBox: {
    paddingHorizontal: '20@s',
    flexDirection: 'row',
    marginTop: Metrics.baseMargin,
  },
  instructorBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: Metrics.baseMargin,
  },
  avatarImage: {
    width: '49@s',
    height: '49@s',
    borderRadius: '24.5@s',
  },
  instructorText: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
  },
  expandText: {
    fontSize: Fonts.size.medium,
    textAlign: 'right',
  },
  btnStyle: {
    backgroundColor: 'transparent',
    paddingLeft: 0,
    paddingRight: 0,
    width: '94@s',
    marginRight: Metrics.baseMargin,
  },
  roundBox: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  equipments: {
    width: Platform.isPad ? '35@vs' : '30@s',
    height: Platform.isPad ? '35@vs' : '30@s',
  },
  equipmentsText: {
    ...Fonts.style.extraSmall,
    width: '60@s',
    textAlign: 'center',
  },
  imageBox: {
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: Metrics.baseMargin,
  },
  captureBox: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginTop: '20@vs',
    marginLeft: Metrics.baseMargin,
  },
  captureImg: {
    width: '30@s',
    height: '30@vs',
    marginLeft: '15@vs',
  },
  captureText: {
    ...Fonts.style.extraSmall500,
    width: '64@s',
    textAlign: 'center',
  },
  liveImgWidth: {
    width: '100%',
    height: Screen.verticalScale(230),
    overflow: 'hidden',
  },
  liveTitle: {
    ...Fonts.style.regular500,
    letterSpacing: 1,
    color: Colors.secondary,
  },
  liveBoxStyle: {
    borderRadius: 7.5,
    backgroundColor: Colors.black,
    marginTop: Metrics.baseMargin / 4,
    alignItems: 'center',
  },
  liveText: {
    ...Fonts.style.extraSmall500,
    paddingVertical: Metrics.baseMargin / 8,
    paddingHorizontal: Metrics.baseMargin / 2,
    color: Colors.white,
  },
  liveLiveBoxStyle: {
    borderRadius: 7,
    width: '55@s',
    height: '15@vs',
    backgroundColor: Colors.white,
    marginBottom: 4,
    alignItems: 'center',
  },
  liveLiveText: {
    fontSize: 10,
    fontWeight: '500',
    letterSpacing: 1,

    color: Colors.primary,
  },
  liveFragment: {
    flexDirection: 'row',
    flex: 1,
  },
  liveLeftBox: {
    flex: 1,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  liveRightBox: {
    marginRight: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  liveLeft: {
    flexDirection: 'column',
  },
  liveRight: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },
  liveTime: {
    fontSize: '11@vs',
    color: '#f0f0f0',
  },
  liveSecondTitle: {
    ...Fonts.style.extraSmall500,
    color: Colors.gray_04,
  },
  liveSecondIcon: {
    width: '50@vs',
    backgroundColor: 'blue',
    color: Colors.white,
    flexDirection: 'row',
  },
  liveEquipments: {
    width: '20@vs',
    height: '20@vs',
  },
  liveImageBox: {
    width: '40@vs',
    height: '40@vs',
    borderRadius: '40@vs',
    marginRight: '8@s',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  liveEquipmentsBox: {
    flexDirection: 'row',
  },
  liveTimeBox: {
    width: '100%',
    height: '13@vs',
    marginBottom: '12@vs',
  },
  stripeBtn: {
    height: '48@vs',
    lineHeight: '48@vs',
  },
  stripeStyle: {
    alignItems: 'center',
    paddingRight: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    height: '48@vs',
    lineHeight: '48@vs',
  },
  stripeText: {
    ...Fonts.style.medium500,
  },
  stripeSecondText: {
    fontSize: Fonts.size.extraSmall,
  },
  stripe: {
    marginTop: 1,
  },
  hrLine: {
    marginBottom: 0,
  },
  stripeImage: {
    width: '30@s',
    height: '30@vs',
    marginRight: Metrics.baseMargin,
  },
  stripeRight: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnBookmark: {
    position: 'absolute',
    backgroundColor: Colors.white_02,
    top: Metrics.baseMargin,
    right: 0,
    width: '48@s',
    height: '48@s',
    borderRadius: Metrics.baseMargin,
  },
  btnBookmarkImage: {
    height: '32@s',
  },
});
