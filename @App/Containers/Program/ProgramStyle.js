import { ScaledSheet } from 'App/Helpers';
import { Styles, Colors, Fonts, Metrics } from 'App/Theme';

export default ScaledSheet.create({
  layout: {
    ...Styles.screenMargin,
    flexDirection: 'column',
    height: '100%',
  },
  title: {
    ...Fonts.style.medium500,
    marginTop: Metrics.baseMargin,
    color: Colors.black,
  },
  content: {
    ...Fonts.style.small500,
    color: Colors.gray_02,
    marginTop: Metrics.baseMargin / 2,
  },
});
