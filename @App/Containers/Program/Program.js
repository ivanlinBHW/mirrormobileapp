import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, SafeAreaView, Text } from 'react-native';

import { ProgramActions, UserActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { Classes } from 'App/Theme';
import {
  ProgramCard,
  AutoHideFlatList,
  AutoHideScrollView,
  FlatListEmptyLoading,
} from 'App/Components';

import ProgramDetail from 'App/Containers/ProgramDetail/ProgramDetail';
import styles from './ProgramStyle';

class Program extends React.Component {
  static propTypes = {
    appRoute: PropTypes.object.isRequired,
    token: PropTypes.string,
    sceneKey: PropTypes.string.isRequired,
    getProgramList: PropTypes.func.isRequired,
    getProgramDetail: PropTypes.func.isRequired,
    programId: PropTypes.string,
    loading: PropTypes.bool,
    list: PropTypes.array,
    isProgramFinished: PropTypes.bool,
    getUserAccount: PropTypes.func.isRequired,
  };

  static defaultProps = {
    isProgramFinished: false,
  };

  componentDidMount() {
    const { getUserAccount } = this.props;
    getUserAccount();
  }

  onPressProgramDetail = (id) => {
    const { token, getProgramDetail } = this.props;
    getProgramDetail(id, token, true);
  };

  onPressDetail = (id) => {
    const { getProgramDetail, token } = this.props;
    getProgramDetail(id, token, true);
  };

  renderItem = ({ item }) => {
    return <ProgramCard data={item} onPress={this.onPressDetail} />;
  };

  renderEmpty = () => {
    const { loading } = this.props;
    return (
      <View style={[Classes.fillCenter, Classes.marginTop]}>
        <FlatListEmptyLoading isLoading={loading} />
      </View>
    );
  };

  render() {
    const { list, programId, sceneKey } = this.props;
    return (
      <SafeAreaView>
        {programId ? (
          <AutoHideScrollView>
            <ProgramDetail isStandalone={false} />
          </AutoHideScrollView>
        ) : (
          <View style={styles.layout}>
            <AutoHideFlatList
              sceneKey={sceneKey}
              ListEmptyComponent={this.renderEmpty()}
              ListHeaderComponent={
                <View>
                  {!isEmpty() && (
                    <Text style={styles.title}>{t('program_start_a_program')}</Text>
                  )}
                  {/* <Text style={styles.content}>{t('program_content')}</Text> */}
                </View>
              }
              data={list}
              renderItem={this.renderItem}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state, params) => ({
    appRoute: state.appRoute,
    token: state.user.token,
    list: state.program.list,
    programId: state.user.activeTrainingProgramId,
    isProgramFinished: state.program.isProgramFinished,
    loading: state.appState.isLoading,
    sceneKey: params.name,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getProgramDetail: ProgramActions.getProgramDetail,
        getProgramList: ProgramActions.getProgramList,
        getUserAccount: UserActions.getUserAccount,
      },
      dispatch,
    ),
)(Program);
