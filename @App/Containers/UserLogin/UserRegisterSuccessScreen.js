import * as Yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { View, Text, Linking, Platform } from 'react-native';

import {
  VerticalTextInput,
  SecondaryNavbar,
  DismissKeyboard,
  KeyboardHandler,
  SquareButton,
  LinkButton,
  BaseIcon,
} from 'App/Components';
import { UserActions } from 'App/Stores';
import { Classes, Colors, Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Config } from 'App/Config';

import styles from './UserRegisterSuccessStyle';

const yString = Yup.string();

const validationSchema = Yup.object().shape({
  subscriptionCode: yString
    .required(t('subscription_validation_code_required'))
    .min(5, t('subscription_validation_code_min_error', { value: 5 })),
});

const initialValues = {
  subscriptionCode: '',
};

class UserRegisterSuccessScreen extends React.Component {
  static propTypes = {
    fetchPutSubscriptionCode: PropTypes.func.isRequired,
    errorCode: PropTypes.string,
  };

  componentDidMount() {
    __DEV__ && console.log('@UserRegisterSuccessScreen');
  }

  state = {
    ...initialValues,
    isKeyboardShow: false,
  };

  renderForm = () => {
    const { fetchPutSubscriptionCode, errorCode } = this.props;
    const { isKeyboardShow } = this.state;
    return (
      <Formik
        initialValues={this.state}
        validationSchema={validationSchema}
        onSubmit={(values) =>
          fetchPutSubscriptionCode(values.subscriptionCode, 'MirrorFirstSetupScreen')
        }
        validateOnChange
      >
        {({
          dirty,
          isValid,
          isSubmitting,
          isValidating,
          values,
          errors,
          touched,
          setFieldValue,
          handleSubmit,
          handleChange,
          setFieldTouched,
        }) => (
          <View style={Classes.fill}>
            <VerticalTextInput
              onChangeText={handleChange('subscriptionCode')}
              error={
                errors.subscriptionCode ||
                (errorCode && !isEmpty(errorCode) ? t(errorCode) : '')
              }
              value={values.subscriptionCode}
              autoCapitalize="none"
              border
            />
            {!isKeyboardShow && (
              <SquareButton
                text={t('submit')}
                style={styles.marginTop80}
                color={Colors.button.primary.content.background}
                onPress={handleSubmit}
                round
              />
            )}
          </View>
        )}
      </Formik>
    );
  };

  render() {
    const { isKeyboardShow } = this.state;
    return (
      <DismissKeyboard>
        <View style={Classes.fill}>
          {Platform.OS === 'android' && (
            <KeyboardHandler
              onKeyboardDidShow={() => this.setState(() => ({ isKeyboardShow: true }))}
              onKeyboardDidHide={() => this.setState(() => ({ isKeyboardShow: false }))}
            />
          )}
          <View style={[Classes.center, Classes.fill, styles.marginTop40]}>
            <BaseIcon
              name="check-circle"
              color={Colors.fontIcon.primary}
              size={60}
              style={styles.introImg}
            />
            <Text style={styles.introTitle}>{t('register_success_title')}</Text>
            <Text style={styles.introContent}>{t('register_success_desc')}</Text>
          </View>

          <View style={[Classes.fill, Classes.padding]}>
            {this.renderForm()}

            <SquareButton
              text={t('__skip')}
              style={styles.marginTop80}
              onPress={() => Actions.MirrorFirstSetupScreen()}
              borderOutline
              round
            />
          </View>

          {!isKeyboardShow && (
            <View
              style={[
                Classes.halfFill,
                Classes.mainEnd,
                Classes.crossCenter,
                Classes.marginBottom,
              ]}
            >
              {/* TODO Set all text to one link */}
              <Text style={Fonts.style.medium}>
                {t('setting_account_no_subscription_code')}
              </Text>
              <LinkButton
                text={t('setting_account_go_shop')}
                textColor={Colors.button.primary.text.text}
                textSize={Fonts.size.small}
                onPress={() => Linking.openURL(Config.SHOP_LINK)}
              />
            </View>
          )}
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({
    errorCode: state.user.errorCode,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        fetchPutSubscriptionCode: UserActions.fetchPutSubscriptionCode,
      },
      dispatch,
    ),
)(UserRegisterSuccessScreen);
