import * as Yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  View,
  Text,
  Linking,
  Platform,
  SafeAreaView,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
  VerticalTextInput,
  SecondaryNavbar,
  DismissKeyboard,
  KeyboardHandler,
  SquareButton,
  LinkButton,
  BaseIcon,
} from 'App/Components';
import { UserActions } from 'App/Stores';
import { Classes, Colors, Fonts, Metrics, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Config } from 'App/Config';
import { openInbox } from 'react-native-email-link';
import CountDown from 'react-native-countdown-component';
import UUID from 'uuid-generate';

class UserRegisterVerifyEmailScreen extends React.Component {
  static propTypes = {
    registerEmail: PropTypes.string.isRequired,
    emailVerifyTimerSaga: PropTypes.func.isRequired,
    sendEmailVerify: PropTypes.func.isRequired,
    isNavLoading: PropTypes.bool.isRequired,
    emailVerifyCountDownId: PropTypes.string,
    emailVerifiedAt: PropTypes.string,
  };
  static defaultProps = {
    registerEmail: '',
  };
  state = {
    emailVerifyCountDownId: UUID.generate(),
    emailVerifyCountDownFinish: false,
  };

  componentDidMount = () => {
    const { emailVerifyTimerSaga, registerEmail: email } = this.props;
    setTimeout(() => {
      emailVerifyTimerSaga(email);
    }, 3000);
  };
  onCountDownFinish = () => {
    const emailVerifyCountDownFinish = true;
    this.setState({ emailVerifyCountDownFinish });
  };
  componentDidUpdate = (nextProps) => {
    console.log(
      '=== nextProps.emailVerifyCountDownId ===',
      nextProps.emailVerifyCountDownId,
    );
    if (
      this.props.emailVerifyCountDownId != null &&
      nextProps.emailVerifyCountDownId !== this.props.emailVerifyCountDownId
    ) {
      const emailVerifyCountDownFinish = false;
      const { emailVerifyCountDownId } = nextProps;
      console.log(
        '=== nextProps.emailVerifyCountDownId ===',
        nextProps.emailVerifyCountDownId,
      );
      this.setState({ emailVerifyCountDownId, emailVerifyCountDownFinish });
    }
  };
  state = {};

  render() {
    const { registerEmail, isNavLoading, sendEmailVerify, emailVerifiedAt } = this.props;
    const { emailVerifyCountDownFinish } = this.state;
    console.log('=== emailVerifyCountDownFinish ===', emailVerifyCountDownFinish);
    console.log(
      '=== this.state.emailVerifyCountDownId ===',
      this.state.emailVerifyCountDownId,
    );
    const emailVerified = emailVerifiedAt != null && emailVerifiedAt != '';
    return (
      <DismissKeyboard>
        <View style={Classes.fill}>
          <SecondaryNavbar
            back
            navTitle=""
            navRightComponent={
              isNavLoading && <ActivityIndicator size="small" color="black" />
            }
          />

          <View style={Classes.fill}>
            <View style={styles.container}>
              <View style={styles.imageWrapper}>
                <Image source={Images.emailAuthorization} style={styles.imageMain} />
              </View>
              <View style={styles.txtWrapper}>
                <Text style={styles.txtHeader}>{t('1-1-a_authorization')}</Text>

                {emailVerified && (
                  <Text style={styles.txtContent}>
                    {t('1-1-a_authorization_mail_success')}
                  </Text>
                )}
                {!emailVerified && (
                  <Text style={styles.txtContent}>
                    {t('1-1-a_authorization_mail_description')}
                  </Text>
                )}

                <Text>{registerEmail}</Text>
              </View>
              <View style={styles.txtContent}>
                {!emailVerified && !emailVerifyCountDownFinish && (
                  <>
                    <Text>{t('1-1-a_check_your_email_verify_description')}</Text>
                    <CountDown
                      until={60 * 5}
                      size={14}
                      onFinish={() => this.onCountDownFinish()}
                      id={this.state.emailVerifyCountDownId}
                      digitStyle={{ backgroundColor: '#FFF', marginHorizontal: 0 }}
                      digitTxtStyle={{ color: '#323232' }}
                      timeToShow={['M', 'S']}
                      timeLabels={{ m: null, s: null }}
                      showSeparator
                    />
                  </>
                )}
                {!emailVerified && emailVerifyCountDownFinish && (
                  <>
                    <Text>{t('1-1-b_authorization_has_expired')}</Text>
                    <Text>{t('1-1-b_please_click_resend')}</Text>
                  </>
                )}
              </View>
              {!emailVerified && (
                <View>
                  <LinkButton
                    text={`${t('1-1-a_resend')}`}
                    textSize={Fonts.size.medium}
                    textColor={Colors.button.primary.text.text}
                    textStyle={styles.txtFooter}
                    onPress={() => {
                      sendEmailVerify({ email: registerEmail });
                    }}
                  />
                </View>
              )}
            </View>
          </View>
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({
    registerEmail: state.user.email,
    emailVerifiedAt: state.user.emailVerifiedAt,
    isNavLoading: state.appState.isNavLoading,
    emailVerifyCountDownId: state.user.emailVerifyCountDownId,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        emailVerifyTimerSaga: UserActions.emailVerifyTimerSaga,
        sendEmailVerify: UserActions.sendEmailVerify,
      },
      dispatch,
    ),
)(UserRegisterVerifyEmailScreen);

import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  container: {
    marginHorizontal: Metrics.baseMargin,
    height: '100%',
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '44@vs',
  },
  imageMain: {
    width: '65@s',
    height: '60@vs',
  },
  txtWrapper: {
    marginVertical: '40@vs',
    alignItems: 'center',
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: Metrics.baseMargin / 2,
  },
  txtFooter: {
    textAlign: 'center',
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'center',
    color: Colors.gray_01,
    textAlign: 'center',
    alignItems: 'center',
  },
});
