import React from 'react';
import * as yup from 'yup';
import UUID from 'uuid-generate';
import PropTypes from 'prop-types';
import firebase from 'react-native-firebase';
import { isNil } from 'lodash';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Text, Input } from 'react-native-elements';
import { ToastAndroid, Platform, Image, View, TouchableOpacity } from 'react-native';

import { SquareButton, DismissKeyboard } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Permission, Dialog } from 'App/Helpers';
import { UserActions } from 'App/Stores';
import { Config } from 'App/Config';
import { Images, Classes } from 'App/Theme';
import Validate from 'App/Helpers/Validate';
import styles from './UserLoginScreenStyle';

class UserLoginScreen extends React.Component {
  static propTypes = {
    onUpdateFcmToken: PropTypes.func.isRequired,
    resetErrorCode: PropTypes.func.isRequired,
    routeName: PropTypes.string.isRequired,
    currentCountryCode: PropTypes.string,
    login: PropTypes.func.isRequired,
    errorCode: PropTypes.string,
    device: PropTypes.object,
    status: PropTypes.string,
    reason: PropTypes.string,
  };

  state = {
    currentCountryCode: '',
    reason: null,
  };

  async componentDidMount() {
    __DEV__ && console.log('@Enter LoginScreen!');

    await Permission.requestFcmPermission();
  }

  onPressLogin = async (values) => {
    let fcmToken = '';
    if (Platform.OS === 'android') {
      const utils = firebase.utils();
      const { isAvailable } = utils.playServicesAvailability;

      if (isAvailable) {
        fcmToken = await firebase.messaging().getToken();
      } else {
        ToastAndroid.show(
          'You should install Google Play Services in order to use Firebase Cloud Messaging.',
          ToastAndroid.SHORT,
        );
      }
    } else {
      fcmToken = await firebase.messaging().getToken();
    }
    await this.handleLogin(fcmToken, values);
    if (!fcmToken) {
      fcmToken = UUID.generate();
    }
    const { onUpdateFcmToken } = this.props;
    onUpdateFcmToken(fcmToken);
  };

  handleLogin = async (deviceAddress, values) => {
    const payload = {
      email: values.email,
      password: values.password,
      deviceAddress,
    };
    if (Validate.isRegionSupport() === false) {
      Dialog.regionIsNotSupportAlert()();
    } else {
      await this.props.login(payload);
    }
  };

  handleRegister = () => {
    if (Validate.isRegionSupport() === false) {
      Dialog.regionIsNotSupportAlert()();
    } else {
      Actions.UserRegisterPrivacyScreen({ isRegisterFlow: true });
    }
  };
  handleForgetPassword = () => {
    if (Validate.isRegionSupport() === false) {
      Dialog.regionIsNotSupportAlert()();
    } else {
      Actions.SendForgotMailScreen();
    }
  };
  render() {
    const { errorCode, currentCountryCode } = this.props;
    return (
      <DismissKeyboard>
        <View style={styles.layout}>
          <View style={styles.imageBox}>
            <Image source={Images.logo} style={styles.imgLogo} />
          </View>
          <Text style={styles.headerText}>{t('login_title')}</Text>
          <Formik
            onSubmit={this.onPressLogin}
            validateOnChange={false}
            initialValues={{
              email: __DEV__ ? '' : '',
              password: __DEV__ ? '' : '',
            }}
            validationSchema={yup.object().shape({
              email: yup
                .string()
                .email()
                .required('Sorry, invalid Email.'),
              password: yup.string(),
            })}
          >
            {({
              values,
              handleChange,
              errors,
              touched,
              handleSubmit,
              dirty,
              setFieldTouched,
              handleBlur,
            }) => (
              <View>
                <Input
                  name="email"
                  inputContainerStyle={styles.wrapper}
                  containerStyle={styles.inputBox}
                  inputStyle={styles.textStyle}
                  placeholder={t('login_email_placeholder')}
                  autoCapitalize="none"
                  value={values.email}
                  onChangeText={(v) => {
                    handleChange('email')(v);
                    if (v === '' || !v) {
                      setFieldTouched('email', false, false);
                    }
                  }}
                  onBlur={handleBlur('email')}
                  onFocus={() => {
                    if (!values.email || errors.email) {
                      setFieldTouched('email', false, false);
                    }
                  }}
                />
                <Input
                  name="password"
                  inputContainerStyle={styles.wrapper}
                  containerStyle={styles.inputBox}
                  inputStyle={styles.textStyle}
                  placeholder={t('login_password_placeholder')}
                  secureTextEntry={true}
                  type="password"
                  value={values.password}
                  autoCapitalize="none"
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                />
                {dirty && !!values.email && touched.email && errors.email && (
                  <Text style={styles.errorStyle}>{t('login_email_error')}</Text>
                )}
                <Text style={styles.errorStyle}>
                  {!isNil(errorCode) ? errorCode : ''}
                </Text>

                <View style={styles.btnbox}>
                  <SquareButton
                    textStyle={styles.loginStyle}
                    text={t('login_sign_in')}
                    onPress={handleSubmit}
                    radius={6}
                    testID="btn_login"
                    accessible={true}
                    accessibilityLabel={'btn_login'}
                  />
                </View>
                {!values.email && touched.email && errors.email && (
                  <Text style={styles.errorStyle}>{t('login_account_error')}</Text>
                )}
              </View>
            )}
          </Formik>

          <View style={[styles.btnbox, styles.createBtn]}>
            <SquareButton
              borderOutline
              textStyle={styles.loginStyle}
              text={t('login_create_an_account')}
              radius={6}
              onPress={this.handleRegister}
            />
          </View>
          <Text style={styles.footerText}>{t('login_footer')}</Text>

          <TouchableOpacity onPress={this.handleForgetPassword}>
            <Text style={styles.ForgotPasswordLink}>{t('forgot_password')}</Text>
          </TouchableOpacity>

          <View style={[Classes.mainEnd, Classes.marginBottom]}>
            <Text style={styles.footerText}>{currentCountryCode}</Text>
          </View>
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({
    status: state.user.status,
    errorCode: state.user.errorCode,
    device: state.bleDevice.lastDevice,
    routeName: state.appRoute.routeName,
    currentCountryCode: state.user.currentCountryCode,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        resetErrorCode: UserActions.resetErrorCode,
        onUpdateFcmToken: UserActions.onFcmTokenUpdate,
        login: UserActions.fetchUserLogin,
      },
      dispatch,
    ),
)(UserLoginScreen);
