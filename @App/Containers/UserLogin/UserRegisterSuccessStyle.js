import { ScaledSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default ScaledSheet.create({
  wrapper: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: '100%',

    marginHorizontal: Metrics.baseMargin,
  },
  introImg: {
    marginBottom: '40@s',
  },
  introTitle: {
    fontSize: Fonts.size.h5,
    fontWeight: 'bold',
    marginBottom: 0,
    textAlign: 'center',
  },
  introContent: {
    ...Fonts.style.medium,
    padding: Metrics.baseMargin,
    color: Colors.gray_01,
    textAlign: 'center',
  },
  introStep5Content: {
    ...Fonts.style.regular500,
    color: Colors.gray_01,
    textAlign: 'center',
    marginBottom: '66@vs',
  },
  marginTop40: {
    marginTop: '40@s',
  },
  marginTop18: {
    marginTop: '18@s',
  },
  marginTop80: {
    marginTop: '80@s',
  },
});
