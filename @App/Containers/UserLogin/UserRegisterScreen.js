import * as yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { has } from 'lodash';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import {
  KeyboardAvoidingView,
  ScrollView,
  Platform,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';

import { Classes, Metrics, Colors, Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Screen, Date as d, isIphoneX } from 'App/Helpers';
import { UserActions } from 'App/Stores';
import {
  DateTimePickerModal,
  HorizontalTextInput,
  DismissKeyboard,
  SecondaryNavbar,
  SquareButton,
  LinkButton,
  HeadLine,
} from 'App/Components';
import styles from './UserLoginScreenStyle';
import Modal from 'react-native-modal';
import { WebView } from 'react-native-webview';
import html_en from 'App/Assets/Html/html_en';
import html_zh from 'App/Assets/Html/html_zh';
import html_th from 'App/Assets/Html/html_th';
import { Images } from 'App/Theme';
const yString = yup.string();

const validationSchema = yup.object().shape({
  email: yString
    .email(t('register_validation_email_type_error'))
    .required(t('register_validation_email_required')),
  password: yString
    .required(t('register_validation_password_required'))
    .min(8, t('register_validation_password_min_error', { value: 8 }))
    .max(16, t('register_validation_password_max_error', { value: 16 })),
  passwordConfirmation: yString
    .required(t('register_validation_password_required'))
    .min(8, t('register_validation_password_min_error', { value: 8 }))
    .max(16, t('register_validation_password_max_error', { value: 16 })),
});

const initialValues = {
  email: '',
  password: '',
  passwordConfirmation: '',
};

class UserRegisterScreen extends React.Component {
  static propTypes = {
    currentLocales: PropTypes.array.isRequired,
    fetchUserRegister: PropTypes.func.isRequired,
    fetchUserTempRegister: PropTypes.func.isRequired,
    resetErrorCode: PropTypes.func.isRequired,
    device: PropTypes.object,
    status: PropTypes.string,
    errorData: PropTypes.string,
    errorCode: PropTypes.string,
    apiErrors: PropTypes.object,
  };

  state = {
    initialValues,
    isShowDatePicker: false,
    isModalVisible: false,
  };

  async componentDidMount() {
    __DEV__ && console.log('@Enter UserRegisterScreen!');
    this.props.resetErrorCode();
  }

  onTriggerDatePicker = (isOpen) => () => {
    this.setState({
      isShowDatePicker: isOpen,
    });
  };

  renderErrorMessage = (fieldName) => {
    const { errorData } = this.props;
    return (
      has(errorData, `errors.${fieldName}`) &&
      errorData.errors[fieldName]
        .map((e, i) => `${t(e)}${i < errorData.errors[fieldName].length && '\n'}`)
        .toString()
    );
  };
  back = () => {
    this.setState({
      isModalVisible: false,
    });

    Actions.UserLoginScreen();
  };
  agree = () => {
    this.setState({
      isModalVisible: false,
    });
  };
  renderHtml = () => {
    const { currentLocales } = this.props;
    if (currentLocales && currentLocales.length > 0) {
      switch (currentLocales[0].languageCode) {
        case 'en':
        default:
          return { html: html_en };
        case 'zh':
          return { html: html_zh };
        case 'th':
          return { html: html_th };
      }
    } else {
      return { html: html_en };
    }
  };
  renderMainView({ dirty, values, errors, handleSubmit, handleChange }) {
    const { currentLocales, errorCode, apiErrors } = this.props;
    const { isShowDatePicker } = this.state;

    console.log('=== apiErrors ===', apiErrors);
    return (
      <View style={Classes.fill}>
        <SecondaryNavbar navTitle={t('register_nav_title')} backConfirm={dirty} back />
        <ScrollView>
          <View style={styles.imageBox}>
            <Image source={Images.createAccount} style={styles.imgLogo} />
          </View>
          <View style={[Classes.fill, styles.layout]}>
            <HorizontalTextInput
              getRef={(r) => {
                this._textInputRef = r;
              }}
              title={t('register_input_email_title')}
              placeholder={t('register_input_email_placeholder')}
              onChangeText={handleChange('email')}
              error={
                errors.email ||
                (apiErrors && apiErrors.Email) ||
                this.renderErrorMessage('email')
              }
              value={values.email}
              paddingVertical={0}
              autoCapitalize="none"
            />
            <HorizontalTextInput
              getRef={(r) => {
                this._textInputRef = r;
              }}
              title={t('register_input_password_title')}
              placeholder={t('register_input_password_placeholder')}
              onChangeText={handleChange('password')}
              error={
                errors.password ||
                (apiErrors && apiErrors.Password) ||
                this.renderErrorMessage('password')
              }
              value={values.password}
              autoCapitalize="none"
              maxLength={16}
              password
            />
            <HorizontalTextInput
              getRef={(r) => {
                this._textInputRef = r;
              }}
              title={t('register_input_confirm_password_title')}
              placeholder={t('register_input_confirm_password_placeholder')}
              onChangeText={handleChange('passwordConfirmation')}
              error={
                errors.passwordConfirmation ||
                (apiErrors && apiErrors.PasswordConfirmation) ||
                this.renderErrorMessage('passwordConfirmation')
              }
              value={values.passwordConfirmation}
              autoCapitalize="none"
              maxLength={16}
              password
            />
          </View>
        </ScrollView>
        <View style={[Classes.margin, Classes.mainEnd, Classes.marginBottom]}>
          <SquareButton
            textStyle={styles.loginStyle}
            text={t('login_create_an_account')}
            disabledOpacity={0.5}
            onPress={handleSubmit}
            radius={6}
          />
        </View>
      </View>
    );
  }

  render() {
    const { fetchUserTempRegister } = this.props;
    return (
      <DismissKeyboard>
        <KeyboardAvoidingView
          style={Classes.fill}
          contentContainerStyle={Classes.fill}
          keyboardVerticalOffset={Platform.select({
            ios: isIphoneX()
              ? Screen.scale(48)
              : Platform.isPad
              ? Screen.scale(12)
              : Screen.scale(14),
            android: Screen.scale(28),
          })}
          behavior={Platform.OS === 'ios' ? 'height' : 'height'}
          enable
        >
          <Formik
            initialValues={this.state.initialValues}
            validationSchema={validationSchema}
            onSubmit={fetchUserTempRegister}
            validateOnChange={false}
          >
            {(formikProps) => this.renderMainView(formikProps)}
          </Formik>
          <View style={{ marginTop: 50 }}>
            <Modal
              animationIn="slideInUp"
              animationOut="slideOutDown"
              isVisible={this.state.isModalVisible}
              style={{
                marginTop: 50,
                backgroundColor: 'white',
                maxHeight: Dimensions.get('window').height - 100,
              }}
            >
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <View style={Classes.padding}>
                  <Text style={[Fonts.style.h7, Fonts.style.bold, Classes.mainEnd]}>
                    {t('setting_privacy_policy')}
                  </Text>
                  <Text style={styles.txtDate}>
                    {t('setting_privacy_effective_date')}
                    {' : '}
                    {'October 15, 2019'}
                  </Text>
                </View>

                <View style={styles.divider} />
                <View style={[Classes.padding, Classes.fill, { paddingBottom: 60 }]}>
                  <WebView source={this.renderHtml()} />
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  position: 'absolute',
                  bottom: 0,
                }}
              >
                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity style={{ padding: 15, width: '50%' }}>
                    <SquareButton
                      borderOutline
                      textStyle={styles.loginStyle}
                      text={t('back')}
                      radius={6}
                      style={[{ height: 32 }]}
                      onPress={() => this.back()}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity style={{ padding: 15, width: '50%' }}>
                    <SquareButton
                      textStyle={styles.loginStyle}
                      text={t('__ok')}
                      radius={6}
                      testID="btn_login"
                      accessible={true}
                      accessibilityLabel={'btn_login'}
                      style={[{ height: 32 }]}
                      onPress={() => this.agree()}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
        </KeyboardAvoidingView>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({
    currentLocales: state.appState.currentLocales,
    currentTimeZone: state.appState.currentTimeZone,
    status: state.user.status,
    device: state.bleDevice.lastDevice,
    errorCode: state.user.errorCode,
    apiErrors: state.user.errors,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        resetErrorCode: UserActions.resetErrorCode,
        fetchUserRegister: UserActions.fetchUserRegister,
        fetchUserTempRegister: UserActions.fetchUserTempRegister,
      },
      dispatch,
    ),
)(UserRegisterScreen);
