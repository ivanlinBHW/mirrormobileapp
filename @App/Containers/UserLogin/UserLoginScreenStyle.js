import { ScaledSheet } from 'App/Helpers';
import { Metrics, Fonts, Colors } from 'App/Theme';

export default ScaledSheet.create({
  layout: {
    marginHorizontal: Metrics.baseMargin,
    height: '100%',
    flex: 1,
  },
  imageBox: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '70@vs',
    marginBottom: '60@vs',
  },
  headerText: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    color: Colors.titleText.primary,
  },
  inputBox: {
    height: '38@s',
    paddingRight: 0,
    paddingLeft: 0,
    borderRadius: 6,
    backgroundColor: Colors.inputBox.primary.background,
    marginBottom: Metrics.baseMargin,

    elevation: 2,
    shadowColor: Colors.inputBox.primary.shadow,
    shadowOpacity: 0.4,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 3,
    },
  },
  wrapper: {
    borderBottomWidth: 0,
  },
  textStyle: {
    fontSize: Fonts.size.regular,
    marginLeft: Metrics.baseMargin,
  },
  loginStyle: {
    ...Fonts.style.medium500,
  },
  createBtn: {
    marginTop: Metrics.baseMargin * 2,
  },
  imgLogo: {
    width: '60@s',
    height: '60@s',
  },
  footerBox: {
    marginTop: '60@vs',
  },
  footerText: {
    ...Fonts.style.small500,
    textAlign: 'center',
    marginTop: Metrics.baseMargin,
    color: Colors.titleText.secondary,
  },
  btnbox: {
    marginTop: '24@vs',
  },
  loginBtn: {
    backgroundColor: '#853E60',
  },
  errorTextBox: {
    textAlign: 'center',
    marginTop: '20@vs',
    marginLeft: '10@s',
  },
  errorText: {
    color: Colors.titleText.danger,
  },
  errorStyle: {
    fontSize: Fonts.size.medium,
    color: Colors.titleText.danger,
    marginTop: Metrics.baseMargin / 2,
  },
  marginTop18: {
    marginTop: '18@s',
  },
  ForgotPasswordLink: {
    ...Fonts.style.underline,
    marginTop: '36@s',
    color: '#393939',
  },
});
