import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView, FlatList, View, Text, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import {
  SecondaryNavbar,
  BaseButton,
  BaseInput,
  BaseStatusAvatar,
  BouncyCheckbox,
  BaseEditAvatar,
} from 'App/Components';
import { Images, Classes, Colors } from 'App/Theme';
import { Date as d } from 'App/Helpers';
import styles from './FriendSelectScreenStyle';
import { CommunityActions } from 'App/Stores/index';

class FriendSelectScreen extends React.Component {
  static propTypes = {
    eventType: PropTypes.string,
    list: PropTypes.array.isRequired,
    token: PropTypes.string,
    timezone: PropTypes.string.isRequired,
    getSearchFriends: PropTypes.func.isRequired,
    saveSelectedFriends: PropTypes.func.isRequired,
    selected: PropTypes.array,
    trainingEventId: PropTypes.string,
  };

  static defaultProps = {
    selected: [],
    friendType: 'event',
    trainingEventId: null,
  };

  state = {
    text: '',
    friendList: [],
    selected: [],
  };

  componentDidMount() {
    __DEV__ && console.log('@FriendSelectScreen');
    const { getSearchFriends, selected, eventType, trainingEventId } = this.props;
    getSearchFriends({
      text: '',
      friendType: eventType,
      trainingEventId,
    });
    this.setState({
      selected,
    });
  }

  componentDidUpdate(prevProps) {
    const {
      trainingEventId,
      eventType,
      getSearchFriends,
      saveSelectedFriends,
    } = this.props;
    if (eventType !== prevProps.eventType) {
      getSearchFriends({
        text: this.state.text,
        friendType: eventType,
        trainingEventId,
      });
      saveSelectedFriends([]);
      this.setState({
        selected: [],
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list !== this.props.list) {
      if (nextProps.list.length > 0) {
        const temp = [];
        nextProps.list.forEach((item) => {
          temp.push({
            ...item,
            isSelect:
              this.state.selected.filter((select) => select.id === item.id).length > 0,
          });
        });
        this.setState({
          friendList: temp,
        });
      } else {
        this.setState({
          friendList: [],
        });
      }
    }
  }

  onPressSelected = (data) => {
    const { selected, friendList } = this.state;
    let temp = JSON.parse(JSON.stringify(selected));
    if (temp.length > 0 && temp.filter((item) => item.id === data.id).length > 0) {
      let tempSelected = [];
      temp.forEach((select) => {
        if (select.id !== data.id) {
          tempSelected.push(select);
        }
      });
      temp = tempSelected;
    } else {
      temp.push(data);
    }
    let tempFriendList = JSON.parse(JSON.stringify(friendList));
    tempFriendList.map((item, index) => {
      if (item.id === data.id) {
        tempFriendList[index].isSelect = !tempFriendList[index].isSelect;
      }
    });
    this.setState({
      selected: temp,
      friendList: tempFriendList,
    });
  };

  onPressSearch = (value) => {
    const { getSearchFriends, eventType, trainingEventId } = this.props;
    this.setState({ text: value });
    getSearchFriends({
      text: value,
      friendType: eventType,
      trainingEventId,
    });
  };

  renderItem = ({ item, index }) => {
    const { lastOnlineMinuteAgo = 0 } = item;
    const { selected } = this.state;
    const { eventType } = this.props;
    const multiSelect = eventType === 'group';
    const disabled =
      (!multiSelect && !item.isSelect && selected.length > 0) || !item.isSubscribed;
    return (
      <BaseButton
        transparent
        style={styles.itemWrapper}
        onPress={() => this.onPressSelected(item)}
        throttleTime={0}
        disabled={disabled}
      >
        <View
          style={[
            styles.itemWrapper,
            { backgroundColor: item.isSelect ? Colors.white_02 : Colors.white },
          ]}
        >
          <View style={styles.leftItem}>
            <View style={styles.avatarBox}>
              <BaseStatusAvatar
                uri={item.signedAvatarUrl}
                isOnline={item.currentStatus === 1}
              />
            </View>
            <View style={styles.itemTitleBox}>
              <Text style={styles.itemTitle}>{item.fullName}</Text>
              <Text style={styles.itemSecondTitle}>
                {item.currentStatus === 1
                  ? 'online'
                  : d.transformLastOnlineDiff(lastOnlineMinuteAgo)}
              </Text>
            </View>
          </View>
          <View style={styles.rightItem}>
            <BouncyCheckbox
              isChecked={item.isSelect}
              fillColor={Colors.black}
              checkboxSize={22}
              text=""
              borderColor={Colors.gray_03}
              onPress={disabled ? () => {} : () => this.onPressSelected(item)}
            />
          </View>
        </View>
      </BaseButton>
    );
  };

  onPressRemove = (data) => {
    const { selected, friendList } = this.state;
    let tempSelected = JSON.parse(JSON.stringify(selected));
    let tempFriendList = JSON.parse(JSON.stringify(friendList));
    let select = [];
    tempSelected.forEach((item) => {
      if (item.id !== data.id) {
        select.push(item);
      }
    });
    tempFriendList.map((item, index) => {
      if (item.id === data.id) {
        tempFriendList[index].isSelect = !tempFriendList[index].isSelect;
      }
    });
    this.setState({
      selected: select,
      friendList: tempFriendList,
    });
  };

  renderSelectedItem = ({ item, index }) => {
    return (
      <View style={styles.selectedWrapper}>
        <View style={styles.editAvatarBox}>
          <BaseEditAvatar
            onPress={() => this.onPressRemove(item)}
            uri={item.signedAvatarUrl}
          />
        </View>
        <Text numberOfLines={1} style={styles.selectedText}>
          {item.fullName}
        </Text>
      </View>
    );
  };

  onPressSave = () => {
    const { saveSelectedFriends } = this.props;
    const { selected } = this.state;
    saveSelectedFriends(selected);
    Actions.pop();
  };

  isShowBackConfirm = () => {
    const { selected = [] } = this.state;
    const { selected: propsSelected = [] } = this.props;

    if (propsSelected.length !== selected.length) {
      return true;
    } else {
      if (propsSelected.length > 0) {
        propsSelected.sort((a, b) => {
          return a.id - b.id;
        });

        selected.sort((a, b) => {
          return a.id - b.id;
        });
        let i = 0;
        propsSelected.map((item, index) => {
          if (item.id !== selected[index].id) {
            i += 1;
          }
        });
        if (i !== 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  };

  renderEmpty = () => {
    return (
      <View style={styles.emptyBox}>
        <Text style={styles.emptyTitle}>{t('see_all_empty_title')}</Text>
        <Text style={styles.emptyContent}>{t('see_all_empty_content')}</Text>
      </View>
    );
  };

  render() {
    const { text, friendList, selected = [] } = this.state;
    return (
      <SafeAreaView style={styles.wrapper}>
        <SecondaryNavbar
          back
          backConfirm={this.isShowBackConfirm()}
          backConfirmDesc={t('community_back_alert_friend_content')}
          navTitle={
            selected.length > 0
              ? `${t('__selected', { count: selected.length })}`
              : `${t('community_friends')}`
          }
          navRightComponent={
            <BaseButton
              throttleTime={0}
              transparent
              style={styles.saveBtn}
              onPress={this.onPressSave}
            >
              <Text style={styles.saveText}>{t('setting_edit_profile_save')}</Text>
            </BaseButton>
          }
        />

        <View style={styles.searchWrapper}>
          <BaseInput
            leftComponent={<Image source={Images.search_gray} style={styles.searchImg} />}
            leftIconContainerStyle={styles.leftIconContainerStyle}
            inputContainerStyle={styles.inputContainerStyle}
            containerStyle={styles.inputStyle}
            placeholder={t('search_title')}
            value={text}
            onChangeText={(value) => this.onPressSearch(value)}
          />
        </View>
        <View style={Classes.fill}>
          <FlatList
            data={friendList}
            renderItem={this.renderItem}
            ListEmptyComponent={this.renderEmpty()}
          />
        </View>
        {selected.length > 0 && (
          <View style={styles.bottomWrapper}>
            <FlatList
              horizontal
              data={selected}
              showsHorizontalScrollIndicator={false}
              renderItem={this.renderSelectedItem}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    list: state.community.friends,
    selected: state.community.friendSelected,
    timezone: state.appState.currentTimeZone,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getSearchFriends: CommunityActions.getSearchFriends,
        saveSelectedFriends: CommunityActions.saveSelectedFriends,
      },
      dispatch,
    ),
)(FriendSelectScreen);
