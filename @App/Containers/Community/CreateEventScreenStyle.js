import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Metrics, Colors, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  cardListFooter: {
    paddingBottom: 128,
    marginBottom: '-16@vs',
  },
  btnRound: {
    flex: 1,
    height: '32@sr',
    borderRadius: 22,
  },
  slideDot: {
    width: '7@s',
    height: '7@s',
    borderRadius: '3.5@s',
    marginLeft: 0,
    marginRight: '9@s',
    backgroundColor: Colors.deepBlack,
  },
  titleStyle: {
    ...Fonts.style.medium500,
  },
  slideDotBackground: {
    backgroundColor: Colors.white,
  },
  slideImageWrapper: {
    flex: 1,
    borderColor: Colors.white,
    borderRadius: '12@s',
    borderWidth: 1,
    overflow: 'hidden',
    height: '230@s',
    width: Screen.width - Metrics.baseMargin * 2,
  },
  slideImage: {
    flex: 1,
    position: 'absolute',
    height: '240@s',
    width: '100%',
  },
  slideCheckbox: {
    position: 'absolute',
    top: '10%',
    right: '8%',
  },
  inputWrapper: {
    paddingHorizontal: Metrics.baseMargin,
  },
  textInput: {
    height: '104@vs',
    borderWidth: 1,
    borderColor: '#545454',
    marginTop: Metrics.baseMargin,
    marginBottom: '35@vs',
    textAlignVertical: 'top',
    width: Screen.width - Screen.scale(32),
    paddingTop: Metrics.baseMargin / 2,
  },
  dateInput: {
    color: Colors.gray_02,
    ...Fonts.style.medium,
  },
  marginTop: {
    marginTop: '-24@vs',
  },
  errorMessage: {
    color: Colors.error,
    paddingTop: Metrics.baseMargin / 6,
    ...Fonts.style.small500,
  },
  publicEventSwitchText: {
    fontSize: 14,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: Colors.titleText.secondary,
  },
});
