import React from 'react';
import PropTypes from 'prop-types';
import * as yup from 'yup';
import { isEmpty, isArray, first, get } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Formik } from 'formik';
import { Actions } from 'react-native-router-flux';
import {
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  Keyboard,
  FlatList,
  Platform,
  Alert,
  Text,
  View,
} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import { CommunityActions, SettingActions } from 'App/Stores';
import { Screen, Date as d, ifIphoneX } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { Classes, Metrics, Colors, Fonts } from 'App/Theme';
import {
  CalendarPickerModal,
  DateTimePickerModal,
  CommunityClassItem,
  VerticalTextInput,
  DismissKeyboard,
  SecondaryNavbar,
  SquareButton,
  HorizontalLine,
  BouncyCheckbox,
  ImageButton,
  BaseSwitch,
  HeadLine,
  RoundButton,
} from 'App/Components';

import styles from './CreateEventScreenStyle';

const yString = yup.string();

class CreateEventScreen extends React.Component {
  static propTypes = {
    mainSubscription: PropTypes.object,
    coverImages: PropTypes.array.isRequired,
    createNewEvent: PropTypes.func.isRequired,
    selectedFriends: PropTypes.array.isRequired,
    selectedClasses: PropTypes.array.isRequired,
    updateSelectedClasses: PropTypes.func.isRequired,
    updateSelectedFriends: PropTypes.func.isRequired,
    getEventClassDetail: PropTypes.func.isRequired,
    resetEventData: PropTypes.func.isRequired,
    currentLocales: PropTypes.array.isRequired,
  };

  static defaultProps = {
    coverImages: [],
    selectedFriends: [],
    selectedClasses: [],
    mainSubscription: null,
  };

  validationSchema = yup.object().shape({
    coverImage: yString.required(
      t('community_event_creation_validation_error_cover_image'),
    ),
    title: yString.required(t('community_event_creation_validation_error_title')),
    description: yString.required(t('community_event_creation_validation_error_desc')),
    startDate: yString.required(
      t('community_event_creation_validation_error_start_date'),
    ),
    dueDate: yString.required(t('community_event_creation_validation_error_due_date')),
    isPublic: yup.bool(),
    canInvite: yup.bool(),
  });

  initialValues = {
    coverImage: '',
    title: __DEV__ ? `test case ${d.moment().format('HHmmss')}` : '',
    description: __DEV__ ? 'tests tests' : '',
    startDate: d.formatDate(new Date()).toString(),
    dueDate: d.formatDate(d.moment().add(1, 'days')).toString(),
    isPublic: true,
    canInvite: true,
    eventType: 'group',
    eventBeginType: 'now',
  };

  scrollView = null;

  state = {
    activeSlide: 0,
    isShowDatePicker: false,
    isShowTimePicker: false,
    isEverSubmitted: false,
    initialValues: this.initialValues,
    time: new Date(),
    acceptCreateEventPolicy: false,
    acceptChangePolicy: false,
    minDate: new Date(),
    maxDate: new Date(d.moment(new Date()).add(30, 'days')),
    computedCoverImages: [],
  };

  componentDidMount() {
    __DEV__ && console.log('@CreateEventScreen');
    const { updateSelectedClasses, updateSelectedFriends } = this.props;
    updateSelectedClasses([]);
    updateSelectedFriends([]);
  }

  componentDidUpdate(prevProps) {
    this.updateCoverImage(prevProps);
  }

  updateCoverImage(prevProps) {
    const { selectedClasses = [] } = this.props;

    if (selectedClasses !== prevProps.selectedClasses) {
      if (this.setFieldValue && isArray(selectedClasses) && !isEmpty(selectedClasses)) {
        this.setFieldValue('coverImage', first(selectedClasses).coverImage);
      } else if (isEmpty(selectedClasses)) {
        this.setFieldValue(
          'coverImage',
          get(first(this.props.coverImages), 'coverImageUrl'),
        );
      }
    }
  }

  eventPolicyModelFinishCallback = (acceptCreateEventPolicy) => {
    console.log(
      '=== eventPolicyModelFinishCallback acceptCreateEventPolicy ===',
      acceptCreateEventPolicy,
    );
    this.setState({ acceptCreateEventPolicy });
    Actions.pop();

    let { privacy } = this.props;

    console.log('=== privacy ===', privacy);

    if (privacy != 1) {
      Actions.EventPolicyChangeModel({
        acceptChangePolicy: this.state.acceptChangePolicy,
        finishCallback: this.changePolicyModelFinishCallback,
      });
    }
  };
  changePolicyModelFinishCallback = (acceptChangePolicy) => {
    let { updateSetting } = this.props;
    this.setState({ acceptChangePolicy });
    updateSetting({ privacy: 1 });
    Actions.pop();
  };
  handleSubmit = (values) => {
    const { selectedClasses, selectedFriends } = this.props;
    const payload = {
      isPublic: false,
      canInvite: false,
      ...values,
      trainingClasses: selectedClasses.map((e) => e.id),
      inviteUserIds: selectedFriends.map((e) => e.id),
      usersCount: 100,
    };
    if (values.eventType === 'group') {
      payload.eventType = 0;
      payload.startDate = d.toUTC(
        d.moment(values.startDate, 'YYYY/MM/DD'),
        d.FORMAT_YY_MM_DD_HH_MM_SS,
      );
      payload.dueDate = d.toUTC(
        d.moment(values.dueDate, 'YYYY/MM/DD'),
        d.FORMAT_YY_MM_DD_HH_MM_SS,
      );
    } else if (values.eventType === '1on1') {
      payload.isPublic = false;
      payload.canInvite = false;
      payload.eventType = 1;
      if (values.eventBeginType === 'now') {
        payload.startDate = '';
        payload.dueDate = '';
      } else if (values.eventBeginType === 'reservation') {
        console.log('handleSubmit values=>', values);
        const date = `${values.startDate} ${values.dueDate}`;
        payload.startDate = d.toUTC(
          d.moment(date, 'YYYY/MM/DD HH:mm'),
          d.FORMAT_YY_MM_DD_HH_MM_SS,
        );
        payload.dueDate = d.toUTC(
          d.moment(date, 'YYYY/MM/DD HH:mm').add(1, 'hours'),
          d.FORMAT_YY_MM_DD_HH_MM_SS,
        );

        console.log('handleSubmit payload=>', payload);
      }
    }
    console.log('payload=>', payload);
    const { createNewEvent } = this.props;

    return selectedClasses.length >= 1 && createNewEvent(payload);
  };

  onDateChange = (setFieldValue) => (date, type) => {
    const formattedDate = d.formatDate(date).toString();
    if (type === 'END_DATE' && formattedDate) {
      setFieldValue('dueDate', formattedDate);
    } else if (formattedDate) {
      setFieldValue('startDate', formattedDate);
      setFieldValue('dueDate', null);
      this.setState({
        maxDate: new Date(d.moment(formattedDate, 'YYYY/MM/DD').add(30, 'days')),
      });
    }
  };

  onTimeChange = (setFieldValue) => (date) => {
    const { timePickerMode } = this.state;
    if (timePickerMode === 'date') {
      const formattedDate = d.formatDate(date).toString();
      setFieldValue('startDate', formattedDate);
    } else if (timePickerMode === 'time') {
      const formattedDate = d.formatDate(date, 'HH:mm').toString();
      setFieldValue('dueDate', formattedDate);
    }
    this.setState({
      time: date,
    });
  };

  onTriggerDatePicker = (isOpen) => () => {
    Keyboard.dismiss();
    if (this.scrollView) {
      this.scrollView.scrollTo({ y: Screen.height * 0.7 });
    }
    this.setState({
      isShowDatePicker: isOpen,
    });
  };

  onTriggerTimePicker = (isOpen, mode) => () => {
    Keyboard.dismiss();
    if (this.scrollView) {
      this.scrollView.scrollTo({ y: Screen.height * 0.7 });
    }
    this.setState({
      isShowTimePicker: isOpen,
      timePickerMode: mode,
    });
  };

  onDeleteSelectedClass = (id) => () => {
    const { updateSelectedClasses, selectedClasses } = this.props;
    updateSelectedClasses(selectedClasses.filter((e) => e.id !== id));
  };

  onPress1on1Reservation = (handleChange) => () => {
    handleChange('eventBeginType')('reservation');
    handleChange('dueDate')(d.formatDate(new Date(), 'HH:mm').toString());
  };

  onPress1on1Now = (handleChange) => () => {
    handleChange('eventBeginType')('now');
  };

  onPressEventTypeGroup = (handleChange) => () => {
    const {
      updateSelectedFriends,
      resetEventData,
      selectedClasses,
      selectedFriends,
    } = this.props;
    handleChange('eventType')('group');
    handleChange('dueDate')(d.formatDate(d.moment().add(1, 'days')).toString());
    if (selectedClasses && selectedClasses.length > 0) {
      resetEventData();
    }
    if (selectedFriends && selectedFriends.length > 0) {
      updateSelectedFriends([]);
    }
    setTimeout(() => this.scrollView.scrollToEnd(), 250);
  };

  onPressEventType1on1 = (handleChange) => () => {
    const {
      updateSelectedFriends,
      resetEventData,
      selectedClasses,
      selectedFriends,
    } = this.props;
    handleChange('eventType')('1on1');
    handleChange('eventBeginType')('now');
    if (selectedClasses && selectedClasses.length > 0) {
      resetEventData();
    }
    if (selectedFriends && selectedFriends.length > 0) {
      updateSelectedFriends([]);
    }
    setTimeout(() => this.scrollView.scrollToEnd(), 250);
  };

  handleSnapToItem = (index) => this.setState({ activeSlide: index });

  handleOnPress = (item, values, setFieldValue) => () => {
    if (values.coverImage && values.coverImage === item.coverImageUrl) {
      setFieldValue('coverImage', '');
    } else {
      setFieldValue('coverImage', item.coverImageUrl);
    }
  };

  renderSlideItem = (setFieldValue, values) => ({ item }) => (
    <View style={Classes.fillCenter}>
      <View style={[Classes.paddingHorizontal, Classes.fullWidth]}>
        <ImageButton
          style={styles.slideImageWrapper}
          imageStyle={styles.slideImage}
          source={item.signedCoverImageObj || { uri: item.signedCoverImage }}
          throttleTime={10}
          resizeMode="cover"
          imageSize="2x"
          imageType="h"
        />
      </View>
      <View style={styles.slideCheckbox}>
        {!isEmpty(this.props.updateSelectedClasses) && (
          <BouncyCheckbox
            onPress={this.handleOnPress(item, values, setFieldValue)}
            isChecked={values.coverImage === item.coverImageUrl}
            fillColor={Colors.black}
            borderColor={Colors.gray_03}
            checkboxSize={Screen.scale(22)}
            borderRadius={Screen.scale(22) / 2}
          />
        )}
      </View>
    </View>
  );

  renderSlide = ({ setFieldValue, setFieldTouched, values, errors }) => {
    const { coverImages = [], selectedClasses = [] } = this.props;
    const { activeSlide } = this.state;

    const selectedClassCoverImages = (selectedClasses || []).map((e) => ({
      coverImageUrl: e.coverImage,
      signedCoverImage: e.signedCoverImage,
    }));
    const slideData = isEmpty(selectedClasses)
      ? isEmpty(coverImages)
        ? []
        : [first(coverImages)]
      : [first(selectedClassCoverImages)];
    if (!this.setFieldValue) {
      this.setFieldValue = setFieldValue;
    }
    return (
      <>
        <Carousel
          data={slideData}
          renderItem={this.renderSlideItem(setFieldValue, values)}
          onSnapToItem={this.handleSnapToItem}
          sliderWidth={Screen.width}
          itemWidth={Screen.width}
          inactiveSlideScale={1}
        />
        <View style={Classes.center}>
          <Text style={styles.errorMessage}>{errors.coverImage}</Text>
        </View>
        <Pagination
          dotsLength={slideData ? slideData.length : 0}
          activeDotIndex={activeSlide}
          containerStyle={styles.slideDotBackground}
          dotStyle={styles.slideDot}
          inactiveDotOpacity={0.3}
          inactiveDotScale={1}
        />
      </>
    );
  };

  renderEventForm = ({ values, handleChange, errors, setFieldValue, isValid, dirty }) => {
    const { selectedFriends, mainSubscription } = this.props;
    const { isEverSubmitted } = this.state;
    const { isPublic, canInvite } = values;
    return (
      <View style={styles.inputWrapper}>
        <VerticalTextInput
          title={t('community_event_creation_input_event_title')}
          placeholder={t('community_event_creation_input_title_placeholder')}
          onChangeText={handleChange('title')}
          titleStyle={styles.titleStyle}
          error={errors.title}
          value={values.title}
          maxLength={24}
          required
          border
          bold
        />

        <VerticalTextInput
          title={t('community_event_creation_input_event_desc')}
          placeholder={t('community_event_creation_input_desc_placeholder')}
          onChangeText={handleChange('description')}
          error={errors.description}
          value={values.description}
          titleStyle={styles.titleStyle}
          numberOfLines={5}
          maxLength={300}
          required
          border
          bold
        />

        <HeadLine
          title={t('community_event_type')}
          hrLine={false}
          height={24}
          paddingTop={0}
          paddingHorizontal={0}
        />

        <View style={[Classes.fillRow, Classes.center, Classes.paddingBottom]}>
          <RoundButton
            height={Screen.scale(32)}
            text={t('community_event_type_group')}
            style={[styles.btnRound, Classes.marginRightHalf]}
            textStyle={Fonts.style.mediumBold}
            textColor={
              values.eventType === 'group'
                ? Colors.button.primary.content.text
                : Colors.button.primary.outline.text
            }
            color={
              values.eventType === 'group'
                ? Colors.button.primary.content.background
                : Colors.button.primary.outline.background
            }
            outline={values.eventType !== 'group'}
            onPress={this.onPressEventTypeGroup(handleChange)}
            disabled={isEmpty(mainSubscription)}
            bold
          />
          <RoundButton
            transparent
            height={Screen.scale(32)}
            text={t('community_event_type_1on1')}
            style={[styles.btnRound, Classes.marginLeftHalf]}
            textStyle={Fonts.style.mediumBold}
            textColor={
              values.eventType === '1on1'
                ? Colors.button.primary.content.text
                : Colors.button.primary.outline.text
            }
            color={
              values.eventType === '1on1'
                ? Colors.button.primary.content.background
                : Colors.button.primary.outline.background
            }
            outline={values.eventType !== '1on1'}
            onPress={this.onPressEventType1on1(handleChange)}
            disabled={isEmpty(mainSubscription)}
            bold
          />
        </View>

        {values.eventType === '1on1' && (
          <View style={Classes.fill}>
            <HeadLine
              title={t('community_event_1on1_begin_time')}
              hrLine={false}
              height={24}
              paddingTop={0}
              paddingHorizontal={0}
            />
            <View style={[Classes.fillRow, Classes.center]}>
              <TouchableOpacity
                style={[Classes.fillRow, Classes.margin]}
                onPress={this.onPress1on1Now(handleChange)}
              >
                <BouncyCheckbox
                  onPress={this.onPress1on1Now(handleChange)}
                  isChecked={values.eventBeginType === 'now'}
                  borderColor={Colors.gray_03}
                  fillColor={Colors.black}
                  checkboxSize={22}
                  text=""
                />
                <Text>{t('community_event_1on1_now')}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[Classes.fillRow, Classes.margin]}
                onPress={this.onPress1on1Reservation(handleChange)}
              >
                <BouncyCheckbox
                  onPress={this.onPress1on1Reservation(handleChange)}
                  isChecked={values.eventBeginType === 'reservation'}
                  borderColor={Colors.checkBox.primary.border}
                  fillColor={Colors.checkBox.primary.background}
                  checkboxSize={22}
                  text=""
                />
                <Text>{t('community_event_1on1_reservation')}</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}

        {values.eventType === 'group' && (
          <>
            <HeadLine
              title={t('community_event_creation_input_choose_date')}
              hrLine={false}
              height={24}
              paddingTop={0}
              paddingHorizontal={0}
            />
            <View style={[Classes.fillRow, Classes.mainStart]}>
              <View style={Classes.fill}>
                <VerticalTextInput
                  title={t('community_event_creation_input_from')}
                  titleStyle={styles.dateInput}
                  placeholder={t('community_event_creation_input_choose_date')}
                  onPress={this.onTriggerDatePicker(true)}
                  error={errors.startDate}
                  value={values.startDate}
                  inputAlign="center"
                  required
                  border
                />
              </View>
              <View style={{ width: Metrics.baseMargin }} />
              <View style={Classes.fill}>
                <VerticalTextInput
                  title={t('community_event_creation_input_to')}
                  titleStyle={styles.dateInput}
                  placeholder={t('community_event_creation_input_choose_date')}
                  onPress={this.onTriggerDatePicker(true)}
                  error={errors.dueDate}
                  value={values.dueDate}
                  inputAlign="center"
                  required
                  border
                />
              </View>
            </View>
          </>
        )}

        {values.eventType === '1on1' && values.eventBeginType === 'reservation' && (
          <>
            <HeadLine
              title={t('community_event_creation_input_choose_date')}
              hrLine={false}
              height={24}
              paddingTop={0}
              paddingHorizontal={0}
            />
            <View style={[Classes.fillRow, Classes.mainStart]}>
              <View style={Classes.fill}>
                <VerticalTextInput
                  title={t('community_event_creation_input_from')}
                  titleStyle={styles.dateInput}
                  placeholder={t('community_event_creation_input_desc_placeholder')}
                  onPress={this.onTriggerTimePicker(true, 'date')}
                  error={errors.startDate}
                  value={values.startDate}
                  inputAlign="center"
                  required
                  border
                />
              </View>
              <View style={{ width: Metrics.baseMargin }} />
              <View style={Classes.fill}>
                <VerticalTextInput
                  title={t('setting_mii_display_time')}
                  titleStyle={styles.dateInput}
                  placeholder={t('community_event_creation_input_desc_placeholder')}
                  onPress={this.onTriggerTimePicker(true, 'time')}
                  error={errors.dueDate}
                  value={values.dueDate}
                  inputAlign="center"
                  required
                  border
                />
              </View>
            </View>
          </>
        )}

        <HeadLine
          title={t('community_search_btn_invite_friend')}
          subTitle={
            isEverSubmitted && values.eventType === '1on1' && selectedFriends.length < 1
              ? t('community_at_least_select_one_friend')
              : undefined
          }
          hrLine={false}
          paddingTop={2}
          onPress={() =>
            Actions.FriendSelectScreen({
              hideTabBar: true,
              eventType: values.eventType === '1on1' ? 'OneOnOne' : values.eventType,
            })
          }
        />

        {values.eventType !== '1on1' && (
          <HeadLine
            title={t('community_event_creation_input_public')}
            hrLine={false}
            paddingTop={2}
            rightComponent={
              <BaseSwitch
                active={isPublic}
                onValueChange={(val) => setFieldValue('isPublic', val)}
              />
            }
          />
        )}

        {values.eventType !== '1on1' && (
          <HeadLine
            title={t('community_event_creation_input_Participant_invite')}
            hrLine={false}
            paddingTop={2}
            rightComponent={
              <BaseSwitch
                active={canInvite}
                onValueChange={(val) => setFieldValue('canInvite', val)}
              />
            }
          />
        )}
      </View>
    );
  };

  renderAddedClasses = ({
    values,
    handleChange,
    errors,
    setFieldValue,
    isValid,
    dirty,
  }) => {
    const { selectedClasses, getEventClassDetail } = this.props;
    const { isEverSubmitted } = this.state;
    const renderItem = ({ item, index }) => (
      <CommunityClassItem
        {...item}
        backgroundColor={index % 2 ? Colors.white_02 : Colors.gray_04}
        onPress={this.onDeleteSelectedClass(item.id)}
        onPressDetail={() => getEventClassDetail(item.id)}
        deletable
      />
    );
    return (
      <>
        <View style={styles.inputWrapper}>
          <HeadLine
            title={t('community_event_creation_section_add_class')}
            subTitle={
              isEverSubmitted && selectedClasses.length < 1
                ? t('community_at_least_select_one_class')
                : undefined
            }
            hrLine={false}
            paddingTop={2}
            paddingBottom={selectedClasses.length < 1 ? 8 : 0}
            onPress={() =>
              Actions.CommunitySearchScreen({
                hideTabBar: true,
                multiSelect: values.eventType === 'group',
              })
            }
          />
        </View>

        <FlatList
          data={selectedClasses}
          renderItem={renderItem}
          keyExtractor={(item, index) => `eventList_${index}`}
          nestedScrollEnabled
        />
      </>
    );
  };

  onPressSubmit = async ({ handleSubmit, validateForm, isValid, values }) => {
    const { selectedClasses, selectedFriends, privacy } = this.props;
    const is1on1 = values.eventType === '1on1' && selectedFriends.length < 1;
    const errors = await validateForm();

    this.setState({ isEverSubmitted: true });

    if (this.state.acceptCreateEventPolicy == false) {
      Actions.EventPolicyModel({
        acceptCreateEventPolicy: this.state.acceptCreateEventPolicy,
        finishCallback: this.eventPolicyModelFinishCallback,
      });
    } else if (privacy != 1) {
      Actions.EventPolicyChangeModel({
        acceptChangePolicy: this.state.acceptChangePolicy,
        finishCallback: this.eventPolicyModelFinishCallback,
      });
    } else if (!isEmpty(errors) || selectedClasses.length < 1 || is1on1) {
      Alert.alert(
        t('community_event_creation_warring_title'),
        t('community_event_creation_warring_desc'),
      );
    } else {
      handleSubmit();
    }
  };

  renderMainView = (formikProps) => {
    const {
      isShowDatePicker,
      isShowTimePicker,
      timePickerMode,
      time,
      minDate,
      maxDate,
    } = this.state;
    const {
      dirty,
      isSubmitting,
      isValidating,
      values,
      errors,
      setFieldValue,
    } = formikProps;
    const { resetEventData, currentLocales } = this.props;
    return (
      <View style={Classes.fill}>
        <SecondaryNavbar
          navTitle={t('community_event_creation_title')}
          backConfirm={dirty}
          onPressBack={resetEventData}
          back
        />
        <ScrollView ref={(r) => (this.scrollView = r)}>
          <DismissKeyboard>
            <View style={Classes.fill}>
              {this.renderSlide(formikProps)}

              <HorizontalLine />
              {this.renderEventForm(formikProps)}

              <HorizontalLine />
              {this.renderAddedClasses(formikProps)}
            </View>
          </DismissKeyboard>
        </ScrollView>

        <SquareButton
          disabled={isSubmitting || isValidating}
          color={Colors.button.primary.content.background}
          onPress={() => this.onPressSubmit(formikProps)}
          text={t('community_event_creation_button_create_event')}
        />
        {isShowDatePicker && (
          <CalendarPickerModal
            onDateChange={this.onDateChange(setFieldValue)}
            onCancelPress={this.onTriggerDatePicker(false)}
            minDate={minDate}
            maxDate={maxDate}
            selectedStartDate={new Date(d.moment(values.startDate, 'YYYY/MM/DD'))}
            selectedEndDate={
              values.dueDate ? new Date(d.moment(values.dueDate, 'YYYY/MM/DD')) : null
            }
          />
        )}
        {isShowTimePicker && (
          <DateTimePickerModal
            date={time}
            onDateChange={this.onTimeChange(setFieldValue)}
            onCancelPress={this.onTriggerTimePicker(false)}
            minimumDate={minDate}
            maximumDate={maxDate}
            mode={timePickerMode}
            selectedStartDate={new Date()}
            locale={currentLocales[0] && currentLocales[0].languageCode}
          />
        )}
      </View>
    );
  };

  render() {
    return (
      <KeyboardAvoidingView
        style={Classes.fill}
        keyboardVerticalOffset={Platform.select({
          ios: ifIphoneX(
            Screen.scale(44),
            Platform.isPad ? Screen.scale(12) : Screen.scale(20),
          ),
          android: Screen.scale(28),
        })}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        enable
      >
        <View style={Classes.fullHeight}>
          <Formik
            onSubmit={this.handleSubmit}
            initialValues={this.state.initialValues}
            validationSchema={this.validationSchema}
            validateOnChange={false}
          >
            {(formikProps) => this.renderMainView(formikProps)}
          </Formik>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  (state, params) => ({
    currentLocales: state.appState.currentLocales,
    mainSubscription: state.user.mainSubscription,
    coverImages: state.community.coverImages,
    selectedClasses: state.community.classesSelected,
    selectedFriends: state.community.friendSelected,
    stashClassesSelected: state.community.stashClassesSelected,
    privacy: state.setting.setting.settings.privacy,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateSelectedClasses: CommunityActions.saveSelectedClasses,
        updateSelectedFriends: CommunityActions.saveSelectedFriends,
        createNewEvent: CommunityActions.createNewEvent,
        getEventClassDetail: CommunityActions.getEventClassDetail,
        resetEventData: CommunityActions.resetEventData,
        updateSetting: SettingActions.updateSetting,
      },
      dispatch,
    ),
)(CreateEventScreen);
