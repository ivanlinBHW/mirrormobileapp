export const slideData = [
  {
    id: 1,
    title: 'I am Title',
    coverUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 2,
    title: 'I am Title',
    coverUrl: 'https://source.unsplash.com/random',
  },

  {
    id: 3,
    title: 'I am Title',
    coverUrl: 'https://source.unsplash.com/random',
  },
];

export const classesData = [
  {
    id: 1,
    level: 1,
    duration: 30,
    title: 'I am Title',
    avatarUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 2,
    level: 2,
    duration: 30,
    title: 'I am Title',
    avatarUrl: 'https://source.unsplash.com/random',
  },

  {
    id: 3,
    level: 3,
    duration: 30,
    title: 'I am Title',
    instructor: 'Adrian Fuller',
    avatarUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 3,
    level: 3,
    duration: 30,
    title: 'I am Title',
    instructor: 'Adrian Fuller',
    avatarUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 3,
    level: 3,
    duration: 30,
    title: 'I am Title',
    instructor: 'Adrian Fuller',
    avatarUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 3,
    level: 3,
    duration: 30,
    title: 'I am Title',
    instructor: 'Adrian Fuller',
    avatarUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 3,
    level: 3,
    duration: 30,
    title: 'I am Title',
    instructor: 'Adrian Fuller',
    avatarUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 3,
    level: 3,
    duration: 30,
    title: 'I am Title',
    instructor: 'Adrian Fuller',
    avatarUrl: 'https://source.unsplash.com/random',
  },
  {
    id: 3,
    level: 3,
    duration: 30,
    title: 'I am Title',
    instructor: 'Adrian Fuller',
    avatarUrl: 'https://source.unsplash.com/random',
  },
];
