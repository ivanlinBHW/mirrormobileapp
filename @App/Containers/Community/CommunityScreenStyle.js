import { ScaledSheet } from 'App/Helpers';
import { Classes, Metrics, Colors, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    ...Classes.fill,
    marginTop: 0,
  },
  title: {
    ...Fonts.style.medium500,
    color: Colors.titleText.primary,
  },
  titleBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: Metrics.baseMargin * 3,
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  secondBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  thirdPadding: {
    paddingTop: Metrics.baseMargin * 3,
  },
  seeallBtn: {
    width: '60@s',
    height: '18@vs',
    marginRight: Metrics.baseMargin,
    marginTop: 0,
    marginBottom: 0,
    paddingTop: 0,
  },
  seeallTextColor: {
    ...Fonts.style.small500,
  },
  hrLine: {
    width: '100%',
    height: 1,
    backgroundColor: Colors.horizontalLine.primary,
    marginTop: Metrics.baseMargin,
  },
  wrapper: {
    paddingBottom: '100@vs',
  },
  btnAdd: {
    flex: 1,
    height: '40@vs',
    backgroundColor: Colors.button.primary.content.background,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: Metrics.baseMargin,
    paddingRight: Metrics.baseMargin,
    marginTop: Metrics.baseMargin,
    alignItems: 'center',
  },
  emptyTitleBox: {
    width: '60%',
  },
  emptyTitle: {
    ...Fonts.style.fontWeight500,
    fontSize: '24@vs',
    textAlign: 'center',
  },
});
