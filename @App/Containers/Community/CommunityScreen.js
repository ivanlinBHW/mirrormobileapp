import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, FlatList, Text } from 'react-native';

import { Screen } from 'App/Helpers';
import { Classes, Colors } from 'App/Theme';
import { CommunityActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { EventList, BaseButton, CommunityCard, AutoHideScrollView } from 'App/Components';

import styles from './CommunityScreenStyle';

class CommunityScreen extends React.Component {
  static propTypes = {
    getList: PropTypes.func.isRequired,
    userEventList: PropTypes.array,
    allEventList: PropTypes.array,
    timezone: PropTypes.string,
    isStandalone: PropTypes.bool,
    getAllMyEvent: PropTypes.func.isRequired,
    getAllEvents: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    getTrainingEvent: PropTypes.func.isRequired,
    joinEvent: PropTypes.func.isRequired,
  };

  static defaultProps = {
    userEventList: [],
    allEventList: [],
    isStandalone: true,
    timezone: 'Asia/Taipei',
  };

  state = {};

  componentDidMount() {
    const { getList } = this.props;
    getList();
    __DEV__ && console.log('@CommunityScreen');
  }

  renderExploreItem = (item, index) => {
    const { timezone, getTrainingEvent, joinEvent } = this.props;
    return (
      <EventList
        key={index}
        data={item}
        timezone={timezone}
        onPressAdd={() => joinEvent(item.id)}
        onPressDetail={() => getTrainingEvent(item.id)}
        backgroundColor={index % 2 ? Colors.gray_04 : Colors.white_02}
      />
    );
  };

  renderMyItem = ({ item, index }) => {
    const { timezone, getTrainingEvent } = this.props;
    return (
      index < 6 && (
        <CommunityCard
          key={index}
          data={item}
          eventType={item.eventType}
          timezone={timezone}
          size="large"
          onPress={() => getTrainingEvent(item.id)}
        />
      )
    );
  };

  renderListEmptyComponent = () => {
    return (
      <View style={[Classes.fillCenter, Classes.mainStart]}>
        <View style={styles.emptyTitleBox}>
          <Text style={styles.emptyTitle}>{t('community_empty_title')}</Text>
        </View>
      </View>
    );
  };

  renderComponent = () => {
    const { userEventList, allEventList, getAllMyEvent, getAllEvents } = this.props;
    return (
      <View>
        {userEventList.length > 0 && (
          <View style={styles.titleBox}>
            <Text style={styles.title}>{t('community_my_event')}</Text>
            {userEventList && userEventList.length > 6 && (
              <BaseButton
                style={styles.seeallBtn}
                onPress={() => getAllMyEvent()}
                text={t('see_all')}
                textStyle={styles.seeallTextColor}
                textColor={Colors.button.primary.text.text}
                transparent
              />
            )}
          </View>
        )}
        <FlatList
          horizontal
          data={userEventList}
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderMyItem}
          keyExtractor={(item, index) => `explore_${index}`}
        />
        {userEventList.length > 0 && <View style={styles.hrLine} />}

        {allEventList.length > 0 && (
          <View
            style={[styles.secondBox, userEventList.length === 0 && styles.thirdPadding]}
          >
            <Text style={styles.title}>{t('community_explore_more_event')}</Text>
            {allEventList && allEventList.length > 15 && (
              <BaseButton
                style={styles.seeallBtn}
                onPress={() => getAllEvents()}
                text={t('see_all')}
                textStyle={styles.seeallTextColor}
                textColor={Colors.button.primary.text.text}
                transparent
              />
            )}
          </View>
        )}
      </View>
    );
  };

  render() {
    const { allEventList = [], getList } = this.props;
    return (
      <AutoHideScrollView
        style={styles.container}
        showRefresh={true}
        onRefresh={getList}
      >
        <View style={[Classes.fill, { paddingBottom: Screen.verticalScale(128) }]}>
          {this.renderComponent()}
          {allEventList.length > 0 && (
            <View style={[Classes.fill]}>
              {allEventList.map((e, i) => this.renderExploreItem(e, i))}
            </View>
          )}
        </View>
        {allEventList.length === 0 && this.renderListEmptyComponent()}
      </AutoHideScrollView>
    );
  }
}

export default connect(
  (state, params) => ({
    userEventList: state.community.userEventList,
    allEventList: state.community.allEventList,
    timezone: state.appState.currentTimeZone,
    isLoading: state.appState.isLoading,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getList: CommunityActions.getList,
        getAllMyEvent: CommunityActions.getAllMyEvent,
        getAllEvents: CommunityActions.getAllEvents,
        getTrainingEvent: CommunityActions.getTrainingEvent,
        joinEvent: CommunityActions.joinEvent,
      },
      dispatch,
    ),
)(CommunityScreen);
