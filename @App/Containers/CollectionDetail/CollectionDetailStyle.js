import { Platform } from 'react-native';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Fonts, Styles, Metrics, Colors } from 'App/Theme';

export default ScaledSheet.create({
  box: {
    flex: 1,
  },
  flatBox: {
    paddingBottom: Metrics.baseMargin,
    width: '100%',
    marginRight: Metrics.baseMargin,
    marginTop: '12@vs',
  },
  navBar: {
    ...Styles.navBar,
  },
  imgStyle: {
    width: '100%',
    height: 300,
  },
  contentBox: {
    marginTop: 16,
  },
  context: {
    paddingRight: 20,
    paddingLeft: 20,
  },
  equipmentBox: {
    paddingRight: 20,
    paddingLeft: 20,
    flexDirection: 'row',
  },
  titleBox: {
    flexDirection: 'row',
  },
  title: {
    fontSize: 36,
  },
  toolBox: {
    flexDirection: 'row',
    paddingRight: 20,
    paddingLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flexBox: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  flexText: {
    width: 60,
    flexWrap: 'wrap',
  },
  leftBox: {
    marginLeft: '5@s',
  },
  rightBox: {
    marginRight: '12@s',
  },
  bookmark: {
    right: '16@s',
  },
  btnStyle: {
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  collectionImgWidth: {
    width: '100%',
    height: Screen.verticalScale(230),
    overflow: 'hidden',
  },
  collectionTitle: {
    ...Fonts.style.regular500,
    color: Colors.goldenYellow,
  },
  collectionBoxStyle: {
    borderRadius: '7.5@s',
    backgroundColor: Colors.black,
    marginTop: Metrics.baseMargin / 4,
    alignItems: 'center',
    minWidth: '80@s',
  },
  collectionText: {
    ...Fonts.style.extraSmall500,
    paddingVertical: Metrics.baseMargin / 8,
    paddingHorizontal: Metrics.baseMargin / 2,
    color: Colors.white,
  },
  collectionLiveBoxStyle: {
    borderRadius: 7,
    width: '55@s',
    height: '15@vs',
    backgroundColor: Colors.white,
    marginBottom: 4,
    alignItems: 'center',
  },
  collectionLiveText: {
    fontSize: 10,
    fontWeight: '500',
    letterSpacing: 1,

    color: Colors.primary,
  },
  collectionFragment: {
    flexDirection: 'row',
    flex: 1,
  },
  collectionLeftBox: {
    flex: 1,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  collectionRightBox: {
    marginRight: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  collectionLeft: {
    flexDirection: 'column',
  },
  collectionRight: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },
  collectionTime: {
    fontSize: '11@vs',
    color: '#f0f0f0',
  },
  collectionSecondTitle: {
    ...Fonts.style.extraSmall500,
    color: Colors.gray_04,
  },
  collectionSecondIcon: {
    width: '50@vs',
    backgroundColor: 'blue',
    color: Colors.white,
    flexDirection: 'row',
  },
  collectionEquipments: {
    width: '20@vs',
    height: '20@vs',
  },
  collectionImageBox: {
    width: '40@vs',
    height: '40@vs',
    borderRadius: '40@vs',
    marginRight: '8@s',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  collectionEquipmentsBox: {
    flexDirection: 'row',
  },
  collectionTimeBox: {
    width: '100%',
    height: '13@vs',
    marginBottom: '12@vs',
  },
  imgEquipment: {
    width: Platform.isPad ? '35@vs' : '30@s',
    height: Platform.isPad ? '35@vs' : '30@s',
  },
  txtEquipmentsText: {
    ...Fonts.style.extraSmall,
    width: '60@s',
    textAlign: 'center',
  },
  imageBox: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: Metrics.baseMargin,
  },
  imgViewers: {
    width: '30@s',
    height: '30@s',
    marginRight: Metrics.baseMargin / 2,
  },
  txtViewers: {
    ...Fonts.style.small500,
    color: Colors.white,
  },
});
