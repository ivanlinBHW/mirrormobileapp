import React from 'react';
import { isNil, isString, isArray } from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Text, SafeAreaView, FlatList } from 'react-native';
import { bindActionCreators } from 'redux';

import {
  ClassCard,
  HeadLine,
  BaseReadMore,
  SecondaryNavbar,
  RoundLabel,
  CachedImage as Image,
  CachedImage as ImageBackground,
  InstructorPanel,
} from 'App/Components';
import { Classes, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { ClassActions, InstructorActions, CollectionActions } from 'App/Stores';
import LinearGradient from 'react-native-linear-gradient';
import styles from './CollectionDetailStyle';

class CollectionDetail extends React.PureComponent {
  static propTypes = {
    getClassDetail: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    token: PropTypes.string,
    setCollectionBookmark: PropTypes.func.isRequired,
    removeCollectionBookmark: PropTypes.func.isRequired,
    setInstructorId: PropTypes.func.isRequired,
  };

  state = {
    showAllInstructorList: false,
  };

  onPressInstructor = (id) => {
    const { setInstructorId, token } = this.props;
    setInstructorId(id, token);
  };

  onPressDetail = (id) => {
    const { getClassDetail, token } = this.props;
    getClassDetail(id, token, 'CollectionScreen');
  };

  onPressBookmark = (id, isBook) => {
    const { setCollectionBookmark, removeCollectionBookmark } = this.props;
    if (isBook) {
      removeCollectionBookmark(id);
    } else {
      setCollectionBookmark(id);
    }
  };

  renderItem = ({ item }) => {
    console.log('item=>', item);
    return (
      <View style={styles.flatBox}>
        <ClassCard
          size="full"
          data={item}
          onPress={this.onPressDetail}
          onPressBookmark={() => this.onPressBookmark(item.id, item.isBookmarked)}
        />
      </View>
    );
  };

  renderEquipmentItems = ({ item, index }) => {
    return (
      <View key={index} style={styles.imageBox}>
        <Image
          source={{ uri: item.deviceCoverImage }}
          style={styles.imgEquipment}
          resizeMode="contain"
        />
        <Text style={styles.txtEquipmentsText}>{item.title}</Text>
      </View>
    );
  };

  renderCollectionCard = () => {
    const { data } = this.props;
    return (
      <ImageBackground
        source={data.signedCoverImageObj}
        style={styles.collectionImgWidth}
        imageSize="2x"
        imageType="h"
      >
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
          locations={[0, 1]}
          colors={[`rgba(50,50,50,0)`, `rgba(50,50,50,0.4)`]}
        />
        {!isNil(data.clickCount) && (
          <View style={[Classes.rowCross, Classes.marginLeft, Classes.marginTop]}>
            <Image source={Images.viewers} style={styles.imgViewers} />
            <Text style={styles.txtViewers}>{data.clickCount}</Text>
          </View>
        )}
        <View style={styles.collectionFragment}>
          <View style={styles.collectionLeftBox}>
            <View style={styles.collectionLeft}>
              <Text style={styles.collectionSecondTitle}>{t('__collection')}</Text>
              <Text style={styles.collectionTitle}>
                {isString(data.title) && data.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.collectionRightBox}>
            <View style={styles.collectionRight}>
              <RoundLabel text={t(`program_card_level_${data.level}`)} />
              <RoundLabel text={`${data.trainingClasses.length} ${t('workouts')}`} />
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  };

  render() {
    const { data } = this.props;
    const { showAllInstructorList } = this.state;
    return (
      <SafeAreaView style={styles.box}>
        <SecondaryNavbar back backTo="Collection" />
        <FlatList
          ListHeaderComponent={
            <View>
              {this.renderCollectionCard()}
              <View style={styles.contentBox}>
                <HeadLine
                  paddingHorizontal={16}
                  title={t('collection_detail_about_this_collection')}
                />
                <BaseReadMore text={data.description} />
              </View>
              <View style={styles.contentBox}>
                <HeadLine
                  paddingHorizontal={16}
                  title={t('class_detail_good_for_tags')}
                />
                <View style={[Classes.row, Classes.paddingLeft, Classes.paddingRight]}>
                  {isArray(data.tags) &&
                    data.tags.map((e, i) => (
                      <Text key={`${e.tagId}`} style={styles.txtTags}>
                        {e.name}
                        {i !== data.tags.length - 1 ? ', ' : '.'}
                      </Text>
                    ))}
                </View>
                <View style={styles.contentBox}>
                  <HeadLine
                    paddingHorizontal={16}
                    title={t('class_detail_equipment_needed')}
                  />
                  {isArray(data.equipmentsList) && (
                    <View style={styles.equipmentBox}>
                      <FlatList
                        horizontal
                        data={data.equipmentsList}
                        renderItem={this.renderEquipmentItems}
                      />
                    </View>
                  )}
                </View>
                <View style={styles.contentBox}>
                  {isArray(data.instructorsList) && (
                    <InstructorPanel
                      data={data.instructorsList}
                      expend={showAllInstructorList}
                      onPressInstructor={this.onPressInstructor}
                      onExpandStateChange={() =>
                        this.setState({
                          showAllInstructorList: !showAllInstructorList,
                        })
                      }
                    />
                  )}
                </View>
                <View style={styles.contentBox}>
                  <HeadLine
                    paddingHorizontal={16}
                    title={`${data.trainingClasses.length} ${t('workouts')}`}
                  />
                </View>
              </View>
            </View>
          }
          keyExtractor={(item, index) => item.key}
          data={data.trainingClasses}
          renderItem={this.renderItem}
        />
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    token: state.user.token,
    isLoading: state.appState.isLoading,
    data: state.collection.detail,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        setInstructorId: InstructorActions.setInstructorId,
        getClassDetail: ClassActions.getClassDetail,
        setCollectionBookmark: CollectionActions.setCollectionBookmark,
        removeCollectionBookmark: CollectionActions.removeCollectionBookmark,
      },
      dispatch,
    ),
)(CollectionDetail);
