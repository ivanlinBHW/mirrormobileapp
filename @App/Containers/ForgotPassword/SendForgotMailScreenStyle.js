import { ScaledSheet } from 'App/Helpers';
import { Fonts, Metrics, Colors } from 'App/Theme';

export default ScaledSheet.create({
  marginTop18: {
    marginTop: '18@s',
  },
  marginTop80: {
    marginTop: '80@s',
  },
  headerText: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
  },
});
