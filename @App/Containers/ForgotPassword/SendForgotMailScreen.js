import * as Yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { View, Text, Linking, Platform, SafeAreaView } from 'react-native';

import {
  VerticalTextInput,
  SecondaryNavbar,
  DismissKeyboard,
  KeyboardHandler,
  SquareButton,
  LinkButton,
  BaseIcon,
} from 'App/Components';
import { UserActions } from 'App/Stores';
import { Classes, Colors, Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Config } from 'App/Config';

import styles from './SendForgotMailScreenStyle';

const yString = Yup.string();

const validationSchema = Yup.object().shape({
  email: yString
    .email(t('register_validation_email_type_error'))
    .required(t('register_validation_email_required')),
});

const initialValues = {
  email: '',
};

class SendForgotMailScreen extends React.Component {
  static propTypes = {
    sendForgotPasswordEmail: PropTypes.func.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@UserRegisterSuccessScreen');
  }

  state = {
    ...initialValues,
    isKeyboardShow: false,
  };

  renderForm = () => {
    const { sendForgotPasswordEmail } = this.props;
    const { isKeyboardShow } = this.state;
    return (
      <Formik
        initialValues={this.state}
        validationSchema={validationSchema}
        onSubmit={(values) => sendForgotPasswordEmail(values.email)}
        validateOnChange
      >
        {({
          dirty,
          isValid,
          isSubmitting,
          isValidating,
          values,
          errors,
          touched,
          setFieldValue,
          handleSubmit,
          handleChange,
          setFieldTouched,
        }) => (
          <View style={Classes.fill}>
            <VerticalTextInput
              onChangeText={handleChange('email')}
              error={errors.email}
              value={values.email}
              autoCapitalize="none"
              border
              placeholder={t('forgot_password_email')}
            />

            <SquareButton
              text={t('forgot_password_send')}
              style={styles.marginTop80}
              color={Colors.button.primary.content.background}
              onPress={handleSubmit}
              round
            />
            {!isKeyboardShow && (
              <Text style={styles.marginTop18}>{t('forgot_password_notice')}</Text>
            )}
          </View>
        )}
      </Formik>
    );
  };

  render() {
    const { isKeyboardShow } = this.state;
    return (
      <DismissKeyboard>
        <View style={Classes.fill}>
          <SecondaryNavbar back navTitle={t('reset_password')} />
          {Platform.OS === 'android' && (
            <KeyboardHandler
              onKeyboardDidShow={() => this.setState(() => ({ isKeyboardShow: true }))}
              onKeyboardDidHide={() => this.setState(() => ({ isKeyboardShow: false }))}
            />
          )}

          <View style={[Classes.fill, Classes.padding, styles.marginTop80]}>
            <Text style={styles.headerText}>{t('forgot_password_email')}</Text>
            {this.renderForm()}
          </View>
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) =>
    bindActionCreators(
      {
        sendForgotPasswordEmail: UserActions.sendForgotPasswordEmail,
      },
      dispatch,
    ),
)(SendForgotMailScreen);
