import * as Yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { View, Text, Linking, Platform, SafeAreaView } from 'react-native';

import {
  VerticalTextInput,
  SecondaryNavbar,
  DismissKeyboard,
  KeyboardHandler,
  SquareButton,
  LinkButton,
  BaseIcon,
} from 'App/Components';
import { UserActions } from 'App/Stores';
import { Classes, Colors, Fonts, Metrics } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Config } from 'App/Config';
import { ScaledSheet } from 'App/Helpers';

const yString = Yup.string();

const validationSchema = Yup.object().shape({
  currentPassword: yString
    .required(t('register_validation_password_required'))
    .min(8, t('register_validation_password_min_error', { value: 8 }))
    .max(16, t('register_validation_password_max_error', { value: 16 })),
  newPassword: yString
    .required(t('register_validation_password_required'))
    .min(8, t('register_validation_password_min_error', { value: 8 }))
    .max(16, t('register_validation_password_max_error', { value: 16 })),
});

const initialValues = {
  currentPassword: '',
  newPassword: '',
};

class ResetPasswordScreen extends React.Component {
  static propTypes = {
    resetPassword: PropTypes.func.isRequired,
    email: PropTypes.string,
    isLogin: PropTypes.boolean,
  };

  componentDidMount() {}

  state = {
    ...initialValues,
    isKeyboardShow: false,
  };

  renderForm = () => {
    const { resetPassword, email, isLogin } = this.props;
    const { isKeyboardShow } = this.state;
    return (
      <Formik
        initialValues={this.state}
        validationSchema={validationSchema}
        onSubmit={(values) =>
          resetPassword(values.currentPassword, values.newPassword, email, isLogin)
        }
        validateOnChange
      >
        {({
          dirty,
          isValid,
          isSubmitting,
          isValidating,
          values,
          errors,
          touched,
          setFieldValue,
          handleSubmit,
          handleChange,
          setFieldTouched,
        }) => (
          <View style={Classes.fill}>
            <View>
              <Text>{t('1-1-2_current_password')}</Text>
              <VerticalTextInput
                error={errors.currentPassword}
                value={values.currentPassword}
                autoCapitalize="none"
                border
                placeholder={t('1-1-2_current_password')}
                secureTextEntry={true}
                type="password"
                onChangeText={handleChange('currentPassword')}
                onBlur={() => setFieldTouched('currentPassword')}
              />
            </View>
            <View style={styles.marginTop40}>
              <Text>{t('1-1-2_new_password')}</Text>
              <VerticalTextInput
                error={errors.newPassword}
                value={values.newPassword}
                autoCapitalize="none"
                border
                placeholder={t('1-1-2_new_password')}
                secureTextEntry={true}
                type="password"
                onChangeText={handleChange('newPassword')}
                onBlur={() => setFieldTouched('newPassword')}
              />
            </View>
            {!isKeyboardShow && (
              <SquareButton
                text={t('1-1-2_submit')}
                style={styles.marginTop80}
                color={Colors.button.primary.content.background}
                onPress={handleSubmit}
                round
              />
            )}
          </View>
        )}
      </Formik>
    );
  };

  render() {
    const { isKeyboardShow } = this.state;
    return (
      <DismissKeyboard>
        <View style={Classes.fill}>
          <SecondaryNavbar back navTitle={t('change_password')} />
          {Platform.OS === 'android' && (
            <KeyboardHandler
              onKeyboardDidShow={() => this.setState(() => ({ isKeyboardShow: true }))}
              onKeyboardDidHide={() => this.setState(() => ({ isKeyboardShow: false }))}
            />
          )}

          <View style={[Classes.fill, Classes.padding, styles.marginTop60]}>
            {this.renderForm()}
          </View>
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({
    email: state.user.email || state.user.resetPasswordEmail,
    isLogin: state.user.email != null && state.user.email !== '',
  }),
  (dispatch) =>
    bindActionCreators(
      {
        resetPassword: UserActions.resetPassword,
      },
      dispatch,
    ),
)(ResetPasswordScreen);

const styles = ScaledSheet.create({
  marginTop18: {
    marginTop: '18@s',
  },
  marginTop40: {
    marginTop: '40@s',
  },
  marginTop80: {
    marginTop: '80@s',
  },
  marginTop60: {
    marginTop: '60@s',
  },
  headerText: {
    fontSize: Fonts.size.medium,
  },
});
