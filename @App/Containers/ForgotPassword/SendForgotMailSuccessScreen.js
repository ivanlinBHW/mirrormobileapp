import * as Yup from 'yup';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, Linking, Platform, SafeAreaView, Image, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
  VerticalTextInput,
  SecondaryNavbar,
  DismissKeyboard,
  KeyboardHandler,
  SquareButton,
  LinkButton,
  BaseIcon,
} from 'App/Components';
import { UserActions } from 'App/Stores';
import { Classes, Colors, Fonts, Metrics, Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { Config } from 'App/Config';
import { openInbox } from 'react-native-email-link';

class SendForgotMailSuccessScreen extends React.Component {
  static propTypes = {
    sendForgotPasswordEmail: PropTypes.func.isRequired,
  };

  componentDidMount() {
    __DEV__ && console.log('@UserRegisterSuccessScreen');
  }

  state = {};

  render() {
    return (
      <DismissKeyboard>
        <View style={Classes.fill}>
          <SecondaryNavbar back navTitle="" />

          <View style={[Classes.fill]}>
            <View style={styles.container}>
              <View style={styles.imageWrapper}>
                <Image source={Images.finished} style={styles.imageMain} />
              </View>
              <View style={styles.txtWrapper}>
                <Text style={styles.txtHeader}>{t('1-1-1_check_your_email')}</Text>
                <Text style={styles.txtContent}>
                  {t('1-1-1_check_your_email_description')}
                </Text>
              </View>
              <SquareButton
                text={t('1-1-1_reset_password')}
                color={Colors.button.primary.content.background}
                onPress={() => Actions.ResetPasswordScreen()}
                round
              />
            </View>
          </View>
          <View style={[Classes.mainEnd, Classes.marginBottom]}>
            <LinkButton
              text={`${t('1-1-1_foot_notice')}`}
              textSize={Fonts.size.medium}
              textColor={Colors.error}
              textStyle={styles.txtFooter}
              onPress={() => {
                openInbox().catch(function(error) {
                  if (error.message === 'No email apps available')
                    Alert.alert(t('alert_title_oops'), error.message);
                });
              }}
            />
          </View>
        </View>
      </DismissKeyboard>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => bindActionCreators({}, dispatch),
)(SendForgotMailSuccessScreen);

import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  container: {
    marginHorizontal: Metrics.baseMargin,
    height: '100%',
  },
  imageWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '44@vs',
  },
  imageMain: {
    width: '60@s',
    height: '60@vs',
  },
  txtWrapper: {
    marginVertical: '40@vs',
    alignItems: 'center',
  },
  txtHeader: {
    ...Fonts.style.h4_500,
    marginBottom: Metrics.baseMargin / 2,
  },
  txtFooter: {
    textAlign: 'center',
  },
  txtContent: {
    fontSize: Fonts.size.medium,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'center',
    color: Colors.gray_01,
    textAlign: 'center',
  },
});
