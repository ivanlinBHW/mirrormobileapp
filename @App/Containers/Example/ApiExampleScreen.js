import React from 'react';
import { Platform, Text, View, Button, ActivityIndicator, Image } from 'react-native';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import ExampleActions from 'App/Stores/Example/Actions';
import { liveInEurope } from 'App/Stores/Example/Selectors';
import Style from './ApiExampleScreenStyle';
import { Images } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';

const instructions = Platform.select({
  ios: t('instructions_ios'),
  android: t('instructions_android'),
});

class ApiExampleScreen extends React.Component {
  componentDidMount() {
    __DEV__ && console.log('@Mount ApiExampleScreen!');
    this._fetchUser();
  }

  render() {
    return (
      <View style={Style.container}>
        {this.props.userIsLoading ? (
          <ActivityIndicator size="large" color="#0000ff" />
        ) : (
          <View>
            <View style={Style.logoContainer}>
              <Image style={Style.logo} source={Images.logo} resizeMode={'contain'} />
            </View>
            <Text style={Style.text}>{t('title')}</Text>
            <Text style={Style.instructions}>{instructions}</Text>
            {this.props.userErrorMessage ? (
              <Text style={Style.error}>{this.props.userErrorMessage}</Text>
            ) : (
              <View>
                <Text style={Style.result}>
                  {t('username')
                  }
                  {this.props.user.name}
                </Text>
                <Text style={Style.result}>
                  {this.props.liveInEurope
                    ? t('live_in_eu')
                    : t('not_live_in_eu')
                  }
                </Text>
              </View>
            )}
            <Button onPress={() => this._fetchUser()} title={t('refresh')} />
          </View>
        )}
      </View>
    );
  }

  _fetchUser() {
    this.props.fetchUser();
  }
}

ApiExampleScreen.propTypes = {
  user: PropTypes.object,
  userIsLoading: PropTypes.bool,
  userErrorMessage: PropTypes.string,
  fetchUser: PropTypes.func,
  liveInEurope: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  user: state.example.user,
  userIsLoading: state.example.userIsLoading,
  userErrorMessage: state.example.userErrorMessage,
  liveInEurope: liveInEurope(state),
});

const mapDispatchToProps = (dispatch) => ({
  fetchUser: () => dispatch(ExampleActions.fetchUser()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApiExampleScreen);
