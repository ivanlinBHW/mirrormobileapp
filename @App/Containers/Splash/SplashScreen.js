import React from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-native';
import { bindActionCreators } from 'redux';
import { Permissions } from 'react-native-unimodules';

import SplashView from './SplashView';

class SplashScreen extends React.PureComponent {
  async componentDidMount() {
    __DEV__ && console.log('@Enter SplashScreen!');

    await this.requestPermission();
  }

  requestPermission = async () => {
    try {
      const { status: getAsyncStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS,
      );
      console.log('getAsyncStatus=>', getAsyncStatus);
      if (getAsyncStatus !== 'granted') {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS,
        );
        console.log('askAsync status=>', status);
        if (status !== 'granted') {
          throw new Error('@SplashScreen: permission not granted.');
        }
      }
    } catch (error) {
      __DEV__ && console.log('@SplashScreen: permission rejected, ', error);
    }
  };

  render() {
    return <SplashView />;
  }
}

export default connect(
  (state) => ({
    isLoading: state.appState.isLoading,
  }),
  (dispatch) => bindActionCreators({}, dispatch),
)(SplashScreen);
