import React from 'react';
import { View, Image } from 'react-native';

import { StyleSheet } from 'App/Helpers';
import { Colors, Styles, Images } from 'App/Theme';

const styles = StyleSheet.create({
  container: {
    ...Styles.screen.container,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  logo: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    backgroundColor: Colors.white,
  },
  logoImg: {
    width: '120@s',
    height: '120@s',
  },
});

const SplashView = ({}) => (
  <View style={styles.container}>
    <View style={styles.logo}>
      {/* You will probably want to insert your logo here */}
      <Image source={Images.logo} style={styles.logoImg} />
    </View>
  </View>
);

export default SplashView;
