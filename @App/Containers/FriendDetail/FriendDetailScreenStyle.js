import { ScaledSheet, Screen } from 'App/Helpers';
import { Metrics, Colors, Fonts } from 'App/Theme';

export default ScaledSheet.create({
  container: {
    flex: 1,
  },
  navContainer: {
    flex: 1,
  },
  full: {
    width: '100%',
  },
  headerWrapper: {
    flexDirection: 'column',
    marginTop: '24@vs',
    alignItems: 'center',
  },
  rightText: {
    ...Fonts.style.regular500,
    marginTop: '24@s',
    marginBottom: '38@s',
  },
  btnStyle: {
    height: '32@vs',
    width: '182@s',
    paddingHorizontal: '18@s',
    borderWidth: 2,
    borderColor: Colors.button.primary.outline.border,
    backgroundColor: 'transparent',
    marginBottom: Metrics.baseMargin,
  },
  activeBtnStyle: {
    backgroundColor: Colors.button.primary.outline.text,
    borderWidth: 0,
  },
  textStyle: {
    ...Fonts.style.medium500,
    color: Colors.white,
    textTransform: 'uppercase',
  },
  activeTextStyle: {
    ...Fonts.style.medium500,
    color: Colors.button.primary.outline.text,
    textTransform: 'uppercase',
  },
  achievementList: {
    paddingHorizontal: Metrics.baseMargin,
    marginVertical: Metrics.baseMargin,
  },
  hrLine: {
    marginTop: '40@vs',
    marginBottom: Metrics.baseMargin,
  },
  headBox: {
    marginHorizontal: Metrics.baseMargin,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headLineTitle: {
    ...Fonts.style.medium500,
  },
  viewBtn: {
    width: '70@s',
    ...Fonts.style.small500,
    height: '16@vs',
  },
  secondLine: {
    marginBottom: Metrics.baseMargin,
  },
  cardWrapper: {
    marginTop: Metrics.baseMargin / 2,
  },
  selectStyle: {
    color: Colors.button.delete.text.text,
    fontSize: '20@s',
    textAlign: 'center',
  },
  cancelContainer: {
    height: '57@s',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  cancelTextStyle: {
    fontSize: Screen.scale(20),
    color: Colors.sky_blue,
  },
  optionTextStyle: {
    height: '57@s',
  },
  confirmBox: {
    flexDirection: 'column',
  },
  btnMarginTop: {
    marginTop: Metrics.baseMargin,
  },
  eventBox: {
    marginBottom: Metrics.baseMargin,
  },
  viewStyle: {
    width: '60@s',
    height: '14@s',
  },
  viewTextStyle: {
    ...Fonts.style.small500,
  },
  confirmBtn: {
    marginBottom: 0,
  },
  achievementBox: {
    marginRight: Metrics.baseMargin / 2,
  },
});
