import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { connect } from 'react-redux';
import { View, SafeAreaView, Text, ScrollView, FlatList } from 'react-native';
import { bindActionCreators } from 'redux';

import { Classes, Colors } from 'App/Theme';
import { Date as d } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import {
  BaseStatusAvatar,
  SecondaryNavbar,
  RoundButton,
  AchievementItem,
  HorizontalLine,
  BaseModalSelector,
  CommunityCard,
  BaseButton,
} from 'App/Components';
import { SettingActions, SearchActions, CommunityActions } from 'App/Stores';

import styles from './FriendDetailScreenStyle';

class FriendDetailScreen extends React.Component {
  static propTypes = {
    detail: PropTypes.object,
    equipmentList: PropTypes.array,
    genres: PropTypes.array.isRequired,
    getEquipmentList: PropTypes.func.isRequired,
    resetEditSetting: PropTypes.func.isRequired,
    timezone: PropTypes.string,
    addFriend: PropTypes.func.isRequired,
    deleteFriend: PropTypes.func.isRequired,
    confirmFriend: PropTypes.func.isRequired,
    deleteUnFriend: PropTypes.func.isRequired,
    getUserEvent: PropTypes.func.isRequired,
    getUserAchievement: PropTypes.func.isRequired,
    userId: PropTypes.string,
    getTrainingEvent: PropTypes.func.isRequired,
  };

  static defaultProps = {
    timezone: 'Asia/Taipei',
  };

  componentDidMount() {
    __DEV__ && console.log('@FriendDetailScreen');
  }

  state = {
    backTo: undefined,
    showAll: false,
    isShowUnFriendDelete: false,
    isShowFriendDelete: false,
  };

  renderFriendStatus = () => {
    const { detail, addFriend, confirmFriend, deleteFriend } = this.props;
    if (detail.friendStatus === 1 && !detail.wasAddFriend) {
      return (
        <View style={styles.confirmBox}>
          <RoundButton
            style={[styles.btnStyle, styles.activeBtnStyle, styles.confirmBtn]}
            textStyle={styles.textStyle}
            uppercase={false}
            text={t('confirm')}
            onPress={() => confirmFriend(detail.id)}
          />
          <RoundButton
            style={[styles.btnStyle, styles.btnMarginTop]}
            textStyle={styles.activeTextStyle}
            uppercase={false}
            text={t('__delete')}
            onPress={() => deleteFriend(detail.id)}
          />
        </View>
      );
    } else if (detail.friendStatus === 2) {
      const { isShowUnFriendDelete } = this.state;
      const data = [
        {
          key: 2,
          label: t('__delete'),
          component: <Text style={styles.selectStyle}>{t('__delete')}</Text>,
        },
      ];
      return (
        <BaseModalSelector
          data={data}
          visible={isShowUnFriendDelete}
          onModalClose={(option) => this.onPressChange(option)}
          cancelTextStyle={styles.cancelTextStyle}
          cancelStyle={styles.cancelContainer}
          optionTextStyle={styles.optionTextStyle}
        >
          <RoundButton
            style={styles.btnStyle}
            textStyle={styles.activeTextStyle}
            uppercase
            text={t('__friend')}
            onPress={() => this.setState({ isShowUnFriendDelete: true })}
          />
        </BaseModalSelector>
      );
    } else if (detail.friendStatus === 1) {
      const { isShowFriendDelete } = this.state;
      const data = [
        {
          key: 1,
          label: t('__delete'),
          component: <Text style={styles.selectStyle}>{t('__delete')}</Text>,
        },
      ];
      return (
        <BaseModalSelector
          data={data}
          visible={isShowFriendDelete}
          onModalClose={(option) => this.onPressChange(option)}
          cancelTextStyle={styles.cancelTextStyle}
          cancelStyle={styles.cancelContainer}
          optionTextStyle={styles.optionTextStyle}
        >
          <RoundButton
            style={styles.btnStyle}
            textStyle={styles.activeTextStyle}
            uppercase={false}
            text={t('community_search_btn_pending')}
            onPress={() => this.setState({ isShowFriendDelete: true })}
          />
        </BaseModalSelector>
      );
    } else {
      return (
        <RoundButton
          style={[styles.btnStyle, styles.activeBtnStyle]}
          textStyle={styles.textStyle}
          uppercase={false}
          text={t('community_search_btn_add_friend')}
          onPress={() => addFriend(detail.id)}
        />
      );
    }
  };

  renderHeader = () => {
    const { detail, userId } = this.props;
    return (
      <View style={styles.headerWrapper}>
        <BaseStatusAvatar
          size="large"
          uri={detail.avatar}
          isOnline={detail.currentStatus === 1}
        />
        <Text style={styles.rightText}>{detail.fullName}</Text>
        {userId !== detail.id && this.renderFriendStatus()}
      </View>
    );
  };

  renderAchievement = (achievements) => {
    const renderItem = ({ item }) => {
      return (
        <View style={styles.achievementBox}>
          <AchievementItem
            name={item.achievement.name}
            date={d.formatDate(
              `${item.achievement.createdAt}`,
              d.ACHIEVEMENT_DATE_FORMAT,
            )}
            title={item.achievement.title}
            description={item.achievement.description}
            active
          />
        </View>
      );
    };
    return (
      <View style={[Classes.rowMain, styles.achievementList]}>
        <FlatList
          horizontal
          data={achievements}
          renderItem={renderItem}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  renderEventsItem = ({ item, index }) => {
    const { timezone, getTrainingEvent } = this.props;
    return (
      index < 6 && (
        <View style={styles.cardWrapper}>
          <CommunityCard
            size="large"
            data={item}
            timezone={timezone}
            onPress={() => getTrainingEvent(item.id)}
          />
        </View>
      )
    );
  };

  onPressChange = (option) => {
    const { detail, deleteFriend, deleteUnFriend } = this.props;
    if (option.key === 1) {
      this.setState({ isShowFriendDelete: false });
      deleteFriend(detail.id);
    } else if (option.key === 2) {
      this.setState({ isShowUnFriendDelete: false });
      deleteUnFriend(detail.id);
    }
  };

  renderPrivacyDetail = () => {
    const { detail, getUserEvent, getUserAchievement, userId } = this.props;
    if (!isEmpty(detail.settings) && detail.settings.privacy) {
      if (
        detail.settings.privacy === 1 ||
        (detail.settings.privacy === 2 && detail.friendStatus === 2) ||
        userId === detail.id
      )
        return (
          <View>
            {/* {!isEmpty(detail.achievedAchievements) && (
              <View>
                <HorizontalLine style={styles.hrLine} />
                <View style={styles.headBox}>
                  <Text style={styles.headLineTitle}>{t('progress_achievement')}</Text>
                  {detail.achievedAchievements.length > 10 && (
                    <BaseButton
                      transparent
                      style={styles.viewStyle}
                      textStyle={styles.viewTextStyle}
                      textColor={Colors.button.primary.text.text}
                      text={t('see_all')}
                      onPress={() => getUserAchievement(detail.id)}
                    />
                  )}
                </View>
                {this.renderAchievement(detail.achievedAchievements)}
              </View>
            )} */}
            {!isEmpty(detail.trainingEvents) && (
              <View style={styles.eventBox}>
                <HorizontalLine style={styles.secondLine} />
                <View style={styles.headBox}>
                  <Text style={styles.headLineTitle}>{t('community_event_title')}</Text>
                  {detail.trainingEvents.length > 6 && (
                    <BaseButton
                      transparent
                      style={styles.viewStyle}
                      textStyle={styles.viewTextStyle}
                      textColor={Colors.button.primary.text.text}
                      text={t('see_all')}
                      onPress={() => getUserEvent(detail.id)}
                    />
                  )}
                </View>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={detail.trainingEvents}
                  renderItem={this.renderEventsItem}
                />
              </View>
            )}
          </View>
        );
    }
  };

  render() {
    return (
      <SafeAreaView style={Classes.fill}>
        <SecondaryNavbar navHeight={44} back navTitle={t('community_friends')} />
        <ScrollView style={styles.container}>
          {this.renderHeader()}
          {this.renderPrivacyDetail()}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default connect(
  (state) => ({
    detail: state.community.userDetail,
    timezone: state.appState.currentTimeZone,
    userId: state.user.userId,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateSetting: SettingActions.updateSetting,
        resetEditSetting: SettingActions.resetEditSetting,
        getEquipmentList: SearchActions.getEquipmentList,
        addFriend: CommunityActions.addFriend,
        deleteFriend: CommunityActions.deleteFriend,
        confirmFriend: CommunityActions.confirmFriend,
        deleteUnFriend: CommunityActions.deleteUnFriend,
        getUserEvent: CommunityActions.getUserEvent,
        getUserAchievement: CommunityActions.getUserAchievement,
        getTrainingEvent: CommunityActions.getTrainingEvent,
      },
      dispatch,
    ),
)(FriendDetailScreen);
