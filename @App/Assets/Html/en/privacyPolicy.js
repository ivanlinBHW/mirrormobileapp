const html = `<p><strong>Privacy Policy</strong></p>
<p>This Privacy Policy applies to the services offered by Johnson Health Tech Co., Ltd. and its affiliates, subsidiaries and divisions (collectively, the <strong>“Company,” “we,” “our,”</strong> and <strong>“us”</strong> ) via a mirror-like device containing a 2-dimension and a 3-dimension camera modules, Johnson@Mirror (the " <strong>Equipment</strong>“) that is marketed and sold under the names and trademarks of either Johnson@Mirror or Horizon@Mirror (the” <strong>Trademarks</strong>“). The Equipment is operated by a mobile device (e.g., a smart mobile phone or a tablet) (the” <strong>Control Device</strong>“) that is on the same WiFi network as the Equipment and is connected to the Equipment. Before being able to connect to the Equipment, the Control Device needs to have an application installed thereon. Such application is marketed and sold under either of the Trademarks, is developed by Johnson, and is available through either Apple Store or Google Play, depending on the operating system of the Control Device you use (the” <strong>App</strong>“). With the Equipment, the Control Device, and the App, you will be able to enjoy online virtual fitness training classes offered by Johnson (the” <strong>Service</strong>").</p>
<p>This Privacy Policy provides you with information on the type of personal information we collect from you or through the devices that you may have connected to the Equipment and how we process such information.</p>
<p>If you have any questions about our privacy practices, please contact us as set forth in the section below, titled “Contact Us.”</p>
<ol type="1">
<li><strong>Information We Collect</strong></li>
</ol>
<ul>
<li><strong>Account Creation.</strong> Before your use of the Service, you are required to create an account via the App and log in. During account registration, we will require you to provide certain information as follows:
<ul>
<li><strong>Email address.</strong></li>
</ul></li>
</ul>
<p>You will be required to provide your email address during account registration. The email will be used 1) for initial account registration verification purpose; 2) for logging into your account thereafter so as to allow You to enjoy the Equipment and the Service; and 3) for Johnson to communicate with You if needed including but not limited to any update on the Terms of Use or this Privacy Policy.</p>
<ul>
<li><strong>Additional Information.</strong></li>
</ul>
<p>Additional information may be collected such as your gender, age group, height, and weight. You may choose not to enter such information. But please understand that should you choose not to enter such information, you will be unable to receive your workout result through the Service as calculation of workout achievement would not be possible without such background information.</p>
<ul>
<li><strong>Location</strong></li>
</ul>
<p>As you set up your account on the App, the App will ask for you to turn on the location service. The information will only be used for determining the available WiFi network connection so as to make it more convenient for you to connect your Control Device with your Equipment and to determine the location for the weather display on the Equipment. You may choose not to switch on the location service and enter the SSID and Password of your WiFi network manually to connect the Equipment and the Control Device.</p>
<ul>
<li><strong>Information collected during your use of the Service.</strong> During your use of the Service, we will collect information about your workout activity through the Equipment and the heart-rate monitor you may have connected to the Equipment (collectively referred to as the “Workout Data”), such as follows:
<ul>
<li>time (and duration) of workout;</li>
<li>heart rate (only if you use compatible 3rd party equipment);</li>
<li>trace of your body motion (only if you take classes with motion capture function enabled, and you agree for us to collect such information by switching on the 3D camera). Such data will only be used to determine the accuracy of your movement in comparison with the movement demonstrated by the virtual trainer and will not be used for other purpose;</li>
<li>Cache of your full body image (only if you choose to take class with your designated user and you agree us to collect by switch on 2D camera) during a partner workout. Such cache will be deleted from Johnson's systems at the end of a workout session;</li>
<li>Cache of your full body image at the end of a workout session should you choose to take a picture using the 2D Camera at the end of a workout session. The cache of such image will be deleted from Johnson's systems should you choose not to save them onto your Control Device or share them on your social media account once the image is taken.</li>
</ul></li>
<li><strong>When You Communicate with us.</strong> It is inevitable that we may ask for additional personal information when you get in touch with us. Nevertheless, we will not require information that is beyond what is required for us to either provide you with the requested customer support or respond to your data subject requests regarding your personal data.</li>
<li><strong>During your use of the Service.</strong> From your logging into the account, browse the classes and workout by using the Service, we automatically collect information on the interaction among the Equipment, the App, and the Service. Some examples of information we collect include service use data (e.g., the features you use with Services, the pages you visit on, workouts performed and tied to your user ID, the classes you have attended, the type of workout session(s) you have participated in, the workout event you have hosted, the time of your workout using the App) ), device connectivity and configuration data (e.g. your Internet service provider, your device's regional and language settings, and your device's identifiers such as IP address and the device ID) and location data (e.g., location derived from an IP address or data that indicates a city level).</li>
</ul>
<p>The information we automatically collect during your use of the Service is to allow us to troubleshoot in case of any problem you may encounter during your use of the Service or the App. As for the location data, such information will only be used for the purpose of identifying the WiFi network that is available for your Control Device and Equipment to be connected to.</p>
<ol start="2" type="1">
<li><strong>How we Keep</strong> <strong>your</strong> <strong>information</strong></li>
</ol>
<p>After our collection of your information, we will store it at cloud server designated and controlled by us.We take reasonable and appropriate security measures designed to protect the information we collect from or about you against unauthorized access, use, alteration, disclosure or destruction. Data is stored until the user explicitly requests the erasure of his/her account data stored on the servers or until the basis for the processing of the data is no longer valid. Please be aware, however, that no method of transmitting information over the Internet or storing information is completely secure. Accordingly, we cannot guarantee the absolute security of any information.</p>
<ol start="3" type="1">
<li><strong>How we share your Information</strong></li>
</ol>
<p>The Company shares your information to the following recipients:</p>
<ul>
<li><strong>Other</strong> <strong>Services users.</strong> The “Name” you choose to enter in your profile (the “Alias”) and the image you choose to upload to your profile (the “Image”) will be searchable by other users of the App regardless of your privacy setting. However, you can decide how much information other users can know about you by modifying your privacy setting in the App.
<ul>
<li>If you choose the privacy setting to “Only for me,” the information shown on your leaderboard and community board will not be shared with others, and the event you create will not be viewable by other users either unless such event is made open to public and/or viewable and open for signing up by all users.</li>
<li>If you choose the privacy setting of “Friends,” the information on your leaderboard and community board will be shared with those users that you have agreed to add as your friends.</li>
<li>If you choose the privacy setting of “Everyone,” the information on your leaderboard and community board will be shared with all other users.</li>
</ul></li>
</ul>
<p>In case that you choose to call a workout event and make the event public and/or allow all Service users to have access to such event, during the workout event, your Alias, Image, workout score and image while exercising will be viewable by all users attending such event.</p>
<ul>
<li><strong>Affiliates.</strong> We share your information with our related entities including our parent and sister companies. For example, we may share your information with our affiliates for customer support, marketing, and technical operations.</li>
<li><strong>Third Parties.</strong> We collaborate with third-party vendors for the storage of data, the analysis of workout posture accuracy, streaming of video services, and processing of payments. For such reasons, the information we have collected as set out above will likely be shared with such vendors.</li>
<li><strong>Security or Compelled Disclosure.</strong> We share your information if we have a good faith belief that access, use, preservation or disclosure of such information is reasonably necessary to (a) satisfy any applicable law, regulation, legal process or enforceable governmental request, (b) enforce the Terms of Service, including investigation of potential violations thereof, (c) detect, prevent, or otherwise address fraud, security or technical issues, or (d) protect against harm to the rights, property or safety of the Company, its users or the public as required or permitted by law. We share your informationin response to lawful requests by public authorities, including to meet national security or law enforcement requirements.</li>
</ul>
<ol start="4" type="1">
<li><strong>Third Party Services</strong></li>
</ol>
<p>The Services may contain links to other services. Any information that you provide on or to a third-party website or service is provided directly to the owner of the website or service and is subject to that party's privacy policy. Our Privacy Policy does not apply to such websites or services and we're not responsible for the content, privacy or security practices and policies of those websites or services. To protect your information, we recommend that you carefully review the privacy policies of other websites and services that you access.</p>
<ol start="5" type="1">
<li><strong>Local-Specific Rights</strong></li>
</ol>
<h3 id="a.-residents-of-taiwan"><strong>A. Residents of Taiwan</strong></h3>
<p>Upon your request, we will make our good-faith efforts to:</p>
<ul>
<li>provide you with access to and copy of the information you have submitted to us through your account;</li>
<li>to correct or supplement this information if it is inaccurate;</li>
<li>to stop the collection or further processing of your data; or</li>
<li>to delete your data.</li>
</ul>
<p>Please feel free to let us know if you would like your information corrected or removed by emailing us as set forth in the section below, entitled “Contact Us”.</p>
<p>You can permanently delete your account through your account settings or by sending us an email to us as set forth in the section entitled “Contact Us” below. We'll take steps to delete your information as soon as is practicable, but some information may remain in archived/backup copies for our records or as otherwise required by law.</p>
<p>Please note that we can only delete your information within the Services. Information that you have shared with our 3rd-party partners and sites cannot be deleted by the Company. Therefore, this information will remain on those 3rd-party sites and services even after you unsubscribe from us. However, upon receiving a deletion request, the Company will send a deletion request to those 3rd-party partners that the Company has previously sent your data to for processing.</p>
<h3 id="c.-residents-of-the-european-economic-area-and-the-united-kingdom"><strong>C. Residents of the European Economic Area and the United Kingdom</strong></h3>
<p>If you are a user of the Equipment and the Service within the European Union or the United Kingdom, you may request us to:</p>
<ul>
<li>provide with access to your data;</li>
<li>rectify your data;</li>
<li>erase the data we have collected about you through the Service;</li>
<li>allow you to transfer the data we have collected about you through the Service to another party designated by you;</li>
<li>require us to stop processing your data for marketing purposes or purposes materially different than for which it was originally collected or subsequently authorized by you;</li>
<li>withdraw your consent at any time for any data processing we do based on consent you have provided to us.</li>
</ul>
<p>If you consider there is any problem with how we collect and process your data, you have the right to lodge a complaint with the data protection agency within your jurisdiction.</p>
<ol start="6" type="1">
<li><strong>International Transfer</strong></li>
</ol>
<p>Considering that Johnson is a multinational company based in Taiwan, international transfer of data will be inevitable. As indicated above, we utilize third-party cloud storage service providers for storing the Service's user data; and as of the date of the latest update of this Privacy Policy, all user data will be stored in Taiwan. As such data is shared by Johnson with the third parties indicated above, such data may be transferred from Taiwan to other jurisdictions. Regardless, please be rest assured that all such transfers will only be undertaken with the appropriate safeguards in place.</p>
<ol start="7" type="1">
<li><strong>Our Policy toward Children</strong></li>
</ol>
<p>The Service is designed and restricted to use by individuals that are at least 18 years old. As such, we do not anticipate receiving personal data from individuals under such age requirement. If we learn that we have collected personal information of a child under 13 we will take steps to delete such information from our files as soon as possible.</p>
<ol start="8" type="1">
<li><strong>Changes to This Privacy Policy</strong></li>
</ol>
<p>Please note that this Privacy Policy may change from time to time. We will notify you of such changes either through push notification to your Equipment or through email. Please review it before you continue to use the Equipment and the Service.</p>
<p>All changes will be effective immediately upon posting of the revised Privacy Policy via the Service. Your continued use of the Service indicates that you have read and understood the changes to the Privacy Policy then posted.</p>
<ol start="9" type="1">
<li><strong>Contact Us</strong></li>
</ol>
<p>Please feel free to direct any questions or concerns regarding this Privacy Policy or the Company's data practices by contacting us at:dpo@johnsonfitness.com.</p>`;

export default html;
