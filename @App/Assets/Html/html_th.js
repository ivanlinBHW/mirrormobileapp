const html_th = `<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=unicode">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List
href="THA_Johnson%20Health%20Tech%20Privacy%20Policy.fld/filelist.xml">
<title>Johnson Health Tech Privacy Policy - 10-17-2019</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Marta Rodriguez Diaz</o:Author>
  <o:LastAuthor>Microsoft Office User</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>1</o:TotalTime>
  <o:Created>2020-06-03T02:54:00Z</o:Created>
  <o:LastSaved>2020-06-03T02:54:00Z</o:LastSaved>
  <o:Pages>8</o:Pages>
  <o:Words>2871</o:Words>
  <o:Characters>16369</o:Characters>
  <o:Lines>136</o:Lines>
  <o:Paragraphs>38</o:Paragraphs>
  <o:CharactersWithSpaces>19202</o:CharactersWithSpaces>
  <o:Version>16.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData
href="THA_Johnson%20Health%20Tech%20Privacy%20Policy.fld/themedata.thmx">
<link rel=colorSchemeMapping
href="THA_Johnson%20Health%20Tech%20Privacy%20Policy.fld/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
  DefSemiHidden="false" DefQFormat="false" DefPriority="99"
  LatentStyleCount="376">
  <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 9"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="header"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footer"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index heading"/>
  <w:LsdException Locked="false" Priority="35" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of figures"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope return"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="line number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="page number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of authorities"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="macro"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="toa heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 5"/>
  <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Closing"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Signature"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="true"
   UnhideWhenUsed="true" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Message Header"/>
  <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Salutation"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Date"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Block Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="FollowedHyperlink"/>
  <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Document Map"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Plain Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="E-mail Signature"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Top of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Bottom of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal (Web)"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Acronym"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Cite"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Code"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Definition"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Keyboard"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Preformatted"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Sample"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Typewriter"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Variable"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Table"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation subject"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="No List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Contemporary"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Elegant"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Professional"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Balloon Text"/>
  <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Theme"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" QFormat="true"
   Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" QFormat="true"
   Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" QFormat="true"
   Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" QFormat="true"
   Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" QFormat="true"
   Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" QFormat="true"
   Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" SemiHidden="true"
   UnhideWhenUsed="true" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Mention"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Smart Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hashtag"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Unresolved Mention"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Smart Link"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 1 6 1 0 1 1 1 1 1;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Tahoma",sans-serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:TH;
	mso-fareast-language:#1000;}
h1
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"標題 1 字元";
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:1;
	font-size:24.0pt;
	font-family:"Tahoma",sans-serif;
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:TH;
	mso-fareast-language:#1000;
	font-weight:bold;}
h3
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"標題 3 字元";
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:3;
	font-size:13.5pt;
	font-family:"Tahoma",sans-serif;
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:TH;
	mso-fareast-language:#1000;
	font-weight:bold;}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:purple;
	text-decoration:underline;
	text-underline:single;}
p
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Tahoma",sans-serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:TH;
	mso-fareast-language:#1000;}
p.msonormal0, li.msonormal0, div.msonormal0
	{mso-style-name:msonormal;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Tahoma",sans-serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:TH;
	mso-fareast-language:#1000;}
span.1
	{mso-style-name:"標題 1 字元";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"標題 1";
	mso-ansi-font-size:16.0pt;
	mso-bidi-font-size:16.0pt;
	font-family:"Arial",sans-serif;
	mso-ascii-font-family:Arial;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:細明體;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Arial;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#2F5496;
	mso-themecolor:accent1;
	mso-themeshade:191;}
span.inline-comment-marker
	{mso-style-name:inline-comment-marker;
	mso-style-unhide:no;}
span.3
	{mso-style-name:"標題 3 字元";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"標題 3";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"Arial",sans-serif;
	mso-ascii-font-family:Arial;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:細明體;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:Arial;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#1F3763;
	mso-themecolor:accent1;
	mso-themeshade:127;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	mso-ascii-font-family:Tahoma;
	mso-hansi-font-family:"Times New Roman";
	mso-font-kerning:0pt;
	mso-ansi-language:TH;
	mso-fareast-language:#1000;}
 @page
	{mso-page-border-surround-header:no;
	mso-page-border-surround-footer:no;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 @list l0
	{mso-list-id:81876049;
	mso-list-template-ids:-211786044;}
@list l0:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l0:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l0:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1
	{mso-list-id:322053170;
	mso-list-template-ids:1946576924;}
@list l1:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l1:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l1:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2
	{mso-list-id:626397552;
	mso-list-template-ids:1461375458;}
@list l2:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l2:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l2:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3
	{mso-list-id:727072739;
	mso-list-template-ids:1936103888;}
@list l3:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l3:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l3:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4
	{mso-list-id:886603673;
	mso-list-template-ids:-570018504;}
@list l4:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l4:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l4:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5
	{mso-list-id:1116828760;
	mso-list-template-ids:1623598860;}
@list l5:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l5:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l5:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6
	{mso-list-id:1146584173;
	mso-list-template-ids:1520444454;}
@list l6:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l6:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l6:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7
	{mso-list-id:1844737114;
	mso-list-template-ids:-189209590;}
@list l7:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l7:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l7:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8
	{mso-list-id:2136563767;
	mso-list-template-ids:-1081196432;}
@list l8:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l8:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l8:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 table.MsoNormalTable
	{mso-style-name:表格內文;
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Tahoma",sans-serif;
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-ansi-language:TH;
	mso-fareast-language:#1000;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ZH-TW link=blue vlink=purple style='tab-interval:36.0pt'>

<div class=WordSection1>

<h1><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>นโยบายความเป็นส่วนตัวของ</span><span lang=TH> Johnson Health Tech - </span><span
lang=TH style='font-size:12.0pt'>10-17-2019</span><span lang=TH
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p><strong><span lang=TH style='mso-ascii-font-family:Tahoma'>นโยบายความเป็นส่วนตัว</span></strong><span
lang=TH> <br>
</span><span class=SpellE><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>อัป</span></span><span lang=TH style='font-family:
"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เดต<span
style='color:#2F3438'>เมื่อ</span></span><span lang=TH style='color:#2F3438'>:</span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>&nbsp;</span><span
lang=TH>18/10/2019 <br>
</span><span class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>นโยบายความเป็นส่วนตัวฉบับนี้มีผลกับผลิตภัณฑ์และบริการทั้งหมดที่เสนอโดย</span><span
lang=TH style='color:#2F3438'> Johnson Health Tech Co., <span class=SpellE>Ltd</span>.
</span></span><span class=inline-comment-marker><span lang=TH style='font-family:
"Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>และบริษัทในเครือ</span><span
lang=TH style='color:#2F3438'> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>บริษัทสาขา</span><span lang=TH style='color:#2F3438'> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>แผนก</span><span lang=TH
style='color:#2F3438'> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>และแบรนด์ของบริษัทฯ</span><span lang=TH style='color:#2F3438'> (</span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ให้เรียกรวมกันว่า</span><span
lang=TH style='color:#2F3438'> &quot;<span class=SpellE>Matrix</span>,&quot;
&quot;Johnson,&quot; &quot;Johnson <span class=SpellE>Fitness</span>,&quot;
&quot;</span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>บริษัท</span><span lang=TH style='color:#2F3438'>,&quot; &quot;</span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เรา</span><span lang=TH
style='color:#2F3438'>,</span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>”</span><span lang=TH style='color:#2F3438'> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และ</span><span lang=TH
style='color:#2F3438'> &quot;</span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>ของเรา</span><span lang=TH style='color:#2F3438'>&quot; ) </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ผ่านเว็บไซต์ที่</span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>&nbsp;</span><span class=SpellE><span lang=TH>www</span></span><span
lang=TH>.<span class=SpellE>matrixfitness</span>.<span class=SpellE>com</span><span
style='color:#2F3438'>, </span></span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>แอปพลิ<span class=SpellE>เค</span>ชันในแผงควบคุมของผลิตภัณฑ์ออกกำลังกาย</span><span
lang=TH style='color:#2F3438'>, </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ผลิตภัณฑ์ดิจิทัล</span><span
lang=TH style='color:#2F3438'>, </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>แอปพลิ<span class=SpellE>เค</span>ชันในมือถือ</span><span
lang=TH style='color:#2F3438'> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>และเว็บไซต์</span><span lang=TH style='color:#2F3438'> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>รวมถึงแอปพลิ<span class=SpellE>เค</span>ชันในมือถือ</span><span
lang=TH style='color:#2F3438'> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>ที่บริษัทจัดหาให้กับคู่ค้าทางธุรกิจภายนอก</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ที่มีนโยบายความเป็นส่วนตัวฉบับนี้หรือที่คุณอ่านนโยบายดังกล่าวได้</span><span
lang=TH style='color:#2F3438'> (&quot;</span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>บริการใน</span><span lang=TH
style='color:#2F3438'> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span>&quot;) </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>โปรดอ่านนโยบายความเป็นส่วนตัวฉบับนี้อย่างรอบคอบก่อนที่คุณจะใช่บริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span></span></span><span
lang=TH><o:p></o:p></span></p>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>หากคุณมีคำถามใด</span><span lang=TH style='color:#2F3438'>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>ๆ</span><span lang=TH style='color:#2F3438'> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>เกี่ยวกับหลักปฏิบัติด้านความเป็นส่วนตัวของเรา</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>โปรดติดต่อเราตามที่ระบุในหัวข้อ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>“ติดต่อเรา”</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ด้านล่าง</span><span lang=TH><o:p></o:p></span></p>

<h1><strong><span lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>1. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>ข้อมูลที่เรารวบรวม</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<h3><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>เป้าหมายหลักของเราในการรวบรวมข้อมูลคือเพื่อให้บริการและปรับปรุงบริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span>, </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>เพื่อจัดการการใช้บริการใน</span><span lang=TH style='color:#2F3438'>
<span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> (</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>รวมถึงบัญชีของคุณ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ถ้าคุณเป็นเจ้าของบัญชี</span><span
lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และเพื่อช่วยให้คุณเพลิดเพลินและใช้บริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>ได้อย่างง่ายดาย</span><span lang=TH style='color:#2F3438'> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>เรายังรวบรวมข้อมูลเพื่อวัตถุประสงค์อื่นตามที่อธิบายไว้ในนโยบายความเป็นส่วนตัวฉบับนี้ด้วย</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ตัวอย่างบางส่วนของประเภทข้อมูลที่เรารวบรวมและสถานที่ที่เรารวบรวมข้อมูลมีดังต่อไปนี้</span><span
lang=TH style='color:#2F3438'>:</span><span lang=TH> <br>
</span><strong><span lang=TH style='mso-ascii-font-family:Tahoma'>ก</span></strong><strong><span
lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman"'>. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>ข้อมูลที่คุณมอบให้</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l1 level1 lfo1;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>การสร้างบัญชี</span></strong><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>คุณอาจใช้บริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>บางส่วนได้โดยไม่ต้องลงทะเบียน</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เมื่อคุณสมัครบัญชีในแผงควบคุมหรือบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>อื่น</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ที่ต้องมีการลงทะเบียน</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เราจะขอข้อมูลจากคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ซึ่งประกอบด้วยข้อมูลที่สามารถระบุตัวตนเกี่ยวกับคุณได้</span><span
lang=TH> (</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ข้อมูลที่สามารถใช้ระบุตัวตนของคุณได้</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เช่น</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ชื่อและนามสกุล</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และที่อยู่อีเมลของคุณ</span><span lang=TH>) </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เรายังรวบรวมรหัสผ่าน</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>คำใบ้รหัสผ่าน</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และข้อมูลอื่น</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ๆ</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>สำหรับการยืนยันตัวตนและการเข้าสู่บัญชี</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ซึ่งเป็นส่วนหนึ่งของกระบวนการลงทะเบียนด้วย</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หากคุณเลือกสร้างบัญชีผ่านหนึ่งในบัญชีของคุณจากบริการภายนอกบางอย่าง</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เช่น</span><span lang=TH> <span class=SpellE>Facebook</span>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>หรือ</span><span lang=TH> <span class=SpellE>Twitter</span> (</span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>แต่ละอย่างคือบริการเครือข่ายโซ<span
class=SpellE>เชีย</span>ลหรือ</span><span lang=TH> &quot;</span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>บัญชี</span><span lang=TH> SNS</span></span><span
lang=TH>&quot;) </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>คุณอาจต้องมอบชื่อผู้ใช้</span><span lang=TH> (</span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>หรือ</span><span
lang=TH> ID </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ผู้ใช้</span><span lang=TH>) </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ของคุณให้กับเรา</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เพื่อให้สามารถยืนยันตัวตนของคุณได้โดยใช้บัญชี</span><span
lang=TH> SNS </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เมื่อยืนยันตัวตนของคุณเสร็จสมบูรณ์แล้ว</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เราจะสามารถเชื่อมโยงบัญชีของคุณกับบัญชี</span><span
lang=TH> SNS </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ได้</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และเข้าถึงข้อมูลบางอย่าง</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เช่น</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ชื่อและที่อยู่อีเมลของคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และข้อมูลอื่น</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ที่การตั้งค่าความเป็นส่วนตัวในบัญชี</span><span
lang=TH> SNS </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>อนุญาตให้เราเข้าถึง&nbsp;</span><span lang=TH><o:p></o:p></span></p>

<p><span lang=TH><o:p>&nbsp;</o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l5 level1 lfo2;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma'>ข้อมูลโปรไฟล์</span></strong><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>คุณมีตัวเลือกในการมอบข้อมูลเพิ่มเติม</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ซึ่งเป็นส่วนหนึ่งของโปรไฟล์ของคุณ</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ซึ่งบางส่วนเป็นข้อมูลที่ระบุตัวตนได้</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>หมายเลขโทรศัพท์</span><span lang=TH> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>หรือภาพของโปรไฟล์</span><span lang=TH> </span><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และคำอธิบายสั้น</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ๆ</span><span lang=TH> </span><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เกี่ยวกับตัวคุณเอง</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>แต่ข้อมูลเหล่านี้ไม่ใช่ข้อมูลที่จำเป็นต้องระบุ&nbsp;</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><span lang=TH><o:p>&nbsp;</o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l4 level1 lfo3;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma'>กิจกรรมฟิต<span class=SpellE>เนส</span>ของคุณ</span></strong><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#333333'>เราอาจรวบรวมข้อมูลเกี่ยวกับกิจกรรมฟิต<span
     class=SpellE>เนส</span>และลักษณะทางกายภาพของคุณ</span><span lang=TH
     style='color:#333333'> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#333333'>เช่น</span><span lang=TH
     style='color:#333333'> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#333333'>คลับหลัก</span><span lang=TH
     style='color:#333333'>, ID </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#333333'>สมาชิกฟิต<span class=SpellE>เนส</span></span><span
     lang=TH style='color:#333333'>, </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>เพศ</span><span
     lang=TH style='color:#333333'>, </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>วันเกิด</span><span
     lang=TH style='color:#333333'>, </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ส่วนสูง</span><span
     lang=TH style='color:#333333'>, </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>น้ำหนัก</span><span
     lang=TH style='color:#333333'>, </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ชื่อของครูฝึก</span><span
     lang=TH style='color:#333333'>, </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ตารางการออกกำลังกาย</span><span
     lang=TH style='color:#333333'>,</span><span lang=TH> </span><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
     color:#333333'>ข้อมูลการออกกำลังกาย</span><span lang=TH style='color:#333333'>
     (</span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#333333'>เช่น</span><span lang=TH
     style='color:#333333'> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#333333'>ระยะเวลาและความเข้มข้นของการออกกำลังกาย</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>เวลาออกกำลังกาย</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>อุปกรณ์ที่ใช้</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>แคลอรี่ที่เผาผลาญ</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ระยะทาง</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ความเร็วเฉลี่ย</span><span
     lang=TH style='color:#333333'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>คุณอาจเพิ่มข้อมูลที่เก็บรวบรวมนี้ได้ด้วยตนเองหรือเราอาจจะได้รับข้อมูลจากบริการภายนอกที่เชื่อมโยงกับบัญชีของคุณ</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>เรายังอาจจัดเก็บข้อมูลเกี่ยวกับระดับสมรรถภาพร่างกาย</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>การวัด</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ดัชนีมวลกาย</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>และข้อมูลอื่น</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ๆ</span><span
     lang=TH style='color:#333333'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#333333'>ที่คุณมอบให้แก่ครูฝึกในนามของสถาบันฟิต<span
     class=SpellE>เนส</span>ของคุณด้วย</span><span lang=TH style='mso-fareast-font-family:
     "Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><span lang=TH><o:p>&nbsp;</o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l8 level1 lfo4;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma'>การสื่อสาร</span></strong><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>เรารวบรวมเนื้อหาข้อความที่คุณส่งให้กับเราหรือครูฝึกของคุณ</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>เช่น</span><span lang=TH> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>คำติชม</span><span lang=TH> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma'>คำถาม</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>และข้อมูลที่คุณมอบให้แก่ฝ่ายสนับสนุนลูกค้า</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>คุณอาจเลือกส่งข้อมูลอื่น</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ให้แก่เราโดยสมัครใจผ่านบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ซึ่งเราไม่ได้ขอ</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และในกรณีดังกล่าว</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>คุณก็เป็นผู้รับผิดชอบข้อมูลดังกล่าวแต่เพียงผู้เดียว</span><span
lang=TH><o:p></o:p></span></p>

<p><span lang=TH><br>
</span><strong><span lang=TH style='mso-ascii-font-family:Tahoma'>ข</span></strong><strong><span
lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman"'>. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>ข้อมูลที่รวบรวมโดยอัตโนมัติ</span></strong><span
lang=TH> <br>
</span><span class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เรารวบรวมข้อมูลเกี่ยวกับอุปกรณ์ของคุณและวิธีที่อุปกรณ์ของคุณโต้ตอบกับ</span></span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>บริการในแผงควบคุม&nbsp;</span><span class=SpellE><span lang=TH
style='color:#2F3438'>Matrix</span></span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>โดยอัตโนมัติ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ตัวอย่างบางส่วนของข้อมูลที่เรารวบรวมได้แก่ข้อมูลการใช้บริการ</span><span
lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เช่น</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>คุณสมบัติที่คุณใช้งานด้วยบริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>และในผลิตภัณฑ์ดิจิทัล</span><span lang=TH style='color:#2F3438'>,
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>หน้าเว็บที่คุณเยี่ยมชมผ่านแผงควบคุม</span><span lang=TH
style='color:#2F3438'>, </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>การออกกำลังกายของคุณที่ผูกเข้ากับ</span><span
lang=TH style='color:#2F3438'> ID </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ผู้ใช้ของคุณ</span><span lang=TH
style='color:#2F3438'>, </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เวลาของวันที่คุณเรียกดู</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และหน้าเว็บที่เชื่อมโยงและที่ออก</span><span
lang=TH style='color:#2F3438'>), </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ข้อมูลการเชื่อมต่อและการกำหนดค่าของอุปกรณ์</span><span
lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เช่น</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ประเภทอุปกรณ์และ<span class=SpellE>เบ</span>ราว์<span
class=SpellE>เซอร์</span>ที่คุณใช้</span><span lang=TH style='color:#2F3438'>, </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>ระบบปฏิบัติการของอุปกรณ์</span><span lang=TH style='color:#2F3438'>,
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>ผู้ให้บริการอินเทอร์เน็ตของคุณ</span><span lang=TH
style='color:#2F3438'>, </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>การตั้งค่าภูมิภาคและภาษาของอุปกรณ์</span><span
lang=TH style='color:#2F3438'>, </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และตัวระบุของอุปกรณ์ของคุณ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เช่น</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ที่อยู่</span><span lang=TH
style='color:#2F3438'> IP, </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ที่อยู่</span><span lang=TH
style='color:#2F3438'> MAC </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และ</span><span lang=TH
style='color:#2F3438'> ID </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>โฆษณาของอุปกรณ์</span><span
lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และข้อมูลตำแหน่งที่ตั้ง</span><span
lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เช่น</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ข้อมูลละติจูด</span><span lang=TH
style='color:#2F3438'>/</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ลองจิจูด</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือตำแหน่งที่ตั้งที่ได้จากที่อยู่</span><span
lang=TH style='color:#2F3438'> IP </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือข้อมูลที่ระบุระดับเมืองหรือรหัสไปรษณีย์</span><span
lang=TH style='color:#2F3438'>)</span><span lang=TH> <br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>เราใช้เทคโนโลยีที่พัฒนาขึ้นทั้งในปัจจุบันและในภายภาคหน้า</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เพื่อรวบรวมข้อมูลนี้</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>รวมถึงเทคโนโลยีดังต่อไปนี้</span><span
lang=TH style='color:#2F3438'>:</span><span lang=TH><o:p></o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l3 level1 lfo5;tab-stops:list 36.0pt'><span
     class=inline-comment-marker><b><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ข้อมูลบันทึก</span></b></span><strong><span
     lang=TH style='mso-ascii-font-family:Tahoma'>&nbsp;</span></strong><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>&nbsp;เมื่อคุณเข้าถึงบริการใน</span><span lang=TH> <span
     class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ข้อมูลที่สร้างขึ้นระหว่าง<span class=SpellE>เซ</span>สชันของคุณจะได้รับการบันทึกและจัดเก็บใน<span
     class=SpellE>เซิร์ฟเวอร์</span>ของเราโดยอัตโนมัติ</span><span lang=TH
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l3 level1 lfo5;tab-stops:list 36.0pt'><span
     class=inline-comment-marker><b><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>คุกกี้</span></b></span><strong><span
     lang=TH style='mso-ascii-font-family:Tahoma'>&nbsp;</span></strong><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>&nbsp;เรารวบรวมข้อมูลบางอย่างผ่านการใช้</span><span lang=TH> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>“คุกกี้”</span><span lang=TH> </span><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ซึ่งก็คือไฟล์ข้อความขนาดเล็กที่<span
     class=SpellE>เบ</span>ราว์<span class=SpellE>เซอร์</span>บันทึกไว้เมื่อคุณเข้าถึงบริการใน</span><span
     lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>เราอาจใช้ทั้งคุกกี้ชั่วคราว</span><span
     lang=TH> (<span class=SpellE>session</span> <span class=SpellE>cookies</span>)
     </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>และคุกกี้ที่มีกำหนดอายุ</span><span lang=TH>
     (<span class=SpellE>persistent</span> <span class=SpellE>cookies</span>) </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>เพื่อระบุว่าคุณเข้าสู่ระบบบริการใน</span><span lang=TH> <span
     class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>และเพื่อแจ้งเราว่าคุณโต้ตอบกับบริการใน</span><span
     lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>อย่างไรและเมื่อใด</span><span lang=TH> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>เรายังอาจใช้คุกกี้เพื่อติดตามการใช้งานแบบรวมและเส้นทางการรับส่งข้อมูลของเว็บบนบริการใน</span><span
     lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>และเพื่อปรับแต่งและปรับปรุงบริการใน</span><span
     lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span><span style='mso-spacerun:yes'>  </span></span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>อีกด้วย</span><span lang=TH> </span><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>นอกจากนี้</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>คุกกี้ยังช่วยให้เรานำเสนอโฆษณาให้แก่คุณ</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ทั้งในและนอกบริการใน</span><span lang=TH> <span
     class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>คุกกี้ชั่วคราวจะถูกลบไปเมื่อคุณออกจากระบบบริการใน</span><span
     lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
     class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>และปิด<span class=SpellE>เบ</span>ราว์<span
     class=SpellE>เซอร์</span></span><span lang=TH> </span><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ซึ่งแตกต่างจากคุกกี้ที่มีกำหนดเวลา</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ผู้ให้บริการภายนอกบางส่วนอาจวางคุกกี้ของตนเองไว้ใน<span
     class=SpellE>เบ</span>ราว์<span class=SpellE>เซอร์</span>ของคุณด้วย</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>ทั้งนี้</span><span lang=TH> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma'>โปรดทราบว่านโยบายความเป็นส่วนตัวฉบับนี้ครอบคลุมเฉพาะการใช้คุกกี้ของเรา</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>และไม่รวมคุกกี้จากบุคคลภายนอกดังกล่าว</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p style='margin-bottom:12.0pt'><strong><span lang=TH style='mso-ascii-font-family:
Tahoma'>ค</span></strong><strong><span lang=TH style='font-family:"Tahoma",sans-serif;
mso-hansi-font-family:"Times New Roman";mso-bidi-font-family:"Times New Roman"'>.
</span></strong><strong><span lang=TH style='mso-ascii-font-family:Tahoma'>ข้อมูลจากแหล่งอื่น</span></strong><span
lang=TH> <br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>เรายังรวบรวมข้อมูลเกี่ยวกับคุณจากแหล่งภายนอกอื่น</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ด้วย</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ภายในขอบเขตที่เรารวมข้อมูลที่ได้จากแหล่งภายนอกดังกล่าวเข้ากับข้อมูลที่เรารวบรวมเกี่ยวกับคุณผ่านบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เราจะปฏิบัติต่อข้อมูลที่รวมนั้นตามหลักปฏิบัติที่อธิบายไว้ในนโยบายความเป็นส่วนตัวฉบับนี้</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>รวมถึงข้อจำกัดเพิ่มเติมใด</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ที่แหล่งที่มาของข้อมูลกำหนด</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>แหล่งภายนอกเหล่านี้แตกต่างกันไปเมื่อเวลาผ่านไป</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>แต่รวมถึง</span><span lang=TH>: <o:p></o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo6;tab-stops:list 36.0pt'><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>อุปกรณ์ฟิต<span
     class=SpellE>เนส</span>ที่เชื่อมโยงกับกิจกรรมของคุณ</span><span lang=TH
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo6;tab-stops:list 36.0pt'><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การออกกำลังกายที่เชื่อมต่อกับบัญชีผู้ใช้ของคุณ</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo6;tab-stops:list 36.0pt'><span lang=TH
     style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>พันธมิตรที่เราเสนอการให้บริการร่วมแบรนด์</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>การขายหรือจัดจำหน่ายผลิตภัณฑ์ของเรา</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>หรือมีส่วนร่วมในกิจกรรมการตลาดร่วมกัน</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><span lang=TH><br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>หากต้องการข้อมูลเพิ่มเติมเกี่ยวกับบริการภายนอก</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>โปรดดูหัวข้อ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>“บุคคลภายนอก”</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ด้านล่าง</span><span
lang=TH> <br>
<br>
<strong><span style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>2. </span></strong></span><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>การใช้ข้อมูล</span></strong><span
lang=TH> <br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>บริษัทประมวลผลข้อมูลของคุณเพื่อวัตถุประสงค์ตามที่อธิบายในนโยบายความเป็นส่วนตัวฉบับนี้</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>นอกจากวัตถุประสงค์ที่ระบุที่อื่นภายในนโยบายความเป็นส่วนตัวฉบับนี้แล้ว</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>บริษัทยังใช้ข้อมูลเกี่ยวกับคุณสำหรับผลประโยชน์ที่ชอบด้วยกฎหมายของเรา</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>รวมถึง</span><span lang=TH>:<o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การจัดการบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>รวมถึงการลงทะเบียนและบัญชีของคุณ</span><span
lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การให้บริการที่คุณร้องขอ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เช่น</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ตอบความคิดเห็น</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>คำถาม</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และคำขอของคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และให้บริการลูกค้า</span><span lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การรับประกันการทำงานเชิงเทคนิคของบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การส่งการแจ้งเตือนทางเทคนิค</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>การอ<span class=SpellE>ัป</span>เดต</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>การแจ้งเตือนด้านความปลอดภัย</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ข้อมูลเกี่ยวกับการเปลี่ยนแปลงนโยบาย</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และข้อความสนับสนุนและข้อความการดูแล</span><span
lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การปกป้องสิทธิและทรัพย์สินของบริษัท</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>บุคคลภายนอก</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>หรือผู้ใช้ของเรา</span><span
lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การป้องกันและจัดการกับการฉ้อโกง</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>การละเมิดนโยบายหรือเงื่อนไข</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และภัยคุกคามหรืออันตราย</span><span
lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การตรวจสอบ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>วิจัย</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และวิเคราะห์เพื่อรักษา</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ปกป้อง</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และปรับปรุงบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ผลิตภัณฑ์ของบริษัทอื่น</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>การทำตลาดและบริการ</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และการใช้อุปกรณ์ออกกำลังกาย</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และ</span><span lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การพัฒนาบริการใหม่</span><span
lang=TH><o:p></o:p></span></p>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>เรายังใช้ข้อมูลเกี่ยวกับคุณโดยที่ได้รับความยินยอมของคุณแล้ว</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เช่นเพื่อ</span><span lang=TH>:<o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>พัฒนาและแสดงเนื้อหาและโฆษณาที่ปรับแต่ง</span><span
lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>แสดงโฆษณาที่เลือกสรรตามความสนใจของคุณในบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และบริการภายนอกและ</span><span lang=TH><o:p></o:p></span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=TH style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ปฏิบัติตามวัตถุประสงค์อื่นใดที่เปิดเผยให้คุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และได้รับความยินยอมจากคุณ</span><span lang=TH><o:p></o:p></span></p>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>เราอาจใช้ข้อมูลที่ไม่ระบุตัวตนของคุณ</span><span lang=TH> (</span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>รวมถึง</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ข้อมูลที่ทำให้ไม่สามารถระบุตัวตนได้</span><span
lang=TH>) </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>โดยไม่มีข้อผูกพันกับคุณ</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ยกเว้นมีกฎหมายที่เกี่ยวข้องสั่งห้าม</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>สำหรับข้อมูลเกี่ยวกับสิทธิและทางเลือกเกี่ยวกับวิธีที่เราใช้งานข้อมูลของคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>โปรดดูหัวข้อ</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>“สิทธิและทางเลือกของคุณ”</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ด้านล่าง</span><span lang=TH><br>
<br>
<strong><span style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman";color:#2F3438'>3. </span></strong></span><strong><span
lang=TH style='mso-ascii-font-family:Tahoma;color:#2F3438'>การแบ่งปันข้อมูล</span></strong><span
lang=TH><br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>บริษัทแบ่งปันข้อมูลคุณดังต่อไปนี้</span><span lang=TH
style='color:#2F3438'>:</span><span lang=TH><o:p></o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma;color:#2F3438'>คลับและครูฝึก</span></strong><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เราแบ่งปันข้อมูลของคุณ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>รวมถึงชื่อ</span><span
     lang=TH style='color:#2F3438'>, </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ที่อยู่อีเมล</span><span
     lang=TH style='color:#2F3438'>, ID </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>บัญชีบริการใน</span><span
     lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
     class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma;color:#2F3438'>และข้อมูลการออกกำลังกาย</span><span lang=TH
     style='color:#2F3438'> (</span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#2F3438'>เช่น</span><span lang=TH
     style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#2F3438'>ระยะเวลาและความเข้มข้นของการออกกำลังกาย</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เวลาออกกำลังกาย</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>อุปกรณ์ที่ใช้</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>แคลอรี่ที่เผาผลาญ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ระยะทาง</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ความเร็วเฉลี่ย</span><span
     lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>กับคลับหลัก</span><span
     lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ที่ที่คุณออกกำลังกาย</span><span
     lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>และครูฝึก</span><span
     lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>คนที่คุณออกกำลังกายด้วย</span><span
     lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ที่คุณได้กำหนดไว้</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หากเรามีหน้าที่เป็นผู้ประมวลผลสำหรับคลับและลูกค้าครูฝึก</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เราก็จะแบ่งปันข้อมูลของคุณเพื่ออำนวยความสะดวกในการปฏิบัติตามคำขอของคุณ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>และเพื่อปฏิบัติตามกฎหมายที่ใช้บังคับด้วย</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma;color:#2F3438'>บริษัทในเครือ</span></strong><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#2F3438'>เราแบ่งปันข้อมูลของคุณกับนิติบุคคลที่เกี่ยวข้อง</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>รวมถึงบริษัทแม่และบริษัทในเครือ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ตัวอย่างเช่น</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เราอาจแบ่งปันข้อมูลของคุณกับบริษัทในเครือเพื่อการสนับสนุนลูกค้า</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>การตลาด</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>และการดำเนินการทางเทคนิค</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma'>บุคคลภายนอก</span></strong><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma'>เราแบ่งปันข้อมูลของคุณกับบุคคลภายนอกเพื่ออำนวยความสะดวกในการปฏิบัติตามคำขอของคุณ</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#2F3438'>ตัวอย่างเช่น</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หากคุณเลือกเชื่อมโยงบัญชีของคุณในบริการใน</span><span
     lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
     class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma;color:#2F3438'>เข้ากับเว็บไซต์ภายนอกและเว็บไซต์เครือข่ายโซ<span
     class=SpellE>เชีย</span>ล</span><span lang=TH style='color:#2F3438'> </span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma;color:#2F3438'>และต้องการเปิดเผยข้อมูลสาธารณะของคุณในเว็บไซต์ภายนอก</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>คุณอาจขอให้เราเปิดเผยข้อมูลได้ผ่านบริการใน</span><span
     lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
     class=SpellE>On</span>-<span class=SpellE>Console</span></span><span
     lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
     Tahoma;color:#2F3438'>&nbsp;</span><span lang=TH style='mso-fareast-font-family:
     "Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma;color:#2F3438'>การควบรวมหรือซื้อกิจการ</span></strong><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หากบริษัทเข้าไปเกี่ยวข้องกับการควบรวมหรือการซื้อกิจการ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หรือการขายสินทรัพย์บางส่วนหรือทั้งหมด</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ข้อมูลของคุณอาจถูกถ่ายโอน</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>โดยเกี่ยวข้องกับธุรกรรมดังกล่าว</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ในกรณีดังกล่าว</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เราจะใช้ความพยายามอันสมเหตุสมผลทางการค้าเพื่อรักษาความลับของข้อมูลใด</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ๆ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ที่เกี่ยวข้องในธุรกรรมดังกล่าว</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>และแจ้งคุณเมื่อข้อมูลส่วนบุคคลของคุณอยู่ภายใต้นโยบายความเป็นส่วนตัวฉบับอื่น</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma;color:#2F3438'>การรักษาความปลอดภัยหรือการเปิดเผยที่บังคับ</span></strong><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เราแบ่งปันข้อมูลของคุณ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หากเรามีความเชื่อโดยสุจริตใจว่าการเข้าถึง</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>การใช้</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>การรักษา</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หรือการเปิดเผยข้อมูลดังกล่าวมีความจำเป็นที่สมเหตุสมผลเพื่อ</span><span
     lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ก</span><span
     lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ปฏิบัติตามกฎหมาย</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ระเบียบข้อบังคับ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>กระบวนการทางกฎหมาย</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หรือคำขอของรัฐที่มีผลบังคับใช้</span><span
     lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ข</span><span
     lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>บังคับใช้เงื่อนไขการบริการ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>รวมถึงการตรวจสอบการฝ่าฝืนใด</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ๆ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ที่เป็นไปได้</span><span
     lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ค</span><span
     lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ตรวจจับ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ป้องกัน</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หรือจัดการกับการฉ้อโกง</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>การรักษาความปลอดภัย</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หรือปัญหาทางเทคนิคโดยประการอื่นหรือ</span><span
     lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ง</span><span
     lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ป้องกันอันตรายต่อสิทธิ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ทรัพย์สิน</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หรือความปลอดภัยของบริษัท</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ผู้ใช้</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>หรือสาธารณชน</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>ตามที่กฎหมายกำหนดหรืออนุญาต</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เราแบ่งปันข้อมูลของคุณ</span><span
     lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
     mso-ascii-font-family:Tahoma;color:#141412'>เพื่อตอบสนองคำขอที่ชอบด้วยกฎหมายจากหน่วยงานสาธารณะ</span><span
     lang=TH style='color:#141412'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#141412'>รวมถึงเพื่อปฏิบัติตามข้อกำหนดด้านการรักษาความปลอดภัยระดับประเทศหรือการบังคับใช้กฎหมาย</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=TH
     style='mso-ascii-font-family:Tahoma;color:#2F3438'>ความยินยอม</span></strong><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>เราแบ่งปันข้อมูลของคุณสำหรับวัตถุประสงค์อื่นใดที่เปิดเผยต่อคุณ</span><span
     lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:
     "Times New Roman",serif;mso-ascii-font-family:Tahoma;color:#2F3438'>และเมื่อเรามีความยินยอมของคุณ</span><span
     lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>ทั้งนี้ไม่มีข้อจำกัดของข้อความข้างต้น</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เราอาจแบ่งปันข้อมูลแบบรวมบางส่วน</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ซึ่งไม่ระบุตัวตนของคุณให้แก่บุคคลภายนอก</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เช่น</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ระยะเวลาและความเข้มข้นของการออกกำลังกาย</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ตัวอย่างเช่น</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เพื่อการวิเคราะห์อุตสาหกรรม</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>การรวบรวมข้อมูลทางประชากรศาสตร์</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และวัตถุประสงค์ด้านการวิจัยและการตลาดอื่นที่คล้ายกัน</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>โปรดติดต่อเราตามช่องทางที่ระบุไว้ในหัวข้อ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>“ติดต่อเรา”</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ด้านล่าง</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หากมีคำถามเพิ่มเติมเกี่ยวกับการจัดการหรือการใช้ข้อมูลของคุณ</span><span
lang=TH> <br>
<br>
<strong><span style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>4. </span></strong></span><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>บุคคลภายนอก</span></strong><span
lang=TH><o:p></o:p></span></p>

<p><strong><span lang=TH style='mso-ascii-font-family:Tahoma'>ก</span></strong><strong><span
lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman"'>. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>บริการภายนอก</span></strong><span
lang=TH> <br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>บริการใน</span><span lang=TH style='color:#2F3438'> <span
class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span class=SpellE>Console</span>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>อาจมีลิงก์ที่เชื่อมโยงไปยังเว็บไซต์หรือบริการอื่น</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ข้อมูลใด</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ๆ</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ที่คุณมอบให้ในหรือมอบให้แก่เว็บไซต์หรือบริการภายนอก</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>รวมถึงแต่ไม่จำกัดเพียง</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>itbit</span>.<span
class=SpellE>com</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือ</span><span lang=TH
style='color:#2F3438'> <span class=SpellE>mapmyfitness</span>.<span
class=SpellE>com</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เป็นการมอบให้แก่เจ้าของของเว็บไซต์หรือบริการโดยตรง</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และอยู่ภายใต้นโยบายความเป็นส่วนตัวของบุคคลภายนอกนั้น</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>นโยบายความเป็นส่วนตัวของเราไม่มีผลบังคับใช้กับเว็บไซต์หรือบริการดังกล่าว</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และเราไม่รับผิดชอบสำหรับเนื้อหา</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ความเป็นส่วนตัว</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือแนวปฏิบัติและนโยบายด้านการรักษาความปลอดภัยของเว็บไซต์หรือบริการเหล่านั้น</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เพื่อเป็นการคุ้มครองข้อมูลของคุณ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เราแนะนำให้คุณตรวจสอบนโยบายความเป็นส่วนตัวของเว็บไซต์และบริการอื่นที่คุณเข้าถึงอย่างรอบคอบ</span><span
lang=TH><o:p></o:p></span></p>

<p><span class=inline-comment-marker><b><span lang=TH>Google <span
class=SpellE>Analytics</span></span></b></span><span lang=TH><br>
<span class=inline-comment-marker>Google <span class=SpellE>Analytics</span> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>คือบริการของ</span><span lang=TH> Google Inc. </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ซึ่งตั้งอยู่ที่</span><span lang=TH> 1600 <span
class=SpellE>Amphitheatre</span> <span class=SpellE>Parkway</span>, <span
class=SpellE>Mountain</span> <span class=SpellE>View</span>, CA 94043, USA
(&quot;Google&quot;) </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>บริษัทใช้ฟังก์ชันการทำงานของ</span><span
lang=TH> Google <span class=SpellE>Analytics</span> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ซึ่งก็คือ</span><span lang=TH> &quot;ID </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ผู้ใช้</span><span lang=TH>&quot; </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ผ่านทุกแพลตฟอร์มในบริการใน</span><span lang=TH> <span
class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span class=SpellE>Console</span>
</span></span><span class=inline-comment-marker><span lang=TH style='font-family:
"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เพื่อรวบรวมข้อมูลเกี่ยวกับการที่คุณใช้บริการของเรา</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ทั้งในระดับท้องถิ่นและระดับโลก</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เพื่อปรับปรุงและช่วยเราวิเคราะห์บริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เว็บไซต์ของเราใช้</span><span
lang=TH> Google <span class=SpellE>Analytics</span> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เพื่อตรวจติดตามการรับส่งข้อมูลและประสบการณ์ของผู้ใช้</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>แต่ไม่มีการรวบรวมข้อมูลที่ระบุตัวตนได้ในลักษณะนี้&nbsp;ข้อมูลเกี่ยวกับการใช้ฟังก์ชันและแอปพลิ<span
class=SpellE>เค</span>ชันในแผงควบคุมได้รับการรวบรวมผ่านการลงทะเบียนและการใช้บริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ซึ่งประกอบด้วยสิ่งต่าง</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ต่อไปนี้</span><span
lang=TH>: </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>แอปพลิ<span
class=SpellE>เค</span>ชันที่ใช้ระหว่างการออกกำลังกาย</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ระหว่างการใช้งาน</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ประวัติการค้นหา</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และสถิติการออกกำลังกาย</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>รวมถึงข้อมูลแบบรวมเกี่ยวกับการใช้แอปพลิ<span
class=SpellE>เค</span>ชัน</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>คีย์ประจำตัวช่วยให้ระบุตัวของผู้ใช้แต่ละคนได้</span><span
lang=TH style='color:#091E42'> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#091E42'>อย่างไรก็ตาม</span><span lang=TH style='color:#091E42'> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>คีย์นี้ไม่มีการระบุตัวตน</span><span
lang=TH style='color:#091E42'> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#091E42'>และไม่สามารถผูกกับข้อมูลที่ระบุตัวตนของผู้ใช้ได้</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ข้อมูลเข้าสู่ระบบของผู้ใช้ไม่มีการจัดเก็บหรือประมวลผล</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ผลการวิเคราะห์เกี่ยวกับการใช้งานแอปพลิ<span
class=SpellE>เค</span>ชันช่วยเราปรับปรุงบริการของเรา</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เพื่อพัฒนาคุณสมบัติใหม่</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ๆ</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และเพื่อปรับปรุงความมุ่งเน้นของเราตามความต้องการของผู้ใช้ให้ดีที่สุด&nbsp;</p>

<h3><strong><span lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>5. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>สิทธิและทางเลือกของคุณ</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<h3><strong><span lang=TH style='mso-ascii-font-family:Tahoma'>ก</span></strong><strong><span
lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman"'>. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>การเข้าถึงและลบข้อมูลบัญชี</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>เมื่อคุณใช้บริการใน</span><span lang=TH> <span class=SpellE>Matrix</span>
<span class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เราจะใช้ความพยายามอันสุจริตใจที่สมเหตุสมผลเพื่อให้คุณเข้าถึงข้อมูลที่คุณส่งให้กับเราผ่านบัญชีของคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และเพื่อแก้ไขข้อมูลนี้หากไม่ถูกต้อง</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หรือเพื่อลบข้อมูลตามคำขอของคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หากไม่จำเป็นต้องเก็บรักษาไว้ตามกฎหมายโดยประการอื่นหรือเพื่อวัตถุประสงค์ทางธุรกิจที่ถูกต้องตามกฎหมาย</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>โปรดอย่าลังเลที่จะแจ้งให้เราทราบ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หากคุณต้องการแก้ไขหรือลบข้อมูลของคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>โดยส่งอีเมลหาเราตามที่ระบุในหัวข้อ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>“ติดต่อเรา”</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ด้านล่าง</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เราอาจขอข้อมูลเพิ่มเติมจากคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เพื่อให้เราสามารถยืนยันตัวตนของคุณได้</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>โปรดทราบว่าเราจะเก็บรักษาและใช้งานข้อมูลของคุณตามที่จำเป็นเพื่อปฏิบัติตามข้อผูกพันทางกฎหมาย</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ระงับข้อพิพาท</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และบังคับใช้ข้อตกลงของเรา</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ผู้ที่พำนักอาศัย<span class=SpellE>อยู๋</span>ในรัฐแคลิฟอร์เนียและเจ้าของข้อมูลในยุโรปมีสิทธิเพิ่มเติมตามที่ระบุในหัวข้อ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>“สิทธิความเป็นส่วนตัวในแคลิฟอร์เนียของคุณ”</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และ</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>“สิทธิความเป็นส่วนตัวในยุโรปของคุณ”</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ด้านล่าง&nbsp;</span><span lang=TH><br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>เราสามารถลบบัญชีของคุณอย่างถาวรได้ผ่านการตั้งค่าบัญชี</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หรือโดยส่งอีเมลแจ้งเราตามที่ระบุในหัวข้อ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>“ติดต่อเรา”</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ด้านล่าง</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เราจะดำเนินการลบข้อมูลของคุณทันทีที่ทำได้</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>แต่ข้อมูลบางส่วนอาจยังคงอยู่ในสำเนาเก็บบันทึกถาวร</span><span
lang=TH>/</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>สำเนาสำรองข้อมูลสำหรับทะเบียนของเรา</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หรือตามที่กฎหมายกำหนด</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>โปรดทราบว่าเราสามารถลบข้อมูลของคุณภายในบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ได้เท่านั้น</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#091E42'>บริษัทไม่สามารถลบข้อมูลที่คุณแบ่งปันให้กับพันธมิตรและเว็บไซต์ภายนอกได้</span><span
lang=TH style='color:#091E42'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>ดังนั้น</span><span lang=TH
style='color:#091E42'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>ข้อมูลนี้จะยังคงอยู่ในเว็บไซต์และบริการภายนอก</span><span
lang=TH style='color:#091E42'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>แม้ว่าคุณยกเลิกการสมัครรับข้อมูลจากเราแล้ว</span><span
lang=TH style='color:#091E42'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>อย่างไรก็ตาม</span><span lang=TH
style='color:#091E42'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>เมื่อบริษัทได้รับคำขอให้ลบข้อมูล</span><span
lang=TH style='color:#091E42'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#091E42'>บริษัทจะส่งคำขอลบข้อมูลไปยังพันธมิตรภายนอกเหล่านั้นที่บริษัทเคยส่งข้อมูลของคุณไปประมวลผลก่อนหน้านี้&nbsp;</span><span
lang=TH><br>
<br>
</span><strong><span lang=TH style='mso-ascii-font-family:Tahoma;color:#2F3438'>ข</span></strong><strong><span
lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:#2F3438'>. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma;color:#2F3438'>สิทธิความเป็นส่วนตัวในแคลิฟอร์เนียของคุณ</span></strong><span
lang=TH> <br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>กฎหมาย</span><span lang=TH style='color:#2F3438'> &quot;<span
class=SpellE>Shine</span> <span class=SpellE>the</span> <span class=SpellE>Light</span>&quot;
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>ของรัฐแคลิฟอร์เนียอนุญาตให้ลูกค้าในรัฐแคลิฟอร์เนียขอรายละเอียดบางอย่างเกี่ยวกับวิธีการแบ่งปันข้อมูลบางประเภทของตนกับบุคคลภายนอก</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และในบางกรณี</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ก็รวมถึงบริษัทในเครือ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เพื่อวัตถุประสงค์ทางการตลาดโดยตรงของบุคคลภายนอกและบริษัทในเครือ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ตามกฎหมายแล้ว</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ธุรกิจควรมอบข้อมูลบางอย่างให้กับลูกค้าชาวแคลิฟอร์เนียเมื่อมีการร้องขอ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรืออนุญาตให้ลูกค้าชาวแคลิฟอร์เนียเลือกรับหรือไม่รับการแบ่งปันประเภทนี้</span><span
lang=TH> <br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>หากคุณเป็นผู้ที่พำนักอาศัยอยู่ในรัฐแคลิฟอร์เนีย</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และต้องการขอข้อมูลเกี่ยวกับการปฏิบัติตามกฎหมายฉบับนี้ของเรา</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>โปรดติดต่อเราตามที่ระบุในหัวข้อ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>“ติดต่อเรา”</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ด้านล่าง</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>คำขอต้องระบุว่า</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>“คำขอด้านสิทธิความเป็นส่วนตัวในแคลิฟอร์เนีย”</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ในบรรทัดแรกของคำอธิบายและระบุชื่อ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ที่อยู่</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เมือง</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>รัฐ</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และรหัสไปรษณีย์ของคุณ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>โปรดทราบว่าบริษัทไม่จำเป็นต้องตอบสนองคำขอที่ส่งมาด้วยวิธีอื่นนอกจากที่อยู่อีเมลและที่อยู่ทางไปรษณีย์ที่ระบุ</span><span
lang=TH> <br>
<br>
</span><strong><span lang=TH style='mso-ascii-font-family:Tahoma;color:#2F3438'>ค</span></strong><strong><span
lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:"Times New Roman";
mso-bidi-font-family:"Times New Roman";color:#2F3438'>. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma;color:#2F3438'>สิทธิความเป็นส่วนตัวในยุโรปของคุณ</span></strong><span
lang=TH> <br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>หากคุณเป็นเจ้าของข้อมูลในยุโรป</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>คุณมีสิทธิที่จะเข้าถึง</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>แก้ไข</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือลบข้อมูลส่วนบุคคลใด</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ๆ</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ที่เรารวบรวมเกี่ยวกับคุณผ่านบริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>คุณยังมีสิทธิในการโอนย้ายข้อมูล</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>สิทธิที่จะถูกลืม</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และสิทธิที่จะจำกัดหรือคัดค้านการประมวลผลข้อมูลส่วนบุคคลที่เรารวบรวมเกี่ยวคุณผ่านบริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>นอกจากนี้</span><span lang=TH style='color:#2F3438'> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>คุณมีสิทธิที่จะขอไม่ให้เราประมวลผลข้อมูลส่วนบุคคลของคุณ</span><span
lang=TH style='color:#2F3438'> (</span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือมอบให้แก่บุคคลภายนอกเพื่อประมวลผล</span><span
lang=TH style='color:#2F3438'>) </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เพื่อวัตถุประสงค์ทางการตลาด</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือวัตถุประสงค์ที่แตกต่างจากวัตถุประสงค์เดิมหรือที่คุณให้อนุญาตอย่างมีนัยสำคัญ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>คุณอาจเพิกถอนความยินยอมสำหรับการประมวลผลข้อมูลที่เราอาศัยความยินยอมที่คุณมอบให้กับเราเมื่อใดก็ได้</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เพียงแค่ติดต่อเราตามที่ระบุในหัวข้อ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>“ติดต่อเรา”</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ด้านล่าง</span><span
lang=TH><br>
</span><span lang=TH style='font-size:13.5pt;font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หากคุณมีปัญหาใด</span><span lang=TH
style='font-size:13.5pt'> </span><span lang=TH style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH style='font-size:13.5pt'> </span><span lang=TH style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>กับการปฏิบัติตามระเบียบ</span><span
lang=TH style='font-size:13.5pt'> </span><span lang=TH style='font-size:13.5pt;
font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>คุณมีสิทธิที่จะยื่นคำร้องกับหน่วยงานกำกับดูแลของยุโรป&nbsp;</span><span
lang=TH><o:p></o:p></span></p>

<p><strong><span lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>6. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>การรักษาความปลอดภัยข้อมูล</span></strong><span
lang=TH><br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>เราใช้มาตรการรักษาความปลอดภัยที่สมเหตุสมผลและเหมาะสม</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ซึ่งออกแบบมาเพื่อคุ้มครองข้อมูลที่เรารวบรวมจากหรือเกี่ยวกับคุณ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เพื่อป้องกันการเข้าถึง</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การใช้งาน</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>การดัดแปลง</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>การเปิดเผย</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หรือการทำลายที่ไม่ได้รับอนุญาต</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ข้อมูลจะถูกจัดเก็บไว้</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>จนกว่ามีคำขอให้ลบข้อมูลของบัญชีของผู้ใช้ที่จัดเก็บไว้ใน<span
class=SpellE>เซิร์ฟเวอร์</span>ของเราอย่างชัดเจน</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>หรือจนกว่าหลักการทางกฎหมายสำหรับการประมวลผลข้อมูลไม่มีผลใช้งานอีกต่อไป</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>อย่างไรก็ตาม</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>โปรดทราบว่าไม่มีวิธีการส่งข้อมูลทางอินเทอร์เน็ตหรือการจัดเก็บข้อมูลใด</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ๆ</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ที่ปลอดภัยอย่างสมบูรณ์</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ดังนั้น</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เราจึงไม่สามารถรับประกันถึงการรักษาความปลอดภัยของข้อมูลใด</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ๆ</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>โดยสมบูรณ์ได้</span><span
lang=TH><o:p></o:p></span></p>

<h3><strong><span lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>7. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>การถ่ายโอนข้อมูลระหว่างประเทศ</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>เราตั้งอยู่ในสหรัฐอเมริกาและข้อมูลที่เรารวบรวมถูกกำกับดูแลโดยกฎหมายของสหรัฐอเมริกา</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ข้อมูลของคุณอาจถ่ายโอนไปยังและเก็บรักษาไว้ในคอมพิวเตอร์ที่ตั้งอยู่นอกรัฐ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>จังหวัด</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ประเทศ</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หรือเขตการปกครองของรัฐบาลอื่น</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ที่ซึ่งกฎหมายความเป็นส่วนตัวอาจไม่คุ้มครองข้อมูลได้เทียบเท่ากับกฎหมายในเขตการปกครองของคุณ</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>หากคุณอยู่นอกสหรัฐอเมริกาและเลือกจะให้ข้อมูลกับเรา</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เราอาจถ่ายโอนข้อมูลดังกล่าวไปยังสหรัฐอเมริกา</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และเขตการปกครองอื่น</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ๆ</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และประมวลผลข้อมูลที่นั้น</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>การใช้งานบริการใน</span><span
lang=TH style='color:#2F3438'> <span class=SpellE>Matrix</span> <span
class=SpellE>On</span>-<span class=SpellE>Console</span> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>หรือการให้ข้อมูลใด</span><span lang=TH style='color:#2F3438'> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>ๆ</span><span lang=TH style='color:#2F3438'> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>แสดงให้เห็นว่าคุณตกลงที่จะให้ถ่ายโอนและประมวลผลข้อมูล</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หากข้อมูลของคุณรวบรวมในยุโรป</span><span lang=TH>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>เราจะถ่ายโอนข้อมูลส่วนบุคคลของคุณโดยมีการป้องกันที่ถูกต้องหรือเหมาะสม</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>เช่น</span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ข้อสัญญาตามมาตรฐาน&nbsp;</span><span
lang=TH><o:p></o:p></span></p>

<p><span lang=TH><br>
<strong><span style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>8. </span></strong></span><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>นโยบายของเราเกี่ยวกับเด็ก</span></strong><span
lang=TH><br>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>บริการใน</span><span lang=TH style='color:#2F3438'> <span
class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span class=SpellE>Console</span>
</span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>ไม่ได้มีไว้สำหรับผู้ที่มีอายุต่ำกว่า</span><span lang=TH
style='color:#2F3438'> 13 </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ปี</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>และเราไม่มีการรวบรวมข้อมูลส่วนบุคคลตามที่กำหนดโดยรัฐบัญญัติการคุ้มครองความเป็นส่วนตัวทางออนไลน์ของเด็ก</span><span
lang=TH style='color:#2F3438'> (&quot;COPPA&quot;) </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>ของสหรัฐฯ</span><span lang=TH style='color:#2F3438'> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>จากเด็กอายุน้อยกว่า</span><span lang=TH style='color:#2F3438'>
13 </span><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>ปีโดยเจตนา</span><span lang=TH style='color:#2F3438'> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma;
color:#2F3438'>หากเราทราบว่าเรารวบรวมข้อมูลส่วนบุคคลของเด็กอายุต่ำกว่า</span><span
lang=TH style='color:#2F3438'> 13 </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ปี</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เราจะดำเนินการลบข้อมูลดังกล่าวจากไฟล์ของเราโดยเร็วที่สุด</span><span
lang=TH><o:p></o:p></span></p>

<h3><strong><span lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>9. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>การเปลี่ยนแปลงนโยบายความเป็นส่วนตัวฉบับนี้</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma'>โปรดทราบว่านโยบายความเป็นส่วนตัวฉบับนี้อาจมีการเปลี่ยนแปลงเป็นครั้งคราว</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และคุณควรทบทวนนโยบายฉบับนี้เป็น<span
class=SpellE>ระยะๆ</span></span><span lang=TH> </span><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ด้วย</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>การเปลี่ยนแปลงใด</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>จะมีผลทันทีเมื่อได้มีการประกาศนโยบายความเป็นส่วนตัวฉบับแก้ไข</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>การใช้บริการของเราต่อบ่งบอกถึงความยินยอมของคุณต่อนโยบายความเป็นส่วนตัวที่ประกาศ</span><span
lang=TH> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>หากมีการเปลี่ยนแปลงที่สำคัญ</span><span lang=TH> </span><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เราอาจมีการแจ้งเตือนคุณเพิ่มเติมทางที่อยู่อีเมลของคุณ</span><span
lang=TH><o:p></o:p></span></p>

<h3><strong><span lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>10. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>ผู้ควบคุมข้อมูล</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>กฎหมายคุ้มครองข้อมูลของสหภาพยุโรปแยกแยะระหว่างองค์กรที่ประมวลผลข้อมูลส่วนบุคคลเพื่อวัตถุประสงค์ของตนเอง</span><span
lang=TH> (</span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เรียกว่า</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>“ผู้ควบคุม”</span><span
lang=TH>) </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และองค์กรที่ประมวลผลข้อมูลในนามขององค์อื่น</span><span
lang=TH> (</span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เรียกว่า</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>“ผู้ประมวลผล”</span><span
lang=TH>) </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>บริษัทอาจทำหน้าที่เป็นผู้ควบคุมหรือผู้ประมวลผลในส่วนที่เกี่ยวข้องกับข้อมูลส่วนบุคคลของคุณ</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ขึ้นอยู่กับสถานการณ์</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>บริษัทที่ตั้งอยู่ตามที่อยู่ในหัวข้อ</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>“ติดต่อเรา”</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ด้านล่าง</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เป็นผู้ควบคุมข้อมูลที่คุณป้อนผ่านบริการใน</span><span
lang=TH> <span class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span
class=SpellE>Console</span> </span></span><span class=inline-comment-marker><span
lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ข้อมูลนี้ประกอบด้วยข้อมูลประจำตัวสำหรับการลงชื่อเข้าใช้ครั้งเดียว</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ข้อมูลผู้ใช้สำหรับ</span><span
lang=TH> <span class=SpellE>Asset</span> Management </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>และบริการอื่น</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ๆ</span><span lang=TH> </span></span><span
lang=TH><o:p></o:p></span></p>

<p><span class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>บริษัทที่ตั้งอยู่ตามที่อยู่ในหัวข้อ</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>“ติดต่อเรา”</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ด้านล่าง</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เป็นผู้ประมวลผลข้อมูลการออกกำลังกายใด</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ที่สร้างขึ้นผ่านอุปกรณ์ฟิต<span
class=SpellE>เนส</span>&nbsp;</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ลูกค้าที่ซื้อบริการใน</span><span lang=TH> <span
class=SpellE>Matrix</span> <span class=SpellE>On</span>-<span class=SpellE>Console</span>
</span></span><span class=inline-comment-marker><span lang=TH style='font-family:
"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เพื่อมอบประสบการณ์ให้แก่ผู้ใช้ขั้นปลายของตนจะถือเป็นผู้ควบคุมข้อมูล&nbsp;</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ในกรณีนี้</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>บริษัทจะเป็นผู้ประมวลผลข้อมูล</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ซึ่งประมวลผลข้อมูลผู้ใช้ตามความต้องการของลูกค้า</span></span><span
lang=TH><o:p></o:p></span></p>

<p><span class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>บางครั้ง</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>บริษัทดำเนินการในฐานะผู้ประมวลผลในนามของลูกค้า</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ซึ่งเป็นนิติบุคคลที่แยกออกไปและเป็นผู้ควบคุม</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ตัวอย่างเช่น</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>บริษัทจัดหาเทคโนโลยีให้แก่คลับและครูฝึก</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เพื่อมอบแอ<span
class=SpellE>ปแ</span>ละการติดตามฟิต<span class=SpellE>เนส</span>ของพวกเขาเองให้แก่คุณ</span></span><span
lang=TH><o:p></o:p></span></p>

<p><span class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>ในกรณดังกล่าว</span><span lang=TH> </span></span><span
class=inline-comment-marker><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma'>บริษัทจะประมวลผลข้อมูลส่วนบุคคลในนามและตามคำสั่งของลูกค้า</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>โปรดดูข้อมูลเกี่ยวกับหลักปฏิบัติด้านความเป็นส่วนตัวของพวกเขาได้ในนโยบายความเป็นส่วนตัวของลูกค้าที่เกี่ยวข้อง</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>หากคุณมีคำถามใด</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>ๆ</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เกี่ยวกับข้อมูลส่วนบุคคลดังกล่าว</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>และสิทธิของคุณภายใต้กฎหมายการคุ้มครองข้อมูล</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>คำถามดังกล่าวควรส่งไปให้แก่ลูกค้า</span><span
lang=TH> </span></span><span class=inline-comment-marker><span lang=TH
style='font-family:"Times New Roman",serif;mso-ascii-font-family:Tahoma'>เนื่องจากผู้ควบคุมข้อมูลไม่ใช่บริษัท</span></span><span
lang=TH><o:p></o:p></span></p>

<h3><strong><span lang=TH style='font-family:"Tahoma",sans-serif;mso-hansi-font-family:
"Times New Roman";mso-bidi-font-family:"Times New Roman"'>11. </span></strong><strong><span
lang=TH style='mso-ascii-font-family:Tahoma'>ติดต่อเรา</span></strong><span
lang=TH style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span lang=TH style='font-family:"Times New Roman",serif;mso-ascii-font-family:
Tahoma;color:#2F3438'>โปรดอย่าลังเลที่จะส่งคำถามหรือข้อกังวลใด</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>ๆ</span><span lang=TH
style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>เกี่ยวกับนโยบายความเป็นส่วนตัวหรือหลักปฏิบัติด้านข้อมูลของบริษัท</span><span
lang=TH style='color:#2F3438'> </span><span lang=TH style='font-family:"Times New Roman",serif;
mso-ascii-font-family:Tahoma;color:#2F3438'>โดยติดต่อเราที่</span><span
lang=TH style='color:#2F3438'>:</span><span lang=TH> <br>
<br>
<span class=SpellE><span class=inline-comment-marker>privacy</span></span><span
class=inline-comment-marker>@matrixfitness.<span class=SpellE>com</span></span><o:p></o:p></span></p>

<p><span lang=TH><br>
<span class=SpellE>Privacy</span> <span class=SpellE>Team</span><br>
Johnson Health Tech<br>
1600 <span class=SpellE>Landmark</span> Dr<br>
<span class=SpellE>Cottage</span> <span class=SpellE>Grove</span>, WI 53527<o:p></o:p></span></p>

</div>

</body>

</html>`;
export default html_th;