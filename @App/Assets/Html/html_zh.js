const html_zh = `<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=unicode">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List
href="CHT_Johnson%20Health%20Tech%20Privacy%20Policy.fld/filelist.xml">
<title>Johnson Health Tech Privacy Policy - 10-17-2019</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Marta Rodriguez Diaz</o:Author>
  <o:LastAuthor>Microsoft Office User</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>3</o:TotalTime>
  <o:Created>2020-06-03T02:54:00Z</o:Created>
  <o:LastSaved>2020-06-03T02:54:00Z</o:LastSaved>
  <o:Pages>7</o:Pages>
  <o:Words>1099</o:Words>
  <o:Characters>6269</o:Characters>
  <o:Lines>52</o:Lines>
  <o:Paragraphs>14</o:Paragraphs>
  <o:CharactersWithSpaces>7354</o:CharactersWithSpaces>
  <o:Version>16.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData
href="CHT_Johnson%20Health%20Tech%20Privacy%20Policy.fld/themedata.thmx">
<link rel=colorSchemeMapping
href="CHT_Johnson%20Health%20Tech%20Privacy%20Policy.fld/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
  DefSemiHidden="false" DefQFormat="false" DefPriority="99"
  LatentStyleCount="376">
  <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 9"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="header"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footer"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index heading"/>
  <w:LsdException Locked="false" Priority="35" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of figures"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope return"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="line number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="page number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of authorities"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="macro"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="toa heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 5"/>
  <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Closing"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Signature"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="true"
   UnhideWhenUsed="true" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Message Header"/>
  <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Salutation"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Date"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Block Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="FollowedHyperlink"/>
  <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Document Map"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Plain Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="E-mail Signature"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Top of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Bottom of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal (Web)"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Acronym"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Cite"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Code"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Definition"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Keyboard"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Preformatted"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Sample"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Typewriter"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Variable"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Table"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation subject"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="No List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Contemporary"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Elegant"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Professional"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Balloon Text"/>
  <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Theme"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" QFormat="true"
   Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" QFormat="true"
   Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" QFormat="true"
   Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" QFormat="true"
   Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" QFormat="true"
   Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" QFormat="true"
   Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" SemiHidden="true"
   UnhideWhenUsed="true" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Mention"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Smart Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hashtag"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Unresolved Mention"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Smart Link"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 1 6 1 0 1 1 1 1 1;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-ansi-language:#1000;}
h1
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"標題 1 字元";
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:1;
	font-size:24.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-ansi-language:#1000;
	font-weight:bold;}
h3
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"標題 3 字元";
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:3;
	font-size:13.5pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-ansi-language:#1000;
	font-weight:bold;}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:purple;
	text-decoration:underline;
	text-underline:single;}
p
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-ansi-language:#1000;}
p.msonormal0, li.msonormal0, div.msonormal0
	{mso-style-name:msonormal;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:minor-fareast;
	mso-ansi-language:#1000;}
span.1
	{mso-style-name:"標題 1 字元";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"標題 1";
	mso-ansi-font-size:16.0pt;
	mso-bidi-font-size:16.0pt;
	font-family:"新細明體",serif;
	mso-ascii-font-family:新細明體;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:新細明體;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#2F5496;
	mso-themecolor:accent1;
	mso-themeshade:191;}
span.inline-comment-marker
	{mso-style-name:inline-comment-marker;
	mso-style-unhide:no;}
span.3
	{mso-style-name:"標題 3 字元";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"標題 3";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:"新細明體",serif;
	mso-ascii-font-family:新細明體;
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:新細明體;
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	color:#1F3763;
	mso-themecolor:accent1;
	mso-themeshade:127;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	mso-ascii-font-family:"Times New Roman";
	mso-fareast-font-family:新細明體;
	mso-hansi-font-family:"Times New Roman";
	mso-font-kerning:0pt;
	mso-ansi-language:#1000;}
 @page
	{mso-page-border-surround-header:no;
	mso-page-border-surround-footer:no;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 @list l0
	{mso-list-id:81876049;
	mso-list-template-ids:-211786044;}
@list l0:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l0:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l0:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1
	{mso-list-id:322053170;
	mso-list-template-ids:1946576924;}
@list l1:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l1:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l1:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2
	{mso-list-id:626397552;
	mso-list-template-ids:1461375458;}
@list l2:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l2:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l2:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3
	{mso-list-id:727072739;
	mso-list-template-ids:1936103888;}
@list l3:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l3:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l3:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4
	{mso-list-id:886603673;
	mso-list-template-ids:-570018504;}
@list l4:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l4:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l4:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5
	{mso-list-id:1116828760;
	mso-list-template-ids:1623598860;}
@list l5:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l5:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l5:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6
	{mso-list-id:1146584173;
	mso-list-template-ids:1520444454;}
@list l6:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l6:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l6:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7
	{mso-list-id:1844737114;
	mso-list-template-ids:-189209590;}
@list l7:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l7:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l7:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8
	{mso-list-id:2136563767;
	mso-list-template-ids:-1081196432;}
@list l8:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l8:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l8:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 table.MsoNormalTable
	{mso-style-name:表格內文;
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;
	mso-ansi-language:#1000;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>

<body lang=ZH-TW link=blue vlink=purple style='tab-interval:36.0pt'>

<div class=WordSection1>

<h1><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>喬山健康科技隱私權政策</span>
- <span style='font-size:12.0pt'>2019 </span><span style='font-size:12.0pt;
font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman"'>年</span><span
style='font-size:12.0pt'> 10 </span><span style='font-size:12.0pt;font-family:
"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman"'>月</span><span
style='font-size:12.0pt'> 17 </span><span style='font-size:12.0pt;font-family:
"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman"'>日</span><span
style='font-size:12.0pt;mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h1>

<p><strong><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>隱私權政策</span></strong>
<br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>上次更新日期：</span>2019/10/18<br>
<span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>本隱私權政策適用於喬山健康科技有限公司及其關係企業、子公司、分公司和品牌（統稱「</span><span
style='color:#2F3438'>Matrix</span></span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>」、「</span><span style='color:#2F3438'>Johnson</span></span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>」、「</span><span style='color:#2F3438'>Johnson
Fitness</span></span><span class=inline-comment-marker><span style='font-family:
"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman";color:#2F3438'>」、「本公司」或「我們」）透過其位於</span><span
style='color:#2F3438'> </span>www.matrixfitness.com<span style='color:#2F3438'>
</span></span><span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>的網站、其運動產品儀表上的應用程式、數位產品、其行動應用程式，以及本公司為我們第三方業務合作夥伴提供支援且載有或公佈本隱私權政策的網站和行動應用程式提供之所有產品和服務（統稱「</span><span
style='color:#2F3438'>Matrix On-Console </span></span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>服務」）。使用</span><span style='color:#2F3438'>
Matrix On-Console </span></span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>服務之前，請仔細閱讀本隱私權政策。</span></span></p>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>若對我們的隱私權實踐存在任何疑問，請參閱下文中標題為「聯絡我們」的章節聯絡我們。</span></p>

<h1><strong>1.</strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>我們蒐集的資訊</span></strong><span
lang=EN-US style='mso-fareast-font-family:"Times New Roman";mso-ansi-language:
EN-US'><o:p></o:p></span></h1>

<h3><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>我們蒐集資訊的主要目標是提供和改進</span><span style='color:#2F3438'> Matrix
On-Console </span><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>服務，管理您對</span><span style='color:#2F3438'>
Matrix On-Console </span><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>服務的使用情況（若您是帳戶持有者，則包括您的帳戶），以及讓您享用並輕鬆導覽</span><span
style='color:#2F3438'> Matrix On-Console </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務。我們還因本隱私權政策所述之其他用途蒐集資訊。下面列示了我們蒐集的一些資訊類型範例及其來源：</span>
<br>
<strong>A. </strong><strong><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>您提供的資訊</span></strong><span style='mso-fareast-font-family:
"Times New Roman"'><o:p></o:p></span></h3>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l1 level1 lfo1;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><strong><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>帳戶建立。</span></strong><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>您可以在不註冊的情況下使用</span><span style='color:#2F3438'> Matrix
On-Console </span><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>服務的某些功能。</span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>您在註冊儀表帳戶或其他需要註冊的</span> Matrix
On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>服務時，我們會要求您提供一些資訊，包括您的個人可識別資訊（可用於識別您身分的資訊，例如您的姓名和電子郵件地址）。作為註冊程序的一部分，我們還會蒐集密碼、密碼提示以及帳戶存取和驗證所需的其他資訊。若您選擇透過您的某個第三方服務（如</span>
Facebook <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>或</span>
Twitter<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>）帳戶（單獨稱為社交網路服務或「</span><span
class=inline-comment-marker>SNS </span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>帳戶</span></span><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>」）建立帳戶，您可能需要向我們提供您的使用者名稱（或使用者</span>
ID<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>），以便</span>
SNS <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>帳戶對您的身分進行驗證。完成驗證後，我們可以將您的帳戶與</span>
SNS <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>帳戶關聯，並存取某些資訊，例如您的姓名和電子郵件地址，以及</span>
SNS <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>帳戶上您的隱私權設定允許我們存取的其他資訊。</span>&nbsp;</p>

<p><o:p>&nbsp;</o:p></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l5 level1 lfo2;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>個人檔案資訊。</span></strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>您可以選擇提供其他資訊來補充您的個人檔案，其中包括個人識別資訊、電話號碼，或用於個人檔案的相片以及個人簡介，但這些資訊並非必填項。</span>&nbsp;<span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><o:p>&nbsp;</o:p></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l4 level1 lfo3;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>您的健身活動。</span></strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
     color:#333333'>我們可能會蒐集與您的健身活動和身體特徵有關的資訊，例如您辦卡的俱樂部、健身會員</span><span
     style='color:#333333'> ID</span><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#333333'>、性別、出生日期、身高、體重、教練姓名、運動時間表、運動資料（例如運動時長和強度、運動時間、所用器材、已消耗卡路里、距離、平均速度）。蒐集的該資訊可能是您手動新增，也可能是從與您帳戶關聯的其他第三方服務處接收。我們可能還會儲存與您身體狀況、測量結果、身高體重指數</span><span
     style='color:#333333'> (BMI) </span><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#333333'>有關的資訊，以及您向健身房教練提供的其他資訊。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><o:p>&nbsp;</o:p></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l8 level1 lfo4;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>通訊。</span></strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>我們會蒐集您向我們或教練傳送的訊息內容，例如意見回饋、問題，以及您向客戶支援人員提供的資訊。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>您可以選擇透過</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務主動向我們提交我們未要求提供的其他資訊，在這種情況下，您對該等資訊全權負責。</span></p>

<p><br>
<strong>B. </strong><strong><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>自動蒐集的資訊</span></strong> <br>
<span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>我們自動蒐集與您的裝置以及您的裝置與</span><span
style='color:#2F3438'> </span></span><span style='color:#2F3438'>Matrix
On-Console </span><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>服務</span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>互動方式有關的資訊。<span style='color:#2F3438'>我們蒐集的一些資訊範例包括服務使用資料（例如您使用的</span></span><span
style='color:#2F3438'> Matrix On-Console </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務或數位產品的功能、您在儀表上造訪的頁面、與使用者</span><span
style='color:#2F3438'> ID </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>關聯的已執行運動、瀏覽時間、進入和退出的頁面）、裝置連線情況和組態資料（例如裝置類型或所用瀏覽器、裝置的操作軟體、網路服務提供者、裝置的區域和語言設定以及諸如</span><span
style='color:#2F3438'> IP </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>位址、</span><span
style='color:#2F3438'>MAC </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>位址和裝置廣告</span><span
style='color:#2F3438'> ID </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>等裝置識別碼）以及位置資料（例如維度</span><span
style='color:#2F3438'>/</span><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>經度資料或從</span><span style='color:#2F3438'> IP </span><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>位址衍生的位置或表示城市或郵遞區號的資料）。</span> <br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>我們使用各種現行技術和日後開發的技術來蒐集此資訊，包括：</span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l3 level1 lfo5;tab-stops:list 36.0pt'><span
     class=inline-comment-marker><b><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman"'>日誌資訊</span></b></span><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>。</span></strong>&nbsp;<span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>您存取</span>
     Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
     "Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
     "Times New Roman"'>服務時，訓練期間產生的資訊將被自動記錄並儲存在我們的伺服器上。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l3 level1 lfo5;tab-stops:list 36.0pt'><span
     class=inline-comment-marker><b>Cookie</b></span><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>。</span></strong>&nbsp;<span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>我們透過使用「</span>Cookie<span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>」蒐集某些資訊，這是在您存取</span>
     Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
     "Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
     "Times New Roman"'>服務時瀏覽器儲存的小型文字檔案。我們可使用工作階段</span> Cookie <span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>和永久性</span>
     Cookie <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>來確定您已登入</span>
     Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
     "Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
     "Times New Roman"'>服務，並瞭解您與</span> Matrix On-Console <span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>服務互動的方式和時間。我們還可使用</span>
     Cookie <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>來監控</span>
     Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
     "Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
     "Times New Roman"'>服務上的總體使用情況和網路流量路由，以及定製並改進</span> Matrix On-Console <span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>服務。此外，</span>Cookie
     <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>允許我們在登入和登出</span>
     Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
     "Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
     "Times New Roman"'>服務時向您展示廣告。與永久性</span> Cookie <span style='font-family:
     "新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
     minor-fareast;mso-hansi-font-family:"Times New Roman"'>不同，在您登出</span>
     Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
     "Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
     "Times New Roman"'>服務並關閉瀏覽器時，工作階段</span> Cookie <span style='font-family:
     "新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
     minor-fareast;mso-hansi-font-family:"Times New Roman"'>會被刪除。某些第三方服務提供者可能還會將他們自己的</span>
     Cookie <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>儲存在您的瀏覽器上。注意，本隱私權政策僅涵蓋我們對</span>
     Cookie <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>的使用，並不涉及該等第三方對</span>
     Cookie <span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>的使用。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p style='margin-bottom:12.0pt'><strong>C. </strong><strong><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>其他來源的資訊</span></strong>
<br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>我們還從其他第三方來源獲取您的相關資訊。若我們將該等第三方提供的資訊與我們透過</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務蒐集的與您有關的資訊進行合併，我們會依據本隱私權政策所述的做法，以及該等資料來源規定的任何其他限制來處理該等合併資訊。該等第三方來源會隨時間發生變化，但包括：</span>
</p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo6;tab-stops:list 36.0pt'><span style='font-family:
     "新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
     minor-fareast;mso-hansi-font-family:"Times New Roman"'>與您的活動關聯的健身器材。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo6;tab-stops:list 36.0pt'><span style='font-family:
     "新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
     minor-fareast;mso-hansi-font-family:"Times New Roman"'>與您的使用者帳戶相連接的運動。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo6;tab-stops:list 36.0pt'><span style='font-family:
     "新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
     minor-fareast;mso-hansi-font-family:"Times New Roman"'>我們與之提供聯合品牌服務、銷售或經銷產品或參與聯合行銷活動的合作夥伴。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>如需有關第三方服務的更多資訊，請參閱下文標題為「第三方」的章節。</span>
<br>
<br>
<strong>2.</strong><strong><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>資訊使用</span></strong> <br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>本公司依據本隱私權政策規定的用途處理您的資訊。除了本隱私權政策其他地方規定的用途外，本公司還會為了我們的合法權益使用您的相關資訊，包括：</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>管理</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務，包括您的註冊和帳戶；</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>執行您請求的服務，例如回應您的評論、問題和請求，並提供客戶服務；</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>確保</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務在技術上運作正常；</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>向您傳送技術聲明、更新、安全提醒、與我們的政策變更有關的資訊以及支援和管理訊息；</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>保護本公司、第三方或我們使用者的權利和財產；</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>防止和處理詐騙、違反政策或條款的行為、威脅或傷害事件；</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>進行稽核、研究和分析，以便維護、保護和改進</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務、其他公司產品、行銷活動、服務和運動器材的使用；以及</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>開發新服務。</span></p>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>在徵得您同意後，我們還將您的資訊用於以下用途：</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>開發和展示定製的內容和廣告；</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>在</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務和第三方服務上投放您感興趣的廣告；以及</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l0 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span style='font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>滿足向您揭露之任何其他用途並徵得您的同意。</span></p>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>我們可能會使用不識別您身分的資訊（包括去身分識別化的資訊），而無需對您承擔任何義務，適用法律禁止的情況除外。就我們如何使用您的資訊，如需瞭解您的相關權利和選擇，請參閱下文標題為「您的權利和選擇」的章節。</span><br>
<br>
<strong><span style='color:#2F3438'>3.</span></strong><strong><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>資訊共用</span></strong><br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>本公司在如下所述情況下共用您的資訊：</span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
     color:#2F3438'>與俱樂部和教練共用。</span></strong><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>我們會與辦卡俱樂部（您運動所在的地方）和您指定的教練（陪伴您運動的人）共用您的資訊，包括您的姓名、電子郵件地址、</span><span
     style='color:#2F3438'>Matrix On-Console </span><span style='font-family:
     "新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
     minor-fareast;mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務帳戶</span><span
     style='color:#2F3438'> ID </span><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>和運動資料（例如運動時長和強度、運動時間、所用器材、已消耗卡路里、距離、平均速度）。如果我們作為俱樂部和教練客戶的資料處理者，為了方便處理您的請求和遵循適用法律，我們還會共用您的資訊。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
     color:#2F3438'>與關係企業共用。</span></strong><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>我們與我們的關聯實體（包括我們的母公司和姐妹公司）共用您的資訊。例如，我們可能與我們的關係企業共用您的資訊，以便提供客戶支援、進行行銷或技術操作。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>與第三方共用。</span></strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>為了加快處理您的請求，我們會與第三方共用您的資訊。<span
     style='color:#2F3438'>例如，若您選擇將</span></span><span style='color:#2F3438'>
     Matrix On-Console </span><span style='font-family:"新細明體",serif;mso-ascii-font-family:
     "Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
     "Times New Roman";color:#2F3438'>服務帳戶用於其他第三方和社交網站，並希望向這些第三方網站提供您的公共資訊，則您可以請求我們透過</span><span
     style='color:#2F3438'> Matrix On-Console </span><span style='font-family:
     "新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
     minor-fareast;mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務進行該等操作。</span><span
     style='color:#2F3438'>&nbsp;</span><span style='mso-fareast-font-family:
     "Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
     color:#2F3438'>用於合併或收購。</span></strong><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>若本公司涉足合併、收購或以任何形式出售其全部或部分資產，您的資訊會隨該等交易進行轉移，在這種情況下，我們會盡商業上的合理努力，確保對該等交易中涉及的任何資訊保密，並在您的個人資訊受到其他隱私權政策的約束時通知您。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
     color:#2F3438'>安全或強制性揭露。</span></strong><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>若我們有充分理由相信，在下述情況下，存取、使用、保留或揭露該等資訊是合理必要的，則我們會共用您的資訊：</span><span
     style='color:#2F3438'>(a) </span><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>符合任何適用法律、法規、法律程序或可強制執行的政府要求，</span><span
     style='color:#2F3438'>(b) </span><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>強制執行《服務條款》，包括對可能違反《服務條款》的情況開展調查，</span><span
     style='color:#2F3438'>(c) </span><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>偵測、防止或以其他方式解決詐騙、安全或技術問題，或</span><span
     style='color:#2F3438'> (d) </span><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>依據法律規定或在法律允許的範圍內，保護本公司、其使用者或大眾的權利、財產或安全。我們會共用您的資訊</span><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
     color:#141412'>以滿足政府機構的合法要求，包括滿足國家安全或執法要求。</span><span style='mso-fareast-font-family:
     "Times New Roman"'><o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l7 level1 lfo9;tab-stops:list 36.0pt'><strong><span
     style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
     mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
     color:#2F3438'>徵得同意。</span></strong><span style='font-family:"新細明體",serif;
     mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
     mso-hansi-font-family:"Times New Roman";color:#2F3438'>出於向您揭露的任何其他用途以及在徵得您同意的情況下，我們會共用您的資訊。</span><span
     style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></li>
</ul>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>在不限制前述規定的前提下，例如出於行業分析、人口統計和其他類似研究和行銷等用途，我們會與第三方共用無法識別您身分的某些合併資訊，例如運動時長和強度。如需瞭解與您的資訊管理或使用有關的任何其他問題，請透過下文標題為「聯絡我們」章節所列之方式與我們聯絡。</span>
<br>
<br>
<strong>4.</strong><strong><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>第三方</span></strong></p>

<p><strong>A. </strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>第三方服務</span></strong> <br>
<span style='color:#2F3438'>Matrix On-Console </span><span style='font-family:
"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務可能包含其他網站和服務的連結。您在第三方網站或服務（包括但不限於</span><span
style='color:#2F3438'> fitbit.com </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>或</span><span
style='color:#2F3438'> mapmyfitness.com</span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>）提供的或向該等網站或服務提供的任何資訊會直接提供給該等網站或服務的所有者，並受該方隱私權政策的約束。我們的隱私權政策不適用於該等網站或服務，且我們不對該等網站或服務的內容、隱私權或安全實踐及政策負責。為了保護您的資訊，我們建議您在存取其他網站和服務時仔細檢閱其隱私權政策。</span></p>

<p><span class=inline-comment-marker><b>Google Analytics</b></span><br>
<span class=inline-comment-marker>Google Analytics </span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>是</span> Google Inc.</span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>（地址為</span> 1600 Amphitheatre Parkway, Mountain View, CA
94043, USA</span><span class=inline-comment-marker><span style='font-family:
"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman"'>，簡稱「</span>Google</span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>」）提供的一項服務。本公司在</span> Matrix On-Console </span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務的各個平台上使用</span> Google Analytics </span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>功能「使用者</span> ID</span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>」，來蒐集與您在本地和全球範圍內使用我們服務有關的資訊，以改進和幫助我們分析</span>
Matrix On-Console </span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>服務。我們的網站也使用</span>
Google Analytics </span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>來監控流量和使用者體驗，但我們不會透過這種方式來蒐集可識別個人身分的資訊。在</span>
Matrix On-Console </span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>服務的註冊和使用過程中，我們會蒐集與儀表功能和應用程式使用情況有關的資料。這些資料包含：運動時所用的應用程式、訓練時長、搜尋歷史和運動統計資料以及與應用程式使用情況有關的綜合資料。<span
style='color:#091E42'>標識關鍵字可讓人們識別應用程式上的使用者個人，但該關鍵字經過匿名化處理，無法與任何使用者的個人可識別資訊</span>關聯。我們不會儲存或處理使用者的登入資訊。所產生的對應用程式使用情況的分析有助於我們改進服務、開發新功能，以及最佳化我們對使用者需求的注重。</span>&nbsp;
</span><span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'></p>

<h3><strong>5.</strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>您的權利和選擇</span></strong><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<h3><strong>A. </strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>存取和刪除帳戶資訊</span></strong><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>您在使用</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務時，我們會作出合理真誠努力，讓您存取您透過帳戶向我們提交的資訊，或者若該等資訊不準確，則對其進行更正，或者若該等資訊並非法律要求保留的資訊或並非出於合法商業用途而保留的資訊，則會根據您的請求刪除資訊。若您想更正或刪除您的資訊，請按照下文標題為「聯絡我們」的章節所示方式，向我們傳送電子郵件，隨時告知我們。我們會要求您提供其他資訊，以便我們確定您的身分。請注意，為了遵守我們的法律義務、解決糾紛以及執行我們的協議，我們會在必要時保留並使用您的資訊。加州居民和歐洲的資料主體擁有下文標題為「您的加州隱私權」和「您的歐洲隱私權」章節規定的其他權利。</span>&nbsp;<br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>您可以透過帳戶設定或按照下文標題為「聯絡我們」章節所示方式向我們傳送電子郵件，永久刪除您的帳戶。我們會儘快採取措施刪除您的資訊，但為了備案或在法律另有規定的情況下，我們會對某些資訊進行封存</span>/<span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>備份。請注意，我們只能在</span>
Matrix On-Console <span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務中刪除您的資訊。<span style='color:#091E42'>本公司無法刪除您與我們的第三方夥伴及網站共用的資訊。因此，您取消訂閱之後，此資訊仍會保留在該等第三方網站和服務上。然而，在收到刪除請求後，對於之前從本公司接收您的資料進行處理的第三方夥伴，本公司會向其傳送刪除請求。</span></span><br>
<br>
<strong><span style='color:#2F3438'>B. </span></strong><strong><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>您的加州隱私權</span></strong> <br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>加州的《反客戶資訊揭露法》允許加州客戶索取某些詳細資訊，以瞭解如何與第三方和（在某些情況下）關係企業共用某些類型的資訊，以用於該等第三方和關係企業的直接行銷用途。依據法律規定，企業應根據加州客戶的請求向其提供某些資訊，或允許加州客戶選擇接受或拒絕此類共用。</span>
<br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>若您是加州居民，且希望獲得與我們遵守該法律有關的資訊，請按照下文標題為「聯絡我們」章節所示方式與我們聯絡。請求必須在描述部分第一行中包含「加州隱私權請求」，並包含您的姓名、街道地址、城市、州和郵遞區號。請注意，對於透過指定電子郵件地址或郵寄地址以外的方式提出的請求，本公司無需作出答覆。</span>
<br>
<br>
<strong><span style='color:#2F3438'>C. </span></strong><strong><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>您的歐洲隱私權</span></strong> <br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>若您是歐洲的資料主體，您有權存取、更正或刪除我們透過</span><span style='color:#2F3438'>
Matrix On-Console </span><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman";color:#2F3438'>服務蒐集的與您有關的任何個人資料。您還擁有資料可攜權、被遺忘權以及限制或反對我們處理透過</span><span
style='color:#2F3438'> Matrix On-Console </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務蒐集的您個人資料的權利。此外，您有權要求我們不得出於行銷用途或與最初的個人資料蒐集或您之後授權的個人資料蒐集完全不同的用途來處理您的個人資料（或將其提供給第三方進行處理）。對於我們在徵得您同意後進行的任何資料處理，您可隨時撤銷該同意。</span><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>只需按照下文標題為「聯絡我們」章節所示方式與我們聯絡。</span><br>
<span style='font-size:13.5pt;font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>若對我們的法規遵循行為存在任何疑問，您有權向歐洲監管部門投訴。</span><span
style='font-size:13.5pt'>&nbsp;</span></p>

<p><strong>6.</strong><strong><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>資訊安全</span></strong><br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>我們採取合理適當的安全措施，以防我們從您那裡蒐集的資訊或您的相關資訊遭到未經授權的存取、使用、篡改、揭露或損毀。在使用者明確請求刪除其儲存在我們伺服器上的帳戶資料之前，或在處理該等資料的法律依據失效之前，我們會保留該等資料。但請注意，任何透過網路傳輸資訊或儲存資訊的方式都不是絕對安全的。因此，我們無法保證任何資訊的絕對安全。</span></p>

<h3><strong>7.</strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>跨境傳輸</span></strong><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>我們公司總部位於美國，我們蒐集的資訊受美國法律的管轄。您的資訊可能會被傳輸至位於您所在州、省、國家或其他政府管轄範圍以外的電腦並在其上進行維護，這些地區的隱私權保護法可能與您所在司法轄區的隱私權保護法在保護程度上有所不同。若您位於美國以外地區，並選擇向我們提供資訊，我們會將該等資訊傳輸至美國和其他司法管轄區並在那裡處理。您使用</span><span
style='color:#2F3438'> Matrix On-Console </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務或提供任何資訊，即表示您同意該等傳輸和處理。</span><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>若您的資料在歐洲蒐集，我們會根據適當的或合適的保障措施（如標準合約條款）對您的個人資料進行傳輸。</span>&nbsp;</p>

<p><br>
<strong>8.</strong><strong><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>我們針對兒童的政策</span></strong><br>
<span style='color:#2F3438'>Matrix On-Console </span><span style='font-family:
"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman";color:#2F3438'>服務不會傳送給</span><span
style='color:#2F3438'> 13 </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>歲以下的人士，且我們不會蓄意向</span><span
style='color:#2F3438'> 13 </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>歲以下兒童蒐集《美國兒童線上隱私保護法》（「</span><span
style='color:#2F3438'>COPPA</span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>」）定義的個人資訊。若我們得知我們蒐集了</span><span
style='color:#2F3438'> 13 </span><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman";color:#2F3438'>歲以下兒童的個人資訊，我們會採取措施，儘快從我們的檔案中刪除該等資訊。</span></p>

<h3><strong>9.</strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>本隱私權政策的變更</span></strong><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>請注意，我們會不時變更本隱私權政策，您應定期檢閱。任何變更將在隱私權政策修訂版公佈後即刻生效。您繼續使用我們的服務，即表示您同意當時公佈的隱私權政策。若變更內容非常重要，我們可能會另行傳送電子郵件通知您。</span></p>

<h3><strong>10.</strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>資料控制者</span></strong><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>歐盟資料保護法對為其自身用途處理個人資料的組織（稱為「控制者」）與代表其他組織處理個人資料的組織（稱為「處理者」）進行了區分。根據情況，本公司既可作為您個人資料的控制者，也可作為處理者。本公司（地址如下文標題為「聯絡我們」的章節所示）是您透過</span>
Matrix On-Console </span><span class=inline-comment-marker><span
style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>服務輸入的資訊的控制者。此資訊包括單一登入憑證、</span>Asset
Management </span><span class=inline-comment-marker><span style='font-family:
"新細明體",serif;mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:"Times New Roman"'>使用者資訊和其他服務。</span> </span></p>

<p><span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>本公司（地址如下文標題為「聯絡我們」章節所示）是透過健身器材產生之任何運動資訊的處理者。</span>&nbsp;
</span><span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>購買</span> Matrix On-Console </span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>服務供其終端使用者體驗的客戶為資料控制者。</span>&nbsp; </span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-ascii-font-family:
"Times New Roman";mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
"Times New Roman"'>在此情況下，本公司為資料處理者，代表客戶的意願對使用者資料進行處理。</span></span></p>

<p><span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>有時，本公司代表作為獨立法律實體的客戶（控制者）進行資料處理。例如，本公司為俱樂部和教練提供技術，以便他們向您提供他們自有的應用程式以及進行健身追蹤。</span></span></p>

<p><span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>在此類情況下，本公司代表其客戶並根據其指示處理個人資料。請檢閱相關客戶的隱私權政策，瞭解他們的隱私權實踐。若對該等個人資料以及您在資料保護法項下的權利存在任何疑問，應聯絡作為資料控制者的客戶，而不是本公司。</span></span></p>

<h3><strong>11.</strong><strong><span style='font-family:"新細明體",serif;
mso-ascii-font-family:"Times New Roman";mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:"Times New Roman"'>聯絡我們</span></strong><span
style='mso-fareast-font-family:"Times New Roman"'><o:p></o:p></span></h3>

<p><span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman";
color:#2F3438'>若對本隱私權政策或本公司的資料實踐存在任何疑問或疑慮，請隨時透過以下方式聯絡我們：</span> <br>
<br>
<span class=inline-comment-marker>privacy@matrixfitness.com</span></p>

<p><br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>喬山健康科技公司</span><br>
<span style='font-family:"新細明體",serif;mso-ascii-font-family:"Times New Roman";
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:"Times New Roman"'>隱私權團隊</span><br>
1600 Landmark Dr<br>
Cottage Grove, WI 53527</p>

</div>

</body>

</html>`;
export default html_zh;