const html_en = `<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=unicode">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">
<link rel=File-List
href="Johnson%20Health%20Tech%20Privacy%20Policy.fld/filelist.xml">
<title>Johnson Health Tech Privacy Policy - 10-17-2019</title>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Microsoft Office User</o:Author>
  <o:LastAuthor>Microsoft Office User</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>2</o:TotalTime>
  <o:Created>2020-06-03T02:55:00Z</o:Created>
  <o:LastSaved>2020-06-03T02:55:00Z</o:LastSaved>
  <o:Pages>8</o:Pages>
  <o:Words>3237</o:Words>
  <o:Characters>18452</o:Characters>
  <o:Lines>153</o:Lines>
  <o:Paragraphs>43</o:Paragraphs>
  <o:CharactersWithSpaces>21646</o:CharactersWithSpaces>
  <o:Version>16.00</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:AllowPNG/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<link rel=themeData
href="Johnson%20Health%20Tech%20Privacy%20Policy.fld/themedata.thmx">
<link rel=colorSchemeMapping
href="Johnson%20Health%20Tech%20Privacy%20Policy.fld/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SplitPgBreakAndParaMark/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
  DefSemiHidden="false" DefQFormat="false" DefPriority="99"
  LatentStyleCount="376">
  <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 9"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="header"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footer"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index heading"/>
  <w:LsdException Locked="false" Priority="35" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of figures"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope return"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="line number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="page number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of authorities"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="macro"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="toa heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 5"/>
  <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Closing"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Signature"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="true"
   UnhideWhenUsed="true" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Message Header"/>
  <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Salutation"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Date"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Block Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="FollowedHyperlink"/>
  <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Document Map"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Plain Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="E-mail Signature"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Top of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Bottom of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal (Web)"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Acronym"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Cite"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Code"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Definition"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Keyboard"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Preformatted"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Sample"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Typewriter"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Variable"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Table"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation subject"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="No List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Contemporary"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Elegant"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Professional"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Balloon Text"/>
  <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Theme"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" QFormat="true"
   Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" QFormat="true"
   Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" QFormat="true"
   Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" QFormat="true"
   Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" QFormat="true"
   Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" QFormat="true"
   Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" SemiHidden="true"
   UnhideWhenUsed="true" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Mention"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Smart Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hashtag"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Unresolved Mention"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Smart Link"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
@media print {
    #main {
        padding-bottom: 1em !important;
    }

    body {
        font-family: Arial, Helvetica, FreeSans, sans-serif;
        font-size: 10pt;
        line-height: 1.2;
    }

    body, #full-height-container, #main, #page, #content, .has-personal-sidebar #content {
        background: #fff !important;
        color: #000 !important;
        border: 0 !important;
        width: 100% !important;
        height: auto !important;
        min-height: auto !important;
        margin: 0 !important;
        padding: 0 !important;
        display: block !important;
    }

    a, a:link, a:visited, a:focus, a:hover, a:active {
        color: #000;
    }

    #content h1,
    #content h2,
    #content h3,
    #content h4,
    #content h5,
    #content h6 {
        font-family: Arial, Helvetica, FreeSans, sans-serif;
        page-break-after: avoid;
    }

    pre {
        font-family: Monaco, "Courier New", monospace;
    }

    #header,
    .aui-header-inner,
    #navigation,
    #sidebar,
    .sidebar,
    #personal-info-sidebar,
    .ia-fixed-sidebar,
    .page-actions,
    .navmenu,
    .ajs-menu-bar,
    .noprint,
    .inline-control-link,
    .inline-control-link a,
    a.show-labels-editor,
    .global-comment-actions,
    .comment-actions,
    .quick-comment-container,
    #addcomment {
        display: none !important;
    }
    #splitter-content {
        position: relative !important;
    }

    .comment .date::before {
        content: none !important;
    }

    h1.pagetitle img {
        height: auto;
        width: auto;
    }

    .print-only {
        display: block;
    }

    #footer {
        position: relative !important;
        margin: 0;
        padding: 0;
        background: none;
        clear: both;
    }

    #poweredby {
        border-top: none;
        background: none;
    }

    #poweredby li.print-only {
        display: list-item;
        font-style: italic;
    }

    #poweredby li.noprint {
        display: none;
    }
    .wiki-content .table-wrap,
    .wiki-content p,
    .panel .codeContent,
    .panel .codeContent pre,
    .image-wrap {
        overflow: visible !important;
    }
    #children-section,
    #comments-section .comment,
    #comments-section .comment .comment-body,
    #comments-section .comment .comment-content,
    #comments-section .comment p {
        page-break-inside: avoid;
    }

    #page-children a {
        text-decoration: none;
    }
    #comments-section.pageSection .section-header,
    #comments-section.pageSection .section-title,
    #children-section.pageSection .section-header,
    #children-section.pageSection .section-title,
    .children-show-hide {
        padding-left: 0;
        margin-left: 0;
    }

    .children-show-hide.icon {
        display: none;
    }
    .has-personal-sidebar #content {
        margin-right: 0px;
    }

    .has-personal-sidebar #content .pageSection {
        margin-right: 0px;
    }

    .no-print, .no-print * {
        display: none !important;
    }
}
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;
	mso-font-charset:2;
	mso-generic-font-family:auto;
	mso-font-pitch:variable;
	mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
	{font-family:新細明體;
	panose-1:2 2 5 0 0 0 0 0 0 0;
	mso-font-alt:PMingLiU;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:"\@新細明體";
	panose-1:2 1 6 1 0 1 1 1 1 1;
	mso-font-charset:136;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0cm;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體",serif;
	mso-bidi-font-family:新細明體;}
h1
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"標題 1 字元";
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:1;
	font-size:24.0pt;
	font-family:"新細明體",serif;
	mso-bidi-font-family:新細明體;
	font-weight:bold;}
h3
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"標題 3 字元";
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	mso-outline-level:3;
	font-size:13.5pt;
	font-family:"新細明體",serif;
	mso-bidi-font-family:新細明體;
	font-weight:bold;}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:purple;
	text-decoration:underline;
	text-underline:single;}
p
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體",serif;
	mso-bidi-font-family:新細明體;}
p.msonormal0, li.msonormal0, div.msonormal0
	{mso-style-name:msonormal;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0cm;
	mso-margin-bottom-alt:auto;
	margin-left:0cm;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"新細明體",serif;
	mso-bidi-font-family:新細明體;}
span.1
	{mso-style-name:"標題 1 字元";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"標題 1";
	mso-ansi-font-size:26.0pt;
	mso-bidi-font-size:26.0pt;
	font-family:"Calibri Light",sans-serif;
	mso-ascii-font-family:"Calibri Light";
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:"Calibri Light";
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	mso-font-kerning:26.0pt;
	font-weight:bold;}
span.3
	{mso-style-name:"標題 3 字元";
	mso-style-noshow:yes;
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"標題 3";
	mso-ansi-font-size:18.0pt;
	mso-bidi-font-size:18.0pt;
	font-family:"Calibri Light",sans-serif;
	mso-ascii-font-family:"Calibri Light";
	mso-ascii-theme-font:major-latin;
	mso-fareast-font-family:新細明體;
	mso-fareast-theme-font:major-fareast;
	mso-hansi-font-family:"Calibri Light";
	mso-hansi-theme-font:major-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:major-bidi;
	font-weight:bold;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;
	mso-ascii-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-font-kerning:0pt;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;
	mso-header-margin:36.0pt;
	mso-footer-margin:36.0pt;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 @list l0
	{mso-list-id:681279484;
	mso-list-template-ids:-1798818524;}
@list l0:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l0:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l0:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l0:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1
	{mso-list-id:685327342;
	mso-list-template-ids:1640153844;}
@list l1:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l1:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l1:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l1:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2
	{mso-list-id:1040278588;
	mso-list-template-ids:662203790;}
@list l2:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l2:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l2:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l2:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3
	{mso-list-id:1043096654;
	mso-list-template-ids:-1774537378;}
@list l3:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l3:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l3:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l3:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4
	{mso-list-id:1054157934;
	mso-list-template-ids:-2030010216;}
@list l4:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l4:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l4:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l4:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5
	{mso-list-id:1358778044;
	mso-list-template-ids:-1507275418;}
@list l5:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l5:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l5:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l5:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6
	{mso-list-id:1552963806;
	mso-list-template-ids:548193370;}
@list l6:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l6:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l6:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l6:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7
	{mso-list-id:1758400166;
	mso-list-template-ids:1442890920;}
@list l7:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l7:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l7:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l7:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8
	{mso-list-id:1841460481;
	mso-list-template-ids:359402882;}
@list l8:level1
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:36.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Symbol;}
@list l8:level2
	{mso-level-number-format:bullet;
	mso-level-text:o;
	mso-level-tab-stop:72.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:"Courier New";
	mso-bidi-font-family:"Times New Roman";}
@list l8:level3
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:108.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level4
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:144.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level5
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:180.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level6
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:216.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level7
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:252.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level8
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:288.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
@list l8:level9
	{mso-level-number-format:bullet;
	mso-level-text:;
	mso-level-tab-stop:324.0pt;
	mso-level-number-position:left;
	text-indent:-18.0pt;
	mso-ansi-font-size:10.0pt;
	font-family:Wingdings;}
ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>
<!--[if gte mso 10]>
<style>
 table.MsoNormalTable
	{mso-style-name:表格內文;
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin:0cm;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman",serif;}
</style>
<![endif]-->
</head>

<body lang=ZH-TW link=blue vlink=purple style='tab-interval:24.0pt'>

<div class=WordSection1>

<h1><span lang=EN-US>Johnson Health Tech Privacy Policy - </span><span
lang=EN-US style='font-size:12.0pt'>10-17-2019</span></h1>

<p><strong><span lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:
新細明體'>Privacy Policy</span></strong><span lang=EN-US> <br>
<span style='color:#2F3438'>Last Updated:</span>&nbsp;10/18/2019 <br>
<span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-bidi-font-family:新細明體;color:#2F3438'><span data-ref=e87c7b06-f5ca-4b2d-afbb-f916e7d75f81>This
Privacy Policy applies to all of the products and services offered by Johnson
Health Tech Co., Ltd. and its affiliates, subsidiaries, divisions, and brands.
(collectively, &quot;Matrix,&quot; &quot;Johnson,&quot; &quot;Johnson
Fitness,&quot; the &quot;Company,&quot; &quot;we,&quot; &quot;our,&quot; and
&quot;us&quot;) via its website located at</span></span><span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-bidi-font-family:
新細明體'>&nbsp;www.matrixfitness.com</span><span style='color:#2F3438'><span
data-ref=e87c7b06-f5ca-4b2d-afbb-f916e7d75f81>, its applications on its
exercise products' consoles, digital products, its mobile applications, and the
websites and mobile applications that the Company powers for our third-party
business partners</span></span><span data-ref=e87c7b06-f5ca-4b2d-afbb-f916e7d75f81>
<span style='color:#2F3438'></span><span data-ref=e87c7b06-f5ca-4b2d-afbb-f916e7d75f81>where
this Privacy Policy is presented or made available to you (&quot;Matrix
On-Console Services&quot;). Please read this Privacy Policy carefully before
you use the Matrix On-Console Services.</span></span></span></span></span></p>

<p><span lang=EN-US style='color:#2F3438'>If you have any questions about our
privacy practices, please contact us as set forth in the section below,
entitled &quot;Contact Us.&quot;</span><span lang=EN-US><o:p></o:p></span></p>

<h1 id=JohnsonHealthTechPrivacyPolicy-10-17-2019-1.InformationWeCollect><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>1.
Information We Collect</span></strong><span lang=EN-US><o:p></o:p></span></h1>

<h3
id="JohnsonHealthTechPrivacyPolicy-10-17-2019-OurprimarygoalsincollectinginformationaretoprovideandimprovetheMatrixOn-ConsoleServices,toadministeryouruseoftheMatrixOn-ConsoleServices(includingyouraccount,ifyouareanaccountholder),andtoenableyoutoenjoyandeasily"><span
lang=EN-US style='color:#2F3438'>Our primary goals in collecting information
are to provide and improve the Matrix On-Console Services, to administer your
use of the Matrix On-Console Services (including your account, if you are an
account holder), and to enable you to enjoy and easily navigate the Matrix
On-Console Services. We also collect information for other purposes as
described in this Privacy Policy. Some examples of the types of information we
collect and where we collect that information include the following:</span><span
lang=EN-US> <br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>A.
Information You Provide</span></strong></span></h3>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l7 level1 lfo1;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>Account
Creation.</span></strong><span lang=EN-US> <span style='color:#2F3438'>You may
use some of the Matrix On-Console Services without registration.</span> When
you sign up for an on-console account or other Matrix On-Console Service that
requires registration, we ask you for information, which includes personally
identifiable information about you (information that can be used to identify
you, such as your first and last name and email address). As part of the
registration process, we also collect passwords, password hints, and other
information for authentication and account access. If you choose to create an
account through one of your accounts with certain third-party services such as
Facebook or Twitter (each, a Social Networking Service, or &quot;<span
class=inline-comment-marker><span style='font-family:"新細明體",serif;mso-bidi-font-family:
新細明體'><span data-ref=42ceabe4-7b3d-4920-b723-635511c4d1d7>SNS Account</span></span></span>&quot;),
you may have to provide us with your username (or user ID) so that your
identity can be authenticated by the SNS Account. When the authentication is
complete, we'll be able to link your account with the SNS Account and access
certain information, such as your name and email address, and other information
that your privacy settings on the SNS Account permit us to access.&nbsp;</span></p>

<p><span lang=EN-US><o:p>&nbsp;</o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l8 level1 lfo2;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>Profile
     Information.</span></strong><span lang=EN-US> You have the option to
     provide additional information as part of your profile, some of which is
     personally identifying information, phone number, or a picture for your
     profile and a short description about yourself, but these items are not
     required.&nbsp;</span></li>
</ul>

<p><span lang=EN-US><o:p>&nbsp;</o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l2 level1 lfo3;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>Your Fitness
     Activity.</span></strong><span lang=EN-US> <span style='color:#333333'>We
     may collect information about your fitness activity and physical characteristics
     such as your home club, fitness facility membership ID, gender, date of
     birth, height, weight, the name of your trainer, workout schedule,</span> <span
     style='color:#333333'>your workout data (such as the length and intensity
     of a workout, time of workout, equipment used, calories burned, distance,
     average speed). This collected information my be added manually by you, or
     it may be received from the other 3rd party services connected to your
     account. We may also store information regarding your fitness level,
     measurements, BMI, and other information that you provide to your trainer
     on behalf of your fitness facility.</span></span></li>
</ul>

<p><span lang=EN-US><o:p>&nbsp;</o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l3 level1 lfo4;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>Communications.</span></strong><span
     lang=EN-US> We collect the content of messages you send to us or your
     trainer, such as feedback, questions, and information you provide to
     customer support.</span></li>
</ul>

<p><span lang=EN-US>You may choose to voluntarily submit other information to
us through the Matrix On-Console Services that we do not request, and, in such
instances, you are solely responsible for such information.</span></p>

<p><span lang=EN-US><br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>B.
Information Collected Automatically</span></strong> <br>
<span class=inline-comment-marker><span data-ref=1877aed0-5d88-4c3c-b943-666776a28c47><span
style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;color:#2F3438'>We
automatically collect information about your device and how your device
interacts with</span></span></span><span style='color:#2F3438'>&nbsp;Matrix
On-Console Services.</span> <span style='color:#2F3438'>Some examples of
information we collect include service use data (e.g., the features you use
with Matrix On-Console Services and in digital products, the pages you visit on
the console, workouts performed and tied to your user ID, the time of day you
browse, and your referring and exiting pages), device connectivity and
configuration data (e.g., the type of device or browser you use, your device's
operating software, your Internet service provider, your device's regional and
language settings, and your device's identifiers such as IP address, MAC
address, and device advertising ID) and location data (e.g., latitude/longitude
data or location derived from an IP address or data that indicates a city or
postal code level).</span> <br>
<span style='color:#2F3438'>We use various current – and later – developed
technologies to collect this information, including the following:</span><o:p></o:p></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo5;tab-stops:list 36.0pt'><span
     class=inline-comment-marker><b><span lang=EN-US style='font-family:"新細明體",serif;
     mso-bidi-font-family:新細明體'><span data-ref=28098d83-2072-4e11-a77a-2bbe21f74db7>Log
     Information</span></span></b></span><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>.&nbsp;</span></strong><span
     lang=EN-US>&nbsp;When you access the Matrix On-Console Services,
     information generated during your session will be automatically recorded
     and stored on our servers.<o:p></o:p></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l1 level1 lfo5;tab-stops:list 36.0pt'><span
     class=inline-comment-marker><b><span lang=EN-US style='font-family:"新細明體",serif;
     mso-bidi-font-family:新細明體'><span data-ref=3059865e-82c8-4d88-87d6-c5cb07ba4bc0>Cookies</span></span></b></span><strong><span
     lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>.&nbsp;</span></strong><span
     lang=EN-US>&nbsp;We collect certain information through the use of
     &quot;cookies,&quot; which are small text files that are saved by your
     browser when you access the Matrix On-Console Services. We may use both
     session cookies and persistent cookies to identify that you've logged in
     to the Matrix On-Console Services and to tell us how and when you interact
     with the Matrix On-Console Services. We may also use cookies to monitor
     aggregate usage and web traffic routing on the Matrix On-Console Services
     and to customize and improve the Matrix On-Console Services. Additionally,
     cookies allow us to bring you advertising both on and off the Matrix
     On-Console Services. Unlike persistent cookies, session cookies are
     deleted when you log off from the Matrix On-Console Services and close
     your browser. Some third-party service providers may also place their own
     cookies on your browser. Note that this Privacy Policy covers only our use
     of cookies and does not include use of cookies by such third parties.</span></li>
</ul>

<p style='margin-bottom:12.0pt'><strong><span lang=EN-US style='font-family:
"新細明體",serif;mso-bidi-font-family:新細明體'>C. Information <span class=GramE>From</span>
Other Sources</span></strong><span lang=EN-US> <br>
We also obtain information about you from other third party sources. To the
extent we combine such <span class=GramE>third party</span> sourced information
with information we have collected about you through the Matrix On-Console
Services, we will treat the combined information in accordance with the
practices described in this Privacy Policy, plus any additional restrictions
imposed by the source of the data. These <span class=GramE>third party</span>
sources vary over time, but have included: </span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l5 level1 lfo6;tab-stops:list 36.0pt'><span lang=EN-US>Fitness
     equipment tied to your activities.</span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l5 level1 lfo6;tab-stops:list 36.0pt'><span lang=EN-US>Workouts
     connected with your user account.</span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l5 level1 lfo6;tab-stops:list 36.0pt'><span lang=EN-US>Partners
     with which we offer co-branded services, sell or distribute our products,
     or engage in joint marketing activities.</span></li>
</ul>

<p><span lang=EN-US><br>
For further information on third party services, see the section entitled
&quot;Third Parties&quot; below. <br>
<br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>2.
Information Use</span></strong> <br>
The Company processes your information for the purposes described in this
Privacy Policy. In addition to the purposes stated elsewhere in this Privacy
Policy, the Company uses information about you for our legitimate interests,
including to:</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Manage the Matrix On-Console Services, including your registration
and account;</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Perform services requested by you, such as to respond to your
comments, questions, and requests, and provide customer service;</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Ensure the technical functioning of the Matrix On-Console Services;</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Send you technical notices, updates, security alerts, information
regarding changes to our policies, and support and administrative messages;</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Protect the rights and property of the Company, third parties, or
our users;</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Prevent and address fraud, breaches of policies or terms, and
threats or harm;</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Audit, research and analyze in order to maintain, protect and
improve the Matrix On-Console Services, other Company products, marketing
efforts, and services, and the use of exercise equipment; and</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l4 level1 lfo7;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Develop new services.</span></p>

<p><span lang=EN-US>We also use information about you with your consent,
including to:</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Develop and display customized content and advertising;</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Serve advertising tailored to your interests on the Matrix
On-Console Services and <span class=GramE>third party</span> services; and</span></p>

<p style='margin-left:36.0pt;text-indent:-18.0pt;mso-list:l6 level1 lfo8;
tab-stops:list 36.0pt'><![if !supportLists]><span lang=EN-US style='font-size:
10.0pt;mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:
Symbol;mso-bidi-font-family:Symbol'><span style='mso-list:Ignore'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><![endif]><span
lang=EN-US>Fulfill any other purpose disclosed to you and with your consent.</span></p>

<p><span lang=EN-US>We may use information that does not identify you
(including information that has been de-identified) without obligation to you
except as prohibited by applicable law. For information on your rights and
choices regarding how we use your information, please see the section entitled
&quot;Your Rights and Choices&quot; below.<br>
<br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;
color:#2F3438'>3. Information Sharing</span></strong><br>
<span style='color:#2F3438'>The Company shares your information as follows:</span></span></p>

<ul type=disc>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;color:#2F3438'>Clubs
     and Trainers.</span></strong><span lang=EN-US style='color:#2F3438'> We
     share your information, including your name, email address, Matrix
     On-Console Services account ID and workout data (such as the length and
     intensity of a workout, time of workout, equipment used, calories burned,
     distance, average speed), with the home club (where you work out) and
     trainers (who you work out with) you have designated. Where we act as a
     processor for club and trainer customers, we also share your information
     to facilitate your requests and otherwise comply with applicable law.</span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;color:#2F3438'>Affiliates.</span></strong><span
     lang=EN-US> <span style='color:#2F3438'>We share your information with our
     related entities including our parent and sister companies. For example,
     we may share your information with our affiliates for customer support,
     marketing, and technical operations.</span></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>Third Parties.</span></strong><span
     lang=EN-US> We share your information with third parties for purposes of
     facilitating your requests. <span style='color:#2F3438'>For example, if
     you choose to connect your account in the Matrix On-Console Services to
     other third-party and social networking sites and want to make your public
     information available on those third-party sites, you may request that we
     do so via the Matrix On-Console Services.&nbsp;</span></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;color:#2F3438'>Merger
     or Acquisition.</span></strong><span lang=EN-US style='color:#2F3438'> If
     the Company becomes involved in a merger, acquisition, or any form of sale
     of some or all of its assets, your information may be transferred in
     connection with such transaction, in which case we will use commercially
     reasonable efforts to ensure the confidentiality of any information
     involved in such transactions and provide notice when your personal
     information becomes subject to a different privacy policy.</span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;color:#2F3438'>Security
     or Compelled Disclosure.</span></strong><span lang=EN-US style='color:
     #2F3438'> We share your information if we have a good faith belief that
     access, use, preservation or disclosure of such information is reasonably
     necessary to (a) satisfy any applicable law, regulation, legal process or
     enforceable governmental request, (b) enforce the Terms of Service,
     including investigation of potential violations thereof, (c) detect,
     prevent, or otherwise address fraud, security or technical issues, or (d)
     protect against harm to the rights, property or safety of the Company, its
     users or the public as required or permitted by law. We share your
     information</span><span lang=EN-US> <span style='color:#141412'>in
     response to lawful requests by public authorities, including to meet national
     security or law enforcement requirements.</span></span></li>
 <li class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
     mso-list:l0 level1 lfo9;tab-stops:list 36.0pt'><strong><span lang=EN-US
     style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;color:#2F3438'>Consent.</span></strong><span
     lang=EN-US style='color:#2F3438'> We share your information for any other
     purpose disclosed to you and where we have your consent.</span></li>
</ul>

<p><span lang=EN-US style='color:#2F3438'>Without limiting the foregoing, we
may share with third <span class=GramE>parties</span> certain pieces of
aggregated information that does not identify you, such as the duration and
intensity of a workout, for example, for industry analysis, demographic
profiling and other similar research and marketing purposes. Please contact us
as provided in the section below, entitled &quot;Contact Us,&quot; for any
additional questions about the management or use of your information.</span><span
lang=EN-US> <br>
<br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>4.
Third Parties</span></strong></span></p>

<p><strong><span lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:
新細明體'>A. Third Party Services</span></strong><span lang=EN-US> <br>
<span style='color:#2F3438'>The Matrix On-Console Services may contain links to
other websites and services. Any information that you provide on or to a
third-party website or service, including but not limited to fitbit.com or
mapmyfitness.com, is provided directly to the owner of the website or service and
is subject to that party's privacy policy. Our Privacy Policy does not apply to
such websites or services and we're not responsible for the content, privacy or
security practices and policies of those websites or services. To protect your <span
class=GramE>information</span> we recommend that you carefully review the
privacy policies of other websites and services that you access.</span><o:p></o:p></span></p>

<p><span class=inline-comment-marker><b><span lang=EN-US style='font-family:
"新細明體",serif;mso-bidi-font-family:新細明體'><span data-ref=acdcb6d9-db61-4f0c-8546-675b661a7e73>Google
Analytics</span></span></b></span><span lang=EN-US><br>
<span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-bidi-font-family:新細明體'><span data-ref=acdcb6d9-db61-4f0c-8546-675b661a7e73>Google
Analytics is a service provided by Google Inc., 1600 Amphitheatre Parkway,
Mountain View, CA 94043, USA (&quot;Google&quot;). The Company uses the Google
Analytics functionality &quot;User ID&quot; via all platforms on the Matrix
On-Console Services to gather information on the usage of our services by you,
both locally and globally to improve and help us analyze the Matrix On-Console
Services. Google Analytics is also used on our websites to monitor traffic and
user experiences, but no personally identifiable information is <span
class=GramE>collect</span> in this manner.&nbsp;</span><span data-ref=acdcb6d9-db61-4f0c-8546-675b661a7e73>Data
on the usage of the on-console function and applications is gathered with the
registration and usage of the Matrix On-Console Services. This contains the
following: Applications used during workout, duration of session, search
history, and workout statistics, as well as aggregated data on the usage of the
application. <span style='color:#091E42'>An identification key allows individual
users of the application to be identified, however this key is anonymized and
cannot be tied to any user's personally identifiable information</span>. The
user's login information is not stored or processed. The resulting analysis on
the usage of the application helps us to improve our service, to develop new
features and to optimize our focus on the needs of our users.</p>

<h3 id=JohnsonHealthTechPrivacyPolicy-10-17-2019-5.YourRightsandChoices><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>5. Your
Rights and Choices</span></strong><span lang=EN-US><o:p></o:p></span></h3>

<h3
id=JohnsonHealthTechPrivacyPolicy-10-17-2019-A.AccessandDeletionofAccountInformation><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>A. Access
and Deletion of Account Information</span></strong></h3>

<p><span lang=EN-US>When you use the Matrix On-Console Services, we make
reasonable good faith efforts to provide you with access to information you
have submitted to us through your account and either to correct this
information if it is inaccurate or to delete information at your request if it
is not otherwise required to be retained by law or for legitimate business
purposes. Please feel free to let us know if you would like your information
corrected or removed by emailing us as set forth in the section below, entitled
&quot;Contact Us&quot;. We may require additional information from you to allow
us to confirm your identity. Please note that we will retain and use your
information as necessary to comply with our legal obligations, resolve
disputes, and enforce our agreements. California residents and data subjects in
Europe have additional rights as set forth in the sections below, entitled
&quot;Your California Privacy Rights&quot; and &quot;Your European Privacy
Rights&quot;.&nbsp;<br>
You can permanently delete your account through your account settings or by
sending us an email to us as set forth in the section entitled &quot;Contact
Us&quot; below. We'll take steps to delete your information as soon as is
practicable, but some information may remain in archived/backup copies for our
records or as otherwise required by law. Please note that we can only delete
your information within the Matrix On-Console Services. <span style='color:
#091E42'>Information that you have shared with our 3rd party partners and sites
cannot be deleted by the Company. <span class=GramE>Therefore</span> this
information will remain on those 3rd party sites and services even after you
unsubscribe from us. However, upon receiving a deletion request, the Company
will send a deletion request to those <span class=GramE>third party</span>
partners that the Company has previously sent your data to for
processing.&nbsp;</span><br>
<br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;
color:#2F3438'>B. Your California Privacy Rights</span></strong> <br>
<span style='color:#2F3438'>California's &quot;Shine the Light&quot; law
permits customers in California to request certain details about how certain
types of their information are shared with third parties and, in some cases,
affiliates, for those third parties' and affiliates' own direct marketing
purposes. Under the law, a business should either provide California customers
certain information upon request or permit California customers to opt in to,
or opt out of, this type of sharing.</span> <br>
<span style='color:#2F3438'>If you are a California resident and wish to obtain
information about our compliance with this law, please contact us as set forth
in the section below, entitled &quot;Contact Us&quot;. Requests must include
&quot;California Privacy Rights Request&quot; in the first line of the description
and include your name, street address, city, state, and ZIP code. Please note
that the Company is not required to respond to requests made by means other
than through the provided email address or mail address.</span> <br>
<br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體;
color:#2F3438'>C. Your European Privacy Rights</span></strong> <br>
<span style='color:#2F3438'>If you are a data subject in Europe, you have the
right to access, rectify, or erase any personal data we have collected about
you through the Matrix On-Console Service. You also have the right to data
portability, right to be forgotten, and the right to restrict or object to our processing
of personal data we have collected about you through the Matrix On-Console
Service. In addition, you have the right to ask us not to process your personal
data (or provide it to third parties to process) for marketing purposes or
purposes materially different than for which it was originally collected or
subsequently authorized by you. You may withdraw your consent at any time for
any data processing we do based on consent you have provided to us.</span>
Simply contact us as set forth in the section below, entitled &quot;Contact
Us&quot;.<br>
</span><span lang=EN-US style='font-size:13.5pt'>If you have any issues with
our compliance, you have the right to lodge a complaint with a European
supervisory authority.&nbsp;</span></p>

<p><strong><span lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:
新細明體'>6. Information Security</span></strong><span lang=EN-US><br>
We take reasonable and appropriate security measures designed to protect the
information we collect from or about you against unauthorized access, use,
alteration, disclosure or destruction. Data is stored until the user explicitly
requests the erasure of his/her account data stored on our servers or until the
legal basis for the processing of the data is no longer valid. Please be aware,
however, that no method of transmitting information over the Internet or
storing information is completely secure. Accordingly, we cannot guarantee the
absolute security of any information.<o:p></o:p></span></p>

<h3 id=JohnsonHealthTechPrivacyPolicy-10-17-2019-7.InternationalTransfer><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>7.
International Transfer</span></strong></h3>

<p><span lang=EN-US style='color:#2F3438'>We are based in the United States and
the information we collect is governed by United States law. Your information
may be transferred to, and maintained on, computers located outside of your
state, province, country or other governmental jurisdiction where the privacy
laws may not be as protective as those in your jurisdiction. If you are located
outside the United States and choose to provide your information to us, we may
transfer that information to the United States and other jurisdictions and
process it there. Your use of the Matrix On-Console Services or provision of
any information represents your agreement to that transfer and processing.</span><span
lang=EN-US> If your data is collected in Europe, we will transfer your personal
data subject to appropriate or suitable safeguards, such as standard
contractual clauses.&nbsp;</span></p>

<p><span lang=EN-US><br>
<strong><span style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>8. Our
Policy toward Children</span></strong><br>
<span style='color:#2F3438'>The Matrix On-Console Services are not directed to
people under 13 and we do not knowingly collect personal information as defined
by the U.S. Children's Online Privacy Protection Act (&quot;COPPA&quot;) from
children under 13. If we learn that we have collected personal information of a
child under 13 we will take steps to delete such information from our files as
soon as possible.</span><o:p></o:p></span></p>

<h3 id=JohnsonHealthTechPrivacyPolicy-10-17-2019-9.ChangestoThisPrivacyPolicy><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>9.
Changes to This Privacy Policy</span></strong></h3>

<p><span lang=EN-US>Please note that this Privacy Policy may change from time
to time, and you should review it periodically. Any changes will be effective
immediately upon posting of the revised Privacy Policy. Your continued use of
our services indicates your consent to the Privacy Policy then posted. If the
changes are material, we may provide you additional notice to your email
address.<o:p></o:p></span></p>

<h3 id=JohnsonHealthTechPrivacyPolicy-10-17-2019-10.DataController><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>10. Data
Controller</span></strong><span lang=EN-US><o:p></o:p></span></h3>

<p><span data-ref=555e6614-e52e-4d54-ae15-2727296be4cb><span
class=inline-comment-marker><span lang=EN-US style='font-family:"新細明體",serif;
mso-bidi-font-family:新細明體'>EU data protection law makes a distinction between
organizations that process personal data for their own purposes (known as
&quot;controllers&quot;) and organizations that process personal data on behalf
of other organizations (known as &quot;processors&quot;). The Company may act
as either a controller or a processor in respect to your personal data,
depending on the circumstances. The Company, located at the address in the
section below, entitled &quot;Contact Us&quot;, is the controller with respect
to information you input to through the Matrix On-Console Services. This
information includes single sign on credentials, Asset Management user information,
and other services. </span></span></span><span lang=EN-US><o:p></o:p></span></p>

<p><span data-ref=555e6614-e52e-4d54-ae15-2727296be4cb><span
class=inline-comment-marker><span lang=EN-US style='font-family:"新細明體",serif;
mso-bidi-font-family:新細明體'>The Company, located at the address in the section
below, entitled &quot;Contact Us&quot;, is the processor with respect to any
workout information generated via the fitness equipment.&nbsp; The customer
which purchased the Matrix On-Console Services to provide an experience to
their end users, is the data controller.&nbsp; In this case, The Company is a
data processor, processing user data on behalf of the wishes of the customer.</span></span></span><span
lang=EN-US><o:p></o:p></span></p>

<p><span data-ref=555e6614-e52e-4d54-ae15-2727296be4cb><span
class=inline-comment-marker><span lang=EN-US style='font-family:"新細明體",serif;
mso-bidi-font-family:新細明體'>Sometimes the Company operates as a processor on
behalf of a customer, a separate legal entity, which is the controller. For
example, the Company provides technology for clubs and trainers to provide you
with their own apps and fitness tracking.</span></span></span><span lang=EN-US><o:p></o:p></span></p>

<p><span data-ref=555e6614-e52e-4d54-ae15-2727296be4cb><span
class=inline-comment-marker><span lang=EN-US style='font-family:"新細明體",serif;
mso-bidi-font-family:新細明體'>In such circumstances, the Company processes
personal data on behalf and at the instruction of its customers. Please visit
the applicable customer's privacy policy for information about their privacy
practices. Any questions that you may have relating to such personal data and
your rights under data protection law should therefore be directed to the
customer as the controller, not to the Company</span></span></span><span
lang=EN-US>.<o:p></o:p></span></p>

<h3 id=JohnsonHealthTechPrivacyPolicy-10-17-2019-11.ContactUs><strong><span
lang=EN-US style='font-family:"新細明體",serif;mso-bidi-font-family:新細明體'>11.
Contact Us</span></strong></h3>

<p><span lang=EN-US style='color:#2F3438'>Please feel free to direct any
questions or concerns regarding this Privacy Policy or the Company's data
practices by contacting us at:</span><span lang=EN-US> <br>
<br>
<span class=inline-comment-marker><span style='font-family:"新細明體",serif;
mso-bidi-font-family:新細明體'><span data-ref=aada31c5-d1ee-477a-9556-e552ee0a5aed>privacy@matrixfitness.com</span></span></span></span></p>

<p><span lang=EN-US><br>
Privacy Team<br>
Johnson Health Tech<br>
1600 Landmark Dr<br>
Cottage Grove, WI 53527</span></p>

</div>

</body>

</html>`;

export default html_en;