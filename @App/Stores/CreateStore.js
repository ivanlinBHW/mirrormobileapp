import AsyncStorage from '@react-native-community/async-storage';
import { applyMiddleware, compose, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import { Platform } from 'react-native';
import reduxReset from 'redux-reset';
import thunk from 'redux-thunk';
import { persistVersion, persistMigrate } from './CreateMigrations';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  timeout: null,
  version: persistVersion,
  migrate: persistMigrate,
  blacklist: [
    'appState',
    'appRoute',
    'websocket',
    'bleDevice',
    'progress',
    'mdns',
    'class',
    'search.workoutTypeList',
    'seeAll',
    'appApi',
    'search',
  ],
  debug: __DEV__,
};

export default (rootReducer, rootSaga) => {
  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const sagaMiddleware = createSagaMiddleware();
  const middleware = [];
  middleware.push(sagaMiddleware);
  middleware.push(thunk);
  let composeEnhancers = compose;
  if (__DEV__) {
    composeEnhancers = (
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ||
      require('remote-redux-devtools').composeWithDevTools
    )({
      name: Platform.OS,
      trace: true,
      traceLimit: 25,
    });

    const createDebugger = require('redux-flipper').default;
    middleware.push(createDebugger({ resolveCyclic: true }));
  }
  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('App/Stores').default;
      store.replaceReducer(nextRootReducer);
    });
  }
  const store = createStore(
    persistedReducer,
    composeEnhancers(applyMiddleware(...middleware), reduxReset()),
  );
  sagaMiddleware.run(rootSaga);
  return {
    persistor: persistStore(store),
    store,
  };
};
