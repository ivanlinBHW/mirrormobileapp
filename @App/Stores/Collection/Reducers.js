/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { CollectionTypes } from './Actions';

export const getCollectionListSuccess = (state, { list }) => ({
  ...state,
  list: list,
});

export const getCollectionDetailSuccess = (state, { item }) => ({
  ...state,
  detail: item,
});

export const setCollectionBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.detail.trainingClasses)) {
    temp = JSON.parse(JSON.stringify(state.detail.trainingClasses));
  }
  temp.forEach((data) => {
    if (data.id === id) {
      data.isBookmarked = true;
    }
  });
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClasses: temp,
    },
  };
};

export const removeCollectionBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.detail.trainingClasses)) {
    temp = JSON.parse(JSON.stringify(state.detail.trainingClasses));
  }
  temp.forEach((data) => {
    if (data.id === id) {
      data.isBookmarked = false;
    }
  });
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClasses: temp,
    },
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [CollectionTypes.GET_COLLECTION_LIST_SUCCESS]: getCollectionListSuccess,
  [CollectionTypes.GET_COLLECTION_DETAIL_SUCCESS]: getCollectionDetailSuccess,
  [CollectionTypes.SET_COLLECTION_BOOKMARK_SUCCESS]: setCollectionBookmarkSuccess,
  [CollectionTypes.REMOVE_COLLECTION_BOOKMARK_SUCCESS]: removeCollectionBookmarkSuccess,
});
