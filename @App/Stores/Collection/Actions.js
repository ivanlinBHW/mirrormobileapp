import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getCollectionList: ['token'],
  getCollectionDetail: ['id', 'token'],
  getCollectionListSuccess: ['list'],
  getCollectionDetailSuccess: ['item'],

  setCollectionBookmark: ['id'],
  setCollectionBookmarkSuccess: ['id'],
  removeCollectionBookmark: ['id'],
  removeCollectionBookmarkSuccess: ['id'],
});

export const CollectionTypes = Types;
export default Creators;
