import { createActions } from 'reduxsauce';

const { Types, Creators } = createActions({
  onAlert: {
    type: '',
    title: '',
    content: '',
  },
});

export const AppAlertTypes = Types;
export default Creators;
