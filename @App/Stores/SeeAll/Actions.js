import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  setList: ['data'],
  setBookmark: ['id'],
  removeBookmark: ['id'],
  getTrainingClassList: ['workoutType'],
  setPage: ['curPage', 'perPage', 'total'],
  resetList: null,
  setClassList: ['classList'],
  loadingFilter: null,
  setClassListFilter: ['filter'],

  getSeeAllList: ['mode', 'curPage', 'perPage'],
  getSeeAllListSuccess: ['data', 'total', 'hasNext'],
});

export const SeeAllTypes = Types;
export default Creators;
