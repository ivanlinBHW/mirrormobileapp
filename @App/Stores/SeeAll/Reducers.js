/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { uniqBy } from 'lodash';
import { SeeAllTypes } from './Actions';
import { SearchTypes } from '../Search/Actions';

export const setList = (state, { data }) => ({
  ...state,
  list: data,
});

export const setBookmarkSuccess = (state, { id }) => {
  console.log('setBookmarkSuccess id =>', id);
  let temp = JSON.parse(JSON.stringify(state.list));
  temp.forEach((item, index) => {
    if (item.id === id) {
      temp[index].isBookmarked = true;
    }
  });
  return {
    ...state,
    list: temp,
  };
};

export const removeBookmarkSuccess = (state, { id }) => {
  console.log('removeBookmarkSuccess id =>', id);
  let temp = JSON.parse(JSON.stringify(state.list));
  temp.forEach((item, index) => {
    if (item.id === id) {
      temp[index].isBookmarked = false;
    }
  });
  return {
    ...state,
    list: temp,
  };
};

export const setPage = (state, { curPage = 1, pageSize = 15, total = 0 }) => {
  return {
    ...state,
    curPage: curPage,
    pageSize: pageSize,
    total: total,
  };
};

export const resetList = (state) => {
  return {
    ...state,
    list: [],
    listLoading: true,
  };
};

export const setClassList = (state, { classList }) => {
  let uniqTemp = [];
  if (classList && classList.length > 0) {
    let temp = [...state.list, ...classList];
    uniqTemp = uniqBy(temp, 'id');
  }

  return {
    ...state,
    list: uniqTemp,
    listLoading: false,
  };
};

export const setClassListFilter = (state, { filter }) => {
  console.log('setClassListFilter=>', filter);
  return {
    ...state,
    filter,
    filterLoading: false,
  };
};

export const loadingFilter = (state) => {
  return {
    ...state,
    filterLoading: true,
  };
};

export const getSeeAllList = (state, { mode, curPage = 1, perPage = 15 }) => {
  return {
    ...state,
    list: curPage === 1 ? [] : state.list,
    mode,
    perPage,
    curPage,
    listLoading: true,
  };
};

export const getSeeAllListSuccess = (state, { data, total, hasNext }) => {
  return {
    ...state,
    list: uniqBy([...state.list, ...data], 'id'),
    total,
    hasNext,
    listLoading: false,
  };
};

export const search = (state, { page }) => {
  return {
    ...state,
    list: page.page === 1 ? [] : state.list,
    listLoading: true,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [SeeAllTypes.SET_LIST]: setList,
  [SeeAllTypes.SET_BOOKMARK]: setBookmarkSuccess,
  [SeeAllTypes.REMOVE_BOOKMARK]: removeBookmarkSuccess,
  [SeeAllTypes.SET_PAGE]: setPage,
  [SeeAllTypes.RESET_LIST]: resetList,
  [SearchTypes.SEARCH]: search,
  [SeeAllTypes.SET_CLASS_LIST]: setClassList,
  [SeeAllTypes.SET_CLASS_LIST_FILTER]: setClassListFilter,
  [SeeAllTypes.LOADING_FILTER]: loadingFilter,

  [SeeAllTypes.GET_SEE_ALL_LIST_SUCCESS]: getSeeAllListSuccess,
  [SeeAllTypes.GET_SEE_ALL_LIST]: getSeeAllList,
});
