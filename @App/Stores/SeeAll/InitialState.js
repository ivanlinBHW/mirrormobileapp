/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  list: [],
  filter: null,
  listLoading: false,
  filterLoading: false,

  mode: undefined,
  curPage: 1,
  perPage: 10,
  total: 0,
  hasNext: false,
};
