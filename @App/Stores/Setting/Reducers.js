/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import FastImage from 'react-native-fast-image';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { SettingTypes } from './Actions';
import { UserTypes } from '../User/Actions';

export const updateSettingStore = (state, action) => ({
  ...state,
  ...action.data,
});

export const getSettingSuccess = (state, { payload }) => {
  const data = Object.assign(state, {
    setting: { ...payload },
    editSetting: { ...payload },
    payload: {},
  });
  return data;
};

export const getGenreSuccess = (state, { genres }) => ({
  ...state,
  genres: genres,
});

export const updateAvatarSuccess = (state, { payload }) => ({
  ...state,
  setting: {
    ...state.setting,
    avatar: payload,
  },
});

export const resetAvatar = (state) => ({
  ...state,
  setting: {
    ...state.setting,
    avatar: null,
  },
});

export const updateEditSetting = (state, { item }) => ({
  ...state,
  editSetting: item,
});

export const updateEditEquipment = (state, { item }) => ({
  ...state,
  editEquipment: item,
});

export const updatePayload = (state, { item }) => ({
  ...state,
  payload: {
    ...state.payload,
    ...item,
  },
});

export const resetEditSetting = (state) => {
  return {
    ...state,
    editSetting: JSON.parse(JSON.stringify(state.setting)),
    payload: {},
    editEquipment: [],
  };
};

export const updateEquipmentSuccess = (state, { item }) => ({
  ...state,
  setting: {
    ...state.setting,
    equipments: item,
  },
});
export const reducer = createReducer(INITIAL_STATE, {
  [UserTypes.ON_USER_RESET]: () => INITIAL_STATE,
  [SettingTypes.GET_GENRE_SUCCESS]: getGenreSuccess,
  [SettingTypes.GET_SETTING_SUCCESS]: getSettingSuccess,
  [SettingTypes.UPDATE_AVATAR_SUCCESS]: updateAvatarSuccess,
  [SettingTypes.RESET_AVATAR]: resetAvatar,
  [SettingTypes.UPDATE_EDIT_SETTING]: updateEditSetting,
  [SettingTypes.UPDATE_EDIT_EQUIPMENT]: updateEditEquipment,
  [SettingTypes.UPDATE_PAYLOAD]: updatePayload,
  [SettingTypes.RESET_EDIT_SETTING]: resetEditSetting,
  [SettingTypes.UPDATE_EQUIPMENT_SUCCESS]: updateEquipmentSuccess,
  [SettingTypes.UPDATE_SETTING_STORE]: updateSettingStore,
});
