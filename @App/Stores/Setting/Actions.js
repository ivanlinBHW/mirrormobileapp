import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getSetting: [],
  getSettingSuccess: ['payload'],

  getGenre: null,
  getGenreSuccess: ['genres'],
  updateSetting: ['payload'],
  updateEquipment: ['payload'],
  updateAvatar: ['img'],
  updateAvatarSuccess: ['payload'],
  resetAvatar: [''],
  updateEditSetting: ['item'],
  updatePayload: ['item'],
  updateEditEquipment: ['item'],
  resetEditSetting: null,
  updateEquipmentSuccess: ['item'],

  onUpdateUserTimezone: ['timezone'],

  updateSettingStore: ['data'],
  fetchGetPrivacyPolicy: null,
  fetchGetUsagePolicy: null,
});

export const SettingTypes = Types;
export default Creators;
