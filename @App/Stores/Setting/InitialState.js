/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  setting: {
    fullName: 'Johns',
    age: 23,
    settings: {
      gender: 2,
      height: 148,
      weight: 67,
    },
  },
  editSetting: {},
  payload: {},
  editEquipment: [],

  genres: [],

  privacyPolicy: {},
  usagePolicy: {},
};
