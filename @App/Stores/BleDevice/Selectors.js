/**
 * Selectors let us factorize logic that queries the state.
 *
 * Selectors can be used in sagas or components to avoid duplicating that logic.
 *
 * Writing selectors is optional as it is not always necessary, we provide a simple example below.
 */

export const getShouldScanning = (state) => {
  return !state.bleDevice.isScanning;
};

export const getScannedDevices = (state) => {
  return state.bleDevice.searchDevices;
};

export const getScanUuidFilter = (state) => {
  return state.bleDevice.lastFilterUuid;
};
