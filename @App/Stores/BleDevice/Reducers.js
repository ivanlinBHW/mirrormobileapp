/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { INITIAL_STATE, ConnectionState } from './InitialState';
import { BleDeviceTypes as Types } from './Actions';
import { MirrorTypes } from 'App/Stores/Mirror/Actions';
import { State } from 'react-native-ble-plx';
import { uniqBy } from 'lodash';

export const pairDevice = (state, { device }) => ({
  ...state,
  lastDevice: device,
});
export const reducer = createReducer(INITIAL_STATE, {
  [Types.ON_DEVICE_CONNECTED]: pairDevice,
  [Types.ON_LOG]: (state, action) => {
    __DEV__ && console.log('ON_BLE_LOG: ', action.message);
    return {
      ...state,
      logs: [action.message, ...state.logs],
    };
  },
  [Types.ON_LOG_ERROR]: (state, { error }) => ({
    ...state,
    logs: [
      'ERROR: ' +
      error.message +
      ', ATT: ' +
      (error.attErrorCode || 'null') +
      ', iOS: ' +
      (error.iosErrorCode || 'null') +
      ', android: ' +
      (error.androidErrorCode || 'null') +
      ', reason: ' +
      (error.reason || 'null'),
      ,
      ...state.logs,
    ],
  }),
  [Types.ON_CLEAR_LOGS]: (state) => ({
    ...state,
    logs: [],
  }),
  [Types.ON_SCAN]: (state, action) => ({
    ...state,
    isScanning: true,
    lastFilterUuid: action.uuids || undefined,
    isManual: action.isManual,
  }),
  [Types.ON_INITIAL]: (state) => ({
    ...state,
  }),
  [Types.ON_STOP_SCAN]: (state) => ({
    ...state,
    isScanning: false,
  }),
  [MirrorTypes.START_SCAN]: (state) => ({
    ...state,
    isScanning: true,
  }),
  [MirrorTypes.END_SCAN]: (state) => ({
    ...state,
    isScanning: false,
  }),
  [Types.ON_CONNECT]: (state, action) => ({
    ...state,
    activeDevice: action.device,
    pairedDevices: uniqBy([...state.pairedDevices, action.device], 'id'),
    searchDevices: state.searchDevices.filter(
      (item) => action.device && item && item.localName !== action.device.localName,
    ),
    error: null,
    data: null,
  }),
  [Types.ON_DISCONNECT]: (state, action) => ({
    ...state,
    activeDevice: null,
    error: null,
    data: null,
    pairedDevices: [],
  }),
  [Types.ON_UPDATE_CONNECTION_STATE]: (state, action) => {
    return {
      ...state,
      connectionState: action.state,
      pairedDevices:
        action.state === 'CONNECTED'
          ? uniqBy([state.activeDevice, ...state.pairedDevices], 'id')
          : state.pairedDevices.filter(
            (device) =>
              action.device && device.localName !== state.activeDevice.localName,
          ),
      searchDevices:
        action.state !== 'CONNECTED'
          ? uniqBy(state.searchDevices, 'id')
          : state.searchDevices.filter(
            (device) =>
              action.device && device.localName !== state.activeDevice.localName,
          ),
      logs: ['Connection state changed: ' + action.state, ...state.logs],
    };
  },
  [Types.ON_BLE_STATE_UPDATED]: (state, action) => ({
    ...state,
    bleState: action.state,
    logs: ['BLE state changed: ' + action.state, ...state.logs],
    activeDevice: action.state === State.PoweredOff ? null : state.activeDevice,
  }),
  [Types.ON_DEVICE_FOUND]: (state, action) => ({
    ...state,
    searchDevices:
      action.device &&
        state.pairedDevices &&
        state.pairedDevices.length > 0 &&
        state.pairedDevices.some(
          (item) => action.device && item.localName === action.device.localName,
        )
        ? [...state.searchDevices]
        : [action.device, ...state.searchDevices],
    logs: [`Device found: ${action.device.id}(${action.device.name})`, ...state.logs],
  }),
  [Types.ON_FORGET_DEVICES]: (state) => ({
    ...state,
    searchDevices: [],
    pairedDevices: [],
  }),
  [Types.ON_EXECUTE_TEST]: (state, action) => {
    if (state.connectionState !== ConnectionState.CONNECTED) {
      return state;
    }
    return { ...state, currentTest: action.id };
  },
  [Types.ON_TEST_FINISHED]: (state) => ({
    ...state,
    activeDevice: null,
  }),
  [Types.ON_CHARACTERISTIC_UPDATE]: (state, action) => ({
    ...state,
    error: action.error ? action.error : state.error,
    data: action.data,
  }),
});
