import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  onInitial: null,
  onDeviceConnected: ['device'],
  onLog: ['message'],
  onLogError: ['error'],
  onClearLogs: null,
  onConnect: ['device'],
  onConnectById: ['id'],
  onScan: ['uuids', 'isManual'],
  onStopScan: null,
  onUpdateConnectionState: ['state'],
  onDisconnect: null,
  onBleStateUpdated: ['state'],
  onDeviceFound: ['device'],
  onForgetDevices: null,
  onTestFinished: null,
  onExecuteTest: ['id'],

  onBleMonitorCharacteristic: ['serviceUuid', 'characteristicUuid'],
  onBleWriteCharacteristic: ['serviceUuid', 'characteristicUuid', 'data'],

  onCharacteristicUpdate: ['data', 'error'],
});

export const BleDeviceTypes = Types;

export const BleServices = {
  HEAR_RATE_SERVICE_GUID: '180D',
  HEART_RATE_MEASUREMENT_CHARACTERISTIC_GUID: '2A37',
};

export default Creators;
