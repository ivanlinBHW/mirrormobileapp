import { State } from 'react-native-ble-plx';
const EMPTY_DEVICE = {
  fullName: '',
  name: '',
  port: '',
  host: '',
  ip: '',
};

export const ConnectionState = {
  DISCONNECTED: 'DISCONNECTED',
  CONNECTING: 'CONNECTING',
  DISCOVERING: 'DISCOVERING',
  CONNECTED: 'CONNECTED',
  DISCONNECTING: 'DISCONNECTING',
};

export const INITIAL_STATE = {
  lastDevice: Object.assign({}, EMPTY_DEVICE),
  lastFilterUuid: undefined,
  bleState: State.Unknown,
  activeDevice: null,
  connectionState: ConnectionState.DISCONNECTED,
  currentTest: null,
  logs: [],
  searchDevices: [],
  pairedDevices: [],
  isScanning: false,
  error: null,
  data: null,
};
