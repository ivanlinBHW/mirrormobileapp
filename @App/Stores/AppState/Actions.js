import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  onLoading: ['isLoading', 'message', 'options', 'isNavLoading'],
  onNavLoading: ['isNavLoading', 'message', 'options'],
  onStateChange: ['currentState'],
  onOrientationChange: ['currentOrientation'],
  onNetInfoChange: ['state'],
  onEnvChange: ['env'],
  doEnvChange: ['env'],
  onAppInit: null,
  onLocaleChange: {
    currentLocales: null,
    currentTimeZone: '',
  },
  onVersionChange: {
    appVersion: '',
    buildVersion: '',
    bundleIdentifier: '',
  },
  onDeviceChange: ['device'],
  onDeviceWifiInit: ['ssid'],
  onAlertingChange: ['currentAlert'],

  setCurrentWifiSettingRoute: ['currentWifiSettingRoute'],

  reset: ['state'],
});

export const AppStateTypes = Types;

export default Creators;
