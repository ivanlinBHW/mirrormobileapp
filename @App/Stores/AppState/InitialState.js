/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  currentOrientation: '',
  currentState: 'active',
  currentTimeZone: 'Asia/Taipei',
  currentVersion: {},
  currentLocales: [],
  currentNetworkInfo: {
    details: {
      subnet: '',
      ipAddress: '',
      strength: 0,
      ssid: '',
      isConnectionExpensive: null,
    },
    isInternetReachable: null,
    isConnected: null,
    type: '',
  },
  currentAlert: {
    title: '',
    content: '',
    type: '',
    isAutoConnectAlerting: false,
    isGoBackPageAlerting: false,
  },
  loadingMessage: '',
  isLoading: false,
  loadingOptions: {},
  currentDevice: {
    isTablet: null,
    isEmulator: null,
    systemVersion: '',
    deviceId: '',
    deviceName: '',
    totalMemoryInMb: 0,
  },

  wifiSettingRoute: '',
};
