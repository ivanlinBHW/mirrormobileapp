/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { INITIAL_STATE } from './InitialState';
import { AppStateTypes } from './Actions';

export const onAppVersionChange = (
  state,
  { appVersion, buildVersion, bundleIdentifier },
) => ({
  ...state,
  currentVersion: {
    appVersion,
    buildVersion,
    bundleIdentifier,
  },
});

export const onLoadingChange = (state, { isLoading, message, options }) => {
  return {
    ...state,
    isLoading,
    loadingMessage: message,
    loadingOptions: options,
  };
};
export const onNavLoading = (state, { isNavLoading, message, options }) => {
  return {
    ...state,
    isNavLoading,
    loadingMessage: message,
    loadingOptions: options,
  };
};
export const onAppStateChange = (state, { currentState }) => ({
  ...state,
  currentState,
});

export const onLocaleChange = (state, { currentLocales, currentTimeZone }) => ({
  ...state,
  currentLocales,
  currentTimeZone,
});
export const onEnvChange = (state, { env }) => ({
  ...state,
  env,
});

export const onOrientationChange = (state, { currentOrientation }) => ({
  ...state,
  currentOrientation,
});

export const onNetworkInfoChange = (state, action) => ({
  ...state,
  currentNetworkInfo: action.state,
});

export const onDeviceChange = (state, { device }) => ({
  ...state,
  currentDevice: {
    ...state.currentDevice,
    ...device,
  },
});

export const onDeviceWifiInit = (state, { ssid }) => ({
  ...state,
  deviceWifiSSID: ssid,
});

export const onAlertingChange = (state, { currentAlert }) => {
  console.log('=== reducer onAlertingChange ===', currentAlert);
  return {
    ...state,
    currentAlert,
  };
};

export const setCurrentWifiSettingRoute = (state, { currentWifiSettingRoute }) => {
  console.log('=== set currentWifiSettingRoute ===', currentWifiSettingRoute);
  return {
    ...state,
    wifiSettingRoute: currentWifiSettingRoute,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [AppStateTypes.ON_LOADING]: onLoadingChange,
  [AppStateTypes.ON_NAV_LOADING]: onNavLoading,
  [AppStateTypes.ON_DEVICE_CHANGE]: onDeviceChange,
  [AppStateTypes.ON_LOCALE_CHANGE]: onLocaleChange,
  [AppStateTypes.ON_STATE_CHANGE]: onAppStateChange,
  [AppStateTypes.ON_VERSION_CHANGE]: onAppVersionChange,
  [AppStateTypes.ON_NET_INFO_CHANGE]: onNetworkInfoChange,
  [AppStateTypes.ON_ORIENTATION_CHANGE]: onOrientationChange,
  [AppStateTypes.ON_ENV_CHANGE]: onEnvChange,
  [AppStateTypes.ON_DEVICE_WIFI_INIT]: onDeviceWifiInit,
  [AppStateTypes.ON_ALERTING_CHANGE]: onAlertingChange,
  [AppStateTypes.SET_CURRENT_WIFI_SETTING_ROUTE]: setCurrentWifiSettingRoute,
});
