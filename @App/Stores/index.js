import { combineReducers } from 'redux';
import configureStore from './CreateStore';
import rootSaga from 'App/Sagas';
import { reducer as AppStateReducer } from './AppState/Reducers';
import { reducer as AppRouteReducer } from './AppRoute/Reducers';
import { reducer as AppApiReducer } from './AppApi/Reducers';
import { reducer as AppBulletinReducer } from './AppBulletin/Reducers';
import { reducer as UserReducer } from './User/Reducers';
import { reducer as LiveReducer } from './Live/Reducers';
import { reducer as MdnsReducer } from './Mdns/Reducers';
import { reducer as ClassReducer } from './Class/Reducers';
import { reducer as MirrorReducer } from './Mirror/Reducers';
import { reducer as ExampleReducer } from './Example/Reducers';
import { reducer as ProgramReducer } from './Program/Reducers';
import { reducer as ProgressReducer } from './Progress/Reducers';
import { reducer as BleDeviceReducer } from './BleDevice/Reducers';
import { reducer as WebsocketReducer } from './Websocket/Reducers';
import { reducer as CollectionReducer } from './Collection/Reducers';
import { reducer as InstructorReducer } from './Instructor/Reducers';
import { reducer as SpotifyReducer } from './Spotify/Reducers';
import { reducer as PlayerReducer } from './Player/Reducers';
import { reducer as SearchReducer } from './Search/Reducers';
import { reducer as NotificationReducer } from './Notification/Reducers';
import { reducer as SettingReducer } from './Setting/Reducers';
import { reducer as SeeAllReducer } from './SeeAll/Reducers';
import { reducer as CommunityReducer } from './Community/Reducers';
import { reducer as ChannelReducer } from './Channel/Reducers';
import { reducer as DeviceVersionInfoReducer } from './DeviceVersionInfo/Reducers';
import { reducer as TrackRecordReducer } from './TrackRecord/Reducers';
export { store as AppStore } from '../App';
export { default as AppApiActions } from './AppApi/Actions';
export { default as AppAlertActions } from './AppAlert/Actions';
export { default as AppStateActions } from './AppState/Actions';
export { default as AppBulletinActions } from './AppBulletin/Actions';
export { default as BleDeviceActions } from './BleDevice/Actions';
export { default as WebsocketActions } from './Websocket/Actions';
export { default as ProgressActions } from './Progress/Actions';
export { default as ProgramActions } from './Program/Actions';
export { default as SpotifyActions } from './Spotify/Actions';
export { default as SearchActions } from './Search/Actions';
export { default as MirrorActions } from './Mirror/Actions';
export { default as CollectionActions } from './Collection/Actions';
export { default as ClassActions } from './Class/Actions';
export { default as MdnsActions } from './Mdns/Actions';
export { default as LiveActions } from './Live/Actions';
export { default as PlayerActions } from './Player/Actions';
export { default as UserActions } from './User/Actions';
export { default as SettingActions } from './Setting/Actions';
export { default as NotificationActions } from './Notification/Actions';
export { default as SeeAllActions } from './SeeAll/Actions';
export { default as CommunityActions } from './Community/Actions';
export { default as InstructorActions } from './Instructor/Actions';
export { default as ChannelActions } from './Channel/Actions';
export { default as DeviceVersionInfoActions } from './DeviceVersionInfo/Actions';
export { default as TrackRecordActions } from './TrackRecord/Actions';

export default () => {
  const rootReducer = combineReducers({
    appState: AppStateReducer,
    appRoute: AppRouteReducer,
    appApi: AppApiReducer,
    appBulletin: AppBulletinReducer,
    live: LiveReducer,
    mdns: MdnsReducer,
    user: UserReducer,
    class: ClassReducer,
    mirror: MirrorReducer,
    example: ExampleReducer,
    program: ProgramReducer,
    progress: ProgressReducer,
    bleDevice: BleDeviceReducer,
    websocket: WebsocketReducer,
    collection: CollectionReducer,
    player: PlayerReducer,
    instructor: InstructorReducer,
    search: SearchReducer,
    spotify: SpotifyReducer,
    setting: SettingReducer,
    notification: NotificationReducer,
    seeAll: SeeAllReducer,
    community: CommunityReducer,
    channel: ChannelReducer,
    deviceVersionInfo: DeviceVersionInfoReducer,
    trackRecord: TrackRecordReducer,
  });

  return configureStore(rootReducer, rootSaga);
};
