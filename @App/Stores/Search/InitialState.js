/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  channelList: [],
  equipmentList: [],
  popularTagList: [],
  instructorList: [],
  workoutTypeList: [],
  list: [],
  searchData: {},

  communitySearch: {
    total: 0,
    list: [],
    payload: {
      page: 1,
      pageSize: 10,
      filter: [],
      type: 'page',
    },
  },

  searchHistory: [],
  hotKeywords: [],
};
