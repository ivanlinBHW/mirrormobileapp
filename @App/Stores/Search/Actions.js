import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getInstructorList: ['token'],
  getInstructorListSuccess: ['action'],
  getEquipmentList: ['token'],
  getEquipmentListSuccess: ['action'],
  getWorkoutTypeList: null,
  getWorkoutTypeListSuccess: ['data'],
  search: ['payload', 'page', 'showLoadingIndicator'],
  searchFilter: ['payload'],

  communitySearch: ['payload', 'page', 'pageSize', 'isEvent', 'onSuccessCb'],
  communitySearchSuccess: ['data', 'total'],
  searchSuccess: ['action'],
  setSearchData: ['data'],

  getSearchableData: null,
  getSearchableDataSuccess: {
    channelList: [],
    equipmentList: [],
    instructorList: [],
    popularTagList: [],
    workoutTypeList: [],
  },

  getSearchHotKeyword: null,
  getSearchHotKeywordSuccess: ['data'],
  setSearchHistory: ['keyword'],
});

export const SearchTypes = Types;
export default Creators;
