/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { SearchTypes } from './Actions';
import { uniqBy, indexOf } from 'lodash';

export const getSearchableDataSuccess = (
  state,
  { workoutTypeList, channelList, equipmentList, popularTagList, instructorList },
) => ({
  ...state,
  workoutTypeList,
  channelList,
  equipmentList,
  instructorList,
  popularTagList,
});

export const communitySearch = (state, { payload, page, pageSize }) => {
  return {
    ...state,
    communitySearch: {
      ...state.communitySearch,
      list: page === 1 ? [] : state.communitySearch.list,
      payload: {
        ...payload,
        page,
        pageSize,
      },
    },
  };
};

export const communitySearchSuccess = (state, { data, total }) => {
  return {
    ...state,
    communitySearch: {
      ...state.communitySearch,
      list: uniqBy([...state.communitySearch.list, ...data], 'id'),
      total,
    },
  };
};

export const getSearchHotKeywordSuccess = (state, { data }) => {
  return {
    ...state,
    hotKeywords: data,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [SearchTypes.GET_SEARCHABLE_DATA_SUCCESS]: getSearchableDataSuccess,
  [SearchTypes.GET_INSTRUCTOR_LIST_SUCCESS]: (state, { action }) => ({
    ...state,
    instructorList: action,
  }),
  [SearchTypes.GET_EQUIPMENT_LIST_SUCCESS]: (state, { action }) => ({
    ...state,
    equipmentList: action,
  }),
  [SearchTypes.SEARCH_SUCCESS]: (state, { action }) => {
    return {
      ...state,
      list: action,
    };
  },
  [SearchTypes.SEARCH]: (state, { action }) => {
    return {
      ...state,
      list: [],
    };
  },
  [SearchTypes.SET_SEARCH_DATA]: (state, { action, data }) => {
    let newSearchHistory = [...(state.searchHistory || [])];

    if (data.text) {
      newSearchHistory = newSearchHistory.filter((e) => e !== data.text);
      if (newSearchHistory.length >= 3) {
        newSearchHistory.pop();
      }
      newSearchHistory.unshift(data.text);
    }

    return {
      ...state,
      searchData: data,
      searchHistory: newSearchHistory,
    };
  },
  [SearchTypes.GET_WORKOUT_TYPE_LIST_SUCCESS]: (state, { data }) => {
    return {
      ...state,
      workoutTypeList: data,
    };
  },
  [SearchTypes.COMMUNITY_SEARCH]: communitySearch,
  [SearchTypes.COMMUNITY_SEARCH_SUCCESS]: communitySearchSuccess,
  [SearchTypes.GET_SEARCH_HOT_KEYWORD_SUCCESS]: getSearchHotKeywordSuccess,
});
