/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { InstructorTypes } from './Actions';
export const reducer = createReducer(INITIAL_STATE, {
  [InstructorTypes.GET_INSTRUCTOR_SUCCESS]: (state, { action }) => ({
    ...state,
    ...action,
  }),
  [InstructorTypes.SET_INSTRUCTOR_ID_SUCCESS]: (state, { id }) => ({
    ...state,
    id: id,
  }),
});
