import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getInstructor: ['id', 'token'],
  getInstructorSuccess: ['action'],
  setInstructorId: ['id', 'token'],
  setInstructorIdSuccess: ['id'],
});

export const InstructorTypes = Types;
export default Creators;
