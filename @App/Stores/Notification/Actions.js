import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getNotificationList: null,
  getNotificationListSuccess: ['items'],

  getNotificationStatus: null,
  getNotificationStatusSuccess: ['hasUnread'],
});

export const NotificationTypes = Types;
export default Creators;
