/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { NotificationTypes } from './Actions';

export const getNotificationListSuccess = (state, { items }) => ({
  ...state,
  list: items,
});
export const getNotificationStatusSuccess = (state, { hasUnread }) => ({
  ...state,
  hasUnread,
});
export const reducer = createReducer(INITIAL_STATE, {
  [NotificationTypes.GET_NOTIFICATION_LIST_SUCCESS]: getNotificationListSuccess,
  [NotificationTypes.GET_NOTIFICATION_STATUS_SUCCESS]: getNotificationStatusSuccess,
});
