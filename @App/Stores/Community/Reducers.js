/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { CommunityTypes } from './Actions';
import { UserTypes } from '../User/Actions';

export const getListSuccess = (state, { list }) => ({
  ...state,
  list,
});

export const setUserEventList = (state, { data }) => ({
  ...state,
  userEventList: data,
});

export const setAllEventList = (state, { data }) => ({
  ...state,
  allEventList: data,
});

export const getAllMyEventSuccess = (state, { data }) => ({
  ...state,
  allMyEventList: data,
});

export const getAllEventsSuccess = (state, { data }) => ({
  ...state,
  allEventsList: data,
});

export const getSearchFriendsSuccess = (state, { data }) => ({
  ...state,
  friends: data,
});
export const setSearchEventsText = (state, { data }) => ({
  ...state,
  searchText: data,
});

export const saveSelectedFriends = (state, { data }) => ({
  ...state,
  friendSelected: data,
});

const getCoverImagesSuccess = (state, { data }) => ({
  ...state,
  coverImages: data,
});

export const getUserDetailSuccess = (state, { data }) => ({
  ...state,
  userDetail: data,
});

export const addFriendSuccess = (state, { data }) => {
  return {
    ...state,
    userDetail: {
      ...state.userDetail,
      friendStatus: data.userFriendStatus,
    },
  };
};

export const createEventSuccess = (state, { data }) => ({
  ...state,
  userEventList: [data, ...state.userEventList],
  allEventList: [data, ...state.allEventList],
});

export const getSearchEventsSuccess = (state, { data }) => ({
  ...state,
  events: data,
});

export const getTrainingEventSuccess = (state, { data }) => {
  let { events } = state;
  console.log('=== data ===', data.id);

  events = events.map((event) => {
    if (event.id === data.id) return data;
    else return event;
  });

  return {
    ...state,
    eventDetail: data,
    events,
  };
};

export const setCommunityClassBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.eventDetail.trainingClasses)) {
    temp = JSON.parse(JSON.stringify(state.eventDetail.trainingClasses));
  }
  temp.forEach((data) => {
    if (data.id === id) {
      data.isBookmarked = true;
    }
  });
  return {
    ...state,
    eventDetail: {
      ...state.eventDetail,
      trainingClasses: temp,
    },
  };
};

export const removeCommunityClassBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.eventDetail.trainingClasses)) {
    temp = JSON.parse(JSON.stringify(state.eventDetail.trainingClasses));
  }
  temp.forEach((data) => {
    if (data.id === id) {
      data.isBookmarked = false;
    }
  });
  return {
    ...state,
    eventDetail: {
      ...state.eventDetail,
      trainingClasses: temp,
    },
  };
};

export const searchEventFriendSuccess = (state, { data }) => ({
  ...state,
  eventFriends: data,
});

export const setEventClassDetail = (state, { data }) => {
  return {
    ...state,
    eventClass: data,
  };
};

export const updateEventClassDetail = (state, { data }) => {
  return {
    ...state,
    eventClass: {
      ...state.eventClass,
      trainingEventClassHistory: _.has(data, 'eventClassHistory')
        ? data.eventClassHistory
        : {
            ...state.eventClass.eventClassHistory,
          },
      trainingClassHistory: _.has(data, 'trainingClassHistory')
        ? data.trainingClassHistory
        : {
            ...state.eventClass.trainingClassHistory,
          },
    },
  };
};

export const getEventLeaderBoardSuccess = (state, { data }) => {
  return {
    ...state,
    leaderBoard: data,
  };
};

export const leaveEventSuccess = (state, { id }) => {
  const tempEvents =
    state.events.length > 0 ? JSON.parse(JSON.stringify(state.events)) : [];
  if (tempEvents.length > 0) {
    tempEvents.map((item, index) => {
      if (item.id === id) {
        tempEvents[index].actionType = 'join';
      }
    });
  }
  const tempAllEventList =
    state.allEventList.length > 0 ? JSON.parse(JSON.stringify(state.allEventList)) : [];
  if (tempAllEventList.length > 0) {
    tempAllEventList.map((item, index) => {
      if (item.id === id) {
        tempAllEventList[index].actionType = 'join';
      }
    });
  }
  const tempAllEventsList =
    state.allEventsList.length > 0 ? JSON.parse(JSON.stringify(state.allEventsList)) : [];
  if (tempAllEventsList.length > 0) {
    tempAllEventsList.map((item, index) => {
      if (item.id === id) {
        tempAllEventsList[index].actionType = 'join';
      }
    });
  }
  return {
    ...state,
    events: tempEvents,
    allEventList: tempAllEventList,
    allEventsList: tempAllEventsList,
  };
};

export const joinEventSuccess = (state, { id }) => {
  const tempEvents =
    state.events.length > 0 ? JSON.parse(JSON.stringify(state.events)) : [];
  if (tempEvents.length > 0) {
    tempEvents.map((item, index) => {
      if (item.id === id) {
        tempEvents[index].actionType = 'invite';
      }
    });
  }
  const tempAllEventList =
    state.allEventList.length > 0 ? JSON.parse(JSON.stringify(state.allEventList)) : [];
  if (tempAllEventList.length > 0) {
    tempAllEventList.map((item, index) => {
      if (item.id === id) {
        tempAllEventList[index].actionType = 'invite';
      }
    });
  }
  const tempAllEventsList =
    state.allEventsList.length > 0 ? JSON.parse(JSON.stringify(state.allEventsList)) : [];
  if (tempAllEventsList.length > 0) {
    tempAllEventsList.map((item, index) => {
      if (item.id === id) {
        tempAllEventsList[index].actionType = 'invite';
      }
    });
  }
  return {
    ...state,
    events: tempEvents,
    allEventList: tempAllEventList,
    allEventsList: tempAllEventsList,
  };
};

export const addSelectedClasses = (state, { data }) => {
  let tempClassesSelected = JSON.parse(JSON.stringify(state.classesSelected));
  tempClassesSelected.push(data);
  return {
    ...state,
    classesSelected: tempClassesSelected,
  };
};

export const removeSelectedClasses = (state, { data }) => {
  let tempClassesSelected = JSON.parse(JSON.stringify(state.classesSelected));
  let temp = tempClassesSelected.filter((item) => item.id !== data.id);
  return {
    ...state,
    classesSelected: temp,
  };
};

export const saveSelectedClasses = (state, { data }) => ({
  ...state,
  classesSelected: data,
});

export const saveStashSelectedClasses = (state, { data }) => ({
  ...state,
  stashClassesSelected: data,
});

export const addStashSelectedClasses = (state, { data }) => {
  let tempStashClassesSelected = JSON.parse(JSON.stringify(state.stashClassesSelected));
  tempStashClassesSelected.push(data);
  return {
    ...state,
    stashClassesSelected: tempStashClassesSelected,
  };
};

export const removeStashSelectedClasses = (state, { data }) => {
  let tempStashClassesSelected = JSON.parse(JSON.stringify(state.stashClassesSelected));
  let temp = tempStashClassesSelected.filter((item) => item.id !== data.id);
  return {
    ...state,
    stashClassesSelected: temp,
  };
};

export const cleanSelectedClasses = (state) => {
  return {
    ...state,
    classesSelected: [],
  };
};

export const resetEventData = (state, { data }) => {
  return {
    ...state,
    stashClassesSelected: [],
    classesSelected: [],
  };
};

export const update1on1OnlineStatus = (state, { isReady }) => {
  return {
    ...state,
    isReady,
    classStartAt: isReady ? state.classStartAt : null,
  };
};

export const update1on1StartTime = (state, { classStartAt }) => {
  return {
    ...state,
    classStartAt,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [UserTypes.ON_USER_RESET]: () => INITIAL_STATE,
  [CommunityTypes.GET_LIST_SUCCESS]: getListSuccess,
  [CommunityTypes.SET_USER_EVENT_LIST]: setUserEventList,
  [CommunityTypes.SET_ALL_EVENT_LIST]: setAllEventList,
  [CommunityTypes.SET_SEARCH_EVENTS_TEXT]: setSearchEventsText,
  [CommunityTypes.GET_ALL_MY_EVENT_SUCCESS]: getAllMyEventSuccess,
  [CommunityTypes.GET_ALL_EVENTS_SUCCESS]: getAllEventsSuccess,
  [CommunityTypes.GET_SEARCH_FRIENDS_SUCCESS]: getSearchFriendsSuccess,
  [CommunityTypes.SAVE_SELECTED_FRIENDS]: saveSelectedFriends,
  [CommunityTypes.GET_COVER_IMAGES_SUCCESS]: getCoverImagesSuccess,
  [CommunityTypes.SAVE_SELECTED_CLASSES]: saveSelectedClasses,
  [CommunityTypes.SAVE_STASH_SELECTED_CLASSES]: saveStashSelectedClasses,
  [CommunityTypes.ADD_SELECTED_CLASSES]: addSelectedClasses,
  [CommunityTypes.REMOVE_SELECTED_CLASSES]: removeSelectedClasses,
  [CommunityTypes.ADD_STASH_SELECTED_CLASSES]: addStashSelectedClasses,
  [CommunityTypes.REMOVE_STASH_SELECTED_CLASSES]: removeStashSelectedClasses,
  [CommunityTypes.CLEAN_SELECTED_CLASSES]: cleanSelectedClasses,
  [CommunityTypes.GET_USER_DETAIL_SUCCESS]: getUserDetailSuccess,
  [CommunityTypes.ADD_FRIEND_SUCCESS]: addFriendSuccess,
  [CommunityTypes.CREATE_EVENT_SUCCESS]: createEventSuccess,
  [CommunityTypes.GET_SEARCH_EVENTS_SUCCESS]: getSearchEventsSuccess,
  [CommunityTypes.GET_TRAINING_EVENT_SUCCESS]: getTrainingEventSuccess,
  [CommunityTypes.SET_COMMUNITY_CLASS_BOOKMARK_SUCCESS]: setCommunityClassBookmarkSuccess,
  [CommunityTypes.REMOVE_COMMUNITY_CLASS_BOOKMARK_SUCCESS]: removeCommunityClassBookmarkSuccess,
  [CommunityTypes.SEARCH_EVENT_FRIEND_SUCCESS]: searchEventFriendSuccess,
  [CommunityTypes.SET_EVENT_CLASS_DETAIL]: setEventClassDetail,
  [CommunityTypes.UPDATE_EVENT_CLASS_DETAIL]: updateEventClassDetail,
  [CommunityTypes.GET_EVENT_LEADER_BOARD_SUCCESS]: getEventLeaderBoardSuccess,
  [CommunityTypes.JOIN_EVENT_SUCCESS]: joinEventSuccess,
  [CommunityTypes.LEAVE_EVENT_SUCCESS]: leaveEventSuccess,
  [CommunityTypes.RESET_EVENT_DATA]: resetEventData,
  [CommunityTypes.UPDATE1ON1_ONLINE_STATUS]: update1on1OnlineStatus,
  [CommunityTypes.UPDATE1ON1_START_TIME]: update1on1StartTime,
});
