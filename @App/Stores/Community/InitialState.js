/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  list: [],
  detail: {},
  userEventList: [],
  allEventList: [],
  allMyEventList: [],
  allEventsList: [],
  friends: [],
  friendSelected: [],
  coverImages: [],
  classesSelected: [],
  stashClassesSelected: [],
  userDetail: {},
  events: [],
  eventDetail: {},
  eventFriends: [],
  eventClass: {},
  leaderBoard: [],

  isReady: false,
  classStartAt: null,
};
