/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { createReducer } from 'reduxsauce';
import { INITIAL_STATE } from './InitialState';
import { ActionTypes } from './Actions';

export const setSkipBulletinVersion = (state, { version }) => ({
  ...state,
  skippedVersion: version,
});

export const getBulletinSuccess = (state, { version, content }) => ({
  ...state,
  version: version ? version : state.version,
  content: content ? content : state.content,
});

export const getBulletinVersion = (state, { version, content }) => ({
  ...state,
});
export const reducer = createReducer(INITIAL_STATE, {
  [ActionTypes.GET_BULLETIN_VERSION]: getBulletinVersion,
  [ActionTypes.GET_BULLETIN_SUCCESS]: getBulletinSuccess,
  [ActionTypes.SET_SKIP_BULLETIN_VERSION]: setSkipBulletinVersion,
});
