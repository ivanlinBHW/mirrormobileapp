import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getBulletinVersion: null,
  getBulletinContent: null,
  getBulletinSuccess: { version: undefined, content: undefined },

  setSkipBulletinVersion: ['version'],
});

export const ActionTypes = Types;

export default Creators;
