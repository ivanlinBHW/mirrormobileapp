/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  skippedVersion: undefined,

  content: undefined,
  version: undefined,
};
