/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { ClassTypes } from './Actions';
import { LiveTypes } from 'App/Stores/Live/Actions';
import { ProgramTypes } from 'App/Stores/Program/Actions';

export const getClassListSuccess = (state, { list }) => ({
  ...state,
  list: {
    bookedCount: list.bookedCount,
    bookmarkCount: list.bookmarkCount,
    hottest: list.currentHottest,
    workoutType: [],
    newArrival: list.newArrival,
    recommended: list.recommended,
    upcomingLive: list.upcomingLive,

    bookmarked: list.bookmarked.liveClasses.concat(list.bookmarked.trainingClasses),
    bookedLive: list.bookedLive,
  },
});

export const getClassDetailSuccess = (state, { item }) => ({
  ...state,
  detail: {
    ...item,
    trainingClassHistory: _.isEmpty(item.trainingClassHistories)
      ? null
      : { ...item.trainingClassHistories[0] },
  },
});

export const resetClassDetail = (state) => ({
  ...state,
  detail: {},
});

export const startClassSuccess = (state, { payload }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClassHistory: {
        ...payload,
      },
      trainingProgramClassHistory: _.has(payload, 'programClassHistory')
        ? payload.programClassHistory
        : {
            ...state.detail.programClassHistory,
          },
    },
  };
};

export const updateEventClassDetail = (state, { data }) => {
  return {
    ...state,
    eventClass: {
      ...state.eventClass,
      trainingEventClassHistory: _.has(data, 'eventClassHistory')
        ? data.eventClassHistory
        : {
            ...state.eventClass.eventClassHistory,
          },
      trainingClassHistory: _.has(data, 'trainingClassHistory')
        ? data.trainingClassHistory
        : {
            ...state.eventClass.trainingClassHistory,
          },
    },
  };
};

export const resetClassPauseStep = (state) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      pausedStep: null,
    },
  };
};

export const setTrainingClassHistory = (state, { item }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClassHistory: item,
    },
  };
};

export const setProgramClassHistory = (state, { item }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      programClassHistory: item,
    },
  };
};

export const setDetailTrainingProgram = (
  state,
  { trainingClassHistory, programClassHistory },
) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClassHistory,
      programClassHistory,
    },
  };
};

export const setClassBookmarkSuccess = (state, { id }) => {
  let temp = JSON.parse(JSON.stringify(state.list));
  let tempList = Object.keys(temp);
  tempList.forEach((key) => {
    if (temp[key] instanceof Array) {
      temp[key].forEach((item) => {
        if (key !== 'bookmarked') {
          if (item.id === id) {
            item.isBookmarked = true;
            let inBookmarked = false;
            temp.bookmarked.forEach((data) => {
              if (data.id === id) {
                inBookmarked = true;
              }
            });
            if (!inBookmarked) {
              temp.bookmarked = [item].concat(temp.bookmarked);
              inBookmarked = false;
            } else {
              inBookmarked = false;
            }
          }
        }
      });
    }
  });
  temp.bookmarkCount = temp.bookmarkCount > 0 ? temp.bookmarkCount + 1 : 0;
  return {
    ...state,
    list: temp,
  };
};

export const removeClassBookmarkSuccess = (state, { id }) => {
  let temp = JSON.parse(JSON.stringify(state.list));
  let tempList = Object.keys(temp);
  tempList.forEach((key) => {
    if (temp[key] instanceof Array) {
      temp[key].forEach((item, index) => {
        if (item.id === id) {
          item.isBookmarked = false;
          temp.bookmarked.forEach((data, num) => {
            if (data.id === id) {
              temp.bookmarked.splice(num, 1);
            }
          });
        }
      });
    }
  });
  temp.bookmarkCount =
    typeof temp.bookmarkCount === 'number' && temp.bookmarkCount > 0
      ? temp.bookmarkCount - 1
      : 0;
  return {
    ...state,
    list: temp,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [ClassTypes.GET_CLASS_LIST_SUCCESS]: getClassListSuccess,
  [ClassTypes.GET_CLASS_DETAIL_SUCCESS]: getClassDetailSuccess,
  [ClassTypes.START_CLASS_SUCCESS]: startClassSuccess,
  [LiveTypes.BOOK_LIVE_SUCCESS]: getClassDetailSuccess,
  [ClassTypes.RESET_CLASS_DETAIL]: resetClassDetail,
  [ClassTypes.RESET_CLASS_PAUSE_STEP]: resetClassPauseStep,
  [ProgramTypes.SET_DETAIL_TRAINING_PROGRAM]: setDetailTrainingProgram,
  [ClassTypes.SET_TRAINING_CLASS_HISTORY]: setTrainingClassHistory,
  [ProgramTypes.SET_PROGRAM_CLASS_HISTORY]: setProgramClassHistory,
  [ClassTypes.SET_CLASS_BOOKMARK_SUCCESS]: setClassBookmarkSuccess,
  [ClassTypes.REMOVE_CLASS_BOOKMARK_SUCCESS]: removeClassBookmarkSuccess,
});
