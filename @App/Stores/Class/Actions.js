import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getClassList: ['params'],
  getClassDetail: ['id', 'token', 'routeName'],
  exitDistanceMeasuringModal: ['id'],
  onlyGetClassDetail: ['id', 'routeName'],
  onlyGetClassHistoryDetail: ['id', 'historyId'],
  onlyClassSummaryGetClassDetail: ['id', 'routeName', 'historyId'],
  getClassListSuccess: ['list'],
  getClassDetailSuccess: ['item'],
  resetClassDetail: null,
  submitFeedback: [
    'id',
    'token',
    'payload',
    'routeName',
    'isProgramFinished',
    'detailId',
    'programId',
  ],
  startClass: ['id', 'token', 'routeName', 'options'],
  resumeClass: ['id', 'token', 'detailId', 'options'],
  pauseClass: ['id', 'token', 'stepId', 'detailId'],
  finishClass: ['id', 'token'],
  startClassSuccess: ['payload'],
  stashClassStepId: ['id', 'stepId'],
  resetClassPauseStep: null,
  setTrainingClassHistory: ['item'],

  setClassBookmark: ['id'],
  setClassBookmarkSuccess: ['id'],
  removeClassBookmark: ['id'],
  removeClassBookmarkSuccess: ['id'],
  setClassLiveBookmark: ['id'],
  setClassLiveBookmarkSuccess: ['id'],
  removeClassLiveBookmark: ['id'],
  removeClassLiveBookmarkSuccess: ['id'],
  sendClassReaction: ['id', 'reaction'],

  isDoLastEvent: ['id', 'routeName'],
});

export const ClassTypes = Types;
export default Creators;
