/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { SpotifyTypes } from './Actions';
import { Date as D } from 'App/Helpers';

const TAG = 'SPOTIFY: ';

const onChangeSettings = (
  state,
  action,
) => {
  return {
    ...state,
    clientID: action.id,
    scopes: action.scopes,
    clientSecret: action.secret,
    redirectUrl: action.redirectUrl,
    tokenSwapUrl: action.tokenSwapUrl,
    tokenRefreshUrl: action.tokenRefreshUrl,
  };
};

const onSpotifyLogin = (state, action) => {
  return {
    ...state,
    currentSession: action.session,
    isLogin: true,
  };
};

const onSpotifyLogout = (state, action) => {
  return {
    ...state,
    currentSession: {},
    playlists: [],
    isLogin: false,
  };
};

const onSpotifyUpdatePlayLists = (state, action) => {
  return {
    ...state,
    playlists: action.data,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [SpotifyTypes.ON_SPOTIFY_LOGIN]: onSpotifyLogin,
  [SpotifyTypes.ON_SPOTIFY_LOGOUT]: onSpotifyLogout,
  [SpotifyTypes.ON_CHANGE_SETTINGS]: onChangeSettings,
  [SpotifyTypes.ON_SPOTIFY_UPDATE_PLAY_LISTS]: onSpotifyUpdatePlayLists,

  [SpotifyTypes.ON_SPOTIFY_LOG]: (state, action) => {
    return {
      ...state,
      logs: [`${D.transformDate()} - ${action.message}`, ...state.logs],
    };
  },
  [SpotifyTypes.ON_SPOTIFY_LOG_ERROR]: (state, { error }) => {
    console.error(`${TAG} error=>`, error);
    return {
      ...state,
      logs: [
        D.transformDate() + ' - ERROR: ' + error.message + ' (' + error.status + ')',
        ,
        ...state.logs,
      ],
    };
  },
  [SpotifyTypes.ON_SPOTIFY_CLEAN_LOGS]: (state) => ({
    ...state,
    logs: [],
  }),
});
