import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  onChangeSettings: {
    id: '',
    scopes: [],
    secret: '',
    redirectUrl: '',
    tokenSwapUrl: '',
    tokenRefreshUrl: '',
  },
  onSpotifyLogin: ['session'],
  onSpotifyLogout: ['session'],
  onSpotifyUpdatePlayLists: ['data'],

  onSpotifyLog: ['message'],
  onSpotifyLogError: ['error'],
  onSpotifyCleanLogs: null,
});

export const SpotifyTypes = Types;
export default Creators;
