/**
 * The initial values for the redux state.
 */
import { Config } from 'App/Config';

export const INITIAL_STATE = {
  clientID: Config.SPOTIFY_CLIENT_ID,
  clientSecret: Config.SPOTIFY_CLIENT_SECRET,
  scopes: Config.SPOTIFY_SCOPES || [
    'streaming',
    'user-read-private',
    'playlist-read',
    'playlist-read-private',
  ],
  redirectUrl: Config.SPOTIFY_REDIRECT_URL,
  tokenSwapUrl: Config.SPOTIFY_TOKEN_SWAP_URL,
  tokenRefreshUrl: Config.SPOTIFY_TOKEN_REFRESH_URL,
  tokenRefreshEarliness: Config.SPOTIFY_TOKEN_REFRESH_EARLINESS || 300,
  sessionUserDefaultsKey: Config.SPOTIFY_SESSION_USER_DEFAULTS_KEY || '_SpotifySession_',
  currentSession: {},
  playlists: [],
  isLogin: false,
  logs: [],
};
