/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { uniqBy, uniq, has } from 'lodash';
import { createReducer } from 'reduxsauce';
import { INITIAL_STATE } from './InitialState';
import { MirrorTypes as Types, MirrorOptions, MirrorEvents } from './Actions';
import { ClassTypes } from 'App/Stores/Class/Actions';
import { Date as d } from 'App/Helpers';

export const getDisplayOptionSuccess = (state, action) => {
  const displayOptions = Object.assign({}, state.displayOptions);
  const slideShowOptions = Object.assign({}, state.slideShowOptions);

  action.payload.options &&
    action.payload.options.forEach((item) => {
      displayOptions[item.optionName] = item.value;
      if (item.optionName === MirrorOptions.DISPLAY_SLIDE_SHOW) {
        if (typeof item[MirrorOptions.SLIDE_SHOW_USB_NAME] === 'string') {
          slideShowOptions[MirrorOptions.SLIDE_SHOW_USB_NAME] =
            item[MirrorOptions.SLIDE_SHOW_USB_NAME];
        }
        if (typeof item[MirrorOptions.SLIDE_SHOW_FOLDER_LIST] === 'object') {
          slideShowOptions[MirrorOptions.SLIDE_SHOW_FOLDER_LIST] =
            item[MirrorOptions.SLIDE_SHOW_FOLDER_LIST];
        }
      }
    });
  return {
    ...state,
    displayOptions,
    slideShowOptions,
  };
};

export const setDisplayOption = (state, { optionName, value }) => {
  if (optionName) {
    return {
      ...state,
      displayOptions: {
        ...state.displayOptions,
        [optionName]: value,
      },
    };
  }
  return {
    ...state,
  };
};

export const onMirrorGetAudioSettingSuccess = (state, action) => {
  const options = state.options;
  action.options.forEach((item) => {
    options[item.optionName] = item.value;
  });
  return {
    ...state,
    options,
  };
};

export const handleSettingSuccess = (state, action) => {
  const options = {};
  action.options.forEach((item) => {
    options[item.optionName] = item.value;
  });
  return {
    ...state,
    options,
  };
};

export const onMirrorChangeAudioVolumeSuccess = (state, action) => {
  return {
    ...state,
    options: {
      ...state.options,
      [action.volumeType]: action.volume,
    },
  };
};

export const handleReset = (state) => {
  return Object.assign({}, INITIAL_STATE);
};
export const reducer = createReducer(INITIAL_STATE, {
  [Types.UPDATE_MIRROR_STORE]: (state, action) => ({
    ...state,
    ...action.data,
  }),
  [Types.ON_MIRROR_CONNECTED]: (state, action) => ({
    ...state,
    lastDevice: action.device,
    connectedDevice: action.device,
    isPaired: true,
    isConnected: true,
    lastDeviceConnectedAt: new Date(),
  }),
  [Types.ON_MIRROR_PRE_CONNECTED]: (state, action) => ({
    ...state,
    connectedDevice: action.device,
    isConnected: true,
  }),
  [Types.ON_MIRROR_DISCONNECT]: (state, action) => ({
    ...state,
    isWaitingForDeviceStartClass: false,
    connectedDevice: null,
    isConnected: false,
  }),
  [Types.ON_MIRROR_DISCONNECT_BY_USER]: (state, action) => ({
    ...state,
    isWaitingForDeviceStartClass: false,
    connectedDevice: null,
    isConnected: false,
    lastDevice: null,
    isPaired: false,
    lastDeviceConnectedAt: null,
  }),
  [Types.ON_MIRROR_DISCOVER]: (state, action) => ({
    ...state,
    isScanning: true,
  }),
  [Types.ON_MIRROR_DISCOVERED]: (state, action) => ({
    ...state,
    discoveredDevice: action.device,
    isScanning: false,
  }),
  [Types.ON_MIRROR_GET_WIFI_LIST_SUCCESS]: (state, action) => ({
    ...state,
    discoveredSsid: action.data,
  }),
  [Types.ON_MIRROR_CLEAR_WIFI_LIST]: (state, action) => ({
    ...state,
    discoveredSsid: [],
  }),
  [Types.ON_MIRROR_GET_AUDIO_SETTING_SUCCESS]: onMirrorGetAudioSettingSuccess,
  [Types.ON_MIRROR_CHANGE_AUDIO_VOLUME_SUCCESS]: onMirrorChangeAudioVolumeSuccess,
  [Types.ON_MIRROR_RESPONSE_BLUETOOTH_AUDIO_DEVICES]: (state, action) => ({
    ...state,
    outputPairedDevices: action.pairedDevices,
    outputOtherDevices: action.otherDevices,
  }),
  [Types.ON_MIRROR_OUTPUT_AUDIO_DEVICE]: (state, action) => ({
    ...state,
    options: {
      ...state.options,
      [MirrorOptions.OUTPUT_AUDIO_DEVICE]: action.target.type,
      [MirrorOptions.OUTPUT_AUDIO_DEVICE_DATA]: action.target.device,
    },
    outputPairedDevices: has(action, 'target.device.deviceId')
      ? uniqBy([...state.outputPairedDevices, action.target.device], 'deviceId')
      : state.outputPairedDevices,
    outputOtherDevices: has(action, 'target.device.deviceId')
      ? uniq(
          state.outputOtherDevices
            .filter((e) => e.deviceId !== action.target.device.deviceId),
        )
      : state.outputOtherDevices,
  }),
  [Types.ON_MIRROR_OUTPUT_AUDIO_DEVICE_SUCCESS]: (state, action) => {
    console.log('ON_MIRROR_OUTPUT_AUDIO_DEVICE_SUCCESS', action.wsData.type);
    return {
      ...state,
      options: {
        ...state.options,
        [MirrorOptions.OUTPUT_AUDIO_DEVICE]: action.wsData.type,
        connectBluetoothSpeakerSuccess:
          action.wsData.type == 'bluetooth-speaker' ? true : false,
      },
    };
  },
  [Types.UPDATE_CONNECT_BLUETOOTH_SPEAKER_SUCCESS]: (
    state,
    { connectBluetoothSpeakerSuccess },
  ) => {
    console.log(
      'UPDATE_CONNECT_BLUETOOTHSPEAKER_SUCCESS',
      connectBluetoothSpeakerSuccess,
    );
    return {
      ...state,
      options: {
        ...state.options,
        connectBluetoothSpeakerSuccess,
      },
    };
  },
  [Types.SET_VIDEO_SIZE]: (state, { data }) => {
    return {
      ...state,
      options: {
        ...state.options,
        [MirrorOptions.VIDEO_SIZE]: data,
      },
    };
  },
  [Types.SET_LOCATION]: (state, { data }) => ({
    ...state,
    displayOptions: {
      ...state.displayOptions,
      [MirrorOptions.DISPLAY_LOCATION]: data,
    },
  }),
  [Types.ON_MIRROR_UPDATE_MUSIC_PLAY_SETTING_SUCCESS]: (state) => ({
    ...state,
    shuffle: !state.shuffle,
  }),
  [Types.ON_MIRROR_PLAYING_CLASS]: (state, action) => {
    console.log('action =>', action);
    if (action.data) {
      return {
        ...state,
        currentClassId: action.data.classId,
        currentDuration: parseInt(action.data.currentPosition, 10),
        currentStepId: action.data.stepId,
        temp: action,
      };
    }
    return {
      ...state,
      temp: action,
    };
  },
  [Types.RESET_CURRENT_POSITION]: (state) => ({
    ...state,
    currentDuration: 0,
  }),
  [Types.SET_NEXT_CURRENT_POSITION]: (state) => ({
    ...state,
    currentDuration: state.currentDuration + 1,
  }),
  [Types.RESET_CURRENT_STEP]: (state) => ({
    ...state,
    currentStepId: '',
  }),
  [Types.SET_CURRENT_POSITION]: (state, { item }) => ({
    ...state,
    currentDuration: item,
  }),
  [Types.SET_CURRENT_PLAYER_STATE]: (state, { item }) => ({
    ...state,
    playerState: item,
  }),
  [Types.RESET_THUMBNAIL]: (state) => ({
    ...state,
    thumbnail: null,
    captureThumbnails: null,
  }),
  [Types.SET_ACTIVE_EVENT]: (state, { item }) => ({
    ...state,
    activeEvent: item,
  }),
  [Types.ON_MIRROR_PLAYER_STATUS]: (state, action) => ({
    ...state,
    currentState: action.state,
  }),
  [Types.ON_MIRROR_PLAYER_CONTROL_SUCCESS]: (state, action) => ({
    ...state,
    currentAction: action.action,
  }),
  [Types.ON_MIRROR_TAKE_PICTURE_SUCCESS]: (state, action) => ({
    ...state,
    thumbnail: action.image,
  }),
  [Types.ON_MIRROR_PICTURE_PREVIEW]: (state, action) => ({
    ...state,
    thumbnail: action.thumbnail,
  }),
  [Types.ON_MIRROR_GET_MOTION_CAPTURE_PREVIEW_SUCCESS]: (state, action) => ({
    ...state,
    captureThumbnails: action.thumbnail,
  }),
  [Types.CHANGE_PLAYLIST_SUCCESS]: (state, action) => {
    console.log('action =>', action);
    return {
      ...state,
      activeList: action,
    };
  },
  [Types.SET_ACTIVE_LIST]: (state, { data }) => {
    return {
      ...state,
      activeList: data,
    };
  },
  [Types.SET_ACTIVE_LIST_TYPE]: (state, { data }) => {
    return {
      ...state,
      activeList: {
        ...state.activeList,
        data: {
          ...state.activeList.data,
          type: data,
        },
      },
    };
  },
  [Types.ON_MIRROR_SET_PICTURE_SUCCESS]: (state, action) => ({
    ...state,
    thumbnail: `data:image/jpeg;base64,${action.thumbnail.thumbnail}`,
  }),
  [ClassTypes.SUBMIT_FEEDBACK]: (state, action) => ({
    ...state,
    thumbnail: null,
  }),
  [Types.ON_MIRROR_SET_WORKOUT_SETTING_SUCCESS]: (state, action) => {
    return {
      ...state,
      options: {
        ...state.options,
        [action.options.optionName]: !state.options[action.options.optionName],
      },
    };
  },
  [Types.ON_MIRROR_LOG]: (state, action) => {
    return {
      ...state,
      logs: [
        `${d.formatDate(new Date(), 'MM/DD-HH:mm')}: ${action.message}`,
        ...state.logs,
      ],
    };
  },
  [Types.ON_MIRROR_LOG_CLEAR]: (state, action) => ({
    ...state,
    logs: [],
  }),
  [Types.ON_MIRROR_SETUP_SSID]: (state, { ssid, password }) => ({
    ...state,
    connectedWifi: {
      ssid,
      password,
    },
  }),
  [Types.RESET_ACTIVITY_LIST]: (state) => ({
    ...state,
    activeList: {
      data: '',
    },
  }),
  [Types.RESET_CURRENT_ACTION]: (state) => ({
    ...state,
    currentAction: '',
  }),
  [Types.SET_CURRENT_STEP_ID]: (state, { id }) => ({
    ...state,
    currentStepId: id,
  }),
  [Types.ON_MIRROR_GET_WORKOUT_SETTING_SUCCESS]: handleSettingSuccess,
  [Types.ON_MIRROR_RESET]: handleReset,
  [Types.ON_MIRROR_PAIR_HEART_RATE_DEVICE_BY_PHONE]: (state, action) => {
    if (!action.device) {
      return {
        ...state,
        blePairedHeartRateDevice: {},
        blePairedDevices: [],
      };
    }
    return {
      ...state,
      blePairedHeartRateDevice: action.device,
      blePairedDevices: uniqBy([...state.blePairedDevices, action.device], 'id'),
    };
  },
  [Types.GET_DISPLAY_OPTIONS_SUCCESS]: getDisplayOptionSuccess,
  [Types.SET_DISPLAY_OPTION]: setDisplayOption,
  [Types.ON_MIRROR_CHANGE_AUDIO_OUTPUT_DEVICE]: (state, action) => ({
    ...state,
    activeBtAudioDevice: action.device,
  }),
  [Types.UPDATE_TRAINING_RECORDS]: (state, action) => ({
    ...state,
    trainingRecords: [...state.trainingRecords, action.data],
  }),

  [Types.ON_MIRROR_COMMUNICATE]: (state, { event, data }) => {
    switch (event) {
      case MirrorEvents.CHANGE_PLAYLIST: {
        return {
          ...state,
          activeList: {
            ...state.activeList,
            data: {
              ...state.activeList.data,
              type: data.type,
            },
          },
        };
      }

      case MirrorEvents.START_CLASS: {
        return {
          ...state,
          isWaitingForDeviceStartClass: true,
        };
      }

      case MirrorEvents.EXCEPTION:
      case MirrorEvents.EXIT_CLASS:
      case MirrorEvents.CLASS_PREVIEW:
      case MirrorEvents.EXIT_CLASS_PREVIEW:
      case MirrorEvents.DEVICE_LOGIN: {
        return {
          ...state,
          isWaitingForDeviceStartClass: false,
        };
      }

      default: {
        return state;
      }
    }
  },

  [Types.GET_DEVICE_INFO_SUCCESS]: (state, { data }) => {
    return {
      ...state,
      ...data,
    };
  },
});
