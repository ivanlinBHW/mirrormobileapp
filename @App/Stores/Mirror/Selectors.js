import { get } from 'lodash';

export const filterMirrorService = (state) =>
  state.mdns.services.filter((e) => {
    if (state.mirror.lastDevice) {
      return (
        e.name.toLowerCase().startsWith('mirror-') &&
        e.host !== state.mirror.lastDevice.host &&
        !e.host.startsWith(':')
      );
    }
    return (
      e.name && e.name.toLowerCase().startsWith('mirror-') && !e.host.startsWith(':')
    );
  });

export const filterInitializedMirrorService = (state, isFilterLastDevice = true) => {
  return state.mdns.services.filter((e) => {
    if (isFilterLastDevice && state.mirror.lastDevice) {
      const isMirror =
        e.name.toLowerCase().startsWith('mirror-') &&
        e.host !== state.mirror.lastDevice.host;
      return isMirror && (e.txt && e.txt.initialized && JSON.parse(e.txt.initialized));
    }
    return (
      e.name &&
      e.name.toLowerCase().startsWith('mirror-') &&
      (e.txt && e.txt.initialized && JSON.parse(e.txt.initialized))
    );
  });
};

export const filterLastRecordMirrorService = (state) =>
  state.mdns.services.find((e) => {
    if (state.mirror.lastDevice) {
      return e.host === state.mirror.lastDevice.host;
    }
    return null;
  });

export const findMirrorService = (state) =>
  state.mdns.services.find((e) => e.name.toLowerCase().startsWith('mirror-'));

export const filterNotPairedHeartRateDevices = (state) => {
  const pairedDevice = state.bleDevice.activeDevice;

  if (pairedDevice) {
    return state.bleDevice.searchDevices.filter((e) => e && e.id !== pairedDevice.id);
  }
  return state.bleDevice.searchDevices;
};

export const filterMirrorHasFeatureOption = (state, featureKey) => {
  const { featureOptions = [] } = state.mirror;

  const option = (featureOptions || []).find((e) => e.optionName === featureKey);

  if (featureOptions && option) {
    return option.value;
  }
  return false;
};

export const filterMirrorHasFeatureOptionData = (state, featureKey) => {
  const { featureOptions = [] } = state.mirror;

  const option = (featureOptions || []).find((e) => e.optionName === featureKey);

  if (featureOptions && option) {
    return option.data;
  }
  return false;
};
