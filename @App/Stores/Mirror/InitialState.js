import { MirrorOptions } from './Actions';
export const INITIAL_STATE = {
  isConnected: false,
  isConnecting: false,
  isScanning: false,
  isPaired: false,
  isPlayingClass: false,
  isSameStepChange: false,
  isDiffStepChange: false,
  isStepUpdateDelay: false,
  connectSuccessResult: {},
  connectBluetoothSpeakerSuccess: false,
  deviceAppVersion: '',
  deviceHardwareVersion: '',
  currentClassType: '',

  lastDevice: null,
  lastDeviceConnectedAt: null,
  connectedDevice: null,
  discoveredDevice: null,
  discoveredSsid: [],
  connectedSsidName: null,
  blePairedDevices: [],
  bleOtherDevices: [],
  blePairedHeartRateDevice: {},
  acitiveBle: {},
  activeBtAudioDevice: {},
  shuffle: false,
  currentStepId: '',
  currentDuration: 0,
  currentState: '',
  currentClassId: '',
  currentAction: '',
  activeList: {
    data: {
      type: 'mirror-music',
    },
  },
  temp: null,
  thumbnail: null,
  captureThumbnails: null,
  preRecordedNextScreen: null,
  preRecordedTrainingEventId: null,
  preRecordedFriendId: null,
  outputPairedDevices: [],
  outputOtherDevices: [],

  connectedWifi: {
    ssid: '',
    password: '',
  },

  musicSetting: {
    type: '',
  },

  options: {
    [MirrorOptions.VIDEO_SIZE]: 'large',
    [MirrorOptions.HEART_RATE_DISPLAY]: false,
    [MirrorOptions.CALORIES_DISPLAY]: false,
    [MirrorOptions.EXERCISE_NAME_DISPLAY]: false,
    [MirrorOptions.TIMERS_DISPLAY]: false,
    [MirrorOptions.COMMUNITY_DISPLAY]: false,
    [MirrorOptions.MOTION_TRACKER]: false,
    [MirrorOptions.MUSIC_VOLUME]: 0,
    [MirrorOptions.INSTROCTOR_VOLUME]: 0,
    [MirrorOptions.OUTPUT_AUDIO_DEVICE]: 'mirror-speaker',
    [MirrorOptions.OUTPUT_AUDIO_DEVICE_DATA]: {
      deviceName: 'bluetooth headset 02',
      deviceId: 'device-bt-02',
    },
  },

  displayOptions: {
    [MirrorOptions.DISPLAY_STANDBY_MODE_DISPLAY]: true,
    [MirrorOptions.DISPLAY_TEMPERATURE]: true,
    [MirrorOptions.DISPLAY_AIR_QUALITY]: true,
    [MirrorOptions.DISPLAY_SLIDE_SHOW]: true,
    [MirrorOptions.DISPLAY_HUMIDITY]: true,
    [MirrorOptions.DISPLAY_TIME]: true,
    [MirrorOptions.DISPLAY_WEATHER_ICON]: true,
    [MirrorOptions.DISPLAY_LOCATION]: '----------',
  },

  slideShowOptions: {
    [MirrorOptions.SLIDE_SHOW_USB_NAME]: 'Johndoe',
    [MirrorOptions.SLIDE_SHOW_FOLDER_LIST]: [
      { name: 'folder1', selected: true },
      { name: 'folder2', selected: false },
      { name: 'folder3', selected: false },
      { name: 'folder4', selected: false },
    ],
  },

  logs: [],
  activeEvent: 'play',
  trainingRecords: [],
  isWaitingForDeviceStartClass: false,
  isCasting: false,
  featureOptions: [
    {
      optionName: 'partnerWorkout',
      value: false,
    },
    {
      optionName: 'coachVI',
      value: false,
    },
    {
      optionName: 'distanceAdjustment',
      value: false,
    },
    {
      optionName: 'mirrorCamera',
      value: false,
    },
    {
      optionName: 'screenCasting',
      value: false,
    },
    {
      optionName: 'freeTrial',
      value: true,
      data: {
        ids: [],
      },
    },
  ],
};
