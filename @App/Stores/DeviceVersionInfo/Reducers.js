/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { DeviceVersionInfoTypes } from './Actions';

export const getIndexSuccess = (state, { items }) => ({
  ...state,
  items,
});

export const updateSkippedVersion = (state, { skippedVersion }) => {
  console.log('====updateSkippedVersion====', skippedVersion);
  return {
    ...state,
    skippedVersion,
  };
};

export const setVersionUpdateFinished = (state, { isFinished }) => {
  console.log('====updateSkippedVersion====', isFinished);
  return {
    ...state,
    updateFinished: isFinished,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [DeviceVersionInfoTypes.GET_INDEX_SUCCESS]: getIndexSuccess,
  [DeviceVersionInfoTypes.UPDATE_SKIPPED_VERSION]: updateSkippedVersion,
  [DeviceVersionInfoTypes.SET_VERSION_UPDATE_FINISHED]: setVersionUpdateFinished,
});
