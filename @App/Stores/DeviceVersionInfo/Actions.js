import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  indexSaga: null,
  getIndexSuccess: ['items'],
  updateSkippedVersion: ['skippedVersion'],
  setVersionUpdateFinished: ['isFinished'],
});

export const DeviceVersionInfoTypes = Types;
export default Creators;
