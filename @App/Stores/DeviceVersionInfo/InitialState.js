/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  items: [],
  skippedVersion: '0.0.0',
  updateFinished: false,
};
