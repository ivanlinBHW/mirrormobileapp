import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  track: {
    trackObjectType: undefined,
    trackObjectId: undefined,
    trackActionType: undefined,
  },
});

export const ActionTypes = Types;

export default Creators;
