/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { ProgramTypes } from './Actions';
import { UserTypes } from '../User/Actions';

export const getProgramListSuccess = (state, { list }) => ({
  ...state,
  list: list,
});

export const getProgramDetailSuccess = (state, { item }) => ({
  ...state,
  detail: item,
});

export const finishProgramClassSuccess = (state, { data }) => ({
  ...state,
  isProgramFinished: data.isProgramFinished,
});

export const resetIsProgramFinished = (state) => {
  return {
    ...state,
    isProgramFinished: false,
  };
};

export const setProgramClassHistory = (state, { item }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      programClassHistory: item,
    },
  };
};

export const setProgramBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.detail.weekSchedules)) {
    temp = JSON.parse(JSON.stringify(state.detail.weekSchedules));
  }
  temp.forEach((item) => {
    item.forEach((data) => {
      if (data.id === id) {
        data.isBookmarked = true;
      }
    });
  });

  return {
    ...state,
    detail: {
      ...state.detail,
      weekSchedules: temp,
    },
  };
};

export const removeProgramBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.detail.weekSchedules)) {
    temp = JSON.parse(JSON.stringify(state.detail.weekSchedules));
  }
  temp.forEach((item) => {
    item.forEach((data) => {
      if (data.id === id) {
        data.isBookmarked = false;
      }
    });
  });

  return {
    ...state,
    detail: {
      ...state.detail,
      weekSchedules: temp,
    },
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [UserTypes.ON_USER_RESET]: () => INITIAL_STATE,
  [ProgramTypes.GET_PROGRAM_LIST_SUCCESS]: getProgramListSuccess,
  [ProgramTypes.GET_PROGRAM_DETAIL_SUCCESS]: getProgramDetailSuccess,
  [ProgramTypes.FINISH_PROGRAM_CLASS_SUCCESS]: finishProgramClassSuccess,
  [ProgramTypes.RESET_IS_FINISHED_PROGRAM]: resetIsProgramFinished,
  [ProgramTypes.SET_PROGRAM_CLASS_HISTORY]: setProgramClassHistory,
  [ProgramTypes.SET_PROGRAM_BOOKMARK_SUCCESS]: setProgramBookmarkSuccess,
  [ProgramTypes.REMOVE_PROGRAM_BOOKMARK_SUCCESS]: removeProgramBookmarkSuccess,
});
