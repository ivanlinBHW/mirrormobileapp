import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getProgramList: ['token'],
  getProgramDetail: ['id', 'token', 'showDetail'],
  onlyGetProgramDetail: ['id', 'token'],
  getProgramListSuccess: ['list'],
  getProgramDetailSuccess: ['item'],
  getProgramClassDetail: [
    'id',
    'token',
    'trainingProgramClassHistory',
    'isFinished',
    'historyId',
    'routeName',
  ],
  onlyGetProgramClassDetail: ['id', 'routeName', 'trainingProgramClassHistory'],
  onlyClassSummaryGetProgramClassDetail: [
    'id',
    'routeName',
    'trainingProgramClassHistory',
  ],
  finishProgramClass: ['id', 'token', 'body', 'activeTrainingProgramId'],
  finishProgramClassSuccess: ['data'],
  startProgram: ['id', 'token'],
  leaveProgram: ['id', 'token'],
  resetIsFinishedProgram: null,
  resumeProgramClass: ['id', 'token', 'routeName', 'detailId'],
  pauseProgramClass: [
    'id',
    'token',
    'stepId',
    'trainingProgramScheduleId',
    'isProgramFinished',
    'detailId',
  ],
  startProgramClass: ['id', 'token', 'detailId', 'options'],
  setDetailTrainingProgram: ['trainingClassHistory', 'programClassHistory'],
  stashProgramStepId: ['trainingProgramClassHistoryId', 'stepId'],
  setProgramClassHistory: ['item'],

  setProgramBookmark: ['id'],
  setProgramBookmarkSuccess: ['id'],
  removeProgramBookmark: ['id'],
  removeProgramBookmarkSuccess: ['id'],
  programSubmitFeedback: ['id', 'payload', 'isProgramFinished'],
});

export const ProgramTypes = Types;
export default Creators;
