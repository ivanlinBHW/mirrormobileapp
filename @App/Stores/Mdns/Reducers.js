/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { uniqBy } from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { MdnsTypes as Types } from './Actions';
export const reducer = createReducer(INITIAL_STATE, {
  [Types.ON_MDNS_LOG]: (state, action) => ({
    ...state,
    logs: [action.message, ...state.logs],
  }),
  [Types.ON_MDNS_LOG_CLEAR]: (state) => ({
    ...state,
    logs: [],
  }),
  [Types.ON_MDNS_SCAN]: (state) => ({
    ...state,
    services: [],
    error: null,
  }),
  [Types.ON_MDNS_SCAN_STARTED]: (state) => ({
    ...state,
    isScanning: true,
    services: [],
    error: null,
    logs: [`Scanning...`, ...state.logs],
  }),
  [Types.ON_MDNS_SCAN_STOPPED]: (state) => ({
    ...state,
    isScanning: false,
    logs: [`Scan stopped.`, ...state.logs],
  }),
  [Types.ON_MDNS_SCAN_RESOLVED]: (state, action) => ({
    ...state,
    services: uniqBy([action.service, ...state.services], 'fullName'),
    logs: [
      `Service found: ${action.service.host}, initialized: ${
        action.service.txt ? action.service.txt.initialized : null
      }, wifiSignalLevel: ${
        action.service.txt ? action.service.txt.wifiSignalLevel : null
      }`,
      ...state.logs,
    ],
  }),
  [Types.ON_MDNS_SCAN_ERROR]: (state, action) => ({
    ...state,
    error: action.error,
    isScanning: false,
    logs: [`Scan Error: ${action.error}`, ...state.logs],
  }),
  [Types.ON_MDNS_SCAN_CLEAR]: (state) => ({
    ...state,
    services: [],
    logs: [`Services cleared.`, ...state.logs],
  }),
});
