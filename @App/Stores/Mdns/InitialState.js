/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  logs: [],
  services: [],
  isScanning: false,
  error: null,
};
