import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  onMdnsInit: null,
  onMdnsScan: ['uriPrefix', 'protocol', 'domain'],
  onMdnsStop: null,
  onMdnsEnd: null,
  onMdnsScanClear: null,
  onMdnsScanStarted: null,
  onMdnsScanStopped: null,
  onMdnsLog: ['message'],
  onMdnsLogClear: ['message'],
  onMdnsScanError: ['error'],
  onMdnsScanResolved: ['service'],
});

export const MdnsTypes = Types;
export default Creators;
