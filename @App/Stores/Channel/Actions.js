import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getChannelList: null,
  getChannelListSuccess: ['data'],
  getChannelDetail: ['channelId', 'toDetailPage'],
  getChannelDetailSuccess: ['data'],
  setChannelId: ['id'],
  getChannelSeeAll: ['channelId', 'genreId'],

  setChannelBookmark: ['groupId', 'id'],
  setChannelBookmarkSuccess: ['groupId', 'id'],

  delChannelBookmark: ['groupId', 'id'],
  delChannelBookmarkSuccess: ['groupId', 'id'],
});

export const ChannelTypes = Types;
export default Creators;
