/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  channelList: [],
  channelDetail: [],
  channelId: '',
};
