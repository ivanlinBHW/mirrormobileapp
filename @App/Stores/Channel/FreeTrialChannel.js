const FREE_TRIAL_CHANNEL = {
  id: 'FreeTrial',
  title: 'FreeTrial',
  code: 'FTC01',
  isFreeTrial: true,
  createdAt: '2021-04-01 10:43:15.463178',
  updatedAt: '2021-04-01 10:43:15.463178',
  deletedAt: null,
  ids: [],
};

export default FREE_TRIAL_CHANNEL;
