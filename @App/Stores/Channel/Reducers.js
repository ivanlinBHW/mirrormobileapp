/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { isArray, get } from 'lodash';
import { createReducer } from 'reduxsauce';

import FreeTrialChannel from './FreeTrialChannel';
import { ChannelTypes } from './Actions';
import { INITIAL_STATE } from './InitialState';
import { MirrorTypes } from '../Mirror/Actions';
import { UserTypes } from '../User/Actions';

export const getChannelListSuccess = (state, { data }) => {
  return {
    ...state,
    channelList: data,
  };
};

export const getChannelDetailSuccess = (state, { data }) => ({
  ...state,
  channelDetail: data,
});

export const setChannelId = (state, { id }) => ({
  ...state,
  channelId: id,
});

export const setChannelBookmarkSuccess = (state, { groupId, id }) => {
  return {
    ...state,
    channelDetail: state.channelDetail.map((g) => {
      if (g.group.id === groupId) {
        return {
          ...g,
          trainingClasses: g.trainingClasses.map((e) => ({
            ...e,
            isBookmarked: e.id === id ? true : e.isBookmarked,
          })),
        };
      }
      return g;
    }),
  };
};

export const delChannelBookmarkSuccess = (state, { groupId, id }) => {
  return {
    ...state,
    channelDetail: state.channelDetail.map((g) => {
      if (g.group.id === groupId) {
        return {
          ...g,
          trainingClasses: g.trainingClasses.map((e) => ({
            ...e,
            isBookmarked: e.id === id ? false : e.isBookmarked,
          })),
        };
      }
      return g;
    }),
  };
};

const getDeviceInfoSuccess = (state, { data }) => {
  if (data.featureOptions && isArray(data.featureOptions)) {
    const freeTrial = data.featureOptions.find((e) => e.optionName === 'freeTrial');

    if (freeTrial && freeTrial.value && !state.channelList.some((e) => e.isFreeTrial)) {
      FreeTrialChannel.ids = get(freeTrial, 'data.ids', get(freeTrial, 'data', []));
      return {
        ...state,
        channelList: [FreeTrialChannel, ...state.channelList],
      };
    }
  }
  return {
    ...state,
  };
};

export const removeFreeTrail = (state, action) => {
  const channelList = state.channelList.filter((e) => e.isFreeTrial);
  console.log('removeFreeTrail channelList=>', channelList);
  return {
    ...state,
    channelList,
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [ChannelTypes.SET_CHANNEL_BOOKMARK_SUCCESS]: setChannelBookmarkSuccess,
  [ChannelTypes.DEL_CHANNEL_BOOKMARK_SUCCESS]: delChannelBookmarkSuccess,
  [ChannelTypes.GET_CHANNEL_LIST_SUCCESS]: getChannelListSuccess,
  [ChannelTypes.GET_CHANNEL_DETAIL_SUCCESS]: getChannelDetailSuccess,
  [ChannelTypes.SET_CHANNEL_ID]: setChannelId,
  [MirrorTypes.GET_DEVICE_INFO_SUCCESS]: getDeviceInfoSuccess,
  [MirrorTypes.ON_MIRROR_DISCONNECT]: removeFreeTrail,
  [MirrorTypes.ON_MIRROR_DISCONNECT_BY_USER]: removeFreeTrail,
  [UserTypes.ON_USER_LOGOUT]: removeFreeTrail,
  [UserTypes.ON_USER_RESET]: removeFreeTrail,
});
