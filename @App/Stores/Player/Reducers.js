/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { PlayerTypes } from './Actions';
import { ClassTypes } from 'App/Stores/Class/Actions';
import { LiveTypes } from 'App/Stores/Live/Actions';
import { CollectionTypes } from 'App/Stores/Collection/Actions';
import { ProgramTypes } from 'App/Stores/Program/Actions';
import { CommunityTypes } from 'App/Stores/Community/Actions';
import { MirrorOptions } from 'App/Stores/Mirror/Actions';

export const getDetailSuccess = (state, { item }) => ({
  ...state,
  detail: {
    ...item,
    trainingClassHistory: _.isEmpty(item.trainingClassHistories)
      ? null
      : { ...item.trainingClassHistories[0] },
  },
});

export const setRouteName = (state, { routeName }) => {
  return {
    ...state,
    routeName: routeName,
  };
};

export const startClassSuccess = (state, { payload }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClassHistory: _.has(payload, 'trainingClassHistory')
        ? payload.trainingClassHistory
        : {
            ...state.detail.trainingClassHistory,
          },
      trainingProgramClassHistory: _.has(payload, 'programClassHistory')
        ? payload.programClassHistory
        : {
            ...state.detail.programClassHistory,
          },
    },
  };
};

export const setDetailTrainingProgram = (
  state,
  { trainingClassHistory, programClassHistory },
) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClassHistory,
      programClassHistory,
    },
  };
};

export const setTrainingClassHistory = (state, { item }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingClassHistory: item,
    },
  };
};

export const setLiveClassHistory = (state, { payload }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      liveClassHistory: {
        ...payload,
      },
    },
  };
};

export const setEventClassDetail = (state, { data }) => {
  return {
    ...state,
    detail: data,
  };
};

export const updateEventClassDetail = (state, { data }) => {
  console.log('eventClassHistory data =>', data);
  return {
    ...state,
    detail: {
      ...state.detail,
      trainingEventClassHistory: _.has(data, 'eventClassHistory')
        ? data.eventClassHistory
        : {
            ...state.detail.eventClassHistory,
          },
      trainingClassHistory: _.has(data, 'trainingClassHistory')
        ? data.trainingClassHistory
        : {
            ...state.detail.trainingClassHistory,
          },
    },
  };
};

export const setProgramClassHistory = (state, { item }) => {
  return {
    ...state,
    detail: {
      ...state.detail,
      programClassHistory: item,
    },
  };
};

export const updatePlayerStore = (state, { data }) => {
  return {
    ...state,
    ...data,
  };
};

export const updateBookmarkStatus = (value) => (state, { id }) => {
  const { detail } = state;
  if (detail.id === id) {
    detail.isBookmarked = value;
  }
  console.log('detail.id=>', detail.id);
  console.log('id=>', id);
  console.log('value=>', value);
  console.log('detail.isBookmarked=>', detail.isBookmarked);
  return {
    ...state,
    detail: { ...detail },
  };
};
export const reducer = createReducer(INITIAL_STATE, {
  [PlayerTypes.UPDATE_PLAYER_STORE]: updatePlayerStore,
  [LiveTypes.GET_LIVE_DETAIL_SUCCESS]: getDetailSuccess,
  [ClassTypes.START_CLASS_SUCCESS]: startClassSuccess,
  [ProgramTypes.SET_PROGRAM_CLASS_HISTORY]: setProgramClassHistory,
  [PlayerTypes.SET_ROUTE_NAME]: setRouteName,
  [ProgramTypes.SET_DETAIL_TRAINING_PROGRAM]: setDetailTrainingProgram,
  [ClassTypes.SET_TRAINING_CLASS_HISTORY]: setTrainingClassHistory,
  [LiveTypes.SET_LIVE_CLASS_HISTORY]: setLiveClassHistory,
  [CommunityTypes.SET_EVENT_CLASS_DETAIL]: setEventClassDetail,
  [CommunityTypes.UPDATE_EVENT_CLASS_DETAIL]: updateEventClassDetail,
  [ClassTypes.GET_CLASS_DETAIL_SUCCESS]: getDetailSuccess,

  [ClassTypes.REMOVE_CLASS_BOOKMARK_SUCCESS]: updateBookmarkStatus(false),
  [ClassTypes.SET_CLASS_BOOKMARK_SUCCESS]: updateBookmarkStatus(true),
  [CommunityTypes.REMOVE_COMMUNITY_CLASS_BOOKMARK_SUCCESS]: updateBookmarkStatus(false),
  [CommunityTypes.SET_COMMUNITY_CLASS_BOOKMARK_SUCCESS]: updateBookmarkStatus(true),
  [CollectionTypes.SET_COLLECTION_BOOKMARK_SUCCESS]: updateBookmarkStatus(true),
  [CollectionTypes.REMOVE_COLLECTION_BOOKMARK_SUCCESS]: updateBookmarkStatus(false),
  [ProgramTypes.SET_PROGRAM_BOOKMARK_SUCCESS]: updateBookmarkStatus(true),
  [ProgramTypes.REMOVE_PROGRAM_BOOKMARK_SUCCESS]: updateBookmarkStatus(false),
});
