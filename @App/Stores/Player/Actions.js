import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getLiveList: ['token'],
  setRouteName: ['routeName'],
  updatePlayerStore: ['data'],
});

export const PlayerTypes = Types;
export default Creators;
