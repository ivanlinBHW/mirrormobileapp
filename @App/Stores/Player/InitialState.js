/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  detail: {},
  routeName: '',
  isMotionCaptureEnabled: false,
};
