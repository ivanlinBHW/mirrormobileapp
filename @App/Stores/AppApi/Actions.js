import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  onApiFetching: ['method', 'url', 'options'],

  onApiFetchSuccess: ['data'],
  onApiFetchFailure: ['error'],
});

export const AppApiTypes = Types;

export default Creators;
