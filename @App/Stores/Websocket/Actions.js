import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  initConnection: ['url', 'protocols', 'options'],
  closeConnection: ['code', 'reason'],

  onWsSendMessage: ['payload'],
  onWsMessageReceived: ['message'],
  onWsError: ['error'],
  onWsOpened: ['device'],
  onWsClosed: ['event'],
  onWsLog: ['message'],
  onWsLogClear: null,
  onWsUpdateRetryCount: ['retryCount'],
  onWsConnect: ['url', 'protocols', 'options'],
  onWsConnectSuccess: ['device'],
  onWsDisconnect: null,
  onWsCommunicate: ['event', 'message'],
});

export const WebsocketTypes = Types;

export const ConnectionState = {
  CONNECTING: 'CONNECTING',
  DISCOVERING: 'DISCOVERING',
  CONNECTED: 'CONNECTED',
  DISCONNECTED: 'DISCONNECTED',
  DISCONNECTING: 'DISCONNECTING',
};

export default Creators;
