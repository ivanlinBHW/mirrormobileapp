/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import JSON from 'circular-json';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { WebsocketTypes, ConnectionState } from './Actions';

export const isJSON = (str) => {
  const { isError, attempt } = require('lodash');
  return !isError(attempt(JSON.parse, str));
};

export const onSendMessage = (state, { payload }) => ({
  ...state,
  lastSentData: payload,
  logs: [
    `Sending: ${typeof payload === 'object' ? JSON.stringify(payload) : payload}`,
    ...state.logs,
  ],
});

export const onMessageReceived = (state, { message }) => ({
  ...state,
  lastReceivedMessage: isJSON(message) ? JSON.parse(message) : message,
  logs: [`Received: ${message}`, ...state.logs],
});

export const onConnecting = (state, action) => ({
  ...state,
  connectionState: ConnectionState.CONNECTING,
  logs: [`Connecting to "${action.url}"...`, ...state.logs],
});

export const onSocketOpened = (state) => ({
  ...state,
  isConnected: true,
  connectionState: ConnectionState.CONNECTED,
  error: null,
  logs: ['Socket opened.', ...state.logs],
});

export const onDisconnecting = (state) => ({
  ...state,
  connectionState: ConnectionState.DISCONNECTING,
  isConnected: false,
  logs: ['Socket disconnecting', ...state.logs],
});

export const onSocketClosed = (state) => ({
  ...state,
  connectionState: ConnectionState.DISCONNECTED,
  isConnected: false,
  logs: ['Socket closed', ...state.logs],
});

export const onErrorThrown = (state, { error }) => ({
  ...state,
  isConnected: false,
  error,
  logs: [
    `Error happens: ${typeof error === 'object' ? JSON.stringify(error) : error}`,
    ...state.logs,
  ],
});

export const onUpdateRetryCount = (state, { retryCount }) => ({
  ...state,
  retryCount,
  logs: [`Reconnecting... (${retryCount})`, ...state.logs],
});

export const onWsLog = (state, { message }) => {
  return {
    ...state,
    logs: [message, ...state.logs],
  };
};

export const onWsLogClear = (state) => ({
  ...state,
  logs: [],
});
export const reducer = createReducer(INITIAL_STATE, {
  [WebsocketTypes.ON_WS_SEND_MESSAGE]: onSendMessage,
  [WebsocketTypes.ON_WS_MESSAGE_RECEIVED]: onMessageReceived,
  [WebsocketTypes.ON_WS_CONNECT]: onConnecting,
  [WebsocketTypes.ON_WS_DISCONNECT]: onDisconnecting,
  [WebsocketTypes.ON_WS_OPENED]: onSocketOpened,
  [WebsocketTypes.ON_WS_CLOSED]: onSocketClosed,
  [WebsocketTypes.ON_WS_ERROR]: onErrorThrown,
  [WebsocketTypes.ON_WS_UPDATE_RETRY_COUNT]: onUpdateRetryCount,
  [WebsocketTypes.ON_WS_LOG]: onWsLog,
  [WebsocketTypes.ON_WS_LOG_CLEAR]: onWsLogClear,
});
