import { ConnectionState } from 'App/Stores/Websocket/Actions';
export const INITIAL_STATE = {
  retryCount: 0,
  isConnected: false,
  error: null,
  connectionState: ConnectionState.DISCONNECTED,
  lastSentData: {},
  lastReceivedMessage: {},
  logs: [],
};
