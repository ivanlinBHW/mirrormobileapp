import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getLiveList: ['token'],
  getLiveDetail: ['id'],
  onlyGetLiveDetail: ['id', 'token'],
  onlyClassSummaryGetLiveDetail: ['id', 'historyId'],
  onlyPlayingClassGetLiveDetail: ['id'],
  getLiveListSuccess: ['list'],
  getLiveDetailSuccess: ['item'],
  getLiveDate: ['params', 'token'],
  removeLiveList: null,
  getLiveDateSuccess: ['list', 'params'],
  getLiveDateDetail: ['day', 'currentTimeZone'],
  getLiveDateDetailSuccess: ['list'],
  joinLive: ['id', 'token'],
  onlyJoinLive: ['id', 'token'],
  bookLive: ['id', 'token'],
  finishLiveClass: ['id'],
  bookLiveSuccess: ['item'],
  submitLiveFeedback: ['id', 'token', 'payload'],
  setLiveClassHistory: ['payload'],

  setLiveBookmark: ['id'],
  setLiveBookmarkSuccess: ['id'],
  removeLiveBookmark: ['id'],
  removeLiveBookmarkSuccess: ['id'],

  setCountDown: ['duration'],
  exitLiveClass: ['params'],
  setCountDownSuccess: ['time'],
  resetCountDown: null,
  sendLiveReaction: ['id', 'reaction'],
});

export const LiveTypes = Types;
export default Creators;
