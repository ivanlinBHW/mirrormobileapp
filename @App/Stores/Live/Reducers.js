/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import _ from 'lodash';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { LiveTypes } from './Actions';

export const getLiveListSuccess = (state, { list }) => ({
  ...state,
  list,
});

export const getLiveDetailSuccess = (state, { item }) => ({
  ...state,
  detail: item,
});

export const getLiveDateSuccess = (state, { list, params }) => ({
  ...state,
  liveDate: list,
  start: params.start,
  end: params.end,
});

export const removeLiveList = (state, { list, params }) => ({
  ...state,
  list: [],
});

export const getLiveDateDetailSuccess = (state, { list }) => {
  const temp = [];
  Object.keys(list).forEach((item) => {
    temp.push({
      title: item,
      data: list[item],
    });
  });
  console.log('temp ->', temp);

  return {
    ...state,
    list: temp,
  };
};

export const setLiveClassHistory = (state, { payload }) => {
  console.log('payload =>', payload);
  return {
    ...state,
    detail: {
      ...state.detail,
      liveClassHistory: {
        ...payload,
      },
    },
  };
};

export const setLiveBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.list)) {
    temp = JSON.parse(JSON.stringify(state.list));
  }
  temp.forEach((items) => {
    items.data.forEach((item) => {
      if (item.id === id) {
        item.isBookmarked = true;
      }
    });
  });
  return {
    ...state,
    list: temp,
  };
};

export const removeLiveBookmarkSuccess = (state, { id }) => {
  let temp = [];
  if (!_.isEmpty(state.list)) {
    temp = JSON.parse(JSON.stringify(state.list));
  }
  temp.forEach((items) => {
    items.data.forEach((item) => {
      if (item.id === id) {
        item.isBookmarked = false;
      }
    });
  });
  return {
    ...state,
    list: temp,
  };
};

export const setCountDownSuccess = (state, { time }) => ({
  ...state,
  time,
});

export const resetCountDown = (state) => ({
  ...state,
  time: '',
});
export const reducer = createReducer(INITIAL_STATE, {
  [LiveTypes.REMOVE_LIVE_LIST]: removeLiveList,
  [LiveTypes.GET_LIVE_LIST_SUCCESS]: getLiveListSuccess,
  [LiveTypes.GET_LIVE_DETAIL_SUCCESS]: getLiveDetailSuccess,
  [LiveTypes.GET_LIVE_DATE_SUCCESS]: getLiveDateSuccess,
  [LiveTypes.GET_LIVE_DATE_DETAIL_SUCCESS]: getLiveDateDetailSuccess,
  [LiveTypes.SET_LIVE_CLASS_HISTORY]: setLiveClassHistory,
  [LiveTypes.SET_LIVE_BOOKMARK_SUCCESS]: setLiveBookmarkSuccess,
  [LiveTypes.REMOVE_LIVE_BOOKMARK_SUCCESS]: removeLiveBookmarkSuccess,
  [LiveTypes.SET_COUNT_DOWN_SUCCESS]: setCountDownSuccess,
  [LiveTypes.RESET_COUNT_DOWN]: resetCountDown,
});
