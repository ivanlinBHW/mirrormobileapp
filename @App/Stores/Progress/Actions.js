import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  getAchievements: null,
  getAchievementsSuccess: ['data'],

  getProgressDate: ['start', 'end'],
  getProgressDateSuccess: ['data'],

  getProgressOverview: ['start', 'end'],
  getProgressOverviewSuccess: ['data'],

  getProgressDetail: { token: '', id: '', workoutType: '' },
  getProgressDetailSuccess: ['data'],

  setWorkoutDetailBookmark: ['id', 'workoutType'],
  setWorkoutDetailBookmarkSuccess: ['data'],

  deleteWorkoutDetailBookmark: ['id', 'workoutType'],
  deleteWorkoutDetailBookmarkSuccess: ['data'],

  deleteClassHistory: { id: '', workoutType: '' },
  deleteClassHistorySuccess: ['id'],
});

export const ProgressTypes = Types;
export default Creators;
