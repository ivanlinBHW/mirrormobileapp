/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { ProgressTypes } from './Actions';

export const getAchievementsSuccess = (state, { data }) => ({
  ...state,
  allAchievements: data,
});

export const getProgressDateSuccess = (state, { data }) => ({
  ...state,
  historyDate: data,
});

export const getProgressDetailSuccess = (state, { data }) => ({
  ...state,
  detail: data,
});

export const getProgressOverviewSuccess = (state, { data }) => ({
  ...state,
  overview: data,
});

export const getProgressOverview = (state, { start, end }) => ({
  ...state,
  startDate: start,
  endDate: end,
});

export const changeWorkoutDetailBookmarkSuccess = (state, { data }) => ({
  ...state,
  detail: {
    ...state.detail,
    trainingClass: {
      ...state.detail.trainingClass,
      isBookmarked: !state.detail.trainingClass.isBookmarked,
    },
  },
});

export const deleteClassHistorySuccess = (state, { id }) => ({
  ...state,
  overview: {
    ...state.overview,
    activities: state.overview.activities.filter((a) => a.id !== id),
  },
});
export const reducer = createReducer(INITIAL_STATE, {
  [ProgressTypes.GET_ACHIEVEMENTS_SUCCESS]: getAchievementsSuccess,
  [ProgressTypes.GET_PROGRESS_DATE_SUCCESS]: getProgressDateSuccess,
  [ProgressTypes.GET_PROGRESS_DETAIL_SUCCESS]: getProgressDetailSuccess,
  [ProgressTypes.GET_PROGRESS_OVERVIEW_SUCCESS]: getProgressOverviewSuccess,
  [ProgressTypes.GET_PROGRESS_OVERVIEW]: getProgressOverview,
  [ProgressTypes.SET_WORKOUT_DETAIL_BOOKMARK_SUCCESS]: changeWorkoutDetailBookmarkSuccess,
  [ProgressTypes.DELETE_WORKOUT_DETAIL_BOOKMARK_SUCCESS]: changeWorkoutDetailBookmarkSuccess,
  [ProgressTypes.DELETE_CLASS_HISTORY_SUCCESS]: deleteClassHistorySuccess,
});
