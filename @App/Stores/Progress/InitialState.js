const date = [
  '2019-10-16',
  '2019-10-17',
  '2019-10-18',
  '2019-10-19',
  '2019-10-21',
  '2019-10-24',
  '2019-10-25',
  '2019-10-27',
  '2019-10-30',
];
export const INITIAL_STATE = {
  startDate: '',
  endDate: '',
  historyDate: [...date],
  allAchievements: [],
  overview: {
    summary: {},
    history: [1, 1, 1, 1, 1, 1, 1, 1],
    achievements: [],
  },
  detail: {
    id: null,
    startedAt: null,
    trainingClass: {},
    feedback: {},
  },
};
