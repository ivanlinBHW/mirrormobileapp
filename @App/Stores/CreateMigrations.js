import VersionNumber from 'react-native-version-number';
import { createMigrate } from 'redux-persist';
import { get } from 'lodash';

import { Handler, User } from 'App/Api';

export const persistVersion = parseInt(
  `${VersionNumber.appVersion.toString().replace(/\.*/gm, '')}${
    VersionNumber.buildVersion
  }`,
  10,
);

const migrations = {
  [persistVersion]: async (state) => {
    const token = get(state, 'user.token');
    if (token) {
      await Handler.post({ Authorization: token })(User.logout());
    }

    console.log('@persistVersion=>', persistVersion);

    return undefined;
  },
};

export const persistMigrate = createMigrate(migrations, { debug: __DEV__ });
