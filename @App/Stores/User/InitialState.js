/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {
  name: '',
  email: '',
  token: '',
  status: '',
  fcmToken: '',
  deviceId: '',
  errorCode: '',
  currentCountryCode: '',
  activeTrainingProgramId: '',
  trainingProgramClassHistoryId: '',
  activeTrainingProgramHistoryId: '',
  spotifyTokens: {},
  isFirstTimeUse: true,
  isNextShowDistance: true,
  notificationEnable: false,
  hasNotificationsPermission: false,
};
