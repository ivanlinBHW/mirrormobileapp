/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import FastImage from 'react-native-fast-image';
import { INITIAL_STATE } from './InitialState';
import { createReducer } from 'reduxsauce';
import { UserTypes } from './Actions';
import { SettingTypes } from '../Setting/Actions';

export const fetchUserLoading = (state) => ({
  ...state,
  userIsLoading: true,
  userErrorMessage: null,
});

export const updateUserStore = (state, action) => ({
  ...state,
  ...action.data,
});

export const fetchUserLoginSuccess = (state, { user }) => {
  let spotifyTokens = {};
  if (user.oauthAccounts) {
    spotifyTokens = user.oauthAccounts.spotify;
  }
  return {
    ...state,
    token: user.accessToken,
    userId: user.userId,
    deviceId: user.deviceId,
    spotifyTokens,
    activeTrainingProgramId: user.activeTrainingProgramId || '',
    activeTrainingProgramHistoryId: user.activeTrainingProgramHistoryId || '',
    userIsLoading: false,
    status: 'success',
  };
};
export const fetchUserRegisterSuccess = (state, { user }) => {
  return {
    ...state,
    ...user,
  };
};
export const sendForgotPasswordEmailSuccess = (state, { email }) => {
  return {
    ...state,
    resetPasswordEmail: email,
  };
};

export const sendEmailVerifySuccess = (
  state,
  { payload: { emailVerifyCountDownId } },
) => {
  console.log('=== emailVerifyCountDownId ===', emailVerifyCountDownId);
  return {
    ...state,
    emailVerifyCountDownId,
  };
};

export const resetProgram = (state) => {
  return {
    ...state,
    activeTrainingProgramId: '',
    activeTrainingProgramHistoryId: '',
    trainingProgramScheduleId: '',
  };
};

export const resetProgramId = (state) => {
  return {
    ...state,
    activeTrainingProgramId: '',
    activeTrainingProgramHistoryId: '',
  };
};

export const setProgramId = (state, { data }) => {
  return {
    ...state,
    activeTrainingProgramId: data.trainingProgramId,
    activeTrainingProgramHistoryId: data.id,
  };
};

export const settrainingProgramClassHistoryId = (
  state,
  { trainingProgramClassHistory },
) => {
  return {
    ...state,
    trainingProgramClassHistoryId: trainingProgramClassHistory,
  };
};

export const updateAvatarSuccess = (state, { payload }) => {
  FastImage.preload([
    {
      uri: payload,
    },
  ]);
  return {
    ...state,
    avatar: payload,
  };
};

export const getUserAccountSuccess = (state, { item }) => {
  const data = Object.assign(state, item);
  return data;
};

export const fetchUserLoginFail = (state, { errorMessage }) => ({
  ...state,
  userErrorMessage: errorMessage,
  userIsLoading: false,
  user: {},
  status: 'fail',
});

export const handleUserReset = (state) => Object.assign({}, INITIAL_STATE);

export const onUserRefreshToken = (state, { Authorization }) => {
  return {
    ...state,
    token: Authorization.split('Bearer ')[1],
  };
};

export const onUpdateSpotifyToken = (state, { session }) => {
  return {
    ...state,
    spotifyTokens: {
      ...state.spotifyTokens,
      expireTime: session.expireTime,
      accessToken: session.accessToken,
      refreshToken: session.refreshToken,
    },
  };
};

export const onResetSpotifyToken = (state, { session }) => {
  return {
    ...state,
    spotifyTokens: {},
  };
};

export const setErrorCode = (state, { item, errors = null }) => {
  return {
    ...state,
    errorCode: item,
    errors,
  };
};

export const resetErrorCode = (state) => {
  return {
    ...state,
    errorCode: null,
    errors: null,
  };
};
export const onFcmTokenUpdate = (state, action) => ({
  ...state,
  fcmToken: action.token,
});
export const reducer = createReducer(INITIAL_STATE, {
  [SettingTypes.UPDATE_AVATAR_SUCCESS]: updateAvatarSuccess,
  [UserTypes.FETCH_USER_LOGIN_SUCCESS]: fetchUserLoginSuccess,
  [UserTypes.FETCH_USER_REGISTER_SUCCESS]: fetchUserRegisterSuccess,
  [UserTypes.FETCH_USER_LOGIN_FAIL]: fetchUserLoginFail,
  [UserTypes.ON_USER_RESET]: handleUserReset,
  [UserTypes.RESET_PROGRAM_ID]: resetProgramId,
  [UserTypes.SET_PROGRAM_ID]: setProgramId,
  [UserTypes.SET_TRAINING_PROGRAM_CLASS_HISTORY_ID]: settrainingProgramClassHistoryId,
  [UserTypes.RESET_PROGRAM]: resetProgram,
  [UserTypes.ON_UPDATE_SPOTIFY_TOKEN]: onUpdateSpotifyToken,
  [UserTypes.ON_RESET_SPOTIFY_TOKEN]: onResetSpotifyToken,
  [UserTypes.GET_USER_ACCOUNT_SUCCESS]: getUserAccountSuccess,
  [UserTypes.ON_USER_REFRESH_TOKEN]: onUserRefreshToken,
  [UserTypes.SET_ERROR_CODE]: setErrorCode,
  [UserTypes.RESET_ERROR_CODE]: resetErrorCode,
  [UserTypes.ON_FCM_TOKEN_UPDATE]: onFcmTokenUpdate,
  [UserTypes.UPDATE_USER_STORE]: updateUserStore,
  [UserTypes.SEND_FORGOT_PASSWORD_EMAIL_SUCCESS]: sendForgotPasswordEmailSuccess,
  [UserTypes.SEND_EMAIL_VERIFY_SUCCESS]: sendEmailVerifySuccess,
});
