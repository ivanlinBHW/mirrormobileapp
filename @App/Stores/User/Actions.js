import { createActions } from 'reduxsauce';
const { Types, Creators } = createActions({
  onPassIntro: null,
  fetchUserLogin: ['payload', 'device'],
  updateNotificationPermission: ['payload', 'device'],
  fetchUserLoginSuccess: ['user'],
  fetchUserRegister: ['payload'],
  fetchUserTempRegister: ['payload'],
  fetchUserRegisterSuccess: ['user'],
  emailVerifyTimerSaga: ['email'],
  emailVerifySaga: ['email'],
  resetProgramId: null,
  fetchUserLoginFail: ['errorMessage'],
  setProgramId: ['data'],
  setTrainingProgramClassHistoryId: ['trainingProgramClassHistory'],

  onUserRefreshToken: ['Authorization'],
  onUserLogout: null,
  onUserReset: null,
  resetProgram: null,

  onResetSpotifyToken: null,
  onUpdateSpotifyToken: ['session'],

  getUserAccount: null,
  getUserAccountSuccess: ['item'],
  setErrorCode: ['item', 'errors'],
  resetErrorCode: null,

  onFcmTokenUpdate: ['token'],
  updateUserStore: ['data'],

  fetchPutSubscriptionCode: ['code', 'goto'],
  sendForgotPasswordEmail: ['email'],
  fetchAddMemberSubscriptionCode: null,
  fetchDelMemberSubscriptionCode: ['code'],
  fetchDelMemberSubscription: null,

  fetchPutNotificationEnable: ['isEnable'],

  fetchCheckServerRegion: null,
  sendForgotPasswordEmailSuccess: ['email'],
  resetPassword: ['currentPassword', 'newPassword', 'email', 'isLogin'],
  sendEmailVerifySuccess: ['payload'],
  sendEmailVerify: ['payload'],

  deleteAccountSaga: null,
});

export const UserTypes = Types;
export default Creators;
