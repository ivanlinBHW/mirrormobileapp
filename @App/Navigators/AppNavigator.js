import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { LoadingIndicator } from '@ublocks-react-native/component';
import { Actions, Router, Reducer } from 'react-native-router-flux';

import { AppStateActions } from 'App/Stores';
import {
  NotificationMonitor,
  AppBulletinMonitor,
  StatusBarMonitor,
  SpotifyMonitor,
  MirrorMonitor,
  AppMonitor,
} from 'App/Monitors';
import AppScenes from './AppScenes';

class AppNavigator extends React.Component {
  static propTypes = {
    onLoading: PropTypes.func.isRequired,
    loadingOptions: PropTypes.object,
    loadingMessage: PropTypes.string,
    isLoading: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    token: PropTypes.string,
    isPaired: PropTypes.bool.isRequired,
    routeName: PropTypes.string,
    routeParams: PropTypes.object,
  };

  static defaultProps = {
    loadingOptions: {},
    loadingMessage: '',
    routeName: '',
    routeParams: {},
    token: null,
  };

  constructor(props) {
    super(props);
    const { token } = props;

    this.state = {
      scenes: Actions.create(AppScenes(token)),
    };
  }

  onReducerCreate = (params) => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
      this.props.dispatch(action);
      return defaultReducer(state, action);
    };
  };

  onLoadingLongPress = () => {
    const { onLoading, isLoading } = this.props;

    onLoading(!isLoading);

    if (__DEV__ && global.canceler) {
      global.canceler.cancel(`force cancel request`);
    }
  };

  render() {
    const { isLoading, loadingMessage, loadingOptions, routeName } = this.props;
    const { scenes } = this.state;
    return (
      <AppMonitor>
        <AppBulletinMonitor />
        <MirrorMonitor>
          <NotificationMonitor />
          <StatusBarMonitor>
            <Router scenes={scenes} />
            <LoadingIndicator
              open={isLoading && !loadingOptions.hide && !routeName.includes('Modal')}
              onLongPress={this.onLoadingLongPress}
              text={loadingMessage}
              countdown={__DEV__}
              dismissDelay={500}
            />
            <SpotifyMonitor />
          </StatusBarMonitor>
        </MirrorMonitor>
      </AppMonitor>
    );
  }
}

export default connect(
  (state) => ({
    routeParams: state.appRoute.params,
    isLoading: state.appState.isLoading,
    loadingMessage: state.appState.loadingMessage,
    loadingOptions: state.appState.loadingOptions,
    token: state.user.token,
    routeName: state.appRoute.routeName,
    isPaired: state.mirror.isPaired,
  }),
  (dispatch) => {
    const actions = bindActionCreators(
      {
        onLoading: AppStateActions.onLoading,
      },
      dispatch,
    );
    return { ...actions, dispatch };
  },
)(AppNavigator);
