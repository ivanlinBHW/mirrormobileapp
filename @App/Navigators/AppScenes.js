import React from 'react';
import {
  ActionConst,
  Stack,
  Scene,
  Tabs,
  Modal,
  Overlay,
  Lightbox,
} from 'react-native-router-flux';
import {
  HomeTabBar,
  HomeNavbar,
  PrimaryNavbar,
  PrimaryTabBar,
  CommunityNavbar,
} from 'App/Components';
import SplashScreen from 'App/Containers/Splash/SplashScreen';
import IntroScreenTab from 'App/Containers/Introduction/IntroScreenTab';
import UserLoginScreen from 'App/Containers/UserLogin/UserLoginScreen';
import UserRegisterScreen from 'App/Containers/UserLogin/UserRegisterScreen';
import UserRegisterVerifyEmailScreen from 'App/Containers/UserLogin/UserRegisterVerifyEmailScreen';
import AddProfileScreen from 'App/Containers/MyProfile/AddProfileScreen';
import AddProfileDetailScreen from 'App/Containers/MyProfile/AddProfileDetailScreen';

import MirrorFirstSetupScreen from 'App/Containers/MirrorSetup/MirrorFirstSetupScreen';

import UserRegisterSuccessScreen from 'App/Containers/UserLogin/UserRegisterSuccessScreen';
import SendForgotMailScreen from 'App/Containers/ForgotPassword/SendForgotMailScreen';
import SendForgotMailSuccessScreen from 'App/Containers/ForgotPassword/SendForgotMailSuccessScreen';
import ResetPasswordScreen from 'App/Containers/ForgotPassword/ResetPasswordScreen';
import MirrorPairScreen from 'App/Containers/MirrorSetup/MirrorPairScreen';
import MirrorSetupWifiInitScreen from 'App/Containers/MirrorSetup/MirrorSetupWifiInitScreen';
import MirrorSetupScreen from 'App/Containers/MirrorSetup/MirrorSetupScreen';
import MirrorWifiInputScreen from 'App/Containers/MirrorSetup/MirrorWifiInputScreen';
import MirrorWifiHomeConnectScreen from 'App/Containers/MirrorSetup/MirrorWifiHomeConnectScreen';
import MirrorCloseHotspotScreen from 'App/Containers/MirrorSetup/MirrorCloseHotspotScreen';
import MirrorWifiMobileCheckScreen from 'App/Containers/MirrorSetup/MirrorWifiMobileCheckScreen';
import MirrorOpenHotspotScreen from 'App/Containers/MirrorSetup/MirrorOpenHotspotScreen';
import ClassScreen from 'App/Containers/Class/ClassScreen';
import ChannelScreen from 'App/Containers/Channel/ChannelScreen';
import SearchScreen from 'App/Containers/Search/SearchScreen';
import SeeAllScreen from 'App/Containers/SeeAllScreen/SeeAllScreen';
import ClassDetail from 'App/Containers/ClassDetail/ClassDetail';
import ChannelClassDetailScreen from 'App/Containers/ChannelClassDetail/ChannelClassDetailScreen';
import ChannelDetailScreen from 'App/Containers/ChannelDetail/ChannelDetailScreen';

import InstructorScreen from 'App/Containers/Instructor/InstructorScreen';
import ProgramScreen from 'App/Containers/Program/Program';
import ProgramClassDetailScreen from 'App/Containers/ProgramClassDetail/ProgramClassDetailScreen';
import ProgramDetail from 'App/Containers/ProgramDetail/ProgramDetail';
import ProgramPlayerScreen from 'App/Containers/Player/ProgramPlayerScreen';
import ProgramFeedbackScreen from 'App/Containers/ProgramFeedback/ProgramFeedbackScreen';
import LiveScreen from 'App/Containers/Live/LiveScreen';
import LiveDetail from 'App/Containers/LiveDetail/LiveDetail';
import CollectionScreen from 'App/Containers/Collection/CollectionScreen';
import CollectionDetail from 'App/Containers/CollectionDetail/CollectionDetail';
import CommunityScreen from 'App/Containers/Community/CommunityScreen';
import MyEventScreen from 'App/Containers/MyEvent/MyEventScreen';
import AllEventsScreen from 'App/Containers/AllEvents/AllEventsScreen';
import CreateEventScreen from 'App/Containers/Community/CreateEventScreen';
import FriendSelectScreen from 'App/Containers/FriendSelect/FriendSelectScreen';
import FriendDetailScreen from 'App/Containers/FriendDetail/FriendDetailScreen';
import FriendSearchScreen from 'App/Containers/FriendSearch/FriendSearchScreen';
import InviteFriendsScreen from 'App/Containers/CommunityDetail/InviteFriendsScreen';
import CommunityDetailClassDetailScreen from 'App/Containers/CommunityDetailClassDetail/CommunityDetailClassDetailScreen';
import LeaderBoardScreen from 'App/Containers/CommunityDetail/LeaderBoardScreen';
import ParticipantsScreen from 'App/Containers/CommunityDetail/ParticipantsScreen';
import CommunityDetailScreen from 'App/Containers/CommunityDetail/CommunityDetailScreen';
import EventSearchScreen from 'App/Containers/EventSearch/EventSearchScreen';
import CommunitySearchScreen from 'App/Containers/CommunitySearch/CommunitySearchScreen';
import ClassesSelectedScreen from 'App/Containers/ClassesSelected/ClassesSelectedScreen';
import ReserveNew1on1Screen from 'App/Containers/1on1/ReserveNew1on1Screen';
import Prepare1on1SessionScreen from 'App/Containers/1on1/Prepare1on1SessionScreen';
import SettingScreen from 'App/Containers/Setting/SettingScreen';
import TermScreen from 'App/Containers/Setting/TermScreen';
import PrivacyScreen from 'App/Containers/Setting/PrivacyScreen';
import AudioScreen from 'App/Containers/Setting/AudioScreen';
import DisplayScreen from 'App/Containers/Setting/DisplayScreen';
import SlideshowScreen from 'App/Containers/Setting/SlideshowScreen';
import WeatherScreen from 'App/Containers/Setting/WeatherScreen';
import MotionTrackerScreen from 'App/Containers/Setting/MotionTrackerScreen';
import MiiSettingScreen from 'App/Containers/Setting/MiiSettingScreen';
import MusicScreen from 'App/Containers/Setting/MusicScreen';
import HeartRateScreen from 'App/Containers/Setting/HeartRateScreen';
import PrivacySettingScreen from 'App/Containers/Setting/PrivacySettingScreen';
import AccountSettingScreen from 'App/Containers/Setting/AccountSettingScreen';
import AccountInfoScreen from 'App/Containers/Setting/AccountInfoScreen';
import FamilyMemberScreen from 'App/Containers/Setting/FamilyMemberScreen';
import AnotherPurchaseScreen from 'App/Containers/Setting/AnotherPurchaseScreen';
import MyProfileScreen from 'App/Containers/MyProfile/MyProfileScreen';
import EditProfileScreen from 'App/Containers/MyProfile/EditProfileScreen';
import AbilityScreen from 'App/Containers/MyProfile/AbilityScreen';
import GoalScreen from 'App/Containers/MyProfile/GoalScreen';
import InjuriesScreen from 'App/Containers/MyProfile/InjuriesScreen';
import ActivityScreen from 'App/Containers/MyProfile/ActivityScreen';
import EquipmentScreen from 'App/Containers/MyProfile/EquipmentScreen';
import ScheduleScreen from 'App/Containers/MyProfile/ScheduleScreen';
import NotificationScreen from 'App/Containers/Notification/NotificationScreen';
import DeveloperModeScreen from 'App/Containers/Setting/DeveloperModeScreen';
import EnterSubscriptionScreen from 'App/Containers/Setting/EnterSubscriptionScreen';
import NotificationSettingScreen from 'App/Containers/Setting/NotificationSettingScreen';
import CalendarView from 'App/Containers/Progress/CalendarView';
import ProgressScreen from 'App/Containers/Progress/ProgressScreen';
import WorkoutDetailScreen from 'App/Containers/WorkoutDetail/WorkoutDetailScreen';
import AchievementListScreen from 'App/Containers/Achievement/AchievementListScreen';
import AchievementScreen from 'App/Containers/Achievement/AchievementScreen';
import ProgramAchievementScreen from 'App/Containers/ProgramDetail/ProgramAchievementScreen';
import PlayerScreen from 'App/Containers/Player/PlayerScreen';
import LivePlayerScreen from 'App/Containers/Player/LivePlayerScreen';
import EventPlayerScreen from 'App/Containers/Player/EventPlayerScreen';
import FeedbackScreen from 'App/Containers/Feedback/FeedbackScreen';
import SaveActivityScreen from 'App/Containers/SaveActivity/SaveActivityScreen';
import SharePreview from 'App/Containers/SaveActivity/SharePreview';
import MirrorCamera from 'App/Containers/SaveActivity/MirrorCamera';
import PhotoPreview from 'App/Containers/SaveActivity/PhotoPreview';
import FcmDebugScreen from 'App/Containers/__Debug__/FcmDebugScreen';
import DebugTabBarIcon from 'App/Containers/__Debug__/DebugTabBarIcon';
import DebugMenuScreen from 'App/Containers/__Debug__/DebugMenuScreen';
import DebugIconScreen from 'App/Containers/__Debug__/DebugIconScreen';
import MdnsDebugScreen from 'App/Containers/__Debug__/MdnsDebugScreen';
import SpotifyDebugScreen from 'App/Containers/__Debug__/SpotifyDebugScreen';
import BleDeviceDebugScreen from 'App/Containers/__Debug__/BleDeviceDebugScreen';
import ShopScreen from 'App/Containers/Shop/ShopScreen';
import BulletinModel from 'App/Containers/ModalScreen/BulletinModel';
import HeartRateModal from 'App/Containers/ModalScreen/HeartRateModal';
import AudioModal from 'App/Containers/ModalScreen/AudioModal';
import DeviceModal from 'App/Containers/ModalScreen/DeviceModal';
import FilterDifficultyTagsModal from 'App/Containers/ModalScreen/FilterDifficultyTagsModal';
import FilterDurationTagsModal from 'App/Containers/ModalScreen/FilterDurationTagsModal';
import FilterEquipmentTagsModal from 'App/Containers/ModalScreen/FilterEquipmentTagsModal';
import FilterFilterTagsModal from 'App/Containers/ModalScreen/FilterFilterTagsModal';
import FilterPopularTagsModal from 'App/Containers/ModalScreen/FilterPopularTagsModal';
import FilterInstructorTagsModal from 'App/Containers/ModalScreen/FilterInstructorTagsModal';
import ReactionModal from 'App/Containers/ModalScreen/ReactionModal';
import WorkoutOptionModal from 'App/Containers/ModalScreen/WorkoutOptionModal';
import EventPolicyModel from 'App/Containers/ModalScreen/EventPolicyModel';
import EventPolicyChangeModel from 'App/Containers/ModalScreen/EventPolicyChangeModel';
import DistanceMeasuringModal from 'App/Containers/ModalScreen/DistanceMeasuringModal';
import ConnectFailureModal from 'App/Containers/ModalScreen/ConnectFailureModal';
import OfflineModal from 'App/Containers/ModalScreen/OfflineModal';
import WifiBarCodeScanScreen from 'App/Containers/ModalScreen/WifiBarCodeScanScreen';
import MirrorCastScreen from 'App/Containers/Setting/MirrorCastScreen';

export default function scenes(isAuth) {
  return (
    <Overlay>
      <Modal hideNavBar>
        <Lightbox>
          <Stack key="SCREEN" wrap={false}>
            {/* <Scene key="SplashScreen" component={SplashScreen} hideNavBar /> */}
            {/* Main Stack */}
            <Stack key="MAIN" wrap={false} hideNavBar>
              <Scene key="IntroScreen" component={IntroScreenTab} initial={!isAuth} />
              <Scene key="UserLoginScreen" component={UserLoginScreen} />
              <Scene key="UserRegisterScreen" component={UserRegisterScreen} />
              <Scene
                key="UserRegisterVerifyEmailScreen"
                component={UserRegisterVerifyEmailScreen}
              />
              <Scene key="AddProfileScreen" component={AddProfileScreen} />
              <Scene key="MirrorFirstSetupScreen" component={MirrorFirstSetupScreen} />
              <Scene key="MiiFirstSettingScreen" component={MiiSettingScreen} />
              <Scene key="AddProfileDetailScreen" component={AddProfileDetailScreen} />

              <Scene key="UserRegisterTermScreen" component={TermScreen} />
              <Scene key="UserRegisterPrivacyScreen" component={PrivacyScreen} />
              <Scene key="UserRegisterTermScreen" component={TermScreen} />
              <Scene key="SendForgotMailScreen" component={SendForgotMailScreen} />
              <Scene
                key="SendForgotMailSuccessScreen"
                component={SendForgotMailSuccessScreen}
              />
              <Scene key="ResetPasswordScreen" component={ResetPasswordScreen} />
              <Scene
                key="UserRegisterSuccessScreen"
                component={UserRegisterSuccessScreen}
                type={ActionConst.REPLACE}
                panHandlers={null}
              />
              <Scene key="MirrorSetupScreen" component={MirrorSetupScreen} />
              <Scene
                key="MirrorPairScreen"
                component={MirrorPairScreen}
                panHandlers={null}
              />
              <Scene key="MirrorWifiInputScreen" component={MirrorWifiInputScreen} />
              <Scene
                key="MirrorWifiHomeConnectScreen"
                component={MirrorWifiHomeConnectScreen}
              />
              <Scene
                key="MirrorCloseHotspotScreen"
                component={MirrorCloseHotspotScreen}
              />
              <Scene
                key="MirrorWifiMobileCheckScreen"
                component={MirrorWifiMobileCheckScreen}
              />
              <Scene key="MirrorOpenHotspotScreen" component={MirrorOpenHotspotScreen} />
              <Scene
                key="MirrorSetupWifiInitScreen"
                component={MirrorSetupWifiInitScreen}
              />
              <Tabs
                key="PrimaryTab"
                tabBarComponent={PrimaryTabBar}
                activeBackgroundColor="red"
                panHandlers={null}
                initial={isAuth}
                backToInitial
                lazy
              >
                <Stack
                  key="Home"
                  icon="home"
                  title="Home"
                  type={ActionConst.REPLACE}
                  navBar={HomeNavbar}
                  panHandlers={null}
                  wrap={false}
                  back={false}
                >
                  <Tabs
                    key="HomeTab"
                    tabBarComponent={HomeTabBar}
                    activeBackgroundColor="red"
                    tabBarPosition="top"
                    animationEnabled={false}
                    swipeEnabled={false}
                    wrap={false}
                    backToInitial
                    lazy
                  >
                    <Scene
                      key="ClassScreen"
                      title="Class"
                      component={ClassScreen}
                      type={ActionConst.REPLACE}
                      panHandlers={null}
                      back={false}
                      animationEnabled={false}
                    />
                    <Scene key="Program" title="Program" component={ProgramScreen} />
                    <Scene
                      key="Collection"
                      title="Collection"
                      component={CollectionScreen}
                    />
                    <Scene key="Live" component={LiveScreen} />
                    <Scene key="Channel" component={ChannelScreen} />
                  </Tabs>
                </Stack>

                <Scene
                  key="Progress"
                  icon="stream"
                  title="Progress"
                  component={ProgressScreen}
                  navBar={PrimaryNavbar}
                />

                <Stack
                  key="Community"
                  title="Community"
                  navBar={CommunityNavbar}
                >
                  <Scene
                    key="CommunityScreen"
                    component={CommunityScreen}
                    panHandlers={null}
                    back={false}
                    initial={true}
                  />
                  <Scene
                    key="CommunitySearchScreen"
                    component={CommunitySearchScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="CreateEventScreen"
                    component={CreateEventScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="ClassesSelectedScreen"
                    component={ClassesSelectedScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="FriendSearchScreen"
                    component={FriendSearchScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="InviteFriendsScreen"
                    component={InviteFriendsScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="CommunityDetailClassDetailScreen"
                    component={CommunityDetailClassDetailScreen}
                    panHandlers={null}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="LeaderBoardScreen"
                    component={LeaderBoardScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="ParticipantsScreen"
                    component={ParticipantsScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="CommunityDetailScreen"
                    component={CommunityDetailScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="AllEventsScreen"
                    component={AllEventsScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="EventSearchScreen"
                    component={EventSearchScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="FriendDetailScreen"
                    component={FriendDetailScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene key="MyEventScreen" component={MyEventScreen} hideNavBar />
                  <Scene
                    key="FriendSelectScreen"
                    component={FriendSelectScreen}
                    hideNavBar
                    hideTabBar
                  />
                  <Scene
                    key="Prepare1on1SessionScreen"
                    component={Prepare1on1SessionScreen}
                    hideNavBar
                    hideTabBar
                  />
                </Stack>
                <Scene
                  key="Shop"
                  icon="shopping-cart"
                  title="Shop"
                  component={ShopScreen}
                />
                <Stack key="SETTING" icon="cog" wrap={false} hideNavBar>
                  <Scene
                    key="SettingScreen"
                    icon="cog"
                    component={SettingScreen}
                    title="Setting"
                  />
                  <Scene key="MyProfileScreen" component={MyProfileScreen} hideTabBar />
                  <Scene
                    key="EditProfileScreen"
                    component={EditProfileScreen}
                    hideTabBar
                  />
                  <Scene key="AbilityScreen" component={AbilityScreen} />
                  <Scene key="MirrorCastScreen" component={MirrorCastScreen} />
                  <Scene key="DisplayScreen" component={DisplayScreen} />
                  <Scene key="SlideshowScreen" component={SlideshowScreen} />
                  <Scene key="WeatherScreen" component={WeatherScreen} />
                  <Scene key="MotionTrackerScreen" component={MotionTrackerScreen} />
                  <Scene
                    key="NotificationSettingScreen"
                    component={NotificationSettingScreen}
                  />
                  <Scene key="ResetPasswordScreen" component={ResetPasswordScreen} />
                  <Scene key="MiiSettingScreen" component={MiiSettingScreen} />
                  <Scene key="MusicScreen" component={MusicScreen} />
                  <Scene key="GoalScreen" component={GoalScreen} />
                  <Scene key="InjuriesScreen" component={InjuriesScreen} />
                  <Scene key="ActivityScreen" component={ActivityScreen} />
                  <Scene key="EquipmentScreen" component={EquipmentScreen} />
                  <Scene key="ScheduleScreen" component={ScheduleScreen} />
                  <Scene key="TermScreen" component={TermScreen} />
                  <Scene key="PrivacyScreen" component={PrivacyScreen} />
                  <Scene key="AudioScreen" component={AudioScreen} />
                  <Scene key="HeartRateScreen" component={HeartRateScreen} />
                  <Scene key="PrivacySettingScreen" component={PrivacySettingScreen} />
                  <Scene key="AccountSettingScreen" component={AccountSettingScreen} />
                  <Scene key="DeveloperModeScreen" component={DeveloperModeScreen} />
                </Stack>
              </Tabs>
              <Scene
                key="AccountInfoScreen"
                component={AccountInfoScreen}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="FamilyMemberScreen"
                component={FamilyMemberScreen}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="AnotherPurchaseScreen"
                component={AnotherPurchaseScreen}
                hideNavBar
                hideTabBar
              />
              <Scene key="EnterSubscriptionScreen" component={EnterSubscriptionScreen} />
              {/* <Scene key="TestScreen" cowmponent={TestScreen} hideNavBar initial /> */}
              {/* Home Detail */}
              <Scene key="SearchScreen" component={SearchScreen} hideNavBar />
              <Scene key="SeeAllScreen" component={SeeAllScreen} hideNavBar />
              <Scene
                key="ClassDetail"
                component={ClassDetail}
                panHandlers={null}
                hideNavBar
              />
              <Stack hideNavBar key="ProgramDetailStack">
                <Scene
                  key="ProgramClassDetailScreen"
                  component={ProgramClassDetailScreen}
                  hideNavBar
                />
                <Scene
                  key="ProgramPlayerScreen"
                  component={ProgramPlayerScreen}
                  panHandlers={null}
                />
                <Scene
                  key="ProgramSaveActivityScreen"
                  component={SaveActivityScreen}
                  panHandlers={null}
                />
                <Scene
                  key="ProgramFeedbackScreen"
                  component={ProgramFeedbackScreen}
                  panHandlers={null}
                />
                <Scene key="ProgramMirrorCamera" component={MirrorCamera} hideNavBar />
                <Scene key="ProgramPhotoPreview" component={PhotoPreview} hideNavBar />
                <Scene
                  key="ProgramSharePreview"
                  component={SharePreview}
                  panHandlers={null}
                  hideNavBar
                />
              </Stack>
              <Scene
                key="ChannelClassDetailScreen"
                component={ChannelClassDetailScreen}
                hideNavBar
              />
              <Scene
                key="ChannelDetailScreen"
                component={ChannelDetailScreen}
                panHandlers={null}
                hideNavBar
              />
              <Scene
                key="ProgramAchievementScreen"
                component={ProgramAchievementScreen}
                hideNavBar
              />
              <Scene key="InstructorScreen" component={InstructorScreen} hideNavBar />
              <Scene key="ProgramDetail" component={ProgramDetail} hideNavBar />
              <Scene key="CollectionDetail" component={CollectionDetail} hideNavBar />
              <Scene
                key="LiveDetail"
                component={LiveDetail}
                panHandlers={null}
                hideNavBar
              />
              <Scene
                key="SaveActivityScreen"
                component={SaveActivityScreen}
                panHandlers={null}
                hideNavBar
              />
              <Scene key="ReserveNew1on1Screen" component={ReserveNew1on1Screen} />
              <Scene key="MirrorCamera" component={MirrorCamera} hideNavBar />
              <Scene key="PhotoPreview" component={PhotoPreview} hideNavBar />
              <Scene
                key="FeedbackScreen"
                component={FeedbackScreen}
                panHandlers={null}
                hideNavBar
              />
              <Scene
                key="PlayerScreen"
                component={PlayerScreen}
                panHandlers={null}
                hideNavBar
              />
              <Scene
                key="EventPlayerScreen"
                component={EventPlayerScreen}
                panHandlers={null}
                hideNavBar
              />
              <Scene
                key="SharePreview"
                component={SharePreview}
                panHandlers={null}
                hideNavBar
              />
              <Scene key="LivePlayerScreen" component={LivePlayerScreen} hideNavBar />
              <Scene
                key="WorkoutDetailScreen"
                component={WorkoutDetailScreen}
                hideNavBar
              />
              <Scene key="AchievementListScreen" component={AchievementListScreen} />
              <Scene key="AchievementScreen" component={AchievementScreen} />
              {/* Home Detail */}
              <Scene key="NotificationScreen" component={NotificationScreen} hideNavBar />
              <Scene
                key="NotificationFriendDetailScreen"
                component={FriendDetailScreen}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="NotificationInviteFriendsScreen"
                component={InviteFriendsScreen}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="NotificationLeaderBoardScreen"
                component={LeaderBoardScreen}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="NotificationCommunityDetailClassDetailScreen"
                component={CommunityDetailClassDetailScreen}
                panHandlers={null}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="NotificationCommunityDetailScreen"
                component={CommunityDetailScreen}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="NotificationParticipantsScreen"
                component={ParticipantsScreen}
                hideNavBar
                hideTabBar
              />
              <Scene
                key="WifiBarCodeScanScreen"
                component={WifiBarCodeScanScreen}
                hideNavBar
              />
            </Stack>
          </Stack>
          {/* Debug Mode Stack */}
          {/* Lightbox */}
          <Scene key="AudioModal" component={AudioModal} />
          <Scene key="DeviceModal" component={DeviceModal} />
          <Scene key="FilterDifficultyTagsModal" component={FilterDifficultyTagsModal} />
          <Scene key="FilterDurationTagsModal" component={FilterDurationTagsModal} />
          <Scene key="FilterEquipmentTagsModal" component={FilterEquipmentTagsModal} />
          <Scene key="FilterPopularTagsModal" component={FilterPopularTagsModal} />
          <Scene key="FilterFilterTagsModal" component={FilterFilterTagsModal} />
          <Scene key="FilterInstructorTagsModal" component={FilterInstructorTagsModal} />
          <Scene key="OfflineModal" component={OfflineModal} panHandlers={null} />
          <Scene key="ReactionModal" component={ReactionModal} modal />
          <Scene key="HeartRateModal" component={HeartRateModal} modal />
          <Scene key="ConnectFailureModal" component={ConnectFailureModal} />
          <Scene key="WorkoutOptionModal" component={WorkoutOptionModal} modal />
          <Scene key="EventPolicyModel" component={EventPolicyModel} modal />
          <Scene key="EventPolicyChangeModel" component={EventPolicyChangeModel} modal />
          <Scene key="DistanceMeasuringModal" component={DistanceMeasuringModal} />
          <Scene key="BulletinModel" component={BulletinModel} />
          {/* Lightbox */}
        </Lightbox>
        {/* Modals */}
        {/* Debug Mode Stack */}
        <Stack key="DEBUG" title="DEBUG MODE" hideNavBar={false} wrap={false}>
          <Tabs key="DebugTab" wrap={false} hideNavBar lazy>
            <Scene
              tabBarLabel="Menu"
              key="DebugMenuScreen"
              iconName="tools"
              icon={DebugTabBarIcon}
              component={DebugMenuScreen}
            />
            <Scene
              tabBarLabel="Connect"
              key="MdnsDebugScreen"
              iconName="connectdevelop"
              icon={DebugTabBarIcon}
              component={MdnsDebugScreen}
            />
            <Scene
              tabBarLabel="BLE"
              key="BleDeviceDebugScreen"
              iconName="bluetooth"
              icon={DebugTabBarIcon}
              component={BleDeviceDebugScreen}
            />
            {/* <Scene
              tabBarLabel="Spotify"
              key="SpotifyDebugScreen"
              iconName="spotify"
              icon={DebugTabBarIcon}
              component={SpotifyDebugScreen}
            /> */}
            <Scene
              tabBarLabel="FCM"
              key="FcmDebugScreen"
              iconName="bell"
              icon={DebugTabBarIcon}
              component={FcmDebugScreen}
            />
          </Tabs>
        </Stack>

        {/* Modals */}
      </Modal>
      <Scene key="DebugIconScreen" component={DebugIconScreen} />
    </Overlay>
  );
}
