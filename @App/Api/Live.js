import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getLiveList: () => `${prefix}/live-class/index`,
  getLiveDetail: ({ id }) => `${prefix}/live-class/${id}`,
  getLiveHistoryDetail: ({ id, historyId }) =>
    `${prefix}/live-class/${id}?liveClassHistoryId=${historyId}`,
  getLiveDate: ({ params }) =>
    `${prefix}/live-class/status?start=${params.start}&end=${params.end}`,
  getLiveDateDetail: ({ start, end }) =>
    `${prefix}/live-class/day?start=${start}&end=${end}`,
  submitFeedback: ({ id }) => `${prefix}/live-class/history/${id}/feedback`,
  joinLive: ({ id }) => `${prefix}/live-class/${id}/join`,
  bookLive: ({ id }) => `${prefix}/live-class/${id}/book`,
  finishLive: ({ id }) => `${prefix}/live-class/history/${id}/finish`,

  setBookmark: ({ id }) => `${prefix}/live-class/${id}/bookmark`,
  deleteBookmark: ({ id }) => `${prefix}/live-class/${id}/bookmark`,
  sendReaction: ({ id }) => `${prefix}/live-class/${id}/reaction`,
  exitLiveClass: ({ id, historyId }) => `${prefix}/live-class/${id}/exit?liveClassHistoryId=${historyId}`,
};
