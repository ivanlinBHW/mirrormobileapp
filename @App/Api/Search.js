import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getChannels: () => `${prefix}/channel/index`,
  getPopularTags: () => `${prefix}/tags/popular`,
  getInstructorList: () => `${prefix}/instructor`,
  getEquipmentList: () => `${prefix}/training-equipment`,
  getWorkoutType: () => `${prefix}/genre`,
  search: (page, pageSize) => `${prefix}/search?page=${page}&pageSize=${pageSize}`,
  searchFilter: () => `${prefix}/search-filter`,
  communitySearch: (page = 1, pageSize = 10, isEvent = true) =>
    `${prefix}/search?page=${page}&pageSize=${pageSize}&isEvent=${isEvent}`,
  searchHotKeyword: () => `${prefix}/search-hotkeywords`,
};
