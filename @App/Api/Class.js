import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getClassList: (workoutTypeIds) => {
      return `${prefix}/home`;
  },
  getClassDetail: ({ id }) => `${prefix}/training-class/${id}`,
  startClass: ({ id }) => `${prefix}/training-class/${id}/start`,
  resumeClass: ({ id }) => `${prefix}/training-class/history/${id}/resume`,
  finishClass: ({ id }) => `${prefix}/training-class/history/${id}/finish`,
  pauseClass: ({ id }) => `${prefix}/training-class/history/${id}/pause`,
  submitFeedback: ({ id }) => `${prefix}/training-class/history/${id}/feedback`,
  getHistoryDetail: ({ id, historyId }) =>
    `${prefix}/training-class/${id}?trainingClassHistoryId=${historyId}`,

  setBookmark: ({ id }) => `${prefix}/training-class/${id}/bookmark`,
  deleteBookmark: ({ id }) => `${prefix}/training-class/${id}/bookmark`,
  setLiveBookmark: ({ id }) => `${prefix}/live-class/${id}/bookmark`,
  deleteLiveBookmark: ({ id }) => `${prefix}/live-class/${id}/bookmark`,
  stashStep: ({ trainingClassHistoryId }) =>
    `${prefix}/training-class/history/${trainingClassHistoryId}/step/stash`,
  sendReaction: ({ id }) => `${prefix}/training-class/${id}/reaction`,
  trainingRecord: () => `${prefix}/training-record`,
  getClassHistory: ({ id, historyId }) =>
    `${prefix}/training-class/${id}?trainingClassHistoryId=${historyId}`,
};
