import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;
export default {
  getNotificationList: () => `${prefix}/user/notification`,

  getNotificationStatus: () => `${prefix}/user/notification/status`,
};
