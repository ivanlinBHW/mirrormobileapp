import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getAchievements: () => `${prefix}/user/achievements`,

  getProgressOverview: ({ start, end }) =>
    `${prefix}/progress/period/detail?start=${start}&end=${end}`,

  getProgressDate: ({ start, end }) =>
    `${prefix}/progress/period/status?start=${start}&end=${end}`,

  getProgressDetail: ({ workoutType, id }) => `${prefix}/${workoutType}/history/${id}`,

  deleteClassHistory: ({ id, workoutType }) => `${prefix}/${workoutType}/history/${id}`,

  setBookmark: ({ id, workoutType }) => `${prefix}/${workoutType}/${id}/bookmark`,

  deleteBookmark: ({ id, workoutType }) => `${prefix}/${workoutType}/${id}/bookmark`,
};
