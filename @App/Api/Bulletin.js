import { Config } from 'App/Config';
const prefix = ``;

export default {
  getBulletinContent: () => `/bulletin/content.html`,
  getBulletinVersion: () => `/bulletin/version.json`,
};
