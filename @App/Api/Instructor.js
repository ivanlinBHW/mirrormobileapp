import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getInstructorDetail: ({ id }) => `${prefix}/instructor/${id}`,
};
