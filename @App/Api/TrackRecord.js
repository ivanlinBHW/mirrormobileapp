import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  track: ({
    trackObjectType = 'TrainingClass',
    trackObjectId = undefined,
    trackActionType = 'view',
  }) => `${prefix}/track-record/${trackObjectType}/${trackObjectId}/${trackActionType}`,
};
