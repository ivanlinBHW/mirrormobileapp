import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getCoverImages: () => `${prefix}/training-event/cover-images`,
  getList: () => `${prefix}/community/index`,
  getAllMyEvent: () => `${prefix}/user/training-event`,
  searchEvent: () => `${prefix}/search-training-event`,
  getAllEvents: () => `${prefix}/training-event/list`,
  searchUser: () => `${prefix}/search-user`,
  postEventCreation: () => `${prefix}/training-event`,
  getTrainingEvent: ({ id }) => `${prefix}/training-event/${id}`,
  searchFriend: ({ type, eventId }) =>
    `${prefix}/search-friend?type=${type}${eventId ? '&trainingEventId=' + eventId : ''}`,
  getUserDetail: ({ id }) => `${prefix}/user/${id}`,
  addFriend: ({ id }) => `${prefix}/friend/${id}/add`,
  deleteFriend: ({ id }) => `${prefix}/friend/${id}/delete`,
  deleteUnFriend: ({ id }) => `${prefix}/friend/${id}/unfriend`,
  confirmFriend: ({ id }) => `${prefix}/friend/${id}/confirm`,
  getUserAchievement: ({ id }) => `${prefix}/user/${id}/achievements`,
  getUserEvent: ({ id }) => `${prefix}/user/${id}/training-event`,
  inviteFriends: ({ id }) => `${prefix}/training-event/${id}/invite`,
  searchEventFriend: ({ id, eventId }) =>
    `${prefix}/search-friend/training-event/${id}${
      eventId ? '?trainingEventId=' + eventId : ''
    }`,
  getDetailLeaderBoard: ({ id }) => `${prefix}/training-event/${id}/leaderboard`,
  participateEvent: ({ id }) => `${prefix}/training-event/${id}/participate`,
  startEventClass: () => `${prefix}/training-event/training-class/start`,
  pauseEventClass: () => `${prefix}/training-event/training-class/pause`,
  resumeEventClass: () => `${prefix}/training-event/training-class/resume`,
  finishEventClass: () => `${prefix}/training-event/training-class/finish`,
  stashStepEventClass: () => `${prefix}/training-event/training-class/step/stash`,
  getClassDetail: ({ id, eventId }) =>
    `${prefix}/training-class/${id}?trainingEventId=${eventId}`,
  getHistoryClassDetail: ({ id, eventId }) =>
    `${prefix}/training-class/${id}?trainingEventClassHistoryId=${eventId}`,
  deleteTrainingEvent: ({ id }) => `${prefix}/training-event/${id}`,
  rejectTrainingEvent: ({ id }) => `${prefix}/training-event/${id}/reject`,
  put1on1OnlineStatus: ({ trainingEventId, status }) =>
    `${prefix}/training-event/${trainingEventId}/1on1/${status}`,
  get1on1OnlineStatus: ({ trainingEventId }) =>
    `${prefix}/training-event/${trainingEventId}/1on1`,

  postSend1on1Notification: ({ trainingEventId }) =>
    `${prefix}/training-event/${trainingEventId}/push`,

  getMeetingSession: ({ trainingEventId }) =>
    `${prefix}/meeting-session/${trainingEventId}`,
};
