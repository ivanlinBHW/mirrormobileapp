import { Config } from 'App/Config';
const prefix = `/api/${Config.API_VERSION}`;

export default {
  logout: ({} = {}) => `${prefix}/admin/account/signout`,
  updateNotificationPermission: ({} = {}) => `${prefix}/client/notification/permission`,

  login: ({} = {}) => `${prefix}/admin/account/signin`,

  register: ({} = {}) => `${prefix}/client/account/register`,
  tempRegister: ({} = {}) => `${prefix}/client/account/tempRegister`,
  sendVerifyEmail: ({} = {}) => `${prefix}/client/account/send-verify-email`,

  updateSpotifyToken: ({} = {}) => `${prefix}/client/spotify/token`,

  deleteSpotifyToken: ({} = {}) => `${prefix}/client/spotify/token`,

  putSubscriptionCode: ({} = {}) => `${prefix}/client/subscription`,

  delMemberSubscription: ({} = {}) => `${prefix}/client/subscription/member`,

  addMemberSubscriptionCode: ({} = {}) => `${prefix}/client/subscription/member-code`,

  delMemberSubscriptionCode: ({ code } = {}) =>
    `${prefix}/client/subscription/member-code/${code}`,

  getUserAccount: ({ fcmToken } = {}) =>
    `${prefix}/admin/account/me?fcmToken=${fcmToken}`,

  notificationEnable: ({} = {}) => `${prefix}/client/user/notification/setting`,

  ipLocation: ({} = {}) => `${prefix}/util/ip-location`,
  resetPassword: ({} = {}) => `${prefix}/auth/reset-password`,
  updatePassword: ({} = {}) => `${prefix}/admin/account/reset-password`,
  deleteAccount: ({} = {}) => `${prefix}/client/account`,
};
