import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getCollectionList: () => `${prefix}/training-collection/index`,
  getCollectionDetail: ({ id }) => `${prefix}/training-collection/${id}`,
};
