import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  genre: () => `${prefix}/genre`,
  userOnline: () => `${prefix}/user/online`,
  userOffline: () => `${prefix}/user/offline`,
  setting: () => `${prefix}/user-data/settings`,
  updateEquipment: () => `${prefix}/user-data/equipments`,
  updateAvatar: () => `${prefix}/user-data/avatar/form`,
  getAvatar: ({ user_id }) =>
    `api/${
      Config.API_VERSION
    }/admin/file/url?objectKey=images/user/${user_id}/avatar.jpg`,

  getPrivacyPolicy: ({} = {}) => `${prefix}/policy/privacy`,
  getUsagePolicy: ({} = {}) => `${prefix}/policy/usage`,
};
