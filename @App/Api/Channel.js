import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getChannelList: () => `${prefix}/channel/index`,
  getChannelDetail: ({ channelId }) => `${prefix}/channel/${channelId}`,
  getChannelSeeAll: ({ channelId, genreId }) =>
    `${prefix}/channel/${channelId}/workout-type/${genreId}`,
};
