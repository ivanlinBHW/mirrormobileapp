import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getProgramList: () => `${prefix}/training-program/index`,
  getProgramDetail: ({ id }) => `${prefix}/training-program/${id}`,
  getProgramClassDetail: ({ id, trainingProgramClassHistory }) =>
    `${prefix}/training-class/${id}?programClassHistoryId=${trainingProgramClassHistory}`,
  seeProgramClassDetail: ({ id }) => `${prefix}/training-class/${id}`,
  startProgram: ({ id }) => `${prefix}/training-program/${id}/start`,
  leaveProgram: ({ id }) => `${prefix}/training-program/history/${id}/leave`,
  startProgramClass: () => `${prefix}/training-program/training-class/start`,
  resumeProgramClass: () => `${prefix}/training-program/training-class/resume`,
  pauseProgramClass: () => `${prefix}/training-program/training-class/pause`,
  finishProgramClass: () => `${prefix}/training-program/training-class/finish`,
  stashProgramStepId: () => `${prefix}/training-program/training-class/step/stash`,
};
