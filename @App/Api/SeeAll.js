import { Config } from 'App/Config';
const prefix = `api/${Config.API_VERSION}/client`;

export default {
  getList: ({ type, curPage = 1 }) => `${prefix}/${type}?page=${curPage}`,
  getClassListByWorkoutType: ({ workoutTypeId }) =>
    `${prefix}/training-class/workoutType/${workoutTypeId}`,
};
