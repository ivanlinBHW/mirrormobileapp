const prefix = '/api';

export default {
  index: ({} = {}) => `${prefix}/client/deviceVersionInfo`,
};
