import React from 'react';
import PropTypes from 'prop-types';
import Ping from 'react-native-ping';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  AppStore,
  AppStateActions,
  BleDeviceActions,
  MirrorActions,
  SpotifyActions,
  UserActions,
  MdnsActions,
} from 'App/Stores';
import { Spotify, Dialog } from 'App/Helpers';
import { Config } from 'App/Config';
import { Platform } from 'react-native';

const TAG = 'MirrorMonitor:';
const AUTO_CONNECT_LAST_DEVICE_WAITING_TIME = 1500;

class MirrorMonitor extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    updateSpotifySession: PropTypes.func.isRequired,
    mirrorDisconnectByUser: PropTypes.func.isRequired,
    mirrorDisconnect: PropTypes.func.isRequired,
    onLoading: PropTypes.func.isRequired,
    mirrorLogClear: PropTypes.func.isRequired,
    mirrorConnect: PropTypes.func.isRequired,
    getUserAccount: PropTypes.func.isRequired,
    isConnected: PropTypes.bool.isRequired,
    isConnecting: PropTypes.bool.isRequired,
    mirrorLog: PropTypes.func.isRequired,
    isPaired: PropTypes.bool.isRequired,
    connectedSsidName: PropTypes.string,
    currentCountryCode: PropTypes.string,
    currentNetworkInfo: PropTypes.object.isRequired,
    currentState: PropTypes.string,
    systemVersion: PropTypes.string,
    lastDevice: PropTypes.object,
    token: PropTypes.string,
    bleInitial: PropTypes.func.isRequired,

    fetchCheckServerRegion: PropTypes.func.isRequired,
    mdnsStop: PropTypes.func.isRequired,
    mdnsClear: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mdns: PropTypes.array,
    onAppInit: PropTypes.func.isRequired,
    deviceAppVersion: PropTypes.string,
    deviceVersionInfoItems: PropTypes.array,
    fcmToken: PropTypes.string,
    deviceVersionUpdateFinished: PropTypes.bool,
    lastDeviceConnectedAt: PropTypes.oneOfType([
      PropTypes.instanceOf(Date),
      PropTypes.String,
    ]),
    routeName: PropTypes.string.isRequired,
  };

  static defaultProps = {
    connectedSsidName: null,
    currentState: '',
    currentCountryCode: '',
    lastDevice: null,
    token: null,
  };
  state = {
    hasAlertReconnect: false,
  };

  lastIsConnected = false;
  _mdnsScaning = false;

  checkAppAndDeviceVersionUpdate = () => {
    const { deviceAppVersion, fcmToken } = this.props;

    const deviceAppVersionVerified = deviceAppVersion.slice(1).split('.');
    const appVersionVerified = Config.APP_VERSION.split('.');

    console.log(
      '=== CHECK APP_VERSION ===',
      deviceAppVersionVerified,
      appVersionVerified,
    );
    if (deviceAppVersion != '') {
      for (let versionSlot = 0; versionSlot < 2; versionSlot++) {
        if (
          parseInt(appVersionVerified[versionSlot], 10) <
          parseInt(deviceAppVersionVerified[versionSlot], 10)
        ) {
          console.log(
            '=== requestAppOlderVersionUpdateOrNotAlert ===',
            Config.APP_VERSION,
            deviceAppVersion,
          );
          Dialog.requestAppOlderVersionUpdateOrNotAlert();
          break;
        } else if (
          parseInt(appVersionVerified[versionSlot], 10) >
          parseInt(deviceAppVersionVerified[versionSlot], 10)
        ) {
          console.log(
            '=== requestDeviceOlderVersionUpdateOrNotAlert ===',
            Config.APP_VERSION,
            deviceAppVersion,
          );
          Dialog.requestDeviceOlderVersionUpdateOrNotAlert(
            fcmToken,
            Config.APP_VERSION,
            deviceAppVersionVerified,
          );
          break;
        }
      }
    }
  };
  async componentDidMount() {
    const {
      isPaired,
      bleInitial,
      mirrorDisconnect,
      mdns,
      mirrorDiscover,
      mdnsStop,
      mdnsClear,
      onAppInit,
      currentNetworkInfo,
    } = this.props;

    mirrorDisconnect();
    bleInitial();

    if (isPaired) {
      Spotify.initializeIfNeeded();
    }

    if (Config.IS_ENABLE_AUTO_REGION_SWITCH) {
      this.handleCheckServerHostRegion();
    }
    onAppInit();

    if (
      (currentNetworkInfo.isInternetReachable ||
        currentNetworkInfo.isInternetReachable == null) &&
      this._mdnsScaning == false
    ) {
      this._mdnsScaning = true;

      mdnsClear();
      mirrorDiscover();
      setTimeout(async () => {
        mdnsStop();
        this._mdnsScaning = false;
      }, 5000);
    }
  }

  componentDidUpdate(prevProps, currProps) {
    const {
      token,
      mirrorLog,
      mirrorConnect,
      lastDevice,
      currentState,
      isConnected,
      getUserAccount,
      currentNetworkInfo,
      mdns,
      deviceAppVersion,
      deviceVersionUpdateFinished,
    } = this.props;

    if (
      isConnected === true &&
      currentNetworkInfo.isInternetReachable &&
      deviceAppVersion !== undefined &&
      deviceAppVersion != '' &&
      prevProps.deviceAppVersion !== deviceAppVersion &&
      Config.CHECK_VERSION
    ) {
      console.log('=== checkAppAndDeviceVersionUpdate ===');
      this.checkAppAndDeviceVersionUpdate();
    }

    if (currentState !== prevProps.currentState) {
      mirrorLog(`MirrorMonitor currentState=>${currentState}`);

      if (currentState === 'active') {
        mirrorLog(`MirrorMonitor lastIsConnected=>${this.lastIsConnected}`);
        mirrorLog(`MirrorMonitor isConnected=>${isConnected}`);
        mirrorLog(`MirrorMonitor lastDevice=>${lastDevice ? lastDevice.host : null}`);

        if (this.lastIsConnected && !isConnected && lastDevice && token) {
          mirrorConnect(lastDevice, {
            maxRetries: 2,
          });
        }
        if (isConnected && currentNetworkInfo.isInternetReachable) {
          getUserAccount();

          if (Config.CHECK_VERSION) {
            this.checkAppAndDeviceVersionUpdate();
          }
        }
      } else {
        this.lastIsConnected = isConnected;
      }
    }
    if (
      Config.IS_ENABLE_AUTO_REGION_SWITCH &&
      (currentNetworkInfo !== prevProps.currentNetworkInfo ||
        currentState !== prevProps.currentState ||
        token !== prevProps.token)
    ) {
      this.handleCheckServerHostRegion();
    }

    console.log('=== Check update finished ===', deviceVersionUpdateFinished);

    if (deviceVersionUpdateFinished) {
      if (Platform.OS === 'android') {
        if (mdns != [] && currentNetworkInfo != null) {
          console.log('=== handleAutoConnectLastDevice android ===');
          setTimeout(
            () => this.handleAutoConnectLastDevice(mdns),
            AUTO_CONNECT_LAST_DEVICE_WAITING_TIME,
          );
        }
      } else if (mdns != []) {
        console.log('=== handleAutoConnectLastDevice ios ===');
        setTimeout(
          () => this.handleAutoConnectLastDevice(mdns),
          AUTO_CONNECT_LAST_DEVICE_WAITING_TIME,
        );
      }
    }
  }
  componentWillUnmount() {
    const { mirrorDisconnectByUser, routeName } = this.props;

    if (
      routeName &&
      !routeName.includes('Player') &&
      !routeName.includes('Detail') &&
      !routeName.includes('Modal')
    ) {
      mirrorDisconnectByUser();
    }

    console.log(`${TAG} componentWillUnmount`);
  }

  handleAutoConnectLastDevice = async (mdns) => {
    console.log('=== handleAutoConnectLastDevice ===');
    const {
      token,
      isConnected,
      mirrorConnect,
      lastDevice,
      lastDeviceConnectedAt,
    } = this.props;
    let shouldAutoMirrorDeviceConnect = false;

    if (lastDevice && lastDeviceConnectedAt) {
      let connectedAt = null;
      if (lastDeviceConnectedAt instanceof Date) connectedAt = lastDeviceConnectedAt;
      else connectedAt = new Date(lastDeviceConnectedAt);

      let diffMinutes = Math.floor((new Date() - connectedAt) / 1000 / 60);
      console.log('=== diffMinutes ===', diffMinutes);
      if (diffMinutes < 60) shouldAutoMirrorDeviceConnect = true;
    }
    if (
      shouldAutoMirrorDeviceConnect &&
      !isConnected &&
      token &&
      lastDevice &&
      lastDevice.host &&
      !this.state.hasAlertReconnect
    ) {
      try {
        let hasLastDeviceMdns = false;
        console.log('=== mdns ===', mdns);

        if (mdns != null) {
          mdns.forEach((service) => {
            if (service.name === lastDevice.name) {
              hasLastDeviceMdns = true;
            }
          });
        }
        const ms = await Ping.start(lastDevice.host, {
          timeout: 8000,
        });

        let hasMdnsPingResponse = false;
        if (ms > 0 && ms <= 8000) hasMdnsPingResponse = true;
        console.log('=== lastDevice ===', lastDevice);
        console.log('=== this.props.isConnecting ===', this.props.isConnecting);

        if (
          hasLastDeviceMdns &&
          hasMdnsPingResponse &&
          !this.state.hasAlertReconnect &&
          lastDevice != null &&
          token
        ) {
          this.setState({ hasAlertReconnect: true }, () => {
            let currentAlert = { isAutoConnectAlerting: true };
            AppStore.dispatch(AppStateActions.onAlertingChange(currentAlert));
            if (!this.props.isConnected) {
              Dialog.showConfirmConnectLastDeviceAlert(
                lastDevice.name ? lastDevice.name : lastDevice.host,
                () => {
                  let currentAlert = { isAutoConnectAlerting: false };
                  AppStore.dispatch(AppStateActions.onAlertingChange(currentAlert));
                  mirrorConnect(lastDevice);

                  setTimeout(() => {
                    this.setState({ hasAlertReconnect: false });
                  }, 3000);
                },
              );
            }
          });
        }
      } catch (e) {
      }
    }
  };

  handleCheckServerHostRegion = () => {
    const { fetchCheckServerRegion } = this.props;
    fetchCheckServerRegion();
  };

  handleSpotifySessionEvent = (session) => {
    const { updateSpotifySession } = this.props;
    updateSpotifySession(session);
  };

  handleAutoConnectMirror = () => {
    setTimeout(() => {
      const { mirrorConnect, token, isPaired, lastDevice } = this.props;
      if (token && isPaired && lastDevice) {
        mirrorConnect(lastDevice, {
          maxRetries: 2,
        });
      }
    }, 200);
  };

  render() {

    const { children } = this.props;
    return children;
  }
}

export default connect(
  (state) => ({
    device: state.bleDevice,
    routeName: state.appRoute.routeName,
    currentState: state.appState.currentState,
    currentNetworkInfo: state.appState.currentNetworkInfo,
    currentCountryCode: state.user.currentCountryCode,
    deviceAppVersion: state.mirror.deviceAppVersion,
    deviceVersionInfoItems: state.deviceVersionInfo.items,
    deviceVersionUpdateFinished: state.deviceVersionInfo.updateFinished,
    fcmToken: state.user.fcmToken,
    isConnecting: state.mirror.isConnecting,
    isConnected: state.mirror.isConnected,
    isPaired: state.mirror.isPaired,
    isLoading: state.appState.isLoading,
    token: state.user.token,
    lastDevice: state.mirror.lastDevice,
    lastDeviceConnectedAt: state.mirror.lastDeviceConnectedAt,
    connectedSsidName: state.mirror.connectedSsidName,
    mdns: state.mdns.services,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        mirrorDisconnect: MirrorActions.onMirrorDisconnect,
        mirrorDisconnectByUser: MirrorActions.onMirrorDisconnectByUser,
        mirrorLog: MirrorActions.onMirrorLog,
        mirrorLogClear: MirrorActions.onMirrorLogClear,
        mirrorConnect: MirrorActions.onMirrorConnect,
        mirrorLogout: MirrorActions.onMirrorLogout,
        updateSpotifySession: SpotifyActions.onSpotifyLogin,
        mdnsClear: MdnsActions.onMdnsScanClear,
        mdnsStop: MdnsActions.onMdnsStop,
        mirrorDiscover: MdnsActions.onMdnsScan,
        bleInitial: BleDeviceActions.onInitial,
        getUserAccount: UserActions.getUserAccount,

        onLoading: AppStateActions.onLoading,
        fetchCheckServerRegion: UserActions.fetchCheckServerRegion,
        onAppInit: AppStateActions.onAppInit,
      },
      dispatch,
    ),
)(MirrorMonitor);
