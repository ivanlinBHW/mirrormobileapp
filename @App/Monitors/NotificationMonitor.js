import { AppRegistry, Platform } from 'react-native';
import { CommunityActions, MirrorActions } from 'App/Stores';
import { Fcm, Notifs } from 'App/Helpers';

import { Config } from 'App/Config';
import PropTypes from 'prop-types';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash';

AppRegistry.registerHeadlessTask(
  'RNFirebaseBackgroundMessage',
  () => Notifs.onBackgroundMessageReceived,
);

class NotificationMonitor extends React.Component {
  static propTypes = {
    isConnected: PropTypes.bool.isRequired,
    isPaired: PropTypes.bool.isRequired,
    getTrainingEvent: PropTypes.func.isRequired,
    getUserDetail: PropTypes.func.isRequired,
    token: PropTypes.string,
    updateMirrorStore: PropTypes.func.isRequired,
  };

  static defaultProps = {
    token: null,
  };

  constructor(props) {
    super(props);
  }
  async componentDidMount() {
    if (Platform.OS === 'ios') {
      await Fcm.requestPermission();
    }

    this.fcmListeners = this.initialiseFcm();
  }
  componentWillUnmount() {
    Fcm.clearListeners(this.fcmListeners);
  }

  initialiseFcm = () => {
    Fcm.createAndroidChannel({
      CHANNEL_ID: Config.CHANNEL_ID,
      CHANNEL_NAME: Config.CHANNEL_NAME,
    });

    Fcm.getInitialNotif(Notifs.onMessageReceived);

    Fcm.getScheduledNotifications().then((notifications) => {
      console.log('getScheduledNotifications=>', notifications);
    });

    return Fcm.createNotificationListeners({
      CHANNEL_ID: Config.CHANNEL_ID,

      onOpenedHandler: this.onNotifsOpenedHandler,
      onMessageHandler: Notifs.onMessageReceived,
      onIncomingHandler: Notifs.onMessageReceived,
    });
  };

  onNotifsOpenedHandler = (message) => {
    const {
      isPaired,
      isConnected,
      getUserDetail,
      getTrainingEvent,
      updateMirrorStore,
    } = this.props;

    const data = get(message, '_data');

    Notifs.onNotifsOpened({
      data,
      isPaired,
      isConnected,
      getUserDetail,
      getTrainingEvent,
      updateMirrorStore,
    });
  };

  render() {
    return null;
  }
}

export default connect(
  (state) => ({
    device: state.bleDevice,
    routeName: state.appRoute.routeName,
    isConnected: state.mirror.isConnected,
    isPaired: state.mirror.isPaired,
    isLoading: state.appState.isLoading,
    token: state.user.token,
    connectedSsidName: state.mirror.connectedSsidName,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getTrainingEvent: CommunityActions.notificationGetTrainingEvent,
        getUserDetail: CommunityActions.notificationGetUserDetail,
        updateMirrorStore: MirrorActions.updateMirrorStore,
      },
      dispatch,
    ),
)(NotificationMonitor);
