import React from 'react';
import PropTypes from 'prop-types';
import DeviceInfo from 'react-native-device-info';
import * as RNLocalize from 'react-native-localize';
import NetInfo from '@react-native-community/netinfo';
import VersionNumber from 'react-native-version-number';
import Orientation from 'react-native-orientation-locker';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { Alert, AppState, BackHandler, Platform } from 'react-native';
import { setI18nConfig, translate as t } from 'App/Helpers/I18n';
import { Permission } from 'App/Helpers';
import { AppStateActions, SettingActions, MdnsActions, AppStore } from 'App/Stores';
import {
  UserActions,
  MirrorActions,
  SearchActions,
  DeviceVersionInfoActions,
} from 'App/Stores/index';
import SystemSetting from 'react-native-system-setting';
import { MirrorEvents, MirrorOptions } from 'App/Stores/Mirror/Actions';
import { Dialog } from 'App/Helpers';
import { Config } from 'App/Config';
export const EVENTS = {
  CHANGE: 'change',
  HARDWARE_BACK_PRESS: 'hardwareBackPress',
};
setI18nConfig();

const TAG = 'AppMonitor:';

class AppMonitor extends React.Component {
  removeNetInfoListener = null;

  _mounted = false;
  _mdnsScaning = false;

  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    currentNetworkInfo: PropTypes.object.isRequired,
    handleAppStateUpdate: PropTypes.func.isRequired,
    handleAppDeviceUpdate: PropTypes.func.isRequired,
    handleAppLocaleUpdate: PropTypes.func.isRequired,
    handleAppVersionUpdate: PropTypes.func.isRequired,
    handleAppNetInfoUpdate: PropTypes.func.isRequired,
    handleUpdateUserTimezone: PropTypes.func.isRequired,
    handleAppOrientationUpdate: PropTypes.func.isRequired,
    updateNotificationPermission: PropTypes.func.isRequired,
    onMirrorCommunicate: PropTypes.func.isRequired,
    onFcmTokenUpdate: PropTypes.func.isRequired,
    updateUserStore: PropTypes.func.isRequired,
    resetErrorCode: PropTypes.func.isRequired,
    routeName: PropTypes.string.isRequired,
    isConnected: PropTypes.bool.isRequired,
    currentState: PropTypes.string,
    languageCode: PropTypes.array,
    token: PropTypes.string,
    email: PropTypes.string,
    emailVerifySaga: PropTypes.func,
    mirror_music_volume: PropTypes.number,
    mirror_instructor_volume: PropTypes.number,
    mdnsStop: PropTypes.func.isRequired,
    mdnsClear: PropTypes.func.isRequired,
    mirrorDiscover: PropTypes.func.isRequired,
    mirrorDisconnect: PropTypes.func.isRequired,
    getDeviceVersionInfoItems: PropTypes.func.isRequired,
    deviceVersionInfoItems: PropTypes.array,
    deviceVersionInfoSkippedVersion: PropTypes.string,
    setVersionUpdateFinished: PropTypes.func.isRequired,
  };

  static defaultProps = {
    token: null,
    children: null,
    languageCode: [],
    currentState: '',
  };

  checkAppVersionUpdate = () => {
    const {
      getDeviceVersionInfoItems,
      deviceVersionInfoItems,
      deviceVersionInfoSkippedVersion,
      setVersionUpdateFinished,
    } = this.props;

    setVersionUpdateFinished(false);

    console.log('=== check deviceVersionInfoItems ===');
    console.log(deviceVersionInfoItems);
    const currentVersion = deviceVersionInfoItems.find(
      (item) => item.version === Config.APP_VERSION,
    );
    const mustUpdateVersion = deviceVersionInfoItems.find((item) => item.mustUpdate == 1);
    const latestVersion = deviceVersionInfoItems[0];

    console.log('=======currentVersion======', currentVersion);
    console.log('=======mustUpdateVersion======', mustUpdateVersion);
    console.log('=======latestVersion======', latestVersion);
    console.log('=======stored skippedVersion======', deviceVersionInfoSkippedVersion);
    try {
      if (
        currentVersion != null &&
        currentVersion.endOfLifeDate != null &&
        Date.now() > Date.parse(currentVersion.endOfLifeDate)
      ) {
        console.log('=== check 1 ===');
        Dialog.requestAppMustUpdateAlert(function() {
          console.log('=== setVersionUpdateFinished 1===', true);
          AppStore.dispatch(setVersionUpdateFinished(true));
        });
      } else if (mustUpdateVersion && Config.APP_VERSION < mustUpdateVersion.version) {
        console.log('=== check 2 ===');
        Dialog.requestAppMustUpdateAlert(function() {
          console.log('=== setVersionUpdateFinished 2===', true);
          AppStore.dispatch(setVersionUpdateFinished(true));
        });
      } else if (
        Config.APP_VERSION < latestVersion.version
      ) {
        console.log('=== check 3 ===');
        Dialog.requestAppUpdateOrNotAlert(latestVersion.version, function() {
          console.log('=== setVersionUpdateFinished 3===', true);
          AppStore.dispatch(setVersionUpdateFinished(true));
        });
      } else {
        console.log('=== no need update ===');
        AppStore.dispatch(setVersionUpdateFinished(true));
      }
    } catch (error) {
      console.log('=== check error ===', error);
    }
  };
  async componentDidMount() {
    const {
      handleAppDeviceUpdate,
      handleAppVersionUpdate,
      resetErrorCode,
      updateUserStore,
      getSearchableData,
      mirror_music_volume,
      mirror_instructor_volume,
      getDeviceVersionInfoItems,
      routeName,
    } = this.props;

    console.log('=== call getDeviceVersionInfoItems ===');
    getDeviceVersionInfoItems();

    handleAppVersionUpdate(VersionNumber);
    getSearchableData();
    handleAppDeviceUpdate({
      isTablet: DeviceInfo.isTablet(),
      isEmulator: await DeviceInfo.isEmulator(),
      systemVersion: DeviceInfo.getSystemVersion(),
      deviceId: DeviceInfo.getDeviceId(),
      deviceName: await DeviceInfo.getDeviceName(),
      totalMemoryInMb: (await DeviceInfo.getTotalMemory()) / 1024 / 1024,
    });
    Orientation.lockToPortrait();
    this.onAppStateChange('active');
    this.onLocalizationChange();
    AppState.addEventListener(EVENTS.CHANGE, this.onAppStateChange);
    RNLocalize.addEventListener(EVENTS.CHANGE, this.onLocalizationChange);
    Orientation.addOrientationListener(this.onOrientationChange);
    this.removeNetInfoListener = NetInfo.addEventListener(this.onNetInfoChange);
    let currentChangeVolume = (await SystemSetting.getVolume()) * 100;

    this.volumeListener = SystemSetting.addVolumeListener((data) => {
      const volume = data.value * 100;

      console.info(
        '=== got event volume ===',
        currentChangeVolume,
        volume,
        mirror_music_volume,
        mirror_instructor_volume,
      );

      if (volume - currentChangeVolume < 7) {
        currentChangeVolume = volume;

        if (
          this.props.isConnected &&
          this.props.currentState === 'active' &&
          mirror_music_volume !== volume
        ) {
          console.log('=== update mirror volume ===');

          this.onMusicValueChange('instructor-volume', volume, true);
          this.onMusicValueChange('music-volume', volume, true);
        }
      } else {
        SystemSetting.setVolume(currentChangeVolume / 100);
      }
    });
    resetErrorCode();
    global.showRetryOrCancelAlert = false;
    if (Platform.OS === 'android') {
      this.backHandler = BackHandler.addEventListener(
        EVENTS.HARDWARE_BACK_PRESS,
        this.onAndroidBackButtonPressed,
      );
    }
    const hasPermission = await Permission.checkFcmPermission();
    updateUserStore({ hasNotificationsPermission: hasPermission });

    this._mounted = true;

    setTimeout(() => {
      const { currentNetworkInfo } = this.props;
      if (
        currentNetworkInfo.type === 'none' ||
        (currentNetworkInfo.isConnected === false &&
          currentNetworkInfo.isInternetReachable === false)
      ) {
        Actions.OfflineModal({
          description: t('modal_offline_internet_not_reachable'),
        });
      }
    }, 500);
  }

  async componentDidUpdate(prevProps) {
    const {
      email,
      token,
      routeName,
      currentState,
      updateUserStore,
      onFcmTokenUpdate,
      currentNetworkInfo,
      updateNotificationPermission,
      deviceVersionInfoItems,
    } = this.props;
    if (prevProps.deviceVersionInfoItems !== deviceVersionInfoItems) {
      this.checkAppVersionUpdate();
    }
    if (
      this._mounted &&
      ![
        'DEBUG',
        'FcmDebugScreen',
        'DebugMenuScreen',
        'MdnsDebugScreen',
        'SpotifyDebugScreen',
        'BleDeviceDebugScreen',
        'OfflineModal',
        'MirrorCloseHotspotScreen',
        'MirrorOpenHotspotScreen',
        'MirrorPairScreen',
        'MirrorSetupScreen',
        'MirrorWifiInputScreen',
        'WifiBarCodeScanScreen',
        'MirrorSetupWifiInitScreen',
      ].includes(routeName) &&
      currentNetworkInfo !== prevProps.currentNetworkInfo
    ) {
      const normalWifiConnected =
        !currentNetworkInfo.details.ssid ||
        (currentNetworkInfo.details.ssid &&
          !currentNetworkInfo.details.ssid.includes('mirror-'));
      const isCheckConnectRoute =
        routeName !== 'MiiSettingScreen' && routeName !== 'MiiSettingScreen';
      console.log(
        '=== connection status ===',
        isCheckConnectRoute,
        currentNetworkInfo.isInternetReachable,
        normalWifiConnected,
      );
      if (currentNetworkInfo.type === 'none') {
        Actions.OfflineModal({
          description: t('modal_offline_internet_not_reachable'),
        });
      } else if (
        isCheckConnectRoute &&
        currentNetworkInfo.isInternetReachable === false &&
        normalWifiConnected
      ) {
        Actions.OfflineModal({
          description: t('modal_offline_internet_not_reachable'),
        });
      }
    }

    if (currentState !== prevProps.currentState && currentState === 'active') {
      const hasPermission = await Permission.checkFcmPermission();
      updateUserStore({ hasNotificationsPermission: hasPermission });
      if (hasPermission) {
        const fcmToken = await Permission.requestFcmToken();
        if (fcmToken) {
          onFcmTokenUpdate(fcmToken);
          if (token != null && token != '') {
            updateNotificationPermission({
              email,
              deviceAddress: fcmToken,
            });
          }
        }
      }
    }
  }
  componentWillUnmount() {
    AppState.removeEventListener(EVENTS.CHANGE);
    RNLocalize.removeEventListener(EVENTS.CHANGE);
    Orientation.removeOrientationListener(this.onOrientationChange);
    this.removeNetInfoListener();
    SystemSetting.removeVolumeListener(this.volumeListener);
    if (Platform.OS === 'android') {
      this.backHandler.remove();
    }

    this._mounted = false;
    console.log(`${TAG} componentWillUnmount`);
  }

  onMusicValueChange = (volumeType, volume, save) => {
    this.props.onMirrorCommunicate(MirrorEvents.CHANGE_AUDIO_VOLUME, {
      type: volumeType,
      volume: Math.round(volume),
      save,
    });
  };
  onNetInfoChange = (state) => {
    __DEV__ && console.log('@onNetInfoChange: ', state);
    const { handleAppNetInfoUpdate, mirrorDisconnect } = this.props;
    if (Platform.OS === 'android') {
    }

    handleAppNetInfoUpdate(state);

    if (state.type == 'cellular' || state.type == 'none') {
      mirrorDisconnect();
    }
    if (state.type == 'wifi' && this._mdnsScaning == false) {
      const { mirrorDiscover, mdnsStop, mdnsClear } = this.props;
      this._mdnsScaning = true;
      console.log('=== mdns scan ===');
      setTimeout(async () => {
        mirrorDiscover();
        setTimeout(async () => {
          mdnsStop();
          this._mdnsScaning = false;
        }, 5000);
      }, 3000);
    }
  };
  onOrientationChange = (orientation) => {
    __DEV__ && console.log('@onAppOrientationChange: ', orientation);
    const { handleAppOrientationUpdate } = this.props;
    handleAppOrientationUpdate(orientation);
  };
  onAppStateChange = (nextAppState, currentAppstate) => {
    const {
      email,
      routeName,
      emailVerifyTimerSaga,
      getUserAccount,
      token,
      currentNetworkInfo,
      mirror_instructor_volume,
      isConnected,
    } = this.props;

    __DEV__ && console.log('@onAppStateChange: ', nextAppState, routeName, token);
    console.log('token: ', token);

    if (nextAppState == 'active' && isConnected) {
      const systemVolume = SystemSetting.getVolume().then((systemVolume) => {
        console.log('=== active system voulme ===', systemVolume * 100);
        console.log('=== active check volume ===', mirror_instructor_volume);

        if (mirror_instructor_volume < systemVolume * 100) {
          console.log('lower system value', mirror_instructor_volume);
          SystemSetting.setVolume(mirror_instructor_volume / 100);
        } else if (mirror_instructor_volume > systemVolume * 100) {
          console.log('lower mirror value', systemVolume * 100);
          this.onMusicValueChange('instructor-volume', systemVolume * 100, true);
          this.onMusicValueChange('music-volume', systemVolume * 100, true);
        }
      });
    }

    if (nextAppState == 'active' && token != null && token != '') {
      getUserAccount();
    }

    if (nextAppState == 'active' && routeName == 'UserRegisterVerifyEmailScreen') {
      emailVerifyTimerSaga(email);
    }
    if (nextAppState == 'active') {
      if (currentNetworkInfo.type == 'wifi' && this._mdnsScaning == false) {
        const { mirrorDiscover, mdnsStop } = this.props;
        this._mdnsScaning = true;
        console.log('=== mdns scan ===');
        setTimeout(async () => {
          mirrorDiscover();
          setTimeout(async () => {
            mdnsStop();
            this._mdnsScaning = false;
          }, 5000);
        }, 3000);
      }
    }
    const { handleAppStateUpdate } = this.props;
    handleAppStateUpdate(nextAppState);
  };
  onLocalizationChange = () => {
    const {
      handleAppLocaleUpdate,
      handleUpdateUserTimezone,
      currentNetworkInfo: { isInternetReachable },
      token,
    } = this.props;
    handleAppLocaleUpdate({
      currentLocales: RNLocalize.getLocales(),
      currentTimeZone: RNLocalize.getTimeZone(),
    });
    setI18nConfig();
    this.forceUpdate();

    if (token && isInternetReachable) {
      handleUpdateUserTimezone(RNLocalize.getTimeZone());
    }
  };

  onAndroidBackButtonPressed = () => {
    const { routeName } = this.props;

    console.log('onAndroidBackButtonPressed', routeName);

    const backButtonQuitSceneList = [
      'ClassScreen',
      'Progress',
      'CommunityScreen',
      'Shop',
      'Setting',
      'IntroScreen',
      'UserLoginScreen',
    ];
    if (backButtonQuitSceneList.includes(routeName)) {
      BackHandler.exitApp();
      return true;
    }
    return false;
  };

  render() {
    const {
      children,
      deviceVersionInfoItems,
      deviceVersionInfoSkippedVersion,
    } = this.props;
    console.log('=== deviceVersionInfoItems ===', deviceVersionInfoItems);
    console.log(
      '=== deviceVersionInfoSkippedVersion ===',
      deviceVersionInfoSkippedVersion,
    );
    return children;
  }
}

export default connect(
  (state) => ({
    token: state.user.token,
    email: state.user.email,
    device: state.bleDevice,
    routeName: state.appRoute.routeName,
    isConnected: state.mirror.isConnected,
    currentState: state.appState.currentState,
    languageCode: state.appState.currentLocales,
    currentNetworkInfo: state.appState.currentNetworkInfo,
    mirror_music_volume: state.mirror.options[MirrorOptions.MUSIC_VOLUME],
    mirror_instructor_volume: state.mirror.options[MirrorOptions.INSTROCTOR_VOLUME],
    deviceVersionInfoItems: state.deviceVersionInfo.items,
    deviceVersionInfoSkippedVersion: state.deviceVersionInfo.skippedVersion,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        handleAppDeviceUpdate: AppStateActions.onDeviceChange,
        handleAppStateUpdate: AppStateActions.onStateChange,
        handleAppLocaleUpdate: AppStateActions.onLocaleChange,
        handleAppVersionUpdate: AppStateActions.onVersionChange,
        handleAppNetInfoUpdate: AppStateActions.onNetInfoChange,
        handleAppOrientationUpdate: AppStateActions.onOrientationChange,
        onFcmTokenUpdate: UserActions.onFcmTokenUpdate,
        updateUserStore: UserActions.updateUserStore,

        updateNotificationPermission: UserActions.updateNotificationPermission,
        handleUpdateUserTimezone: SettingActions.onUpdateUserTimezone,
        resetErrorCode: UserActions.resetErrorCode,
        emailVerifySaga: UserActions.emailVerifySaga,
        emailVerifyTimerSaga: UserActions.emailVerifyTimerSaga,
        onMirrorCommunicate: MirrorActions.onMirrorCommunicate,
        getSearchableData: SearchActions.getSearchableData,
        getUserAccount: UserActions.getUserAccount,
        mdnsClear: MdnsActions.onMdnsScanClear,
        mdnsStop: MdnsActions.onMdnsStop,
        mirrorDiscover: MdnsActions.onMdnsScan,
        mirrorDisconnect: MirrorActions.onMirrorDisconnect,
        getDeviceVersionInfoItems: DeviceVersionInfoActions.indexSaga,
        setVersionUpdateFinished: DeviceVersionInfoActions.setVersionUpdateFinished,
      },
      dispatch,
    ),
)(AppMonitor);
