import React from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { AppBulletinActions, ClassActions } from 'App/Stores';

class AppBulletinMonitor extends React.PureComponent {
  static propTypes = {
    getBulletinVersion: PropTypes.func.isRequired,
    skippedVersion: PropTypes.string,
    newestVersion: PropTypes.string,
  };

  static defaultProps = {
    skippedVersion: undefined,
    newestVersion: undefined,
  };

  constructor(props) {
    super(props);
  }
  componentDidMount() {
    setTimeout(() => {
      this.props.getBulletinVersion();
    }, 1);
  }

  componentDidUpdate(prevProps) {}

  render() {
    return null;
  }
}

export default connect(
  (state) => ({
    skippedVersion: state.appBulletin.skippedVersion,
    newestVersion: state.appBulletin.version,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        getClassList: ClassActions.getClassList,
        getBulletinVersion: AppBulletinActions.getBulletinVersion,
      },
      dispatch,
    ),
)(AppBulletinMonitor);
