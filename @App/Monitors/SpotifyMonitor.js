import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Config } from 'App/Config';
import { SpotifyActions } from 'App/Stores';
import { Spotify } from 'App/Helpers';

class SpotifyMonitor extends React.Component {
  static propTypes = {
    apiToken: PropTypes.string,
    accessToken: PropTypes.string,
    refreshToken: PropTypes.string,
    accessTokenExpirationDate: PropTypes.string,
    currentNetworkInfo: PropTypes.object.isRequired,
    currentState: PropTypes.string,
  };

  static defaultProps = {
    apiToken: null,
    accessToken: null,
    refreshToken: null,
    currentState: null,
    accessTokenExpirationDate: null,
  };

  timer = null;
  componentDidMount() {
    this.setExpirationCheckTimer();
  }

  componentDidUpdate(prevProps) {
    const {
      currentState,
      currentNetworkInfo: { isInternetReachable },
    } = this.props;

    if (
      isInternetReachable !== get(prevProps, 'currentNetworkInfo.isInternetReachable')
    ) {
      Spotify.initializeIfNeeded();
    }

    if (prevProps.currentState !== currentState && currentState === 'active') {
      this.setExpirationCheckTimer();
    }
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  setExpirationCheckTimer = () => {
    if (this.timer) {
      clearInterval(this.timer);
    }

    this.timer = setInterval(() => {
      const { isLogin, isExpired, countdown } = Spotify.checkExpiredStatus();

      console.log('SpotifyMonitor: isLogin', isLogin);
      console.log('SpotifyMonitor: isExpired', isExpired);
      console.log('SpotifyMonitor: countdown', countdown);

      if ((isLogin && isExpired) || (isLogin && countdown < 5)) {
        Spotify.initializeIfNeeded();
      }
    }, Config.SPOTIFY_CHECK_PERIOD || 55 * 60 * 1000);
  };

  render() {
    return null;
  }
}

export default connect(
  (state) => ({
    currentState: state.appState.currentState,
    currentNetworkInfo: state.appState.currentNetworkInfo,
    apiToken: state.user.token,
    refreshToken: state.spotify.currentSession.refreshToken,
    accessToken: state.spotify.currentSession.accessToken,
    accessTokenExpirationDate: state.spotify.currentSession.accessTokenExpirationDate,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onSpotifyLogin: SpotifyActions.onSpotifyLogin,
        onSpotifyLogout: SpotifyActions.onSpotifyLogout,
      },
      dispatch,
    ),
)(SpotifyMonitor);
