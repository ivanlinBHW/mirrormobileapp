import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';

import {
  PrimaryNavbar,
  ImageButton,
  AutoHideViewHoc,
  FullWidthBarButton,
} from 'App/Components';
import { CommunityActions } from 'App/Stores';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet, isIphoneX } from 'App/Helpers';
import { Images, Colors, Metrics, Classes } from 'App/Theme';
import { Screen } from 'App/Helpers';

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    marginTop: Screen.scale(20),
    backgroundColor: Colors.navbar.community.background,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  btn: {
    width: '30@s',
    height: '30@vs',
    marginLeft: '24@vs',
  },
  imgStyle: {
    width: '30@s',
    height: '30@vs',
  },
});

class CommunityNavbar extends React.Component {
  static propTypes = {
    resetEventData: PropTypes.func.isRequired,
  };

  render() {
    return (
      <View style={styles.container}>
        <PrimaryNavbar
          navText={t('community')}
          navComponent={
            <View style={styles.wrapper}>
              <ImageButton
                style={styles.btn}
                imageStyle={styles.imgStyle}
                image={Images.members_black}
                onPress={Actions.FriendSearchScreen}
              />
              <ImageButton
                style={styles.btn}
                imageStyle={styles.imgStyle}
                image={Images.search}
                onPress={Actions.EventSearchScreen}
              />
            </View>
          }
        />

        <View style={Classes.marginTop}>
          <FullWidthBarButton
            text={t('community_event_creation_button_create_event')}
            onPress={() => {
              Actions.CreateEventScreen({ hideTabBar: true });
              this.props.resetEventData();
            }}
          />
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => ({
    programId: state.user.activeTrainingProgramId,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        resetEventData: CommunityActions.resetEventData,
      },
      dispatch,
    ),
)(
  AutoHideViewHoc(
    CommunityNavbar,
    0,
    isIphoneX() ? Metrics.homeNavBarHeight - 10 : Metrics.homeNavBarHeight + 12,
  ),
);
