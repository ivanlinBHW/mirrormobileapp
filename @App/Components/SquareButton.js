import React from 'react';
import { Text } from 'react-native';
import { PropTypes } from 'prop-types';
import { RoundButton } from '@ublocks-react-native/component';

import { ScaledSheet, Screen } from 'App/Helpers';
import { Colors, Fonts } from 'App/Theme';
import { BaseIcon } from 'App/Components';

const styles = ScaledSheet.create({
  imgButtonStyle: {
    height: '44@vs',
    backgroundColor: Colors.button.primary.content.background,
    flexDirection: 'row',
    borderRadius: 22,
  },
  button: {
    height: '44@vs',
    backgroundColor: Colors.button.primary.content.background,
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonRound: {
    borderRadius: '6@s',
  },
  buttonOutline: {
    backgroundColor: Colors.button.primary.outline.background,
  },
  textOutline: {
    color: Colors.button.primary.outline.text,
  },
  image: {
    width: '30@s',
    height: '30@vs',
    marginRight: '8@vs',
  },
  imageButton: {
    backgroundColor: Colors.button.primary.image.background,
  },
  text: {
    ...Fonts.style.medium500,
    color: Colors.button.primary.content.text,
  },
  icon: {
    marginLeft: '-45@s',
    marginRight: '20@s',
  },
  buttonBorderOutline: {
    borderWidth: 2,
    borderColor: Colors.button.primary.outline.border,
    backgroundColor: Colors.button.primary.outline.background,
  },
  textBorderOutline: {
    color: Colors.button.primary.outline.text,
  },
});

export default class SquareButton extends React.PureComponent {
  static propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    color: PropTypes.string,
    textColor: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    round: PropTypes.bool,
    outline: PropTypes.bool,
    width: PropTypes.number,
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    disabled: PropTypes.bool,
    icon: PropTypes.string,
    iconColor: PropTypes.string,
    radius: PropTypes.number,
    borderOutline: PropTypes.bool,
    throttleTime: PropTypes.number,
  };

  static defaultProps = {
    style: {},
    color: null,
    round: false,
    outline: false,
    disabled: false,
    borderOutline: false,
    onPress: () => {},
    width: 0,
    textStyle: {},
    icon: null,
    iconColor: Colors.button.primary.text.text,
    throttleTime: 400,
  };

  render() {
    const {
      onPress,
      width,
      text,
      textColor,
      textStyle,
      style,
      color,
      disabled,
      round,
      outline,
      icon,
      iconColor,
      borderOutline,
      radius,
      throttleTime,
      ...props
    } = this.props;
    return (
      <RoundButton
        {...props}
        translucent
        transparent
        style={[
          styles.button,
          round && styles.buttonRound,
          outline && styles.buttonOutline,
          color && { backgroundColor: color },
          width && { width: Screen.scale(width) },
          radius && { borderRadius: radius },
          borderOutline && styles.buttonBorderOutline,
          style,
        ]}
        disabled={disabled}
        onPress={onPress}
        throttleTime={throttleTime}
      >
        {icon && <BaseIcon name={icon} color={iconColor} style={styles.icon} />}
        <Text
          style={[
            styles.text,
            outline && styles.textOutline,
            borderOutline && styles.textBorderOutline,
            textColor && { color: textColor },
            textStyle,
          ]}
        >
          {text}
        </Text>
      </RoundButton>
    );
  }
}
