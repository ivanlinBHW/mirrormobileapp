import React from 'react';
import { Image } from 'react-native';
import { PropTypes } from 'prop-types';
import { isObject, isString, isEmpty, isNil, set, get } from 'lodash';

import { ScaledSheet, getS3SignedHeaders } from 'App/Helpers';
import { Colors, Images } from 'App/Theme';
import { CachedImage } from 'App/Components';

const BaseAvatar = ({
  uri = null,
  image = Images.defaultAvatar,
  style,
  size,
  active,
  borderColor,
  borderWidth,
  cache = false,
  time = null,
  imageSize = undefined,
  imageType = undefined,
  ...props
}) => {
  let source = {};

  if (isObject(uri) && !isNil(uri)) {
    source = { ...uri };
  } else if (isString(uri) && !isNil(uri) && !isEmpty(uri)) {
    source = { uri };
  } else {
    source = image;
  }

  if (!cache && isString(get(source, 'uri'))) {
    if (source.headers) {
      set(source, 'headers', getS3SignedHeaders(source.headers));
      set(source, 'headers.Cache-Control', 'no-store');
      set(source, 'headers.Pragma', 'no-cache');
    }
    if (time) {
      set(source, 'uri', `${get(source, 'uri')}?t=${new Date(time).getTime()}`);
    }
  }
  const ImageComponent = cache ? CachedImage : Image;

  return (
    (uri || image) && (
      <ImageComponent
        errorImage={Images.defaultAvatar}
        source={cache ? uri : source}
        resizeMode="cover"
        imageType={imageType}
        imageSize={imageSize}
        style={[
          styles.wrapper,
          {
            width: size,
            height: size,
            borderRadius: size / 2,
          },
          active ? styles.activeBorder : styles.notActiveBorder,
          borderColor && { borderColor },
          borderWidth && { borderWidth },
          style,
        ]}
      />
    )
  );
};

const styles = ScaledSheet.create({
  activeBorder: {
    borderWidth: 2,
    borderColor: Colors.avatar.primary.border,
  },
  notActiveBorder: {
    borderWidth: 0,
    borderColor: Colors.transparent,
  },
  wrapper: {
    overflow: 'hidden',
    borderColor: 'white',
  },
});

BaseAvatar.propTypes = {
  uri: PropTypes.any,
  size: PropTypes.number,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  active: PropTypes.bool,
  image: PropTypes.number,
  borderWidth: PropTypes.any,
  borderColor: PropTypes.any,
  imageSize: PropTypes.string,
  imageType: PropTypes.string,
  time: PropTypes.string,
  cache: PropTypes.bool,
};

BaseAvatar.defaultProps = {
  size: 24,
  style: {},
  active: false,
  borderColor: null,
  borderWidth: null,
  image: Images.defaultAvatar,
  imageSize: '1x',
  imageType: undefined,
  time: null,
};

export default BaseAvatar;
