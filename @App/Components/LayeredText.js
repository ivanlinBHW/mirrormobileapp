import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import { StyleSheet, Screen } from 'App/Helpers';
import { Colors, Classes, Fonts } from 'App/Theme';

const style = StyleSheet.create({
  container: {
    ...Classes.fillCenter,
    marginVertical: Screen.verticalScale(6),
  },
  upperText: {
    letterSpacing: Screen.scale(0.67),
    fontSize: Fonts.size.regular,
    color: Colors.black,
  },
  lowerText: {
    letterSpacing: Screen.scale(1),
    fontSize: Screen.scale(10),
    color: Colors.black,
  },
});

const LayeredText = ({ title, subtitle }) => {
  return (
    <View style={style.container}>
      <Text style={style.upperText}>{title}</Text>
      <Text style={style.lowerText}>{subtitle}</Text>
    </View>
  );
};

LayeredText.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  subtitle: PropTypes.string.isRequired,
};

export default LayeredText;
