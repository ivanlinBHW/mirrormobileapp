import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'App/Helpers';
import { SquareButton } from 'App/Components';
import { Metrics, Fonts, Colors } from 'App/Theme';

const styles = StyleSheet.create({
  button: {
    justifyContent: 'flex-start',
    marginBottom: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin * 4,
  },
});

const ConnectItemButton = ({
  text,
  style,
  textColor,
  textStyle,
  onPress,
  disabled,
  active,
  icon,
  iconColor,
  iconSize,
}) => (
  <SquareButton
    text={text}
    style={[styles.button, style]}
    textColor={Colors.black}
    textStyle={[Fonts.style.medium500, textStyle]}
    onPress={onPress}
    disabled={disabled}
    icon={active ? icon : null}
    iconColor={active ? iconColor : null}
    iconSize={active ? iconSize : null}
    throttleTime={0}
    outline
  />
);

ConnectItemButton.propTypes = {
  text: PropTypes.string.isRequired,
  style: PropTypes.object,
  textColor: PropTypes.string,
  textStyle: PropTypes.object,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  active: PropTypes.bool,
  icon: PropTypes.string,
  iconColor: PropTypes.string,
};

ConnectItemButton.defaultProps = {
  style: {},
  textColor: Colors.black,
  textStyle: Fonts.style.medium500,
  onPress: () => {},
  disabled: false,
  active: false,
  icon: 'check-circle',
  iconColor: Colors.primary,
};

export default ConnectItemButton;
