import React from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
  TextInput,
  View,
  Text,
} from 'react-native';

import { BaseIconButton } from 'App/Components';
import { ScaledSheet, Screen, ifIphoneX } from 'App/Helpers';
import { Classes, Metrics, Colors, Fonts } from 'App/Theme';

const styles = ScaledSheet.create({
  container: {
    paddingVertical: Metrics.baseMargin / 4,
  },
  containerHorizontal: {
    flex: 1,
    paddingTop: Metrics.baseMargin / 2,
    paddingBottom: Metrics.baseMargin / 4,
  },
  txtTitle: {
    justifyContent: 'flex-start',
    textAlign: 'left',
  },
  txtTitleHorizontal: {
    textAlign: 'left',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtError: {
    color: Colors.error,
    paddingTop: 0,
    ...Fonts.style.small500,
  },
  txtRequired: {
    color: Colors.error,
    paddingHorizontal: Metrics.baseMargin / 4,
    ...Fonts.style.medium500,
  },
  txtRequiredHorizontal: {
    color: Colors.error,
    paddingLeft: Metrics.baseMargin / 4,
    marginRight: Metrics.baseMargin / 2,
    ...Fonts.style.medium500,
  },
  txtPostfix: {
    backgroundColor: Colors.transparent,
    position: 'absolute',
    right: 0,
    top: Platform.select({
      ios: ifIphoneX(Screen.verticalScale(10), Screen.verticalScale(6)),
      android: Screen.verticalScale(10),
    }),
  },
  btnShowPassword: {
    position: 'absolute',
    right: 0,
    top: Platform.select({
      ios: ifIphoneX(Screen.verticalScale(-2), Screen.verticalScale(-2)),
      android: Screen.verticalScale(2),
    }),
  },
  textInput: {
    flex: 1,
    marginTop: Metrics.baseMargin / 4,
    minHeight: Metrics.baseMargin * 2.5,
    paddingHorizontal: Metrics.baseMargin,
    paddingBottom: 0,
  },
  textInputVerticalWrapper: {
  },
  fullBorder: {
    borderWidth: 1,
    borderColor: Colors.gray_02,
    borderRadius: Metrics.baseMargin / 2.5,
  },
  bottomBorder: {
    borderBottomWidth: Screen.onePixel,
    borderColor: Colors.gray_02,
  },
  txtTitleHorizontalWrapper: {
    flex: 0.5,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: Platform.OS === 'ios' ? Screen.verticalScale(5) : Screen.verticalScale(6),
    paddingBottom: 0,
    marginTop: 0,
    marginBottom: Metrics.baseMargin / 4,
  },
  textInputHorizontalWrapper: {
    flex: 1,
    width: '100%',
    marginBottom: 0,
    paddingBottom: 0,
    paddingTop: 0,

    justifyContent: 'center',
  },
  textInputHorizontal: {
    flex: 1,
    paddingTop: Platform.select({
      ios: Screen.verticalScale(2),
      android: Screen.verticalScale(0),
    }),
    paddingBottom: 0,
    marginBottom: 0,
    height: 'auto',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

const propTypes = {
  titleStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  errorStyle: PropTypes.object,
  requiredStyle: PropTypes.object,
  containerStyle: PropTypes.object,
  postfix: PropTypes.string,
  postfixStyle: PropTypes.object,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  inputSize: PropTypes.number,
  inputAlign: PropTypes.string,
  title: PropTypes.string,
  maxLength: PropTypes.number,
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func,
  onPress: PropTypes.func,
  numberOfLines: PropTypes.number,
  multiline: PropTypes.bool,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  bold: PropTypes.bool,
  error: PropTypes.string,
  required: PropTypes.bool,
  inputColor: PropTypes.string,
  border: PropTypes.bool,
  paddingHorizontal: PropTypes.number,
  borderStyle: PropTypes.object,
  component: PropTypes.object,
  password: PropTypes.bool,
  editable: PropTypes.bool,
  disabledColor: PropTypes.string,
  titleFlex: PropTypes.number,
  getRef: PropTypes.func,
};

const defaultProps = {
  titleFlex: 0.5,
  titleStyle: {},
  inputStyle: {},
  errorStyle: {},
  requiredStyle: {},
  containerStyle: {},
  inputAlign: 'left',
  title: '',
  maxLength: 45,
  height: 40,
  placeholder: '',
  onPress: undefined,
  onChangeText: undefined,
  numberOfLines: 1,
  multiline: false,
  inputSize: Fonts.size.medium,
  bold: false,
  required: false,
  error: '',
  value: '',
  border: false,
  borderStyle: {},
  inputColor: Colors.black,
  paddingHorizontal: 0,
  component: null,
  editable: true,
  disabledColor: Colors.brownGrey,
  getRef: () => {},
};

export const BaseVerticalTextInput = ({
  titleStyle,
  inputStyle,
  errorStyle,
  requiredStyle,
  containerStyle,
  inputSize,
  inputAlign,
  value,
  title,
  error,
  inputColor,
  maxLength,
  placeholder,
  onChangeText,
  onPress,
  numberOfLines,
  multiline,
  height,
  bold,
  border,
  borderStyle,
  required,
  editable,
  disabledColor,
  component,
  paddingHorizontal,
  getRef,
  ...props
}) => {
  const fontWeight = bold ? '900' : '100';
  const multipleLinesInputStyle = {
    minHeight: Screen.verticalScale(height - inputSize + numberOfLines * inputSize),
    lineHeight: inputSize,
    textAlignVertical: 'top',
    paddingTop: Platform.OS === 'ios' ? Metrics.baseMargin / 2 : Metrics.baseMargin,
    paddingBottom: 0,
  };
  const singleLineInputStyle = {
    minHeight: Screen.verticalScale(height),
    textAlignVertical: 'center',
    paddingTop: 0,
    paddingBottom: 0,
  };
  const WrappedComponent =
    typeof onPress === 'function' ? TouchableWithoutFeedback : View;
  return (
    <WrappedComponent
      style={{ height: 'auto' }}
      onPress={onPress}
    >
      <View style={[styles.container]}>
        <View style={Classes.row}>
          <Text style={[styles.txtTitle, { fontWeight }, titleStyle]}>{title}</Text>
          {required && <Text style={[styles.txtRequired, requiredStyle]}>*</Text>}
        </View>

        <View
          style={styles.textInputVerticalWrapper}
          pointerEvents={typeof onPress === 'function' ? 'none' : 'auto'}
        >
          {component || (
            <TextInput
              {...props}
              ref={getRef}
              style={[
                styles.textInput,
                border ? styles.fullBorder : styles.bottomBorder,
                border && borderStyle,
                height === defaultProps.height && numberOfLines > 1
                  ? multipleLinesInputStyle
                  : singleLineInputStyle,
                {
                  color: inputColor,
                  textAlign: inputAlign,
                },
                !editable && { color: disabledColor },
              ]}
              numberOfLines={numberOfLines}
              onChangeText={onChangeText}
              placeholder={placeholder}
              maxLength={maxLength}
              multiline={multiline || numberOfLines > 1}
              value={value}
              editable={typeof onPress !== 'function'}
            />
          )}
          <Text style={[styles.txtError, errorStyle]}>{error}</Text>
        </View>
      </View>
    </WrappedComponent>
  );
};

BaseVerticalTextInput.propTypes = propTypes;
BaseVerticalTextInput.defaultProps = defaultProps;

export class BaseHorizontalTextInput extends React.PureComponent {
  static propTypes = propTypes;

  static defaultProps = defaultProps;

  state = {
    isShowPassword: false,
  };

  triggerShowPassword = () => {
    this.setState((state) => ({
      ...state,
      isShowPassword: !state.isShowPassword,
    }));
  };

  render() {
    const {
      titleStyle,
      inputStyle,
      errorStyle,
      requiredStyle,
      containerStyle,
      postfix,
      postfixStyle,
      inputSize,
      inputAlign,
      titleFlex,
      value,
      title,
      error,
      inputColor,
      maxLength,
      placeholder,
      onChangeText,
      onPress,
      numberOfLines,
      multiline,
      height,
      bold,
      border,
      borderStyle,
      required,
      editable,
      disabledColor,
      component,
      password,
      getRef,
      ...props
    } = this.props;
    const { isShowPassword } = this.state;

    const fontWeight = bold ? '900' : '100';
    const iconPlatform = Platform.OS === 'ios' ? 'ios' : 'md';
    const multipleLinesInputStyle = {
      minHeight: height - inputSize + numberOfLines * inputSize,
      lineHeight: inputSize,
      paddingTop: Metrics.baseMargin,
      textAlignVertical: 'top',
    };
    return (
      <TouchableWithoutFeedback
        onPress={onPress}
        disabled={!onPress}
      >
        <View style={[Classes.rowCenter, styles.containerHorizontal, containerStyle]}>
          {/* form-control-title-wrapper */}
          <View
            style={[
              Classes.row,
              styles.txtTitleHorizontalWrapper,
              { flex: titleFlex },
              numberOfLines > 1 && {
                paddingTop: Metrics.baseMargin,
              },
            ]}
          >
            <Text style={[styles.txtTitleHorizontal, { fontWeight }, titleStyle]}>
              {title}
            </Text>
            {required && (
              <Text style={[styles.txtRequiredHorizontal, requiredStyle]}>*</Text>
            )}
          </View>

          {/* form-control-wrapper */}
          <View
            style={[styles.textInputHorizontalWrapper]}
            pointerEvents={typeof onPress === 'function' ? 'none' : 'auto'}
          >
            {component || (
              <TextInput
                {...props}
                ref={getRef}
                style={[
                  styles.textInputHorizontal,
                  border ? styles.fullBorder : styles.bottomBorder,
                  border && borderStyle,
                  height === defaultProps.height && numberOfLines > 1
                    ? multipleLinesInputStyle
                    : {
                      },
                  {
                    color: inputColor,
                    textAlign: inputAlign,
                  },
                  !editable && { color: disabledColor },
                  inputStyle,
                ]}
                numberOfLines={numberOfLines}
                onChangeText={onChangeText}
                placeholder={placeholder}
                maxLength={maxLength}
                multiline={multiline || numberOfLines > 1}
                value={value}
                editable={editable}
                autoCompleteType={password ? 'password' : props.autoCompleteType}
                secureTextEntry={password && !isShowPassword}
              />
            )}
            {!!error && <Text style={[styles.txtError, errorStyle]}>{!!error && error}</Text>}
          </View>

          {postfix && typeof postfix === 'string' ? (
            <Text style={[styles.txtPostfix, postfixStyle]}>{postfix}</Text>
          ) : (
            postfix
          )}

          {password && (
            <BaseIconButton
              iconName={
                isShowPassword ? `${iconPlatform}-eye` : `${iconPlatform}-eye-off`
              }
              iconSize={16}
              iconColor="gray"
              style={[styles.btnShowPassword]}
              width={Metrics.baseMargin * 2}
              onPress={this.triggerShowPassword}
            />
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default {
  BaseVerticalTextInput,
  BaseHorizontalTextInput,
};
