import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, SafeAreaView } from 'react-native';

import { StyleSheet } from 'App/Helpers';
import { Colors, Metrics } from 'App/Theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    backgroundColor: Colors.white02,
    paddingHorizontal: Metrics.baseMargin,
    height: Metrics.homeNavBarHeight - Metrics.baseVerticalMargin,
  },
  titleText: {
    fontSize: '36@s',
    justifyContent: 'flex-start',
  },
  leftBox: {
    width: '60%',
    height: 'auto',
  },
  rightBox: {
    width: '40%',
    height: 'auto',
    justifyContent: 'flex-end',
    marginBottom: '5@vs',
    zIndex: 500,
  },
});

class PrimaryNavbar extends React.PureComponent {
  static propTypes = {
    navText: PropTypes.string.isRequired,
    navComponent: PropTypes.object,
  };

  render() {
    const { navText, navComponent } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.leftBox}>
          <Text style={styles.titleText}>{navText}</Text>
        </View>
        <View style={styles.rightBox}>{navComponent}</View>
      </SafeAreaView>
    );
  }
}

export default PrimaryNavbar;
