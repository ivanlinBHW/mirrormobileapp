import React from 'react';
import Image from './CachedImage';
import { Screen } from 'App/Helpers';
import { PropTypes } from 'prop-types';
import { RoundButton as Button } from '@ublocks-react-native/component';

export default class ImageButton extends React.PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
    color: PropTypes.string,
    icon: PropTypes.string,
    image: PropTypes.any,
    source: PropTypes.any,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    round: PropTypes.bool,
    outline: PropTypes.bool,
    disabled: PropTypes.bool,
    imageStyle: PropTypes.object,
    throttleTime: PropTypes.number,
    blurRadius: PropTypes.number,
    imageWidth: PropTypes.number,
    imageHeight: PropTypes.number,
    resizeMode: PropTypes.string,
    imageSize: PropTypes.string,
    imageType: PropTypes.string,
  };

  static defaultProps = {
    style: {},
    width: null,
    color: null,
    round: false,
    outline: false,
    disabled: false,
    imageStyle: {},
    onPress: () => {},
    throttleTime: 1500,
    resizeMode: 'contain',
    imageSize: undefined,
    imageType: undefined,
  };

  render() {
    const {
      onPress,
      style,
      disabled,
      image,
      source,
      imageStyle,
      blurRadius,
      throttleTime,
      imageWidth,
      imageHeight,
      resizeMode,
      imageSize,
      imageType,
      ...props
    } = this.props;
    return (
      <Button
        {...props}
        transparent
        disabled={disabled}
        style={style}
        onPress={onPress}
        throttleTime={throttleTime}
      >
        {(source || image) && (
          <Image
            source={source || image}
            blurRadius={blurRadius}
            imageSize={imageSize}
            imageType={imageType}
            resizeMode={resizeMode}
            style={[
              {
                justifyContent: 'center',
                alignItems: 'center',
                height: imageHeight ? Screen.scale(imageHeight) : '100%',
                width: imageWidth ? Screen.scale(imageWidth) : '100%',
                padding: Screen.scale(4),
              },
              imageStyle,
            ]}
          />
        )}
      </Button>
    );
  }
}
