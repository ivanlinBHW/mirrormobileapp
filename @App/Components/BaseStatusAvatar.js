import React from 'react';
import { isString, isObject } from 'lodash';
import { Image, View } from 'react-native';
import { PropTypes } from 'prop-types';
import { CachedImage } from 'App/Components';
import { Colors, Images } from 'App/Theme';
import { ScaledSheet } from 'App/Helpers';

const getSource = (uri) => {
  if (isString(uri)) {
    return { uri };
  }
  if (isObject(uri)) {
    return uri;
  }
  return Images.defaultAvatar;
};

const BaseStatusAvatar = ({ uri, isOnline, size = 'small', ...props }) => (
  <View style={[size === 'small' ? styles.wrapper : styles.largeWrapper]}>
    <CachedImage
      source={getSource(uri)}
      errorImage={Images.defaultAvatar}
      style={[size === 'small' ? styles.avatarImg : styles.largeAvatarImg]}
    />
    <View
      style={[
        size === 'small' ? styles.baseCircle : styles.largeBaseCircle,
        { backgroundColor: isOnline ? Colors.weird_green : Colors.gray_02 },
      ]}
    />
  </View>
);

const styles = ScaledSheet.create({
  avatarImg: {
    width: '40@s',
    height: '40@s',
    borderRadius: '20@s',
  },
  largeAvatarImg: {
    width: '80@s',
    height: '80@s',
    borderRadius: '40@s',
  },
  baseCircle: {
    width: '8@s',
    height: '8@s',
    borderRadius: '4@s',
    position: 'absolute',
    bottom: 0,
    right: 2,
  },
  largeBaseCircle: {
    width: '16@s',
    height: '16@s',
    borderRadius: '8@s',
    position: 'absolute',
    bottom: 0,
    right: 2,
  },
  wrapper: {
    width: '40@s',
    height: '40@s',
  },
  largeWrapper: {
    width: '80@s',
    height: '80@s',
  },
});

BaseStatusAvatar.propTypes = {
  uri: PropTypes.string,
  isOnline: PropTypes.bool,
  size: PropTypes.string,
};

BaseStatusAvatar.defaultProps = {
  isOnline: true,
  size: 'small',
};

export default BaseStatusAvatar;
