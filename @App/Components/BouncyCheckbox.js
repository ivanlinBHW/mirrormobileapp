import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Animated, Easing, TouchableOpacity } from 'react-native';
import Icon from 'react-native-dynamic-vector-icons';
import { ScaledSheet } from 'App/Helpers';
import { Metrics } from 'App/Theme';

const styles = ScaledSheet.create({
  container: {
    marginRight: Metrics.baseMargin,
    alignItems: 'center',
    flexDirection: 'row',
  },
});

const iconContainer = (
  size,
  checked,
  borderRadius,
  borderColor,
  fillColor,
  unfillColor,
) => {
  return {
    width: size,
    borderColor,
    borderRadius,
    height: size,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: checked ? fillColor : unfillColor,
  };
};

class BouncyCheckbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false,
      springValue: new Animated.Value(1),
    };
  }

  spring = () => {
    const { onPress, disabled } = this.props;
    if (!disabled) {
      const { springValue } = this.state;
      springValue.setValue(0.7);
      Animated.spring(springValue, {
        toValue: 1,
        friction: 3,
      }).start();
      if (onPress) onPress();
    }
  };

  renderCheckIcon = () => {
    const { springValue } = this.state;
    const {
      checkboxSize,
      borderColor,
      fillColor,
      borderRadius,
      unfillColor,
      isChecked,
    } = this.props;
    return (
      <Animated.View
        style={[
          iconContainer(
            checkboxSize,
            isChecked,
            borderRadius,
            borderColor,
            fillColor,
            unfillColor,
          ),
          { transform: [{ scale: springValue }] },
        ]}
      >
        <Icon
          {...this.props}
          name="check"
          type="Entypo"
          size={15}
          color={isChecked ? 'white' : 'transparent'}
        />
      </Animated.View>
    );
  };

  render() {
    const { disabled } = this.props;
    return (
      <TouchableOpacity
        disabled={disabled}
        style={styles.container}
        onPress={this.spring.bind(this, Easing.bounce)}
      >
        {this.renderCheckIcon()}
      </TouchableOpacity>
    );
  }
}

BouncyCheckbox.propTypes = {
  borderRadius: PropTypes.number,
  fontSize: PropTypes.number,
  isChecked: PropTypes.bool,
  checkboxSize: PropTypes.number,
  borderColor: PropTypes.string,
  fillColor: PropTypes.string,
  unfillColor: PropTypes.string,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
};

BouncyCheckbox.defaultProps = {
  fontSize: 16,
  checkboxSize: 25,
  borderRadius: 25 / 2,
  isChecked: false,
  text: '',
  textColor: '#757575',
  fillColor: '#ffc484',
  borderColor: '#ffc484',
  unfillColor: 'transparent',
  fontFamily: 'JosefinSans-Regular',
  disabled: false,
};

export default BouncyCheckbox;
