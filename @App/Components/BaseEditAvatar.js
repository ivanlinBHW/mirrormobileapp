import React from 'react';
import { Image } from 'react-native';
import { PropTypes } from 'prop-types';

import { ScaledSheet } from 'App/Helpers';
import { BaseButton, CachedImage } from 'App/Components';
import { Images, Metrics } from 'App/Theme';

const BaseEditAvatar = ({ uri, onPress, isOnline, ...props }) => (
  <BaseButton onPress={onPress} transparent style={styles.wrapper}>
    <CachedImage
      source={uri || Images.defaultAvatar}
      style={styles.avatarImg}
      errorImage={Images.defaultAvatar}
    />
    <Image source={Images.member_edit} style={styles.baseCircle} />
  </BaseButton>
);

const styles = ScaledSheet.create({
  avatarImg: {
    width: '40@s',
    height: '40@s',
    borderRadius: '20@s',
  },
  baseCircle: {
    width: Metrics.baseMargin,
    height: Metrics.baseMargin,
    borderRadius: Metrics.baseMargin / 2,
    position: 'absolute',
    top: 0,
    right: -1,
    resizeMode: 'contain',
  },
  wrapper: {
    width: '40@s',
    height: '40@s',
  },
});

BaseEditAvatar.propTypes = {
  uri: PropTypes.string,
  isOnline: PropTypes.bool,
  onPress: PropTypes.func,
};

BaseEditAvatar.defaultProps = {
  size: 24,
  isOnline: true,
  onPress: () => {},
};

export default BaseEditAvatar;
