import React from 'react';
import { Text, View, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  flatBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  text: {
    width: 200,
    marginTop: 10,
  },
  iconStyle: {
    marginTop: 60,
  },
});

export default class EmptyComponent extends React.PureComponent {

  render() {
    return (
      <View style={styles.flatBox}>
        <Icon name="hand-paper" size={60} style={styles.iconStyle} />
        <Text style={styles.text}>Oops! Looks like something went wrong.</Text>
        <Text style={styles.text}> Please try again later!!!</Text>
        <Button title="TRY AGAIN" />
      </View>
    );
  }
}
