import React from 'react';
import PropTypes from 'prop-types';
import { Platform } from 'react-native';
import { CalendarProvider } from 'react-native-calendars';

import { Date as d, ScaledSheet } from 'App/Helpers';
import { Colors, Global } from 'App/Theme';
import { ExpandableCalendar } from 'App/Components';

const styles = ScaledSheet.create({
  androidShadowBar: {
    elevation: 5,
    marginTop: 0,
    marginBottom: 10,
    zIndex: 1,
    height: 1,
    backgroundColor: Colors.white,
  },
  androidShadowBlocker: {
    marginTop: -10,
    zIndex: 5,
    height: 15,
    backgroundColor: Colors.white,
  },
  calendar: {
  },
  calendarStyle: {
    backgroundColor: Colors.white,
    elevation: -20,
    zIndex: 5,
    marginTop: 0,
    paddingTop: 0,

    shadowColor: Colors.black5,
    shadowOffset: { height: '10@vs', width: 0 },
    shadowOpacity: 0.4,
    shadowRadius: 8,
  },
});

export default class LiveCalendar extends React.PureComponent {
  static propTypes = {
    date: PropTypes.array,
    dateTitle: PropTypes.string,
    calendarStyle: PropTypes.any,
    calendarTheme: PropTypes.object,
    calendarProviderStyle: PropTypes.any,
    calendarProviderTheme: PropTypes.object,
    firstDay: PropTypes.number,
    markedDates: PropTypes.object,
    hideKnob: PropTypes.bool,
    disablePan: PropTypes.bool,
    horizontal: PropTypes.bool,
    hideArrows: PropTypes.bool,
    showTodayButton: PropTypes.bool,
    disabledOpacity: PropTypes.number,
    onDateChanged: PropTypes.func,
    onMonthChange: PropTypes.func,
    children: PropTypes.object,
    themeColor: PropTypes.string,
    expanded: PropTypes.bool,
    allowShadow: PropTypes.bool,
    allowAndroidShadow: PropTypes.bool,
    androidShadowStyle: PropTypes.object,
    leftArrowImageSource: PropTypes.any,
    onDayPress: PropTypes.func,
    rightArrowImageSource: PropTypes.any,
    context: PropTypes.object,
    addWeek: PropTypes.func,
  };

  static defaultProps = {
    dateTitle: '',
    date: '',
    calendarProviderStyle: {
      marginTop: 0,
      paddingTop: 0,
      display: 'flex',
    },
    calendarStyle: {},
    firstDay: 1,
    markedDates: {},
    calendarTheme: {},
    hideKnob: false,
    disablePan: false,
    horizontal: true,
    hideArrows: false,
    showTodayButton: false,
    disabledOpacity: 0.7,
    onDateChanged: () => {},
    onMonthChange: () => {},
    calendarProviderTheme: {},
    children: null,
    themeColor: Colors.goldenYellow,
    expanded: false,
    allowShadow: false,
    allowAndroidShadow: false,
    androidShadowStyle: {},
    leftArrowImageSource: undefined,
    rightArrowImageSource: undefined,
  };

  state = {
    current: d.formatDate(new Date(), 'YYYY-MM-DD'),
  };

  getCalendarTheme = () => {
    const { calendarTheme, themeColor } = this.props;
    const lightThemeColor = '#e6efff';
    const disabledColor = '#a6acb1';
    const black = '#20303c';
    const white = '#ffffff';

    return {
      arrowColor: black,
      arrowStyle: { padding: 0 },
      monthTextColor: black,
      textMonthFontSize: 16,
      textMonthFontFamily: Global.TextProps.style.fontFamily,
      textMonthFontWeight: 'bold',
      textSectionTitleColor: black,
      textDayHeaderFontSize: 12,
      textDayHeaderFontFamily: Global.TextProps.style.fontFamily,
      textDayHeaderFontWeight: 'normal',
      todayBackgroundColor: lightThemeColor,
      todayTextColor: themeColor,
      dayTextColor: Colors.black,
      textDayFontSize: 18,
      textDayFontFamily: Global.TextProps.style.fontFamily,
      textDayFontWeight: '500',
      textDayStyle: {
        marginTop: Platform.OS === 'android' ? 2 : 4,
      },
      selectedDayBackgroundColor: themeColor,
      selectedDayTextColor: white,
      textDisabledColor: disabledColor,
      dotColor: themeColor,
      selectedDotColor: white,
      disabledDotColor: disabledColor,
      dotStyle: { marginTop: -2 },
      ...calendarTheme,
    };
  };

  onDateChanged = (date) => {
    const { onDateChanged } = this.props;
    onDateChanged(date);
  };

  onMonthChange = () => {
    console.log('date');
  };

  getCalendarProviderTheme = () => {
    const { calendarProviderTheme } = this.props;
    return {
      todayButtonTextColor: Colors.primary,
      ...calendarProviderTheme,
    };
  };

  render() {
    const {
      dateTitle,
      calendarStyle,
      calendarProviderStyle,
      firstDay,
      markedDates,
      disabledOpacity,
      leftArrowImageSource,
      rightArrowImageSource,
      onDayPress,
      context,
      addWeek,
    } = this.props;
    return (
      <CalendarProvider
        theme={this.getCalendarProviderTheme()}
        disabledOpacity={disabledOpacity}
        style={calendarProviderStyle}
      >
        <ExpandableCalendar
          titleText={dateTitle}
          horizontal={false}
          onDayPress={onDayPress}
          context={context}
          addWeek={addWeek}
          disablePan={false}
          hideKnob={false}
          initialPosition={ExpandableCalendar.positions.CLOSED}
          firstDay={firstDay}
          markedDates={markedDates}
          theme={this.getCalendarTheme()}
          style={[styles.calendarStyle, calendarStyle]}
          headerStyle={styles.calendar}
          leftArrowImageSource={leftArrowImageSource}
          rightArrowImageSource={rightArrowImageSource}
        />
      </CalendarProvider>
    );
  }
}
