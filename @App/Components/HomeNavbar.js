import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Badge } from 'react-native-elements';
import { View, SafeAreaView } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { PrimaryNavbar, AutoHideViewHoc } from 'App/Components';
import { Images, Styles, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet } from 'App/Helpers';
import ImageButton from './ImageButton';

const styles = ScaledSheet.create({
  container: {
    ...Styles.screen.container,
    backgroundColor: Colors.white,
    flex: 1,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  btn: {
    width: '30@s',
    height: '30@s',
    marginLeft: '24@vs',
  },
  imgStyle: {
    width: '30@s',
    height: '30@s',
  },
  badge: {
    position: 'absolute',
    bottom: 5,
    right: 0,
  },
});

const HomeNavbar = ({ hasUnread }) => (
  <SafeAreaView style={styles.container}>
    <PrimaryNavbar
      navText={t('tab_home')}
      navComponent={
        <View style={styles.wrapper}>
          <ImageButton
            style={styles.btn}
            imageStyle={styles.imgStyle}
            image={Images.search}
            onPress={Actions.SearchScreen}
          />
          <ImageButton
            style={styles.btn}
            imageStyle={styles.imgStyle}
            image={Images.notification}
            onPress={Actions.NotificationScreen}
          />
          {hasUnread && <Badge status="error" badgeStyle={styles.badge} />}
        </View>
      }
    />
  </SafeAreaView>
);

HomeNavbar.propTypes = {
  hasUnread: PropTypes.bool.isRequired,
};

export default connect((state) => ({
  programId: state.user.activeTrainingProgramId,
}))(
  AutoHideViewHoc(
    connect((state) => ({
      hasUnread: state.notification.hasUnread,
    }))(HomeNavbar),
  ),
);
