import React from 'react';
import { Text, View } from 'react-native';
import { PropTypes } from 'prop-types';
import { RoundButton } from '@ublocks-react-native/component';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  fragment: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
  },
  children: {
    flex: 1,
  },
  text: {
    flex: 1,
  },
});

export default class IconText extends React.PureComponent {
  static propTypes = {
    url: PropTypes.string,
    text: PropTypes.string,
    layoutStyle: PropTypes.object,
  };

  render() {
    const { text, layoutStyle } = this.props;
    return (
      <RoundButton translucent style={[styles.fragment, layoutStyle]} onPress={() => {}}>
        <View style={styles.children}>
          {/* <Image source={{ uri: url }} /> */}
          <Icon name="baseball-ball" size={36} />
        </View>
        <View style={styles.text}>
          <Text>{text}</Text>
        </View>
      </RoundButton>
    );
  }
}
