import React from 'react';
import { Text, Image } from 'react-native';
import { PropTypes } from 'prop-types';
import { RoundButton as Button } from '@ublocks-react-native/component';

import { ScaledSheet } from 'App/Helpers';
import { Styles } from 'App/Theme';
import Icon from 'react-native-vector-icons/FontAwesome5';

const styles = ScaledSheet.create(Styles.RoundButton);

export default class RoundButton extends React.PureComponent {
  static propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    color: PropTypes.string,
    icon: PropTypes.string,
    image: PropTypes.any,
    style: PropTypes.any,
    width: PropTypes.number,
    round: PropTypes.bool,
    outline: PropTypes.bool,
    imageStyle: PropTypes.object,
    uppercase: PropTypes.bool,
    bold: PropTypes.bool,
    textStyle: PropTypes.object,
    textColor: PropTypes.string,
    disabled: PropTypes.bool,
    throttleTime: PropTypes.number,
  };

  static defaultProps = {
    style: {},
    width: null,
    color: null,
    round: false,
    bold: false,
    outline: false,
    imageStyle: {},
    textStyle: {},
    uppercase: true,
    disabled: false,
    throttleTime: 0,
  };

  render() {
    const {
      onPress,
      text,
      style,
      color,
      round,
      outline,
      icon,
      bold,
      image,
      uppercase,
      imageStyle,
      textColor,
      textStyle,
      disabled,
      throttleTime,
    } = this.props;

    return (
      <Button
        translucent
        transparent
        style={[
          styles.button,
          outline && styles.buttonOutline,
          round && styles.buttonRound,
          round && { borderRadius: 22 },
          color && { backgroundColor: color },
          style,
        ]}
        onPress={onPress}
        disabled={disabled}
        throttleTime={throttleTime}
      >
        {icon && <Icon name={icon} size={30} style={styles.icon} />}
        {image && (
          <Image source={image} style={[styles.image, imageStyle]} resizeMode="contain" />
        )}
        <Text
          style={[
            styles.text,
            outline && styles.textOutline,
            uppercase && styles.uppercase,
            bold && styles.bold,
            textStyle,
            textColor && { color: textColor },
          ]}
        >
          {text}
        </Text>
      </Button>
    );
  }
}
