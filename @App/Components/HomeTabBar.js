import React from 'react';
import Emitter from 'tiny-emitter/instance';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import { PropTypes } from 'prop-types';
import { Actions } from 'react-native-router-flux';
import { Text, View, TouchableOpacity } from 'react-native';
import AutoHideViewHoc from 'App/Components/AutoHideViewHoc';

import { Fonts, Colors, Metrics, Classes } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { StyleSheet } from 'App/Helpers';

const styles = StyleSheet.create({
  baseTab: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin / 2,
    marginHorizontal: Metrics.baseMargin,
    marginVertical: Metrics.baseVerticalMargin,
    backgroundColor: Colors.tabBar.background,
    borderRadius: '40@s',
    zIndex: 500,
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    zIndex: 500,
  },
  focusTab: {
    backgroundColor: Colors.tabBar.focus,
    borderRadius: 40,
    shadowColor: Colors.tabBar.shadow,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    elevation: 4,
  },
  text: {
    ...(Platform.isPad ? Fonts.style.extraSmall500 : Fonts.style.small500),
    lineHeight: '30@vs',
    color: Colors.tabBar.text,
  },
  focusText: {
    color: Colors.white,
  },
  bar: {
    backgroundColor: Colors.white,
    borderBottomColor: Colors.black_15,
    borderBottomWidth: 1,
  },
});

class HomeTabBar extends React.PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  render() {
    const { navigation } = this.props;
    const { state } = navigation;

    return (
      <View style={Classes.fill}>
        <View style={styles.baseTab}>
          {state.routes.map((element, index) => (
            <TouchableOpacity
              style={[styles.tab, state.index === index && styles.focusTab]}
              key={element.key}
              onPress={() => {
                requestAnimationFrame(() => {
                  Actions[element.key]();
                });
                setTimeout(() => {
                  requestAnimationFrame(() => {
                    Emitter.emit('onListReset');
                  });
                }, 50);
              }}
            >
              <Text style={[styles.text, state.index === index && styles.focusText]}>
                {t(`__${element.key.toLowerCase()}`)}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.bar} />
        {/* TODO what is this?*/}
      </View>
    );
  }
}

export default connect((state) => ({
  programId: state.user.activeTrainingProgramId,
}))(AutoHideViewHoc(HomeTabBar, Metrics.homeNavBarHeight));
