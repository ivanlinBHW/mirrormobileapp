import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { Badge } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import  MirrorActions,{ MirrorOptions} from 'App/Stores/Mirror/Actions';
import { Classes, Images, Colors, Metrics, Fonts } from 'App/Theme';
import { ImageButton } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { StyleSheet } from 'App/Helpers';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';

const styles = StyleSheet.create({
  flatBox: {
    flexDirection: 'row',
    paddingTop: '8@vs',
    paddingBottom: '8@vs',
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_10,
    alignItems: 'center',
  },
  title: {
    fontSize: Fonts.size.medium,
    letterSpacing: 1,
    lineHeight: '30@vs',
  },
  badge: {
    width: '12@sr',
    height: '12@sr',
    borderRadius: '12@sr',
  },
  badgeBox: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    lineHeight: '30@vs',
    height: '30@vs',
    width: '30@s',
    marginRight: Metrics.baseMargin,
  },
  header: {
    marginLeft: '16@vs',
    fontSize: Fonts.size.medium,
    marginVertical: Metrics.baseMargin,
  },
  headerBox: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: '13@s',
  },
  btnRefresh: {
    width: '20@s',
    height: '20@vs',
  },
  imgStyle: {
    width: '20@s',
    height: '20@vs',
    marginTop: '6@vs',
  },
  wrapper: {
    flex: 1,
  },
  img: {
    width: '32@s',
    height: '32@s',
    marginLeft: Metrics.baseMargin,
    marginRight: '40@s',
  },
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
});

class BluetoothAudioDevicePanel extends React.PureComponent {
  static propTypes = {
    onMirrorCommunicate: PropTypes.func.isRequired,
    onMirrorChangeAudioOutputDevice: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    outputOtherDevices: PropTypes.array,
    outputPairedDevices: PropTypes.array,
    outputDeviceType: PropTypes.string,
    outputDeviceData: PropTypes.object,
    activeDevice: PropTypes.object,
    isScanning: PropTypes.bool,
    connectBluetoothSpeakerSuccess: PropTypes.bool,
    updateConnectBluetoothSpeakerSuccess:PropTypes.func.isRequired,
  };

  static defaultProps = {
    outputDeviceData: {},
    outputPairedDevices: [],
    outputOtherDevices: [],
    outputDeviceType: '',
    activeDevice: null,
    isScanning: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { outputDeviceData = {}, outputDeviceType } = nextProps;
    const { activeDevice } = prevState;

    console.log("=== outputDeviceData ===",outputDeviceData)
    if (nextProps.activeDevice !== prevState.activeDevice) {
      if (nextProps.activeDevice !== 'bluetooth-speaker') {
        return {
          activeDevice: nextProps.activeDevice,
        };
      }
    }

    return null;
  }

  state = {
    connectingDevice :null,
    activeDevice: null,
    isConnecting:false
  };

  componentDidMount() {
    __DEV__ && console.log('@BluetoothAudioDevicePanel');
    this.onRefreshBluetoothDevice();
  }


  componentDidUpdate(prevProps) {
    const {connectBluetoothSpeakerSuccess} = this.props

    console.log("=== activeDevice ===",this.state.activeDevice)


    if(prevProps.connectBluetoothSpeakerSuccess !== connectBluetoothSpeakerSuccess && connectBluetoothSpeakerSuccess){
      console.log("=== outputDeviceType ===",connectBluetoothSpeakerSuccess)

      console.log("=== connectingDevice ===", this.state.connectingDevice)

      this.setState({
        activeDevice: {...this.state.connectingDevice},
        connectingDevice: null,
        isConnecting: false
      });
    }
  }

  onPressActiveDevice = (item) => () => {
    const { updateConnectBluetoothSpeakerSuccess} = this.props;
    const {connectingDevice,isConnecting}=this.state

    console.log("=== isConnecting ===" , isConnecting)

    if(connectingDevice === null){

      const device = {
        deviceName: item.deviceName,
        deviceId: item.deviceId,
      };
  

      updateConnectBluetoothSpeakerSuccess(false);


      this.setState({
        activeDevice:null,
        connectingDevice: device,
        isConnecting:true
      });
      

      setTimeout(()=> {
        console.log('=== check isConnecting ===',this.state.isConnecting)
        if(this.state.isConnecting){
          this.setState({
            activeDevice:null,
            connectingDevice: null,
            isConnecting:false
          });

          this.props.onMirrorCommunicate(MirrorEvents.OUTPUT_AUDIO_DEVICE, {
            type: 'mirror-speaker',
          });
          this.props.onMirrorChangeAudioOutputDevice(null);
        } 


      },30000)

      this.props.onMirrorCommunicate(MirrorEvents.OUTPUT_AUDIO_DEVICE, {
        type: 'bluetooth-speaker',
        device,
      });


      this.props.onMirrorChangeAudioOutputDevice(device);
    }
  };

  onRefreshBluetoothDevice = () => {
    this.props.onMirrorCommunicate(MirrorEvents.REQUEST_BLUETOOTH_AUDIO_DEVICES);
  };

  getIsConnecting = (outputDeviceType, item) => {
    const { connectingDevice = null } = this.state;
    if (
      connectingDevice &&
      connectingDevice.deviceId === item.deviceId &&
      outputDeviceType === 'bluetooth-speaker'
    ) {
      return true;
    } else if (connectingDevice !== 'bluetooth-speaker') {
      return false;
    }
  };

  getIsActive = (outputDeviceType, item) => {
    const { activeDevice = null } = this.state;
    if (
      activeDevice &&
      activeDevice.deviceId === item.deviceId &&
      outputDeviceType === 'bluetooth-speaker'
    ) {
      return true;
    } else if (outputDeviceType !== 'bluetooth-speaker') {
      return false;
    }
  };

  renderItem = ({ item, index }) => {
    const { outputDeviceType } = this.props;
    return (
      <TouchableOpacity style={styles.flatBox} onPress={this.onPressActiveDevice(item)} disabled={this.state.isConnecting}>
        <Image source={Images.bluetooth} style={styles.img} />
        <Text style={styles.title}>
          {item.deviceName ? item.deviceName : item.deviceId}
        </Text>
        {this.getIsActive(outputDeviceType, item) &&
          <View style={styles.badgeBox}>
            <Badge status="success" badgeStyle={styles.badge} />
          </View>
        }
        {this.getIsConnecting(outputDeviceType, item)  &&
          <View style={styles.badgeBox}>
          <ActivityIndicator
              size="small"
              color="black"
              style={styles.badge}
          />
        </View>
        }
      </TouchableOpacity>
    );
  };

  renderInternalSpeaker = () => {
    const { outputDeviceType } = this.props;
    const connectInternalSpeaker = () => {
      this.props.onMirrorCommunicate(MirrorEvents.OUTPUT_AUDIO_DEVICE, {
        type: 'mirror-speaker',
      });
      this.props.onMirrorChangeAudioOutputDevice(null);
    };
    return (
      <TouchableOpacity style={styles.flatBox} onPress={connectInternalSpeaker} disabled={this.state.isConnecting}>
        <Image source={Images.bluetooth} style={styles.img} />
        <Text style={styles.title}>{t('audio_mirror_speaker')}</Text>
        {outputDeviceType !== 'bluetooth-speaker' && (
          <View style={styles.badgeBox}>
            <Badge status="success" badgeStyle={styles.badge} />
          </View>
        )}
      </TouchableOpacity>
    );
  };

  render() {
    const { outputPairedDevices, outputOtherDevices, isScanning } = this.props;
    return (
      <View style={Classes.fill}>
        <Text style={styles.headerTitle}>{t('setting_audio_output_title')}</Text>
        {this.renderInternalSpeaker()}
        <View style={styles.wrapper}>
          <FlatList
            ListHeaderComponent={
              outputPairedDevices.length > 0 && (
                <View style={styles.headerBox}>
                  <Text style={styles.header}>{t('heart_rate_pair_devices')}</Text>
                  {isScanning ? (
                      <ActivityIndicator
                        size="small"
                        color="black"
                        style={styles.imgStyle}
                      />
                    ) : (
                      null
                    )}
                </View>
              )
            }
            data={outputPairedDevices}
            keyExtractor={(item) => item.deviceId}
            renderItem={this.renderItem}
            ListFooterComponent={
              <FlatList
                ListHeaderComponent={
                  <View style={styles.headerBox}>
                    <Text style={styles.header}>{t('heart_rate_other_devices')}</Text>

                    {isScanning ? (
                      <ActivityIndicator
                        size="small"
                        color="black"
                        style={styles.imgStyle}
                      />
                    ) : (
                      <ImageButton
                        style={styles.btnRefresh}
                        imageStyle={styles.imgStyle}
                        image={Images.refresh}
                        onPress={this.onRefreshBluetoothDevice}
                      />
                    )}
                  </View>
                }
                data={outputOtherDevices}
                keyExtractor={(item) => item.deviceId}
                renderItem={this.renderItem}
              />
            }
          />
        </View>
      </View>
    );
  }
}

export default connect(
  (state) => (
    {
      outputDeviceType: state.mirror.options[MirrorOptions.OUTPUT_AUDIO_DEVICE],
      connectBluetoothSpeakerSuccess : state.mirror.options.connectBluetoothSpeakerSuccess
  }),
  (dispatch) =>
    bindActionCreators(
      {
        updateConnectBluetoothSpeakerSuccess : MirrorActions.updateConnectBluetoothSpeakerSuccess
      },
      dispatch,
    ),
)(BluetoothAudioDevicePanel);
