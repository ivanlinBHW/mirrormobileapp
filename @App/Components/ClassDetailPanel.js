import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Image,
  FlatList,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { StripeView } from '@ublocks-react-native/component';

import { Date as d } from 'App/Helpers';
import {
  SquareButton,
  BaseImageButton,
  HeadLine,
  BaseReadMore,
  RoundLabel,
  SecondaryNavbar,
  CachedImage as ImageBackground,
} from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Images, Colors, Styles, Classes, Metrics, Fonts } from 'App/Theme';

const styles = ScaledSheet.create({
  navBar: {
    ...Styles.navBar,
  },
  navBox: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  layoutBox: {
    flex: 1,
  },
  container: {
    ...Classes.fill,
  },
  btnBox: {
    width: '100%',
  },
  navVoiceIcon: {
    width: '30@s',
    height: '30@s',
    marginRight: '21@s',
  },
  navSettingIcon: {
    width: '30@s',
    height: '30@vs',
  },
  marginBetween: {
    marginHorizontal: Metrics.baseMargin,
  },
  imgStyle: {
    width: '100%',
    height: '300@vs',
  },
  contentBox: {
    marginTop: Metrics.baseMargin,
  },
  context: {
    paddingHorizontal: '20@s',
    marginTop: Metrics.baseMargin,
    fontSize: Fonts.size.medium,
  },
  equipmentBox: {
    paddingHorizontal: '20@s',
    flexDirection: 'row',
    marginTop: Metrics.baseMargin,
    flex: 1,
    zIndex: 9999,
  },
  instructorBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginHorizontal: Metrics.baseMargin,
    marginTop: Metrics.baseMargin / 2,
  },
  avatarImage: {
    width: '49@s',
    height: '49@s',
    borderRadius: '24.5@s',
  },
  instructorText: {
    ...Fonts.style.medium500,
    marginLeft: Metrics.baseMargin,
  },
  expandText: {
    fontSize: Fonts.size.medium500,
    color: Colors.titleText.primary,
    textAlign: 'right',
    marginTop: Metrics.baseMargin / 2,
  },
  equipments: {
    width: Platform.isPad ? '35@s' : '30@s',
    height: Platform.isPad ? '35@s' : '30@s',
  },
  equipmentsText: {
    ...Fonts.style.extraSmall,
    width: '60@s',
    textAlign: 'center',
  },
  imageBox: {
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: Metrics.baseMargin,
  },
  captureBox: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginTop: '20@vs',
    marginLeft: Metrics.baseMargin,
  },
  captureImg: {
    width: '30@s',
    height: '30@vs',
    marginLeft: '15@vs',
  },
  captureText: {
    ...Fonts.style.extraSmall500,
    width: '64@s',
    textAlign: 'center',
  },
  classImgWidth: {
    width: '100%',
    height: Screen.verticalScale(230),
    overflow: 'hidden',
  },
  classTitle: {
    ...Fonts.style.regular500,
    color: Colors.secondary,
  },
  classBoxStyle: {
    borderRadius: 7.5,
    backgroundColor: Colors.black,
    marginTop: Metrics.baseMargin / 4,
    alignItems: 'center',
  },
  classText: {
    ...Fonts.style.extraSmall500,
    paddingVertical: Metrics.baseMargin / 8,
    paddingHorizontal: Metrics.baseMargin / 2,
    color: Colors.white,
  },
  classLiveBoxStyle: {
    borderRadius: 7,
    width: '55@s',
    height: '15@vs',
    backgroundColor: Colors.white,
    marginBottom: 4,
    alignItems: 'center',
  },
  classLiveText: {
    fontSize: 10,
    fontWeight: '500',
    letterSpacing: 1,

    color: Colors.primary,
  },
  classFragment: {
    flexDirection: 'row',
    flex: 1,
  },
  classLeftBox: {
    flex: 1,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  classRightBox: {
    marginRight: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
    justifyContent: 'flex-end',
  },
  classLeft: {
    flexDirection: 'column',
  },
  classRight: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },
  classTime: {
    fontSize: '11@vs',
    color: '#f0f0f0',
  },
  classSecondTitle: {
    ...Fonts.style.extraSmall500,
    color: Colors.white,
  },
  classSecondIcon: {
    width: '50@vs',
    backgroundColor: 'blue',
    color: Colors.white,
    flexDirection: 'row',
  },
  classEquipments: {
    width: '20@vs',
    height: '20@vs',
  },
  classImageBox: {
    width: '40@vs',
    height: '40@vs',
    borderRadius: '40@vs',
    marginRight: '8@s',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  classEquipmentsBox: {
    flexDirection: 'row',
  },
  classTimeBox: {
    width: '100%',
    height: '13@vs',
    marginBottom: '12@vs',
  },
  stripeBtn: {
    height: '48@vs',
    lineHeight: '48@vs',
  },
  stripeStyle: {
    alignItems: 'center',
    paddingRight: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    height: '48@vs',
    lineHeight: '48@vs',
  },
  stripeText: {
    ...Fonts.style.medium500,
  },
  stripeSecondText: {
    fontSize: Fonts.size.extraSmall,
  },
  stripe: {
    marginTop: 1,
  },
  hrLine: {
    marginBottom: 0,
  },
  stripeImage: {
    width: '30@s',
    height: '30@vs',
    marginRight: Metrics.baseMargin,
  },
  stripeRight: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtCaloriesDesc: {
    color: Colors.gray_02,
  },
});

export default class ClassDetailPanel extends React.PureComponent {
  static propTypes = {
    data: PropTypes.object.isRequired,
    startClass: PropTypes.func.isRequired,
    routeName: PropTypes.string.isRequired,
    activeTrainingProgramId: PropTypes.string,
    instructor: PropTypes.object,
    requiredEquipments: PropTypes.array,
    trainingSteps: PropTypes.array,
    resumeClass: PropTypes.func.isRequired,
    isOnlySee: PropTypes.bool,
    onPressBack: PropTypes.func,
    onPressInstructor: PropTypes.func,
    onPressBackTo: PropTypes.string,
    isConnected: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    instructor: {},
    requiredEquipments: [],
    trainingSteps: [],
    onPressBackTo: '',
    isOnlySee: false,
    onPressBack: () => {},
    onPressInstructor: () => {},
  };

  renderStripesView = (list) =>
    list.map((item) => ({
      leftComponent: <Text style={styles.stripeText}>{item.title}</Text>,
      rightComponent: (
        <View style={styles.stripeRight}>
          {item.captureSetting && (
            <Image source={Images.motion_capture} style={styles.stripeImage} />
          )}
          <Text style={styles.stripeSecondText}>{d.mmss(item.duration)}</Text>
        </View>
      ),
      disabled: true,
    }));

  renderItem = ({ item, index }) => {
    return (
      <View key={index} style={styles.imageBox}>
        <Image
          source={{
            uri: typeof item.signedCoverImage === 'string' ? item.signedCoverImage : '',
          }}
          style={styles.equipments}
        />
        <Text style={styles.equipmentsText}>{item.title}</Text>
      </View>
    );
  };

  renderClassCard = () => {
    const { data } = this.props;
    return (
      <ImageBackground
        resizeMode="cover"
        source={data.signedCoverImageObj || { uri: data.signedCoverImage }}
        style={styles.classImgWidth}
        imageSize="2x"
        imageType="h"
      >
        <View style={styles.classFragment}>
          <View style={styles.classLeftBox}>
            <View style={styles.classLeft}>
              <Text style={styles.classSecondTitle}>{t('__class')}</Text>
              <Text style={styles.classTitle}>
                {_.isString(data.title) && data.title.toUpperCase()}
              </Text>
            </View>
          </View>
          <View style={styles.classRightBox}>
            <View style={styles.classRight}>
              <RoundLabel text={t(`search_lv${data.level}`)} />
              <RoundLabel text={`${Math.round(data.duration / 60)} ${t('min')}`} />
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  };

  handleStartButton = () => {
    const { data, routeName, activeTrainingProgramId, startClass } = this.props;
    if (routeName === 'ProgramClassDetail') {
      if (
        !_.isEmpty(activeTrainingProgramId) &&
        !_.isEmpty(data.trainingProgramClassHistory) &&
        data.trainingProgramClassHistory.status === 0 &&
        data.trainingProgramClassHistory.pausedStepId === null
      ) {
        return (
          <SquareButton
            color={Colors.primary}
            onPress={startClass}
            text={t('class_detail_start_class')}
          />
        );
      } else if (
        !_.isEmpty(activeTrainingProgramId) &&
        (data.trainingProgramClassHistory &&
          data.trainingProgramClassHistory.pausedStepId !== null)
      ) {
        return (
          <SquareButton
            color={Colors.black}
            onPress={startClass}
            text={t('class_detail_start_class')}
          />
        );
      }
    } else {
      if (data.trainingClassHistory && data.trainingClassHistory.pausedStepId) {
        return (
          <SquareButton
            color={Colors.black}
            onPress={startClass}
            text={t('class_detail_start_class')}
          />
        );
      } else {
        return (
          <SquareButton
            color={Colors.primary}
            onPress={startClass}
            text={t('class_detail_start_class')}
          />
        );
      }
    }
  };

  handleResumeButton = () => {
    const { data, routeName, resumeClass } = this.props;

    if (routeName === 'ProgramClassDetail') {
      if (
        !_.isEmpty(data.trainingProgramClassHistory) &&
        data.trainingProgramClassHistory.pausedStepId
      ) {
        return (
          <SquareButton
            onPress={resumeClass}
            color={Colors.primary}
            text={t('class_detail_resume_class')}
          />
        );
      }
    } else {
      if (
        data.trainingClassHistory &&
        !_.isEmpty(data.trainingClassHistory.pausedStepId)
      ) {
        return (
          <SquareButton
            onPress={resumeClass}
            color={Colors.primary}
            text={t('class_detail_resume_class')}
          />
        );
      }
    }
  };

  render() {
    const {
      data,
      instructor,
      requiredEquipments,
      trainingSteps,
      isOnlySee = false,
      onPressBack,
      onPressBackTo = '',
      onPressInstructor,
      isConnected,
    } = this.props;
    const { title = '', description = '', signedAvatarImage = '' } = instructor;
    return (
      <SafeAreaView style={styles.container}>
        <SecondaryNavbar
          back
          backTo={onPressBackTo}
          onPressBack={onPressBack}
          navRightComponent={
            <View style={styles.navBox}>
              {/* <BaseIconButton
                iconType="FontAwesome5"
                iconName="wifi"
                iconColor={isConnected ? 'darkgreen' : 'darkred'}
                iconSize={Platform.isPad ? Screen.scale(8) : Screen.scale(24)}
                onPress={Actions.DeviceModal}
                style={styles.navVoiceIcon}
              /> */}
              <BaseImageButton
                style={styles.navVoiceIcon}
                source={Images.home_black}
                onPress={Actions.DeviceModal}
              />
              <BaseImageButton
                style={styles.navVoiceIcon}
                source={Images.voice}
                onPress={Actions.AudioModal}
                disabled={!isConnected}
              />
              <BaseImageButton
                style={styles.navSettingIcon}
                source={Images.setting}
                onPress={Actions.WorkoutOptionModal}
                disabled={!isConnected}
              />
            </View>
          }
        />
        <View style={styles.layoutBox}>
          <ScrollView style={styles.scrollBox}>
            {this.renderClassCard()}
            {data.hasMotionCapture && (
              <View style={styles.captureBox}>
                <Image source={Images.motion_capture} style={styles.captureImg} />
                <Text style={styles.captureText}>{t('motion_capture')}</Text>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine
                paddingHorizontal={16}
                title={t('class_detail_about_this_workout')}
              />
              <BaseReadMore text={data.description} />
            </View>
            <View style={styles.contentBox}>
              <HeadLine paddingHorizontal={16} title={t('class_detail_good_for_tags')} />
              {data.tags &&
                data.tags.map((e, i) => (
                  <Text key={`${e.tagId}`} style={styles.txtTags}>
                    {e.name}
                  </Text>
                ))}
            </View>
            {(!!data.calories || data.calories !== 0) && (
              <View style={styles.contentBox}>
                <HeadLine
                  paddingHorizontal={16}
                  title={t('class_detail_reference_calories')}
                />
                <Text style={Classes.paddingLeft}>
                  {data.calories} {t('cal')}
                </Text>
                <Text style={[styles.context, styles.txtCaloriesDesc]}>
                  {t('class_detail_calories_description')}
                </Text>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine paddingHorizontal={16} title={t('class_detail_instructor')} />
              <View style={styles.instructorBox}>
                <Image
                  source={{
                    uri: signedAvatarImage
                      ? signedAvatarImage
                      : 'https://via.placeholder.com/368x207',
                  }}
                  style={styles.avatarImage}
                />
                <Text style={styles.instructorText}>{title}</Text>
              </View>
              <Text style={styles.context} numberOfLines={2}>
                {description}
              </Text>
              <TouchableOpacity onPress={onPressInstructor}>
                <Text style={[styles.expandText, styles.marginBetween]}>
                  {t('view_profile')}
                </Text>
              </TouchableOpacity>
            </View>
            {requiredEquipments.length > 0 && (
              <View style={styles.contentBox}>
                <HeadLine
                  paddingHorizontal={16}
                  title={t('class_detail_equipment_needed')}
                />
                <View style={styles.equipmentBox}>
                  <FlatList
                    horizontal
                    data={requiredEquipments}
                    renderItem={this.renderItem}
                  />
                </View>
              </View>
            )}
            <View style={styles.contentBox}>
              <HeadLine
                title={`${trainingSteps.length} ${t('class_detail_exercise')}`}
                hrLineStyle={styles.hrLine}
                paddingHorizontal={16}
              />
              <StripeView
                style={styles.stripe}
                buttonStyle={styles.stripeBtn}
                stripStyle={styles.stripeStyle}
                stripeLightColor={Colors.white}
                stripeDarkColor={Colors.white_02}
                stripes={this.renderStripesView(trainingSteps)}
              />
            </View>
          </ScrollView>
        </View>
        {!isOnlySee && isConnected ? (
          <View style={styles.btnBox}>
            {this.handleResumeButton()}
            {this.handleStartButton()}
          </View>
        ) : (
          <View style={styles.btnBox}>
            <SquareButton
              onPress={Actions.DeviceModal}
              color={Colors.primary}
              text={t('class_detail_connect_device')}
            />
          </View>
        )}
      </SafeAreaView>
    );
  }
}
