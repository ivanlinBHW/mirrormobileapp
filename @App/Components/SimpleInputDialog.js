import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Picker } from 'react-native';
import Dialog from 'react-native-dialog';
import { Colors, Fonts } from 'App/Theme';
import { StyleSheet } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';

const styles = StyleSheet.create({
  picker: { height: '50@vs', width: 'auto' },
  dialogInputWrapper:
    Platform.OS === 'android'
      ? {
          borderBottomWidth: 1,
          borderBottomColor: Colors.black,
        }
      : {},
});

const renderComponent = (item, index) => {
  if (
    !item.type ||
    (typeof item.type === 'string' && item.type.toLowerCase() === 'input')
  ) {
    return (
      <Dialog.Input
        {...item}
        key={index}
        autoCapitalize="none"
        placeholder={item.placeholder}
        wrapperStyle={styles.dialogInputWrapper}
        onChangeText={item.onChangeText}
        value={item.value}
        keyboardType={item.keyboardType || 'default'}
        maxLength={item.maxLength || 63}
      />
    );
  } else if (typeof item.type === 'string' && item.type.toLowerCase() === 'select') {
    return (
      <Picker
        {...item}
        key={index}
        selectedValue={item.value}
        style={styles.picker}
        onValueChange={item.onValueChange}
      >
        {item.items.map((e, i) => (
          <Picker.Item
            {...e}
            key={`item-${i}`}
            label={e.label || e.value}
            value={e.value}
          />
        ))}
      </Picker>
    );
  }
};

const SimpleInputDialog = ({
  visible,
  title,
  description,
  inputFields,
  onConfirmPress,
  onCancelPress,
  isConfirmDisabled,
}) => {
  const getBtnStyle = () => ({ opacity: isConfirmDisabled ? 0.2 : 1 });
  return (
    <Dialog.Container visible={visible}>
      <Dialog.Title style={Fonts.style.input600}>{title}</Dialog.Title>
      {description && <Dialog.Description>{description}</Dialog.Description>}
      {inputFields.map(renderComponent)}
      <Dialog.Button label={t('__cancel')} onPress={onCancelPress} />
      <Dialog.Button
        label={t('__ok')}
        onPress={onConfirmPress}
        style={getBtnStyle()}
        disabled={isConfirmDisabled}
        maxLength
        bold
      />
    </Dialog.Container>
  );
};

SimpleInputDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  inputFields: PropTypes.array.isRequired,
  onConfirmPress: PropTypes.func.isRequired,
  onCancelPress: PropTypes.func.isRequired,
  isConfirmDisabled: PropTypes.bool,
};

SimpleInputDialog.defaultProps = {
  isConfirmDisabled: false,
  description: '',
};

export default SimpleInputDialog;
