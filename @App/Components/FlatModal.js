import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';

import BaseModal from './BaseModal';

export default class FlatModal extends React.PureComponent {
  static propTypes = {
    title: PropTypes.string,
    renderItem: PropTypes.array,
    list: PropTypes.array,
  };

  render() {
    const { list, renderItem, title } = this.props;
    return (
      <BaseModal title={title}>
        <FlatList data={list} renderItem={renderItem} />
      </BaseModal>
    );
  }
}
