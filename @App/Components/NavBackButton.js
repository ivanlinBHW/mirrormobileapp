import React from 'react';
import PropTypes from 'prop-types';
import { Text, Image } from 'react-native';
import { RoundButton } from '@ublocks-react-native/component';
import { Actions } from 'react-native-router-flux';

import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet } from 'App/Helpers';
import { Colors, Images, Fonts } from 'App/Theme';

const styles = ScaledSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 0,
    marginLeft: -4,
    flex: 1,
  },
  img: {
    width: '30@s',
    height: '30@s',
    marginTop: '1@s',
  },
  text: {
    fontSize: Fonts.size.regular,
    color: Colors.button.primary.text.text,
  },
});

export default class NavBackButton extends React.PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
  };

  static defaultProps = {
    onPress: () => Actions.pop(),
  };

  render() {
    const { onPress } = this.props;
    return (
      <RoundButton transparent onPress={onPress} style={styles.wrapper}>
        <Image style={styles.img} source={Images.back_active} />
        <Text style={styles.text}>{t('back')}</Text>
      </RoundButton>
    );
  }
}
