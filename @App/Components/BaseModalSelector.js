import React from 'react';
import PropTypes from 'prop-types';
import ModalSelector from 'react-native-modal-selector';
import { translate as t } from 'App/Helpers/I18n';
import { Colors, Classes } from 'App/Theme';
import { StyleSheet } from 'App/Helpers';

const styles = StyleSheet.create({
  cancelTextStyle: { color: Colors.red },
  optionTextStyle: { ...Classes.margin },
});

export default class BaseModalSelector extends React.PureComponent {
  static propTypes = {
    children: PropTypes.any,
    animationType: PropTypes.string,
    overlayAlign: PropTypes.string,
    cancelTextStyle: PropTypes.object,
    optionTextStyle: PropTypes.object,
  };

  static defaultProps = {
    animationType: 'fade',
    children: null,
    overlayAlign: 'flex-end',
    optionTextStyle: {},
    cancelTextStyle: {},
  };

  render() {
    const {
      children,
      animationType,
      cancelTextStyle,
      optionTextStyle,
      overlayAlign,
      ...props
    } = this.props;
    return (
      <ModalSelector
        {...props}
        optionTextStyle={[styles.optionTextStyle, optionTextStyle]}
        cancelTextStyle={[styles.cancelTextStyle, cancelTextStyle]}
        animationType={animationType}
        cancelText={t('__cancel')}
        overlayStyle={{ justifyContent: overlayAlign }}
      >
        {children}
      </ModalSelector>
    );
  }
}
