import React from 'react';
import { PropTypes } from 'prop-types';
import { Text, View, Platform } from 'react-native';

import { ScaledSheet, Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { Colors, Metrics, Fonts, Images } from 'App/Theme';
import {
  BaseButton as RoundButton,
  CachedImage as ImageBackground,
} from 'App/Components';
import LinearGradient from 'react-native-linear-gradient';

const styles = ScaledSheet.create({
  base: {
    marginLeft: Metrics.baseMargin,
    overflow: 'hidden',
    borderRadius: '12@sr',
    borderWidth: 0,
  },
  sizeBaseNormal: {
    width: Screen.scale(281),
    height: Screen.scale(159),
  },
  sizeBaseLarge: {
    width: Screen.scale(364),
    height: Platform.isPad ? '251@s' : '214@sr',
  },
  sizeBaseFull: {
    height: Platform.isPad ? '251@s' : '214@sr',
    marginRight: Metrics.baseMargin,
  },
  sizeNormal: {
    width: Screen.scale(281),
    height: Screen.scale(159),
  },
  sizeLarge: {
    width: Screen.scale(368),
    height: Platform.isPad ? '251@s' : '214@sr',
  },
  sizeFull: {
    width: '100%',
    height: Platform.isPad ? '251@s' : '214@s',
  },
  normalImgWidth: {
    width: Screen.scale(301),
    height: Screen.scale(159),
    borderRadius: 12,
    overflow: 'hidden',
  },
  largeImgWidth: {
    width: Screen.scale(394),
    height: Platform.isPad ? '251@s' : '214@sr',
    borderRadius: 12,
    overflow: 'hidden',
  },
  fullImgWidth: {
    width: '115%',
    height: Platform.isPad ? '251@s' : '214@sr',
    borderRadius: 12,
    overflow: 'hidden',
    paddingHorizontal: '12@sr',
  },
  title: {
    ...Fonts.style.regular500,
    color: Colors.primary,
    marginLeft: Metrics.baseMargin,
    textTransform: 'uppercase',
  },
  boxStyle: {
    minWidth: '80@s',
    height: 15,
    borderRadius: '7.5@s',
    backgroundColor: Colors.black,
    paddingHorizontal: Metrics.baseMargin / 2,
    marginBottom: 4,
    alignItems: 'center',
  },
  text: {
    ...Fonts.style.extraSmall500,
    color: '#ffffff',
  },
  fragment: {
    flexDirection: 'column',
    backgroundColor: 'red',
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin * 2,
  },
  baseBox: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 100,
    flexDirection: 'column',
    alignContent: 'flex-end',
    width: '100%',
    height: Platform.isPad ? '251@s' : '214@sr',
    borderRadius: '12@sr',
  },
  memberImg: {
    width: '30@s',
    height: '30@s',
  },
  noBorder: {
    borderWidth: 0,
  },
  lastItem: {
    marginRight: Metrics.baseMargin,
  },

  countText: {
    ...Fonts.style.small500,
    color: Colors.gray_04,
    marginLeft: Metrics.baseMargin / 2,
  },
  countBox: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    alignItems: 'center',
  },
  timeText: {
    ...Fonts.style.small500,
    color: Colors.gray_04,
    marginLeft: Metrics.baseMargin,
    marginTop: Metrics.baseMargin / 2,
  },
  bottomBox: {
    flexDirection: 'column',
    flex: 1,
    marginBottom: Metrics.baseMargin,
    justifyContent: 'flex-end',
  },
  signText: {
    ...Fonts.style.small500,
    color: Colors.gray_04,
    marginLeft: Metrics.baseMargin,
    textTransform: 'uppercase',
  },
});

export default class ChannelCard extends React.Component {
  static propTypes = {
    data: PropTypes.object,
    size: PropTypes.string,
    title: PropTypes.string,
    onPressMarked: PropTypes.func,
    onPress: PropTypes.func,
    onPressBookmark: PropTypes.func,
    timezone: PropTypes.string,
    image: PropTypes.any,
    isHover: PropTypes.bool,
    leftBoxStyle: PropTypes.object,
    rightBoxStyle: PropTypes.object,
    bookmarkStyle: PropTypes.object,
    lastItem: PropTypes.bool,
  };

  static defaultProps = {
    isHover: false,
    leftBoxStyle: {},
    rightBoxStyle: {},
    bookmarkStyle: {},
    timezone: 'Asia/Taipei',
    lastItem: false,
    image: undefined,
  };

  getBaseStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.sizeBaseNormal;
      case 'large':
        return styles.sizeBaseLarge;
      case 'full':
        return styles.sizeBaseFull;
      default:
        return styles.sizeBaseNormal;
    }
  };

  getWrapStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.sizeNormal;
      case 'large':
        return styles.sizeLarge;
      case 'full':
        return styles.sizeFull;
      default:
        return styles.sizeNormal;
    }
  };

  getImgStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.normalImgWidth;
      case 'large':
        return styles.largeImgWidth;
      case 'full':
        return styles.fullImgWidth;
      default:
        return styles.sizeNormal;
    }
  };

  render() {
    const { image, data, onPress, lastItem } = this.props;
    return (
      <View style={[styles.base, this.getBaseStyle(), lastItem && styles.lastItem]}>
        <RoundButton
          style={[this.getWrapStyle(), styles.noBorder]}
          onPress={() => onPress(data.id, data)}
          roundRadius={12}
          throttleTime={300}
        >
          <ImageBackground
            resizeMode="cover"
            source={data.isFreeTrial ? Images.free_trial_cover : data.signedCoverImageObj}
            style={[this.getImgStyle()]}
            imageSize="2x"
            imageType="h"
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
              }}
              locations={[0, 1]}
              colors={[`rgba(50,50,50,0)`, `rgba(50,50,50,0.4)`]}
            />
          </ImageBackground>

          <View style={styles.baseBox}>
            <View style={styles.bottomBox}>
              <Text style={styles.signText}>{t('__channel')}</Text>
              <Text style={styles.title}>{data.title}</Text>
            </View>
          </View>
        </RoundButton>
      </View>
    );
  }
}
