import React from 'react';
import { PropTypes } from 'prop-types';
import { Actions } from 'react-native-router-flux';
import {
  Platform,
  Text,
  View,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';

import { Styles, Colors } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet, Screen } from 'App/Helpers';
import Images from 'App/Theme/Images';

const styles = ScaledSheet.create({
  linearGradient: {
    height: Screen.onePixel,
    backgroundColor: Colors.primaryTabBar.background,
    ...Platform.select({
      ios: {
        shadowColor: Colors.primaryTabBar.shadow,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
      },
      android: {
        elevation: 0.7,
      },
    }),
  },
  bottomTab: {
    ...Styles.PrimaryTabBar.bottomTab,
    borderTopWidth: Screen.onePixel,
    borderTopColor: Colors.primaryTabBar.borderTop,
  },
  tab: {
    ...Styles.PrimaryTabBar.tab,
  },
  text: {
    ...Styles.PrimaryTabBar.text,
  },
  icon: {
    color: Colors.primaryTabBar.icon,
  },
  focusIcon: {
    color: Colors.primaryTabBar.focusIcon,
  },
  focusText: {
    ...Styles.PrimaryTabBar.text,
    color: Colors.primaryTabBar.focusText,
  },
  img: {
    width: Platform.isPad ? '18@s' : '23@s',
    height: Platform.isPad ? '18@s' : '23@s',
  },
});

const ImageKey = ['home', 'progress', 'community', 'shop', 'setting'];
export default class PrimaryTabBar extends React.PureComponent {
  static propTypes = {
    navigation: PropTypes.object,
  };

  onPressTabBar = (key) => {
    switch (key) {
      case 'Home':
        if (Actions.currentScene === 'ClassScreen') break;
        Actions.jump('HomeTab');
        break;
      case 'Progress':
        if (Actions.currentScene === 'Progress') break;
        Actions.jump('Progress');
        break;
      case 'Shop':
        if (Actions.currentScene === 'Shop') break;
        Actions.jump('Shop');
        break;
      case 'Community':
        if (Actions.currentScene === 'CommunityScreen') break;
        Actions.jump('CommunityScreen');
        break;
      case 'SETTING':
        if (Actions.currentScene === 'Setting') break;
        Actions.jump('SettingScreen');
        break;
    }
  };

  render() {
    const { navigation } = this.props;
    const { state } = navigation;
    return (
      <SafeAreaView>
        <View style={styles.linearGradient} />
        <View style={styles.bottomTab}>
          {state.routes.map((element, index) => (
            <TouchableOpacity
              style={styles.tab}
              key={element.key}
              onPress={() => this.onPressTabBar(element.key)}
            >
              <Image
                source={
                  state.index === index
                    ? Images[`${ImageKey[index]}_active`]
                    : Images[`${ImageKey[index]}`]
                }
                style={styles.img}
              />
              <Text style={state.index === index ? styles.focusText : styles.text}>
                {t(`tab_${element.key.toLowerCase()}`)}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </SafeAreaView>
    );
  }
}
