import React from 'react';
import { PropTypes } from 'prop-types';
import { Platform } from 'react-native';
import { isString, isEmpty } from 'lodash';
import { Text, View } from 'react-native';
import { RoundButton } from '@ublocks-react-native/component';
import { AppStore, TrackRecordActions } from 'App/Stores';

import { ScaledSheet, Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { Colors, Metrics, Fonts, Images } from 'App/Theme';
import {
  ImageButton,
  CachedImage as Image,
  CachedImage as ImageBackground,
} from 'App/Components';

const styles = ScaledSheet.create({
  base: {
    marginRight: Metrics.baseMargin,
    overflow: 'hidden',
    borderWidth: 0,
    flex: 1,
    width: Platform.isPad ? Screen.scale(214) : Screen.scale(182),
    height: Platform.isPad ? Screen.scale(120) : Screen.scale(102),
  },
  imgWidth: {
    borderRadius: Screen.scale(12),
    width: (Platform.isPad ? Screen.scale(214) : Screen.scale(182)) + Screen.scale(12),
    height: '100%',
    flex: 1,
    borderWidth: 0,
    overflow: 'hidden',
  },
  title: {
    ...Fonts.style.small500,
    letterSpacing: 0.75,
    color: Colors.secondary,
    width: '80%',
  },
  secondTitle: {
    ...Fonts.style.small500,
    letterSpacing: 0.75,
    color: Colors.white,
  },
  boxStyle: {
    borderRadius: Screen.scale(10),
    backgroundColor: Colors.black,
    alignItems: 'center',
    width: Platform.isPad ? Screen.scale(65) : Screen.scale(55),
    minWidth: '80@s',
  },
  text: {
    ...Fonts.style.extraSmall500,
    color: Colors.white,
  },
  fragment: {
    flexDirection: 'row',
    flex: 1,
    paddingTop: Platform.isPad ? Metrics.baseMargin / 2 : Metrics.baseMargin / 4,
    paddingBottom: Platform.isPad ? Metrics.baseMargin : Metrics.baseMargin / 2,
  },
  leftBox: {
    flex: 1,
    paddingLeft: Metrics.baseMargin / 2,
    justifyContent: 'space-around',
  },
  rightBox: {
    flex: 1,
    marginRight: Metrics.baseMargin,
  },
  left: {
    flexDirection: 'column',
    height: '28@vs',
  },
  right: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },
  secondIcon: {
    marginBottom: Metrics.baseMargin / 2,
    marginRight: 10,
  },
  equipments: {
    width: Platform.isPad ? '12@s' : '10@s',
    height: Platform.isPad ? '12@s' : '10@s',
  },
  imageBox: {
    width: Platform.isPad ? '21@s' : '18@s',
    height: Platform.isPad ? '21@s' : '18@s',
    borderRadius: Platform.isPad ? '10.5@s' : '9@s',
    marginRight: Metrics.baseMargin / 2,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  equipmentsBox: {
    minHeight: '18@vs',
    height: '18@vs',
    flexDirection: 'row',
  },
  imgStyle: {
    width: '19@s',
    height: '32@s',
  },
  btnStyle: {
    width: '35@s',
    height: '35@vs',
    position: 'absolute',
    right: Screen.scale(7),
    top: Screen.scale(-8),
    zIndex: 20,
  },
});

export default class LiveCard extends React.PureComponent {
  static propTypes = {
    data: PropTypes.object,
    onPressMarked: PropTypes.func,
    onPress: PropTypes.func,
    onPressBookmark: PropTypes.func.isRequired,
  };

  renderEquipments = () => {
    const { data } = this.props;
    return data.requiredEquipments.map((item, index) => {
      console.log('== item ==', item);
      return (
        <View key={index} style={styles.imageBox}>
          {item.signedCoverImage && typeof item.signedCoverImage === 'string' && (
            <Image
              source={item.signedCoverImageObj || { uri: item.signedCoverImage }}
              style={styles.equipments}
              imageSize="1x"
              imageType="v"
            />
          )}
        </View>
      );
    });
  };

  handleOnPress = () => {
    const { onPress, data } = this.props;

    AppStore.dispatch(
      TrackRecordActions.track({
        trackObjectType: 'LiveClass',
        trackObjectId: data.id,
      }),
    );

    onPress(data.id, data);
  };

  render() {
    const { data, onPressBookmark } = this.props;
    return (
      <RoundButton roundRadius={12} style={styles.base} onPress={this.handleOnPress}>
        <ImageBackground
          source={data.signedCoverImageObj || { uri: data.signedCoverImage }}
          style={styles.imgWidth}
          resizeMode="cover"
          imageSize="2x"
          imageType="h"
        >
          {data.isBookmarked ? (
            <ImageButton
              image={Images.bookmark_solid}
              imageStyle={styles.imgStyle}
              style={styles.btnStyle}
              onPress={() => onPressBookmark(data)}
            />
          ) : (
            <ImageButton
              image={Images.bookmark}
              imageStyle={styles.imgStyle}
              style={styles.btnStyle}
              onPress={() => onPressBookmark(data)}
            />
          )}
          <View style={styles.fragment}>
            <View style={styles.leftBox}>
              <View style={styles.boxStyle}>
                <Text style={styles.text}>{`${t(`search_lv${data.level}`)}`}</Text>
              </View>
              <View style={styles.boxStyle}>
                <Text style={styles.text}>
                  {data.duration / 60} {t('min')}
                </Text>
              </View>
              <View style={styles.equipmentsBox}>{this.renderEquipments()}</View>
              <View style={styles.left}>
                <Text style={styles.title} numberOfLines={2}>
                  {isString(data.title) && data.title.toUpperCase()}
                </Text>
                <Text style={styles.secondTitle}>
                  {!isEmpty(data.instructor) &&
                    isString(data.instructor.title) &&
                    data.instructor.title.toUpperCase()}
                </Text>
              </View>
            </View>
          </View>
        </ImageBackground>
      </RoundButton>
    );
  }
}
