import React from 'react';
import { PropTypes } from 'prop-types';
import { translate as t } from 'App/Helpers/I18n';
import { Text, View, FlatList } from 'react-native';

import { BaseButton, ClassCard } from 'App/Components';
import { ScaledSheet } from 'App/Helpers';
import { Colors, Metrics, Fonts } from 'App/Theme';

const styles = ScaledSheet.create({
  title: {
    ...Fonts.style.medium500,
    color: '#323232',
  },
  seeAllText: {
    ...Fonts.style.small500,
    color: Colors.primary,
    marginTop: Metrics.baseMargin,
  },
  hrLine: {
    borderBottomColor: 'rgba(0, 0, 0, 0.15)',
    borderBottomWidth: 1,
    marginTop: Metrics.baseMargin,
  },
  titleBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  seeallBtn: {
    width: '60@s',
    height: 'auto',
    marginRight: Metrics.baseMargin,
    marginVertical: 0,
    paddingVertical: 0,
  },
  seeallTextColor: {
    ...Fonts.style.small500,
  },
});

export default class ClassTtitleCard extends React.PureComponent {
  static propTypes = {
    data: PropTypes.array,
    size: PropTypes.string,
    onPressMarked: PropTypes.func,
    onPress: PropTypes.func,
    index: PropTypes.number,
    title: PropTypes.string,
    onPressClassDetail: PropTypes.func,
    onPressBookmark: PropTypes.func,
    timezone: PropTypes.string,
    getDataToSeeAll: PropTypes.func.isRequired,
    hrLine: PropTypes.bool,
    count: PropTypes.number.isRequired,
    type: PropTypes.string,
    showMore: PropTypes.bool,
  };

  static defaultProps = {
    timezone: 'Asia/Taipei',
    hrLine: false,
    showMore: false,
    type: '',
    count: 6,
  };

  renderItem = ({ item, index }) => {
    const {
      size,
      onPressClassDetail,
      title,
      onPressBookmark,
      timezone,
      data,
    } = this.props;
    return (
      <ClassCard
        title={title}
        size={size}
        data={item}
        timezone={timezone}
        onPress={onPressClassDetail}
        onPressBookmark={(id) => onPressBookmark(id, item)}
        lastItem={index === 5 || index === data.length - 1}
      />
    );
  };

  onPressSeeAll = (type) => {
    let mode = '';
    switch (type) {
      case 'recommended':
        mode = 'recommended-class';
        break;
      case 'upcomingLive':
        mode = 'upcoming-live-class';
        break;
      case 'hottest':
        mode = 'current-hottest-class';
        break;
      case 'newArrival':
        mode = 'new-arrival-class';
        break;
      case 'bookmarked':
        mode = 'bookmarked-class';
        break;
      case 'bookedLive':
        mode = 'booked-live-class';
        break;
      default:
        console.log('which type is missing =>', type);
        break;
    }

    if (mode !== '') {
      this.props.getDataToSeeAll(mode);
    }
  };

  render() {
    const { data, title, type, showMore, hrLine } = this.props;
    return (
      <View>
        {data.length > 0 && (
          <View style={styles.titleBox}>
            <Text style={styles.title}>{title}</Text>
            {showMore && (
              <BaseButton
                style={styles.seeallBtn}
                onPress={() => this.onPressSeeAll(type)}
                textStyle={styles.seeallTextColor}
                textColor={Colors.button.primary.text.text}
                text={t('see_all')}
                transparent
              />
            )}
          </View>
        )}
        <FlatList
          horizontal
          data={data.slice(0, 6)}
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderItem}
          keyExtractor={(item, i) => `class_title_${i}`}
        />
        {data.length > 0 && hrLine && <View style={styles.hrLine} />}
      </View>
    );
  }
}
