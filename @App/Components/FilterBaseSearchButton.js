import React from 'react';
import { PropTypes } from 'prop-types';
import { View, Text, Image } from 'react-native';

import { TouchableOpacity } from 'react-native-gesture-handler';
import { Colors, Fonts, Metrics } from 'App/Theme';
import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  img: {
    width: '35@s',
    height: '35@s',
    marginBottom: Metrics.baseMargin / 2,
  },
  text: {
    ...Fonts.style.medium500,
    color: Colors.button.primary.unSelected.text,
    alignSelf: 'center',
    maxWidth: '125@s',
    minWidth: '23@s',
  },
  selectedText: {
    color: Colors.button.primary.content.text,
    alignSelf: 'center',
  },
  countsText: {
    ...Fonts.style.medium500,
    color: Colors.white,
    flexWrap: 'wrap',
    width: '10@s',
  },
  btn: {
    backgroundColor: Colors.button.primary.unSelected.background,
    borderWidth: 0,
    borderRadius: 20,
    paddingVertical: 7,
    paddingHorizontal: 12,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: Metrics.baseMargin / 2,
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2,
  },
  btnShadow: {
    shadowColor: Colors.shadow_black_03,
    shadowOffset: {
      width: 3,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 2,
  },
  isSelectedBtn: {
    backgroundColor: Colors.button.primary.content.background,
  },
  container: {
    backgroundColor: Colors.white,
  },
});

export default class FilterBaseSearchButton extends React.PureComponent {
  static propTypes = {
    size: PropTypes.string,
    isSelected: PropTypes.bool,
    text: PropTypes.string,
    image: PropTypes.any,
    onPress: PropTypes.func,
    imageStyle: PropTypes.object,
    countOfSelected: PropTypes.any,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    size: '',
    isSelected: false,
    text: '',
    image: '',
    onPress: () => {},
    imageStyle: {},
    countOfSelected: null,
    disabled: false,
  };

  render() {
    const {
      image,
      text,
      isSelected,
      size,
      onPress,
      imageStyle,
      countOfSelected,
      disabled,
    } = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={[styles.btn, styles.btnShadow, isSelected && styles.isSelectedBtn]}
          onPress={onPress}
          throttleTime={0}
          disabled={disabled}
        >
          <Text
            style={[styles.text, isSelected && styles.selectedText]}
            ellipsizeMode="tail"
            numberOfLines={1}
          >
            {text}
          </Text>
          {!!image && <Image source={image} style={{ width: 20, height: 20 }} />}
          {countOfSelected > 1 && isSelected && (
            <Text style={styles.countsText}>{countOfSelected}</Text>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}
