import React from 'react';
import { isString } from 'lodash';
import { PropTypes } from 'prop-types';
import { Text, View, Platform } from 'react-native';

import {
  RoundLabel,
  BaseButton as RoundButton,
  CachedImage as ImageBackground,
} from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet } from 'App/Helpers';
import { Colors, Metrics, Fonts } from 'App/Theme';
import LinearGradient from 'react-native-linear-gradient';

const styles = ScaledSheet.create({
  base: {
    width: '100%',
    height: Platform.isPad ? '251@s' : '214@sr',
    overflow: 'hidden',
    borderWidth: 0,
    marginTop: Metrics.baseMargin,
  },
  ImgWidth: {
    width: '100%',
    height: Platform.isPad ? '251@s' : '215@sr',
    overflow: 'hidden',
  },
  title: {
    ...Fonts.style.regular500,
    color: Colors.goldenYellow,
    marginBottom: Metrics.baseMargin / 2,
  },
  boxStyle: {
    borderRadius: '7.5@s',
    backgroundColor: Colors.black,
    marginTop: Metrics.baseMargin / 4,
    alignItems: 'center',
    minWidth: '80@s',
  },
  text: {
    ...Fonts.style.extraSmall500,
    paddingVertical: Metrics.baseMargin / 8,
    paddingHorizontal: Metrics.baseMargin / 2,
    color: Colors.white,
  },
  liveBoxStyle: {
    borderRadius: 7,
    width: '55@s',
    height: '15@vs',
    backgroundColor: Colors.white,
    marginBottom: 4,
    alignItems: 'center',
  },
  liveText: {
    fontSize: 10,
    fontWeight: '500',
    letterSpacing: 1,
    color: Colors.primary,
  },
  fragment: {
    flexDirection: 'row',
  },
  leftBox: {
    flex: 1,
    marginLeft: Metrics.baseMargin,
    justifyContent: 'flex-end',
  },
  rightBox: {
    minWidth: '110@vs',
    marginRight: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  left: {
    flexDirection: 'column',
  },
  right: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },
  time: {
    fontSize: '11@vs',
    color: '#f0f0f0',
  },
  secondTitle: {
    ...Fonts.style.extraSmall500,
    color: Colors.gray_04,
  },
  secondIcon: {
    width: '50@vs',
    backgroundColor: 'blue',
    color: Colors.white,
    flexDirection: 'row',
  },
  equipments: {
    width: '20@vs',
    height: '20@vs',
  },
  imageBox: {
    width: '40@vs',
    height: '40@vs',
    borderRadius: '40@vs',
    marginRight: '8@s',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  equipmentsBox: {
    flexDirection: 'row',
  },
  timeBox: {
    width: '100%',
    height: '13@vs',
    marginBottom: '12@vs',
  },
});

export default class CollectionCard extends React.Component {
  static propTypes = {
    data: PropTypes.object,
    onPressMarked: PropTypes.func,
    onPress: PropTypes.func,
  };

  render() {
    const { data, onPress } = this.props;
    return (
      <RoundButton style={styles.base} onPress={() => onPress(data.id)}>
        <ImageBackground
          style={styles.ImgWidth}
          source={data.signedCoverImageObj}
          imageSize="2x"
          imageType="h"
        >
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0, y: 1 }}
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
            }}
            locations={[0, 1]}
            colors={[`rgba(50,50,50,0)`, `rgba(50,50,50,0.4)`]}
          />
          <View style={styles.fragment}>
            <View style={styles.leftBox}>
              <View style={styles.left}>
                <Text style={styles.secondTitle}>{t('__collection')}</Text>
                <Text style={styles.title} numberOfLines={2}>
                  {isString(data.title) && data.title.toUpperCase()}
                </Text>
              </View>
            </View>
            <View style={styles.rightBox}>
              <View style={styles.right}>
                <RoundLabel text={t(`program_card_level_${data.level}`)} />
                <RoundLabel
                  text={`${data.trainingCollectionClassesCount} ${t('workouts')}`}
                />
              </View>
            </View>
          </View>
        </ImageBackground>
      </RoundButton>
    );
  }
}
