import React from 'react';
import PropTypes from 'prop-types';
import ViewShot from 'react-native-view-shot';
import * as ImagePicker from 'react-native-image-picker';
import { isNumber, isEmpty, has } from 'lodash';
import { Actions } from 'react-native-router-flux';
import { Platform, View, Text, ImageBackground, Image, Alert } from 'react-native';

import { BaseButton } from 'App/Components';
import { Fonts, Styles, Colors, Metrics, Images, Classes } from 'App/Theme';
import { Date as d, StyleSheet, Dialog, Permission } from 'App/Helpers';

import { translate as t } from 'App/Helpers/I18n';

const styles = StyleSheet.create({
  hiddenForCapturing: {
    opacity: 0,
  },
  tabImage: {
    height: '380@s',
    flexDirection: 'column',
    alignItems: 'center',
  },
  secondTitle: {
    fontSize: Fonts.size.input,
    fontWeight: '500',
    color: Colors.white,
    marginTop: '4@vs',
  },
  firstTitle: {
    fontSize: Fonts.size.medium,
    letterSpacing: 0.88,
    fontWeight: '500',
    color: Colors.secondary,
    marginTop: '24@vs',
  },
  textStyle: {
    fontSize: Fonts.size.regular,
    fontWeight: '500',
    color: Colors.secondary,
    letterSpacing: 1,
  },
  btnStyle: {
    width: '180@s',
    height: '44@vs',
    borderRadius: '22@vs',
    borderWidth: 2,
    borderColor: Colors.secondary,
    marginTop: '103@vs',
    marginBottom: '128@vs',
  },
  bootomBox: {
    ...Styles.container,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
  },
  logoImage: {
    width: '36@s',
    height: '36@s',
    marginBottom: Metrics.baseMargin,
  },
  bottomText: {
    ...Fonts.style.medium500,
    color: Colors.secondary,
    textAlign: 'center',
    marginBottom: Metrics.baseMargin,
    width: '70@s',
  },
});

class AppendPhotoView extends React.PureComponent {
  static propTypes = {
    routeName: PropTypes.string.isRequired,
    detail: PropTypes.object.isRequired,
    image: PropTypes.string,
    getRef: PropTypes.func.isRequired,
    isCapturing: PropTypes.bool.isRequired,
    showAddButton: PropTypes.bool,
    onMirrorSetPictureSuccess: PropTypes.func.isRequired,
    hasMirrorCamera: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    showAddButton: true,
    image: null,
  };

  renderDuration = () => {
    const { detail } = this.props;
    if (detail.scheduledAt) {
      if (
        !isEmpty(detail.liveClassHistory) &&
        isNumber(detail.liveClassHistory.totalDuration)
      ) {
        return detail.liveClassHistory.totalDuration;
      }
      return detail.duration;
    } else {
      if (
        has(detail, 'trainingProgramClassHistory') &&
        !isEmpty(detail.trainingProgramClassHistory)
      ) {
        return detail.trainingProgramClassHistory.totalDuration;
      } else if (
        has(detail, 'trainingClassHistory') &&
        !isEmpty(detail.trainingClassHistory)
      ) {
        return detail.trainingClassHistory.totalDuration;
      } else {
        return detail.duration;
      }
    }
  };

  handleOpenCamera = async () => {
    if (Platform.OS === 'android') {
      const hasPermission = await Permission.checkAndRequestPermission(
        Permission.permissionType.CAMERA,
      );
      if (!hasPermission) {
        Dialog.requestCameraPermissionFromSystemAlert();
      }
    }
    const { onMirrorSetPictureSuccess } = this.props;
    const options = {
      includeBase64: true,
      mediaType: 'image',
      quality: 1,
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.error) {
        Dialog.requestCameraPermissionFromSystemAlert();
      }
      console.log('launchCamera options=>', options);
      console.log('launchCamera response=>', response);
      if (response.base64) {
        onMirrorSetPictureSuccess({ thumbnail: response.base64 });
      }
    });
  };

  showAlert = () => {
    const { routeName } = this.props;
    console.log('routeName=>', routeName);
    Alert.alert(
      t('take_a_photo_from'),
      t('take_photo_content'),
      [
        {
          style: 'cancel',
          text: t('__cancel'),
          onPress: () => console.log('OK Pressed'),
        },
        {
          text: t('mirror_camera_phone_camera'),
          onPress: this.handleOpenCamera,
        },
        {
          text: t('mirror_camera_mirror_camera'),
          onPress:() => {
            const { hasMirrorCamera } = this.props;
            console.log("=== hasMirrorCamera ===", hasMirrorCamera);
            if(hasMirrorCamera == false){
              Dialog.showDeviceHasNoCameraAlert();
            }else {
              if(routeName === 'ProgramSaveActivityScreen')
                Actions.ProgramMirrorCamera();
              else Actions.MirrorCamera();
            }

          }

        },
      ],
      { cancelable: false },
    );
  };

  onPressCalories = () => {
    const { detail, routeName } = this.props;
    if (detail.scheduledAt) {
      if (
        !isEmpty(detail.liveClassHistory) &&
        isNumber(detail.liveClassHistory.totalCalories)
      ) {
        return detail.liveClassHistory.totalCalories;
      }
      return detail.duration;
    } else {
      if (routeName === 'ProgramClassDetail') {
        if (
          !isEmpty(detail.trainingProgramClassHistory) &&
          isNumber(detail.trainingProgramClassHistory.totalCalories)
        ) {
          return detail.trainingProgramClassHistory.totalCalories;
        }
        return detail.duration;
      } else {
        if (
          !isEmpty(detail.trainingClassHistory) &&
          isNumber(detail.trainingClassHistory.totalCalories)
        ) {
          return detail.trainingClassHistory.totalCalories;
        }
        return detail.duration;
      }
    }
  };

  renderAppendPhotoView = () => {
    const { detail, image, isCapturing, showAddButton } = this.props;
    return (
      <ImageBackground
        source={{ uri: image || detail.signedCoverImage }}
        resizeMode="cover"
        style={styles.tabImage}
      >
        <Text style={styles.firstTitle}>{detail.title}</Text>
        <Text style={styles.secondTitle}>{t('class_card_workout_completed')}</Text>
        <View style={Platform.isPad ? Classes.fillCenter : Classes.fill}>
          {!isCapturing && showAddButton && (
            <BaseButton
              text={isCapturing ? '' : t('save_activity_add_photo')}
              style={isCapturing ? styles.hiddenForCapturing : styles.btnStyle}
              textStyle={styles.textStyle}
              textColor={Colors.secondary}
              onPress={this.showAlert}
              transparent
            />
          )}
        </View>
        <View style={styles.bootomBox}>
          <Text style={styles.bottomText}>{`${t('save_activity_duration')}\n${d.mmss(
            this.renderDuration(),
          )}`}</Text>
          <Text style={styles.bottomText}>{`${t(
            'save_activity_calories',
          )}\n${this.onPressCalories()} cal`}</Text>
          <Image source={Images.logo_small} style={styles.logoImage} />
        </View>
      </ImageBackground>
    );
  };

  render() {
    const { getRef } = this.props;
    return (
      <ViewShot
        ref={(r) => {
          this.viewShot = r;
          getRef(r);
        }}
        options={{ format: 'jpg', quality: 1 }}
      >
        {this.renderAppendPhotoView()}
      </ViewShot>
    );
  }
}

export default AppendPhotoView;
