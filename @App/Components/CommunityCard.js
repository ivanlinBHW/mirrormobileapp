import { Colors, Fonts, Images, Metrics } from 'App/Theme';
import { Platform, Text, View } from 'react-native';
import { ScaledSheet, Screen, Date as d } from 'App/Helpers';

import { CachedImage as Image, CachedImage as ImageBackground } from 'App/Components';
import { PropTypes } from 'prop-types';
import React from 'react';
import { RoundButton } from '@ublocks-react-native/component';
import { translate as t } from 'App/Helpers/I18n';

const PAD_HEIGHT = '290@sr';

const styles = ScaledSheet.create({
  base: {
    marginLeft: Metrics.baseMargin,
    overflow: 'hidden',
    borderRadius: '12@sr',
    borderWidth: 0,
  },
  sizeBaseNormal: {
    width: Screen.scale(281),
    height: Screen.scale(159),
  },
  sizeBaseLarge: {
    width: Screen.scale(364),
    height: Platform.isPad ? PAD_HEIGHT : '214@sr',
  },
  sizeBaseFull: {
    height: Platform.isPad ? PAD_HEIGHT : '214@sr',
    marginRight: Metrics.baseMargin,
  },
  sizeNormal: {
    width: Screen.scale(281),
    height: Screen.scale(159),
  },
  sizeLarge: {
    width: Platform.isPad ? Screen.scale(300) : Screen.scale(368),
    height: Platform.isPad ? PAD_HEIGHT : '214@sr',
  },
  sizeFull: {
    width: '100%',
    height: Platform.isPad ? PAD_HEIGHT : '214@sr',
  },
  normalImgWidth: {
    width: Screen.scale(301),
    height: Screen.scale(159),
    borderRadius: 12,
    overflow: 'hidden',
  },
  largeImgWidth: {
    width: Screen.scale(394),
    height: Platform.isPad ? PAD_HEIGHT : '214@sr',
    borderRadius: 12,
    overflow: 'hidden',
  },
  fullImgWidth: {
    width: '115%',
    height: Platform.isPad ? PAD_HEIGHT : '214@sr',
    borderRadius: 12,
    overflow: 'hidden',
    paddingHorizontal: '12@sr',
  },
  title: {
    ...Fonts.style.regular500,
    color: Colors.secondary,
    marginLeft: Metrics.baseMargin,
  },
  boxStyle: {
    minWidth: '80@s',
    height: 15,
    borderRadius: '7.5@s',
    backgroundColor: Colors.black,
    paddingHorizontal: Metrics.baseMargin / 2,
    marginBottom: 4,
    alignItems: 'center',
  },
  text: {
    ...Fonts.style.extraSmall500,
    color: '#ffffff',
  },
  fragment: {
    flexDirection: 'column',
    backgroundColor: 'red',
    marginVertical: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin * 2,
  },
  baseBox: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 100,
    flexDirection: 'column',
    alignContent: 'space-between',
    width: '100%',
    height: Platform.isPad ? PAD_HEIGHT : '214@sr',
    borderRadius: '12@sr',
  },
  memberImg: {
    width: '30@s',
    height: '30@s',
  },
  noBorder: {
    borderWidth: 0,
  },
  lastItem: {
    marginRight: Metrics.baseMargin,
  },

  countText: {
    ...Fonts.style.small500,
    color: Colors.gray_04,
    marginLeft: Metrics.baseMargin / 2,
  },
  countBox: {
    flexDirection: 'row',
    marginTop: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
    alignItems: 'center',
  },
  timeText: {
    ...Fonts.style.small500,
    color: Colors.gray_04,
    marginLeft: Metrics.baseMargin,
    marginTop: Metrics.baseMargin / 2,
  },
  bottomBox: {
    flexDirection: 'column',
    flex: 1,
    marginBottom: Metrics.baseMargin,
    justifyContent: 'flex-end',
  },
});

export default class CommunityCard extends React.PureComponent {
  static propTypes = {
    data: PropTypes.object,
    size: PropTypes.string,
    title: PropTypes.string,
    onPressMarked: PropTypes.func,
    onPress: PropTypes.func,
    onPressBookmark: PropTypes.func,
    timezone: PropTypes.string,
    eventType: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isHover: PropTypes.bool,
    leftBoxStyle: PropTypes.object,
    rightBoxStyle: PropTypes.object,
    bookmarkStyle: PropTypes.object,
    lastItem: PropTypes.bool,
  };

  static defaultProps = {
    isHover: false,
    leftBoxStyle: {},
    rightBoxStyle: {},
    bookmarkStyle: {},
    timezone: 'Asia/Taipei',
    lastItem: false,
    eventType: '',
  };

  getBaseStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.sizeBaseNormal;
      case 'large':
        return styles.sizeBaseLarge;
      case 'full':
        return styles.sizeBaseFull;
      default:
        return styles.sizeBaseNormal;
    }
  };

  getWrapStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.sizeNormal;
      case 'large':
        return styles.sizeLarge;
      case 'full':
        return styles.sizeFull;
      default:
        return styles.sizeNormal;
    }
  };

  getImgStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.normalImgWidth;
      case 'large':
        return styles.largeImgWidth;
      case 'full':
        return styles.fullImgWidth;
      default:
        return styles.sizeNormal;
    }
  };

  renderDate = () => {
    const { data, timezone } = this.props;
    return `${d.formatDate(
      d.transformDate(data.startDate, timezone),
      d.DEFAULT_DATE_FORMAT,
    )} - ${d.formatDate(d.transformDate(data.dueDate, timezone), d.DEFAULT_DATE_FORMAT)}`;
  };

  renderTime = () => {
    const { data, timezone } = this.props;
    return `${t('community_1on1_begins_at')} ${d.formatDate(
      d.transformDate(data.startDate, timezone),
      d.MINUTE_FORMAT,
    )} - ${d.formatDate(d.transformDate(data.dueDate, timezone), d.DEFAULT_DATE_FORMAT)}`;
  };

  render() {
    const { data, onPress, lastItem, eventType } = this.props;
    return (
      <View style={[styles.base, this.getBaseStyle(), lastItem && styles.lastItem]}>
        <RoundButton
          style={[this.getWrapStyle(), styles.noBorder]}
          onPress={() => onPress(data.id, data)}
          roundRadius={12}
          throttleTime={300}
        >
          <ImageBackground
            resizeMode="cover"
            source={data.signedCoverImageObj || { uri: data.signedCoverImage }}
            style={[this.getImgStyle()]}
            imageSize="2x"
            imageType="h"
          />
          <View style={styles.baseBox}>
            <View style={styles.countBox}>
              <Image source={Images.members_white} style={styles.memberImg} />
              <Text style={styles.countText}>
                {eventType === 0 && data.participateCount}
                {eventType === 1 && '1 ON 1'}
              </Text>
            </View>
            <View style={styles.bottomBox}>
              <Text style={styles.title}>{data.title}</Text>
              <Text style={styles.timeText}>
                {eventType === 0 && this.renderDate()}
                {eventType === 1 && this.renderTime()}
              </Text>
            </View>
          </View>
        </RoundButton>
      </View>
    );
  }
}
