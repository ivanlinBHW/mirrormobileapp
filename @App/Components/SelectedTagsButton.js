import React from 'react';
import { PropTypes } from 'prop-types';
import { View, Text, Image, Platform } from 'react-native';
import { RoundButton } from '@ublocks-react-native/component';
import { BaseImageButton } from 'App/Components';
import { Colors, Fonts, Metrics, Images } from 'App/Theme';

import { ScaledSheet, Screen } from 'App/Helpers';

const styles = ScaledSheet.create({
  wrapper: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    ...Fonts.style.extraSmall500,
    color: Colors.white,
  },
  btn: {
    backgroundColor: Colors.button.primary.content.background,
    elevation: 2,
    borderWidth: 0,
    shadowColor: Colors.shadow_black_03,
    marginLeft: '8@s',
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2,
    paddingLeft: '8@s',
    paddingRight: '4@s',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: {
      height: 3,
      width: 3,
    },
  },

  dismissIcon: {
    margin: 0,
  },
  normalSize: {
    height: '22@s',
    borderRadius: '15@s',
  },
});

export default class SelectedTagsButton extends React.PureComponent {
  static propTypes = {
    text: PropTypes.string,
    onPressCancelFilter: PropTypes.func,
  };

  static defaultProps = {
    text: '',
    onPressCancelFilter: () => {},
  };

  render() {
    const { text, onPressCancelFilter } = this.props;
    return (
      <View style={[styles.btn, styles.normalSize]} throttleTime={0}>
        <View style={styles.wrapper}>
          <Text style={styles.text}>{text}</Text>
          <View style={styles.rightBox}>
            <BaseImageButton
              style={styles.dismissIcon}
              source={Images.cancel_secondary}
              width={Screen.scale(16)}
              onPress={onPressCancelFilter}
            />
          </View>
        </View>
      </View>
    );
  }
}
