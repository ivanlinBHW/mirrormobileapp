import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import { StyleSheet, Screen } from 'App/Helpers';
import { Classes, Colors, Metrics, Fonts } from 'App/Theme';

const styles = StyleSheet.create({
  container: {
    ...Classes.center,
    borderRadius: '7.5@s',
    paddingVertical: Metrics.baseMargin / 8,
    paddingHorizontal: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 4,
    minWidth: '88@s',
    minHeight: '15@s',
  },
  text: {
    ...Fonts.style.extraSmall500,
  },
});

const getStyle = ({ width, backgroundColor }) => [
  styles.container,
  {
    backgroundColor,
    width: typeof width === 'number' ? Screen.scale(width) : 'auto',
  },
];

const RoundLabel = ({ text, width, backgroundColor, color }) => (
  <View style={getStyle({ width, backgroundColor, color })}>
    <Text style={[styles.text, { color }]}>{text}</Text>
  </View>
);

RoundLabel.propTypes = {
  text: PropTypes.string.isRequired,
  width: PropTypes.number,
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
};

RoundLabel.defaultProps = {
  width: undefined,
  backgroundColor: Colors.black,
  color: Colors.white,
};

export default RoundLabel;
