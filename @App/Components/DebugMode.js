import React from 'react';
import { SafeAreaView, Text, TouchableOpacity, View, FlatList } from 'react-native';
import { StyleSheet } from 'App/Helpers';

const styles = StyleSheet.create({
  DebugContainer: {
    flex: 1,
    backgroundColor: '#a92a35',
    padding: '4@s',
    alignItems: 'stretch',
    height: '100%',
    width: '100%',
  },
  TitleTextStyle: {
    color: 'white',
    fontSize: '20@s',
  },
  Button: {
    flex: 1,
  },
  ButtonTextStyle: {
    borderWidth: 1,
    borderRadius: '5@s',
    padding: '5@s',
    backgroundColor: '#af3c46',
    color: 'white',
    textAlign: 'center',
    fontSize: '20@s',
  },
  ButtonDisabledTextStyle: {
    backgroundColor: '#614245',
    color: '#919191',
  },
  ButtonContainer: {
    flexDirection: 'row',
    paddingVertical: '4@s',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  LogList: {
    marginVertical: '4@vs',
    flex: 1,
    flexGrow: 1,
  },
  LogListBorder: {
    borderWidth: 1,
    borderColor: 'black',
  },
  LogTextStyle: {
    color: 'white',
    fontSize: 9,
  },
});

export const Title = ({ children, numberOfLines = 1, ...props }) => (
  <Text style={styles.TitleTextStyle} numberOfLines={numberOfLines || 1} {...props}>
    {children}
  </Text>
);

export const LogList = ({ data, renderItem = null, height, ...props }) => {
  const renderDefaultItem = ({ item }) => <Text style={styles.LogTextStyle}>{item}</Text>;
  return (
    <FlatList
      style={[styles.LogList, styles.LogListBorder, height && { height }]}
      data={data}
      renderItem={renderItem || renderDefaultItem}
      keyExtractor={(item, index) => index.toString()}
      {...props}
    />
  );
};

export const Container = ({ children, ...props }) => (
  <SafeAreaView style={styles.DebugContainer} {...props}>
    {children}
  </SafeAreaView>
);

export const ButtonContainer = ({ children, ...props }) => (
  <View style={styles.ButtonContainer} {...props}>
    {children}
  </View>
);

export const Button = (props) => {
  const { onPress, text, ...restProps } = props;
  return (
    <TouchableOpacity onPress={onPress} {...restProps} style={styles.Button}>
      <Text
        style={[
          styles.ButtonTextStyle,
          restProps.disabled ? styles.ButtonDisabledTextStyle : null,
        ]}
      >
        {text}
      </Text>
    </TouchableOpacity>
  );
};
