import React from 'react';
import { BaseVerticalTextInput, BaseHorizontalTextInput } from './BaseTextInputBox';
import { Colors, Metrics } from 'App/Theme';

const containerStyle = {
  height: '38@vs',
  paddingRight: 0,
  paddingLeft: 0,
  borderRadius: 6,
  backgroundColor: Colors.white,
  marginBottom: Metrics.baseMargin,

  elevation: 2,
  shadowColor: Colors.shadow,
  shadowOpacity: 0.4,
  shadowRadius: 1,
  shadowOffset: {
    height: 1,
    width: 3,
  },
};

const propTypes = {};

const defaultProps = {};

export const VerticalTextInput = ({
  ...props
}) => {
  return (
    <BaseVerticalTextInput
      {...props}
    />
  );
};

VerticalTextInput.propTypes = propTypes;
VerticalTextInput.defaultProps = defaultProps;

export const HorizontalTextInput = ({
  ...props
}) => {
  return (
    <BaseHorizontalTextInput
      {...props}
    />
  );
};

HorizontalTextInput.propTypes = propTypes;
HorizontalTextInput.defaultProps = defaultProps;

export default {
  VerticalTextInput,
  HorizontalTextInput,
};
