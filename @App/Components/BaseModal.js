import React from 'react';
import PropTypes from 'prop-types';
import { throttle } from 'lodash';
import { connect } from 'react-redux';
import {
  TouchableWithoutFeedback,
  BackHandler,
  Platform,
  View,
  Text,
} from 'react-native';
import { NavBar, IconButton, LoadingIndicator } from '@ublocks-react-native/component';
import * as Animatable from 'react-native-animatable';
import { Actions } from 'react-native-router-flux';

import { BaseImageButton, BaseButton } from 'App/Components';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Colors, Styles, Fonts, Images, Metrics } from 'App/Theme';

const styles = ScaledSheet.create({
  container: {
    backgroundColor: 'white',
    position: 'absolute',
    width: '100%',
    height: '100%',
    flex: 1,
    paddingBottom: 0,
    bottom: 0,
    zIndex: 10,
  },
  navBar: {
    ...Styles.modalNavBar,
    borderBottomWidth: 1,
    borderBottomColor: Colors.shadow,
    paddingHorizontal: 0,
  },
  loadingIndicatorWrapper: {
    position: 'absolute',
    top: `${Metrics.baseNavBarHeightRaw + 4}@vs`,
    height: '100%',
    width: '100%',
    zIndex: 100,
  },
  title: {
    ...Fonts.style.regular500,
  },
  border: {
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderWidth: Screen.onePixel * 2,
    borderColor: Colors.shadow,
  },
  shadow: {
    shadowColor: Colors.deepBlack,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    elevation: 100,
  },
  rightBox: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    position: 'absolute',
    right: Platform.select({
      ios: Metrics.baseMargin,
      android: Metrics.baseMargin,
    }),
  },
  darkBackground: {
    top: 0,
    height: '100%',
    width: '100%',
    position: 'absolute',
    flex: 1,
    backgroundColor: Colors.shadow,
  },
  leftStyle: {
    justifyContent: 'flex-start',
    position: 'absolute',
    left: Platform.select({
      ios: Metrics.baseMargin,
      android: Metrics.baseMargin * 1.5,
    }),
  },
  rightStyle: {
    backgroundColor: 'transparent',
  },
  saveText: {
    color: Colors.button.primary.outline.text,
    ...Fonts.style.regular500,
  },
});

class BaseModal extends React.PureComponent {
  static propTypes = {
    title: PropTypes.string,
    animation: PropTypes.string,
    children: PropTypes.object,
    isShowBack: PropTypes.bool,
    onBackPress: PropTypes.func,
    handleClose: PropTypes.func,
    onOpenModal: PropTypes.func,
    onCloseModal: PropTypes.func,
    isLoading: PropTypes.bool,
    style: PropTypes.object,
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    duration: PropTypes.number,
    showLoadingIndicator: PropTypes.bool,
    navRightComponent: PropTypes.object,
  };

  static defaultProps = {
    onBackPress: undefined,
    handleClose: undefined,
    animation: 'slideInUp',
    isShowBack: false,
    isLoading: false,
    style: {},
    height: '50%',
    duration: 400,
    onOpenModal: undefined,
    onCloseModal: undefined,
    showLoadingIndicator: false,
    navRightComponent: null,
  };

  componentDidMount() {
    const { onOpenModal } = this.props;
    typeof onOpenModal === 'function' && onOpenModal();
    if (Platform.OS === 'android') {
      this.backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        this.onAndroidBackButtonPressed,
      );
    }
  }
  componentWillUnmount() {
    if (Platform.OS === 'android') {
      this.backHandler.remove();
    }
  }

  onAndroidBackButtonPressed = () => {
    const { onBackPress } = this.props;
    if (typeof onBackPress === 'function') {
      onBackPress();
      return true;
    }
    return false;
  };

  handleViewRef = (ref) => (this.view = ref);

  handleClose = throttle(() => {
    const { handleClose, onCloseModal } = this.props;
    return (
      this.view &&
      this.view.slideOutDown(250).then((endState) => {
        typeof onCloseModal === 'function' && onCloseModal();
        return typeof handleClose === 'function' ? handleClose() : Actions.pop();
      })
    );
  }, 550);

  render() {
    const {
      title,
      children,
      animation = 'slideInUp',
      isShowBack = false,
      isLoading = false,
      showLoadingIndicator = true,
      onBackPress,
      style,
      height,
      duration,
      navRightComponent = null,
    } = this.props;
    return (
      <>
        <TouchableWithoutFeedback onPress={this.handleClose}>
          <Animatable.View
            style={styles.darkBackground}
            animation={'fadeIn'}
            duration={duration}
            delay={duration}
            useNativeDriver={true}
          />
        </TouchableWithoutFeedback>
        <Animatable.View
          ref={this.handleViewRef}
          style={[styles.container, styles.shadow, styles.border, { height }, style]}
          animation={animation}
          duration={duration}
          useNativeDriver={true}
        >
          <NavBar
            style={styles.navBar}
            backIconColor="black"
            title={title}
            titleStyle={styles.title}
            leftComponent={
              isShowBack ? (
                <View style={styles.leftStyle}>
                  <IconButton
                    iconType="FontAwesome5"
                    iconName="chevron-left"
                    iconSize={14}
                    width={Screen.scale(14)}
                    onPress={onBackPress || (() => Actions.pop())}
                  />
                </View>
              ) : (
                false
              )
            }
            rightComponent={
              navRightComponent ? (
                <BaseButton
                  transparent
                  throttleTime={0}
                  style={styles.rightStyle}
                  onPress={() => {
                    navRightComponent.onPress();
                    this.handleClose();
                  }}
                >
                  <Text style={styles.saveText}>{navRightComponent.text}</Text>
                </BaseButton>
              ) : (
                <View style={styles.rightBox}>
                  {/* <IconButton
                  iconType="FontAwesome5"
                  iconName="times"
                  iconSize={14}
                  width={Screen.scale(14)}
                  onPress={this.handleClose}
                /> */}

                  <BaseImageButton
                    style={styles.navVoiceIcon}
                    source={Images.cancel}
                    width={Screen.scale(14)}
                    onPress={this.handleClose}
                  />
                </View>
              )
            }
          />
          {children}
          {showLoadingIndicator && isLoading && (
            <View style={styles.loadingIndicatorWrapper}>
              <LoadingIndicator
                height="100%"
                width="100%"
                cover={false}
                open={isLoading}
              />
            </View>
          )}
        </Animatable.View>
      </>
    );
  }
}

export default connect((state) => ({
  isLoading: state.appState.isLoading,
}))(BaseModal);
