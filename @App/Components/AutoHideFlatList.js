import React from 'react';
import PropTypes from 'prop-types';
import Emitter from 'tiny-emitter/instance';
import { connect } from 'react-redux';
import { Animated, Platform, View, RefreshControl } from 'react-native';

import { Metrics } from 'App/Theme';
import { ScaledSheet, ifIphoneX } from 'App/Helpers';

const styles = ScaledSheet.create({
  listFooter: {
    paddingBottom: Metrics.baseVerticalMargin * 2,
  },
});

const HEADER_HEIGHT = Metrics.homeNavBarHeight;

let unmounted = true;

class AutoHideFlatList extends React.PureComponent {
  static propTypes = {
    style: PropTypes.any,
    contentContainerStyle: PropTypes.any,
    sceneKey: PropTypes.string,
    prevRoute: PropTypes.string,
    currRoute: PropTypes.string,
    refreshing: PropTypes.bool,
    onRefresh: PropTypes.func,
    footerStyle: PropTypes.object,
    showRefresh: PropTypes.bool,
    programId: PropTypes.string,
    keyExtractor: PropTypes.func,
    ListFooterComponent: PropTypes.any,
    scrollEventThrottle: PropTypes.number,
  };

  static defaultProps = {
    contentContainerStyle: {},
    keyExtractor: undefined,
    style: {},
    prevRoute: '',
    currRoute: '',
    sceneKey: '',
    onRefresh: () => {},
    refreshing: false,
    footerStyle: {},
    showRefresh: false,
    ListFooterComponent: null,
    scrollEventThrottle: 1,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.currRoute !== nextProps.sceneKey && !unmounted) {
      unmounted = true;
    }
    if (nextProps.currRoute === nextProps.sceneKey && unmounted) {
      unmounted = false;
    }
    return null;
  }

  state = {
    scrollY: new Animated.Value(
      Platform.OS === 'ios' ? -Metrics.homeNavBarHeight : 0,
    ),
  };

  componentDidMount() {
    Emitter.on('onListReset', this.onResetListPosition);
  }

  componentWillUnmount() {
    unmounted = true;
    Emitter.off('onListReset');
  }

  setRef = (c) => {
    this.listRef = c;
  };

  onResetListPosition = () => {
    this.setState({ scrollY: new Animated.Value(0) });
    if (this.listRef) {
      this.listRef.scrollToOffset({ offset: -HEADER_HEIGHT, animated: true });
    }
  };

  onScroll = () =>
    Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: { y: this.state.scrollY },
          },
        },
      ],
      {
        useNativeDriver: true,
        listener: (event) => {
          const { currRoute, sceneKey } = this.props;
          if (currRoute === sceneKey) {
            Emitter.emit('onListScroll', event.nativeEvent.contentOffset);
          }
        },
      },
    );

  getStyle = () => [
    {
      backgroundColor: 'transparent',
      paddingTop:
        Metrics.homeNavBarHeight +
        ifIphoneX(Metrics.baseVerticalMargin / 2, Metrics.baseVerticalMargin),
      marginTop: Metrics.baseNavBarHeight,
    },
    this.props.style,
  ];

  render() {
    const {
      style,
      contentContainerStyle,
      footerStyle,
      onRefresh,
      showRefresh,
      refreshing,
      ListFooterComponent,
      scrollEventThrottle,
    } = this.props;
    return (
      <Animated.FlatList
        {...this.props}
        automaticallyAdjustContentInsets={false}
        contentContainerStyle={[
          {
            paddingTop:
              Platform.OS === 'ios'
                ? Metrics.homeNavBarHeight / 2
                : Metrics.homeNavBarHeight * 1.75,
          },
          contentContainerStyle,
        ]}
        contentInset={{
          top: HEADER_HEIGHT,
        }}
        contentOffset={{ x: 0, y: -HEADER_HEIGHT }}
        style={style}
        ref={this.setRef}
        ListFooterComponent={
          <View style={[styles.listFooter, footerStyle]}>{ListFooterComponent}</View>
        }
        keyExtractor={
          typeof this.props.keyExtractor === 'function'
            ? this.props.keyExtractor
            : (item, index) => `index-${index}`
        }
        showsVerticalScrollIndicator={false}
        scrollEventThrottle={scrollEventThrottle}
        onScroll={this.onScroll()}
        refreshControl={
          showRefresh && (
            <RefreshControl
              progressViewOffset={HEADER_HEIGHT * 2}
              onRefresh={onRefresh}
              refreshing={refreshing}
            />
          )
        }
      />
    );
  }
}

export default connect((state) => ({
  currRoute: state.appRoute.routeName,
  prevRoute: state.appRoute.prevRoute,
  nextRoute: state.appRoute.nextRoute,
}))(AutoHideFlatList);
