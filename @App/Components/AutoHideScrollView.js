import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Animated, RefreshControl } from 'react-native';
import Emitter from 'tiny-emitter/instance';
import { Metrics } from 'App/Theme';

class AutoHideScrollView extends React.PureComponent {
  static propTypes = {
    style: PropTypes.any,
    hasTopTabBar: PropTypes.bool,
    isStandalone: PropTypes.bool,
    showRefresh: PropTypes.bool,
    onRefresh: PropTypes.func,
    refreshing: PropTypes.bool,
  };

  static defaultProps = {
    style: {},
    hasTopTabBar: true,
    isStandalone: false,
  };

  state = {
    scrollY: new Animated.Value(
      Platform.OS === 'ios' ? -Metrics.homeNavBarHeight : 0,
    ),
  };

  onScroll = () =>
    Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: { y: this.state.scrollY },
          },
        },
      ],
      {
        useNativeDriver: true,
        listener: (event) => {
          Emitter.emit('onListScroll', event.nativeEvent.contentOffset);
        },
      },
    );

  getStyle = (hasTopTabBar, isStandalone) => [
    isStandalone
      ? {
          paddingTop: 0,
          marginTop: 0,
          paddingBottom: 0,
        }
      : {
          paddingTop: Metrics.homeNavBarHeight + Metrics.baseVerticalMargin / 2,
          paddingBottom: Metrics.homeNavBarHeight + Metrics.baseVerticalMargin / 2,
          marginTop: hasTopTabBar ? Metrics.baseNavBarHeight - 1 : 0,
        },
    this.props.style,
  ];

  render() {
    const { hasTopTabBar, isStandalone, showRefresh, onRefresh, refreshing } = this.props;
    return (
      <Animated.ScrollView
        {...this.props}
        style={this.getStyle(hasTopTabBar, isStandalone)}
        showsVerticalScrollIndicator={false}
        scrollEventThrottle={1}
        onScroll={this.onScroll()}
        refreshControl={
          showRefresh && (
            <RefreshControl
              progressViewOffset={Metrics.communityNavBarHeight}
              onRefresh={onRefresh}
              refreshing={refreshing}
            />
          )
        }
      />
    );
  }
}

export default AutoHideScrollView;
