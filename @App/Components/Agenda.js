import React from 'react';
import PropTypes from 'prop-types';
import { Platform, View, Text } from 'react-native';
import moment from 'moment';
import { ExpandableCalendar, CalendarProvider } from 'react-native-calendars';

import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  calendar: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  section: {
    backgroundColor: '#f0f4f7',
    color: '#79838a',
  },
  itemTitleText: {
    color: 'black',
    marginLeft: 16,
    fontWeight: 'bold',
    fontSize: 16,
  },
  itemButtonContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  emptyItem: {
    paddingLeft: 20,
    height: 52,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#e8ecf0',
  },
  emptyItemText: {
    color: '#79838a',
    fontSize: 14,
  },
});

export default class Agenda extends React.Component {
  static propTypes = {
    data: PropTypes.array,
    style: PropTypes.any,
    onPressDetail: PropTypes.func,
    getLiveDate: PropTypes.func,
    liveDate: PropTypes.array,
    getLiveDateDetail: PropTypes.func,
    children: PropTypes.object,
  };

  static defaultProps = {
    onRenderItem: null,
    onItemPressed: null,
  };

  componentDidMount() {
    const { getLiveDate } = this.props;
    const weekOfday = moment().format('E');
    const prev_day = moment()
      .subtract(weekOfday - 1, 'days')
      .format('YYYY/MM/DD');
    const next_day = moment()
      .add(7 - weekOfday, 'days')
      .format('YYYY/MM/DD');

    getLiveDate({ start: prev_day, end: next_day });
  }

  onDateChanged = (date) => {
    const { getLiveDateDetail, getLiveDate } = this.props;
    const prev_day = moment(date).subtract(6, 'days');
    const next_day = moment(date).add(7, 'days');

    getLiveDate({
      start: moment(prev_day).format('YYYY/MM/DD'),
      end: moment(next_day).format('YYYY/MM/DD'),
    });
    getLiveDateDetail(date);
  };

  onMonthChange = (month) => {
  };

  renderEmptyItem() {
    return (
      <View style={styles.emptyItem}>
        <Text style={styles.emptyItemText}>No Events Planned</Text>
      </View>
    );
  }

  getMarkedDates = () => {
    const marked = {};
    const { liveDate } = this.props;
    liveDate.forEach((item) => {
      marked[item] = { marked: true };
    });
    return marked;
  };

  getTheme = () => {
    const { style } = this.props;
    if (style) {
      return style;
    }

    const themeColor = '#0059ff';
    const lightThemeColor = '#e6efff';
    const disabledColor = '#a6acb1';
    const black = '#20303c';
    const white = '#ffffff';

    return {
      arrowColor: black,
      arrowStyle: { padding: 0 },
      monthTextColor: black,
      textMonthFontSize: 16,
      textMonthFontFamily: 'HelveticaNeue',
      textMonthFontWeight: 'bold',
      textSectionTitleColor: black,
      textDayHeaderFontSize: 12,
      textDayHeaderFontFamily: 'HelveticaNeue',
      textDayHeaderFontWeight: 'normal',
      todayBackgroundColor: lightThemeColor,
      todayTextColor: themeColor,
      dayTextColor: themeColor,
      textDayFontSize: 18,
      textDayFontFamily: 'HelveticaNeue',
      textDayFontWeight: '500',
      textDayStyle: {
        marginTop: Platform.OS === 'android' ? 2 : 4,
      },
      selectedDayBackgroundColor: themeColor,
      selectedDayTextColor: white,
      textDisabledColor: disabledColor,
      dotColor: themeColor,
      selectedDotColor: white,
      disabledDotColor: disabledColor,
      dotStyle: { marginTop: -2 },
    };
  };

  render() {
    return (
      <CalendarProvider
        onDateChanged={this.onDateChanged}
        onMonthChange={this.onMonthChange}
        theme={{ todayButtonTextColor: '#0059ff' }}
        showTodayButton
        disabledOpacity={0.6}
      >
        <ExpandableCalendar
          disablePan
          hideKnob
          firstDay={1}
          markedDates={this.getMarkedDates()}
          theme={this.getTheme()}
        />
        {/* {children} */}
        <Text>asd</Text>
      </CalendarProvider>
    );
  }
}
