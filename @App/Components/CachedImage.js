import React from 'react';
import FastImage from 'react-native-fast-image';
import { PropTypes } from 'prop-types';
import { get, has, isNil, isEmpty, isString } from 'lodash';
import { createImageProgress } from 'react-native-image-progress';
import { Image, ImageBackground, View } from 'react-native';

import { getS3SignedHeaders } from 'App/Helpers';

const transformUri = (source, imageSize, imageType) => {
  const regx = /(\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b)/;

  const types = ['avatar/', 'images/', 'image/cover/'];

  const { uri } = source;

  if (imageType && imageSize) {
    const newUri = uri
      .split(regx)
      .map((e) => {
        const type = types.find((t) => e.includes(t));
        if (type) {
          return e.replace(type, `${type}resized/${imageType}/${imageSize}/`);
        }
        return e;
      })
      .join('');
    return newUri;
  }
  return uri;
};

const hasStringUri = (source) => {
  return (
    typeof source === 'object' &&
    has(source, 'uri') &&
    !isNil(source.uri) &&
    isString(source.uri) &&
    !isEmpty(source.uri)
  );
};

export default class CachedImage extends React.PureComponent {
  static propTypes = {
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    source: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    errorImage: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    resizeMode: PropTypes.string,
    priority: PropTypes.string,
    cache: PropTypes.string,
    imageSize: PropTypes.string,
    imageType: PropTypes.string,
    loadingIndicator: PropTypes.bool,
  };

  static defaultProps = {
    style: null,
    source: null,
    children: undefined,
    resizeMode: 'cover',
    errorImage: null,
    priority: 'normal',
    cache: 'immutable',
    imageSize: '1x',
    imageType: undefined,
    loadingIndicator: true,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { source, imageSize, imageType } = nextProps;
    if (hasStringUri(source) && prevState.uri !== source.uri) {
      return {
        uri: transformUri(source, imageSize, imageType),
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    const { source, imageSize, imageType } = props;

    this.state = {
      isRefreshing: false,
      isError: false,
      isLoaded: false,

      uri: hasStringUri(source) ? transformUri(source, imageSize, imageType) : undefined,
      headers: get(props, 'source.headers'),
    };
  }

  render() {
    const {
      source,
      errorImage,
      style,
      children,
      resizeMode,
      imageSize,
      imageType,
      cache,
      priority,
      loadingIndicator,
      ...props
    } = this.props;
    const { isLoaded, isError, headers, uri } = this.state;
    if (
      isError &&
      errorImage &&
      (typeof errorImage === 'number' || typeof errorImage === 'object')
    ) {
      console.log('- CachedImage: error mode');
      return children ? (
        <ImageBackground {...this.props} source={errorImage} />
      ) : (
        <Image {...this.props} source={errorImage} />
      );
    } else if (!isError && typeof source === 'number') {
      console.log('- CachedImage: file mode');
      return children ? (
        <ImageBackground {...this.props} source={source} />
      ) : (
        <Image {...this.props} source={source} />
      );
    } else if (hasStringUri(source)) {
      const resizeModeEnum =
        FastImage.resizeMode[resizeMode] || FastImage.resizeMode.cover;
      console.log('- CachedImage: cached mode');
      return (
        <FastImage
          resizeMode={resizeModeEnum}
          {...props}
          style={style}
          source={{
            priority: FastImage.priority[priority] || 'normal',
            cache: FastImage.cacheControl[cache] || 'immutable',
            ...source,
            uri,
            headers: headers && getS3SignedHeaders(headers),
          }}
          onLoadEnd={() => {
            if (isError) this.setState({ isError: false });
            if (!isLoaded) this.setState({ isLoaded: false });
          }}
          onError={() => {
            console.log('error! fallback to =>', source.uri);
            if (!isError) {
              this.setState({
                isError: true,
                uri:
                  uri === source.uri && errorImage && !isNil(errorImage.uri)
                    ? errorImage.uri
                    : source.uri || '',
              });
            }
          }}
        >
          {children && children}
        </FastImage>
      );
    }
    return <View {...props} style={style} />;
  }
}
