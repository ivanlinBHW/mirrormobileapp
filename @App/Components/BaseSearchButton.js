import React from 'react';
import { Platform, Text, View, Image } from 'react-native';
import { RoundButton } from '@ublocks-react-native/component';
import { isNil, isEmpty, isString, isObject } from 'lodash';
import { PropTypes } from 'prop-types';

import { Colors, Fonts, Metrics } from 'App/Theme';
import { ScaledSheet } from 'App/Helpers';
import { BaseIcon } from 'App/Components';

const styles = ScaledSheet.create({
  img: {
    width: '35@s',
    height: '35@s',
    marginBottom: Metrics.baseMargin / 2,
  },
  wrapper: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '93@s',
  },
  text: {
    ...Fonts.style.extraSmall500,
    color: Colors.button.primary.unSelected.text,
    textAlign: 'center',
    width: '75@s',
    flexWrap: 'wrap',
  },
  selectedText: {
    color: Colors.button.primary.content.text,
  },
  btn: {
    width: Platform.isPad ? '98@s' : '83@s',
    backgroundColor: Colors.button.primary.unSelected.background,
    elevation: 2,
    borderWidth: 0,
    shadowColor: Colors.button.primary.unSelected.shadow,
    marginLeft: Metrics.baseMargin,
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2,
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: {
      height: 3,
      width: 3,
    },
  },
  hasBorderbtn: {
    width: Platform.isPad ? '98@s' : '83@s',
    backgroundColor: Colors.white,
    elevation: 2,
    shadowColor: Colors.shadow_black_03,
    marginLeft: Metrics.baseMargin,
    marginTop: Metrics.baseMargin / 2,
    marginBottom: Metrics.baseMargin / 2,
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: {
      height: 3,
      width: 3,
    },
  },
  squareSize: {
    height: '86@s',
    borderRadius: '12@s',
  },
  normalSize: {
    height: '30@s',
    borderRadius: '15@s',
  },
  isSelectedBtn: {
    backgroundColor: Colors.button.primary.content.background,
  },
});

export default class BaseSearchButton extends React.PureComponent {
  static propTypes = {
    size: PropTypes.string,
    isSelected: PropTypes.bool,
    text: PropTypes.string,
    image: PropTypes.any,
    onPress: PropTypes.func,
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    imageStyle: PropTypes.object,
    hasBorder: PropTypes.bool,
    disabled: PropTypes.bool,
    imageSize: PropTypes.string,
    imageType: PropTypes.string,

    iconName: PropTypes.string,
    iconSize: PropTypes.string,
    iconColor: PropTypes.string,
    iconStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  };

  static defaultProps = {
    size: 'square',
    isSelected: false,
    text: '',
    image: '',
    onPress: () => {},
    imageStyle: {},
    textStyle: {},
    hasBorder: false,
    disabled: false,
    imageSize: undefined,
    imageType: undefined,
  };

  render() {
    const {
      image,
      text,
      isSelected,
      size,
      onPress,
      imageStyle,
      hasBorder,
      disabled,
      textStyle,
      imageSize,
      imageType,
      iconName,
      iconSize,
      iconColor,
      iconStyle,
    } = this.props;

    let source = isString(image)
      ? {
          uri:
            isEmpty(image) || isNil(image) || image.includes('null') ? undefined : image,
        }
      : image;
    if (isObject(source) && (isNil(source.uri) || source.uri === '')) {
      source = undefined;
    }
    return (
      <RoundButton
        style={[
          hasBorder ? styles.hasBorderbtn : styles.btn,
          size === 'square' ? styles.squareSize : styles.normalSize,
          isSelected && styles.isSelectedBtn,
        ]}
        onPress={onPress}
        throttleTime={0}
        disabled={disabled}
      >
        <View style={styles.wrapper}>
          {!!image && <Image source={source} style={[styles.img, imageStyle]} />}
          {!!iconName && (
            <BaseIcon
              name={iconName}
              size={iconSize}
              color={iconColor}
              style={iconStyle}
            />
          )}
          <Text style={[styles.text, textStyle, isSelected && styles.selectedText]}>
            {text}
          </Text>
        </View>
      </RoundButton>
    );
  }
}
