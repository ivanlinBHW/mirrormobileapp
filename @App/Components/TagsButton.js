import React from 'react';
import { PropTypes } from 'prop-types';
import { View, Text} from 'react-native';
import { RoundButton } from '@ublocks-react-native/component';
import { Colors, Fonts, Metrics } from 'App/Theme';

import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  text: {
    ...Fonts.style.extraSmall500,
    color: Colors.black,
    textAlign: 'center',
  },
  selectedText: {
    color: Colors.white,
  },
  btn: {
    backgroundColor: Colors.white,
    padding: '5@s',
  },
  squareSize: {
    height: '86@s',
    borderRadius: '12@s',
  },
  normalSize: {
    height: '30@s',
    borderRadius: '15@s',
  },
  isSelectedBtn: {
    backgroundColor: Colors.primary,
  },
  container: {
    backgroundColor: Colors.white,
    marginLeft: Metrics.baseMargin / 2,
    marginTop: Metrics.baseMargin / 4,
    marginBottom: Metrics.baseMargin / 4,
    flexDirection: 'column',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default class TagsButton extends React.PureComponent {
  static propTypes = {
    size: PropTypes.string,
    isSelected: PropTypes.bool,
    text: PropTypes.string,
    onPress: PropTypes.func,
  };

  static defaultProps = {
    size: 'square',
    isSelected: false,
    text: '',
    onPress: () => {},
  };

  render() {
    const {
      text,
      isSelected,
      size,
      onPress,
    } = this.props;
    return (
      <View style={styles.container}>
        <RoundButton style={[
          styles.btn,
          size === 'square' ? styles.squareSize : styles.normalSize,
          isSelected && styles.isSelectedBtn,
        ]}
        onPress={onPress}
        throttleTime={0}
        >
          <View style={{ alignSelf: 'center'}} >
            <Text style={[styles.text, isSelected && styles.selectedText]}>{text}</Text>
          </View>
        </RoundButton>
      </View>
    );
  }
}
