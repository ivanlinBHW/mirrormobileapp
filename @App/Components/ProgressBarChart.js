import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Platform, processColor, Text } from 'react-native';

import { BarChart } from 'react-native-charts-wrapper';
import { translate as t } from 'App/Helpers/I18n';
import { StyleSheet } from 'App/Helpers';
import { Colors, Fonts, Metrics } from 'App/Theme';

const styles = StyleSheet.create({
  chart: {
    flex: 1,
  },
  txtZeroValues: {
    ...Fonts.style.regular500,
    top: '40%',
    alignSelf: 'center',
  },
  txtUnit: {
    fontSize: Platform.isPad ? Fonts.size.extraSmall : Fonts.size.medium,
    color: Colors.gray_01,
    left: 4,
    top: -Metrics.baseMargin * 2,
    alignSelf: 'flex-start',
    position: 'absolute',
  },
});

export class ProgressBarChart extends PureComponent {
  static propTypes = {
    data: PropTypes.array.isRequired,
    height: PropTypes.number,
    style: PropTypes.any,
    weekdays: PropTypes.array.isRequired,
    unit: PropTypes.string,
  };

  static defaultProps = {
    height: 235,
    style: {},
    unit: 'min',
  };

  static getDerivedStateFromProps(props, state) {
    if (state.xAxis.valueFormatter !== props.weekdays) {
      return {
        ...state,
        xAxis: {
          ...state.xAxis,
          valueFormatter: props.weekdays,
        },
      };
    }
    return null;
  }

  state = {
    legend: {
      enabled: false,
      textSize: 14,
      form: 'SQUARE',
      formSize: 14,
      xEntrySpace: 10,
      yEntrySpace: 5,
      formToTextSpace: 5,
      wordWrapEnabled: true,
      maxSizePercent: 0.5,
      position: 'BELOW_CHART_LEFT',
    },

    xAxis: {
      valueFormatter: ['10/17', '10/18', '10/19', '10/20', '10/21', '10/22', '10/23'],
      granularityEnabled: true,
      granularity: 1,
      drawGridLines: false,
      textSize: 12,
      avoidFirstLastClipping: false,
      position: 'BOTTOM',
      drawLimitLinesBehindData: false,
      drawAxisLine: false,
    },

    yAxis: {
      left: {
        spaceTop: 5,
        granularityEnabled: true,
        granularity: 25.0,
        textColor: processColor('#666'),
        textSize: 12,
        axisMinimum: 0.0,
        axisMaximum: 100.0,
        drawLimitLinesBehindData: false,
        gridLineWidth: 0,
        axisLineWidth: 0,
        drawAxisLine: false,
      },
      right: {
        enabled: false,
      },
    },

    highlights: [],

    data: {
      dataSets: [],
      config: {
        barWidth: 0.5,
        drawGridLines: false,
        drawBorders: false,
      },
    },
  };

  render() {
    const { height, style, weekdays, data } = this.props;
    const { legend, xAxis, yAxis } = this.state;
    const isValuesAreZero = data.every((e) => e === 0);
    const values = data.map((e) => (e === 0 ? 0.5 : e));
    return (
      <React.Fragment>
        {isValuesAreZero && (
          <Text style={styles.txtZeroValues}>{t('progress_chart_no_record')}</Text>
        )}
        <Text style={styles.txtUnit}>{t('min')}</Text>
        <BarChart
          data={{
            dataSets: [
              {
                values,
                label: t('min'),
                config: {
                  color: processColor(Colors.progressBarChart.primary),
                  barWidth: 0.5,
                  drawGridLines: false,
                  drawBorders: false,
                  drawValues: false,
                  touchEnabled: false,
                  highlightEnabled: false,
                },
              },
            ],
          }}
          style={[styles.chart, { height }, style]}
          xAxis={{
            ...xAxis,
            valueFormatter: weekdays,
          }}
          yAxis={yAxis}
          config={data.config}
          legend={legend}
          animation={{ durationX: 2000 }}
          gridBackgroundColor={processColor('#ffffff')}
          visibleRange={{
            x: { min: 7, max: 7 },
            y: { min: 0, max: 100 },
          }}
          drawBarShadow={false}
          drawValueAboveBar={true}
          drawHighlightArrow={true}
          scaleEnabled={false}
          doubleTapToZoomEnabled={false}
          chartDescription={{ text: '' }}
        />
      </React.Fragment>
    );
  }
}

export default ProgressBarChart;
