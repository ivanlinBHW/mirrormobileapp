import React from 'react';
import PropTypes from 'prop-types';
import { View, Alert, Platform, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { EVENTS } from 'App/Monitors/AppMonitor';

import { StyleSheet, Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { Colors, Metrics, Fonts } from 'App/Theme';
import { BaseNavBar, NavBackButton, BaseButton } from 'App/Components';

const styles = StyleSheet.create({
  titleStyle: {
    ...Fonts.style.regular500,
    color: Colors.titleText.primary,
  },
  cancelText: {
    fontSize: Fonts.size.regular,
    color: Colors.button.primary.text.text,
  },
  cancelTextBtn: {
    justifyContent: 'center',
  },
  rightComponent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    position: 'absolute',
  },
  leftComponent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    position: 'absolute',
  },
});

export default class SecondaryNavbar extends React.PureComponent {
  static propTypes = {
    navTitleComponent: PropTypes.any,
    navTitle: PropTypes.string,
    navTitleColor: PropTypes.string,
    navTitleStyle: PropTypes.any,
    navRightComponent: PropTypes.object,
    navLeftComponent: PropTypes.object,
    back: PropTypes.bool,
    backTo: PropTypes.string,
    backConfirm: PropTypes.bool,
    backConfirmTitle: PropTypes.string,
    backConfirmDesc: PropTypes.string,
    navHeight: PropTypes.number,
    cancel: PropTypes.bool,
    cancelTo: PropTypes.string,
    onPressCancel: PropTypes.func,
    onPressBack: PropTypes.func,
    onPressYesBack: PropTypes.func,
    isAutoPop: PropTypes.bool,
    style: PropTypes.object,
  };

  static defaultProps = {
    back: false,
    backTo: '',
    backConfirm: false,
    backConfirmTitle: t('alert_before_leave_title'),
    backConfirmDesc: t('alert_before_leave_desc'),
    cancel: false,
    navTitle: '',
    navTitleColor: Colors.titleText.primary,
    navTitleStyle: {},
    navTitleComponent: null,
    navRightComponent: null,
    navLeftComponent: null,
    navHeight: Metrics.baseNavBarHeightRaw,
    onPressBack: () => {},
    onPressYesBack: () => {},
    isAutoPop: true,
    style: {},
  };

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.backHandler = BackHandler.addEventListener(
        EVENTS.HARDWARE_BACK_PRESS,
        this.handleBackTo,
      );
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      this.backHandler.remove();
    }
  }

  onPressYes = () => {
    const { onPressYesBack } = this.props;
    if (typeof onPressYesBack === 'function') {
      onPressYesBack();
    }
    Actions.pop();
  };

  handleBackTo = () => {
    const {
      backTo,
      backConfirm,
      backConfirmTitle,
      backConfirmDesc,
      onPressBack,
      isAutoPop,
    } = this.props;
    if (typeof onPressBack === 'function') {
      onPressBack();
    }
    if (backConfirm) {
      Alert.alert(backConfirmTitle, backConfirmDesc, [
        {
          text: t('__cancel'),
          style: 'cancel',
        },
        {
          text: t('player_screen_quit'),
          onPress: this.onPressYes,
        },
      ]);
    }
    if (isAutoPop && !backConfirm) {
      backTo ? Actions.popTo(backTo) : Actions.pop();
    }
    return true;
  };

  getNavBarStyle = (navHeight) => ({
    height: Screen.verticalScale(navHeight),
    backgroundColor: Colors.transparent,
    marginHorizontal: 0,
    zIndex: 5000000,
    paddingHorizontal: Metrics.baseMargin,
  });

  renderLeft = () => {
    const { back, cancel, onPressCancel, navLeftComponent, cancelTo } = this.props;
    if (back) {
      return <NavBackButton onPress={this.handleBackTo} />;
    } else if (cancel) {
      return (
        <BaseButton
          transparent
          style={styles.cancelTextBtn}
          textStyle={styles.cancelText}
          textColor={Colors.button.primary.text.text}
          text={t('__cancel')}
          onPress={async () => {
            Actions[cancelTo]();
            if (typeof onPressCancel === 'function') {
              await onPressCancel();
            }
          }}
        />
      );
    } else {
      return navLeftComponent;
    }
  };

  render() {
    const {
      navTitle,
      navTitleColor,
      navTitleStyle,
      navTitleComponent,
      navRightComponent,
      navHeight,
      style,
    } = this.props;
    return (
      <BaseNavBar
        style={[this.getNavBarStyle(navHeight), style]}
        statusbarStyle={Platform.OS === 'ios' ? 'dark-content' : 'default'}
        title={navTitle}
        titleColor={navTitleColor}
        titleStyle={[styles.titleStyle, navTitleStyle]}
        titleComponent={navTitleComponent}
        leftComponent={<View style={styles.leftComponent}>{this.renderLeft()}</View>}
        rightComponent={<View style={styles.rightComponent}>{navRightComponent}</View>}
      />
    );
  }
}
