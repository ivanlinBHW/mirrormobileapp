import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { Text, View, Image } from 'react-native';

import { BaseButton } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Classes, Metrics, Colors, Fonts, Images } from 'App/Theme';

const styles = ScaledSheet.create({
  row: {
    ...Classes.fillRowCenter,
    minHeight: Metrics.baseMargin * 2.5,
  },
  title: {
    ...Fonts.style.medium500,
    textTransform: 'uppercase',
    alignSelf: 'flex-start',
    marginTop: -Metrics.baseMargin / 2,
  },
  subTitle: {
    ...Fonts.style.small500,
    alignSelf: 'flex-start',
  },
  hrLine: {
    borderBottomColor: Colors.black_15,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin / 2,
  },
  btnViewAll: {
    width: '64@s',
    position: 'absolute',
    right: Metrics.baseMargin,
    top: Metrics.baseMargin,
  },
  btnViewAllText: {
    ...Fonts.style.small500,
    textAlign: 'right',
    width: '100%',
  },
  viewAllWrapper: {
    paddingRight: Metrics.baseMargin,
    marginTop: -Metrics.baseMargin / 2,
    height: '14@s',
  },
  image: {
    height: '12@s',
    width: '12@s',
    alignSelf: 'center',
    paddingRight: Metrics.baseMargin,
  },
  rightComponent: {
    paddingRight: 0,
    marginRight: '-2@s',
    marginTop: -Metrics.baseMargin / 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});

export default class HeadLine extends React.PureComponent {
  static propTypes = {
    titleComponent: PropTypes.object,
    title: PropTypes.string.isRequired,
    titleStyle: PropTypes.object,
    titleColor: PropTypes.string,
    subTitle: PropTypes.string,
    subTitleColor: PropTypes.string,
    subTitleStyle: PropTypes.object,
    onViewAll: PropTypes.func,
    hrLine: PropTypes.bool,
    paddingTop: PropTypes.number,
    paddingHorizontal: PropTypes.number,
    hrLineStyle: PropTypes.object,
    onPress: PropTypes.func,
    rightComponent: PropTypes.object,
    paddingBottom: PropTypes.number,
    style: PropTypes.object,
    height: PropTypes.number,
    marginTop: PropTypes.number,
    backgroundColor: PropTypes.string,
  };

  static defaultProps = {
    hrLine: true,
    onPress: null,
    paddingTop: 16,
    paddingHorizontal: 0,
    onViewAll: null,
    rightComponent: null,
    hrLineStyle: {},
    titleComponent: null,
    titleStyle: {},
    titleColor: Colors.black,
    subTitle: '',
    subTitleColor: Colors.error,
    subTitleStyle: {},
    height: 48,
    paddingBottom: 0,
    marginTop: 0,
  };

  getContainerStyle = () => {
    const { paddingTop, paddingHorizontal = 0, paddingBottom } = this.props;
    return [
      styles.row,
      {
        paddingTop: Screen.verticalScale(paddingTop),
        paddingHorizontal: Screen.scale(paddingHorizontal),
        paddingBottom: Screen.verticalScale(paddingBottom),
      },
    ];
  };

  render() {
    const {
      titleComponent,
      title,
      titleColor,
      titleStyle,
      subTitle,
      subTitleColor,
      subTitleStyle,
      hrLine,
      hrLineStyle,
      style,
      height = 48,
      backgroundColor,
      rightComponent,
      onPress,
      onViewAll,
      marginTop,
    } = this.props;
    const Component = onPress ? BaseButton : View;
    return (
      <Component
        style={[
          {
            backgroundColor,
            height: Screen.scale(height),
            marginBottom: Metrics.baseMargin,
            marginTop: Screen.verticalScale(marginTop),
          },
          style,
        ]}
        onPress={onPress}
        transparent
      >
        <View style={this.getContainerStyle()}>
          <View style={Classes.fillCenter}>
            {titleComponent || (
              <Text style={[styles.title, { color: titleColor }, titleStyle]}>
                {title}
              </Text>
            )}
            {!isEmpty(subTitle) && (
              <Text style={[styles.subTitle, { color: subTitleColor }, subTitleStyle]}>
                {subTitle}
              </Text>
            )}
          </View>

          {/* view-all */}
          {onViewAll && (
            <BaseButton
              onPress={onViewAll}
              height={Screen.verticalScale(16)}
              textColor={Colors.button.primary.text.text}
              text={t('see_all')}
              style={styles.btnViewAll}
              textStyle={styles.btnViewAllText}
              throttleTime={0}
              transparent
            />
          )}

          {!onViewAll && onPress && (
            <View style={[Classes.crossEnd, Classes.mainCenter]}>
              <Image resizeMode="contain" style={styles.image} source={Images.next} />
            </View>
          )}
          {!onViewAll && !onPress && (
            <View style={styles.rightComponent}>{rightComponent}</View>
          )}
        </View>
        {hrLine && <View style={[styles.hrLine, hrLineStyle]} />}
      </Component>
    );
  }
}
