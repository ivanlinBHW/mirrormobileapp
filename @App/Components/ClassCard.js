import { Colors, Fonts, Images, Metrics } from 'App/Theme';
import { Platform, Text, View } from 'react-native';
import {
  ImageButton,
  BaseButton as RoundButton,
  RoundLabel,
  CachedImage as Image,
  CachedImage as ImageBackground,
} from 'App/Components';
import { ScaledSheet, Screen, Date as d, getRoundNumber } from 'App/Helpers';
import { isEmpty, isString, get } from 'lodash';

import { AppStore, TrackRecordActions } from 'App/Stores';

import LinearGradient from 'react-native-linear-gradient';
import { PropTypes } from 'prop-types';
import React from 'react';
import moment from 'moment';
import { translate as t } from 'App/Helpers/I18n';

const CARD_WIDTH = () =>
  getRoundNumber(
    Platform.isPad
      ? Screen.width - Metrics.baseMargin * 2
      : Screen.width - Metrics.baseMargin * 3,
  );

const CARD_HEIGHT = () =>
  getRoundNumber(Platform.isPad ? CARD_WIDTH() / 1.9834710744 : Screen.scale(214));

const styles = ScaledSheet.create({
  base: {
    marginLeft: Metrics.baseMargin,
    overflow: 'hidden',
    borderRadius: '12@vs',
    borderWidth: 0,
  },
  sizeBaseNormal: {
    width: Screen.scale(281),
    height: Screen.scale(159),
  },
  sizeBaseLarge: {
    width: CARD_WIDTH(),
    height: CARD_HEIGHT(),
  },
  sizeBaseFull: {
    height: CARD_HEIGHT(),
    marginRight: Metrics.baseMargin,
  },
  sizeNormal: {
    width: Screen.scale(281),
    height: Screen.scale(159),
  },
  sizeLarge: {
    width: CARD_WIDTH(),
    height: CARD_HEIGHT(),
  },
  sizeFull: {
    width: '100%',
    height: CARD_HEIGHT(),
  },
  normalImgWidth: {
    width: Screen.scale(301),
    height: Screen.scale(159),
    borderRadius: 12,
    overflow: 'hidden',
  },
  largeImgWidth: {
    width: '102%',
    height: CARD_HEIGHT(),
    borderRadius: '12@vsr',
    overflow: 'hidden',
  },
  fullImgWidth: {
    width: '102%',
    height: CARD_HEIGHT(),
    borderRadius: '12@vsr',
    overflow: 'hidden',
  },
  title: {
    ...Fonts.style.regular500,
    color: Colors.secondary,
  },
  boxStyle: {
    minWidth: '80@s',
    height: '15@s',
    borderRadius: '7.5@s',
    backgroundColor: Colors.black,
    marginBottom: '4@s',
    alignItems: 'center',
  },
  text: {
    ...Fonts.style.extraSmall500,
    color: '#ffffff',
  },
  liveBoxStyle: {
    borderRadius: 7.5,
    width: '55@s',
    height: '15@vs',
    backgroundColor: 'white',
    marginBottom: 4,
    minWidth: Metrics.baseMargin * 4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  liveText: {
    fontSize: 10,
    fontWeight: '500',
    letterSpacing: 1,

    color: Colors.primary,
  },
  fragment: {
    flexDirection: 'row',
    bottom: 0,
    left: 0,
    zIndex: 100,
    alignContent: 'space-between',
    width: '100%',
    height: '100%',
    borderRadius: '12@vs',
    position: 'absolute',
  },
  leftBox: {
    flex: 1,
    marginBottom: Platform.isPad ? Metrics.baseMargin : 0,
  },
  rightBox: {
    width: '85@s',
    marginRight: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin / 2,
  },
  left: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    height: '100%',
    marginLeft: Metrics.baseMargin,
  },
  right: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: '100%',
  },
  time: {
    fontSize: '10@vs',
    color: '#f0f0f0',
  },
  secondTitle: {
    ...Fonts.style.regular500,
    color: Colors.white,
    letterSpacing: 1,
    marginBottom: '8@vs',
  },
  secondIcon: {
    width: '50@vs',
    backgroundColor: 'blue',
    color: Colors.white,
    flexDirection: 'row',
  },
  equipments: {
    width: '20@vs',
    height: '20@vs',
  },
  imageBox: {
    width: '40@vs',
    height: '40@vs',
    borderRadius: '40@vs',
    marginRight: '8@s',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  equipmentsBox: {
    flexDirection: 'row',
  },
  imgStyle: {
    width: '19@s',
    height: '32@s',
  },
  btnStyle: {
    width: '19@s',
    height: '32@s',
    position: 'absolute',
    right: Metrics.baseMargin,
    top: '-2@s',
    zIndex: 200,
  },
  timeBox: {
    width: '100%',
    height: '13@vs',
    marginBottom: '12@vs',
  },
  blackBox: {
    backgroundColor: Colors.black_80,
  },
  transparent: {
    backgroundColor: 'transparent',
  },
  baseBox: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 100,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: CARD_HEIGHT(),
    borderRadius: '12@vs',
  },
  basePosition: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 99,
  },
  finishImg: {
    width: '32@s',
    height: '32@s',
    zIndex: 122122,
  },
  completedText: {
    ...Fonts.style.medium500,
    marginTop: Metrics.baseMargin / 2,
    color: Colors.white,
  },
  noBorder: {
    borderWidth: 0,
  },
  motionImg: {
    width: '30@s',
    height: '30@vs',
    marginBottom: Metrics.baseMargin / 4,
  },
  lastItem: {
    marginRight: Metrics.baseMargin,
  },
});

export default class ClassCard extends React.PureComponent {
  static propTypes = {
    data: PropTypes.object,
    size: PropTypes.string,
    title: PropTypes.string,
    onPressMarked: PropTypes.func,
    onPress: PropTypes.func,
    onPressBookmark: PropTypes.func,
    isHover: PropTypes.bool,
    leftBoxStyle: PropTypes.object,
    rightBoxStyle: PropTypes.object,
    bookmarkStyle: PropTypes.object,
    lastItem: PropTypes.bool,
    timezone: PropTypes.any.isRequired,
  };

  static defaultProps = {
    isHover: false,
    leftBoxStyle: {},
    rightBoxStyle: {},
    bookmarkStyle: {},
    timezone: 'Asia/Taipei',
    lastItem: false,
  };

  getBaseStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.sizeBaseNormal;
      case 'large':
        return styles.sizeBaseLarge;
      case 'full':
        return styles.sizeBaseFull;
      default:
        return styles.sizeBaseNormal;
    }
  };

  getWrapStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.sizeNormal;
      case 'large':
        return styles.sizeLarge;
      case 'full':
        return styles.sizeFull;
      default:
        return styles.sizeNormal;
    }
  };

  getImgStyle = () => {
    const { size } = this.props;
    switch (size) {
      case 'normal':
        return styles.normalImgWidth;
      case 'large':
        return styles.largeImgWidth;
      case 'full':
        return styles.fullImgWidth;
      default:
        return styles.sizeNormal;
    }
  };

  handleOnPress = () => {
    const { onPress, data } = this.props;

    AppStore.dispatch(
      TrackRecordActions.track({
        trackObjectType: 'TrainingClass',
        trackObjectId: data.id,
      }),
    );

    onPress(data.id, data);
  };

  renderLive = () => {
    const { data } = this.props;
    if (data.isLive) {
      return (
        <RoundLabel
          text={t(`__live`)}
          backgroundColor={Colors.white}
          color={Colors.primary}
        />
      );
    }
  };

  renderTime = () => {
    const { data, timezone } = this.props;
    if (!isEmpty(data.scheduledAt)) {
      const duration = d.transformDiffMinuteTime(data.scheduledAt);
      if (duration > 60) {
        return (
          <Text style={styles.time}>
            {`${t('aired')} ${data.scheduledAt &&
              moment(d.transformDate(data.scheduledAt, timezone)).format(
                d.FULL_DATE_FORMAT,
              )}`}
          </Text>
        );
      } else if (duration < 0) {
        if (duration < -60) {
          return <Text style={styles.time}>{t('class_card_class_in_end')}</Text>;
        } else {
          return <Text style={styles.time}>{t('class_card_class_in_progress')}</Text>;
        }
      } else {
        return (
          <Text style={styles.time}>
            {t('class_card_class_begins_in')} {Math.round(duration)} {t('min')}
          </Text>
        );
      }
    }
  };

  renderEquipments = () => {
    const { data } = this.props;
    return data.requiredEquipments.map((item, index) => {
      return (
        <View key={index} style={styles.imageBox}>
          <Image
            source={{ uri: get(item, 'signedCoverImage') || '' }}
            style={styles.equipments}
            resizeMode="contain"
            imageSize="1x"
            imageType="v"
          />
        </View>
      );
    });
  };

  render() {
    const {
      data,
      onPressBookmark,
      isHover,
      leftBoxStyle,
      rightBoxStyle,
      bookmarkStyle,
      lastItem,
      timezone,
    } = this.props;

    let isNewArrival = false;
    if (data.publishAt) {
      const aWeekAgo = d.transformDate(d.moment().subtract(7, 'd'), timezone);
      const publishAt = d.transformDate(data.publishAt, timezone);
      isNewArrival = d.isSameOrAfterDate(publishAt, aWeekAgo);
    }
    return (
      <View style={[styles.base, this.getBaseStyle(), lastItem && styles.lastItem]}>
        <RoundButton
          style={[this.getWrapStyle(), styles.noBorder]}
          onPress={this.handleOnPress}
          roundRadius={12}
          throttleTime={300}
        >
          <ImageBackground
            resizeMode="cover"
            source={data.signedCoverImageObj || { uri: data.signedCoverImage }}
            style={[this.getImgStyle()]}
            imageSize="1x"
            imageType="h"
          >
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
              }}
              locations={[0, 1]}
              colors={[`rgba(50,50,50,0)`, `rgba(50,50,50,0.4)`]}
            />
          </ImageBackground>
          <View style={styles.fragment}>
            {onPressBookmark &&
              ((data.classType && data.classType === 0) || !data.classType) &&
              (data.isBookmarked ? (
                <ImageButton
                  throttleTime={0}
                  image={Images.bookmark_solid}
                  imageStyle={styles.imgStyle}
                  style={[styles.btnStyle, bookmarkStyle]}
                  onPress={() => onPressBookmark(data.id, data.isBookmarked)}
                />
              ) : (
                <ImageButton
                  throttleTime={0}
                  image={Images.bookmark}
                  imageStyle={styles.imgStyle}
                  style={[styles.btnStyle, bookmarkStyle]}
                  onPress={() => onPressBookmark(data.id, data.isBookmarked)}
                />
              ))}
            <View style={[styles.leftBox, leftBoxStyle]}>
              <View style={styles.left}>
                {!isEmpty(data.requiredEquipments) &&
                  data.requiredEquipments.length > 0 && (
                    <View style={styles.equipmentsBox}>{this.renderEquipments()}</View>
                  )}
                <Text style={styles.title} numberOfLines={2}>
                  {isString(data.title) && data.title.toUpperCase()}
                </Text>
                <Text style={styles.secondTitle}>
                  {!isEmpty(data.instructor) &&
                    isString(data.instructor.title) &&
                    data.instructor.title.toUpperCase()}
                </Text>
                <View style={styles.timeBox}>{this.renderTime()}</View>
              </View>
            </View>
            <View style={[styles.rightBox, rightBoxStyle]}>
              <View style={styles.right}>
                {data.hasMotionCapture && (
                  <Image source={Images.motion_capture_icon} style={styles.motionImg} />
                )}
                {this.renderLive()}
                {isNewArrival && (
                  <RoundLabel
                    backgroundColor={Colors.white}
                    color={Colors.primary}
                    text={t('new')}
                  />
                )}
                <RoundLabel text={t(`search_lv${data.level}`)} />
                <RoundLabel text={`${Math.round(data.duration / 60)} ${t('min')}`} />
              </View>
            </View>
          </View>
          {isHover && (
            <View style={[styles.blackBox, styles.baseBox]}>
              <Image source={Images.finished_secondary} style={styles.finishImg} />
              <Text style={styles.completedText}>
                {t('class_card_workout_completed')}
              </Text>
            </View>
          )}
        </RoundButton>
      </View>
    );
  }
}
