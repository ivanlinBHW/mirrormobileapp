import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  Platform,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import StarRating from 'react-native-star-rating';
import RNPickerSelect from 'react-native-picker-select';

import StepIndicator from 'react-native-step-indicator';
import { translate as t } from 'App/Helpers/I18n';
import { Screen, ScaledSheet } from 'App/Helpers';
import { SquareButton, BaseIcon } from 'App/Components';
import { Colors, Classes, Fonts, Metrics } from 'App/Theme';

const pickerSelectStyles = ScaledSheet.create({
  inputIOS: {
    fontSize: Fonts.size.small,
    paddingVertical: '10@vs',
    borderWidth: 1,
    borderColor: Colors.black,
    borderRadius: 2,
    color: 'black',
    marginHorizontal: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
  },
  inputAndroid: {
    fontSize: Fonts.size.small,
    paddingVertical: '10@vs',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 2,
    color: 'black',
    marginHorizontal: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
  },
  iconContainer: {
    ...Platform.select({
      ios: {
        right: 30,
        top: 6,
      },
      android: {
        right: 32,
        top: 10,
      },
    }),
  },
});

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'column',
    paddingTop: '13@vs',
  },
  wrapperBox: {
    ...Classes.fill,
  },
  title: {
    ...Fonts.style.regular500,
    textAlign: 'center',
  },
  question: {
    ...Fonts.style.medium500,
    marginTop: '48.5@vs',
    marginBottom: '24@vs',
    textAlign: 'center',
  },
  star: {
    marginRight: '10@s',
  },
  starBox: {
    width: Screen.width - Screen.scale(80),
    marginLeft: '40@s',
  },
  stepBox: {
    backgroundColor: '#ffffff',
    marginHorizontal: Metrics.baseMargin,
  },
  pickerItem: {
    width: '100%',
  },
  textInputBox: {
    height: '104@vs',
    borderWidth: 1,
    borderColor: '#545454',
    marginTop: Metrics.baseMargin,
    marginBottom: '35@vs',
    textAlignVertical: 'top',
    marginHorizontal: Metrics.baseMargin,
    width: Screen.width - Screen.scale(32),
    paddingHorizontal: Metrics.baseMargin,
    paddingTop: Metrics.baseMargin / 2,
  },
  submitStyle: {
    height: '44@vs',
    color: Colors.white,
    fontSize: '14@vs',
  },
  flexWrap: {
    flex: 1,
  },
});

const thirdIndicatorStyles = {
  stepIndicatorSize: 9,
  currentStepIndicatorSize: 9,
  separatorStrokeWidth: 1,
  currentStepStrokeWidth: 1,
  stepStrokeCurrentColor: Colors.fontIcon.primary,
  stepStrokeWidth: 1,
  stepStrokeFinishedColor: Colors.fontIcon.primary,
  stepStrokeUnFinishedColor: Colors.fontIcon.primary,
  separatorFinishedColor: Colors.fontIcon.primary,
  separatorUnFinishedColor: Colors.fontIcon.primary,
  stepIndicatorFinishedColor: Colors.fontIcon.primary,
  stepIndicatorUnFinishedColor: Colors.white,
  stepIndicatorCurrentColor: Colors.white,
  stepIndicatorLabelFontSize: 0,
  currentStepIndicatorLabelFontSize: 0,
  stepIndicatorLabelCurrentColor: 'transparent',
  stepIndicatorLabelFinishedColor: 'transparent',
  stepIndicatorLabelUnFinishedColor: 'transparent',
  labelColor: '#000000',
  labelSize: 10,
  padding: 5,
  currentStepLabelColor: '#000000',
};

export default class FeedbackPanel extends React.Component {
  static propTypes = {
    onPressSubmit: PropTypes.func.isRequired,
    detail: PropTypes.object,
  };

  state = {
    selectText: t('feedback_select_select_1'),
    classRate: 5,
    instructorRate: 5,
    feelRate: 3,
    userComment: t('feedback_select_select_1'),
  };

  onQ1StarRatingPress(rating) {
    this.setState({
      classRate: rating,
    });
  }

  onQ2StarRatingPress(rating) {
    this.setState({
      instructorRate: rating,
    });
  }

  onStepPress = (position) => {
    this.setState({ feelRate: position + 1 });
  };

  onPressPicker = (itemValue) => {
    if (itemValue) {
      this.setState({
        selectText: itemValue,
        userComment: itemValue,
      });
    }
  };

  onChangeText = (value) => {
    this.setState({
      userComment: value,
    });
  };

  onPressSubmit = () => {
    const { onPressSubmit } = this.props;
    const { classRate, instructorRate, feelRate, userComment } = this.state;

    const payload = {
      classRate,
      instructorRate,
      feelRate,
      userComment,
    };
    onPressSubmit(payload);
  };

  render() {
    const { selectText, classRate, instructorRate, feelRate, userComment } = this.state;
    const { detail } = this.props;

    return (
      <>
        <KeyboardAvoidingView
          style={styles.wrapperBox}
          keyboardVerticalOffset={
            Platform.OS === 'ios' ? Screen.scale(48) : Screen.scale(72)
          }
          behavior={Platform.OS === 'ios' ? 'position' : 'height'}
          enable
        >
          <ScrollView>
            <View style={styles.container}>
              <Text style={styles.title}>{t('workout_detail_feedback')}</Text>
              <Text style={styles.question}>{t('feedback_q1')}</Text>
              <StarRating
                disabled={false}
                maxStars={5}
                rating={classRate}
                starStyle={styles.star}
                starSize={25}
                selectedStar={(rating) => this.onQ1StarRatingPress(rating)}
                fullStarColor={Colors.fontIcon.primary}
                emptyStarColor={Colors.fontIcon.primary}
                containerStyle={styles.starBox}
              />
              <Text style={styles.question}>
                {t('feedback_q2', {
                  name: detail.instructor ? detail.instructor.title : '',
                })}
              </Text>
              <StarRating
                disabled={false}
                maxStars={5}
                rating={instructorRate}
                starStyle={styles.star}
                starSize={25}
                selectedStar={(rating) => this.onQ2StarRatingPress(rating)}
                fullStarColor={Colors.fontIcon.primary}
                emptyStarColor={Colors.fontIcon.primary}
                containerStyle={styles.starBox}
              />
              <Text style={styles.question}>{t('feedback_q3')}</Text>
              <View style={styles.stepBox}>
                <StepIndicator
                  stepCount={5}
                  customStyles={thirdIndicatorStyles}
                  currentPosition={feelRate}
                  onPress={this.onStepPress}
                  labels={[
                    t('feedback_step_1'),
                    '',
                    t('feedback_step_3'),
                    '',
                    t('feedback_step_5'),
                  ]}
                />
              </View>
              <Text style={styles.question}>{t('feedback_q4')}</Text>
              <RNPickerSelect
                style={pickerSelectStyles}
                value={selectText}
                Icon={() => <BaseIcon name="angle-down" />}
                onValueChange={(value) => this.onPressPicker(value)}
                useNativeAndroidPickerStyle={false}
                items={[
                  {
                    label: t('feedback_select_select_1'),
                    value: t('feedback_select_select_1'),
                  },
                  {
                    label: t('feedback_select_select_2'),
                    value: t('feedback_select_select_2'),
                  },
                  {
                    label: t('feedback_select_select_3'),
                    value: t('feedback_select_select_3'),
                  },
                ]}
              />
              <TextInput
                style={styles.textInputBox}
                onChangeText={this.onChangeText}
                multiline
                numberOfLines={1}
                value={userComment}
                maxLength={254}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <SquareButton
          text={t('submit')}
          onPress={this.onPressSubmit}
          disabled={!userComment}
        />
      </>
    );
  }
}
