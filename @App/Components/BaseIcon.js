import React from 'react';
import { PropTypes } from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome5';

import { Colors } from 'App/Theme';
import { Screen } from 'App/Helpers';

const BaseIcon = ({ name, color, size, ...props }) => (
  <Icon name={name} color={color} size={Screen.scale(size)} {...props} />
);

BaseIcon.propTypes = {
  name: PropTypes.string.isRequired,
  size: PropTypes.number,
  color: PropTypes.string,
};

BaseIcon.defaultProps = {
  size: 24,
  color: Colors.black,
};

export default BaseIcon;
