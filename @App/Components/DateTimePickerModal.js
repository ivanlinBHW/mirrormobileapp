import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-native-date-picker';

import { View } from 'react-native';
import { BaseModal } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Classes } from 'App/Theme';

class DateTimePickerModal extends React.PureComponent {
  static propTypes = {
    onCancelPress: PropTypes.func,
    mode: PropTypes.string,
  };

  static defaultProps = {
    onCancelPress: undefined,
    mode: 'date',
  };

  render() {
    const { onCancelPress, mode, ...props } = this.props;
    return (
      <BaseModal
        title={
          mode !== 'time'
            ? t('community_event_creation_input_choose_date')
            : t('community_pick_time')
        }
        handleClose={onCancelPress}
        height="40%"
      >
        <View style={Classes.fillCenter}>
          <DatePicker {...props} mode={mode} />
        </View>
      </BaseModal>
    );
  }
}

export default DateTimePickerModal;
