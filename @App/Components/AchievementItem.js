import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { BaseImageButton } from 'App/Components';
import { StyleSheet } from 'App/Helpers';
import { Images } from 'App/Theme';

const styles = StyleSheet.create({
  container: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  image: {
    paddingLeft: 20,
    paddingRight: 20,
  },
});

const getImageName = (name, active = false) => {
  return `${name.replace('_amount', '')}${active ? '_active' : ''}`;
};

const handleOpenAchievementScreen = ({
  name,
  date,
  active,
  title,
  description,
}) => () => {
  Actions.AchievementScreen({
    achievement: name,
    date,
    active,
    title,
    description,
  });
};

const AchievementItem = ({
  large,
  name,
  date,
  active,
  onPress,
  disabled,
  title,
  description,
}) => {
  return Images[getImageName(name, active)] ? (
    <BaseImageButton
      source={Images[getImageName(name, active)]}
      style={[styles.container, { opacity: 1 }]}
      height={large ? 270 : 66}
      width={large ? 270 : 66}
      imageHeight={large ? 270 : 64}
      imageWidth={large ? 270 : 64}
      imageStyle={styles.image}
      onPress={
        onPress || handleOpenAchievementScreen({ name, date, active, title, description })
      }
      disabled={disabled}
    />
  ) : (
    <Text>{name}</Text>
  );
};

AchievementItem.propTypes = {
  name: PropTypes.string.isRequired,
  date: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  active: PropTypes.bool,
  large: PropTypes.bool,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
};

AchievementItem.defaultTypes = {
  disabled: false,
  active: false,
  large: false,
  onPress: null,
  date: '',
  title: '',
  description: '',
};

export default AchievementItem;
