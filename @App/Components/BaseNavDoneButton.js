import React from 'react';
import { PropTypes } from 'prop-types';
import { TouchableOpacity, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { Colors, Fonts } from 'App/Theme';
import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet } from 'App/Helpers';

const styles = ScaledSheet.create({
  navText: {
    fontSize: Fonts.size.regular,
    color: Colors.button.primary.text.text,
  },
  disabledText: {
    color: Colors.gray_03,
  },
});

const BaseNavDoneButton = ({ onPress, disabled }) => (
  <TouchableOpacity onPress={onPress} disabled={disabled}>
    <Text style={[styles.navText, disabled && styles.disabledText]}>{t('__done')}</Text>
  </TouchableOpacity>
);

BaseNavDoneButton.propTypes = {
  onPress: PropTypes.func,
  size: PropTypes.number,
  color: PropTypes.string,
  disabled: PropTypes.bool,
};

BaseNavDoneButton.defaultProps = {
  onPress: () => Actions.pop(),
  color: Colors.black,
  disabled: false,
};

export default BaseNavDoneButton;
