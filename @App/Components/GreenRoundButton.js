import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity } from 'react-native';

import { StyleSheet, Screen } from 'App/Helpers';
import { Colors, Fonts } from 'App/Theme';

const styles = StyleSheet.create({
  pressBox: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  btnBox: {
    width: '188@s',
    height: '188@s',
    borderRadius: '94@s',
  },
  pressBtn: {
    width: '134@s',
    height: '134@s',
    borderRadius: '67@s',
    margin: '27@s',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const GreenRoundButton = ({
  onPress,
  size,
  text,
  outsideColor,
  insideColor,
  textColor,
  ...props
}) => (
  <View style={styles.pressBox}>
    <View
      style={[
        styles.btnBox,
        {
          backgroundColor: outsideColor,
          width: Screen.scale(size),
          height: Screen.scale(size),
        },
      ]}
    >
      <TouchableOpacity
        {...props}
        style={[styles.pressBtn, { backgroundColor: insideColor }]}
        onPress={onPress}
      >
        <Text style={[Fonts.style.h1_500, { color: textColor }]}>{text}</Text>
      </TouchableOpacity>
    </View>
  </View>
);

GreenRoundButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  size: PropTypes.number,
  text: PropTypes.string,
  outsideColor: PropTypes.string,
  insideColor: PropTypes.string,
  textColor: PropTypes.string,
};

GreenRoundButton.defaultProps = {
  size: 188,
  outsideColor: Colors.secondary,
  insideColor: Colors.white,
  textColor: Colors.green_blue,
};

export default GreenRoundButton;
