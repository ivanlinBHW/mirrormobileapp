import { ActivityIndicator, FlatList, Image, Text, View } from 'react-native';
import { BaseButton, ImageButton } from 'App/Components';
import BleActions, { BleServices } from 'App/Stores/BleDevice/Actions';
import { Classes, Colors, Fonts, Images, Metrics } from 'App/Theme';
import { Dialog, StyleSheet } from 'App/Helpers';

import { Badge } from 'react-native-elements';
import PropTypes from 'prop-types';
import React from 'react';
import { State } from 'react-native-ble-plx';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { filterNotPairedHeartRateDevices } from 'App/Stores/Mirror/Selectors';
import { isEmpty } from 'lodash';
import { translate as t } from 'App/Helpers/I18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 100,
  },
  flatBox: {
    flexDirection: 'row',
    paddingVertical: Metrics.baseMargin / 2,
    borderBottomWidth: 1,
    borderBottomColor: Colors.black_10,
    alignItems: 'center',
  },
  img: {
    width: '32@s',
    height: '32@s',
    marginLeft: Metrics.baseMargin,
    marginRight: '40@s',
  },
  title: {
    fontSize: Fonts.size.medium500,
    lineHeight: '30@vs',
  },
  badge: {
    width: '12@s',
    height: '12@s',
    borderRadius: '12@s',
  },
  badgeBox: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
    lineHeight: '30@vs',
    height: '30@vs',
    width: '30@s',
    marginRight: '17@vs',
  },
  header: {
    fontSize: Fonts.size.medium,
    marginLeft: Metrics.baseMargin,
    justifyContent: 'center',
  },
  headerBox: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: '13@s',
    marginBottom: Metrics.baseMargin,
  },
  button: {
    width: '32@s',
    height: '32@s',
  },
  imgStyle: {
    width: '32@s',
    height: '32@s',
  },
});

class BleDevicePanel extends React.PureComponent {
  static propTypes = {
    onConnect: PropTypes.func.isRequired,
    onDisconnect: PropTypes.func.isRequired,
    onScan: PropTypes.func.isRequired,
    stopScan: PropTypes.func.isRequired,
    activeDevice: PropTypes.object,
    pairedDevice: PropTypes.object,
    pairList: PropTypes.array.isRequired,
    unPairList: PropTypes.array.isRequired,
    deviceList: PropTypes.array.isRequired,
    isScanning: PropTypes.bool.isRequired,
    bleState: PropTypes.string.isRequired,
    scanning: PropTypes.bool,
  };

  static defaultProps = {
    scanning: true,
  };

  componentDidMount() {
    __DEV__ && console.log('@BleDevicePanel');

    this.onRefreshBleDevice();

    const { bleState } = this.props;
    this.checkBleStatus(bleState);
  }

  componentDidUpdate(prevProps) {
    const { scanning, bleState } = this.props;
    if (prevProps.bleState !== bleState) {
      this.checkBleStatus(bleState);
    }
    if (scanning !== prevProps.scanning) {
      const { stopScan, onScan } = this.props;
      if (scanning) {
        onScan([BleServices.HEAR_RATE_SERVICE_GUID], false);
      } else {
        stopScan();
      }
    }
  }

  onRefreshBleDevice = () => {
    const { onScan, stopScan } = this.props;
    onScan([BleServices.HEAR_RATE_SERVICE_GUID], true);
    setTimeout(() => {
      stopScan();
    }, 5000);
  };

  onPressConnectDevice = (targetDevice) => () => {
    const { activeDevice, onConnect, onDisconnect } = this.props;
    if (activeDevice && activeDevice.id === targetDevice.id) {
      Dialog.showConfirmDisconnectHeartRateDeviceAlert(() => onDisconnect(targetDevice));

      this.onRefreshBleDevice();
    } else {
      if (activeDevice) {
        Dialog.showConfirmDisconnectHeartRateDeviceAlert(() =>
          onDisconnect(activeDevice),
        );
      }
      onConnect(targetDevice);
    }
  };

  checkBleStatus = (bleState) => {
    if (bleState === State.PoweredOn) {
      this.onRefreshBleDevice();
    } else if (bleState === State.PoweredOff) {
      Dialog.bluetoothPowerOffAlert();
    } else if (bleState === State.Unsupported) {
      Dialog.bluetoothUnsupportedAlert();
    } else if (bleState === State.Unauthorized) {
      Dialog.requestBluetoothPermissionFromSystemAlert();
    }
  };

  renderBleItem = ({ item }) => {
    const { deviceList, activeDevice, pairedDevice, isScanning } = this.props;
    const targetDevice = [...deviceList, activeDevice].find((e) => e && e.id === item.id);
    return (
      <BaseButton
        style={[Classes.mainStart, { marginBottom: Metrics.baseMargin }]}
        disabled={!targetDevice || isScanning}
        onPress={this.onPressConnectDevice(targetDevice)}
        transparent
      >
        <View style={styles.flatBox}>
          <Image source={Images.bluetooth} style={styles.img} />
          <Text style={styles.title}>
            {item.localName || item.name || 'Heart Rate Device'}
            {/* {__DEV__ && `\n(${item.id})`} */}
          </Text>
          <View style={styles.badgeBox}>
            {activeDevice && item.id === activeDevice.id && (
              <Badge status="success" badgeStyle={styles.badge} />
            )}
            {/* {!activeDevice && targetDevice && targetDevice.id === pairedDevice.id && (
              <ImageButton
                style={styles.button}
                imageStyle={styles.imgStyle}
                image={Images.refresh}
              />
            )} */}
          </View>
        </View>
      </BaseButton>
    );
  };

  render() {
    const { unPairList, pairedDevice, activeDevice, isScanning, bleState } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          ListHeaderComponent={
            <View style={styles.headerBox}>
              <Text style={styles.header}>
                {t('heart_rate_pair_devices')}
                {activeDevice ? activeDevice.id : ''}
              </Text>
              {isScanning ? (
                <ActivityIndicator size="small" color="black" style={styles.imgStyle} />
              ) : (
                <ImageButton
                  style={styles.button}
                  imageStyle={styles.imgStyle}
                  image={Images.refresh}
                  onPress={this.onRefreshBleDevice}
                  disabled={bleState !== State.PoweredOn}
                />
              )}
            </View>
          }
          data={!isEmpty(activeDevice) ? [activeDevice] : []}
          keyExtractor={(item) => item.id}
          renderItem={this.renderBleItem}
          ListFooterComponent={
            <FlatList
              ListHeaderComponent={
                <View style={styles.headerBox}>
                  <Text style={styles.header}>{t('heart_rate_other_devices')}</Text>
                </View>
              }
              data={unPairList}
              keyExtractor={(item) => item.id}
              renderItem={this.renderBleItem}
            />
          }
        />
      </View>
    );
  }
}

export default connect(
  (state) => ({
    bleState: state.bleDevice.bleState,
    routeName: state.appRoute.routeName,
    isLoading: state.appState.isLoading,
    deviceList: state.bleDevice.searchDevices,
    unPairList: filterNotPairedHeartRateDevices(state),
    pairList: state.mirror.blePairedDevices,
    activeDevice: state.bleDevice.activeDevice,
    pairedDevice: state.mirror.blePairedHeartRateDevice,
    isScanning: state.bleDevice.isScanning,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        onScan: BleActions.onScan,
        stopScan: BleActions.onStopScan,
        onConnect: BleActions.onConnect,
        onDisconnect: BleActions.onDisconnect,
      },
      dispatch,
    ),
)(BleDevicePanel);
