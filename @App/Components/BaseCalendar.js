import {
  CalendarProvider,
  ExpandableCalendar,
  LocaleConfig,
} from 'react-native-calendars';
import { Colors, Global } from 'App/Theme';
import { Platform, View } from 'react-native';
import { Screen, StyleSheet, Date as d } from 'App/Helpers';

import Locales from 'App/Locales';
import PropTypes from 'prop-types';
import React from 'react';

const styles = StyleSheet.create({
  shadowBar: {
    marginTop: Platform.select({
      ios: '8@vs',
      android: '16@vs',
    }),
    marginBottom: Platform.select({
      ios: Platform.isPad ? '48@s' : '16@s',
      android: '16@vs',
    }),
    zIndex: 1000,
    height: Platform.select({
      ios: Platform.isPad ? '5@s' : '10@s',
      android: '10@vs',
    }),
    backgroundColor: Colors.white,

    elevation: 10,
    shadowColor: Colors.shadow,
    shadowOffset: { height: '4@s', width: 0 },
    shadowOpacity: 1,
    shadowRadius: 5,
  },
  shadowBlocker: {
    backgroundColor: Colors.white,
    marginTop: Platform.select({
      ios: Platform.isPad ? '-24@s' : '-6@s',
      android: '-8@vs',
    }),
    height: Platform.select({
      ios: Platform.isPad ? '26@s' : '10@s',
      android: '10@vs',
    }),
    zIndex: 5000,
  },
  calendar: {
    zIndex: 10000,
  },
  calendarStyle: {
    backgroundColor: Colors.white,
    zIndex: 10000,
    marginTop: 0,
    paddingTop: 0,
    paddingBottom: 0,
    marginBottom: Platform.isPad ? '24@s' : '6@vs',
  },
});

export default class BaseCalendar extends React.PureComponent {
  static propTypes = {
    locale: PropTypes.string,
    current: PropTypes.string,
    date: PropTypes.any,
    dateTitle: PropTypes.string,
    calendarStyle: PropTypes.any,
    calendarTheme: PropTypes.object,
    calendarProviderStyle: PropTypes.any,
    calendarProviderTheme: PropTypes.object,
    firstDay: PropTypes.number,
    markedDates: PropTypes.object,
    hideKnob: PropTypes.bool,
    disablePan: PropTypes.bool,
    horizontal: PropTypes.bool,
    hideArrows: PropTypes.bool,
    showTodayButton: PropTypes.bool,
    disabledOpacity: PropTypes.number,
    onDateChanged: PropTypes.func,
    onMonthChanged: PropTypes.func,
    children: PropTypes.object,
    themeColor: PropTypes.string,
    expanded: PropTypes.bool,
    allowShadow: PropTypes.bool,
    shadowStyle: PropTypes.object,
    leftArrowImageSource: PropTypes.any,
    rightArrowImageSource: PropTypes.any,
    disabledByDefault: PropTypes.bool,
    disableRightArrow: PropTypes.bool,
    disableLeftArrow: PropTypes.bool,
    pastDateSelectable: PropTypes.bool,
    disableFutureMonthSelectable: PropTypes.bool,
    disablePastMonthSelectable: PropTypes.bool,
    disableDefaultSelectFirstDayOfWeek: PropTypes.bool,
  };

  static defaultProps = {
    locale: 'en',
    date: '',
    dateTitle: '',
    calendarProviderStyle: {
      marginTop: 0,
      paddingTop: 0,
      display: 'flex',
    },
    calendarStyle: {},
    firstDay: 1,
    markedDates: {},
    calendarTheme: {},
    hideKnob: false,
    disablePan: false,
    horizontal: true,
    hideArrows: false,
    showTodayButton: false,
    disabledOpacity: 0.7,
    onDateChanged: () => {},
    onMonthChanged: () => {},
    calendarProviderTheme: {},
    children: null,
    themeColor: Colors.goldenYellow,
    expanded: false,
    allowShadow: false,
    shadowStyle: {},
    leftArrowImageSource: undefined,
    rightArrowImageSource: undefined,
    disabledByDefault: false,
    disableRightArrow: false,
    disableLeftArrow: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.current && nextProps.current !== prevState.current) {
      return {
        current: nextProps.current,
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      current: d.formatDate(new Date(), 'YYYY-MM-dd'),
    };

    Object.keys(Locales).forEach((locale) => {
      LocaleConfig.locales[locale] = {
        ...(LocaleConfig.locales[locale] || {}),
        monthNames: [
          Locales[locale]().calendar_january,
          Locales[locale]().calendar_february,
          Locales[locale]().calendar_march,
          Locales[locale]().calendar_april,
          Locales[locale]().calendar_may,
          Locales[locale]().calendar_june,
          Locales[locale]().calendar_july,
          Locales[locale]().calendar_august,
          Locales[locale]().calendar_september,
          Locales[locale]().calendar_october,
          Locales[locale]().calendar_november,
          Locales[locale]().calendar_december,
        ],
        monthNamesShort: [
          Locales[locale]().calendar_jan,
          Locales[locale]().calendar_feb,
          Locales[locale]().calendar_mar,
          Locales[locale]().calendar_apr,
          Locales[locale]().calendar_may,
          Locales[locale]().calendar_jun,
          Locales[locale]().calendar_jul,
          Locales[locale]().calendar_aug,
          Locales[locale]().calendar_sep,
          Locales[locale]().calendar_oct,
          Locales[locale]().calendar_nov,
          Locales[locale]().calendar_dec,
        ],
        dayNames: [
          Locales[locale]().calendar_sunday,
          Locales[locale]().calendar_monday,
          Locales[locale]().calendar_tuesday,
          Locales[locale]().calendar_wednesday,
          Locales[locale]().calendar_thursday,
          Locales[locale]().calendar_friday,
          Locales[locale]().calendar_saturday,
        ],
        dayNamesShort: [
          Locales[locale]().calendar_sun,
          Locales[locale]().calendar_mon,
          Locales[locale]().calendar_tue,
          Locales[locale]().calendar_wed,
          Locales[locale]().calendar_thu,
          Locales[locale]().calendar_fri,
          Locales[locale]().calendar_sat,
        ],
        today: Locales[locale]().calendar_today,
      };
    });
    LocaleConfig.defaultLocale = props.locale;
  }

  getCalendarTheme = () => {
    const { calendarTheme, themeColor } = this.props;
    const lightThemeColor = '#e6efff';
    const disabledColor = '#a6acb1';
    const black = '#20303c';
    const white = '#ffffff';

    return {
      arrowColor: black,
      arrowStyle: { padding: 0 },
      monthTextColor: black,
      textMonthFontSize: Screen.scale(16),
      textMonthFontFamily: Global.TextProps.style.fontFamily,
      textMonthFontWeight: 'bold',
      textSectionTitleColor: black,
      textDayHeaderFontSize: Platform.isPad ? Screen.scale(10) : Screen.scale(12),
      textDayHeaderFontFamily: Global.TextProps.style.fontFamily,
      textDayHeaderFontWeight: 'normal',
      textDayHeaderFontWidth: '100%',
      todayBackgroundColor: lightThemeColor,
      todayTextColor: themeColor,
      dayTextColor: Colors.black,
      textDayFontSize: Platform.isPad ? Screen.scale(10) : Screen.verticalScale(18),
      textDayFontFamily: Global.TextProps.style.fontFamily,
      textDayFontWeight: '500',
      textDayStyle: {
        marginTop:
          Platform.OS === 'android' ? Screen.verticalScale(2) : Screen.verticalScale(4),
      },
      selectedDayBackgroundColor: themeColor,
      selectedDayTextColor: white,
      textDisabledColor: disabledColor,
      dotColor: themeColor,
      selectedDotColor: white,
      disabledDotColor: Colors.gray_12,
      dotStyle: { marginTop: Screen.scale(-3) },
      ...calendarTheme,
    };
  };

  getCalendarProviderTheme = () => {
    const { calendarProviderTheme } = this.props;
    return {
      todayButtonTextColor: Colors.primary,
      ...calendarProviderTheme,
    };
  };

  onMonthChanged = async (date) => {
    const { onMonthChanged } = this.props;
    if (onMonthChanged) {
      await onMonthChanged(date);
    }
    const { startDate } = d.getWeekRangeByDate(date);
    this.setState(
      {
        current: startDate,
        disableRightArrowInternal: true,
        disableLeftArrowInternal: true,
      },
      async () => {
        setTimeout(() => {
          this.setState({
            disableRightArrowInternal: false,
            disableLeftArrowInternal: false,
          });
        }, 1000);
      },
    );
  };

  onDateChanged = async (date) => {
    const { onDateChanged } = this.props;
    if (onDateChanged) {
      await onDateChanged(date);
    }
    this.setState(
      {
        current: date,
        disableRightArrowInternal: true,
        disableLeftArrowInternal: true,
      },
      async () => {
        setTimeout(() => {
          this.setState({
            disableRightArrowInternal: false,
            disableLeftArrowInternal: false,
          });
        }, 1000);
      },
    );
  };

  render() {
    const {
      date,
      dateTitle,
      calendarStyle,
      calendarProviderStyle,
      firstDay,
      markedDates,
      hideKnob,
      disablePan,
      horizontal,
      hideArrows,
      showTodayButton,
      disabledOpacity,
      onDateChanged,
      children,
      expanded,
      allowShadow,
      shadowStyle,
      leftArrowImageSource,
      rightArrowImageSource,
      disabledByDefault,
      disableRightArrow,
      disableLeftArrow,
      pastDateSelectable,
      disableFutureMonthSelectable,
      disablePastMonthSelectable,
      disableDefaultSelectFirstDayOfWeek,
      ...props
    } = this.props;
    const { current, disableRightArrowInternal, disableLeftArrowInternal } = this.state;
    const padStyle = {
      minHeight: Platform.isPad && expanded ? Screen.verticalScale(290) : undefined,
      height: Platform.select({
        ios: undefined,
        android: expanded ? Screen.verticalScale(340) : Screen.verticalScale(98),
      }),
    };
    return (
      <CalendarProvider
        date={current}
        onDateChanged={this.onDateChanged}
        onMonthChange={this.onMonthChanged}
        theme={this.getCalendarProviderTheme()}
        showTodayButton={showTodayButton}
        disabledOpacity={disabledOpacity}
        style={calendarProviderStyle}
      >
        <ExpandableCalendar
          {...props}
          pastScrollRange={100}
          futureScrollRange={100}
          current={current}
          disableLeftArrow={disableLeftArrow}
          disableRightArrow={disableRightArrow}
          disabledByDefault={disabledByDefault}
          disableDefaultSelectFirstDayOfWeek={disableDefaultSelectFirstDayOfWeek}
          titleText={dateTitle}
          horizontal={horizontal}
          hideArrows={hideArrows}
          disablePan={disablePan}
          hideKnob={hideKnob}
          initialPosition={
            expanded
              ? ExpandableCalendar.positions.OPEN
              : ExpandableCalendar.positions.CLOSED
          }
          scrollEnabled={false}
          firstDay={firstDay}
          markedDates={markedDates}
          theme={this.getCalendarTheme()}
          style={[styles.calendarStyle, calendarStyle, padStyle]}
          headerStyle={styles.calendar}
          allowShadow={false}
          leftArrowImageSource={leftArrowImageSource}
          rightArrowImageSource={rightArrowImageSource}
          HEADER_HEIGHT={
            expanded
              ? Platform.select({
                  ios: Platform.isPad ? Screen.verticalScale(128) : undefined,
                  android: Screen.verticalScale(128),
                })
              : Platform.select({
                  ios: Platform.isPad ? Screen.verticalScale(88) : undefined,
                  android: Screen.verticalScale(64),
                })
          }
          WEEK_HEIGHT={
            expanded
              ? Platform.select({
                  ios: Platform.isPad ? undefined : undefined,
                  android: undefined,
                })
              : Platform.select({
                  ios: Platform.isPad ? Screen.verticalScale(32) : undefined,
                  android: Screen.verticalScale(32),
                })
          }
        />
        {allowShadow && (
          <>
            <View
              style={[
                styles.shadowBar,
                shadowStyle,
                expanded && {
                  marginTop: Platform.select({
                    ios: Platform.isPad
                      ? Screen.verticalScale(-24)
                      : Screen.verticalScale(-12),
                    android: Screen.verticalScale(-12),
                  }),
                },
              ]}
            >
              <View style={styles.shadowBlocker} />
            </View>
          </>
        )}
        {children}
      </CalendarProvider>
    );
  }
}
