import React from 'react';
import PropTypes from 'prop-types';
import { isArray } from 'lodash';
import { translate as t } from 'App/Helpers/I18n';
import { View, Text, Platform } from 'react-native';

import { BaseButton, BaseAvatar, HeadLine } from 'App/Components';
import { Colors, Fonts, Images, Metrics } from 'App/Theme';
import { StyleSheet } from 'App/Helpers';

const styles = StyleSheet.create({
  flexBox: {
    flexDirection: 'row',
    marginTop: -Metrics.baseMargin,
    marginBottom: Metrics.baseMargin,
    flexWrap: 'wrap',
  },
  instructorBox: {
    width: Platform.isPad ? '98@s' : '83@s',
    height: '73@vs',
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: Metrics.baseMargin,
    marginTop: '10@vs',
    borderWidth: 0,
  },
  avatar: {
    width: '49@s',
    height: '49@s',
    borderRadius: '24.5@s',
    marginBottom: Metrics.baseMargin / 2,
  },
  instructorText: {
    fontSize: Fonts.size.small,
    width: '83@s',
    textAlign: 'center',
    color: Colors.titleText.primary,
  },
});

const InstructorPanel = ({ data, expend, onExpandStateChange, onPressInstructor }) => {
  return (
    <View style={styles.box}>
      <HeadLine
        paddingHorizontal={16}
        title={t('class_detail_instructor')}
        onViewAll={data.length > 8 && onExpandStateChange}
      />
      <View style={styles.flexBox}>
        {isArray(data) &&
          data.map((item, index) => {
            return (
              (index < 8 || expend) && (
                <BaseButton
                  onPress={() => onPressInstructor(item.id)}
                  key={index}
                  style={styles.instructorBox}
                  throttleTime={0}
                  transparent
                >
                  <BaseAvatar
                    active={item.selected}
                    size={49}
                    uri={item.signedAvatarImage || ''}
                    style={styles.avatar}
                  />
                  <Text style={styles.instructorText}>{item.title}</Text>
                </BaseButton>
              )
            );
          })}
      </View>
    </View>
  );
};

InstructorPanel.propTypes = {
  data: PropTypes.array.isRequired,
  expend: PropTypes.bool.isRequired,
  onPressInstructor: PropTypes.func.isRequired,
  onExpandStateChange: PropTypes.func.isRequired,
};

export default InstructorPanel;
