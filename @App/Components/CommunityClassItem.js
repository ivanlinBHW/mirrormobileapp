import React from 'react';
import { PropTypes } from 'prop-types';
import { View, Text, Platform } from 'react-native';

import { Metrics, Classes, Images, Fonts, Colors } from 'App/Theme';
import { StyleSheet, isIphoneX } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { BaseButton, CachedImage as Image } from 'App/Components';

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: Metrics.baseMargin,
    paddingRight: Metrics.baseMargin,
    height: Platform.isPad ? '92@s' : '78@s',
  },
  imgStyle: {
    width: '62@s',
    height: '62@s',
    borderRadius: '31@s',
  },
  middleBox: {
    flex: 1,
    marginLeft: '37@s',
    justifyContent: 'center',
  },
  title: {
    ...Fonts.style.medium500,
    marginTop: '9@vs',
  },
  instructor: {
    ...Fonts.style.small,
    color: Colors.gray_02,
  },
  text: {
    fontSize: Fonts.size.small,
    minWidth: '70@s',
  },
  btnImage: {
    width: '30@s',
    height: '30@s',
  },
  btnAction: {
    width: '40@s',
    height: '40@s',
    justifyContent: 'center',
  },
});

class CommunityClassItem extends React.PureComponent {
  static propTypes = {
    deletable: PropTypes.bool,
    duration: PropTypes.number,
    level: PropTypes.any,
    title: PropTypes.string,
    instructor: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    backgroundColor: PropTypes.string,
    coverImage: PropTypes.string,
    onPress: PropTypes.func,
    isShowRemove: PropTypes.bool,
    onPressDetail: PropTypes.func,
  };

  static defaultProps = {
    deletable: false,
    duration: 0,
    level: 1,
    title: '',
    avatarUrl: '',
    instructor: '',
    backgroundColor: Colors.transparent,
    onPress: () => {},
    onPressDetail: () => {},
    isShowRemove: true,
  };

  render() {
    const {
      level,
      title,
      deletable,
      duration,
      instructor,
      backgroundColor,
      onPress,
      avatarUrl,
      isShowRemove,
      disabled,
      onPressDetail,
      signedCoverImage,
      signedCoverImageObj,
      ...props
    } = this.props;
    const instructorTitle = typeof instructor === 'object' ? instructor.title : '';
    console.log('props=>', JSON.stringify(props, null, 2));
    return (
      <BaseButton
        transparent
        style={[styles.wrapper, { backgroundColor }]}
        onPress={onPressDetail}
        throttleTime={0}
      >
        <View style={styles.imgStyle}>
          <Image
            source={signedCoverImageObj || signedCoverImage || Images.defaultAvatar}
            style={styles.imgStyle}
          />
        </View>
        <View style={styles.middleBox}>
          <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
            {title}
          </Text>
          <Text style={styles.instructor}>{instructorTitle}</Text>

          <View style={[Classes.fillRow]}>
            <Text style={styles.text}>{t(`search_lv${level}`)}</Text>
            <Text style={styles.text}>{`${duration / 60} ${t('min')}`}</Text>
          </View>
        </View>
        <BaseButton
          style={styles.btnAction}
          onPress={onPress}
          throttleTime={0}
          disabled={disabled}
          transparent
        >
          {deletable ? (
            isShowRemove && (
              <Image source={Images.event_delete_primary} style={styles.btnImage} />
            )
          ) : (
            <Image source={Images.event_add} style={styles.btnImage} />
          )}
        </BaseButton>
      </BaseButton>
    );
  }
}

export default CommunityClassItem;
