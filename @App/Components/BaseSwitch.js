import React from 'react';
import { PropTypes } from 'prop-types';
import { Switch, Platform } from 'react-native';

import { Colors } from 'App/Theme';
import { StyleSheet } from 'App/Helpers';

const styles = StyleSheet.create({
  switch: {
    ...Platform.select({
      ios: {
        transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }],
      },
    }),
  },
});

class BaseSwitch extends React.PureComponent {
  static propTypes = {
    active: PropTypes.bool,
    onValueChange: PropTypes.func,
    style: PropTypes.object,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    style: {},
    active: false,
    disabled: false,
    onValueChange: () => {},
  };

  static getDerivedStateFromProps(props, state) {
    if (props.active !== state.isActive) {
      return {
        isActive: props.active,
      };
    }
    return null;
  }

  constructor(props) {
    super(props);

    this.state = {
      isActive: props.active,
    };
  }
  onValueChange = () => {
    const { onValueChange } = this.props;
    onValueChange(!this.state.isActive);
  };

  render() {
    const { style, disabled } = this.props;
    const { isActive } = this.state;
    return (
      <Switch
        style={[styles.switch, style]}
        ios_backgroundColor={Colors.switchBox.primary.false}
        thumbColor={Colors.switchBox.primary.text}
        value={isActive}
        disabled={disabled}
        trackColor={{
          false: Colors.switchBox.primary.false,
          true: Colors.switchBox.primary.true,
        }}
        onValueChange={this.onValueChange}
      />
    );
  }
}

BaseSwitch.propTypes = {
  active: PropTypes.bool,
  onValueChange: PropTypes.func,
  style: PropTypes.object,
};

BaseSwitch.defaultProps = {
  active: false,
  onValueChange: () => {},
  style: {},
};

export default BaseSwitch;
