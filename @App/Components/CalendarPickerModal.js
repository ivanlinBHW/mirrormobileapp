import React from 'react';
import PropTypes from 'prop-types';
import CalendarPicker from 'react-native-calendar-picker';

import { BaseModal } from 'App/Components';
import { translate as t } from 'App/Helpers/I18n';
import { Colors, Fonts } from 'App/Theme';

class CalendarPickerModal extends React.PureComponent {
  static propTypes = {
    onDateChange: PropTypes.func.isRequired,
    onCancelPress: PropTypes.func.isRequired,
    startFromMonday: PropTypes.bool,
    allowRangeSelection: PropTypes.bool,
  };

  static defaultProps = {
    startFromMonday: true,
    allowRangeSelection: true,
  };

  render() {
    const {
      onCancelPress,
      onDateChange,
      startFromMonday,
      allowRangeSelection,
      ...props
    } = this.props;
    return (
      <BaseModal
        title={t('community_event_creation_input_choose_date')}
        handleClose={onCancelPress}
        height="55%"
      >
        <CalendarPicker
          {...props}
          onDateChange={onDateChange}
          startFromMonday={startFromMonday}
          allowRangeSelection={allowRangeSelection}
          todayBackgroundColor={Colors.primary}
          selectedDayColor={Colors.goldenYellow}
          previousTitle="＜"
          previousTitleStyle={Fonts.style.medium500}
          nextTitleStyle={Fonts.style.medium500}
          nextTitle="＞"
        />
      </BaseModal>
    );
  }
}

export default CalendarPickerModal;
