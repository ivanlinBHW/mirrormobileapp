import React from 'react';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import { Text, View, ActivityIndicator } from 'react-native';

import { ScaledSheet, Screen } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { Colors, Fonts } from 'App/Theme';

const styles = ScaledSheet.create({
  emptyBox: {
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  emptyTitle: {
    fontSize: Screen.scale(24),
    color: Colors.black,
    textAlign: 'center',
    ...Fonts.style.fontWeight500,
  },
  emptyContent: {
    ...Fonts.style.small500,
    width: '267@s',
    marginTop: '12@vs',
    color: Colors.gray_01,
    textAlign: 'center',
  },
  loadingIndicatorWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Screen.scale(266),
  },
  wrapperBox: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
});

export default class FlatListEmptyLoading extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    title: t('see_all_empty_title'),
    content: '',
    isLoading: false,
  };

  render() {
    const { title, content, isLoading } = this.props;
    return (
      <View style={styles.wrapperBox}>
        {isLoading ? (
          <View style={styles.loadingIndicatorWrapper}>
            <ActivityIndicator size="large" color={Colors.gray_02} />
          </View>
        ) : (
            <View style={styles.emptyBox}>
              <Text style={styles.emptyTitle}>{title}</Text>
              <Text style={styles.emptyContent}>{content}</Text>
            </View>
          )}
      </View>
    );
  }
}
