import React from 'react';
import PropTypes from 'prop-types';
import ReadMore from 'react-native-read-more-text';
import { Text, View } from 'react-native';

import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Colors, Fonts, Metrics } from 'App/Theme';

const styles = ScaledSheet.create({
  expandText: {
    fontSize: Fonts.size.medium500,
    textAlign: 'right',
    marginTop: Metrics.baseMargin / 2,
    paddingVertical: Metrics.baseMargin / 2,
  },
  dateTimeText: {
    textAlign: 'left',
    width: Screen.width / 2,
  },
  singleLineDateTime: {
    position: 'absolute',
    top: Metrics.baseVerticalMargin * 1.5,
  },
  textBox: {
    flex: 1,
    paddingHorizontal: Metrics.baseMargin,
    marginTop: Metrics.baseMargin / 2,
    justifyContent: 'center',
  },
});

export default class BaseReadMore extends React.PureComponent {
  static propTypes = {
    text: PropTypes.string.isRequired,
    showLine: PropTypes.number,
    moreText: PropTypes.any,
    lessText: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    expandTextColor: PropTypes.any,
    expandTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    dateTime: PropTypes.string,
    dateTimeTextColor: PropTypes.any,
    dateTimeTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  };

  static defaultProps = {
    showLine: 2,
    moreText: t('more'),
    lessText: t('less'),
    style: {},
    expandTextColor: Colors.button.primary.text.text,
    expandTextStyle: {},
    dateTimeTextColor: Colors.gray_02,
    dateTimeTextStyle: {},
  };

  state = {
    isOpened: false,
  };

  onPressTruncatedFooter = (handlePress) => {
    const { moreText = t('more'), expandTextColor, expandTextStyle } = this.props;
    return (
      <Text
        style={[
          styles.expandText,
          expandTextColor && { color: expandTextColor },
          expandTextStyle,
        ]}
        onPress={() => {
          this.setState(
            {
              isOpened: true,
            },
            handlePress,
          );
        }}
      >
        {moreText}
      </Text>
    );
  };

  onPressRevealedFooter = (handlePress) => {
    const { lessText = t('less'), expandTextColor, expandTextStyle } = this.props;
    return (
      <Text
        style={[
          styles.expandText,
          expandTextColor && { color: expandTextColor },
          expandTextStyle,
        ]}
        onPress={() => {
          this.setState(
            {
              isOpened: false,
            },
            handlePress,
          );
        }}
      >
        {lessText}
      </Text>
    );
  };

  render() {
    const {
      text,
      showLine,
      style,
      dateTimeTextColor,
      dateTimeTextStyle,
      dateTime,
    } = this.props;
    const { isOpened } = this.state;
    return (
      <View style={[styles.textBox, style]}>
        <ReadMore
          numberOfLines={showLine}
          renderTruncatedFooter={this.onPressTruncatedFooter}
          renderRevealedFooter={this.onPressRevealedFooter}
        >
          {text}
        </ReadMore>
        {!isOpened && (
          <Text
            pointerEvents="none"
            style={[
              styles.dateTimeText,
              showLine < 2 && styles.singleLineDateTime,
              dateTimeTextColor && { color: dateTimeTextColor },
              dateTimeTextStyle,
            ]}
          >
            {dateTime}
          </Text>
        )}
      </View>
    );
  }
}
