import React from 'react';
import PropTypes from 'prop-types';
import {
  SafeAreaView,
  FlatList,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

import { translate as t } from 'App/Helpers/I18n';
import { ScaledSheet, Screen } from 'App/Helpers';
import { Styles, Metrics, Fonts, Colors, Classes } from 'App/Theme';
import { ClassCard, SecondaryNavbar } from 'App/Components';

import { isArray } from 'lodash';
import { FilterBaseSearchButton } from 'App/Components';

import { Images } from 'App/Theme';
import { Actions } from 'react-native-router-flux';

const styles = ScaledSheet.create({
  navBar: {
    ...Styles.navBar,
  },
  wrapper: {
    flex: 1,
  },
  contentWrapper: {
    flex: 1,
  },
  box: {},
  listBox: {
    flex: 1,
  },
  emptyBox: {
    flex: 1,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Screen.scale(266),
  },
  emptyTitle: {
    fontSize: Screen.scale(24),
    color: Colors.black,
    textAlign: 'center',
    ...Fonts.style.fontWeight500,
  },
  emptyContent: {
    ...Fonts.style.small500,
    marginTop: '12@vs',
    color: Colors.gray_01,
    textAlign: 'center',
  },
  itemBox: {
    marginBottom: Metrics.baseMargin,
  },
  flexBox: {
    flexDirection: 'row',
    marginBottom: Metrics.baseMargin,
  },
  scrollBox: {
    flex: 1,
    marginLeft: Metrics.baseMargin,
  },
  btnStyle: {
    backgroundColor: 'transparent',
  },
  saveText: {
    fontSize: Fonts.size.small500,
    color: Colors.primary,
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.baseMargin,
  },
});

export default class SeeAllView extends React.PureComponent {
  static propTypes = {
    list: PropTypes.array.isRequired,
    timezone: PropTypes.string.isRequired,

    onPressClassDetail: PropTypes.func.isRequired,
    onPressBookmark: PropTypes.func.isRequired,
    onPressEndReach: PropTypes.func.isRequired,
    setSearchData: PropTypes.func.isRequired,
    onPressClear: PropTypes.func.isRequired,

    loginRoute: PropTypes.string.isRequired,
    searchData: PropTypes.object,
    listLoading: PropTypes.bool.isRequired,

    equipmentList: PropTypes.array,
    instructorList: PropTypes.array,

    appRoute: PropTypes.object.isRequired,
    apiError: PropTypes.object,

    showFilter: PropTypes.bool,
  };

  static defaultProps = {
    apiError: null,
    showFilter: false,
  };

  state = {
    searchData: {
      channel: [],
      duration: [],
      equipments: [],
      genre: [],
      instructors: [],
      isBookmarked: false,
      isCompleted: false,
      isMotionCapture: false,
      level: [],
      tags: [],
      text: '',
    },
    filterList: [
      {
        name: t('search_popular_tags'),
        searchDataName: 'tags',
        tagId: 1,
      },
      {
        name: t('search_instructor'),
        tagId: 3,
        searchDataName: 'instructors',
      },
      {
        name: t('search_equipment'),
        tagId: 4,
        searchDataName: 'equipments',
      },
      {
        name: t('search_difficulty'),
        tagId: 5,
        searchDataName: 'level',
      },
      {
        name: t('search_duration'),
        tagId: 6,
        searchDataName: 'duration',
      },
      {
        name: t('search_filter'),
        tagId: 2,
        searchDataName: [
          { name: 'isCompleted', title: 'search_completed' },
          { name: 'isBookmarked', title: 'search_bookmarked' },
          { name: 'isPopular', title: 'search_popular_class' },
          { name: 'isNewArrival', title: 'search_new_arrival' },
        ],
      },
    ],
    canClear: false,
  };

  endReached = false;

  componentDidUpdate() {
    if (
      this.props.appRoute.routeName === 'SeeAllScreen' &&
      JSON.stringify(this.props.searchData) !== '{}'
    ) {
      this.setState({
        searchData: this.props.searchData,
      });
    }
  }

  renderItem = ({ item, index }) => {
    const { timezone, onPressClassDetail, onPressBookmark, loginRoute } = this.props;
    return (
      <View style={styles.itemBox}>
        <ClassCard
          size="full"
          data={item}
          index={index}
          timezone={timezone}
          onPressBookmark={() => onPressBookmark(item)}
          onPress={onPressClassDetail}
        />
      </View>
    );
  };

  renderEmpty = () => {
    const { apiError, listLoading } = this.props;

    if (!apiError) {
      return listLoading ? (
        <View style={styles.emptyBox}>
          <Text style={styles.emptyTitle}>{t('see_all_loading_title')}</Text>
        </View>
      ) : (
        <View style={styles.emptyBox}>
          <Text style={styles.emptyTitle}>{t('see_all_empty_title')}</Text>
          <Text style={styles.emptyContent}>{t('see_all_empty_content')}</Text>
        </View>
      );
    }
    if (apiError && apiError.message && apiError.message.includes('timeout')) {
      return (
        <View style={styles.emptyBox}>
          <Text style={styles.emptyTitle}>{t('api_error_timeout_title')}</Text>
          <Text style={styles.emptyContent}>{t('api_error_timeout_content')}</Text>
        </View>
      );
    }
    return (
      <View style={styles.emptyBox}>
        <Text style={styles.emptyTitle}>{apiError.message}</Text>
      </View>
    );
  };

  renderFilterTags = () => {
    const { equipmentList, instructorList, listLoading, apiError } = this.props;
    const { searchData } = this.state;

    let canClearTemp = false;
    let canClearTempHasSet = false;

    const FilterList = this.state.filterList.map((item, index) => {
      let text = '';
      let count = 0;

      if (isArray(item.searchDataName)) {
        let textHasSet = false;

        item.searchDataName.forEach((filter) => {
          console.log(filter, searchData[filter.name]);

          if (searchData[filter.name]) {
            count += 1;
          }

          if (!textHasSet && count === 1) {
            text = t(filter.title);
            textHasSet = true;
          } else if (count !== 1) {
            text = item.name;
          }
        });
      } else {
        const data = searchData[item.searchDataName];

        count = data.length;

        if (count === 1) {
          switch (item.tagId) {
            case 1:
              text = data[0];
              break;
            case 3:
              text = instructorList.find((instructor) => {
                return instructor.id === data[0];
              }).title;
              break;
            case 4:
              if (data[0] === 'no-equipment') {
                text = data[0];
              } else {
                text = equipmentList.find((equipment) => {
                  return equipment.id === data[0];
                }).title;
              }

              break;
            case 5:
              text = t(`search_lv${data[0]}`);
              break;
            case 6:
              text = t(`search_duration_${String(data[0][1] / 60)}`);
              break;
          }
        } else {
          text = item.name;
        }
      }

      if (count > 0 && !canClearTempHasSet) {
        canClearTemp = canClearTempHasSet = true;
      }

      return (
        <FilterBaseSearchButton
          key={item.tagId}
          size="normal"
          text={text}
          isSelected={count ? true : false}
          onPress={() => this.onPressTemporaryTag(item.tagId)}
          image={count ? Images.sort_white : Images.sort_black}
          countOfSelected={count}
          disabled={listLoading || apiError}
        />
      );
    });

    this.setState({
      canClear: canClearTemp,
    });

    return FilterList;
  };

  onPressTemporaryTag = (tagId) => {
    switch (tagId) {
      case 1:
        Actions.FilterPopularTagsModal({});
        return;
      case 2:
        Actions.FilterFilterTagsModal();
        return;
      case 3:
        Actions.FilterInstructorTagsModal();
        return;
      case 4:
        Actions.FilterEquipmentTagsModal();
        return;
      case 5:
        Actions.FilterDifficultyTagsModal();
        return;
      case 6:
        Actions.FilterDurationTagsModal();
        return;
      default:
        return;
    }
  };

  scrollToInitialPosition = () => {
    this.scrollViewRef.scrollTo({ x: 0 });
  };

  onLoadData = (showLoadingIndicator = false) => (event) => {
    console.log('event=>', event);
    console.log('this.endReached=>', this.endReached);
    if (!this.endReached) {
      return;
    }
    const { onPressEndReach, curPageProps, pageSizeProps, totalProps } = this.props;
    onPressEndReach(showLoadingIndicator);
    this.endReached = false;
  };

  render() {
    const {
      list,
      onPressEndReach,
      title = '',
      searchData,
      loginRoute,
      onPressClear,
      showFilter,
    } = this.props;
    const { filterList, canClear } = this.state;

    return (
      <SafeAreaView style={styles.wrapper}>
        <SecondaryNavbar style={styles.navBar} navTitle={title} back />
        <View style={styles.contentWrapper}>
          {showFilter && loginRoute == 'seeAll' && (
            <View style={styles.box}>
              <View style={styles.flexBox}>
                <ScrollView
                  style={styles.scrollBox}
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  contentContainerStyle={Classes.center}
                  ref={(ref) => {
                    this.scrollViewRef = ref;
                  }}
                  automaticallyAdjustContentInsets={false}
                >
                  {isArray(filterList) && this.renderFilterTags()}
                  {canClear && (
                    <TouchableOpacity style={styles.btnStyle}>
                      <Text
                        style={styles.saveText}
                        onPress={() => {
                          onPressClear();
                          this.scrollToInitialPosition();
                        }}
                      >
                        {t('search_clear_all')}
                      </Text>
                    </TouchableOpacity>
                  )}
                </ScrollView>
              </View>
            </View>
          )}
          <View style={styles.listBox}>
            <FlatList
              data={list}
              renderItem={this.renderItem}
              ListEmptyComponent={this.renderEmpty()}
              onEndReachedThreshold={0.4}
              onEndReached={() => (this.endReached = true)}
              onMomentumScrollEnd={this.onLoadData(true)}
              keyExtractor={(item) => item.id}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
