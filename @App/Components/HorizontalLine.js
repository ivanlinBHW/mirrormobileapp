import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import { ScaledSheet } from 'App/Helpers';
import { Colors, Metrics } from 'App/Theme';

const styles = ScaledSheet.create({
  hrLine: {
    borderBottomColor: Colors.black_15,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin,
  },
});

const HorizontalLine = ({ style }) => {
  return <View style={[styles.hrLine, style]} />;
};

HorizontalLine.propTypes = {
  style: PropTypes.object,
};

HorizontalLine.defaultProps = {
  style: {},
};

export default HorizontalLine;
