import { Animated, Platform } from 'react-native';
import { Colors, Metrics } from 'App/Theme';
import { Screen, getStatusBarHeight, ifIphoneX } from 'App/Helpers';

import Emitter from 'tiny-emitter/instance';
import PropTypes from 'prop-types';
import React from 'react';

const AutoHideViewHoc = (
  Component,
  top = 0,
  headerMaxHeight = Metrics.homeNavBarHeight,
) =>
  class AutoHideWrapper extends React.PureComponent {
    HEADER_MIN_HEIGHT = 0;

    HEADER_MAX_HEIGHT = headerMaxHeight || Metrics.homeNavBarHeight;

    HEADER_SCROLL_DISTANCE = this.HEADER_MAX_HEIGHT - this.HEADER_MIN_HEIGHT;

    state = {
      scrollY: new Animated.Value(
        Platform.OS === 'ios' ? -this.HEADER_MAX_HEIGHT : 0,
      ),
      unmounted: false,
    };

    componentDidMount() {
      Emitter.on('onListScroll', (event) => {
        if (
          typeof event === 'object' &&
          typeof event.y === 'number'
        ) {
          this.setState({
            scrollY: Platform.OS === 'ios' ? -event.y : event.y,
            unmounted: false,
          });
        }
      });
      Emitter.on('onListReset', () => {
        this.setState({ scrollY: 0 });
      });
    }

    componentWillUnmount() {
      this.setState({ unmounted: true });
      Emitter.off('onListScroll');
      Emitter.off('onListReset');
    }

    render() {
      const scrollY = Animated.add(
        this.state.scrollY,
        Platform.OS === 'ios' ? this.HEADER_MAX_HEIGHT : 0,
      );
      const headerTranslate = scrollY.interpolate({
        ...Platform.select({
          ios: {
            inputRange: [-this.HEADER_SCROLL_DISTANCE, 0],
            outputRange: [0, this.HEADER_SCROLL_DISTANCE],
          },
          android: {
            inputRange: [0, this.HEADER_SCROLL_DISTANCE],
            outputRange: [0, -this.HEADER_SCROLL_DISTANCE],
          },
        }),
        extrapolate: 'clamp',
      });
      const containerStyle = {
        position: 'absolute',
        left: 0,
        right: 0,
        backgroundColor: Colors.white,
        flex: 1,
        transform: [{ scale: 1 }, { translateY: headerTranslate }],
        zIndex: 90000000000,
        ...ifIphoneX(
          {
            top: -this.HEADER_MAX_HEIGHT - getStatusBarHeight(false) + 16 + top,
          },
          {
            top: Platform.OS === 'ios' ? -this.HEADER_MAX_HEIGHT + top : top,
          },
        ),
      };
      return (
        <Animated.View style={containerStyle}>
          <Component {...this.props} />
        </Animated.View>
      );
    }
  };

export default AutoHideViewHoc;
