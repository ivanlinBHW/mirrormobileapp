import React from 'react';
import PropTypes from 'prop-types';
import { Animated, Platform, Text, View } from 'react-native';

import { Colors, Metrics } from 'App/Theme';
import { StyleSheet } from 'App/Helpers';

const HEADER_MAX_HEIGHT = Metrics.homeNavBarHeight + 144;
const HEADER_MIN_HEIGHT = 144;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const styles = StyleSheet.create({
  fill: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    overflow: 'hidden',
    height: HEADER_MAX_HEIGHT,
  },
  bar: {
    backgroundColor: Colors.transparent,
    marginTop: '88@vs',
    height: '64@vs',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 0,
  },
  scrollViewContent: {
    paddingTop: Platform.OS === 'ios' ? 0 : HEADER_MAX_HEIGHT,
    flex: 1,
  },
  row: {
    height: '40@vs',
    margin: '16@s',
    backgroundColor: Colors.very_light_pink,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default class HeaderScrollView extends React.PureComponent {
  static propTypes = {
    headerTitle: PropTypes.string,
    headerComponent: PropTypes.object,
    fixedComponent: PropTypes.object,
    children: PropTypes.any,
    onRefresh: PropTypes.func,
  };

  static defaultProps = {
    headerTitle: 'headerTitle',
    headerComponent: null,
    fixedComponent: null,
    children: null,
    onRefresh: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(
        Platform.OS === 'ios' ? -HEADER_MAX_HEIGHT : 0,
      ),
      refreshing: false,
    };
  }

  _renderScrollViewContent() {
    const data = Array.from({ length: 30 });
    return data.map((_, i) => (
      <View key={i} style={styles.row}>
        <Text>{i}</Text>
      </View>
    ));
  }

  render() {
    const {
      headerTitle,
      headerComponent,
      fixedComponent,
      children,
      onRefresh,
    } = this.props;
    const scrollY = Animated.add(
      this.state.scrollY,
      Platform.OS === 'ios' ? HEADER_MAX_HEIGHT : 0,
    );
    const imageOpacity = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });
    const titleScale = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0.8],
      extrapolate: 'clamp',
    });
    const headerTranslate = scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -HEADER_SCROLL_DISTANCE],
      extrapolate: 'clamp',
    });
    const titleTranslate = scrollY.interpolate({
      ...Platform.select({
        ios: {
          inputRange: [0, Metrics.homeNavBarHeight],
          outputRange: [0, -HEADER_SCROLL_DISTANCE],
        },
        android: {
          inputRange: [0, HEADER_SCROLL_DISTANCE],
          outputRange: [0, -Metrics.homeNavBarHeight],
        },
      }),
      extrapolate: 'clamp',
    });

    return (
      <View style={styles.fill}>
        <Animated.ScrollView
          style={styles.fill}
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { useNativeDriver: true },
          )}
          contentInset={{
            top: HEADER_MAX_HEIGHT,
          }}
          contentOffset={{
            y: -HEADER_MAX_HEIGHT,
          }}
        >
          <View style={styles.scrollViewContent}>
            {/*this._renderScrollViewContent()*/}
            {children}
          </View>
        </Animated.ScrollView>

        <Animated.View
          style={[
            styles.header,
            {
              transform: [{ translateY: headerTranslate }],
            },
          ]}
        >
          {/*<Animated.Image
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }],
              },
            ]}
            source={require('App/Assets/Images/TOM.png')}
          />*/}
          {headerComponent}
        </Animated.View>
        <Animated.View
          style={[
            {
              transform: [{ scale: 1 }, { translateY: titleTranslate }],
            },
            styles.bar,
          ]}
        >
          {/*<Text style={styles.title}>Title</Text>*/}
          {fixedComponent}
        </Animated.View>
      </View>
    );
  }
}
