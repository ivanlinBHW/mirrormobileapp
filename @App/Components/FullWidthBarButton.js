import React from 'react';
import { Text, Image } from 'react-native';
import { PropTypes } from 'prop-types';

import { ScaledSheet } from 'App/Helpers';
import { Images, Colors, Fonts, Metrics } from 'App/Theme';
import { BaseButton } from 'App/Components';

const styles = ScaledSheet.create({
  container: {
    height: '48@vs',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: Metrics.baseMargin,
    paddingRight: Metrics.baseMargin,

    paddingBottom: 0,
    marginBottom: 0,
    alignItems: 'center',
  },
  text: {
    fontSize: Fonts.size.medium,
    color: Colors.button.primary.content.text,
  },
  img: {
    width: '30@vs',
    height: '30@vs',
  },
});

export default class FullWidthBarButton extends React.PureComponent {
  static propTypes = {
    text: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    color: PropTypes.string,
    textColor: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    round: PropTypes.bool,
    outline: PropTypes.bool,
    width: PropTypes.number,
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    disabled: PropTypes.bool,
    icon: PropTypes.string,
    iconColor: PropTypes.string,
    radius: PropTypes.number,
    borderOutline: PropTypes.bool,
    throttleTime: PropTypes.number,
    source: PropTypes.any,
    backgroundColor: PropTypes.string,
  };

  static defaultProps = {
    style: {},
    color: null,
    round: false,
    outline: false,
    disabled: false,
    borderOutline: false,
    onPress: () => {},
    width: 0,
    textStyle: {},
    icon: null,
    iconColor: Colors.icon.primary,
    throttleTime: 250,
    source: Images.event_add_white,
    backgroundColor: Colors.button.primary.content.background,
  };

  render() {
    const {
      onPress,
      width,
      text,
      textColor,
      textStyle,
      style,
      color,
      disabled,
      round,
      outline,
      icon,
      iconColor,
      borderOutline,
      radius,
      throttleTime,
      source,
      backgroundColor,
      ...props
    } = this.props;
    return (
      <BaseButton
        {...props}
        style={[styles.container, { backgroundColor }]}
        onPress={onPress}
        disabled={disabled}
        throttleTime={0}
        transparent
      >
        <Text style={styles.text}>{text}</Text>
        <Image source={source} style={styles.img} />
      </BaseButton>
    );
  }
}
