import React from 'react';
import { PropTypes } from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

import { translate as t } from 'App/Helpers/I18n';
import { Metrics, Images, Fonts, Colors } from 'App/Theme';
import { ScaledSheet, Date as d, Screen } from 'App/Helpers';
import { BaseButton, CachedImage as Image } from 'App/Components';

const styles = ScaledSheet.create({
  wrapper: {
    flexDirection: 'row',
    paddingVertical: Metrics.baseMargin / 2,
    minHeight: '80@s',
    height: 'auto',
  },
  imgWrapper: {
    width: '60@sr',
    height: '60@sr',
    borderRadius: '30@sr',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: Colors.black,
    overflow: 'hidden',
    marginLeft: Metrics.baseMargin,
  },
  imgStyle: {
    position: 'absolute',
    width: '70@s',
    height: '70@s',
    top: '-5@s',
    left: '-5@s',
  },
  middleBox: {
    flex: 1,
    marginLeft: '37@s',
    flexDirection: 'column',
  },
  title: {
    ...Fonts.style.medium500,
  },
  eventType: {
    ...Fonts.style.medium500,
    color: Colors.gray_02,
  },
  memberImg: {
    width: '20@s',
    height: '20@s',
  },
  rightBottomBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: Metrics.baseMargin / 2,
  },
  text: {
    fontSize: Fonts.size.small,
    minWidth: '21@s',
  },
  userCountMargin: {
    marginLeft: Metrics.baseMargin / 4,
  },
  classesMargin: {
    marginLeft: '25@s',
    minWidth: '64@s',
  },
  dateMargin: {
    marginLeft: Metrics.baseMargin,
  },
  addImg: {
    width: '30@s',
    height: '30@s',
  },
  addBtn: {
    marginRight: Metrics.baseMargin,
    zIndex: 12211,
  },
});

class EventList extends React.Component {
  static propTypes = {
    active: PropTypes.bool,
    onValueChange: PropTypes.func,
    style: PropTypes.object,
    disabled: PropTypes.bool,
    backgroundColor: PropTypes.any,
    onPressAdd: PropTypes.func,
    timezone: PropTypes.string,
    onPressDetail: PropTypes.func,
  };

  static defaultProps = {
    active: false,
    onValueChange: () => {},
    style: {},
    disabled: false,
    backgroundColor: Colors.white_02,
    onPressAdd: () => {},
    onPressDetail: () => {},
  };

  state = {
    isActive: false,
  };

  render() {
    const { data, backgroundColor, onPressAdd, timezone, onPressDetail } = this.props;
    return (
      <BaseButton
        transparent
        style={[styles.wrapper, { backgroundColor }]}
        onPress={() => onPressDetail(data.id)}
      >
        <View style={styles.imgWrapper}>
          <Image
            source={data.signedCoverImageObj || { uri: data.signedCoverImage }}
            style={styles.imgStyle}
            resizeMode="cover"
            imageSize="2x"
            imageType="h"
          />
        </View>
        <View style={styles.middleBox}>
          <View>
            <Text style={styles.title}>{data.title}</Text>
            <Text style={styles.eventType}>
              {data.eventType === 0 && t('community_event_title')}
              {data.eventType === 1 && t('community_event_type_1on1')}
            </Text>
          </View>
          <View style={styles.rightBottomBox}>
            <Image source={Images.members} style={styles.memberImg} />
            <Text style={[styles.text, styles.userCountMargin]}>
              {data.participateCount}
            </Text>
            <Text style={[styles.text, styles.classesMargin]}>
              {data.classesCount > 1
                ? `${data.classesCount} ${t('community_event_classes')}`
                : `${data.classesCount} ${t('community_event_class')}`}
            </Text>
            <Text style={[styles.text, styles.dateMargin]}>
              {`${d.formatDate(
                d.transformDate(data.startDate, timezone),
                'MM/DD',
              )}-${d.formatDate(d.transformDate(data.dueDate, timezone), 'MM/DD')}`}
            </Text>
          </View>
        </View>
        {d.isSameOrAfterDate(d.transformDate(data.dueDate, timezone), new Date()) &&
          data.eventType === 0 &&
          data.actionType === 'join' && (
            <BaseButton
              width={Screen.scale(40)}
              height={Screen.scale(40)}
              transparent
              style={styles.addBtn}
              onPress={onPressAdd}
            >
              <Image source={Images.event_add} style={styles.addImg} />
            </BaseButton>
          )}
      </BaseButton>
    );
  }
}

EventList.propTypes = {
  data: PropTypes.object,
};

EventList.defaultProps = {
  data: {
    title: 'Event Title',
    usersCount: 100,
    trainingClassesCount: 10,
  },
};

export default EventList;
