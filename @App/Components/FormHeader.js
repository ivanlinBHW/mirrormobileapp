import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

import { ScaledSheet } from 'App/Helpers';
import { Metrics, Colors, Fonts } from 'App/Theme';

const styles = ScaledSheet.create({
  headerTitle: {
    ...Fonts.style.medium500,
    paddingVertical: Metrics.baseMargin,
    paddingLeft: Metrics.baseMargin,
    backgroundColor: Colors.white_02,
  },
});

export default class FormHeader extends React.PureComponent {
  static propTypes = {
    text: PropTypes.string.isRequired,
  };

  render() {
    const { text } = this.props;
    return <Text style={styles.headerTitle}>{text}</Text>;
  }
}
