import React from 'react';
import PropTypes from 'prop-types';
import StarRating from 'react-native-star-rating';

import { StyleSheet, Screen } from 'App/Helpers';
import { Colors } from 'App/Theme';

const styles = StyleSheet.create({});

const getStyle = (width) => [
  {
    width: typeof width === 'number' ? Screen.scale(width) : 'auto',
  },
];

const RatingSelect = ({ rating, starSize, maxStars, style }) => (
  <StarRating
    emptyStarColor={Colors.fontIcon.primary}
    containerStyle={[styles.starBox]}
    fullStarColor={Colors.fontIcon.primary}
    rating={rating}
    starSize={Screen.scale(starSize)}
    maxStars={maxStars}
    starStyle={[getStyle(starSize), style]}
    disabled
  />
);

RatingSelect.propTypes = {
  rating: PropTypes.number.isRequired,
  starSize: PropTypes.number,
  maxStars: PropTypes.number,
  style: PropTypes.object,
};

RatingSelect.defaultProps = {
  starSize: 25,
  maxStars: 5,
  style: {},
};

export default RatingSelect;
