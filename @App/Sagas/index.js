import { ActionConst } from 'react-native-router-flux';
import { takeLatest, all, takeEvery } from 'redux-saga/effects';
import { UserTypes } from 'App/Stores/User/Actions';
import { WebsocketTypes } from 'App/Stores/Websocket/Actions';

import { ExampleTypes } from 'App/Stores/Example/Actions';
import { StartupTypes } from 'App/Stores/Startup/Actions';
import { InstructorTypes } from 'App/Stores/Instructor/Actions';
import { fetchUser } from './ExampleSaga';
import { startup } from './StartupSaga';

import { initialize, messageReceived, websocketDisconnect } from './WebsocketSaga';
import {
  getProgramList,
  getProgramDetail,
  getProgramClassDetail,
  finishProgramClass,
  startProgram,
  leaveProgram,
  startProgramClass,
  onlyGetProgramDetail,
  resumeProgramClass,
  pauseProgramClass,
  onlyGetProgramClassDetail,
  onlyClassSummaryGetProgramClassDetail,
  stashProgramStepId,
  setProgramBookmark,
  removeProgramBookmark,
  programSubmitFeedback,
} from './ProgramSaga';
import {
  getCollectionList,
  getCollectionDetail,
  setCollectionBookmark,
  removeCollectionBookmark,
} from './CollectionSaga';
import MdnsSaga from './MdnsSaga';
import BleDeviceSaga from './BleDevice/BleDeviceSaga';
import MirrorSetupSaga from './Mirror/MirrorSetupSaga';
import * as MirrorCompareSaga from './Mirror/MirrorCompareSaga';
import * as MirrorConnectSuccessSaga from './Mirror/MirrorConnectSuccessSaga';
import * as UserSaga from './UserSaga';
import * as LiveSaga from './LiveSaga';
import * as ClassSaga from './ClassSaga';
import * as SeeAllSaga from './SeeAllSaga';
import * as CommunitySaga from './CommunitySaga';
import * as ChannelSaga from './ChannelSaga';
import * as SettingSaga from './SettingSaga';
import * as AppRouteSaga from './AppRouteSaga';
import * as AppStateSaga from './AppStateSaga';
import * as ProgressSaga from './ProgressSaga';
import * as NotificationSaga from './NotificationSaga';
import * as MirrorInitSaga from './Mirror/MirrorInitSaga';
import { getInstructorDetail, setInstructorId } from './InstructorSaga';
import * as SearchSaga from './SearchSaga';
import * as DeviceVersionInfoSaga from './DeviceVersionInfoSaga';
import * as AppBulletinSaga from './AppBulletinSaga';
import * as TrackRecordSaga from './TrackRecordSaga';

import { MdnsTypes } from 'App/Stores/Mdns/Actions';
import { LiveTypes } from 'App/Stores/Live/Actions';
import { ClassTypes } from 'App/Stores/Class/Actions';
import { MirrorTypes } from 'App/Stores/Mirror/Actions';
import { ProgramTypes } from 'App/Stores/Program/Actions';
import { ProgressTypes } from 'App/Stores/Progress/Actions';
import { BleDeviceTypes } from 'App/Stores/BleDevice/Actions';
import { CollectionTypes } from 'App/Stores/Collection/Actions';
import { SettingTypes } from 'App/Stores/Setting/Actions';
import { SpotifyTypes } from 'App/Stores/Spotify/Actions';
import { SearchTypes } from 'App/Stores/Search/Actions';
import { NotificationTypes } from 'App/Stores/Notification/Actions';
import { AppStateTypes } from 'App/Stores/AppState/Actions';
import { SeeAllTypes } from 'App/Stores/SeeAll/Actions';
import { CommunityTypes } from 'App/Stores/Community/Actions';
import { ChannelTypes } from 'App/Stores/Channel/Actions';
import { DeviceVersionInfoTypes } from 'App/Stores/DeviceVersionInfo/Actions';
import { ActionTypes as AppBulletinTypes } from 'App/Stores/AppBulletin/Actions';
import { ActionTypes as TrackRecordTypes } from 'App/Stores/TrackRecord/Actions';

export default function* root() {
  yield all([
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(ExampleTypes.FETCH_USER, fetchUser),
    takeLatest(ActionConst.FOCUS, AppRouteSaga.pushActionWatcher),
    takeLatest(AppStateTypes.ON_LOADING, AppStateSaga.handleAutomaticExpiration),
    takeLatest(AppStateTypes.ON_APP_INIT, AppStateSaga.onAppInit),
    takeLatest(AppStateTypes.DO_ENV_CHANGE, AppStateSaga.doEnvChange),
    takeLatest(
      UserTypes.FETCH_DEL_MEMBER_SUBSCRIPTION_CODE,
      UserSaga.fetchDelMemberSubscriptionCode,
    ),
    takeLatest(
      UserTypes.FETCH_ADD_MEMBER_SUBSCRIPTION_CODE,
      UserSaga.fetchAddMemberSubscriptionCode,
    ),
    takeLatest(UserTypes.FETCH_PUT_SUBSCRIPTION_CODE, UserSaga.fetchPutSubscriptionCode),
    takeLatest(UserTypes.SEND_FORGOT_PASSWORD_EMAIL, UserSaga.sendForgotPasswordEmail),

    takeLatest(UserTypes.RESET_PASSWORD, UserSaga.resetPassword),

    takeLatest(UserTypes.ON_USER_REFRESH_TOKEN, UserSaga.handleRefreshDeviceToken),
    takeLatest(UserTypes.FETCH_USER_LOGIN, UserSaga.fetchUserLogin),
    takeLatest(
      UserTypes.UPDATE_NOTIFICATION_PERMISSION,
      UserSaga.updateNotificationPermission,
    ),
    takeLatest(UserTypes.FETCH_USER_REGISTER, UserSaga.fetchUserRegister),
    takeLatest(UserTypes.FETCH_USER_TEMP_REGISTER, UserSaga.fetchUserTempRegister),
    takeLatest(UserTypes.EMAIL_VERIFY_TIMER_SAGA, UserSaga.emailVerifyTimerSaga),
    takeLatest(UserTypes.EMAIL_VERIFY_SAGA, UserSaga.emailVerifySaga),
    takeLatest(UserTypes.SEND_EMAIL_VERIFY, UserSaga.sendEmailVerify),
    takeLatest(UserTypes.ON_USER_LOGOUT, UserSaga.handleUserLogout),
    takeLatest(UserTypes.GET_USER_ACCOUNT, UserSaga.getUserAccount),
    takeLatest(UserTypes.FETCH_CHECK_SERVER_REGION, UserSaga.fetchCheckServerRegion),
    takeLatest(
      UserTypes.FETCH_PUT_NOTIFICATION_ENABLE,
      UserSaga.fetchPutNotificationEnable,
    ),
    takeLatest(
      UserTypes.FETCH_DEL_MEMBER_SUBSCRIPTION,
      UserSaga.fetchDelMemberSubscription,
    ),
    takeLatest(SpotifyTypes.ON_SPOTIFY_LOGIN, UserSaga.handleSpotifyTokenChange),
    takeLatest(SpotifyTypes.ON_SPOTIFY_LOGOUT, UserSaga.handleSpotifyTokenClean),
    takeLatest(UserTypes.DELETE_ACCOUNT_SAGA, UserSaga.deleteAccountSaga),
    takeLatest(BleDeviceTypes.ON_INITIAL, BleDeviceSaga.bleManager),
    takeLatest(MdnsTypes.ON_MDNS_SCAN, MdnsSaga),
    takeLatest(WebsocketTypes.ON_WS_CONNECT, initialize),
    takeLatest(WebsocketTypes.CLOSE_CONNECTION, websocketDisconnect),
    takeEvery(WebsocketTypes.ON_WS_MESSAGE_RECEIVED, messageReceived),
    takeLatest(MirrorTypes.ON_MIRROR_SETUP, MirrorInitSaga.initializer),
    takeLatest(MirrorTypes.ON_MIRROR_LOGOUT, MirrorSetupSaga.resetHandler),
    takeLatest(MirrorTypes.ON_MIRROR_CONNECT, MirrorSetupSaga.connectAndMonitoring),
    takeLatest(MirrorTypes.SET_CAROUSEL_DURATION, MirrorSetupSaga.setCarouselDuration),
    takeLatest(ClassTypes.GET_CLASS_DETAIL, ClassSaga.getClassDetail),
    takeLatest(ClassTypes.ONLY_GET_CLASS_DETAIL, ClassSaga.onlyGetClassDetail),
    takeLatest(
      ClassTypes.EXIT_DISTANCE_MEASURING_MODAL,
      ClassSaga.exitDistanceMeasuringModal,
    ),
    takeLatest(
      ClassTypes.ONLY_CLASS_SUMMARY_GET_CLASS_DETAIL,
      ClassSaga.onlyClassSummaryGetClassDetail,
    ),
    takeLatest(ClassTypes.GET_CLASS_LIST, ClassSaga.getClassList),
    takeLatest(ClassTypes.SUBMIT_FEEDBACK, ClassSaga.submitFeedback),
    takeLatest(ClassTypes.START_CLASS, ClassSaga.startClass),
    takeLatest(ClassTypes.FINISH_CLASS, ClassSaga.finishClass),
    takeLatest(ClassTypes.PAUSE_CLASS, ClassSaga.pauseClass),
    takeLatest(ClassTypes.RESUME_CLASS, ClassSaga.resumeClass),
    takeLatest(ClassTypes.STASH_CLASS_STEP_ID, ClassSaga.stashStepId),
    takeLatest(ClassTypes.SET_CLASS_BOOKMARK, ClassSaga.setClassBookmark),
    takeLatest(ClassTypes.REMOVE_CLASS_BOOKMARK, ClassSaga.removeClassBookmark),
    takeLatest(ClassTypes.SET_CLASS_LIVE_BOOKMARK, ClassSaga.setClassLiveBookmark),
    takeLatest(ClassTypes.REMOVE_CLASS_LIVE_BOOKMARK, ClassSaga.removeClassLiveBookmark),
    takeLatest(ClassTypes.IS_DO_LAST_EVENT, ClassSaga.isDoLastEvent),
    takeLatest(
      ClassTypes.ONLY_GET_CLASS_HISTORY_DETAIL,
      ClassSaga.onlyGetClassHistoryDetail,
    ),
    takeLatest(SeeAllTypes.GET_SEE_ALL_LIST, SeeAllSaga.getSeeAllList),
    takeLatest(SeeAllTypes.GET_TRAINING_CLASS_LIST, SeeAllSaga.getTrainingClassList),
    takeLatest(CommunityTypes.GET_LIST, CommunitySaga.getList),
    takeLatest(CommunityTypes.GET_ALL_MY_EVENT, CommunitySaga.getAllMyEvent),
    takeLatest(CommunityTypes.GET_ALL_EVENTS, CommunitySaga.getAllEvents),
    takeLatest(CommunityTypes.GET_SEARCH_FRIENDS, CommunitySaga.getSearchFriends),
    takeLatest(CommunityTypes.GET_COVER_IMAGES, CommunitySaga.getCoverImages),
    takeLatest(CommunityTypes.GET_USER_DETAIL, CommunitySaga.getUserDetail),
    takeLatest(CommunityTypes.ONLY_GET_USER_DETAIL, CommunitySaga.onlyGetUserDetail),
    takeLatest(
      CommunityTypes.NOTIFICATION_GET_USER_DETAIL,
      CommunitySaga.notificationGetUserDetail,
    ),
    takeLatest(CommunityTypes.GET_SEARCH_USER, CommunitySaga.getSearchUser),
    takeLatest(CommunityTypes.ADD_FRIEND, CommunitySaga.addFriend),
    takeLatest(CommunityTypes.DELETE_FRIEND, CommunitySaga.deleteFriend),
    takeLatest(CommunityTypes.DELETE_UN_FRIEND, CommunitySaga.deleteUnFriend),
    takeLatest(CommunityTypes.CONFIRM_FRIEND, CommunitySaga.confirmFriend),
    takeLatest(CommunityTypes.CREATE_NEW_EVENT, CommunitySaga.postEventCreation),
    takeLatest(CommunityTypes.GET_SEARCH_EVENTS, CommunitySaga.getSearchEvents),
    takeLatest(CommunityTypes.GET_USER_EVENT, CommunitySaga.getUserEvent),
    takeLatest(CommunityTypes.GET_USER_ACHIEVEMENT, CommunitySaga.getUserAchievement),
    takeLatest(CommunityTypes.GET_TRAINING_EVENT, CommunitySaga.getTrainingEvent),
    takeLatest(
      CommunityTypes.NOTIFICATION_GET_TRAINING_EVENT,
      CommunitySaga.notificationGetTrainingEvent,
    ),
    takeLatest(CommunityTypes.ACCEPT_TRAINING_EVENT, CommunitySaga.acceptTrainingEvent),
    takeLatest(CommunityTypes.REJECT_TRAINING_EVENT, CommunitySaga.rejectTrainingEvent),
    takeLatest(CommunityTypes.JOIN_EVENT, CommunitySaga.joinEvent),
    takeLatest(CommunityTypes.LEAVE_EVENT, CommunitySaga.leaveEvent),
    takeLatest(
      CommunityTypes.ONLY_GET_TRAINING_EVENT,
      CommunitySaga.onlyGetTrainingEvent,
    ),
    takeLatest(
      CommunityTypes.SET_COMMUNITY_CLASS_BOOKMARK,
      CommunitySaga.setCommunityClassBookmark,
    ),
    takeLatest(
      CommunityTypes.REMOVE_COMMUNITY_CLASS_BOOKMARK,
      CommunitySaga.removeCommunityClassBookmark,
    ),
    takeLatest(CommunityTypes.SEARCH_EVENT_FRIEND, CommunitySaga.searchEventFriend),
    takeLatest(CommunityTypes.INVITE_FRIENDS, CommunitySaga.inviteFriends),
    takeLatest(CommunityTypes.START_EVENT_CLASS, CommunitySaga.startEventClass),
    takeLatest(CommunityTypes.FINISH_EVENT_CLASS, CommunitySaga.finishEventClass),
    takeLatest(CommunityTypes.PAUSE_EVENT_CLASS, CommunitySaga.pauseEventClass),
    takeLatest(CommunityTypes.RESUME_EVENT_CLASS, CommunitySaga.resumeEventClass),
    takeLatest(CommunityTypes.STASH_STEP_EVENT_CLASS, CommunitySaga.stashStepEventClass),
    takeLatest(
      CommunityTypes.GET_COMMUNITY_CLASS_DETAIL,
      CommunitySaga.getCommunityClassDetail,
    ),
    takeLatest(
      CommunityTypes.ONLY_GET_COMMUNITY_CLASS_DETAIL,
      CommunitySaga.onlyGetCommunityClassDetail,
    ),
    takeLatest(
      CommunityTypes.ONLY_GET_COMMUNITY_HISTORY_CLASS_DETAIL,
      CommunitySaga.onlyGetCommunityHistoryClassDetail,
    ),
    takeLatest(
      CommunityTypes.ONLY_COMMUNITY_SUMMARY_CLASS_DETAIL,
      CommunitySaga.onlyCommunitySummaryClassDetail,
    ),
    takeLatest(CommunityTypes.GET_EVENT_LEADER_BOARD, CommunitySaga.getEventLeaderBoard),
    takeLatest(CommunityTypes.DELETE_TRAINING_EVENT, CommunitySaga.deleteTrainingEvent),
    takeLatest(CommunityTypes.GET_EVENT_CLASS_DETAIL, CommunitySaga.getEventClassDetail),
    takeLatest(
      CommunityTypes.GET_SEARCH_EVENT_CLASS_DETAIL,
      CommunitySaga.getSearchEventClassDetail,
    ),
    takeLatest(
      CommunityTypes.FETCH_PUT1ON1_ONLINE_STATUS,
      CommunitySaga.fetchPut1on1OnlineStatus,
    ),
    takeLatest(
      CommunityTypes.FETCH_POST1ON1_NOTIFICATION,
      CommunitySaga.fetchPost1on1Notification,
    ),
    takeLatest(
      CommunityTypes.FETCH_GET1ON1_ONLINE_STATUS,
      CommunitySaga.fetchGet1on1OnlineStatus,
    ),
    takeLatest(CommunityTypes.SYNC1ON1_START_TIME, CommunitySaga.sync1on1StartTime),
    takeLatest(ChannelTypes.GET_CHANNEL_LIST, ChannelSaga.getChannelList),
    takeLatest(ChannelTypes.GET_CHANNEL_DETAIL, ChannelSaga.getChannelDetail),
    takeLatest(ChannelTypes.GET_CHANNEL_SEE_ALL, ChannelSaga.getChannelSeeAll),
    takeLatest(ChannelTypes.SET_CHANNEL_BOOKMARK, ChannelSaga.setChannelBookmark),
    takeLatest(ChannelTypes.DEL_CHANNEL_BOOKMARK, ChannelSaga.delChannelBookmark),
    takeLatest(ProgramTypes.START_PROGRAM, startProgram),
    takeLatest(ProgramTypes.LEAVE_PROGRAM, leaveProgram),
    takeLatest(ProgramTypes.GET_PROGRAM_LIST, getProgramList),
    takeLatest(ProgramTypes.GET_PROGRAM_DETAIL, getProgramDetail),
    takeLatest(ProgramTypes.START_PROGRAM_CLASS, startProgramClass),
    takeLatest(ProgramTypes.PAUSE_PROGRAM_CLASS, pauseProgramClass),
    takeLatest(ProgramTypes.FINISH_PROGRAM_CLASS, finishProgramClass),
    takeLatest(ProgramTypes.RESUME_PROGRAM_CLASS, resumeProgramClass),
    takeLatest(ProgramTypes.ONLY_GET_PROGRAM_DETAIL, onlyGetProgramDetail),
    takeLatest(ProgramTypes.GET_PROGRAM_CLASS_DETAIL, getProgramClassDetail),
    takeLatest(ProgramTypes.STASH_PROGRAM_STEP_ID, stashProgramStepId),
    takeLatest(ProgramTypes.ONLY_GET_PROGRAM_CLASS_DETAIL, onlyGetProgramClassDetail),
    takeLatest(
      ProgramTypes.ONLY_CLASS_SUMMARY_GET_PROGRAM_CLASS_DETAIL,
      onlyClassSummaryGetProgramClassDetail,
    ),
    takeLatest(ProgramTypes.SET_PROGRAM_BOOKMARK, setProgramBookmark),
    takeLatest(ProgramTypes.REMOVE_PROGRAM_BOOKMARK, removeProgramBookmark),
    takeLatest(ProgramTypes.PROGRAM_SUBMIT_FEEDBACK, programSubmitFeedback),
    takeLatest(CollectionTypes.GET_COLLECTION_LIST, getCollectionList),
    takeLatest(CollectionTypes.GET_COLLECTION_DETAIL, getCollectionDetail),
    takeLatest(CollectionTypes.SET_COLLECTION_BOOKMARK, setCollectionBookmark),
    takeLatest(CollectionTypes.REMOVE_COLLECTION_BOOKMARK, removeCollectionBookmark),
    takeLatest(LiveTypes.GET_LIVE_LIST, LiveSaga.getLiveList),
    takeLatest(LiveTypes.GET_LIVE_DETAIL, LiveSaga.getLiveDetail),
    takeLatest(LiveTypes.ONLY_GET_LIVE_DETAIL, LiveSaga.onlyGetLiveDetail),
    takeLatest(
      LiveTypes.ONLY_PLAYING_CLASS_GET_LIVE_DETAIL,
      LiveSaga.onlyPlayingClassGetLiveDetail,
    ),
    takeLatest(
      LiveTypes.ONLY_CLASS_SUMMARY_GET_LIVE_DETAIL,
      LiveSaga.onlyClassSummaryGetLiveDetail,
    ),
    takeLatest(LiveTypes.GET_LIVE_DATE, LiveSaga.getLiveDate),
    takeLatest(LiveTypes.GET_LIVE_DATE_DETAIL, LiveSaga.getLiveDateDetail),
    takeLatest(LiveTypes.BOOK_LIVE, LiveSaga.bookLive),
    takeLatest(LiveTypes.JOIN_LIVE, LiveSaga.joinLive),
    takeLatest(LiveTypes.ONLY_JOIN_LIVE, LiveSaga.onlyJoinLive),
    takeLatest(LiveTypes.SUBMIT_LIVE_FEEDBACK, LiveSaga.submitLiveFeedback),
    takeLatest(LiveTypes.FINISH_LIVE_CLASS, LiveSaga.finishLiveClass),
    takeLatest(LiveTypes.SET_LIVE_BOOKMARK, LiveSaga.setLiveBookmark),
    takeLatest(LiveTypes.REMOVE_LIVE_BOOKMARK, LiveSaga.removeLiveBookmark),
    takeLatest(LiveTypes.SET_COUNT_DOWN, LiveSaga.setCountDown),
    takeLatest(LiveTypes.EXIT_LIVE_CLASS, LiveSaga.exitLiveClass),
    takeLatest(LiveTypes.SEND_LIVE_REACTION, LiveSaga.sendLiveReaction),
    takeLatest(
      NotificationTypes.GET_NOTIFICATION_LIST,
      NotificationSaga.getNotificationList,
    ),
    takeLatest(
      NotificationTypes.GET_NOTIFICATION_STATUS,
      NotificationSaga.getNotificationStatus,
    ),
    takeLatest(SearchTypes.GET_INSTRUCTOR_LIST, SearchSaga.getInstructorList),
    takeLatest(SearchTypes.GET_EQUIPMENT_LIST, SearchSaga.getEquipmentList),
    takeLatest(SearchTypes.GET_WORKOUT_TYPE_LIST, SearchSaga.getWorkoutTypeList),
    takeLatest(SearchTypes.SEARCH, SearchSaga.search),
    takeLatest(SearchTypes.SEARCH_FILTER, SearchSaga.searchFilter),
    takeLatest(SearchTypes.GET_SEARCH_HOT_KEYWORD, SearchSaga.getSearchHotKeyword),
    takeLatest(SearchTypes.COMMUNITY_SEARCH, SearchSaga.communitySearch),
    takeLatest(SearchTypes.GET_SEARCHABLE_DATA, SearchSaga.getSearchableData),
    takeLatest(SettingTypes.GET_GENRE, SettingSaga.getGenre),
    takeLatest(SettingTypes.GET_SETTING, SettingSaga.getSetting),
    takeLatest(SettingTypes.UPDATE_SETTING, SettingSaga.updateSetting),
    takeLatest(SettingTypes.UPDATE_EQUIPMENT, SettingSaga.updateEquipment),
    takeLatest(SettingTypes.UPDATE_AVATAR, SettingSaga.updateAvatar),
    takeLatest(SettingTypes.ON_UPDATE_USER_TIMEZONE, SettingSaga.updateUserTimezone),
    takeLatest(SettingTypes.FETCH_GET_PRIVACY_POLICY, SettingSaga.fetchGetPrivacyPolicy),
    takeLatest(SettingTypes.FETCH_GET_USAGE_POLICY, SettingSaga.fetchGetUsagePolicy),
    takeLatest(ProgressTypes.GET_ACHIEVEMENTS, ProgressSaga.getAchievements),
    takeLatest(ProgressTypes.GET_PROGRESS_DATE, ProgressSaga.getProgressDate),
    takeLatest(ProgressTypes.GET_PROGRESS_DETAIL, ProgressSaga.getProgressDetail),
    takeLatest(ProgressTypes.GET_PROGRESS_OVERVIEW, ProgressSaga.getProgressOverview),
    takeLatest(ProgressTypes.DELETE_CLASS_HISTORY, ProgressSaga.deleteClassHistory),
    takeLatest(
      ProgressTypes.SET_WORKOUT_DETAIL_BOOKMARK,
      ProgressSaga.setWorkoutDetailBookmark,
    ),
    takeLatest(
      ProgressTypes.DELETE_WORKOUT_DETAIL_BOOKMARK,
      ProgressSaga.deleteWorkoutDetailBookmark,
    ),
    takeLatest(InstructorTypes.GET_INSTRUCTOR, getInstructorDetail),
    takeLatest(InstructorTypes.SET_INSTRUCTOR_ID, setInstructorId),
    takeLatest(MirrorTypes.MIRROR_COMPARE, MirrorCompareSaga.mirrorCompare),
    takeLatest(MirrorTypes.LAST_EVENT_THING, MirrorConnectSuccessSaga.lastEventThing),
    takeLatest(DeviceVersionInfoTypes.INDEX_SAGA, DeviceVersionInfoSaga.indexSaga),
    takeLatest(AppBulletinTypes.GET_BULLETIN_VERSION, AppBulletinSaga.getBulletinVersion),
    takeLatest(AppBulletinTypes.GET_BULLETIN_CONTENT, AppBulletinSaga.getBulletinContent),
    takeLatest(TrackRecordTypes.TRACK, TrackRecordSaga.track),
  ]);
}
