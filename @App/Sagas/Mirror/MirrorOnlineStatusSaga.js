import { put, call, delay, select } from 'redux-saga/effects';

import { MirrorActions } from 'App/Stores';
import { Handler, Setting } from 'App/Api';

export function* startAutoOfflineTimer() {
  for (let i = 0; i < 31; i++) {
    yield delay(1000);

    const token = yield select((state) => state.user.token);
    if (!token) {
      break;
    }

    const isPlayingClass = yield select((state) => state.mirror.isPlayingClass);
    const isConnected = yield select((state) => state.websocket.isConnected);
    const routeName = yield select((state) => state.appRoute.routeName);
    if (
      [
        'OfflineModal',
        'MirrorPairScreen',
        'MirrorPairScreen',
        'MirrorSetupScreen',
        'MirrorWifiInputScreen',
        'WifiBarCodeScanScreen',
        'MirrorSetupWifiInitScreen',
      ].includes(routeName)
    ) {
      yield put(
        MirrorActions.onMirrorLog(
          `Mirror: User is disconnected due to enter to special screens, so set to offline.`,
        ),
      );
      yield call(setOffline);
      break;
    }
    if (!isConnected) {
      yield put(
        MirrorActions.onMirrorLog(
          `Mirror: User is disconnected with device so set to offline.`,
        ),
      );
      yield call(setOffline);
      break;
    }
    if (isPlayingClass) {
      yield put(
        MirrorActions.onMirrorLog(
          `Mirror: User is still online due to start a new class.`,
        ),
      );
      break;
    }
    if (i === 30) {
      if (!isPlayingClass) {
        yield call(setOffline);
        yield put(
          MirrorActions.onMirrorLog(`Mirror: Set Mirror to offline due to user idle.`),
        );
      } else {
        yield put(
          MirrorActions.onMirrorLog(
            `Mirror: User is still online due to start a new class.`,
          ),
        );
      }
      break;
    }
  }
}

export function* setOnline() {
  try {
    const token = yield select((state) => state.user.token);

    yield call(Handler.put({ Authorization: token }), Setting.userOnline());
    yield put(MirrorActions.onMirrorLog(`Mirror: set User online`));
  } catch (err) {
    console.log(err);
  }
}

export function* setOffline() {
  try {
    const token = yield select((state) => state.user.token);

    yield call(Handler.put({ Authorization: token }), Setting.userOffline());
    yield put(MirrorActions.onMirrorLog(`Mirror: set User offline`));
  } catch (err) {
    console.log(err);
  }
}
