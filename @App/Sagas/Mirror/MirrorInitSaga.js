import { put, take, fork, cancel, actionChannel } from 'redux-saga/effects';
import MirrorAction, { MirrorTypes } from 'App/Stores/Mirror/Actions';

import { mirrorConnectionHandler } from './MirrorInitConnectionSaga';
import { mirrorDiscoverHandler } from './MirrorInitDiscoverySaga';

export function* initializer() {
  yield put(MirrorAction.onMirrorLog('mirrorManager starts'));
  const channel = yield actionChannel([
    MirrorTypes.ON_MIRROR_PRE_CONNECT,
    MirrorTypes.ON_MIRROR_DISCOVER,
    MirrorTypes.ON_MIRROR_CONNECT,
  ]);
  let discoverTask, connectTask;
  for (;;) {
    const action = yield take(channel);
    console.log('=== action.type ===', action.type);
    yield put(
      MirrorAction.onMirrorLog(`mirrorManager action=>${JSON.stringify(action)}`),
    );
    if (action.type === MirrorTypes.ON_MIRROR_DISCOVER) {
      if (discoverTask) {
        yield cancel(discoverTask);
      }
      discoverTask = yield fork(mirrorDiscoverHandler, action);
    }
    if (action.type === MirrorTypes.ON_MIRROR_PRE_CONNECT) {
      if (connectTask) {
        yield cancel(connectTask);
      }
      connectTask = yield fork(mirrorConnectionHandler, action);
    }
    if (action.type === MirrorTypes.ON_MIRROR_CONNECT) {
      yield cancel(discoverTask);
      yield cancel(connectTask);
      break;
    }
  }
}
