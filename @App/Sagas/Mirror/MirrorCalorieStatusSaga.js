import { put, call, delay, select } from 'redux-saga/effects';
import { isEmpty, last } from 'lodash';

import { MirrorActions } from 'App/Stores';
import { Handler, Class } from 'App/Api';
import { Calories } from 'App/Helpers';

export function* startMirrorCalorieMonitor() {
  try {
    const lastTrainingRecords = yield select((state) => state.mirror.trainingRecords);
    let count = 0;
    let duration = !isEmpty(lastTrainingRecords)
      ? last(lastTrainingRecords).recordDuration
      : 0;

    if (!lastTrainingRecords || lastTrainingRecords instanceof Array) {
      yield put(MirrorActions.updateMirrorStore({ trainingRecords: [] }));
    }
    if (duration === 0) {
      yield saveTrainingRecord({
        currentClassType: yield select((state) => state.mirror.currentClassType),
        duration,
        calories: 0,
      });
    }

    while (true) {
      const activeBleDevice = yield select((state) => state.bleDevice.activeDevice);
      const currentClassType = yield select((state) => state.mirror.currentClassType);
      if (count === 60) {
        yield put(
          MirrorActions.onMirrorLog(
            `Mirror: Waiting 1 min to calculate calorie data....`,
          ),
        );
        count = 0;

        if (activeBleDevice) {
          yield put(MirrorActions.onMirrorLog(`Mirror: Calorie data calculated.`));
        } else {
          yield put(
            MirrorActions.onMirrorLog(
              `Mirror: No heart rate device connected, so will not calculate calorie data.`,
            ),
          );
        }
      } else {
        yield delay(1000);
        count += 1;
        duration += 1;

        const isPlayingClass = yield select((state) => state.mirror.isPlayingClass);
        yield put(MirrorActions.onMirrorLog(`Mirror: duration ${duration}....`));
        if (!isPlayingClass) {
          yield put(
            MirrorActions.onMirrorLog(
              `Mirror: Playing class stopped, so calculate last record and send data to the server.`,
            ),
          );
          break;
        }
      }
    }
  } catch (error) {
    yield put(
      MirrorActions.onMirrorLog(`Mirror: CalorieMonitor error: ${error.message}`),
    );
  }
}
export function* saveTrainingRecord({ duration, calories, currentClassType }) {
  try {
    const bleDeviceData = yield select((state) => state.bleDevice.data);
    const playerDetail = yield select((state) => state.player.detail);
    const record = {
      recordDuration: duration,
      heartRate: bleDeviceData ? bleDeviceData.heartRate : 0,
      calories: bleDeviceData
        ? calories ||
          Calories.calculate({
            heartRate: bleDeviceData.heartRate,
          })
        : 0,
    };

    switch (currentClassType) {
      case 'class':
        record.trainingClassHistoryId = playerDetail.trainingClassHistory.id;
        break;
      case 'live':
        record.liveClassHistoryId = playerDetail.liveClassHistory.id;
        break;
      case 'program':
        record.trainingProgramClassHistoryId =
          playerDetail.trainingProgramClassHistory.id;
        break;
      case 'event':
        record.trainingEventClassHistoryId = playerDetail.trainingEventClassHistory.id;
        break;

      default:
        break;
    }
    yield put(MirrorActions.updateTrainingRecords(record));
  } catch (error) {
    yield put(
      MirrorActions.onMirrorLog(`Mirror: saveTrainingRecord error: ${error.message}`),
    );
  }
}

export function* uploadTrainingRecords() {
  try {
    while (true) {
      yield delay(500);
      console.log('waiting isPlayingClass...');
      const isPlayingClass = yield select((state) => state.mirror.isPlayingClass);
      if (!isPlayingClass) {
        console.log('waiting isPlayingClass success!!!!');
        yield delay(500);
        break;
      }
    }

    const token = yield select((state) => state.user.token);
    let trainingRecords = yield select((state) => state.mirror.trainingRecords);
    if (trainingRecords) {
      trainingRecords = trainingRecords.filter((e) => !e.id);
    }
    console.log('trainingRecords=>', trainingRecords);
    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
        data: { trainingRecords },
      }),
      Class.trainingRecord(),
    );
    if (res.success) {
      yield put(
        MirrorActions.onMirrorLog(
          `Mirror: uploadTrainingRecord success @ ${new Date().getTime()}`,
        ),
      );
      yield put(MirrorActions.updateMirrorStore({ trainingRecords: [] }));

      return true;
    }
    return false;
  } catch (err) {
    console.log(err);
  }
}
