import {
  put,
  take,
  call,
  race,
  fork,
  delay,
  select,
  cancel,
  actionChannel,
} from 'redux-saga/effects';
import { isString } from 'lodash';
import { eventChannel } from 'redux-saga';
import { Alert, Linking, AppState, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { NetworkInfo } from 'react-native-network-info';
import NetInfo from '@react-native-community/netinfo';
import AndroidWifi from 'react-native-android-wifi-with-ap-status';
import WifiManager from 'react-native-wifi-reborn';
import AndroidOpenSettings from 'react-native-android-open-settings';

import { WebsocketTypes as WsTypes } from 'App/Stores/Websocket/Actions';
import { MirrorTypes, MirrorEvents } from 'App/Stores/Mirror/Actions';
import { initialize } from 'App/Sagas/WebsocketSaga';
import { translate as t } from 'App/Helpers/I18n';
import {
  WebsocketActions as WsAction,
  AppStateActions as AppAction,
  MirrorActions,
  MdnsActions,
} from 'App/Stores';
import { isJSON } from 'App/Helpers';
import { Config } from 'App/Config';
import { Handler } from 'App/Api';
export function* mirrorConnectionHandler({
  device,
  options = { maxRetries: 2 },
  protocols,
  wifiOptions = { SSID: '' },
  isForce,
}) {
  yield put(WsAction.closeConnection());
  if (Platform.OS === 'android') {
    AndroidWifi.isEnabled((isEnabled) => {
      AndroidWifi.forceWifiUsage(true);
    });
  }
  for (;;) {
    const currentState = yield select((state) => state.appState.currentState);

    console.log('currentState=>', currentState);
    yield delay(1000);
    yield put(
      MirrorActions.onMirrorLog(`Waiting for App back to Foreground: "${currentState}"`),
    );
    if (isForce || currentState === 'active') {
      break;
    }
  }

  if (
    !device.host ||
    !device.port ||
    device.host.includes(':') ||
    device.host.startsWith(':')
  ) {
    yield put(AppAction.onLoading(false));
    return Alert.alert(
      t('alert_title_oops'),
      `${t(
        'alert_content_connect_mirror_failed',
      )} Wrong host address format, please reset Mirror.`,
    );
  }
  const wsTask = yield fork(initialize, {
    url: `${Config.WEBSOCKET_MODE_PREFIX || 'ws'}://${device.host}:${device.port}`,
    protocols,
    options,
  });
  const mirrorActionChannel = yield actionChannel([
    WsTypes.ON_WS_SEND_MESSAGE,
    WsTypes.ON_WS_MESSAGE_RECEIVED,
    MirrorTypes.ON_MIRROR_DISCONNECT,
    MirrorTypes.ON_MIRROR_COMMUNICATE,
  ]);
  const connectionChannel = yield actionChannel([
    WsTypes.ON_WS_OPENED,
    WsTypes.ON_WS_ERROR,
  ]);

  let timer = null;
  const timerChannel = yield call(() =>
    eventChannel((emit) => {
      timer = setTimeout(() => {
        setTimeout(() => {
          emit('timeout');
        }, 30 * 1000);
      }, 30 * 1000);
      return () => {
        clearTimeout(timer);
        timer = null;
      };
    }),
  );

  try {
    for (;;) {
      const connectionChange = yield take(connectionChannel);
      if (connectionChange) {
        yield put(
          MirrorActions.onMirrorLog(
            `Connection Change: "${JSON.stringify(connectionChange)}"`,
          ),
        );
        if (connectionChange.type === WsTypes.ON_WS_OPENED) {
          yield put(MirrorActions.onMirrorPreConnected(device));
          yield put(
            MirrorActions.onMirrorLog(
              `Websocket connected: "${JSON.stringify(connectionChange)}"`,
            ),
          );
          const wifiData = {
            event: MirrorEvents.SET_WIFI_PASS,
            data: wifiOptions,
          };
          yield put(WsAction.onWsSendMessage(wifiData));
          yield put(
            MirrorActions.onMirrorLog(
              `Websocket sending: event = "${wifiData.event}", data = "${JSON.stringify(
                wifiOptions,
              )}"`,
            ),
          );

          try {
            for (let i = 0; i < 10; i++) {
              yield put(MirrorActions.onMirrorLog(`Waiting for wifi-init-processing...`));
              const { mirrorResponse, responseTimeoutEvent } = yield race({
                mirrorResponse: take(mirrorActionChannel),
                responseTimeoutEvent: take(timerChannel),
              });
              if (responseTimeoutEvent) {
                const msg = `Mirror didn't send back the "wifi-init-processing" event. Mirror may be powered off, wifi disconnected or the mirror host app crashed.`;
                yield put(
                  MirrorActions.onMirrorLog(
                    `responseTimeoutEvent detected. means that ${msg}`,
                  ),
                );
                yield call(
                  [Alert, Alert.alert],
                  t('alert_title_oops'),
                  `Waiting 30secs for Mirror response but get nothing. ${msg}`,
                );
                yield put(AppAction.onLoading(false));

                return yield call(requestAnimationFrame, Actions.pop);
              }

              yield put(
                MirrorActions.onMirrorLog(
                  `Action detected: ${JSON.stringify(mirrorResponse)}`,
                ),
              );
              if (
                mirrorResponse.type === WsTypes.ON_WS_MESSAGE_RECEIVED &&
                typeof mirrorResponse.message === 'string' &&
                isJSON(mirrorResponse.message)
              ) {
                const { event: wsEvent = '', data: wsData = {} } = JSON.parse(
                  mirrorResponse.message,
                );
                yield put(
                  MirrorActions.onMirrorLog(
                    `Websocket message received: event = "${wsEvent}", data = "${JSON.stringify(
                      wsData,
                    )}"`,
                  ),
                );
                if (wsEvent === MirrorEvents.EXCEPTION) {
                  requestAnimationFrame(() => {
                    Alert.alert(
                      t('alert_title_oops'),
                      `event "${wsData.event}" exception. (${wsData.message})`,
                    );
                  });
                }
                if (wsEvent === MirrorEvents.SET_WIFI_PASS_SUCCESS) {
                  yield put(
                    MirrorActions.onMirrorSetWifiPassSuccess(
                      wsData ? wsData.SSID : wifiOptions.SSID,
                    ),
                  );
                  yield put(
                    MirrorActions.onMirrorLog(
                      `Event detected: "${MirrorEvents.SET_WIFI_PASS_SUCCESS}"`,
                    ),
                  );
                  yield delay(1000);

                  const wifiSettingRoute = yield select(
                    (state) => state.appState.wifiSettingRoute,
                  );
                  if (wifiSettingRoute === 'HotSpot') {
                    MirrorActions.onMirrorLog(`wifiSettingRoute: "${wifiSettingRoute}"`);
                    yield put(AppAction.onLoading(false));
                    break;
                  }

                  yield put(MirrorActions.onMirrorDisconnect());
                  yield put(WsAction.closeConnection());
                  yield put(
                    MirrorActions.onMirrorLog(
                      'Send action to disconnect the websocket connection.',
                    ),
                  );
                  if (isForce) {
                    yield put(
                      MirrorActions.onMirrorLog(
                        `forceConnect detected. will try to connect to the mirror in same wifi lan area...`,
                      ),
                    );
                    yield put(AppAction.onLoading(false));
                    return yield call(requestAnimationFrame, Actions.ClassScreen);
                  }

                  const connectedWifi = yield select(
                    (state) => state.mirror.connectedWifi,
                  );

                  let isWifiFound = false;
                  if (connectedWifi && connectedWifi.ssid) {
                    yield put(
                      MirrorActions.onMirrorLog(
                        `Try to connect target SSID "${
                          connectedWifi.ssid
                        }" by using password "${connectedWifi.password}"...`,
                      ),
                    );
                    if (Platform.OS === 'android') {
                      AndroidWifi.findAndConnect(
                        connectedWifi.ssid,
                        connectedWifi.password,
                        (found) => {
                          if (found) {
                            console.log('wifi is in range');
                          } else {
                            console.log('wifi is not in range');
                          }
                          isWifiFound = found;
                        },
                      );
                      yield delay(5 * 1000);
                    }
                  }

                  for (let j = 0; j <= 40; j++) {
                    yield delay(1000);
                    if (j === 40) {
                      yield call(requestAnimationFrame, Actions.pop);
                      yield put(AppAction.onLoading(false));
                      yield put(
                        MirrorActions.onMirrorLog(
                          `wifiTimeoutEvent detected. means that mirror may not disable it's AP mode, the phone still connected with Mirror.`,
                        ),
                      );
                      yield call(
                        [Alert, Alert.alert],
                        t('alert_title_oops'),
                        t('mirror_connect_switch_ap_timeout'),
                      );
                      return yield call(requestAnimationFrame, Actions.pop);
                    } else {
                      const ssid = yield call(NetworkInfo.getSSID);

                      isWifiFound = ssid === wifiOptions.SSID;
                      yield put(
                        MirrorActions.onMirrorLog(
                          `Wifi Status monitor: SSID "${ssid}", Target SSID "${
                            wifiOptions.SSID
                          }" is ${isWifiFound ? 'founded' : 'not founded'}.`,
                        ),
                      );
                      if (
                        !isWifiFound &&
                        (isString(ssid) && !ssid.toLowerCase().includes('mirror-'))
                      ) {
                        const msg = t('mirror_setup_wifi_connect_to_not_target_ssid', {
                          targetSsid: wifiOptions.SSID,
                          nowSsid: ssid,
                        });
                        yield put(MirrorActions.onMirrorLog(msg));
                        yield put(AppAction.onLoading(false));

                        return yield call(
                          [Alert, Alert.alert],
                          t('alert_title_oops'),
                          msg,
                          [
                            {
                              text: t('__no'),
                              onPress: () => requestAnimationFrame(Actions.ClassScreen),
                            },
                            {
                              text: t('__retry'),
                              onPress: () => requestAnimationFrame(Actions.pop),
                            },
                            {
                              text: t('mirror_setup_wifi_init_wifi_setting'),
                              onPress: () => {
                                requestAnimationFrame(Actions.ClassScreen);
                                if (Platform.OS === 'android') {
                                  AndroidOpenSettings.wifiSettings();
                                } else {
                                  Linking.openURL('App-Prefs:root=WIFI');
                                }
                              },
                            },
                          ],
                        );
                      }
                      else {
                        yield put(MdnsActions.onMdnsScanClear());
                        yield put(
                          MirrorActions.onMirrorLog(
                            `=== connect success to MiiSettingScreen ===`,
                          ),
                        );
                        yield put(
                          MirrorActions.onMirrorLog(`=== wait connect status change ===`),
                        );
                        let timer = 0;
                        for (;;) {
                          timer++;
                          yield delay(1000);
                          try {
                            const currentNetworkInfo = yield select(
                              (state) => state.appState.currentNetworkInfo,
                            );

                            const { details, isInternetReachable } = currentNetworkInfo;

                            yield put(
                              MirrorActions.onMirrorLog(
                                `=== isInternetReachable ${isInternetReachable} ${
                                  details.ssid
                                } ===`,
                              ),
                            );
                            const isInternetReachableAndConnectWifi =
                              details.ssid &&
                              !details.ssid.includes('mirror-') &&
                              isInternetReachable;

                            if (isInternetReachableAndConnectWifi || timer === 30) {
                              break;
                            }
                          } catch (error) {
                            MirrorActions.onMirrorLog(`=== ${error} ===`);
                          }
                        }

                        yield put(AppAction.onLoading(false));
                        const routeName = yield select(
                          (state) => state.appRoute.routeName,
                        );
                        const gotoMiiSettingScreen = () => {
                          Actions.pop();
                          setTimeout(() => {
                            Actions.pop();
                            setTimeout(() => {
                              Actions.pop();
                            }, 100);
                          }, 100);
                        };

                        return yield call(requestAnimationFrame, gotoMiiSettingScreen);
                      }
                    }
                  }
                }
              } else {
                yield put(
                  MirrorActions.onMirrorLog(`websocket wait for response (${i + 1})...`),
                );
                if (i >= 9) {
                  yield put(MirrorActions.onMirrorDisconnect());
                  yield put(WsAction.closeConnection());
                  yield put(AppAction.onLoading(false));
                  yield put(
                    MirrorActions.onMirrorLog(`Send wifi setting to mirror failed`),
                  );
                  yield call(
                    [Alert, Alert.alert],
                    t('alert_title_oops'),
                    t('mirror_connect_no_response'),
                  );
                  return yield call(requestAnimationFrame, Actions.pop);
                }
              }
            }
          } catch (e) {
            console.warn(e.message);
            yield put(MirrorActions.onMirrorLog(e.message));
            break;
          }
        } else if (
          connectionChange.type === WsTypes.ON_WS_ERROR &&
          connectionChange.error.includes('Failed to connect to')
        ) {
          yield put(
            MirrorActions.onMirrorLog(
              `=== Connect to mirror failed dur to error ${connectionChange.error}. ===`,
            ),
          );
          yield put(WsAction.closeConnection());
          yield put(AppAction.onLoading(false));
          yield call(
            [Alert, Alert.alert],
            t('alert_title_oops'),
            t('mirror_connect_create_connection_failed'),
          );
          return yield call(requestAnimationFrame, Actions.pop);
        } else {
          const retryCount = yield select((state) => state.websocket.retryCount);
          yield put(
            MirrorActions.onMirrorLog(
              `Websocket connect to "${device.host}" retryCount: ${retryCount}....`,
            ),
          );
          if (retryCount >= options.maxRetries) {
            MirrorActions.onMirrorLog(
              `=== retryCount >= options.maxRetries onMirrorDisconnect ===`,
            );
            yield put(WsAction.closeConnection());
            yield put(AppAction.onLoading(false));
            yield call(
              [Alert, Alert.alert],
              t('alert_title_oops'),
              `${t('alert_content_connect_mirror_failed')}${
                __DEV__ ? "(Mirror's websocket connection create failed.)" : ''
              }`,
            );
            return yield call(requestAnimationFrame, Actions.pop);
          }
          yield put(WsAction.closeConnection());
        }
      }
    }
  } catch (error) {
    console.warn(error);
  } finally {
    if (Platform.OS === 'android') {
      AndroidWifi.forceWifiUsage(false);
    }

    yield cancel(wsTask);
  }
}
