import { select, call } from 'redux-saga/effects';

import { Dialog } from 'App/Helpers';
import { Handler, Class, Live } from 'App/Api';

import { isEmpty, get } from 'lodash';
import { store } from 'App/App';
import { AppStore, AppStateActions } from 'App/Stores';
export function* mirrorCompare({ onPressNo = () => {} }) {
  try {
    console.log('mirrorCompare =======>');
    const connectSuccessResult = yield select(
      (state) => state.mirror.connectSuccessResult,
    );
    const { classType = 'on-demand', page, classId } = connectSuccessResult;

    const detail = yield select((state) => state.player.detail);
    const token = yield select((state) => state.user.token);
    let isShowLaseEvent = false;
    console.log(
      'mirrorCompare connectSuccessResult =>',
      connectSuccessResult,
      page,
      classType,
    );
    if (!isEmpty(connectSuccessResult)) {
      let {
        isGoBackPageAlerting,
        isAutoConnectAlerting,
      } = store.getState().appState.currentAlert;
      if (isGoBackPageAlerting == undefined) isGoBackPageAlerting = false;
      if (isAutoConnectAlerting == undefined) isAutoConnectAlerting = false;
      console.log('=== isGoBackPageAlerting ===', isGoBackPageAlerting);
      console.log('=== isAutoConnectAlerting ===', isAutoConnectAlerting);
      switch (page) {
        case 'playing-class':
        case 'class-summary':
          let detailName = null;
          if (classType === 'on-demand') {
            console.log('=== connectSuccessResult ===', connectSuccessResult);
            const { data: res } = yield call(
              Handler.get({ Authorization: token }),
              Class.getClassDetail({
                id: connectSuccessResult.classId,
                historyId: connectSuccessResult.trainingClass,
              }),
            );
            detailName = get(res, 'data.trainingClass.title', null);
            console.log('=== detailName ===', detailName);
          } else {
            const { data: res } = yield call(
              Handler.get({ Authorization: token }),
              Live.getLiveDetail({ id: connectSuccessResult.classId }),
            );
            detailName = get(res, 'data.liveClass.title', null);
          }

          if (!isEmpty(detailName)) {
            if (isAutoConnectAlerting == false && isGoBackPageAlerting == false) {
              let currentAlert = { isGoBackPageAlerting: true };
              console.log('=== currentAlert ===', currentAlert);
              AppStore.dispatch(AppStateActions.onAlertingChange(currentAlert));
              isGoBackPageAlerting = store.getState().appState.currentAlert
                .isGoBackPageAlerting;
              console.log('=== after isGoBackPageAlerting ===', isGoBackPageAlerting);
              const routeName = yield select((state) => state.appRoute.routeName);
              if (
                !detail ||
                classId !== detail.id ||
                (classId === detail.id && !routeName.includes('Player'))
              ) {
                yield call(Dialog.requestCompareEventAlert, {
                  name: detailName,
                  onPressNo,
                });
              }
            }
          }
          break;
        case 'class-preview':
          console.log('=== mirrorCompare class-preview ===');
          if (isAutoConnectAlerting == false && isGoBackPageAlerting == false) {
            console.log('=== mirrorCompare class-preview 1 ===');
            let currentAlert = { isGoBackPageAlerting: true };
            AppStore.dispatch(AppStateActions.onAlertingChange(currentAlert));
            console.log('=== mirrorCompare class-preview 2 ===');
            const routeName = yield select((state) => state.appRoute.routeName);
            if (
              !detail ||
              classId !== detail.id ||
              (classId === detail.id && !routeName.includes('ClassDetail'))
            ) {
              yield call(Dialog.requestCompareEventAlert, {
                name: detailName,
                onPressNo,
              });
            }
            console.log('=== mirrorCompare class-preview 3 ===');
          }
          break;
        case 'standby':
        default:
          yield call(onPressNo);
          break;
      }
    } else {
      yield call(onPressNo);
    }
  } catch (error) {
    console.log('=== mirrorCompare error ===', error, error.stack);
  }
}
