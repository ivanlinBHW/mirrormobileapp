import { put, call, delay, select, fork } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import {
  ClassActions,
  ProgramActions,
  LiveActions,
  CommunityActions,
  MirrorActions,
} from 'App/Stores';

import * as MirrorCalorieStatusSaga from './MirrorCalorieStatusSaga';

export function* lastEventThing() {
  const routeName = yield select((state) => state.appRoute.routeName);
  const connectSuccessResult = yield select((state) => state.mirror.connectSuccessResult);
  switch (connectSuccessResult.page) {
    case 'standby':
      console.log('========== standby ============');
      if (routeName !== 'MiiSettingScreen') {
        Actions.ClassScreen({ type: 'replace' });
      }
      break;
    case 'class-preview': {
      console.log('============ class preview ===========');
      if (connectSuccessResult.classType === 'on-demand') {
        if (
          _.has(connectSuccessResult, 'programClassHistoryId') &&
          !_.isEmpty(connectSuccessResult.programClassHistoryId)
        ) {
          console.log('=== class-preview program ===');
          yield put(
            ProgramActions.onlyGetProgramClassDetail(
              connectSuccessResult.classId,
              'ProgramClassDetail',
              connectSuccessResult.programClassHistoryId,
            ),
          );
          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.Program();
            Actions.ProgramDetailStack();
          });
        } else if (
          _.has(connectSuccessResult, 'eventClassHistoryId') &&
          !_.isEmpty(connectSuccessResult.eventClassHistoryId)
        ) {
          console.log('=== class-preview Event ===');
          yield put(
            CommunityActions.onlyGetCommunityClassDetail(
              connectSuccessResult.classId,
              'EventClassDetail',
            ),
          );

          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.CommunityDetailClassDetailScreen();
            Actions.CommunityDetailClassDetailScreen();
          });
        } else {
          console.log('=== class-preview Class ===');
          yield put(
            ClassActions.onlyGetClassDetail(connectSuccessResult.classId, 'ClassDetail'),
          );
          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.ClassDetail();
          });
        }
      } else {
        console.log('=== class-preview Live ===');
        yield put(LiveActions.onlyGetLiveDetail(connectSuccessResult.classId));
        yield call(requestAnimationFrame, () => {
          Actions.ClassScreen({ type: 'replace' });
          Actions.LiveDetail();
        });
      }
      break;
    }
    case 'playing-class':
      yield fork(MirrorCalorieStatusSaga.startMirrorCalorieMonitor);

      if (connectSuccessResult.classType === 'on-demand') {
        if (
          _.has(connectSuccessResult, 'programClassHistoryId') &&
          !_.isEmpty(connectSuccessResult.programClassHistoryId)
        ) {
          console.log('=== playing-class Program ===');
          yield put(
            ProgramActions.onlyGetProgramClassDetail(
              connectSuccessResult.classId,
              'ProgramClassDetail',
              connectSuccessResult.programClassHistoryId,
            ),
          );
          yield put(MirrorActions.setCurrentPosition(connectSuccessResult.duration));
          yield put(MirrorActions.setCurrentStepId(connectSuccessResult.stepId));
          yield put(
            MirrorActions.setCurrentPlayerState(connectSuccessResult.playerState),
          );
          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.ProgramDetailStack();
            Actions.ProgramClassDetailScreen();
            setTimeout(Actions.ProgramPlayerScreen, 3000);
          });
        } else if (
          _.has(connectSuccessResult, 'eventClassHistoryId') &&
          !_.isEmpty(connectSuccessResult.eventClassHistoryId)
        ) {
          yield put(
            CommunityActions.onlyGetCommunityHistoryClassDetail(
              connectSuccessResult.classId,
              connectSuccessResult.eventClassHistoryId,
              'EventClassDetail',
            ),
          );
          yield put(MirrorActions.setCurrentPosition(connectSuccessResult.duration));
          yield put(MirrorActions.setCurrentStepId(connectSuccessResult.stepId));

          const eventDetail = yield select((state) => state.community.eventDetail);

          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.CommunityScreen();
            Actions.CommunityDetailScreen();
            Actions.CommunityDetailClassDetailScreen();

            setTimeout(Actions.EventPlayerScreen, 3000);
          });
        } else {
          console.log('=== playing-class Class ===');
          yield put(
            ClassActions.onlyGetClassHistoryDetail(
              connectSuccessResult.classId,
              connectSuccessResult.trainingClassHistoryId,
            ),
          );
          yield put(MirrorActions.setCurrentPosition(connectSuccessResult.duration));
          yield put(MirrorActions.setCurrentStepId(connectSuccessResult.stepId));
          yield put(
            MirrorActions.setCurrentPlayerState(connectSuccessResult.playerState),
          );
          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.ClassDetail();
            setTimeout(Actions.PlayerScreen, 3000);
          });
        }
      } else {
        console.log('=== playing-class Live ===');
        yield put(
          LiveActions.onlyPlayingClassGetLiveDetail(connectSuccessResult.classId),
        );
        yield call(requestAnimationFrame, () => {
          Actions.ClassScreen({ type: 'replace' });
          Actions.LiveDetail();
          setTimeout(Actions.LivePlayerScreen, 3000);
        });
      }
      break;
    case 'class-summary': {
      if (connectSuccessResult.classType === 'on-demand') {
        if (
          _.has(connectSuccessResult, 'programClassHistoryId') &&
          !_.isEmpty(connectSuccessResult.programClassHistoryId)
        ) {
          yield put(
            ProgramActions.onlyClassSummaryGetProgramClassDetail(
              connectSuccessResult.classId,
              'ProgramClassDetail',
              connectSuccessResult.programClassHistoryId,
            ),
          );
          delay(200);
          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.Program();
            Actions.ProgramDetailStack();
            Actions.ProgramClassDetailScreen();
            Actions.ProgramSaveActivityScreen();
          });
        } else if (
          _.has(connectSuccessResult, 'eventClassHistoryId') &&
          !_.isEmpty(connectSuccessResult.eventClassHistoryId)
        ) {
          yield put(
            CommunityActions.onlyCommunitySummaryClassDetail(
              connectSuccessResult.classId,
              connectSuccessResult.eventClassHistoryId,
              'EventClassDetail',
            ),
          );
          delay(200);
          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.CommunityScreen();
            Actions.CommunityDetailScreen();
            Actions.CommunityDetailClassDetailScreen();
            Actions.SaveActivityScreen();
          });
        } else {
          yield put(
            ClassActions.onlyClassSummaryGetClassDetail(
              connectSuccessResult.classId,
              'ClassDetail',
              connectSuccessResult.trainingClassHistoryId,
            ),
          );
          delay(200);
          yield call(requestAnimationFrame, () => {
            Actions.ClassScreen({ type: 'replace' });
            Actions.ClassDetail();
            Actions.SaveActivityScreen();
          });
        }
      } else {
        yield put(
          LiveActions.onlyClassSummaryGetLiveDetail(
            connectSuccessResult.classId,
            connectSuccessResult.liveClassHistoryId,
          ),
        );
        yield delay(200);
        yield call(requestAnimationFrame, () => {
          Actions.ClassScreen({ type: 'replace' });
          Actions.LiveDetail();
          Actions.SaveActivityScreen();
        });
      }
      break;
    }
    default:
      console.log('========== default standby ============');
      if (routeName !== 'MiiSettingScreen') {
        Actions.ClassScreen({ type: 'replace' });
      }
      break;
  }
  yield put(MirrorActions.updateMirrorStore({ connectSuccessResult: {} }));
}
