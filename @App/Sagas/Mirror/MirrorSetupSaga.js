import { eventChannel } from 'redux-saga';
import {
  put,
  take,
  call,
  race,
  fork,
  delay,
  select,
  cancel,
  debounce,
  cancelled,
  actionChannel,
} from 'redux-saga/effects';
import _ from 'lodash';
import UUID from 'uuid-generate';
import AndroidWifi from 'react-native-android-wifi-with-ap-status';
import { Alert, Platform, ToastAndroid } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { translate as t } from 'App/Helpers/I18n';
import { has, isEmpty, get } from 'lodash';
import { store } from 'App/App';

import { Config } from 'App/Config';
import { getCircularReplacer, Permission, Dialog } from 'App/Helpers';
import WsAction, { WebsocketTypes as WsTypes } from 'App/Stores/Websocket/Actions';
import MirrorAction, {
  MirrorTypes,
  MirrorEvents,
  MirrorOptions,
} from 'App/Stores/Mirror/Actions';
import BleAction, { BleDeviceTypes, BleServices } from 'App/Stores/BleDevice/Actions';
import MdnsAction from 'App/Stores/Mdns/Actions';
import AppAction from 'App/Stores/AppState/Actions';
import UserAction from 'App/Stores/User/Actions';
import {
  AppStore,
  LiveActions,
  ClassActions,
  ProgramActions,
  CommunityActions,
  UserActions,
  MirrorActions,
  PlayerActions,
} from 'App/Stores';
import { initialize } from 'App/Sagas/WebsocketSaga';
import * as MirrorOnlineStatusSaga from './MirrorOnlineStatusSaga';
import * as MirrorCalorieStatusSaga from './MirrorCalorieStatusSaga';
import SystemSetting from 'react-native-system-setting';
import { Calories } from 'App/Helpers';
import { INITIAL_STATE as MirrorInitialState } from 'App/Stores/Mirror/InitialState';

let bleConnectTask = null;
let bleMonitorTask = null;
let websocketTask = null;
let monitorChannel = null;
let communicateTask = null;
let reconnectTask = null;
let isConnectSuccess = false;
let isWaitCastingSuccess = false;

function* markIsNotConnecting() {
  console.log('reConnect task canceled!');
  yield put(
    MirrorAction.updateMirrorStore({
      isConnecting: false,
    }),
  );
}

function* reConnect(device) {
  try {
    yield put(
      MirrorAction.updateMirrorStore({
        isConnecting: true,
      }),
    );
    for (var i = 0; i < 5; i++) {
      yield delay(5000);
      console.log('isConnectSuccess ->', isConnectSuccess);
      if (!isConnectSuccess) {
        console.log('======= onMirrorDisconnect =======');
        yield put(MirrorAction.onMirrorDisconnect());
        console.log('------- onMirrorDisconnect -------');

        const isLoading = yield select((state) => state.appState.isLoading);
        const token = yield select((state) => state.user.isLoading);
        if (!isLoading || !token) {
          break;
        }

        yield put(MirrorAction.onMirrorLog(`Re-init the connection`));
        yield delay(1000);
        yield put(MirrorAction.onMirrorConnect(device));
      } else {
        yield call(markIsNotConnecting);
        break;
      }
    }
    yield call(markIsNotConnecting);
  } finally {
    if (yield cancelled()) {
      yield call(markIsNotConnecting);
    }
  }
}

const showRetryOrCancelDialog = (lastMirrorDevice) => {
  Dialog.showRetryOrCancelAlert(
    lastMirrorDevice,
    () => {
      store.dispatch(MirrorAction.onMirrorDisconnect());
      store.dispatch(WsAction.closeConnection());
      requestAnimationFrame(() => {
        requestAnimationFrame(Actions.ClassScreen);
        requestAnimationFrame(Actions.DeviceModal);
      });
    },
    () => {
      store.dispatch(WsAction.closeConnection());
      setTimeout(() => {
        store.dispatch(MirrorAction.onMirrorConnect(lastMirrorDevice));
      }, 1000);
    },
  );
};

const forceDisconnect = function*() {
  if (monitorChannel) {
    monitorChannel.close();
  }
  if (websocketTask) {
    yield cancel(websocketTask);
  }
  if (communicateTask) {
    yield cancel(communicateTask);
  }
  yield put(MirrorAction.onMirrorDisconnect());
  yield put(BleAction.onDisconnect());
  isConnectSuccess = false;

  if (reconnectTask) {
    yield cancel(reconnectTask);
  }
};

export function* connectAndMonitoring({
  device,
  options = { maxRetries: 2 },
  nextScreen: onConnectedCallback,
}) {
  options.perMessageDeflate = {
    zlibDeflateOptions: {
      chunkSize: 1024,
      memLevel: 7,
      level: 3,
    },
    zlibInflateOptions: {
      chunkSize: 10 * 1024,
    },
    clientNoContextTakeover: true,
    serverNoContextTakeover: true,
    serverMaxWindowBits: 10,
    concurrencyLimit: 10,
    threshold: 1024,
  };

  console.log('=== connectAndMonitoring execute ===');

  if (
    !device.host ||
    !device.port ||
    device.host.includes(':') ||
    device.host.startsWith(':')
  ) {
    yield put(AppAction.onLoading(false));
    return Alert.alert(
      t('alert_title_oops'),
      `${t(
        'alert_content_connect_mirror_failed',
      )} Wrong host address format, please reset Mirror.`,
    );
  }
  yield call(forceDisconnect);

  monitorChannel = yield actionChannel([
    WsTypes.ON_WS_ERROR,
    WsTypes.ON_WS_OPENED,
    WsTypes.ON_WS_CLOSED,
    WsTypes.CLOSE_CONNECTION,
    BleDeviceTypes.ON_CONNECT,
    MirrorTypes.ON_MIRROR_CONNECT,
    MirrorTypes.ON_MIRROR_DISCONNECT,
    MirrorTypes.ON_MIRROR_DISCONNECT_BY_USER,
  ]);
  websocketTask = yield fork(initialize, {
    url: `${Config.WEBSOCKET_MODE_PREFIX || 'ws'}://${device.host}:${device.port}`,
    options,
  });
  communicateTask = yield fork(communicate);
  try {
    for (;;) {
      let isConnected = yield select((state) => state.websocket.isConnected);
      const action = yield take(monitorChannel);
      console.log('action.type, isConnected=>', action.type, isConnected);
      if (action.type === WsTypes.ON_WS_ERROR || action.type === WsTypes.ON_WS_CLOSED) {
        const retryCount = yield select((state) => state.websocket.retryCount);
        const lastMirrorDevice = yield select((state) => state.mirror.lastDevice);
        const lastMirrorDeviceConnectedAt = yield select(
          (state) => state.mirror.lastDeviceConnectedAt,
        );
        let doAutoMirrorDeviceConnect = false;
        console.log(
          'connectAndMonitoring ==>',
          lastMirrorDevice,
          lastMirrorDeviceConnectedAt,
        );
        if (lastMirrorDevice != null && lastMirrorDeviceConnectedAt != null) {
          let connectedAt = null;
          if (lastMirrorDeviceConnectedAt instanceof Date)
            connectedAt = lastMirrorDeviceConnectedAt;
          else connectedAt = new Date(lastMirrorDeviceConnectedAt);
          let diffMinutes = Math.floor((new Date() - connectedAt) / 1000 / 60);
          console.log('=== diffMinutes ===', diffMinutes);
          if (diffMinutes < 60) doAutoMirrorDeviceConnect = true;
        }

        const message = typeof error === 'object' ? JSON.stringify(error) : error;
        const { event = {}, error } = action;
        console.log('action.event=>', action.event);
        if (action.type === WsTypes.ON_WS_ERROR) {
          yield put(MirrorAction.onMirrorLog(`WebSocket error: ${message}`));
          if (Platform.OS === 'android' && __DEV__) {
            ToastAndroid.show(`WebSocket error: ${message}`, ToastAndroid.SHORT);
          }
        } else if (error && typeof error === 'string' && error.includes('ENONET')) {
          Alert.alert(
            t('alert_title_oops'),
            `Please check your Wifi environment, it may blocks WebSocket protocol to communicate.(${error})`,
          );
        }
        if (action.type === WsTypes.ON_WS_CLOSED) {
          if (isConnectSuccess) {
            isConnectSuccess = false;
          }
          yield put(
            MirrorAction.onMirrorLog(`WebSocket retry count before close: ${retryCount}`),
          );
          yield put(MirrorAction.onMirrorLog(`WebSocket close code: ${event.code}`));
          yield put(MirrorAction.onMirrorLog(`WebSocket close reason: ${event.reason}`));
          yield put(
            MirrorAction.onMirrorLog(
              `WebSocket close error? ${message ? message : 'no'}`,
            ),
          );
          console.log('retryCount=>', retryCount);
          console.log('options.maxRetries=>', options.maxRetries);
          if (event.code === 1001) {

            const { event: wsEvent } = yield select(
              (state) => state.websocket.lastReceivedMessage,
            );

            if (wsEvent !== MirrorEvents.EXCEPTION) {
            }
          }
          if (event.code === 1001) {
            yield put(MirrorAction.onMirrorConnect(lastMirrorDevice));
          }
        }

        if (action.type === WsTypes.ON_WS_ERROR) {
          if (isConnectSuccess) {
            isConnectSuccess = false;
          }
          yield put(
            MirrorAction.onMirrorLog(`WebSocket auto reconnect failed ${event.reason}`),
          );
          const currentScreenName = yield select((state) => state.appRoute.routeName);
          console.log('auto connect currentScreenName=>', currentScreenName);
          if (
            !currentScreenName.includes('Debug') &&
            currentScreenName !== 'MirrorPairScreen' &&
            currentScreenName !== 'MiiSettingScreen' &&
            currentScreenName !== 'MirrorSetupWifiInitScreen'
          ) {
            if (retryCount >= options.maxRetries) {
              yield put(MirrorAction.onMirrorLog(`WebSocket error: ${message}`));
              yield put(MirrorAction.onMirrorDisconnect());
              yield put(AppAction.onLoading(false));

              isConnected = yield select((state) => state.websocket.isConnected);
              if (!isConnected) {
                let currentNetworkInfo = yield select(
                  (state) => state.appState.currentNetworkInfo,
                );
                if (currentNetworkInfo.type === 'cellular') {
                  if (Platform.OS === 'android') {
                    AndroidWifi.isEnabled((isEnabled) => {
                      AndroidWifi.forceWifiUsage(true);
                    });
                  }
                  yield delay(5000);
                }
                currentNetworkInfo = yield select(
                  (state) => state.appState.currentNetworkInfo,
                );

                if (currentNetworkInfo) {
                  yield call(showRetryOrCancelDialog, lastMirrorDevice);
                }
              }
            }
          }
        }
      }
      if (action.type === BleDeviceTypes.ON_CONNECT) {
        const deviceObj = JSON.parse(
          JSON.stringify(action.device, getCircularReplacer()),
        );
        yield put(
          MirrorAction.onMirrorLog(
            `Ble device founded and connected id: ${deviceObj.id}`,
          ),
        );
        yield delay(500);
        yield put(
          BleAction.onBleMonitorCharacteristic(
            BleServices.HEAR_RATE_SERVICE_GUID,
            BleServices.HEART_RATE_MEASUREMENT_CHARACTERISTIC_GUID,
          ),
        );
        yield delay(250);
        if (bleMonitorTask) {
          yield cancel(bleMonitorTask);
        }
        yield delay(250);
        bleMonitorTask = yield fork(handleSendHeartRateDate, deviceObj);
      }

      if (action.type === WsTypes.ON_WS_OPENED) {
        const userToken = yield select((state) => state.user.token);
        const userId = yield select((state) => state.user.id);
        const env = Config.DEFAULT_SERVER_ENV;
        let fcmToken = yield select((state) => state.user.fcmToken);
        const appLocales = yield select((state) => state.appState.currentLocales);
        let acceptLanguage = null;
        if (appLocales instanceof Array) {
          acceptLanguage = appLocales.map((e) => e.languageTag).toString();
        }
        if (!fcmToken) {
          fcmToken = UUID.generate();
          yield put(UserActions.onFcmTokenUpdate(fcmToken));
        }

        const coordinate = yield select((state) => state.user.coordinate);

        yield put(
          WsAction.onWsSendMessage({
            event: MirrorEvents.CONNECT_DEVICE,
            data: {
              userToken,
              mobileDeviceId: fcmToken,
              language: acceptLanguage,
              coordinate,
              userId,
              env,
            },
          }),
        );

        if (reconnectTask) {
          yield cancel(reconnectTask);
        }
        reconnectTask = yield fork(reConnect, device);

        yield take(WsTypes.ON_WS_MESSAGE_RECEIVED);
        const { event: wsEvent, data: wsData } = yield select(
          (state) => state.websocket.lastReceivedMessage,
        );
        if (wsEvent === MirrorEvents.CONNECT_DEVICE_SUCCESS) {
          yield put(
            MirrorAction.updateMirrorStore({
              deviceAppVersion: '',
              deviceHardwareVersion: '',
            }),
          );
          const mirror_instructor_volume = yield select(
            (state) => state.mirror.options[MirrorOptions.INSTROCTOR_VOLUME],
          );
          const systemVolume = SystemSetting.getVolume()
            .then((systemVolume) => {
              console.log('=== system voulme ===', systemVolume);
              console.log('=== check volumes ===', mirror_instructor_volume);

              if (mirror_instructor_volume < systemVolume * 100) {
                console.log('lower system value', mirror_instructor_volume);
                SystemSetting.setVolume(mirror_instructor_volume / 100);
              } else if (mirror_instructor_volume > systemVolume * 100) {
                console.log('lower mirror value', systemVolume * 100);

                AppStore.dispatch(
                  MirrorActions.onMirrorCommunicate(MirrorEvents.CHANGE_AUDIO_VOLUME, {
                    type: 'instructor-volume',
                    volume: Math.round(systemVolume * 100),
                    save: true,
                  }),
                );

                AppStore.dispatch(
                  MirrorActions.onMirrorCommunicate(MirrorEvents.CHANGE_AUDIO_VOLUME, {
                    type: 'music-volume',
                    volume: Math.round(systemVolume * 100),
                    save: true,
                  }),
                );
              }
            })
            .catch((err) => {
              console.log('=== voulume sync error ===', err);
            });

          isConnectSuccess = true;
          console.log('isConnectSuccess =>', isConnectSuccess);
          console.log('CONNECT_DEVICE_SUCCESS wsData =>', wsData);
          yield put(MirrorAction.updateMirrorStore({ connectSuccessResult: wsData }));

          let isMotionCaptureEnabled = false;
          if (wsData.enableMotionCapture != null) {
            isMotionCaptureEnabled = wsData.enableMotionCapture;
          }

          yield put(
            PlayerActions.updatePlayerStore({
              isMotionCaptureEnabled,
            }),
          );
          const activeBleDevice = yield select((state) => state.bleDevice.activeDevice);
          if (!activeBleDevice) {
            yield put(BleAction.onInitial());
            if (bleConnectTask) {
              yield cancel(bleConnectTask);
            }
            bleConnectTask = yield fork(autoConnectLastHeartRateDevice);
          }
          yield put(MirrorAction.onMirrorConnected(device));
          if (typeof onConnectedCallback === 'function') {
            yield call(onConnectedCallback);
          }
          const routeName = yield select((state) => state.appRoute.routeName);
          const showDistanceMeasuring = yield select(
            (state) => state.user.isNextShowDistance,
          );

          const pervRouteName = yield select((state) => state.appRoute.prevRoute);
          const classId = yield select((state) => state.player.detail.id);
          yield put(
            MirrorAction.mirrorCompare({
              onPressNo: () => {
                console.log('mirrorCompare onPressNo');
                console.log('pervRouteName=>', pervRouteName);
                if (wsData.page !== 'class-summary' && wsData.page !== 'playing-class') {
                  if (pervRouteName === 'LiveDetail') {
                    AppStore.dispatch(
                      MirrorAction.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
                        classId,
                        classType: 'live',
                      }),
                    );
                  } else if (pervRouteName.includes('ClassDetail')) {
                    AppStore.dispatch(
                      MirrorAction.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
                        classId,
                        classType: 'on-demand',
                      }),
                    );
                  }
                }
              },
            }),
          );

          {
            const preRecordedNextScreen = yield select(
              (state) => state.mirror.preRecordedNextScreen,
            );
            if (typeof preRecordedNextScreen === 'string') {
              const { preRecordedTrainingEventId, preRecordedFriendId } = yield select(
                (state) => state.mirror,
              );
              if (preRecordedTrainingEventId) {
                yield call(requestAnimationFrame, Actions.CommunityDetailScreen);
              } else if (preRecordedFriendId) {
                yield put(CommunityActions.getUserDetail(preRecordedFriendId));
              } else {
                yield call(requestAnimationFrame, Actions[preRecordedNextScreen]);
              }
              yield put(
                MirrorAction.updateMirrorStore({
                  preRecordedNextScreen: null,
                  preRecordedTrainingEventId: null,
                  preRecordedFriendId: null,
                }),
              );
            }
          }
          yield put(AppAction.onLoading(false));
          yield call(MirrorOnlineStatusSaga.setOnline);
          yield fork(MirrorOnlineStatusSaga.startAutoOfflineTimer);
        }
      }
      if (action.type === MirrorTypes.ON_MIRROR_DISCONNECT_BY_USER) {
        if (isConnected) {
          const fcmToken = yield select((state) => state.user.fcmToken);
          yield put(
            WsAction.onWsSendMessage({
              event: MirrorEvents.DISCONNECT_DEVICE,
              data: {
                mobileDeviceId: fcmToken,
              },
            }),
          );
        }
        const { code, reason } = action;
        yield put(WsAction.closeConnection(code, reason));
        console.log('break saga');
        monitorChannel.close();
        if (websocketTask !== null) {
          yield cancel(websocketTask);
        }
        if (communicateTask !== null) {
          yield cancel(communicateTask);
        }
        break;
      }
    }
  } finally {
    console.log('setupSaga ended!!!');
    if (yield cancelled()) {
      console.log('setupSaga cancelled!!!');
      monitorChannel.close();
      if (websocketTask !== null) {
        yield cancel(websocketTask);
      }
      if (communicateTask !== null) {
        yield cancel(communicateTask);
      }
    }
  }
}

let onSuccessMap = {},
  onFailureMap = {};

function* communicate() {
  console.log('@enter communicate saga');
  const sagaChannel = yield actionChannel([
    WsTypes.ON_WS_MESSAGE_RECEIVED,
    MirrorTypes.ON_MIRROR_COMMUNICATE,
    MirrorTypes.ON_MIRROR_GET_AUDIO_SETTING,
  ]);
  try {
    for (;;) {
      const action = yield take(sagaChannel);

      const { type, event = '', data = undefined } = action;
      if (type === MirrorTypes.ON_MIRROR_COMMUNICATE) {
        onSuccessMap[event] = action.onSuccess;
        onFailureMap[event] = action.onFailure;
      }
      console.log('communicate type, event, data=>', type, event, data);
      if (
        type === MirrorTypes.ON_MIRROR_COMMUNICATE &&
        Object.values(MirrorEvents).includes(event)
      ) {
        switch (event) {
          default: {
            if (
              [
                MirrorEvents.ACTIVITY_CAM,
                MirrorEvents.START_LIVE_CLASS,
                MirrorEvents.CHANGE_PLAYLIST,
                MirrorEvents.PICTURE_ACCEPT,
                MirrorEvents.SET_REACTION,
                MirrorEvents.SAVE_FEEDBACK_FINISH,
                MirrorEvents.UPDATE_MUSIC_PLAY_SETTING,
                MirrorEvents.CHANGE_DEVICE_DEBUG_MODE,
                MirrorEvents.PLAYER_CONTROL,
                MirrorEvents.DISPLAY_MODE_SWITCH,
              ].includes(event)
            ) {
              yield put(AppAction.onLoading(false));
            } else if (
              [MirrorEvents.START_CLASS, MirrorEvents.RESUME_CLASS].includes(event)
            ) {
              yield put(AppAction.onLoading(true));
            }
            yield put(
              WsAction.onWsSendMessage({
                event,
                data,
              }),
            );
            break;
          }
          case MirrorEvents.DEVICE_CHECK_UPDATE_REQUEST: {
            const fcmToken = yield select((state) => state.user.fcmToken);
            let updateIfHasNewVersion = true;
            if (data && data.updateIfHasNewVersion != null)
              updateIfHasNewVersion = data.updateIfHasNewVersion;

            console.log('=== updateIfHasNewVersion ===', updateIfHasNewVersion);
            yield put(
              WsAction.onWsSendMessage({
                event,
                data: { mobileDeviceId: fcmToken, updateIfHasNewVersion },
              }),
            );
            break;
          }

          case MirrorEvents.DEVICE_SOFTWARE_UPDATE_ACTION: {
            yield put(
              WsAction.onWsSendMessage({
                event,
                data,
              }),
            );
            if (data.action === 'now') {
              yield put(AppAction.onLoading(true));
              yield delay(500);
              yield call(requestAnimationFrame, Actions.ClassScreen);
              yield put(AppAction.onLoading(false));
            }
            break;
          }

          case MirrorEvents.REQUEST_BLUETOOTH_AUDIO_DEVICES: {
            yield put(MirrorAction.startScan());
            yield put(
              WsAction.onWsSendMessage({
                event,
                data,
              }),
            );
            break;
          }
          case MirrorEvents.DISPLAY_SUMMARY: {
            const playerDetail = yield select((state) => state.player.detail);
            const currentClassType = yield select(
              (state) => state.mirror.currentClassType,
            );
            const summaryData = {
              classId: playerDetail.id,
            };
            switch (currentClassType) {
              case 'class':
                summaryData.trainingClassHistoryId = playerDetail.trainingClassHistory.id;
                break;
              case 'live':
                summaryData.liveClassHistoryId = playerDetail.liveClassHistory.id;
                break;
              case 'program':
                summaryData.programClassHistoryId =
                  playerDetail.trainingProgramClassHistory.id;
                break;
              case 'event':
                summaryData.eventClassHistoryId =
                  playerDetail.trainingEventClassHistory.id;
                break;

              default:
                break;
            }
            yield put(
              WsAction.onWsSendMessage({
                event,
                data: summaryData,
              }),
            );
            break;
          }
          case MirrorEvents.OUTPUT_AUDIO_DEVICE: {
            yield put(
              WsAction.onWsSendMessage({
                event,
                data,
              }),
            );
            yield put(MirrorAction.onMirrorOutputAudioDevice(data));
            break;
          }
          case MirrorEvents.CHANGE_AUDIO_VOLUME: {
            yield put(
              WsAction.onWsSendMessage({
                event,
                data: { type: data.type, volume: data.volume, save: data.save },
              }),
            );
            yield put(
              MirrorAction.onMirrorChangeAudioVolumeSuccess(data.type, data.volume),
            );
            yield put(AppAction.onLoading(false));
            break;
          }
          case MirrorEvents.UPDATE_DEVICE_DISPLAY_SETTING: {
            console.log('data.optionName=>', data.optionName);
            console.log('data.value=>', data.value);
            console.log('data=>', data);
            if (data.optionName === 'location') {
              yield put(MirrorAction.setLocation(data.value));
            } else {
              yield put(MirrorAction.setDisplayOption(data));
            }
            yield put(WsAction.onWsSendMessage({ event, data }));
            yield put(AppAction.onLoading(false));
            break;
          }
          case MirrorEvents.SAVE_ACTIVITY_FINISH: {
            yield put(WsAction.onWsSendMessage({ event, data }));
            yield put(AppAction.onLoading(false));

            const routeName = yield select((state) => state.player.routeName);
            console.log('SAVE_ACTIVITY_FINISH routeName=>', routeName);
            if (routeName === 'ProgramClassDetail') {
              yield call(requestAnimationFrame, Actions.ProgramFeedbackScreen);
            } else {
              yield call(requestAnimationFrame, Actions.FeedbackScreen);
            }
            break;
          }
          case MirrorEvents.SET_WORKOUT_SETTING: {
            if (data.optionName === 'video-size') {
              yield put(MirrorAction.setVideoSize(data.value));
            }
            yield put(WsAction.onWsSendMessage({ event, data }));
            break;
          }
          case MirrorEvents.UPDATE_DEVICE_SETTING: {
            yield put(WsAction.onWsSendMessage({ event, data }));

            if (data.optionName === 'casting') {
              yield put(
                MirrorAction.updateMirrorStore({
                  isCasting: data.value,
                }),
              );

              if (data.value === true) {
                const timerChannel = yield call(() =>
                  eventChannel((emit) => {
                    const timer = setTimeout(() => {
                      emit('timeout');
                    }, 10 * 1000);
                    return () => {
                      clearTimeout(timer);
                    };
                  }),
                );
                const { success, timeout } = yield race({
                  success: take(MirrorTypes.UPDATE_MIRROR_STORE),
                  timeout: take(timerChannel),
                });
                if (timeout) {
                  yield put(
                    MirrorAction.updateMirrorStore({
                      isCasting: false,
                    }),
                  );
                }
              }
            }
            break;
          }
        }
      }
      else if (type === WsTypes.ON_WS_MESSAGE_RECEIVED) {
        const { event: wsEvent, data: wsData } = yield select(
          (state) => state.websocket.lastReceivedMessage,
        );
        yield fork(reaction, wsEvent, wsData);
      }
    }
  } finally {
    if (yield cancelled()) {
      sagaChannel.close();
      if (communicateTask) {
        yield cancel(communicateTask);
      }
      if (bleMonitorTask) {
        yield cancel(bleMonitorTask);
      }
    }
  }
}

function* reaction(wsEvent, wsData) {
  console.debug('wsEvent, data=>', wsEvent, wsData);

  if (wsEvent == 'exception' && wsData != null && wsData.errorCode == 1378) {
    store.dispatch(MirrorActions.onMirrorDisconnectByUser());
  }
  switch (wsEvent) {
    case MirrorEvents.CONNECT_DEVICE_SUCCESS: {
      console.log(
        'CONNECT_DEVICE_SUCCESS  wsData =>',
        MirrorEvents.GET_DEVICE_INFO,
        wsData,
      );
      store.dispatch(MirrorAction.onMirrorCommunicate(MirrorEvents.GET_DEVICE_INFO));

      break;
    }

    case MirrorEvents.PREPARE_1ON1_CLASS_SUCCESS: {
      console.log('PREPARE_1ON1_CLASS_SUCCESS  wsData =>', wsData);
      yield put(CommunityActions.update1on1OnlineStatus(false));
      Actions.replace('Prepare1on1SessionScreen');
      break;
    }

    case MirrorEvents.DEVICE_SOFTWARE_UPDATE: {
      yield put(AppAction.onLoading(false));
      const {
        hasNewVersion,
        canUpdateNewVersion,
        deviceAppVersion,
        deviceHardwareVersion,
        message,
      } = wsData;
      console.log('=== DEVICE_SOFTWARE_UPDATE deviceAppVersion ===', deviceAppVersion);
      yield put(
        MirrorAction.updateMirrorStore({
          deviceHardwareVersion: deviceHardwareVersion,
        }),
      );
      if (hasNewVersion && !canUpdateNewVersion) {
        const fcmToken = yield select((state) => state.user.fcmToken);
        Alert.alert(t('software_update_title'), t('software_update_desc'), [
          {
            text: t('software_update_update_now'),
            onPress: () =>
              store.dispatch(
                MirrorAction.onMirrorCommunicate(
                  MirrorEvents.DEVICE_SOFTWARE_UPDATE_ACTION,
                  { mobileDeviceId: fcmToken, action: 'now' },
                ),
              ),
          },
          {
            text: t('software_update_later'),
            onPress: () =>
              store.dispatch(
                MirrorAction.onMirrorCommunicate(
                  MirrorEvents.DEVICE_SOFTWARE_UPDATE_ACTION,
                  { mobileDeviceId: fcmToken, action: 'later' },
                ),
              ),
          },
          {
            text: t('software_update_do_not_update'),
            onPress: () =>
              store.dispatch(
                MirrorAction.onMirrorCommunicate(
                  MirrorEvents.DEVICE_SOFTWARE_UPDATE_ACTION,
                  { mobileDeviceId: fcmToken, action: 'not-this-version' },
                ),
              ),
          },
        ]);
      }
      else if (!hasNewVersion && !canUpdateNewVersion) {
        Alert.alert(t('alert_title_oops'), message);
      }
      else if (hasNewVersion && canUpdateNewVersion) {
        yield put(MirrorActions.onMirrorDisconnectByUser());
      } else {
        Alert.alert('', wsData.message);
      }
      break;
    }

    case MirrorEvents.DEVICE_SOFTWARE_UPDATE_ACTION_SUCCESS: {
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.OUTPUT_AUDIO_DEVICE_SUCCESS: {
      console.log('=== OUTPUT_AUDIO_DEVICE_SUCCESS ===', wsData);
      yield delay(2000);
      yield put(MirrorAction.onMirrorOutputAudioDeviceSuccess(wsData));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.GET_AUDIO_SETTING_SUCCESS: {
      yield put(MirrorAction.onMirrorGetAudioSettingSuccess(wsData.options));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.GET_WORKOUT_SETTING_SUCCESS: {
      yield put(MirrorAction.onMirrorGetWorkoutSettingSuccess(wsData.options));
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.CLASS_PREVIEW_SUCCESS: {
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.EXIT_CLASS_PREVIEW_SUCCESS: {
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.RESPONSE_BLUETOOTH_AUDIO_DEVICES: {
      yield put(
        MirrorAction.onMirrorResponseBluetoothAudioDevices(
          wsData['paired-devices'],
          wsData['other-devices'],
        ),
      );
      yield put(MirrorAction.endScan());
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.UPDATE_MUSIC_PLAY_SETTING_SUCCESS: {
      yield put(MirrorAction.onMirrorUpdateMusicPlaySettingSuccess());
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.CHANGE_PLAYLIST_SUCCESS: {
      console.log('wsData ->', wsData);
      yield put(MirrorAction.changePlaylistSuccess(wsData));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.START_LIVE_CLASS_SUCCESS: {
      yield put(AppAction.onLoading(false));
      yield fork(MirrorCalorieStatusSaga.startMirrorCalorieMonitor);
      yield put(
        MirrorAction.updateMirrorStore({
          isPlayingClass: true,
          currentClassType: 'live',
        }),
      );
      yield call(MirrorOnlineStatusSaga.setOnline);
      break;
    }

    case MirrorEvents.START_CLASS_SUCCESS: {
      yield put(
        MirrorAction.updateMirrorStore({
          isPlayingClass: true,
          playerState: 'play',
        }),
      );
      const routeName = yield select((state) => state.player.routeName);
      if (routeName === 'ProgramClassDetail') {
        yield put(MirrorAction.updateMirrorStore({ currentClassType: 'program' }));
      } else if (routeName === 'EventClassDetail') {
        yield put(MirrorAction.updateMirrorStore({ currentClassType: 'event' }));
      } else {
        yield put(MirrorAction.updateMirrorStore({ currentClassType: 'class' }));
      }
      yield fork(MirrorCalorieStatusSaga.startMirrorCalorieMonitor);
      yield put(MirrorAction.resetCurrentPosition());
      if (routeName === 'EventClassDetail') {
        yield call(requestAnimationFrame, Actions.EventPlayerScreen);
      } else if (routeName === 'ProgramClassDetail') {
        yield call(requestAnimationFrame, Actions.ProgramPlayerScreen);
      } else {
        yield call(requestAnimationFrame, Actions.PlayerScreen);
      }
      {
        yield delay(5 * 1000);
        const spotifyPlayLists = yield select((state) => state.spotify.playlists);
        const activeList = yield select((state) => state.mirror.activeList);
        const shouldPlayingSpotify =
          activeList === 'spotify-music' || spotifyPlayLists.some((e) => e.isPlaying);
        yield put(
          MirrorAction.onMirrorLog(
            `Music: switch mirror music source to "${
              shouldPlayingSpotify ? 'Spotify' : 'Mirror internal'
            }".`,
          ),
        );
        console.log('shouldPlayingSpotify =>', shouldPlayingSpotify);
        if (shouldPlayingSpotify) {
          const selectedPlayListItem = spotifyPlayLists.find((e) => e.isPlaying);
          const spotifyAccessToken = yield select(
            (state) => state.user.spotifyTokens.accessToken,
          );
          yield put(
            MirrorAction.onMirrorCommunicate(MirrorEvents.CHANGE_PLAYLIST, {
              type: 'spotify-music',
              playlist: {
                id: selectedPlayListItem.id,
                name: selectedPlayListItem.name,
                uri: selectedPlayListItem.uri,
                token: spotifyAccessToken,
              },
            }),
          );
        } else {
          yield put(
            MirrorAction.onMirrorCommunicate(MirrorEvents.CHANGE_PLAYLIST, {
              type: 'mirror-music',
            }),
          );
        }
        const instructorVolume = yield select(
          (state) => state.mirror.options[MirrorOptions.INSTROCTOR_VOLUME],
        );
        yield put(
          MirrorAction.onMirrorLog(
            `Music: set INSTROCTOR_VOLUME to "${instructorVolume}".`,
          ),
        );
        yield put(
          MirrorAction.onMirrorCommunicate(MirrorEvents.CHANGE_AUDIO_VOLUME, {
            type: MirrorOptions.INSTROCTOR_VOLUME,
            volume: instructorVolume,
            save: true,
          }),
        );
        const musicVolume = yield select(
          (state) => state.mirror.options[MirrorOptions.MUSIC_VOLUME],
        );
        yield put(
          MirrorAction.onMirrorLog(`Music: switch MUSIC_VOLUME to "${musicVolume}".`),
        );
        yield put(
          MirrorAction.onMirrorCommunicate(MirrorEvents.CHANGE_AUDIO_VOLUME, {
            type: MirrorOptions.MUSIC_VOLUME,
            volume: musicVolume,
            save: true,
          }),
        );
      }
      yield call(MirrorOnlineStatusSaga.setOnline);
      yield delay(500);
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.PLAYING_CLASS: {
      const currentStepId = yield select((state) => state.mirror.currentStepId);
      const currentDuration = yield select((state) => state.mirror.currentDuration);
      const steps = yield select((state) => state.player.detail.trainingSteps);
      const currentAction = yield select((state) => state.mirror.currentAction);
      const isSameStepChange = yield select((state) => state.mirror.isSameStepChange);
      const isDiffStepChange = yield select((state) => state.mirror.isDiffStepChange);
      const isStepUpdateDelay = yield select((state) => state.mirror.isStepUpdateDelay);
      let currentStepIndex = 0;
      let wsStepIndex = 0;
      if (steps && steps.length > 0) {
        steps.forEach((item, index) => {
          if (item.id === currentStepId) {
            currentStepIndex = index;
          }
          if (item.id === wsData.stepId) {
            wsStepIndex = index;
          }
        });
      }

      function* updateTime() {
        yield put(MirrorAction.onMirrorPlayingClass(wsData));
        yield delay(1000);
        yield put(MirrorAction.setCarouselDuration());
      }
      if (wsStepIndex === currentStepIndex) {
        if (isSameStepChange) {
          yield put(MirrorAction.updateMirrorStore({ isSameStepChange: false }));
        } else {
          if (isStepUpdateDelay) {
            if (wsData.currentPosition === currentDuration) {
              yield updateTime();
            }
            yield put(MirrorAction.updateMirrorStore({ isStepUpdateDelay: false }));
          } else {
            if (currentDuration <= wsData.currentPosition) {
              yield updateTime();
            }
          }
        }
      } else if (wsStepIndex > currentStepIndex || isEmpty(currentAction)) {
        if (isDiffStepChange) {
          yield put(MirrorAction.updateMirrorStore({ isDiffStepChange: false }));
          yield put(MirrorAction.updateMirrorStore({ isStepUpdateDelay: true }));
        } else {
          yield updateTime();
        }
      }
      break;
    }

    case MirrorEvents.PLAYER_STATUS: {
      console.log('wsData.state=>', wsData.state);
      const { state: wsState } = wsData;
      yield put(MirrorAction.onMirrorPlayerStatus(wsData));
      switch (wsState) {
        case 'ended': {
          const fromRouteName = yield select((state) => state.player.routeName);
          const detail = yield select((state) => state.player.detail);
          const token = yield select((state) => state.user.token);
          const activeTrainingProgramId = yield select(
            (state) => state.user.activeTrainingProgramId,
          );
          yield put(
            MirrorAction.updateMirrorStore({
              isPlayingClass: false,
            }),
          );
          console.log('updateMirrorStore isPlayingClass...');
          while (true) {
            yield delay(1000);
            console.log('waiting isPlayingClass...');
            const isPlayingClass = yield select((state) => state.mirror.isPlayingClass);
            if (!isPlayingClass) {
              console.log('waiting isPlayingClass success!!!!');
              yield delay(1000);
              break;
            }
          }

          console.log('ended detail =>', detail);

          if (detail.scheduledAt) {
            console.log('=-------- live -------=');
            yield put(LiveActions.finishLiveClass(detail.liveClassHistory.id));
          } else {
            if (fromRouteName === 'ProgramClassDetail') {
              const trainingProgramClassHistoryId = yield select(
                (state) => state.user.trainingProgramClassHistoryId,
              );
              const trainingProgramHistoryId = yield select(
                (state) =>
                  state.class.detail.trainingProgramClassHistory.trainingProgramHistoryId,
              );
              const body = {
                trainingProgramClassHistoryId,
                trainingProgramHistoryId,
              };
              yield put(
                ProgramActions.finishProgramClass(
                  detail.id,
                  token,
                  body,
                  activeTrainingProgramId,
                  false,
                ),
              );
            } else if (fromRouteName === 'EventClassDetail') {
              const eventId = yield select((state) => state.community.eventDetail.id);
              const eventClassId = yield select((state) => state.community.eventClass.id);
              yield put(CommunityActions.finishEventClass(eventClassId, eventId));
            } else {
              yield put(ClassActions.finishClass(detail.trainingClassHistory.id, token));
            }
          }

          const currentRouteName = yield select((state) => state.appRoute.routeName);

          requestAnimationFrame(() => {
            if (currentRouteName.includes('Modal')) {
              Actions.pop();
            }
          });
          break;
        }

        case 'buffering': {
          yield put(AppAction.onLoading(true, null, { hide: true }));
          break;
        }

        case 'ready': {
          yield delay(200);
          yield put(AppAction.onLoading(false));
          break;
        }

        default: {
          yield put(AppAction.onLoading(false));
          break;
        }
      }
      break;
    }

    case MirrorEvents.PLAYER_CONTROL_SUCCESS: {
      yield put(MirrorAction.onMirrorPlayerControlSuccess(wsData));
      yield put(MirrorAction.setCurrentStepId(wsData.stepId));
      yield put(MirrorAction.resetCurrentAction());
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.EXIT_CLASS_SUCCESS: {
      yield put(
        MirrorAction.updateMirrorStore({
          isPlayingClass: false,
        }),
      );

      yield put(MirrorAction.onMirrorExitClassSuccess(wsData));

      if (typeof onSuccessMap[MirrorEvents.EXIT_CLASS] === 'function') {
        yield call(onSuccessMap[MirrorEvents.EXIT_CLASS]);
        delete onSuccessMap[MirrorEvents.EXIT_CLASS];
      }

      yield put(AppAction.onLoading(false));
      yield MirrorOnlineStatusSaga.startAutoOfflineTimer();
      break;
    }

    case MirrorEvents.TAKE_PICTURE_SUCCESS: {
      yield put(MirrorAction.onMirrorTakePictureSuccess(wsData.image));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.PICTURE_PREVIEW: {
      const stack = yield select((state) => state.appRoute.stack);
      console.log('stack=>', stack);
      if (_.last(stack).includes('Program')) {
        yield call(Actions.ProgramPhotoPreview);
      } else {
        yield call(Actions.PhotoPreview);
      }
      yield put(MirrorAction.onMirrorPicturePreview(wsData.thumbnail));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.GET_MOTION_CAPTURE_PICTURE_PREVIEW_SUCCESS: {
      yield put(MirrorAction.onMirrorGetMotionCapturePreviewSuccess(wsData.thumbnails));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.SET_WORKOUT_SETTING_SUCCESS: {
      if (wsData.optionName !== 'video-size') {
        yield put(MirrorAction.onMirrorSetWorkoutSettingSuccess(wsData));
      }
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.GET_DEVICE_DISPLAY_SETTING_SUCCESS: {
      yield put(MirrorAction.getDisplayOptionsSuccess(wsData));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.UPDATE_DEVICE_DISPLAY_SETTING_SUCCESS: {
      yield put(MirrorAction.setDisplayOptionSuccess(wsData));
      yield put(AppAction.onLoading(false));
      break;
    }

    case MirrorEvents.EXCEPTION: {
      let button = [];
      let exception = '';
      let message = '';
      if ('data' in wsData) {
        exception = get(wsData.data, 'exception', '');
        message = get(wsData.data, 'message', '');
      }
      yield put(
        MirrorAction.updateMirrorStore({
          isWaitingForDeviceStartClass: false,
        }),
      );
      const routeName = yield select((state) => state.appRoute.routeName);
      const playerDetail = yield select((state) => state.player.detail);
      button = [
        {
          text: t('__ok'),
          onPress: () => {
            console.log('joinEvent full book');

            if (routeName === 'LivePlayerScreen' && get(wsData, 'errorCode') === 1392) {
              AppStore.dispatch(
                MirrorAction.onMirrorCommunicate(
                  MirrorEvents.EXIT_CLASS,
                  { classId: playerDetail.id },
                  Actions.pop,
                ),
              );
            }
          },
          style: 'cancel',
        },
        {
          text: t('more'),
          onPress: () => {
            Alert.alert(
              `${t('more')}`,
              `\nEvent: ${wsData.event}\nError Code: ${get(
                wsData,
                'errorCode',
                '',
              )}\n${get(wsData, 'message', '')}\n${exception}\n${message}`,
              [button[0]],
            );
          },
        },
      ];

      Alert.alert(`${t('__exception')}`, `${wsData.title}`, button, {
        cancelable: false,
      });
      switch (wsData.event) {
        case MirrorEvents.CONNECT_DEVICE: {
          const { mobileDeviceId } = wsData;
          const fcmToken = yield select((state) => state.user.fcmToken);
          console.log('fcmToken=>', fcmToken);
          console.log('fcmToken !== mobileDeviceId=>', fcmToken !== mobileDeviceId);

          if (fcmToken !== mobileDeviceId) {
            yield put(WsAction.closeConnection());
            yield put(
              MirrorAction.onMirrorLog(
                `Exception - ${wsData.event}: Client will now disconnect automatically.`,
              ),
            );
          }
          if (reconnectTask) {
            yield cancel(reconnectTask);
          }
          break;
        }
        case MirrorEvents.OUTPUT_AUDIO_DEVICE: {
          console.log('EXCEPTION OUTPUT_AUDIO_DEVICE wsData =>', wsData);
          yield put(MirrorAction.onMirrorChangeAudioOutputDevice(null));
          break;
        }
        default: {
          break;
        }
      }
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.START_DISTANCE_MEASURING_SUCCESS: {
      yield call(requestAnimationFrame, Actions.DistanceMeasuringModal);
      break;
    }
    case MirrorEvents.END_DISTANCE_MEASURING_SUCCESS: {
      if (typeof onSuccessMap[MirrorEvents.START_DISTANCE_MEASURING] === 'function') {
        yield call(onSuccessMap[MirrorEvents.START_DISTANCE_MEASURING]);
        delete onSuccessMap[MirrorEvents.START_DISTANCE_MEASURING];
        yield call(requestAnimationFrame, Actions.pop);
      } else yield call(requestAnimationFrame, Actions.pop);
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.CANCEL_TAKE_PICTURE_SUCCESS: {
      yield call(Actions.pop);
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.GET_DEVICE_INFO_SUCCESS: {
      console.log('GET_DEVICE_INFO_SUCCESS wsData =>', wsData);

      const featureOptions = Object.assign(
        MirrorInitialState.featureOptions,
        wsData.featureOptions,
      );
      console.log('featureOptions=>', featureOptions);
      yield put(
        MirrorAction.getDeviceInfoSuccess({
          deviceAppVersion: wsData.deviceAppVersion,
          deviceHardwareVersion: wsData.deviceHardwareVersion,
          featureOptions,
        }),
      );
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.ENGINEERING_MODE_SUCCESS: {
      console.log('ENGINEERING_MODE_SUCCESS  wsData =>', wsData);
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.END_ENGINEERING_MODE_SUCCESS: {
      console.log('END_ENGINEERING_MODE_SUCCESS  wsData =>', wsData);
      yield put(AppAction.onLoading(false));
      break;
    }
    case MirrorEvents.UPDATE_DEVICE_SETTING_SUCCESS: {
      console.log('UPDATE_DEVICE_SETTING_SUCCESS  wsData =>', wsData);

      if (get(wsData, 'optionName') === 'casting') {
        yield put(MirrorActions.onMirrorDisconnectByUser());
      }
      break;
    }

    default: {
      if (wsEvent !== MirrorEvents.SET_WIFI_PASS_SUCCESS) {
        yield put(AppAction.onLoading(false));
      }
      break;
    }
  }
}

export function* resetHandler() {
  console.log('resetHandler');
  yield put(MdnsAction.onMdnsStop());
  yield put(WsAction.onWsDisconnect());
  yield put(MirrorAction.onMirrorReset());
  yield put(UserAction.onUserReset());

  requestAnimationFrame(Actions.IntroScreen);
}
export function* autoConnectLastHeartRateDevice() {
  const pairedDevice = yield select((state) => state.mirror.blePairedHeartRateDevice);
  if (!isEmpty(pairedDevice) && pairedDevice.id) {
    yield put(BleAction.onForgetDevices());
    yield put(BleAction.onScan([BleServices.HEAR_RATE_SERVICE_GUID]));
    yield put(
      MirrorAction.onMirrorLog(
        `HeartRate: last device found, localName is "${pairedDevice.localName}", id is "${
          pairedDevice.id
        }".`,
      ),
    );
    for (let i = 0; i < 3; i++) {
      yield put(
        MirrorAction.onMirrorLog(
          `HeartRate: Trying to auto connect last device...(${i + 1})`,
        ),
      );
      const timerChannel = yield call(() =>
        eventChannel((emit) => {
          const timer = setTimeout(() => {
            emit('timeout');
          }, 5 * 1000);
          return () => {
            clearTimeout(timer);
          };
        }),
      );
      const { deviceFound, timeoutEvent } = yield race({
        deviceFound: take(BleDeviceTypes.ON_DEVICE_FOUND),
        timeoutEvent: take(timerChannel),
      });
      console.log('deviceFound=>', deviceFound);
      const routeName = yield select((state) => state.appRoute.routeName);
      if (routeName === 'HeartRateScreen' || routeName === 'HeartRateModal') {
        yield put(
          MirrorAction.onMirrorLog(
            `HeartRate: User enter HeartRate device screens, so gave up auto connect.`,
          ),
        );
        yield put(BleAction.onStopScan());
        break;
      }
      if (has(deviceFound, 'device') && deviceFound.device.id === pairedDevice.id) {
        yield put(
          MirrorAction.onMirrorLog(
            `HeartRate: Last device found, connecting to ${deviceFound.device.name}...`,
          ),
        );
        yield put(
          MirrorAction.onMirrorLog(
            `HeartRate: Last device connected success, start to monitoring and stop scanning.`,
          ),
        );
        yield put(BleAction.onConnect(deviceFound.device));
        break;
      }
      if (timeoutEvent) {
        if (has(deviceFound, 'device')) {
          yield put(BleAction.onConnect(deviceFound.device));
        }
        if (i >= 2) {
          yield put(
            MirrorAction.onMirrorLog(
              `HeartRate: no device founded, stop scanning and auto connect.`,
            ),
          );
        }
        continue;
      }
    }
    yield put(BleAction.onStopScan());
  }
}

export function* handleSendHeartRateDate(deviceObj) {
  const bleChannel = yield actionChannel([
    BleDeviceTypes.ON_CHARACTERISTIC_UPDATE,
  ]);
  yield put(MirrorAction.onMirrorLog(`Monitor ble device id: ${deviceObj.id}`));
  yield put(MirrorAction.onMirrorPairHeartRateDeviceByPhone(deviceObj));
  try {
    for (;;) {
      const action = yield take(bleChannel);
      if (action.type === BleDeviceTypes.ON_CHARACTERISTIC_UPDATE) {
        if (__DEV__) {
          yield put(MirrorAction.onMirrorLog(`Ble ON_CHARACTERISTIC_UPDATE detected.`));
        }
        const data = yield select((state) => state.bleDevice.data);
        if (data && data.heartRate) {
          if (__DEV__) {
            yield put(
              MirrorAction.onMirrorLog(
                `Heart rate device notify received: "${data.heartRate}"`,
              ),
            );
          }

          let calories = 0;
          if (data.heartRate > 0)
            calories = Calories.calculate({
              heartRate: data.heartRate,
            });

          if (calories < 0) calories = 0;
          calories = calories / 60;

          yield put(
            WsAction.onWsSendMessage({
              event: MirrorEvents.UPDATE_HEARTBEAT_DATA,
              data: {
                value: data.heartRate,
                calories,
              },
            }),
          );
        }
      }
      yield delay(1000);
    }
  } catch (e) {
    console.error('handleSendHeartRateDate', e);
  } finally {
    if (yield cancelled()) {
      bleChannel.close();
    }
  }
}

export function* setCarouselDuration() {
  const activeEvent = yield select((state) => state.mirror.activeEvent);
  const isConnected = yield select((state) => state.websocket.isConnected);
  const isLoading = yield select((state) => state.appState.isLoading);
  const currentDuration = yield select((state) => state.mirror.currentDuration);
  if (activeEvent === 'play' && isConnected && !isLoading) {
    yield put(MirrorAction.setCurrentPosition(currentDuration + 1));
  }
}

export default {
  connectAndMonitoring,
  resetHandler,
  setCarouselDuration,
};
