import { call, put, take, delay } from 'redux-saga/effects';
import MirrorAction from 'App/Stores/Mirror/Actions';
import MdnsAction, { MdnsTypes } from 'App/Stores/Mdns/Actions';

export function* mirrorDiscoverHandler({ protocol, uriPrefix, domain, onSuccess } = {}) {
  yield put(MdnsAction.onMdnsScanClear());

  yield delay(1000);

  yield put(MdnsAction.onMdnsScan(uriPrefix, protocol, domain));
  for (;;) {
    const action = yield take(MdnsTypes.ON_MDNS_SCAN_RESOLVED);
    const isHostValid = !action.service.host.startsWith(':');
    const isMirror =
      action.service.name && action.service.name.toLowerCase().startsWith('mirror-');

    if (isHostValid && isMirror) {
      yield put(MdnsAction.onMdnsStop());
      yield put(MirrorAction.onMirrorDiscovered(action.service));
      if (typeof onSuccess === 'function') {
        yield call(onSuccess, action.service);
      }
      break;
    }
  }
}
