import { put, delay, select, call } from 'redux-saga/effects';
import { AppStateActions, UserActions, MirrorActions } from 'App/Stores';
import { Config } from 'App/Config';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { Actions } from 'react-native-router-flux';
export function* handleAutomaticExpiration() {
  let isLoading;

  const seconds = Config.API_TIMEOUT / 1000;
  for (let i = 0; i < seconds + 1; i++) {
    yield delay(1000);
    isLoading = yield select((state) => state.appState.isLoading);
    if (!isLoading) {
      break;
    }
  }
  if (isLoading) {
    yield put(AppStateActions.onLoading(false));
  }
}
export function* onAppInit() {
  let appEnv = Config.DEFAULT_SERVER_ENV;
  console.log('=== appEnv ===', appEnv);
  yield put(AppStateActions.onEnvChange(appEnv));
}
export function* doEnvChange(env) {
  console.log('=== doEnvChange ===', env.env);
  yield put(AppStateActions.onLoading(true));
  try {
    yield put(
      MirrorActions.onMirrorCommunicate(MirrorEvents.ENGINEERING_MODE, {
        action: 'set-device-params',
        env: env.env,
      }),
    );
    Actions.replace('UserLoginScreen');
  } catch (e) {
    console.log(e.stack);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}
