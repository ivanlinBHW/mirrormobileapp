import { put, call, delay, select } from 'redux-saga/effects';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { get, has, first, isArray } from 'lodash';

import {
  ClassActions,
  ProgramActions,
  UserActions,
  PlayerActions,
  LiveActions,
  AppStateActions,
  MirrorActions,
  SeeAllActions,
  CommunityActions,
} from 'App/Stores';
import { Handler, Class } from 'App/Api';
import { Dialog } from 'App/Helpers';
import { translate as t } from 'App/Helpers/I18n';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';

import * as MirrorCalorieSaga from 'App/Sagas/Mirror/MirrorCalorieStatusSaga';
export function* getClassList(workoutTypeIds) {
  yield put(AppStateActions.onLoading(true, '', { hide: true }));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Class.getClassList(workoutTypeIds.params),
    );

    const list = yield select((state) => state.class.list);
    if (list != null && list.hottest != null) {
      yield put(
        ClassActions.getClassListSuccess({
          bookedCount: 0,
          bookedLive: [],
          bookmarkCount: 0,
          bookmarked: { liveClasses: [], trainingClasses: [] },
          recommended: [],
          upcomingLive: [],
          currentHottest: list.hottest,
        }),
      );
    }

    if (res.success) {
      yield put(ClassActions.getClassListSuccess(res.data));
    }
  } catch (err) {
    console.log('err', err);
  } finally {
  }
}

export function* getClassDetail({ id, routeName: entryRouteName }) {
  yield put(AppStateActions.onLoading(true));
  yield put(
    PlayerActions.updatePlayerStore({
      isMotionCaptureEnabled: false,
    }),
  );
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Class.getClassDetail({ id }),
    );
    if (res.success) {
      yield put(ClassActions.getClassDetailSuccess(res.data.trainingClass));
      yield put(PlayerActions.setRouteName(entryRouteName));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
          classId: id,
          classType: 'on-demand',
        }),
      );
      const currentRoute = yield select((state) => state.appRoute.routeName);

      if (entryRouteName === 'ClassDetail') {
        if (currentRoute === 'ClassScreen') {
          Actions.ClassDetail();
        } else if (entryRouteName === 'ChannelClassDetailScreen') {
          if (currentRoute === 'Channel') {
            Actions.ChannelClassDetailScreen();
          }
        }
      } else if (entryRouteName === 'CollectionScreen') {
        if (currentRoute === 'CollectionDetail') {
          Actions.ClassDetail();
        }
      } else {
        Actions.ClassDetail();
      }
    }
  } catch (error) {
    console.log('error.status =>', error.status);
    yield put(ClassActions.getClassList());
    if (error.status === 404) {
      Alert.alert(t('alert_title_oops'), t('class_404'));
    }
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* exitDistanceMeasuringModal({ id }) {
  yield put(AppStateActions.onLoading(true));
  try {
    yield put(
      MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
        classId: id,
        classType: 'on-demand',
      }),
    );
    Actions.ClassDetail();
  } catch (error) {
    console.log('error.status =>', error.status);
  } finally {
  }
}

export function* onlyGetClassDetail({ id, routeName }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Class.getClassDetail({ id }),
    );
    if (res.success) {
      if (routeName) {
        yield put(PlayerActions.setRouteName(routeName));
      }
      yield put(ClassActions.getClassDetailSuccess(res.data.trainingClass));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyGetClassHistoryDetail({ id, historyId }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Class.getHistoryDetail({ id, historyId }),
    );
    console.log('res =>', res);
    if (res.success) {
      yield put(PlayerActions.setRouteName('ClassDetail'));
      yield put(ClassActions.getClassDetailSuccess(res.data.trainingClass));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyClassSummaryGetClassDetail({ id, routeName, historyId }) {
  try {
    const token = yield select((state) => state.user.token);
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Class.getClassHistory({ id, historyId }),
    );
    if (res.success) {
      console.log('res.data.trainingClass', res.data.trainingClass);
      yield put(PlayerActions.setRouteName(routeName));
      yield put(ClassActions.getClassDetailSuccess(res.data.trainingClass));
      if (
        has(res.data.trainingClass, 'trainingClassHistory') &&
        res.data.trainingClass.trainingClassHistory.status !== 2
      ) {
        yield put(
          ClassActions.finishClass(res.data.trainingClass.trainingClassHistory.id, token),
        );
      }
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* startClass({ id, token, routeName = 'ClassDetail', options }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Class.startClass({ id }),
    );
    if (res.success) {
      yield put(PlayerActions.setRouteName(routeName));
      yield put(ClassActions.startClassSuccess(res.data));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.START_CLASS, {
          classId: id,
          trainingClassHistoryId: res.data.trainingClassHistory.id,
          ...options,
        }),
      );
    }
  } catch (err) {
    console.log(err);
    yield put(AppStateActions.onLoading(false));
    if (err.status === 404) {
      Alert.alert(t('alert_title_oops'), t('class_404'));
    } else if (
      err.status !== 501 &&
      err.status !== 502 &&
      err.status !== 503 &&
      err.status !== 504
    ) {
      Dialog.showApiExceptionAlert(err);
    }
  } finally {
    if (!get(options, 'start-measure-distance')) {
      yield put(AppStateActions.onLoading(false));
    }
  }
}

export function* finishClass({ id }) {
  yield put(AppStateActions.onLoading(true));
  try {

    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Class.finishClass({ id }),
    );

    console.log('finishClass res.data=>', res.data);
    if (res.success) {
      yield put(
        MirrorActions.onMirrorLog(
          `Mirror: finishClass success @ ${new Date().getTime()}`,
        ),
      );
      yield put(ClassActions.setTrainingClassHistory(res.data.trainingClassHistory));
      yield put(MirrorActions.resetThumbnail());
      yield call(requestAnimationFrame, Actions.SaveActivityScreen);
    }
  } catch (err) {
    if (
      err.status !== 501 &&
      err.status !== 502 &&
      err.status !== 503 &&
      err.status !== 504
    ) {
      Dialog.showApiExceptionAlert(err);
    }
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* pauseClass({ id, stepId, detailId }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const currentDuration = yield select((state) => state.mirror.currentDuration);

    const { trainingSteps } = yield select((state) => state.player.detail);
    if (!stepId && isArray(trainingSteps)) {
      stepId = first(trainingSteps) ? first(trainingSteps).id : null;
    }
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: { pausedStepId: stepId, duration: currentDuration },
      }),
      Class.pauseClass({ id }),
    );
    if (res.success) {
      yield put(ClassActions.setTrainingClassHistory(res.data.trainingClassHistory));
    }
    const routeName = yield select((state) => state.player.routeName);
    console.log('=== pauseClass routeName ===', routeName);
    yield call(requestAnimationFrame, Actions.popTo(routeName));
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* resumeClass({ id, detailId, options }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Class.resumeClass({ id }),
    );
    if (res.success) {

      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.START_CLASS, {
          classId: detailId,
          trainingClassHistoryId: id,
          ...options,
        }),
      );
    }
  } catch (err) {
    console.log(err);
  } finally {
    if (!get(options, 'start-measure-distance')) {
      yield put(AppStateActions.onLoading(false));
    }
  }
}

export function* stashStepId({ id, stepId }) {
  try {
    const token = yield select((state) => state.user.token);

    yield call(
      Handler.put({ Authorization: token, data: { pausedStepId: stepId } }),
      Class.stashStep({ trainingClassHistoryId: id }),
    );
  } catch (err) {
    console.log(err);
  } finally {
  }
}

export function* submitFeedback({
  id,
  payload,
  routeName,
  isProgramFinished,
  detailId,
  programId = null,
}) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token, data: payload }),
      Class.submitFeedback({ id }),
    );
    const eventId = yield select((state) => state.community.eventDetail.id);
    if (res.success) {
      yield call(onlyGetClassDetail, { id: detailId });

      if (isProgramFinished) {
        yield put(ProgramActions.resetIsFinishedProgram());
        yield put(ProgramActions.getProgramList(token));

        Actions.ProgramAchievementScreen();
      } else {
        if (routeName === 'ProgramClassDetail') {
          yield put(ProgramActions.onlyGetProgramDetail(programId, token));
          Actions.ProgramClassDetailScreen();
        } else if (routeName === 'EventClassDetail') {
          yield put(CommunityActions.onlyGetTrainingEvent(eventId));
          Actions.popTo('CommunityDetailClassDetailScreen');
        } else if (routeName === 'ChannelClassDetailScreen') {
          Actions.popTo('ChannelClassDetailScreen');
        } else {
          Actions.popTo('ClassDetail');
        }
      }
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* setClassBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Class.setBookmark({ id }),
    );

    if (res.success) {
      yield put(ClassActions.setClassBookmarkSuccess(id));
      yield put(ProgramActions.setProgramBookmarkSuccess(id));
      yield put(SeeAllActions.setBookmark(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* removeClassBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      Class.deleteBookmark({ id }),
    );

    if (res.success) {
      yield put(ClassActions.removeClassBookmarkSuccess(id));
      yield put(ProgramActions.removeProgramBookmarkSuccess(id));
      yield put(SeeAllActions.removeBookmark(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* setClassLiveBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Class.setLiveBookmark({ id }),
    );
    console.log('setClassLiveBookmark id =>', id);

    if (res.success) {
      yield put(ClassActions.setClassBookmarkSuccess(id));
      yield put(LiveActions.setLiveBookmarkSuccess(id));
      yield put(SeeAllActions.setBookmark(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* removeClassLiveBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      Class.deleteLiveBookmark({ id }),
    );

    if (res.success) {
      yield put(ClassActions.removeClassBookmarkSuccess(id));
      yield put(LiveActions.removeLiveBookmarkSuccess(id));
      yield put(SeeAllActions.removeBookmark(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* sendClassReaction({ id, reaction }) {
  try {
    const token = yield select((state) => state.user.token);

    yield call(
      Handler.post({
        Authorization: token,
        data: {
          reaction,
        },
      }),
      Class.sendReaction({ id }),
    );
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* isDoLastEvent({ id, routeName }) {
  const connectSuccessResult = yield select((state) => state.mirror.connectSuccessResult);
  const token = yield select((state) => state.user.token);
  console.log('connectSuccessResult =>', connectSuccessResult);
}
