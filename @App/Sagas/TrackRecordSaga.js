import { put, call, select } from 'redux-saga/effects';

import { TrackRecordActions } from 'App/Stores';
import { Handler, TrackRecord } from 'App/Api';
import { Dialog } from 'App/Helpers';
import { Config } from 'App/Config';

export function* track({ trackObjectType, trackObjectId, trackActionType }) {
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      TrackRecord.track({ trackObjectType, trackObjectId, trackActionType }),
    );
  } catch (err) {
    console.error('track error=>', err);
  }
}
