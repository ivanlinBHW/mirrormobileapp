import { eventChannel } from 'redux-saga';
import { AppState, Platform } from 'react-native';
import { select, take, call, put, fork, cancelled } from 'redux-saga/effects';
import ReconnectingWebSocket from 'reconnecting-websocket';

import Actions from 'App/Stores/Websocket/Actions';
import { WebsocketTypes as Types } from 'App/Stores/Websocket/Actions';
import { Config } from 'App/Config';
import { store } from 'App/App';

let pingPongTimer = null;
let disconnectTimer = null;

function heartbeat(ws, emitter) {
  try {
    if (pingPongTimer) {
      clearTimeout(pingPongTimer);
      pingPongTimer = null;
    }

    if (disconnectTimer) {
      clearTimeout(disconnectTimer);
      disconnectTimer = null;
    }

    pingPongTimer = setTimeout(() => {
      try {
        if (ws.readyState === ws.OPEN) {
          if (__DEV__) {
            const d = new Date();
            const now = d.toISOString();
          }
          ws.send('pong');

          disconnectTimer = setTimeout(() => {
            try {
              if (
                disconnectTimer &&
                AppState.currentState === 'active' &&
                ws.readyState === ws.OPEN
              ) {
                emitter(Actions.onWsClosed({ code: 1001 }));
                __DEV__ &&
                  console.warn('WEBSOCKET TIMEOUT, ws.readyState=>', ws.readyState);
              }
            } catch (e) {
              throw e;
            }
          }, 12 * 1000);
        }
      } catch (e) {
        throw e;
      }
    }, 2 * 1000);
  } catch (error) {
    console.error(error);
  }
}

const createWebsocketChannel = (ws) =>
  eventChannel((emitter) => {
    ws.addEventListener('message', (message) => {
      if (message.data === 'ping') {
        heartbeat(ws, emitter);
      } else {
        emitter(Actions.onWsMessageReceived(message.data));
      }
    });

    ws.addEventListener('open', () => {
      emitter(Actions.onWsOpened());
      clearTimeout(disconnectTimer);
      ws.send('ping');
    });

    ws.addEventListener('error', (err) => {
      emitter(Actions.onWsError(err.message ? err.message : err));
    });

    ws.addEventListener('close', (e) => {
      emitter(Actions.onWsClosed(e));
    });

    return () => ws && ws.close();
  });

class MyWebSocket extends WebSocket {
  constructor(url, protocol) {
    const {
      WEBSOCKET_SSL_CA: ca = '',
      WEBSOCKET_SSL_PFX: pfx = '',
      WEBSOCKET_SSL_PASSPHRASE: passphrase = '',
    } = Config;
    let mCa = ca,
      mPfx = pfx,
      mPassphrase = passphrase;
    const websocketOption = {
      ca: mCa,
      pfx: mPfx,
      passphrase: mPassphrase,
    };
    if (!mCa) {
      websocketOption.ca = '';
    }
    if (!mPfx) {
      websocketOption.pfx = '';
    }
    if (!mPassphrase) {
      websocketOption.passphrase = '';
    }
    super(url, protocol, websocketOption);
  }
}

function* websocketManager({
  url,
  protocols = [],
  options = {
    maxRetries: 5,
    maxEnqueuedMessages: 1,
    debug: __DEV__,
  },
}) {
  let ws = null;
  let wsChannel = null;
  try {
    ws = new ReconnectingWebSocket(url, protocols, {
      ...options,
      maxEnqueuedMessages: 1,
      WebSocket: MyWebSocket,
    });
    yield fork(websocketMessageSender, ws);
    yield fork(websocketDisconnect, ws);
    wsChannel = yield call(createWebsocketChannel, ws);

    while (true) {
      const action = yield take(wsChannel);
      yield put(action);

      const retryCount = yield select((state) => state.websocket.retryCount);
      if (retryCount !== ws.retryCount) {
        yield put(Actions.onWsUpdateRetryCount(ws.retryCount));
      }
    }
  } catch (error) {
    console.log('websocketManager error=>', error);
  } finally {
    if (yield cancelled()) {
      wsChannel.close();
      ws.close();
    }
  }
}

export function* initialize({ url, protocols, options }) {
  yield fork(websocketManager, { url, protocols, options });
}

export function* messageReceived({ message }) {
}

function* websocketMessageSender(ws) {
  while (true) {
    const action = yield take(Types.ON_WS_SEND_MESSAGE);
    if (action) {
      const message = JSON.stringify(action.payload);
      ws.send(message);
    }
  }
}

export function* websocketDisconnect(ws) {
  while (true) {
    const action = yield take(Types.CLOSE_CONNECTION);
    if (action) {
      const { code, reason } = action;
      ws && ws.close && ws.close(code, reason);
    }
  }
}
