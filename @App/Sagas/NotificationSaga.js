import { put, call, select } from 'redux-saga/effects';
import NotificationActions from 'App/Stores/Notification/Actions';
import AppActions from 'App/Stores/AppState/Actions';
import { Handler, Notification } from 'App/Api';

export function* getNotificationList() {
  try {
    const token = yield select((state) => state.user.token);
    yield put(AppActions.onLoading(true));

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Notification.getNotificationList(),
    );

    if (res.success) {
      yield put(NotificationActions.getNotificationListSuccess(res.data.notifications));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getNotificationStatus() {
  const token = yield select((state) => state.user.token);
  try {
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Notification.getNotificationStatus(),
    );
    if (res.success) {
      yield put(NotificationActions.getNotificationStatusSuccess(res.data.hasUnread));
    }
  } catch (err) {
    console.log(err);
  } finally {
  }
}
