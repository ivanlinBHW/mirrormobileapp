// @flow

import { put, call } from 'redux-saga/effects';
import { BleErrorCode } from 'react-native-ble-plx';
import Actions from 'App/Stores/BleDevice/Actions';

export const SensorTagTests = {
  READ_ALL_CHARACTERISTICS: {
    id: 'READ_ALL_CHARACTERISTICS',
    title: 'Read all characteristics',
    execute: readAllCharacteristics,
  },
  READ_TEMPERATURE: {
    id: 'READ_TEMPERATURE',
    title: 'Read temperature',
    execute: readTemperature,
  },
  READ_HEART_RATE: {
    id: 'READ_HEART_RATE',
    title: 'Read heartbeat rate',
    execute: readHeartbeat,
  },
};

function* readAllCharacteristics(device) {
  try {
    const services = yield call([device, device.services]);
    for (const service of services) {
      yield put(Actions.onLog('Found service: ' + service.uuid));

      const characteristics = yield call([service, service.characteristics]);
      for (const characteristic of characteristics) {
        yield put(Actions.onLog('Found characteristic: ' + characteristic.uuid));

        if (characteristic.uuid === '00002a02-0000-1000-8000-00805f9b34fb') continue;

        const descriptors = yield call([characteristic, characteristic.descriptors]);

        for (const descriptor of descriptors) {
          yield put(Actions.onLog('* Found descriptor: ' + descriptor.uuid));
          const d = yield call([descriptor, descriptor.read]);
          yield put(Actions.onLog('Descriptor value: ' + (d.value || 'null')));
          if (d.uuid === '00002902-0000-1000-8000-00805f9b34fb') {
            yield put(Actions.onLog('Skipping CCC'));
            continue;
          }
          try {
            yield call([descriptor, descriptor.write], 'AAA=');
          } catch (error) {
            const bleError = error;
            if (bleError.errorCode === BleErrorCode.DescriptorWriteFailed) {
              yield put(Actions.onLog('Cannot write to: ' + d.uuid));
            } else {
              throw error;
            }
          }
        }

        yield put(Actions.onLog('Found characteristic: ' + characteristic.uuid));
        if (characteristic.isReadable) {
          yield put(Actions.onLog('Reading value...'));
          var c = yield call([characteristic, characteristic.read]);
          yield put(Actions.onLog('Got base64 value: ' + c.value));
          if (characteristic.isWritableWithResponse) {
            yield call([characteristic, characteristic.writeWithResponse], c.value);
            yield put(Actions.onLog('Successfully written value back'));
          }
        }
      }
    }
  } catch (error) {
    yield put(Actions.onLogError(error));
    return false;
  }

  return true;
}

function* readTemperature(device) {
  yield put(Actions.onLog('Read temperature'));
  return false;
}

export function readHeartbeat(device, bleManager, emit) {
  const HEAR_RATE_SERVICE_GUID = '180D';
  const HEART_RATE_MEASUREMENT_CHARACTERISTIC_GUID = '2A37';

  return bleManager.monitorCharacteristicForDevice(
    device.id,
    HEAR_RATE_SERVICE_GUID,
    HEART_RATE_MEASUREMENT_CHARACTERISTIC_GUID,
    (error, characteristic) => {
      if (error) {
        return emit({ error });
      }

      if (characteristic && characteristic.value) {
        let heartRate = -1;
        let decoded = Buffer.from(characteristic.value, 'base64');
        let firstBitValue = decoded.readInt8(0) & 0x01;
        if (firstBitValue === 0) {
          heartRate = decoded.readUInt8(1);
        } else {
          heartRate = (decoded.readInt8(1) << 8) + decoded.readInt8(2);
        }
        emit({ value: heartRate });
      }
    },
  );
}

export default {
  SensorTagTests,
};
