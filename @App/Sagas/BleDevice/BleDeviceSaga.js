// @flow
import { Platform, Alert } from 'react-native';
import { buffers, eventChannel } from 'redux-saga';
import {
  fork,
  put,
  call,
  take,
  race,
  cancel,
  cancelled,
  select,
  delay,
  actionChannel,
} from 'redux-saga/effects';
import { BleManager, State, LogLevel } from 'react-native-ble-plx';
import { Buffer } from 'buffer';
import { Permissions } from 'react-native-unimodules';

import Actions from 'App/Stores/BleDevice/Actions';
import { BleDeviceTypes as Types } from 'App/Stores/BleDevice/Actions';
import { getScanUuidFilter, getScannedDevices } from 'App/Stores/BleDevice/Selectors';
import { ConnectionState } from 'App/Stores/BleDevice/InitialState';
import { SensorTagTests } from './BleDeviceTestSaga';

let managerInstance = null;

export function* bleManager() {
  yield put(Actions.onLog('BLE connection manager started...'));
  managerInstance = new BleManager();
  managerInstance.setLogLevel(LogLevel.Verbose);
  yield fork(handleBleState, managerInstance);
  yield fork(handleConnection, managerInstance);
  yield fork(handleScanning, managerInstance);
}
function* handleBleState(manager) {
  const stateChannel = yield eventChannel((emit) => {
    const subscription = manager.onStateChange((state) => {
      emit(state);
    }, true);
    return () => {
      subscription.remove();
    };
  }, buffers.expanding(1));

  try {
    for (;;) {
      const newState = yield take(stateChannel);
      yield put(Actions.onBleStateUpdated(newState));
    }
  } finally {
    if (yield cancelled()) {
      stateChannel.close();
    }
  }
}
function* handleScanning(manager) {
  let scanTask = null;
  let shouldScanning = false;
  let bleState = State.Unknown;
  let connectionState;

  const channel = yield actionChannel([
    Types.ON_SCAN,
    Types.ON_STOP_SCAN,
    Types.ON_BLE_STATE_UPDATED,
    Types.ON_UPDATE_CONNECTION_STATE,
  ]);

  for (;;) {
    const action = yield take(channel);

    switch (action.type) {
      case Types.ON_BLE_STATE_UPDATED:
        bleState = action.state;
        break;
      case Types.ON_UPDATE_CONNECTION_STATE:
        connectionState = action.state;
        break;
      case Types.ON_SCAN:
        shouldScanning = true;
        console.log('Types.ON_SCAN');
        break;
      case Types.ON_STOP_SCAN:
        shouldScanning = false;
        break;
    }

    yield delay(200);

    const enableScanning = shouldScanning && bleState === State.PoweredOn;

    console.log('shouldScanning=>', enableScanning);
    if (enableScanning) {
      yield put(Actions.onLog('Scanning started...'));
      if (scanTask != null) {
        yield cancel(scanTask);
      }
      const uuids = yield select((state) => {
        return state.bleDevice.lastFilterUuid;
      });
      const isManual = yield select((state) => {
        return state.bleDevice.isManual;
      });
      scanTask = yield fork(scan, manager, uuids, isManual);
    } else {
      yield put(Actions.onLog('Scanning stopped...'));
      if (scanTask != null) {
        yield cancel(scanTask);
        scanTask = null;
      }
    }
  }
}
function* scan(manager, uuids, isManual = false) {
  console.log('uuids === >', uuids);
  if (Platform.OS === 'android' && Platform.Version >= 23) {
    yield put(Actions.onLog('Scanning: Checking permissions...'));
    let { status } = yield call(Permissions.getAsync, Permissions.LOCATION);

    if (status !== 'granted') {
      yield put(Actions.onLog('Scanning: Permissions disabled, showing...'));
      const { status: renewStatus } = yield call(
        Permissions.askAsync,
        Permissions.LOCATION,
      );
      if (renewStatus !== 'granted') {
        yield put(Actions.onLog('Scanning: Permissions not granted, aborting...'));
        yield call(
          Alert.alert,
          'Get Permissions failed',
          'Please give the app location permission in order to access BLE devices.',
        );
        return;
      }
    }
    yield put(Actions.onLog('Scanning: Permissions are good.'));
  }

  const scanningChannel = yield eventChannel((emit) => {
    manager.startDeviceScan(uuids, { allowDuplicates: true }, (error, scannedDevice) => {
      if (error) {
        emit([error, scannedDevice]);
        return;
      }
      if (scannedDevice != null) {
        emit([error, scannedDevice]);
      }
    });
    return () => {
      manager.stopDeviceScan();
    };
  }, buffers.expanding(1));

  let scannedDevices = [];
  let isActiveDeviceExists = false;
  try {
    let activeDevice = yield select((state) => state.bleDevice.activeDevice);

    for (;;) {
      const [error, scannedDevice] = yield take(scanningChannel);
      if (error != null) {
        throw Error(error);
      }
      console.log('scannedDevice=>', scannedDevice);
      if (scannedDevice != null) {
        scannedDevices = yield select((state) => {
          return state.bleDevice.searchDevices;
        });
        if (
          isActiveDeviceExists === false &&
          activeDevice != null &&
          scannedDevice != null
        ) {
          isActiveDeviceExists = scannedDevice.id === activeDevice.id;
        }

        const isDeviceExists = scannedDevices.some(
          (e) => scannedDevice && e && e.id === scannedDevice.id,
        );
        if (!isDeviceExists) {
          yield put(Actions.onDeviceFound(scannedDevice));
        }
      }
      yield delay(1000);
    }
  } catch (error) {
  } finally {
    if (isManual && isActiveDeviceExists === false) {
      yield put(Actions.onDisconnect());
    }
    if (yield cancelled()) {
      scanningChannel.close();
    }
  }
}

function* handleConnection(manager) {
  let testTask = null;
  let monitorTask = null;
  let writeTask = null;
  const connectActionChannel = yield actionChannel([
    Types.ON_CONNECT_BY_ID,
    Types.ON_CONNECT,
  ]);
  for (;;) {
    const { device } = yield take(connectActionChannel);

    let disconnectedChannel;
    const deviceActionChannel = yield actionChannel([
      Types.ON_DISCONNECT,
      Types.ON_EXECUTE_TEST,
      Types.ON_CHARACTERISTIC_UPDATE,
      Types.ON_BLE_MONITOR_CHARACTERISTIC,
      Types.ON_BLE_WRITE_CHARACTERISTIC,
    ]);

    try {
      disconnectedChannel = yield eventChannel((emit) => {
        const subscription = device.onDisconnected((error) => {
          emit({ type: ConnectionState.DISCONNECTED, error: error });
        });
        return () => {
          subscription && subscription.remove();
        };
      }, buffers.expanding(1));
      yield put(Actions.onUpdateConnectionState(ConnectionState.CONNECTING));
      yield call([device, device.connect]);
      yield put(Actions.onUpdateConnectionState(ConnectionState.DISCOVERING));
      yield call([device, device.discoverAllServicesAndCharacteristics]);
      yield put(Actions.onUpdateConnectionState(ConnectionState.CONNECTED));

      for (;;) {
        const { deviceAction, disconnected } = yield race({
          deviceAction: take(deviceActionChannel),
          disconnected: take(disconnectedChannel),
        });
        if (deviceAction) {
          if (deviceAction.type === Types.ON_DISCONNECT) {
            yield put(Actions.onLog('Disconnected by user...'));
            yield put(Actions.onUpdateConnectionState(ConnectionState.DISCONNECTING));
            if (device) {
              yield call([device, device.cancelConnection]);
            }
            if (monitorTask) {
              yield cancel(monitorTask);
            }
            break;
          }
          switch (deviceAction.type) {
            case Types.ON_EXECUTE_TEST: {
              if (testTask !== null) {
                yield cancel(testTask);
              }
              testTask = yield fork(executeTest, device, deviceAction);
              break;
            }
            case Types.ON_BLE_MONITOR_CHARACTERISTIC: {
              if (monitorTask !== null) {
                yield cancel(monitorTask);
              }
              monitorTask = yield fork(monitorCharacteristic, device, deviceAction);
              break;
            }
            case Types.ON_BLE_WRITE_CHARACTERISTIC: {
              if (writeTask !== null) {
                yield cancel(writeTask);
              }
              const { serviceUuid, characteristicUuid, data } = deviceAction;
              const byteArray = `${JSON.stringify(data)}`
                .split('')
                .map((char) => char.charCodeAt(0));
              console.log('serviceUuid=>', serviceUuid);
              console.log('characteristicUuid=>', characteristicUuid);
              console.log('byteArray=>', byteArray);
              yield call(
                [
                  device,
                  device.writeCharacteristicWithResponseForService,
                  serviceUuid,
                  characteristicUuid,
                  Buffer.from(byteArray).toString('base64'),
                ],
                serviceUuid,
                characteristicUuid,
                Buffer.from(byteArray).toString('base64'),
              );
              break;
            }
          }
        } else if (disconnected) {
          yield put(Actions.onLog('Disconnected by device...'));
          yield put(Actions.onDisconnect());
          if (disconnected.error != null) {
            yield put(Actions.onLogError(disconnected.error));
          }
          if (monitorTask) {
            yield cancel(monitorTask);
          }
          break;
        }
      }
    } catch (error) {
      yield put(Actions.onLogError(error));
    } finally {
      disconnectedChannel && disconnectedChannel.close();
      yield put(Actions.onUpdateConnectionState(ConnectionState.DISCONNECTED));
    }
  }
}

function* executeTest(device, test) {
  yield put(Actions.onLog('Executing test: ' + test.id));
  const start = Date.now();
  const result = yield call(SensorTagTests[test.id].execute, device);
  if (result) {
    yield put(
      Actions.onLog('Test finished successfully! (' + (Date.now() - start) + ' ms)'),
    );
  } else {
    yield put(Actions.onLog('Test failed! (' + (Date.now() - start) + ' ms)'));
  }
  yield put(Actions.onTestFinished());
}

function* monitorCharacteristic(device, { serviceUuid, characteristicUuid }) {
  const monitorChannel = yield eventChannel((emit) => {
    console.log('monitorCharacteristic=>', serviceUuid, characteristicUuid);

    const subscription = device.monitorCharacteristicForService(
      serviceUuid,
      characteristicUuid,
      (error, characteristic) => {
        if (error) {
          emit([error, null]);
          return;
        }
        if (characteristic && characteristic.value) {
          console.log('characteristic.value=>', characteristic.value);
          let heartRate = -1;
          let decoded = Buffer.from(characteristic.value, 'base64');
          let firstBitValue = decoded.readInt8(0) & 0x01;
          if (firstBitValue == 0) {
            heartRate = decoded.readUInt8(1);
          } else {
            heartRate = (decoded.readInt8(1) < 8) + decoded.readInt8(2);
          }
          emit([null, heartRate]);
        }
      },
    );
    return () => {
      subscription.remove();
    };
  }, buffers.expanding(1));

  for (;;) {
    const [error, value] = yield take(monitorChannel);
    if (error) {
      yield put(Actions.onLogError(error));
    } else {
      yield put(Actions.onLog(`monitor new value change: ${value}`));
      yield put(Actions.onCharacteristicUpdate({ heartRate: value }));
    }
  }
}

export default {
  bleManager,
};
