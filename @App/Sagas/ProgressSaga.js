import { put, call, select } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import { Handler, Progress } from 'App/Api';
import { Date as d } from 'App/Helpers';

import {
  AppStateActions as AppActions,
  ProgressActions,
  ClassActions,
  ProgramActions,
  CollectionActions,
  LiveActions,
} from 'App/Stores';

export function* getAchievements() {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Progress.getAchievements(),
    );
    if (res.success) {
      yield put(ProgressActions.getAchievementsSuccess(res.data));
      yield call(requestAnimationFrame, Actions.AchievementListScreen);
    }
  } catch (e) {
    console.log(e);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getProgressDate({ start, end }) {
  yield put(AppActions.onLoading(true, '', { hide: true }));
  try {
    const token = yield select((state) => state.user.token);
    const currentTimeZone = yield select((state) => state.appState.currentTimeZone);
    console.log('start, end=>', start, end);

    console.log('currentTimeZone=>', currentTimeZone);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Progress.getProgressDate({
        start: d.toUTC(
          d.moment(`${start} 00:00:00`, 'YYYY/MM/DD HH:mm:ss'),
          d.FORMAT_YY_MM_DD_HH_MM_SS,
        ),
        end: d.toUTC(
          d.moment(`${end} 23:59:59`, 'YYYY/MM/DD HH:mm:ss'),
          d.FORMAT_YY_MM_DD_HH_MM_SS,
        ),
      }),
    );
    if (res.success) {
      yield put(ProgressActions.getProgressDateSuccess(res.data));
    }
  } catch (e) {
    console.log(e);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getProgressOverview({ start, end }) {
  yield put(AppActions.onLoading(true, '', { hide: true }));
  try {
    const token = yield select((state) => state.user.token);
    const currentTimeZone = yield select((state) => state.appState.currentTimeZone);

    console.log('start, end=>', start, end);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Progress.getProgressOverview({
        start: d.toUTC(
          d.moment(`${start} 00:00:00`, 'YYYY-MM-DD HH:mm:ss'),
          d.FORMAT_YY_MM_DD_HH_MM_SS,
        ),
        end: d.toUTC(
          d.moment(`${end} 23:59:59`, 'YYYY-MM-DD HH:mm:ss').add(1, 'days'),
          d.FORMAT_YY_MM_DD_HH_MM_SS,
        ),
      }),
    );
    if (res.success) {
      yield put(ProgressActions.getProgressOverviewSuccess(res.data));
    }
  } catch (e) {
    console.log(e);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getProgressDetail({ id, workoutType }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Progress.getProgressDetail({ id, workoutType }),
    );
    if (res.success) {
      if (workoutType === 'training-class') {
        yield put(
          ProgressActions.getProgressDetailSuccess(res.data.trainingClassHistory),
        );
      } else if (workoutType === 'live-class') {
        yield put(ProgressActions.getProgressDetailSuccess(res.data.liveClassHistory));
      }
      Actions.WorkoutDetailScreen({ workoutType });
    }
  } catch (e) {
    console.log(e);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* deleteClassHistory({ id, workoutType }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({ Authorization: token }),
      Progress.deleteClassHistory({ id, workoutType }),
    );
    if (res.success) {
      const start = yield select((state) => state.progress.startDate);
      const end = yield select((state) => state.progress.endDate);
      yield put(ProgressActions.getProgressOverview(start, end));

      const { startDate, endDate } = d.getMonthRangeByDate(start);
      yield put(ProgressActions.getProgressDate(startDate, endDate));

      yield put(ProgressActions.deleteClassHistorySuccess(id));
      Actions.popTo('Progress');
    }
  } catch (e) {
    console.log(e);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* setWorkoutDetailBookmark({ id, workoutType }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Progress.setBookmark({ id, workoutType }),
    );
    if (res.success) {
      yield put(ProgressActions.setWorkoutDetailBookmarkSuccess(res.data));
      if (workoutType === 'live-class') {
        yield put(LiveActions.setClassLiveBookmarkSuccess(id));
      } else {
        yield put(ClassActions.setClassBookmarkSuccess(id));
        yield put(ProgramActions.setProgramBookmarkSuccess(id));
        yield put(CollectionActions.setCollectionBookmarkSuccess(id));
      }
    }
  } catch (e) {
    console.log(e);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* deleteWorkoutDetailBookmark({ id, workoutType }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({ Authorization: token }),
      Progress.deleteBookmark({ id, workoutType }),
    );
    if (res.success) {
      yield put(ProgressActions.deleteWorkoutDetailBookmarkSuccess(res.data));
      if (workoutType === 'live-class') {
        yield put(LiveActions.removeClassLiveBookmarkSuccess(id));
      } else {
        yield put(ClassActions.removeClassBookmarkSuccess(id));
        yield put(ProgramActions.removeProgramBookmarkSuccess(id));
        yield put(CollectionActions.removeCollectionBookmarkSuccess(id));
      }
    }
  } catch (e) {
    console.log(e);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export default {
  getAchievements,
  getProgressDate,
  getProgressDetail,
  getProgressOverview,
  setWorkoutDetailBookmark,
  deleteWorkoutDetailBookmark,
  deleteClassHistory,
};
