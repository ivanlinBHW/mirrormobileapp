import { put, call, select, delay } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import InstructorActions from 'App/Stores/Instructor/Actions';
import AppActions from 'App/Stores/AppState/Actions';
import { Handler, Instructor } from 'App/Api';
export function* getInstructorDetail({ id }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Instructor.getInstructorDetail({ id }),
    );
    if (res.success) {
      yield put(InstructorActions.getInstructorSuccess(res.data.instructor));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* setInstructorId({ id }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    yield put(InstructorActions.getInstructor(id, token));
    yield put(InstructorActions.setInstructorIdSuccess(id));
    yield delay(1000);

    Actions.InstructorScreen();
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}
