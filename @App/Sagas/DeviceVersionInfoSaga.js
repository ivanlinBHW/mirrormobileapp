import { put, call, select } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import DeviceVersionInfoActions from 'App/Stores/DeviceVersionInfo/Actions';
import { Handler, DeviceVersionInfo } from 'App/Api';
export function* indexSaga() {
  console.log('=== call indexSaga ===', DeviceVersionInfo.index());
  try {
    const { data: res } = yield call(Handler.get({}), DeviceVersionInfo.index());
    if (res.success) {
      console.log('=== indexSaga result ===', res.data);
      let { items } = res.data;
      yield put(DeviceVersionInfoActions.getIndexSuccess(items));
    }
  } catch (err) {
    console.log(err);
  } finally {
  }
}
