import { put, delay, select } from 'redux-saga/effects';
import {
  UserActions,
  SettingActions,
  CommunityActions,
  SearchActions,
  ClassActions,
  ChannelActions,
  ProgramActions,
  NotificationActions,
  CollectionActions,
  MirrorActions,
} from 'App/Stores';

export function* pushActionWatcher(data) {
  const { routeName } = data;

  switch (routeName) {
    case 'ClassScreen': {
      yield delay(100);
      yield put(SettingActions.getGenre());
      yield put(ClassActions.getClassList());
      yield put(UserActions.getUserAccount());
      yield put(SearchActions.getSearchableData());
      yield put(NotificationActions.getNotificationStatus());
      yield put(CommunityActions.getCoverImages());
      break;
    }
    case 'Channel': {
      yield put(UserActions.getUserAccount());
      yield put(ChannelActions.getChannelList());
      break;
    }
    case 'Collection': {
      yield put(CollectionActions.getCollectionList());
      break;
    }
    case 'Program': {
      yield put(UserActions.getUserAccount());
      yield put(ProgramActions.getProgramList());
      break;
    }
    case 'CreateEventScreen': {
      console.log('enter CreateEventScreen');

      break;
    }
    case 'UserLoginScreen': {
      __DEV__ && console.log('enter UserLoginScreen');
      yield put(UserActions.resetErrorCode());
      break;
    }
    case 'ChannelDetailScreen': {
      __DEV__ && console.log('enter ChannelDetailScreen');
      const channelId = yield select((state) => state.channel.channelId);
      console.log('=== channelId ===', channelId);
      if (channelId != null && channelId != '')
        yield put(ChannelActions.getChannelDetail(channelId));

      break;
    }
    case 'EventSearchScreen': {
      __DEV__ && console.log('enter EventSearchScreen');
      const searchText = yield select((state) => state.community.searchText);
      yield put(CommunityActions.getSearchEvents(searchText));

      break;
    }
    case 'CommunityScreen': {
      yield put(CommunityActions.setSearchEventsText(''));

      break;
    }

    case 'MirrorCastScreen': {
      yield put(
        MirrorActions.updateMirrorStore({
          isCasting: false,
        }),
      );

      break;
    }
    default:
      console.log('pushActionWatcher routeName=>', routeName);
      break;
  }
}
