import { Actions } from 'react-native-router-flux';
import { put, call, delay, select } from 'redux-saga/effects';
import { UserActions, SettingActions, AppStateActions } from 'App/Stores';
import { Handler, Setting } from 'App/Api';
import FastImage from 'react-native-fast-image';

export function* getGenre() {
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Setting.genre(),
    );
    if (res.success && res.data) {
      yield put(SettingActions.getGenreSuccess(res.data.genres));
    }
  } catch (err) {
    console.log(err);
  } finally {
  }
}

export function* getSetting() {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Setting.setting(),
    );
    console.log('res.data=>', res.data);

    if (res.success) {
      yield put(UserActions.getUserAccountSuccess(res.data.user));
      yield put(SettingActions.getSettingSuccess(res.data.user));
    }
    yield getGenre();
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* updateUserTimezone({ timezone }) {
  try {
    const token = yield select((state) => state.user.token);
    yield call(
      Handler.patch({
        Authorization: token,
        data: { timezone },
      }),
      Setting.setting(),
    );
  } catch (err) {
    console.log(err);
  }
}

export function* updateSetting({ payload }) {
  try {
    const token = yield select((state) => state.user.token);

    const data = payload;

    const { data: res } = yield call(
      Handler.patch({
        Authorization: token,
        data,
      }),
      Setting.setting(),
    );

    if (res.success) {
      yield put(UserActions.getUserAccountSuccess(res.data.user));
      yield put(SettingActions.getSettingSuccess(res.data.user));
    }
  } catch (err) {
    console.log(err);
  } finally {
  }
}

export function* updateEquipment({ payload }) {
  try {
    const token = yield select((state) => state.user.token);
    yield put(AppStateActions.onLoading(true));
    console.log(payload);

    const { data: res } = yield call(
      Handler.put({ Authorization: token, data: payload }),
      Setting.updateEquipment(),
    );

    if (res.success) {
      yield put(SettingActions.updateEquipmentSuccess(res.data.equipments));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* updateAvatar({ img }) {
  try {
    const token = yield select((state) => state.user.token);
    const userId = yield select((state) => state.user.userId);
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Setting.updateAvatar(),
    );
    if (res.success) {
      const { formAttributes, formInputs } = res.data;
      const data = new FormData();
      data.append('acl', formInputs.acl);
      data.append('key', formInputs.key);
      data.append('policy', formInputs.policy);
      data.append('xAmzAlgorithm', formInputs.xAmzAlgorithm);
      data.append('xAmzCredential', formInputs.xAmzCredential);
      data.append('xAmzDate', formInputs.xAmzDate);
      data.append('xAmzSignature', formInputs.xAmzSignature);
      data.append('Content-Type', 'image/jpeg');
      data.append('file', {
        uri: img.uri,
        type: img.type,
        name: img.fileName,
      });
      yield put(SettingActions.resetAvatar());
      yield delay(1000);
      yield call(
        Handler.post({
          data,
          baseURL: formAttributes.action,
          headers: { 'Content-Type': formAttributes.enctype },
        }),
      );
      const { data: getAvatarRes } = yield call(
        Handler.get({ Authorization: token }),
        Setting.getAvatar({ user_id: userId }),
      );

      if (getAvatarRes.success) {
        const payload = yield select((state) => state.setting.setting);
        payload.avatar = getAvatarRes.data.fileUrl;
        yield call(updateSetting, { payload });
        yield call(getSetting);
      }
    }
  } catch (err) {
    console.error(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* fetchGetPrivacyPolicy({} = {}) {
  yield put(AppStateActions.onLoading(true));
  try {
    const { data: res } = yield call(Handler.get(), Setting.getPrivacyPolicy());
    console.log('fetchGetPrivacyPolicy res=>', res);
    if (res.success) {
      yield put(
        SettingActions.updateSettingStore({
          privacyPolicy: res.data,
        }),
      );
    }
  } catch (e) {
    console.log('fetchGetPrivacyPolicy error=>', e);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* fetchGetUsagePolicy({} = {}) {
  console.log('fetchGetUsagePolicy');
  yield put(AppStateActions.onLoading(true));
  try {
    const { data: res } = yield call(Handler.get(), Setting.getUsagePolicy());
    console.log('fetchGetUsagePolicy res=>', res);
    if (res.success) {
      yield put(
        SettingActions.updateSettingStore({
          usagePolicy: res.data,
        }),
      );
    }
  } catch (e) {
    console.log('fetchGetUsagePolicy error=>', e);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}
