import { get, uniqBy } from 'lodash';
import { put, call, select } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import AppActions from 'App/Stores/AppState/Actions';
import { ChannelActions, SeeAllActions, ClassActions, ProgramActions } from 'App/Stores';
import { Handler, Channel, Class } from 'App/Api';

import {
  filterMirrorHasFeatureOption,
  filterMirrorHasFeatureOptionData,
} from 'App/Stores/Mirror/Selectors';
import FreeTrialChannel from 'App/Stores/Channel/FreeTrialChannel';
export function* getChannelList() {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Channel.getChannelList(),
    );

    if (res.success) {
      let channels = [...res.data.items];
      const state = yield select((e) => e);
      const isConnected = yield select((e) => e.mirror.isConnected);

      const freeTrial = filterMirrorHasFeatureOption(state, 'freeTrial');
      if (isConnected && freeTrial) {
        FreeTrialChannel.ids = get(
          filterMirrorHasFeatureOptionData(state, 'freeTrial'),
          'ids',
          filterMirrorHasFeatureOptionData(state, 'freeTrial'),
        );
        console.log('FreeTrialChannel.ids=>', FreeTrialChannel.ids);
        channels = uniqBy([FreeTrialChannel, ...channels], 'id');
      }
      yield put(ChannelActions.getChannelListSuccess(channels));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getChannelDetail({ channelId, toDetailPage = false }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    yield put(ChannelActions.setChannelId(channelId));

    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Channel.getChannelDetail({ channelId }),
    );

    const routeName = yield select((state) => state.appRoute.routeName);

    if (res.success) {
      yield put(ChannelActions.getChannelDetailSuccess(res.data.items));
      if (toDetailPage && routeName === 'Channel') Actions.ChannelDetailScreen();
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getChannelSeeAll({ channelId, genreId }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Channel.getChannelSeeAll({ channelId, genreId }),
    );

    if (res.success) {
      yield put(SeeAllActions.setList(res.data.trainingClasses));
      Actions.SeeAllScreen({ isChannel: true, loginRoute: 'channel' });
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* setChannelBookmark({ groupId, id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Class.setBookmark({ id }),
    );

    if (res.success) {
      yield put(ClassActions.setClassBookmarkSuccess(id));
      yield put(ProgramActions.setProgramBookmarkSuccess(id));
      yield put(ChannelActions.setChannelBookmarkSuccess(groupId, id));
      yield put(SeeAllActions.setBookmark(id));
    }
  } catch (err) {
    console.log(err);
  }
}

export function* delChannelBookmark({ groupId, id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      Class.deleteBookmark({ id }),
    );

    if (res.success) {
      yield put(ClassActions.removeClassBookmarkSuccess(id));
      yield put(ProgramActions.removeProgramBookmarkSuccess(id));
      yield put(ChannelActions.delChannelBookmarkSuccess(groupId, id));
      yield put(SeeAllActions.removeBookmark(id));
    }
  } catch (err) {
    console.log(err);
  }
}
