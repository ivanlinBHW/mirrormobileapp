import publicIP from 'react-native-public-ip';
import { has, get, first } from 'lodash';
import { Alert, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { put, call, select, delay } from 'redux-saga/effects';

import {
  AppStore,
  AppStateActions as AppActions,
  ProgramActions,
  UserActions,
  MirrorActions,
  SettingActions,
} from 'App/Stores';
import { Config } from 'App/Config';
import { Handler, User } from 'App/Api';
import { translate as t } from 'App/Helpers/I18n';
import { Spotify, Permission, Date as d, Dialog } from 'App/Helpers';
import Validate from 'App/Helpers/Validate';
import { getBaseUrl } from 'App/Helpers/ApiHandler/ApiHandler';
import UUID from 'uuid-generate';
import { persistor } from 'App/App';

export function* handleUserLogout() {
  console.log('=== handleUserLogout ===');
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      User.logout(),
    );
    if (res.success) {
      yield put(MirrorActions.onMirrorDisconnectByUser());
      yield put(UserActions.onUserReset(res.data));
      yield call(persistor.pause);
      yield call(persistor.flush);
      yield put(
        AppActions.reset({
          appState: AppStore.getState().appState,
          deviceVersionInfo: AppStore.getState().deviceVersionInfo,
        }),
      );
      yield call(persistor.purge);
      yield call(persistor.persist);
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* fetchPutNotificationEnable({ isEnable }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const fcmToken = yield select((state) => state.user.fcmToken);
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: { fcmToken, notificationEnable: isEnable },
      }),
      User.notificationEnable(),
    );
    if (res.success) {
      yield put(
        UserActions.updateUserStore({
          notificationEnable: res.data.notificationEnable,
        }),
      );
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}
export function* fetchUserTempRegister({ payload: data }) {
  yield put(AppActions.onLoading(true));
  console.log('=== fetchUserTempRegister ===', data);
  try {
    const hostUrl = getBaseUrl();
    const { email, password, passwordConfirmation } = data;
    const { data: res } = yield call(
      Handler.post({
        skipAuthRefresh: true,
        data: {
          email,
          password,
          passwordConfirmation,
        },
        isDefaultHandlerEnable: false,
      }),
      User.tempRegister(),
    );
    if (res.success) {
      res.data.tempPassword = password;
      yield put(UserActions.fetchUserRegisterSuccess(res.data));
      yield put(UserActions.resetErrorCode());
      const fcmToken = yield call(Permission.requestFcmToken);
      const currentTimeZone = yield select((state) => state.appState.currentTimeZone);
      yield put(UserActions.onFcmTokenUpdate(fcmToken));
      requestAnimationFrame(() => Actions.UserRegisterVerifyEmailScreen());
    }
  } catch (e) {
    console.log('e =>>>', e);
    console.log('e.data =>>>', e.data);
    console.log('e.message =>>>', e.data.message);
    const { message, errors } = e.data;

    yield put(UserActions.setErrorCode(message, errors));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}
export function* emailVerifySaga({ email }) {
  let emailVerified = false;
  if (email == null) email = yield select((state) => state.user.email);
  let tempPassword = yield select((state) => state.user.tempPassword);
  const { data: res } = yield call(
    Handler.get({}),
    `/api/client/account/checkEmailVerified/${email}`,
  );

  if (res.data.emailVerified) {
    emailVerified = true;
  }
  if (emailVerified) {
    try {
      const fcmToken = yield call(Permission.requestFcmToken);
      const currentTimeZone = yield select((state) => state.appState.currentTimeZone);

      yield put(UserActions.onFcmTokenUpdate(fcmToken));
      const hostUrl = getBaseUrl();
      const myIp = yield call(publicIP);
      const { data: loginRes } = yield call(
        Handler.post({
          data: {
            ...(hostUrl.includes('https')
              ? {
                  platform: Platform.OS === 'ios' ? 2 : 1,
                  deviceType: 0,
                  deviceAddress: fcmToken,
                }
              : {
                  platform: 0,
                  deviceType: 1,
                }),
            email: email,
            password: tempPassword,
            timezone: currentTimeZone,
          },
          header: {
            'True-Client-IP': myIp,
          },
          isDefaultHandlerEnable: false,
        }),
        User.login(),
      );
      if (loginRes.success) {
        loginRes.data.tempPassword = '';
        yield put(UserActions.fetchUserLoginSuccess(loginRes.data));
        yield put(UserActions.getUserAccount());
        yield put(SettingActions.getSetting());
        yield delay(3000);
        yield put(UserActions.resetErrorCode());

        Actions.AddProfileScreen();
      }
    } catch (e) {
      console.log('e =>>>', e);
      console.log('e.message =>>>', e.data.message);
    }
  }

  return emailVerified;
}
export function* emailVerifyTimerSaga({ email }) {
  yield put(AppActions.onNavLoading(true));

  let emailVerified = false;
  while (true) {
    try {
      emailVerified = yield emailVerifySaga({ email });

      if (emailVerified) break;

      yield delay(3000);
    } catch (error) {}
  }
  yield put(AppActions.onNavLoading(false));
}
export function* fetchUserRegister({ payload: data }) {
  yield put(AppActions.onLoading(true));
  try {
    if (Validate.isRegionSupport() === false) {
      return yield call(Dialog.regionIsNotSupportAlert(() => {}));
    }
    const hostUrl = getBaseUrl();

    const { data: res } = yield call(
      Handler.post({
        skipAuthRefresh: true,
        data: {
          email: data.email,
          fullName: data.name,
          userName: data.userName || data.email,
          password: data.password,
          setting: {
            birth: d.moment(data.birth).format('YYYY-MM-DD'),
            gender: data.gender,
            height: data.height,
            weight: data.weight,
          },
        },
        isDefaultHandlerEnable: false,
        errorHandler: async (err) => {
          const { status } = err;
          console.log(err);
          if (status === 400) {
            Alert.alert(t('alert_title_oops'), t('email_exist'));
          } else {
            Alert.alert(t('alert_title_oops'), t('modal_connect_failure_desc'));
          }
        },
      }),
      User.register(),
    );
    if (res.success) {
      yield put(UserActions.fetchUserRegisterSuccess(res.data));
      yield put(UserActions.resetErrorCode());
      const fcmToken = yield call(Permission.requestFcmToken);
      const currentTimeZone = yield select((state) => state.appState.currentTimeZone);
      const currentCountryCode = yield select((state) => state.user.currentCountryCode);

      yield put(UserActions.onFcmTokenUpdate(fcmToken));
      const myIp = yield call(publicIP);
      const { data: loginRes } = yield call(
        Handler.post({
          data: {
            ...(hostUrl.includes('https')
              ? {
                  platform: Platform.OS === 'ios' ? 2 : 1,
                  deviceType: 0,
                  deviceAddress: fcmToken,
                }
              : {
                  platform: 0,
                  deviceType: 1,
                }),
            email: data.email,
            password: data.password,
            timezone: currentTimeZone,
          },
          header: {
            'True-Client-IP': myIp,
          },
          isDefaultHandlerEnable: false,
        }),
        User.login(),
      );
      if (loginRes.success) {
        yield put(UserActions.fetchUserLoginSuccess(loginRes.data));
        yield put(UserActions.getUserAccount());
        yield put(UserActions.resetErrorCode());
        requestAnimationFrame(() =>
          Actions.UserRegisterSuccessScreen({
            type: 'replace',
            panHandlers: null,
          }),
        );
      }
    }
  } catch (e) {
    console.log('e =>>>', e);
    console.log('e.message =>>>', e.data.message);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* fetchUserLogin({ payload }) {
  console.log('=== fetchUserLogin ===');
  yield put(AppActions.onLoading(true));
  try {
    const currentTimeZone = yield select((state) => state.appState.currentTimeZone);

    if (Validate.isRegionSupport() === false) {
      return yield call(Dialog.regionIsNotSupportAlert(() => {}));
    }
    const hostUrl = getBaseUrl();

    const myIp = yield call(publicIP);
    const { data: res } = yield call(
      Handler.post({
        skipAuthRefresh: true,
        data: {
          timezone: currentTimeZone,
          ...payload,
          ...(hostUrl.includes('https')
            ? {
                platform: Platform.OS === 'ios' ? 2 : 1,
                deviceType: 0,
              }
            : {
                platform: Platform.OS === 'ios' ? 2 : 1,
                deviceType: 0,
              }),
        },
        header: {
          'True-Client-IP': myIp,
        },
        isDefaultHandlerEnable: false,
      }),
      User.login(),
    );
    console.log('fetchUserLogin res=>', res.data);
    if (res.success) {
      yield put(UserActions.fetchUserLoginSuccess(res.data));
      yield put(UserActions.getUserAccount());
      yield put(UserActions.resetErrorCode());
      yield put(SettingActions.getSetting());
      Spotify.initializeIfNeeded();
      requestAnimationFrame(() => {
        Actions.ClassScreen({
          type: 'replace',
          panHandlers: null,
        });
      });
    }
  } catch (e) {
    console.log('fetchUserLogin error=>', e);
    console.log(e.toString());
    if (e.toString() == 'Error: Network Error') {
      Alert.alert(t('alert_title_oops'), t('modal_connect_failure_desc'));
    } else if (has(e, 'data.message')) {
      console.log(e.data.message);
      switch (e.data.message) {
        case 'This app is not allowed in your region.': {
          yield put(UserActions.setErrorCode(t('auth_login_region_not_allowed')));
          break;
        }

        default: {
          yield put(UserActions.setErrorCode(e.data.message));
          break;
        }
      }
    }
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* updateNotificationPermission({ payload }) {
  yield put(AppActions.onLoading(true));
  try {
    const myIp = yield call(publicIP);
    const currentTimeZone = yield select((state) => state.appState.currentTimeZone);
    const hostUrl = getBaseUrl();

    const { data: res } = yield call(
      Handler.post({
        skipAuthRefresh: true,
        data: {
          timezone: currentTimeZone,
          ...payload,
          ...(hostUrl.includes('https')
            ? {
                platform: Platform.OS === 'ios' ? 2 : 1,
                deviceType: 0,
              }
            : {
                platform: 0,
                deviceType: 1,
              }),
        },
        header: {
          'True-Client-IP': myIp,
        },
        isDefaultHandlerEnable: false,
      }),
      User.updateNotificationPermission(),
    );
    if (res.success) {
      console.log('=================  update Success =================');
    }
  } catch (e) {
    console.log('fetchUserLogin error=>', e);
    if (has(e, 'data.message')) {
      console.log(e.data.message);
      switch (e.data.message) {
        case 'This app is not allowed in your region.': {
          yield put(UserActions.setErrorCode(t('auth_login_region_not_allowed')));
          break;
        }

        default: {
          yield put(
            UserActions.setErrorCode(`${t('login_password_error')}(${e.status})`),
          );
          break;
        }
      }
    }
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* handleSpotifyTokenChange({ session }) {
  try {
    const token = yield select((state) => state.user.token);
    yield call(
      Handler.put({
        Authorization: token,
        data: {
          scope: session.scopes.toString(),
          accessToken: session.accessToken,
          refreshToken: session.refreshToken,
          expireTime: new Date(session.accessTokenExpirationDate).getTime(),
        },
      }),
      User.updateSpotifyToken(),
    );
    yield put(UserActions.onUpdateSpotifyToken(session));
    yield put(MirrorActions.onMirrorLog('UserSaga: update Spotify session.'));
  } catch (e) {
    console.log(JSON.stringify(e));
  }
}

export function* handleSpotifyTokenClean() {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    yield call(
      Handler.delete({
        Authorization: token,
      }),
      User.deleteSpotifyToken(),
    );
    yield put(UserActions.onResetSpotifyToken());
    yield put(MirrorActions.onMirrorLog('UserSaga: clean Spotify session.'));
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* handleRefreshDeviceToken() {
  try {
    yield put(MirrorActions.onMirrorLog('UserSaga: refresh user token.'));
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getUserAccount() {
  yield put(AppActions.onLoading(true));
  try {
    const myIp = yield call(publicIP);
    const token = yield select((state) => state.user.token);
    const fcmToken = yield select((state) => state.user.fcmToken);
    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
        header: {
          'True-Client-IP': myIp,
        },
      }),
      User.getUserAccount({ fcmToken }),
    );
    if (res.success) {
      yield put(UserActions.getUserAccountSuccess(res.data.user));
      if (res.data.user.activeTrainingProgramId) {
        yield put(
          ProgramActions.getProgramDetail(
            res.data.user.activeTrainingProgramId,
            token,
            false,
          ),
        );
      } else {
        yield put(ProgramActions.getProgramList(token));
      }
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* fetchPutSubscriptionCode({ code, goto = null }) {
  console.log('code=>', code);
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: {
          subscriptionCode: code,
        },
        isDefaultHandlerEnable: false,
        errorHandler: async (err) => {
          const { status, data = {} } = err;
          AppStore.dispatch(UserActions.setErrorCode(data.message));
        },
      }),
      User.putSubscriptionCode(),
    );
    if (res.success) {
      yield call(getUserAccount);
      yield put(UserActions.resetErrorCode());
      if (goto == null) requestAnimationFrame(Actions.pop);
      else
        requestAnimationFrame(
          Actions[goto]({
            type: 'replace',
            panHandlers: null,
          }),
        );
    }
  } catch (e) {
    yield put(UserActions.setErrorCode(e.data.message));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* sendEmailVerify({ payload: { email } }) {
  console.log('sendEmailVerify email=>', email);
  yield put(AppActions.onLoading(true));
  try {
    const { data: res } = yield call(
      Handler.post({
        data: {
          email: email,
        },
        isDefaultHandlerEnable: false,
      }),
      User.sendVerifyEmail(),
    );
    if (res.success) {
      const emailVerifyCountDownId = UUID.generate();
      yield put(UserActions.sendEmailVerifySuccess({ emailVerifyCountDownId }));
    }
  } catch (e) {
    const { message, errors } = e.data;
    yield put(UserActions.setErrorCode(message, errors));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* sendForgotPasswordEmail({ email }) {
  console.log('email=>', email);
  yield put(AppActions.onLoading(true));
  try {
    const { data: res } = yield call(
      Handler.post({
        data: {
          email: email,
        },
        isDefaultHandlerEnable: false,
        errorHandler: async (err) => {
          const { message } = err.data;
          if (message === 'user not exist') {
            Alert.alert(t('alert_title_oops'), t('mail_not_exist'));
          } else {
            Alert.alert(t('alert_title_oops'), t('modal_connect_failure_desc'));
          }
        },
      }),
      User.resetPassword(),
    );
    if (res.success) {
      yield put(UserActions.sendForgotPasswordEmailSuccess(email));
      Actions.SendForgotMailSuccessScreen();
    }
  } catch (e) {
    yield put(UserActions.setErrorCode(e.data.message));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* resetPassword({ currentPassword, newPassword, email, isLogin }) {
  yield put(AppActions.onLoading(true));
  try {
    const isAtSettingUpdatePassword = isLogin === true;
    const isAtLoginForgotPassword = isLogin === false;
    console.log('=== isAtSettingUpdatePassword ===', isAtSettingUpdatePassword);

    const { data: loginRes } = yield call(
      Handler.post({
        data: {
          email: email,
          password: currentPassword,
        },
        isDefaultHandlerEnable: false,
        errorHandler: async (err) => {
          const { status } = err;
          if (status === 400) {
            Alert.alert(t('alert_title_oops'), t('1-1-2_password_error'));
          } else {
            Alert.alert(t('alert_title_oops'), t('modal_connect_failure_desc'));
          }
        },
      }),
      User.login(),
    );
    if (loginRes.success) {
      const { accessToken } = loginRes.data;
      const { data: resetPasswordResult } = yield call(
        Handler.put({
          Authorization: accessToken,
          data: {
            password1: newPassword,
            password2: newPassword,
          },
        }),
        User.updatePassword(),
      );

      if (resetPasswordResult.success) {
        if (isAtLoginForgotPassword) {
          Alert.alert('Success', t('1-1-2_reset_password_success_login'), [
            {
              text: t('__ok'),
              onPress: () => Actions.UserLoginScreen(),
            },
          ]);
        }

        if (isAtSettingUpdatePassword) {
          Alert.alert('Success', t('6-10-1_update_password_success'), [
            {
              text: t('__ok'),
              onPress: () => Actions.SettingScreen(),
            },
          ]);
        }
      }
    }
  } catch (e) {
    yield put(UserActions.setErrorCode(e.data.message));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* fetchAddMemberSubscriptionCode() {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      User.addMemberSubscriptionCode(),
    );
    if (res.success) {
      yield call(getUserAccount);
    } else {
      Alert.alert('error', `${res.message}(${res.statusCode})`);
    }
  } catch (e) {
    console.log(JSON.stringify(e));
    Alert.alert('error', `${e.message}(${e.statusCode}) - ${JSON.stringify(e)}`);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* fetchDelMemberSubscriptionCode({ code }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      User.delMemberSubscriptionCode({ code }),
    );
    console.log('fetchDelMemberSubscriptionCode res=>', res);
    if (res.success) {
      yield call(getUserAccount);
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* fetchDelMemberSubscription() {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      User.delMemberSubscription(),
    );
    console.log('fetchDelMemberSubscription res=>', res);
    if (res.success) {
      yield call(getUserAccount);
      yield call(Actions.pop);
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* fetchCheckServerRegion() {
  try {
    const env = yield select((state) => state.appState.env);
    if (env !== Config.DEFAULT_SERVER_ENV) {
      return yield put(
        UserActions.updateUserStore({
          currentCountryCode: env,
        }),
      );
    }

    const currentCountryCode = yield select((state) => state.user.currentCountryCode);
    const myIp = yield call(publicIP);
    const { data: res } = yield call(
      Handler.get({
        skipAuthRefresh: true,
        header: {
          'True-Client-IP': myIp,
        },
        baseURL: Config.API_BASE_URL[env],
        isDefaultHandlerEnable: false,
      }),
      User.ipLocation(),
    );

    if (has(res, 'data.countryCode')) {
      if (currentCountryCode !== res.data.countryCode) {
        yield put(UserActions.onUserReset());
        if (Config.API_BASE_URL[currentCountryCode]) {
          yield call(Actions.replace, 'UserLoginScreen');
          yield call(Dialog.requestUserReLoginDueToRegionChangedAlert(() => {}));
        } else {
          yield call(Actions.replace, 'UserLoginScreen');
          yield call(Dialog.regionIsNotSupportAlert(() => {}));
        }
        yield put(
          UserActions.updateUserStore({
            currentCountryCode: res.data.countryCode,
          }),
        );
      }
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}
export function* deleteAccountSaga() {
  console.log('=== deleteAccountSaga ===');
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      User.deleteAccount(),
    );

    if (true) {
      yield put(UserActions.onUserLogout());
      Actions.UserLoginScreen({
        type: 'replace',
        panHandlers: null,
      });
    }
  } catch (e) {
    console.log(JSON.stringify(e));
  } finally {
    yield put(AppActions.onLoading(false));
  }
}
