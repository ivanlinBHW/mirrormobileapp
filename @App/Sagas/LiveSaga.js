import { put, call, select, delay } from 'redux-saga/effects';
import { Alert } from 'react-native';
import { has, get } from 'lodash';
import { Actions } from 'react-native-router-flux';

import LiveActions from 'App/Stores/Live/Actions';
import AppActions from 'App/Stores/AppState/Actions';
import MirrorActions, { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { ClassActions, PlayerActions, SeeAllActions } from 'App/Stores';

import { Handler, Live } from 'App/Api';
import { translate as t } from 'App/Helpers/I18n';
import { Date as d, Dialog } from 'App/Helpers';

import { uploadTrainingRecords } from 'App/Sagas/Mirror/MirrorCalorieStatusSaga';
export function* getLiveList() {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Live.getLiveList(),
    );

    if (res.success) {
      yield put(LiveActions.getLiveListSuccess(res.data.liveClasses));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getLiveDetail({ id }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Live.getLiveDetail({ id }),
    );

    if (res.success) {
      yield put(LiveActions.getLiveDetailSuccess(res.data.liveClass));
      yield put(ClassActions.getClassDetailSuccess(res.data.liveClass));

      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
          classId: id,
          classType: 'live',
        }),
      );
      const currentRoute = yield select((state) => state.appRoute.routeName);

      if (currentRoute === 'Live' || currentRoute === 'ClassScreen') Actions.LiveDetail();
    }
  } catch (error) {
    console.log('error.status =>', error.status);
    yield put(ClassActions.getClassList());
    if (error.status === 404) {
      Alert.alert('', t('class_404'), [
        {
          text: t('__ok'),
        },
      ]);
    }
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* onlyGetLiveDetail({ id }) {
  try {
    yield put(AppActions.onLoading(true));
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Live.getLiveDetail({ id }),
    );

    if (res.success) {
      yield put(ClassActions.getClassDetailSuccess(res.data.liveClass));
      yield put(LiveActions.getLiveDetailSuccess(res.data.liveClass));
    }
  } catch (err) {
    console.log(err);
    if (err.status === 404) {
      Dialog.showClassNotFoundAlert();
    } else if (
      err.status !== 501 &&
      err.status !== 502 &&
      err.status !== 503 &&
      err.status !== 504
    ) {
      Dialog.showApiExceptionAlert(err);
    }
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* onlyPlayingClassGetLiveDetail({ id }) {
  try {
    const token = yield select((state) => state.user.token);
    yield put(AppActions.onLoading(true));

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Live.joinLive({ id }),
    );

    if (res.success) {
      const { data: detailRes } = yield call(
        Handler.get({ Authorization: token }),
        Live.getLiveHistoryDetail({ id, historyId: res.data.liveClassHistory.id }),
      );
      yield put(LiveActions.setLiveClassHistory(res.data.liveClassHistory));
      yield put(ClassActions.getClassDetailSuccess(detailRes.data.liveClass));
      yield put(LiveActions.getLiveDetailSuccess(detailRes.data.liveClass));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* onlyClassSummaryGetLiveDetail({ id, historyId }) {
  try {
    yield put(AppActions.onLoading(true));
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Live.getLiveHistoryDetail({ id, historyId }),
    );

    if (res.success) {
      console.log('res.data ->', res.data);
      yield put(ClassActions.getClassDetailSuccess(res.data.liveClass));
      yield put(LiveActions.getLiveDetailSuccess(res.data.liveClass));
      if (
        has(res.data.liveClass, 'liveClassHistory') &&
        res.data.liveClass.liveClassHistory.status !== 2
      ) {
        yield put(LiveActions.finishLiveClass(id));
      }
    }
  } catch (err) {
    console.log(err);
    if (err.status === 404) {
      Dialog.showClassNotFoundAlert();
    } else if (
      err.status !== 501 &&
      err.status !== 502 &&
      err.status !== 503 &&
      err.status !== 504
    ) {
      Dialog.showApiExceptionAlert(err);
    }
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getLiveDate({ params }) {
  yield put(AppActions.onLoading(true, '', { hide: true }));
  try {
    const token = yield select((state) => state.user.token);

    let temp = params;
    if (temp.start) {
      temp.start = d.moment().format('YYYY-MM-DD');
    }
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Live.getLiveDate({ params: temp }),
    );
    if (res.success) {
      yield put(LiveActions.getLiveDateSuccess(res.data.result, params));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getLiveDateDetail({ day, currentTimeZone }) {
  yield put(AppActions.onLoading(true, '', { hide: true }));
  try {
    const token = yield select((state) => state.user.token);

    const nowTime = d.moment().format('HH:mm:ss');
    const isToday = d.moment(day).isSame(new Date(), 'day');
    const start = d.transformDateToUTC(
      d.moment(`${day} ${isToday ? nowTime : '00:00:00'}`),
      'UTC',
      'YYYY-MM-DD HH:mm:ss',
    );
    const end = d.transformDateToUTC(
      d.moment(`${day} 23:59:59`),
      'UTC',
      'YYYY-MM-DD HH:mm:ss',
    );

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Live.getLiveDateDetail({ start, end }),
    );

    if (res.success) {
      yield put(LiveActions.getLiveDateDetailSuccess(res.data.result));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* bookLive({ id }) {
  yield put(AppActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Live.bookLive({ id }),
    );

    if (res.success) {
      yield call(onlyGetLiveDetail, { id });

      const { start, end } = yield select((state) => state.live);
      yield call(getLiveDate, { params: { start, end } });
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* joinLive({ id }) {
  try {
    const token = yield select((state) => state.user.token);
    yield put(AppActions.onLoading(true));

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Live.joinLive({ id }),
    );

    if (res.success) {
      yield put(LiveActions.setLiveClassHistory(res.data.liveClassHistory));
      yield put(MirrorActions.resetCurrentPosition());
      yield put(PlayerActions.setRouteName('LiveDetail'));
      delay(200);
      Actions.LivePlayerScreen();

      const detail = yield select((state) => state.player.detail);
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.START_LIVE_CLASS, {
          classId: detail.id,
          liveClassHistoryId: res.data.liveClassHistory.id,
        }),
      );
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* onlyJoinLive({ id }) {
  try {
    const token = yield select((state) => state.user.token);
    yield put(AppActions.onLoading(true));

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Live.joinLive({ id }),
    );

    if (res.success) {
      yield put(LiveActions.setLiveClassHistory(res.data.liveClassHistory));
      yield put(PlayerActions.setRouteName('LiveDetail'));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}
export function* finishLiveClass({ id }) {
  try {

    const token = yield select((state) => state.user.token);
    yield put(AppActions.onLoading(true));
    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Live.finishLive({ id }),
    );

    if (res.success) {
      yield put(LiveActions.setLiveClassHistory(res.data.liveClassHistory));
      yield put(MirrorActions.resetThumbnail());

      yield call(requestAnimationFrame, Actions.SaveActivityScreen);
    }
    yield put(AppActions.onLoading(false));
  } catch (err) {
    console.log(err);
    if (get(err, 'data.message') === 'liveClassHistory.status.is.finished') {
      yield put(MirrorActions.resetThumbnail());
      yield call(requestAnimationFrame, Actions.SaveActivityScreen);
    }
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* submitLiveFeedback({ id, payload }) {
  try {
    const token = yield select((state) => state.user.token);
    const classId = yield select((state) => state.player.detail.id);
    yield put(AppActions.onLoading(true));

    const { data: res } = yield call(
      Handler.post({ Authorization: token, data: payload }),
      Live.submitFeedback({ id }),
    );

    if (res.success) {
      Actions.popTo('LiveDetail');
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* setLiveBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Live.setBookmark({ id }),
    );

    console.log('setLiveBookmark =>');

    if (res.success) {
      yield put(LiveActions.setLiveBookmarkSuccess(id));
      yield put(ClassActions.setClassLiveBookmarkSuccess(id));
      yield put(SeeAllActions.setBookmark(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* removeLiveBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      Live.deleteBookmark({ id }),
    );

    if (res.success) {
      yield put(LiveActions.removeLiveBookmarkSuccess(id));
      yield put(ClassActions.removeClassLiveBookmarkSuccess(id));
      yield put(SeeAllActions.removeBookmark(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* setCountDown({ duration }) {
  const detail = yield select((state) => state.live.detail);
  let temp = parseInt(duration, 10) - 4;
  yield delay(2000);
  while (temp > 0) {
    temp -= 1;
    yield put(LiveActions.setCountDownSuccess(d.transformMinute(temp)));
    yield delay(1000);
  }
  yield put(
    MirrorActions.onMirrorCommunicate(MirrorEvents.START_LIVE_CLASS, {
      classId: detail.id,
    }),
  );
  yield put(LiveActions.resetCountDown());
}

export function* sendLiveReaction({ id, reaction }) {
  try {
    console.log('=== sendLiveReaction ===', id, reaction);
    const token = yield select((state) => state.user.token);

    yield call(
      Handler.post({
        Authorization: token,
        data: {
          reaction,
        },
      }),
      Live.sendReaction({ id }),
    );
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* exitLiveClass({ params }) {
  try {
    console.log(
      '=== exitLiveClass ===',
      params,
      params.liveClassId,
      params.liveClassHistoryId,
    );
    const token = yield select((state) => state.user.token);

    const { data: detailRes } = yield call(
      Handler.put({ Authorization: token }),
      Live.exitLiveClass({
        id: params.liveClassId,
        historyId: params.liveClassHistoryId,
      }),
    );
  } catch (err) {
    console.log(err);
  } finally {
  }
}
