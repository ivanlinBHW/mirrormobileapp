import { put, call, select } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';

import { AppStateActions, AppBulletinActions } from 'App/Stores';
import { Handler, Bulletin } from 'App/Api';
import { Dialog } from 'App/Helpers';
import { Config } from 'App/Config';

export function* getBulletinVersion() {
  try {
    const { data: res } = yield call(
      Handler.get({
        baseURL: Config.API_BASE_URL.BULLETIN,
        isDefaultHandlerEnable: false,
      }),
      Bulletin.getBulletinVersion(),
    );
    if (res && res.version && res.isPublished) {
      console.log('res.data=>', res);
      const newestVersion = res.version;
      yield put(AppBulletinActions.getBulletinSuccess({ version: newestVersion }));

      const skippedVersion = yield select((state) => state.appBulletin.skippedVersion);
      if (!skippedVersion || skippedVersion !== newestVersion) {
        yield call(Actions.BulletinModel);
      }
    }
  } catch (err) {
    console.error('getBulletinVersion error=>', err);
  }
}

export function* getBulletinContent({} = {}) {
  yield put(AppStateActions.onLoading(true));
  try {
    const { data: res } = yield call(
      Handler.get({
        baseURL: Config.API_BASE_URL.BULLETIN,
        isDefaultHandlerEnable: false,
      }),
      Bulletin.getBulletinContent(),
    );
    if (res) {
      console.log('res.data=>', res);
      yield put(AppBulletinActions.getBulletinSuccess({ content: res }));
    }
  } catch (err) {
    console.error('getBulletin error=>', err);
    if (
      err.status !== 501 &&
      err.status !== 502 &&
      err.status !== 503 &&
      err.status !== 504
    ) {
      Dialog.showApiExceptionAlert(err);
    }
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}
