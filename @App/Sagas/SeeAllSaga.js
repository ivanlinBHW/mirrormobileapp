import { put, call, select, delay } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';

import { SeeAllActions, AppStateActions } from 'App/Stores';
import { Handler, SeeAll } from 'App/Api';
export function* getSeeAllList({ mode, curPage = 1, perPage = 15 }) {
  yield put(AppStateActions.onLoading(true));
  const token = yield select((state) => state.user.token);
  try {
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      SeeAll.getList({ type: mode, curPage, perPage }),
    );
    if (res.success) {
      if (res.data.bookmarked) {
        const { liveClasses, trainingClasses } = res.data.bookmarked;
        const total = liveClasses.total + trainingClasses.total;
        const hasNext = liveClasses.hasNext || trainingClasses.hasNext;

        let data = [];
        if (liveClasses) {
          data = data.concat(liveClasses.data);
        }
        if (trainingClasses) {
          data = data.concat(trainingClasses.data);
        }
        yield put(SeeAllActions.getSeeAllListSuccess(data, total, hasNext));
      } else {
        const { data, total, hasNext } = res.data.items;
        yield put(SeeAllActions.getSeeAllListSuccess(data, total, hasNext));
      }
    }
  } catch (err) {
    console.error('getSeeAllList error=>', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}
export function* getTrainingClassList({ workoutType }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    let workoutTypeId = workoutType.id;
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      SeeAll.getClassListByWorkoutType({ workoutTypeId }),
    );
    if (res.success) {
      const { trainingClasses } = res.data;
      let list = trainingClasses;

      yield put(SeeAllActions.setList(list));
      yield delay(200);
      Actions.SeeAllScreen({ loginRoute: 'seeAll', title: workoutType.title });
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}
