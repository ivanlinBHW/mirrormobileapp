import { put, call, select } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import CollectionActions from 'App/Stores/Collection/Actions';
import AppActions from 'App/Stores/AppState/Actions';
import { ProgramActions, ClassActions } from 'App/Stores';
import { Handler, Collection, Class } from 'App/Api';
export function* getCollectionList({}) {
  try {

    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Collection.getCollectionList(),
    );

    if (res.success) {
      yield put(CollectionActions.getCollectionListSuccess(res.data.trainingCollections));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* getCollectionDetail({ id, token }) {
  try {
    yield put(AppActions.onLoading(true));

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Collection.getCollectionDetail({ id }),
    );

    if (res.success) {
      yield put(
        CollectionActions.getCollectionDetailSuccess(res.data.trainingCollection),
      );

      const currentRoute = yield select((state) => state.appRoute.routeName);

      if (currentRoute === 'Collection') Actions.CollectionDetail();
    }
  } catch (err) {
    console.error(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* setCollectionBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Class.setBookmark({ id }),
    );

    if (res.success) {
      yield put(CollectionActions.setCollectionBookmarkSuccess(id));
      yield put(ProgramActions.setProgramBookmarkSuccess(id));
      yield put(ClassActions.setClassBookmarkSuccess(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}

export function* removeCollectionBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.delete({ Authorization: token }),
      Class.deleteBookmark({ id }),
    );

    if (res.success) {
      yield put(CollectionActions.removeCollectionBookmarkSuccess(id));
      yield put(ProgramActions.removeProgramBookmarkSuccess(id));
      yield put(ClassActions.removeClassBookmarkSuccess(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppActions.onLoading(false));
  }
}
