import { eventChannel } from 'redux-saga';
import {
  cancelled,
  cancel,
  delay,
  take,
  call,
  put,
  fork,
  actionChannel,
  select,
} from 'redux-saga/effects';
import Zeroconf from 'react-native-zeroconf';

import { MdnsActions } from 'App/Stores';
import { MdnsTypes as Types } from 'App/Stores/Mdns/Actions';
import Ping from 'react-native-ping';
const connectionChannel = (manager, routeName) =>
  eventChannel((emit) => {
    console.log('=== connectionChannel routeName ===', routeName);
    manager.once('start', () => {
      emit(MdnsActions.onMdnsScanStarted());
    });

    manager.once('stop', () => {
      emit(MdnsActions.onMdnsScanStopped());
    });

    manager.on('error', (err) => {
      emit(MdnsActions.onMdnsScanError(err));
    });
    manager.on('resolved', (service) => {
      if (routeName == 'MiiSettingScreen' || routeName == 'DeviceModal') {
        if (service.addresses[0].indexOf(':') == -1) {
          console.log('=== service.addresses ===', service.addresses);
          Ping.start(service.addresses[0], {
            timeout: 16000,
          }).then((result) => {
            try {
              emit(MdnsActions.onMdnsScanResolved(service));
            } catch (error) {}
          });
        }
      } else {
        emit(MdnsActions.onMdnsScanResolved(service));
      }
    });

    return () => {
      manager.removeAllListeners();
      manager.removeDeviceListeners();
    };
  });

function* handleConnection(manager) {
  let mDnsChannel = null;
  try {
    const routeName = yield select((state) => state.appRoute.routeName);

    mDnsChannel = yield call(connectionChannel, manager, routeName);
    for (;;) {
      const action = yield take(mDnsChannel);
      yield put(action);

      if (action.type === Types.ON_MDNS_SCAN_ERROR) {
        break;
      }
    }
  } catch (error) {
    console.log('mdnsHandleConnection error=>', error);
  } finally {
    if (yield cancelled()) {
      mDnsChannel.close();
    }
  }
}

let manager;

export default function* mdnsManager({
  uriPrefix = 'http',
  protocol = 'tcp',
  domain = 'local.',
} = {}) {
  yield delay(1000);
  let connectionTask;
  const sagaChannel = yield actionChannel([Types.ON_MDNS_SCAN, Types.ON_MDNS_STOP]);
  try {
    if (!manager) {
      manager = new Zeroconf();
    }
    manager.scan(uriPrefix, protocol, domain);

    connectionTask = yield fork(handleConnection, manager);
    yield put(MdnsActions.onMdnsLog('mdnsManager started...'));

    for (;;) {
      const action = yield take(sagaChannel);

      if (action.type === Types.ON_MDNS_STOP) {
        manager.stop();
        yield delay(100);
        sagaChannel.close();
        yield cancel(connectionTask);
        yield put(MdnsActions.onMdnsLog('mdnsManager stop...'));
        break;
      }
    }
  } catch (error) {
    console.log('mdnsManager error=>', error);
  } finally {
    console.log('mdnsManager ended');
    if (manager !== null) {
      manager.stop();
      manager = null;
    }
    if (yield cancelled()) {
      console.log('mdnsManager cancelled');
      yield cancel(connectionTask);
    }
  }
}
