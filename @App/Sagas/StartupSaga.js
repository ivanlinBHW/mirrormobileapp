import { put } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';

import ExampleActions from 'App/Stores/Example/Actions';
export function* startup() {
  yield put(ExampleActions.fetchUser());
  requestAnimationFrame(() => {
    Actions.ApiExampleScreen();
  });
}
