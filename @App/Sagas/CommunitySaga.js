import { put, call, select, delay } from 'redux-saga/effects';
import { has, first, isEmpty, isArray } from 'lodash';
import { Actions } from 'react-native-router-flux';
import { Alert } from 'react-native';

import {
  CommunityActions,
  AppStateActions,
  ProgressActions,
  MirrorActions,
  PlayerActions,
  ClassActions,
} from 'App/Stores';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { Handler, Community, Class } from 'App/Api';
import moment from 'moment';
import { translate as t, Date as d } from 'App/Helpers/I18n';
import { Dialog } from 'App/Helpers';

import * as MirrorCalorieSaga from 'App/Sagas/Mirror/MirrorCalorieStatusSaga';
export function* getList() {

  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getList(),
    );
    if (res.success) {
      yield put(CommunityActions.setUserEventList(res.data.userTrainingEvents));
      yield put(CommunityActions.setAllEventList(res.data.allTrainingEvents));
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getAllMyEvent() {

  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getAllMyEvent(),
    );
    if (res.success) {
      yield put(CommunityActions.getAllMyEventSuccess(res.data.items));
      Actions.MyEventScreen();
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getAllEvents(shouldJumpScreen = true) {

  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getAllEvents(),
    );
    if (res.success) {
      yield put(CommunityActions.getAllEventsSuccess(res.data.items));
      shouldJumpScreen && Actions.AllEventsScreen();
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getSearchFriends({
  text,
  friendType = 'event',
  trainingEventId: eventId = null,
}) {
  try {
    yield delay(200);
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token, data: { text } }),
      Community.searchFriend({ type: friendType, eventId }),
    );
    if (res.success) {
      yield put(CommunityActions.getSearchFriendsSuccess(res.data.users));
    }
  } catch (err) {
    console.log('err', err);
  }
}

export function* getSearchUser({ text }) {
  try {
    yield delay(200);
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token, data: { text } }),
      Community.searchUser(),
    );
    if (res.success) {
      yield put(CommunityActions.getSearchFriendsSuccess(res.data.users));
    }
  } catch (err) {
    console.log('err', err);
  }
}

export function* getCoverImages() {
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getCoverImages(),
    );
    if (res.success) {
      yield put(CommunityActions.getCoverImagesSuccess(res.data.items));
    }
  } catch (err) {
    console.log('err', err);
  }
}

export function* postEventCreation({ data, onSuccess }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    console.log('postEventCreation data =>', data);
    const { data: res } = yield call(
      Handler.post({ Authorization: token, data }),
      Community.postEventCreation(),
    );
    if (res.success) {
      yield put(CommunityActions.createEventSuccess(res.data));
      if (typeof onSuccess === 'function') {
        yield call(onSuccess);
      } else {
        requestAnimationFrame(() =>
          Actions.CommunityScreen({
            type: 'replace',
          }),
        );
      }
      yield delay(500);
      yield put(CommunityActions.resetEventData());
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getUserDetail({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getUserDetail({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getUserDetailSuccess(res.data.user));

      const routeName = yield select((state) => state.appRoute.routeName);
      if (routeName.includes('Notification')) {
        Actions.NotificationFriendDetailScreen();
      } else {
        Actions.FriendDetailScreen();
      }
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* notificationGetUserDetail({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getUserDetail({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getUserDetailSuccess(res.data.user));
      Actions.NotificationFriendDetailScreen();
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyGetUserDetail({ id }) {
  try {
    console.log('id =>', id);
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getUserDetail({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getUserDetailSuccess(res.data.user));
    }
  } catch (err) {
    console.log('err', err);
  }
}

export function* addFriend({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Community.addFriend({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.onlyGetUserDetail(id));
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* deleteFriend({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Community.deleteFriend({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.onlyGetUserDetail(id));
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* confirmFriend({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Community.confirmFriend({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.onlyGetUserDetail(id));
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getSearchEvents({ text }) {
  try {
    yield delay(200);
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token, data: { text } }),
      Community.searchEvent(),
    );
    if (res.success) {
      yield put(CommunityActions.setSearchEventsText(text));
      yield put(CommunityActions.getSearchEventsSuccess(res.data.trainingEvents));
    }
  } catch (err) {
    console.log('err', err);
  }
}

export function* deleteUnFriend({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Community.deleteUnFriend({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.onlyGetUserDetail(id));
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getUserEvent({ id }) {
  try {
    yield delay(200);
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getUserEvent({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getAllMyEventSuccess(res.data.items));
      Actions.MyEventScreen({ navTitle: t('community_event_title') });
    }
  } catch (err) {
    console.log('err', err);
  }
}

export function* getUserAchievement({ id }) {
  try {
    yield delay(200);
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getUserAchievement({ id }),
    );
    if (res.success) {
      yield put(ProgressActions.getAchievementsSuccess(res.data));
      Actions.AchievementListScreen();
    }
  } catch (err) {
    console.log('err', err);
  }
}

export function* getTrainingEvent({ id }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getTrainingEvent({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getTrainingEventSuccess(res.data.trainingEvent));

      const routeName = yield select((state) => state.appRoute.routeName);
      if (routeName.includes('Notification')) {
        yield call(requestAnimationFrame, Actions.NotificationCommunityDetailScreen);
      } else {
        yield call(requestAnimationFrame, Actions.CommunityDetailScreen);
      }
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* notificationGetTrainingEvent({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getTrainingEvent({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getTrainingEventSuccess(res.data.trainingEvent));
      yield call(Actions.NotificationCommunityDetailScreen);
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyGetTrainingEvent({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getTrainingEvent({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getTrainingEventSuccess(res.data.trainingEvent));
    }
  } catch (err) {
    console.log('err', err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* setCommunityClassBookmark({ id }) {
  try {
    yield put(AppStateActions.onLoading(true, null, { hide: true }));
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Class.setBookmark({ id }),
    );

    if (res.success) {
      yield put(CommunityActions.setCommunityClassBookmarkSuccess(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* removeCommunityClassBookmark({ id }) {
  try {
    yield put(AppStateActions.onLoading(true, null, { hide: true }));
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.delete({ Authorization: token }),
      Class.deleteBookmark({ id }),
    );

    if (res.success) {
      yield put(CommunityActions.removeCommunityClassBookmarkSuccess(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* inviteFriends({ id, friends }) {
  try {
    const token = yield select((state) => state.user.token);
    yield put(AppStateActions.onLoading(true));

    yield call(
      Handler.post({ Authorization: token, data: { inviteUserIds: friends } }),
      Community.inviteFriends({ id }),
    );
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getCommunityClassDetail({
  id,
  eventId,
  wasJoined = false,
  isAccepted = false,
}) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getClassDetail({ id, eventId }),
    );
    if (res.success) {
      console.log('res.data.trainingClass =>', res.data.trainingClass);
      yield put(CommunityActions.setEventClassDetail(res.data.trainingClass));
      if (
        has(res.data.trainingClass, 'trainingEventClassHistory') &&
        !isEmpty(res.data.trainingClass.trainingEventClassHistory)
      ) {
        yield put(
          MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
            classId: id,
            classType: 'on-demand',
            eventClassHistoryId: res.data.trainingClass.trainingEventClassHistory.id,
          }),
        );
      } else {
        yield put(
          MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
            classId: id,
            classType: 'on-demand',
          }),
        );
      }
      const routeName = yield select((state) => state.appRoute.routeName);
      if (routeName.includes('Notification')) {
        Actions.NotificationCommunityDetailClassDetailScreen({
          isOnlySee: wasJoined,
          isAccepted,
        });
      } else {
        Actions.CommunityDetailClassDetailScreen({ isOnlySee: wasJoined, isAccepted });
      }
    }
  } catch (err) {
    console.log(err);
    if (err.status === 404) {
      Dialog.showClassNotFoundAlert();
    } else if (
      err.status !== 501 &&
      err.status !== 502 &&
      err.status !== 503 &&
      err.status !== 504
    ) {
      Dialog.showApiExceptionAlert(err);
    }
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyGetCommunityClassDetail({ id, eventId }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getClassDetail({ id, eventId }),
    );
    if (res.success) {
      yield put(CommunityActions.setEventClassDetail(res.data.trainingClass));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyGetCommunityHistoryClassDetail({ id, eventId, routeName }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getHistoryClassDetail({ id, eventId }),
    );
    if (res.success) {
      yield put(PlayerActions.setRouteName(routeName));
      yield put(CommunityActions.setEventClassDetail(res.data.trainingClass));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyCommunitySummaryClassDetail({ id, eventId, routeName }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Community.getHistoryClassDetail({ id, eventId }),
    );
    if (res.success) {
      yield put(
        CommunityActions.onlyGetTrainingEvent(
          res.data.trainingClass.trainingEventClassHistory.trainingEventId,
        ),
      );
      yield put(PlayerActions.setRouteName(routeName));
      yield put(CommunityActions.setEventClassDetail(res.data.trainingClass));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* searchEventFriend({ id, data, trainingEventId }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({ Authorization: token, data: { text: data } }),
      Community.searchEventFriend({ id, eventId: trainingEventId }),
    );
    if (res.success) {
      yield put(CommunityActions.searchEventFriendSuccess(res.data.users));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* joinEvent({ id }) {
  yield put(AppStateActions.onLoading(true));
  yield delay(250);
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Community.participateEvent({ id }),
    );
    if (res.success) {
      yield call(onlyGetTrainingEvent, { id });
      yield call(getList);
      yield call(getAllEvents, false);
      yield put(CommunityActions.joinEventSuccess(id));
    }
  } catch (error) {
    console.log('error.status =>', error.status);
    if (error.status === 500) {
      Alert.alert(
        t('community_detail_full_booked_title'),
        t('community_detail_full_booked_content'),
        [
          {
            text: t('__ok'),
            onPress: () => console.log('joinEvent full book'),
            style: 'cancel',
          },
        ],
      );
      yield put(CommunityActions.onlyGetTrainingEvent(id));
    }
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* leaveEvent({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({ Authorization: token }),
      Community.participateEvent({ id }),
    );
    if (res.success) {
      yield call(getList);
      yield call(getAllEvents, false);
      yield put(CommunityActions.leaveEventSuccess(id));
      yield put(CommunityActions.onlyGetTrainingEvent(id));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* startEventClass({ classId, eventId, classStartAt = null, options }) {
  try {
    console.log('=== startEventClass options ===', options);
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: { trainingClassId: classId, trainingEventId: eventId },
      }),
      Community.startEventClass(),
    );
    if (res.success) {
      yield put(PlayerActions.setRouteName('EventClassDetail'));
      yield put(CommunityActions.updateEventClassDetail(res.data));
      if (classStartAt === null) {
        yield put(
          MirrorActions.onMirrorCommunicate(MirrorEvents.START_CLASS, {
            classId: classId,
            eventClassHistoryId: res.data.eventClassHistory.id,
            ...options,
          }),
        );
      } else {
        let addTime = moment(classStartAt).add(7, 'seconds');
        yield put(
          MirrorActions.onMirrorCommunicate(MirrorEvents.START_CLASS, {
            classId: classId,
            eventClassHistoryId: res.data.eventClassHistory.id,
            classStartAt: moment(addTime).format('YYYY-MM-DDTHH:mm:ss') + 'Z',
            ...options,
          }),
        );
      }
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* finishEventClass({ classId, eventId }) {
  yield put(AppStateActions.onLoading(true));
  try {

    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: { trainingClassId: classId, trainingEventId: eventId },
      }),
      Community.finishEventClass(),
    );
    if (res.success) {
      yield put(
        CommunityActions.updateEventClassDetail({
          trainingClassHistory: res.data.trainingClassHistory,
          eventClassHistory: res.data.trainingEventClassHistory,
        }),
      );
      yield call(requestAnimationFrame, Actions.SaveActivityScreen);
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* resumeEventClass({
  classId,
  eventId,
  trainingClassHistoryId,
  stepId,
  options,
}) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: {
          trainingClassId: classId,
          trainingEventId: eventId,
          pausedStepId: stepId,
        },
      }),
      Community.resumeEventClass(),
    );
    if (res.success) {
      yield put(PlayerActions.setRouteName('EventClassDetail'));
      yield put(CommunityActions.updateEventClassDetail(res.data));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.START_CLASS, {
          classId: classId,
          eventClassHistoryId: res.data.eventClassHistory.id,
          ...options,
        }),
      );
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* pauseEventClass({ classId, eventId, stepId }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const currentDuration = yield select((state) => state.mirror.currentDuration);
    const { trainingSteps } = yield select((state) => state.player.detail);
    if (!stepId && isArray(trainingSteps)) {
      stepId = first(trainingSteps);
    }
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: {
          trainingClassId: classId,
          trainingEventId: eventId,
          pausedStepId: stepId,
          duration: currentDuration,
        },
      }),
      Community.pauseEventClass(),
    );
    if (res.success) {
      yield put(CommunityActions.updateEventClassDetail(res.data));
      Actions.popTo('CommunityDetailClassDetailScreen');
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* stashStepEventClass({ classId, eventId, stepId }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { trainingSteps } = yield select((state) => state.player.detail);
    if (!stepId && isArray(trainingSteps)) {
      stepId = first(trainingSteps).id;
    }
    yield call(
      Handler.put({
        Authorization: token,
        data: {
          trainingClassId: classId,
          trainingEventId: eventId,
          pausedStepId: stepId,
        },
      }),
      Community.stashStepEventClass(),
    );
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getEventLeaderBoard({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Community.getDetailLeaderBoard({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getEventLeaderBoardSuccess(res.data.items));
      const routeName = yield select((state) => state.appRoute.routeName);
      if (routeName.includes('Notification')) {
        Actions.NotificationLeaderBoardScreen({ hideTabBar: true });
      } else {
        Actions.LeaderBoardScreen({ hideTabBar: true });
      }
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* deleteTrainingEvent({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      Community.deleteTrainingEvent({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getList());
      Actions.pop();
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* acceptTrainingEvent({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({ Authorization: token }),
      Community.participateEvent({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.joinEventSuccess(id));
      yield put(CommunityActions.onlyGetTrainingEvent(id));
    }
  } catch (error) {
    console.log('error.status =>', error.status);
    if (error.status === 500) {
      Alert.alert(
        t('community_detail_full_booked_title'),
        t('community_detail_full_booked_content'),
        [
          {
            text: t('__ok'),
            onPress: () => console.log('joinEvent full book'),
            style: 'cancel',
          },
        ],
      );
      yield put(CommunityActions.onlyGetTrainingEvent(id));
    }
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* rejectTrainingEvent({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      Community.rejectTrainingEvent({ id }),
    );
    if (res.success) {
      yield put(CommunityActions.getList());
      Actions.pop();
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getSearchEventClassDetail({ id, disabled }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Class.getClassDetail({ id }),
    );
    if (res.success) {
      yield put(ClassActions.getClassDetailSuccess(res.data.trainingClass));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
          classId: id,
          classType: 'on-demand',
        }),
      );
      Actions.CommunityDetailClassDetailScreen({
        isOnlySee: true,
        isChooseClass: true,
        isSearchCreate: true,
        isClassSelectable: !disabled,
      });
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getEventClassDetail({ id }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Class.getClassDetail({ id }),
    );
    if (res.success) {
      yield put(ClassActions.getClassDetailSuccess(res.data.trainingClass));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
          classId: id,
          classType: 'on-demand',
        }),
      );
      Actions.CommunityDetailClassDetailScreen({ isOnlySee: true, isChooseClass: true });
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* fetchPut1on1OnlineStatus({ trainingEventId, status, onSuccess }) {
  yield put(AppStateActions.onLoading(true, null, { hide: true }));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
      }),
      Community.put1on1OnlineStatus({ trainingEventId, status }),
    );
    if (res.success) {
      if (typeof onSuccess === 'function') {
        yield call(onSuccess);
      }
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* fetchGet1on1OnlineStatus({ trainingEventId }) {
  yield put(AppStateActions.onLoading(true, null, { hide: true }));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Community.get1on1OnlineStatus({ trainingEventId }),
    );
    if (res.success) {
      yield put(CommunityActions.update1on1OnlineStatus(res.data.isReady));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* fetchPost1on1Notification({ trainingEventId }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Community.postSend1on1Notification({ trainingEventId }),
    );
    if (res.success) {
      yield call(Dialog.send1on1NotificationSuccessAlert);
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* sync1on1StartTime({ trainingEventId }) {
  try {
    yield call(fetchPut1on1OnlineStatus, { trainingEventId, status: 'readyToGo' });
    yield delay(1000);
    yield put(AppStateActions.onLoading(true));
    while (true) {
      const classStartAt = yield select((state) => state.community.classStartAt);
      if (classStartAt) {
        break;
      } else {
        yield call(fetchGet1on1SyncedStartTime, { trainingEventId });
      }
      yield delay(3000);
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* fetchGet1on1SyncedStartTime({ trainingEventId }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({
        Authorization: token,
      }),
      Community.getMeetingSession({ trainingEventId }),
    );
    console.log('fetchGet1on1SyncedStartTime res=>', res.data.meetingSession);
    if (res.success && res.data.meetingSession) {
      yield put(
        CommunityActions.update1on1StartTime(res.data.meetingSession.classStartAt),
      );
    }
  } catch (error) {
    console.log(error);
  }
}
