import { put, call, select } from 'redux-saga/effects';
import { Actions } from 'react-native-router-flux';
import { SearchActions, AppStateActions, SeeAllActions } from 'App/Stores';
import { Handler, Search } from 'App/Api';
export function* getInstructorList() {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Search.getInstructorList(),
    );

    if (res.success) {
      yield put(SearchActions.getInstructorListSuccess(res.data.instructors));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getEquipmentList() {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Search.getEquipmentList(),
    );

    if (res.success) {
      yield put(SearchActions.getEquipmentListSuccess(res.data.trainingEquipments));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getWorkoutTypeList() {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Search.getWorkoutType(),
    );

    if (res.success) {
      yield put(SearchActions.getWorkoutTypeListSuccess(res.data.genres));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* search({ payload, page, showLoadingIndicator }) {
  if (showLoadingIndicator) {
    yield put(AppStateActions.onLoading(true));
  }
  try {
    const token = yield select((state) => state.user.token);
    const locate = yield select((state) => state.appState.currentLocales[0].languageTag);

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
        header: { 'Accept-Language': locate },
        data: payload,
      }),
      Search.search(page.page, page.pageSize),
    );

    if (res.success) {
      yield put(SearchActions.setSearchData(payload));

      if (res.data.total === 0 && page.page !== 1) {
        const total = yield select((state) => state.seeAll.total);
        yield put(SeeAllActions.setPage(page.page, page.pageSize, total));
      } else {
        yield put(SeeAllActions.setPage(page.page, page.pageSize, res.data.total));
      }

      yield put(SeeAllActions.setClassList(res.data.trainingClasses));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* searchFilter({ payload }) {
  try {
    const token = yield select((state) => state.user.token);
    const locate = yield select((state) => state.appState.currentLocales[0].languageTag);

    yield put(SeeAllActions.loadingFilter());

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
        header: { 'Accept-Language': locate },
        data: payload,
      }),
      Search.searchFilter(),
    );

    if (res.success) {
      yield put(SeeAllActions.setClassListFilter(res.data.filter));
    }
  } catch (err) {
    console.log(err);
  } finally {
  }
}

export function* getSearchHotKeyword({}) {
  try {
    const token = yield select((state) => state.user.token);
    const locate = yield select((state) => state.appState.currentLocales[0].languageTag);

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
        header: { 'Accept-Language': locate },
      }),
      Search.searchHotKeyword(),
    );

    if (res.success) {
      yield put(SearchActions.getSearchHotKeywordSuccess(res.data.keywords));
    }
  } catch (err) {
    console.error(err);
  } finally {
  }
}

export function* communitySearch({ payload, page, pageSize, isEvent, onSuccessCb }) {
  try {
    const token = yield select((state) => state.user.token);
    const locate = yield select((state) => state.appState.currentLocales[0].languageTag);
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
        header: { 'Accept-Language': locate },
        data: {
          ...payload,
          page,
        },
      }),
      Search.communitySearch(page, pageSize, isEvent),
    );

    if (res.success) {
      yield put(
        SearchActions.communitySearchSuccess(res.data.trainingClasses, res.data.total),
      );
      if (typeof onSuccessCb === 'function') {
        yield call(onSuccessCb);
      }
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getSearchableData() {
  const token = yield select((state) => state.user.token);
  try {
    const { data: genreRes } = yield call(Handler.get({}), Search.getWorkoutType());
    const workoutTypeList = genreRes.data.genres || [];
    const { data: tagRes } = yield call(Handler.get({}), Search.getPopularTags());
    const popularTagList = tagRes.data.tags || [];
    let channelList = [];
    if (token != null && token != '') {
      const { data: channelRes } = yield call(
        Handler.get({ Authorization: token }),
        Search.getChannels(),
      );
      channelList = channelRes.data.items || [];
    }
    const { data: equipmentRes } = yield call(Handler.get({}), Search.getEquipmentList());
    const equipmentList = equipmentRes.data.trainingEquipments || [];
    const { data: instructorRes } = yield call(
      Handler.get({}),
      Search.getInstructorList(),
    );
    const instructorList = instructorRes.data.instructors || [];

    yield put(
      SearchActions.getSearchableDataSuccess({
        channelList,
        equipmentList,
        popularTagList,
        instructorList,
        workoutTypeList,
      }),
    );
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}
