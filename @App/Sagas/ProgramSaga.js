import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { put, call, select } from 'redux-saga/effects';
import { get, has, first, isArray } from 'lodash';

import {
  ProgramActions,
  ClassActions,
  UserActions,
  AppStateActions,
  PlayerActions,
  CollectionActions,
  MirrorActions,
} from 'App/Stores';
import { MirrorEvents } from 'App/Stores/Mirror/Actions';
import { Handler, Program, Class } from 'App/Api';

import { uploadTrainingRecords } from 'App/Sagas/Mirror/MirrorCalorieStatusSaga';
export function* getProgramList({}) {
  try {
    const token = yield select((state) => state.user.token);
    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Program.getProgramList(),
    );

    if (res.success) {
      yield put(ProgramActions.getProgramListSuccess(res.data.trainingPrograms));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getProgramDetail({ id, token, showDetail }) {
  yield put(AppStateActions.onLoading(true));
  try {
    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Program.getProgramDetail({ id }),
    );

    if (res.success) {
      yield put(ProgramActions.getProgramDetailSuccess(res.data.trainingProgram));

      const routeName = yield select((state) => state.appRoute.routeName);

      if (showDetail && routeName === 'Program') {
        Actions.ProgramDetail();
      }
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyGetProgramDetail({ id, token }) {
  try {
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.get({ Authorization: token }),
      Program.getProgramDetail({ id }),
    );

    if (res.success) {
      yield put(ProgramActions.getProgramDetailSuccess(res.data.trainingProgram));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* getProgramClassDetail({
  id,
  token,
  trainingProgramClassHistory,
  isFinished,
  historyId,
  routeName,
}) {
  try {
    yield put(AppStateActions.onLoading(true));
    let res = null;
    if (trainingProgramClassHistory !== null) {
      res = yield call(
        Handler.get({ Authorization: token }),
        Program.getProgramClassDetail({ id, trainingProgramClassHistory }),
      );
    } else {
      res = yield call(
        Handler.get({ Authorization: token }),
        Program.seeProgramClassDetail({ id }),
      );
    }

    if (res.data.success) {
      yield put(ClassActions.getClassDetailSuccess(res.data.data.trainingClass));
      yield put(
        UserActions.setTrainingProgramClassHistoryId(trainingProgramClassHistory),
      );
      yield put(PlayerActions.setRouteName('ProgramClassDetail'));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.CLASS_PREVIEW, {
          classId: id,
          classType: 'on-demand',
          programClassHistoryId: trainingProgramClassHistory,
        }),
      );

      const currentRoute = yield select((state) => state.appRoute.routeName);

      if (currentRoute === 'Program' || currentRoute === 'ProgramDetail') {
        Actions.ProgramDetailStack();
      }
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyGetProgramClassDetail({
  id,
  routeName,
  trainingProgramClassHistory,
}) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    let res = null;
    if (trainingProgramClassHistory !== null) {
      res = yield call(
        Handler.get({ Authorization: token }),
        Program.getProgramClassDetail({ id, trainingProgramClassHistory }),
      );
    } else {
      res = yield call(
        Handler.get({ Authorization: token }),
        Program.seeProgramClassDetail({ id }),
      );
    }

    if (res.data.success) {
      console.log('onlyGetProgramClassDetail res.data =>', res.data.data);
      yield put(ClassActions.getClassDetailSuccess(res.data.data.trainingClass));
      yield put(
        UserActions.setTrainingProgramClassHistoryId(trainingProgramClassHistory),
      );
      yield put(PlayerActions.setRouteName(routeName));
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* onlyClassSummaryGetProgramClassDetail({
  id,
  routeName,
  trainingProgramClassHistory,
}) {
  try {
    console.log('id =>', id);
    console.log('trainingProgramClassHistory =>', trainingProgramClassHistory);
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    let res = null;
    if (trainingProgramClassHistory !== null) {
      res = yield call(
        Handler.get({ Authorization: token }),
        Program.getProgramClassDetail({ id, trainingProgramClassHistory }),
      );
    } else {
      res = yield call(
        Handler.get({ Authorization: token }),
        Program.seeProgramClassDetail({ id }),
      );
    }

    if (res.data.success) {
      yield put(ClassActions.getClassDetailSuccess(res.data.data.trainingClass));
      yield put(
        UserActions.setTrainingProgramClassHistoryId(trainingProgramClassHistory),
      );
      yield put(PlayerActions.setRouteName(routeName));

      if (has(res.data.trainingProgram, 'trainingClassHistory')) {
        const trainingProgramClassHistoryId = yield select(
          (state) => state.user.trainingProgramClassHistoryId,
        );
        const body = {
          trainingProgramClassHistoryId: trainingProgramClassHistoryId,
          trainingProgramHistoryId:
            res.data.data.trainingClass.trainingProgramClassHistory
              .trainingProgramHistoryId,
        };
        yield put(ProgramActions.finishProgramClass(id, token, body));
      }
    }
  } catch (error) {
    console.log(error);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* startProgram({ id, token }) {
  try {
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.post({ Authorization: token }),
      Program.startProgram({ id }),
    );

    if (res.success) {
      yield put(UserActions.setProgramId(res.data.trainingProgramHistory));
      Actions.Program();
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* leaveProgram({ id, token }) {
  try {
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.delete({ Authorization: token }),
      Program.leaveProgram({ id }),
    );

    if (res.success) {
      yield put(ProgramActions.getProgramList(token));
      yield put(UserActions.resetProgramId());
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* startProgramClass({ id, token, detailId, options }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const { data: res } = yield call(
      Handler.put({ Authorization: token, data: { trainingProgramClassHistoryId: id } }),
      Program.startProgramClass(),
    );
    if (res.success) {
      yield put(ClassActions.startClassSuccess(res.data));
      yield put(PlayerActions.setRouteName('ProgramClassDetail'));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.START_CLASS, {
          classId: detailId,
          programClassHistoryId: id,
          ...options,
        }),
      );
    }
  } catch (err) {
    console.log(err);
    yield put(AppStateActions.onLoading(false));
  } finally {
    if (!get(options, 'start-measure-distance')) {
      yield put(AppStateActions.onLoading(false));
    }
  }
}

export function* pauseProgramClass({
  id,
  token,
  stepId,
  trainingProgramScheduleId,
  isProgramFinished,
  detailId,
}) {
  yield put(AppStateActions.onLoading(true));
  try {
    const currentDuration = yield select((state) => state.mirror.currentDuration);

    const { trainingSteps } = yield select((state) => state.player.detail);
    if (!stepId && isArray(trainingSteps)) {
      stepId = first(trainingSteps).id;
    }

    const { data: res } = yield call(
      Handler.put({
        Authorization: token,
        data: {
          trainingProgramClassHistoryId: id,
          pausedStepId: stepId,
          duration: currentDuration,
        },
      }),
      Program.pauseProgramClass(),
    );
    if (res.success) {
      yield put(ClassActions.startClassSuccess(res.data));
      yield put(ProgramActions.setProgramClassHistory(res.data.programClassHistory));
      Actions.ProgramClassDetailScreen();
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* resumeProgramClass({ id, token, routeName, detailId, options }) {
  try {
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.put({ Authorization: token, data: { trainingProgramClassHistoryId: id } }),
      Program.resumeProgramClass(),
    );
    if (res.success) {
      yield put(ClassActions.startClassSuccess(res.data));
      yield put(
        MirrorActions.onMirrorCommunicate(MirrorEvents.START_CLASS, {
          classId: detailId,
          programClassHistoryId: id,
          ...options,
        }),
      );
    }
  } catch (err) {
    console.log(err);
  } finally {
    if (!get(options, 'start-measure-distance')) {
      yield put(AppStateActions.onLoading(false));
    }
  }
}

export function* finishProgramClass({ id, token, body, activeTrainingProgramId }) {
  try {
    yield put(AppStateActions.onLoading(true));

    const { data: res } = yield call(
      Handler.put({ Authorization: token, data: body }),
      Program.finishProgramClass(),
    );

    if (res.success) {
      console.log('finishProgramClass res.data ->', res.data);
      yield put(ClassActions.startClassSuccess(res.data));
      yield put(
        ProgramActions.setDetailTrainingProgram(
          res.data.trainingClassHistory,
          res.data.programClassHistory,
        ),
      );
      yield put(ProgramActions.finishProgramClassSuccess(res.data));
      yield put(
        ProgramActions.onlyGetProgramClassDetail(
          id,
          'ProgramClassDetail',
          res.data.programClassHistory.id,
        ),
      );
      yield put(MirrorActions.resetThumbnail());
      yield put(MirrorActions.onMirrorCommunicate(MirrorEvents.DISPLAY_SUMMARY));
      yield call(requestAnimationFrame, Actions.ProgramSaveActivityScreen);
    }
  } catch (err) {
    const msg = err.message ? JSON.stringify(err.message) : JSON.stringify(err);
    Alert.alert('Warning', msg);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* stashProgramStepId({ trainingProgramClassHistoryId, stepId }) {
  try {
    yield put(AppStateActions.onLoading(true, null, { hide: true }));
    const token = yield select((state) => state.user.token);

    yield call(
      Handler.put({
        Authorization: token,
        data: {
          trainingProgramClassHistoryId: trainingProgramClassHistoryId,
          pausedStepId: stepId,
        },
      }),
      Program.stashProgramStepId(),
    );
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* setProgramBookmark({ id }) {
  try {
    yield put(AppStateActions.onLoading(true, null, { hide: true }));
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.post({
        Authorization: token,
      }),
      Class.setBookmark({ id }),
    );

    if (res.success) {
      yield put(ProgramActions.setProgramBookmarkSuccess(id));
      yield put(ClassActions.setClassBookmarkSuccess(id));
      yield put(CollectionActions.setCollectionBookmarkSuccess(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* removeProgramBookmark({ id }) {
  try {
    const token = yield select((state) => state.user.token);

    const { data: res } = yield call(
      Handler.delete({
        Authorization: token,
      }),
      Class.deleteBookmark({ id }),
    );

    if (res.success) {
      yield put(ProgramActions.removeProgramBookmarkSuccess(id));
      yield put(ClassActions.removeClassBookmarkSuccess(id));
      yield put(CollectionActions.removeCollectionBookmarkSuccess(id));
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}

export function* programSubmitFeedback({ id, payload, isProgramFinished }) {
  try {
    yield put(AppStateActions.onLoading(true));
    const token = yield select((state) => state.user.token);
    const programId = yield select((state) => state.program.detail.id);
    const { data: res } = yield call(
      Handler.post({ Authorization: token, data: payload }),
      Class.submitFeedback({ id }),
    );
    if (res.success) {
      if (isProgramFinished) {
        yield put(ProgramActions.resetIsFinishedProgram());
        yield put(ProgramActions.getProgramList(token));

        Actions.ProgramAchievementScreen();
      } else {
        yield put(ProgramActions.onlyGetProgramDetail(programId, token));
        Actions.ProgramClassDetailScreen();
      }
    }
  } catch (err) {
    console.log(err);
  } finally {
    yield put(AppStateActions.onLoading(false));
  }
}
