import { version } from './version';

export const Config = {
  API_BASE_URL: 'https://jsonplaceholder.typicode.com',
  API_VER: '',
  VERSION: version,
};
