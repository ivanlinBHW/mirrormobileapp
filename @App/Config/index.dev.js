import { version } from './version';

export const Config = {
  APP_VERSION: version,
  API_TIMEOUT: 15 * 1000,
  API_BASE_URL: 'http://johnson-cloud-server.jx-staging-dev.k8s.trunksys.com',
  API_VERSION: '',
  TIMEOUT: 3000,
  SPOTIFY_CLIENT_ID: '',
  SPOTIFY_CLIENT_SECRET: '',
  SPOTIFY_SCOPES: [
    'streaming',
    'user-read-private',
    'playlist-read',
    'playlist-read-private',
  ],
  SPOTIFY_REDIRECT_URL:
    'http://johnson-cloud-server.jx-staging-dev.k8s.trunksys.com/api/spotify/callback',
  SPOTIFY_TOKEN_SWAP_URL:
    'http://johnson-cloud-server.jx-staging-dev.k8s.trunksys.com/api/spotify/swap-token',
  SPOTIFY_TOKEN_REFRESH_URL:
    'http://johnson-cloud-server.jx-staging-dev.k8s.trunksys.com/api/spotify/refresh-token',
  SPOTIFY_TOKEN_REFRESH_EARLINESS: 300,
};
