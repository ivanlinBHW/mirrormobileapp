import VersionNumber from 'react-native-version-number';

export const Config = {
  APP_VERSION: VersionNumber.appVersion,
  BUILD_VERSION: VersionNumber.buildVersion,
  CHECK_VERSION: true,
  PRODUCT_NAME: 'Johnson@mirror',
  API_TIMEOUT: 65 * 1000,
  API_VERSION: 'v1.6',
  TIMEOUT: 3000,
  SHOP_LINK: 'https://www.johnsonfitnesslive.com/',
  IS_ENABLE_AUTO_REGION_SWITCH: false,

  DEFAULT_SERVER_ENV: 'TW',
  API_BASE_URL: {
    TW: 'https://mirror-tw.johnsonfitnesslive.com',
    STAGING: 'https://mirror-staging.johnsonfitnesslive.com',
    DEV: 'https://johnson-cloud-server.jx-staging-dev.k8s.trunksys.com',
    LOCAL: 'http://172.20.10.2:8000',
    BULLETIN: 'https://johnsonfitness-mirror-tw-s3.s3-ap-northeast-1.amazonaws.com',
    TEST: 'http://d3ks2kbxo5xpkd.cloudfront.net',
  },
  SPOTIFY_CHECK_PERIOD: 55 * 60 * 1000,
  SPOTIFY_CLIENT_ID: '0e8d622a15c043f7a4c6006d2c135140',
  SPOTIFY_CLIENT_SECRET: '',
  SPOTIFY_SCOPES: [
    'streaming',
    'user-read-private',
    'playlist-read',
    'playlist-read-private',
  ],
  SPOTIFY_REDIRECT_URL: 'https://mirror-tw.johnsonfitnesslive.com/api/spotify/callback',
  SPOTIFY_TOKEN_SWAP_URL: 'https://accounts.spotify.com/api/token',
  SPOTIFY_TOKEN_REFRESH_URL: 'https://accounts.spotify.com/api/token',
  WEBSOCKET_MODE_PREFIX: 'wss',
  WEBSOCKET_SSL_CA:
    '-----BEGIN CERTIFICATE-----\nMIIDfzCCAmegAwIBAgIEIPUusjANBgkqhkiG9w0BAQsFADBwMRIwEAYDVQQGEwlN\neUNvdW50cnkxETAPBgNVBAgTCE15UmVnaW9uMQ8wDQYDVQQHEwZNeUNpdHkxDjAM\nBgNVBAoTBU15T3JnMRIwEAYDVQQLEwlNeU9yZ1VuaXQxEjAQBgNVBAMTCTEyNy4w\nLjAuMTAeFw0xOTEyMTAxMTM1NDRaFw0yOTEyMDcxMTM1NDRaMHAxEjAQBgNVBAYT\nCU15Q291bnRyeTERMA8GA1UECBMITXlSZWdpb24xDzANBgNVBAcTBk15Q2l0eTEO\nMAwGA1UEChMFTXlPcmcxEjAQBgNVBAsTCU15T3JnVW5pdDESMBAGA1UEAxMJMTI3\nLjAuMC4xMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzooiQlTzbjaL\npL1/ceWhaBXzW639bgpDkLhgyU6FGnH/BIz4OT7bLyASw9vVy8czys1kjRCdnZl/\nqZb/mZIiunb1EorGvGgjskGHqfUmnQyNtcNwYiyiauen3dbywdch59lvPl5XMnfr\nCX6orY8zp4r+jfEyorbHpxcvXGQZVxHHwSUfoD5AbZ7UDmd876yJK3fNl8vferJz\nt+GYMaXhwNyOrsvsbron9jtn7gV8AD2gqFjN8+ZH1FTUgNrKZ/tvmGQA6eCAJyMf\nIKu4ChKz3CJbEfgnKdO1l9LRDQBSsM9b7HIMaCfBWw6RStJWnUiacTM5Q3bgncEB\nC7ZlnqEH7wIDAQABoyEwHzAdBgNVHQ4EFgQUCeTrf63tLs6gWepYYXVBzpwN+EMw\nDQYJKoZIhvcNAQELBQADggEBAAp0vwjtkN1MvzZaQm9KlSo6UGOKF4KiEcf/8nSG\nx4kuBf6mi60J0dMhdwIHSfpAM+OybEd9ZX4HCuygt6FgEuQSIdu1lZbO7aNpbrKS\neRmZCg7972QrpZoX0qZBJSpFPejUhzDcphPnTQVJNwbcZMiRZ47qWTwCPjde/1xR\nVZ1udXuwr0h12eIQRq1zroltAthUVTGvngmdNd0yU28wv3dlccMgtyYEp3LLGtb3\nO6J8YJ0LqWebCnUVjD+UDRLP1xFtnHMDKb0P9Ho2GE1XUjerzw77EEpWbhRJL92F\nhrjAUCthrk42tj1aF7A1Ts/JEjQrVPcxyTYqy96L/5XWl7k=\n-----END CERTIFICATE-----\n',
  WEBSOCKET_SSL_PFX: '',
  WEBSOCKET_SSL_PASSPHRASE: '',
  CHANNEL_ID: 'push-notification-channel',
  CHANNEL_NAME: 'Mirror channel',
};
