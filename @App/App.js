import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import {
  setCustomView,
  setCustomText,
  setCustomImage,
  setCustomTextInput,
  setCustomTouchableOpacity,
} from 'react-native-global-props';

import { Global } from 'App/Theme';
import createStore from 'App/Stores';
import SplashScreen from 'App/Containers/Splash/SplashScreen';
import AppNavigator from 'App/Navigators/AppNavigator';
import { Text, TextInput, Alert } from 'react-native';
export const { store, persistor } = createStore();

export default class App extends Component {
  constructor(props) {
    super(props);
    setCustomView(Global.ViewProps);
    setCustomText(Global.TextProps);
    setCustomImage(Global.ImageProps);
    setCustomTextInput(Global.TextInputProps);
    setCustomTouchableOpacity(Global.TouchableOpacityProps);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;    

    
    __DEV__ && console.log('@Apply global theme!');
  }
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<SplashScreen />} persistor={persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
    );
  }
}
