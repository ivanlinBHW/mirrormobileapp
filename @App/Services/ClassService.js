import axios from 'axios';
import { Config } from 'App/Config';
function getClassList({ token }) {
  console.log('service token =>', token);
  const getAllClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getAllClass.get('/api/client/home').then((response) => {
    return response.data;
  });
}

async function getClassDetail(id, token) {
  const getDetailClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return (await getDetailClass.get(`/api/client/training-class/${id}`)).data;
}

async function submitFeedback(id, token, payload) {
  const submitFeedbackApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });
  try {
    return (await submitFeedbackApi.post(
      `/api/client/training-class/${id}/feedback`,
      payload,
    )).data;
  } catch (err) {
    console.log('err +>', err);
    return err;
  }
}

function startClass(id, token) {
  const startClassApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    timeout: Config.TIMEOUT,
  });

  return startClassApi.post(`/api/client/training-class/${id}/start`).then((response) => {
    return response.data;
  });
}

function finishClass(id, token) {
  const startClassApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    timeout: Config.TIMEOUT,
  });

  return startClassApi.put(`/api/client/training-class/${id}/finish`).then((response) => {
    return response.data;
  });
}

function pauseClass(id, token, stepId) {
  const pauseClassApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    timeout: Config.TIMEOUT,
  });

  console.log('stepId =>', stepId);

  return pauseClassApi
    .put(`/api/client/training-class/${id}/pause`, { pausedStepId: stepId })
    .then((response) => {
      return response.data;
    });
}

export const ClassService = {
  getClassList,
  getClassDetail,
  submitFeedback,
  startClass,
  pauseClass,
  finishClass,
};
