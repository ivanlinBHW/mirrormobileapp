import axios from 'axios';
import { Config } from 'App/Config';
function getLiveList({ token }) {
  const getAllLive = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getAllLive.post('/api/client/live-class/index').then((response) => {
    return response.data;
  });
}

function getLiveDetail(id, token) {
  const getDetailLive = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getDetailLive.get(`/api/client/live-class/${id}`).then((response) => {
    return response.data;
  });
}

function getLiveDate(params, token) {
  const getDetailLive = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getDetailLive
    .get(`/api/client/live-class/status?start=${params.start}&end=${params.end}`)
    .then((response) => {
      return response.data;
    });
}

function getLiveDateDetail(day, token) {
  const getDetailLive = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getDetailLive
    .get(`/api/client/live-class/day?start=${day} 00:00:00&end=${day}12:00:00`)
    .then((response) => {
      return response.data;
    });
}

function submitFeedback({ id, token, payload }) {
  const submitFeedbackApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    timeout: Config.TIMEOUT,
  });

  return submitFeedbackApi
    .post(`/api/client/live-class/${id}/feedback`, payload)
    .then((response) => {
      return response.data;
    });
}

function joinLive(id, token) {
  console.log('joinLive id =>', id);
  console.log('joinLive token =>', token);
  const getDetailLive = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getDetailLive.post(`/api/client/live-class/${id}/join`).then((response) => {
    return response.data;
  });
}

function bookLive(id, token) {
  const getDetailLive = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });
  console.log('id=> ', id);
  console.log('token=> ', token);

  return getDetailLive.post(`/api/client/live-class/${id}/book`).then((response) => {
    return response.data;
  });
}

function finishClass(id, token) {
  const startClassApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    timeout: Config.TIMEOUT,
  });

  return startClassApi.put(`/api/client/live-class/${id}/finish`).then((response) => {
    return response.data;
  });
}

export const LiveService = {
  getLiveList,
  getLiveDetail,
  getLiveDate,
  getLiveDateDetail,
  joinLive,
  bookLive,
  submitFeedback,
  finishClass,
};
