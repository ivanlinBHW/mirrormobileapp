import axios from 'axios';
import { Config } from 'App/Config';
function getInstructorList(token) {
  const getInstructor = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getInstructor.get('/api/client/instructor').then((response) => {
    return response.data;
  });
}

function getEquipmentList(token) {
  const getEquipment = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getEquipment.get('/api/client/training-equipment').then((response) => {
    return response.data;
  });
}

async function search(payload, token) {
  const getEquipment = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return (await getEquipment.post('/api/client/search', payload)).data;
}

export const SearchService = {
  getInstructorList,
  getEquipmentList,
  search,
};
