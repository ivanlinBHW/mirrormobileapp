import axios from 'axios';
import { Config } from 'App/Config';
import { is, curryN, gte } from 'ramda';

const isWithin = curryN(3, (min, max, value) => {
  const isNumber = is(Number);
  return (
    isNumber(min) &&
    isNumber(max) &&
    isNumber(value) &&
    gte(value, min) &&
    gte(max, value)
  );
});
const in200s = isWithin(200, 299);
const userApiClient = axios.create({
  baseURL: Config.API_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: Config.TIMEOUT,
});

function fetchUser() {
  if (Math.random() > 0.5) {
    return new Promise(function(resolve, reject) {
      resolve(null);
    });
  }

  let number = Math.floor(Math.random() / 0.1) + 1;

  return userApiClient.get(number.toString()).then((response) => {
    if (in200s(response.status)) {
      return response.data;
    }

    return null;
  });
}

function fetchUserLogin(payload) {
  console.log('fetch login');

  return axios
    .post(`${Config.API_ENDPOINT}/api/admin/account/signin`, payload, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then((response) => {
      if (in200s(response.status)) {
        return response.data;
      }
      return null;
    });
}

export const userService = {
  fetchUser,
  fetchUserLogin,
};
