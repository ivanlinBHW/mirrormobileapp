import axios from 'axios';
import { Config } from 'App/Config';
function getAchievements({ token }) {
  console.log('getAchievements token=>', token);
  const getAllClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getAllClass.get(`api/client/user/achievements`).then((response) => {
    return response.data;
  });
}

function getProgressOverview({ token, start, end }) {
  console.log('getProgressOverview token, start, end=>', token, start, end);
  const getAllClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getAllClass
    .get(`/api/client/progress/period/detail?start=${start}&end=${end}`)
    .then((response) => {
      return response.data;
    });
}

function getProgressDate({ token, start, end }) {
  console.log('getProgressDate=>', start, end);
  const getAllClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getAllClass
    .get(`/api/client/progress/period/status?start=${start}&end=${end}`)
    .then((response) => {
      return response.data;
    });
}

async function getProgressDetail({ token, id, workoutType }) {
  const getDetailClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });
  const apiUrl = `api/client/${workoutType}/history/${id}`;
  return (await getDetailClass.get(apiUrl)).data;
}

function setWorkoutDetailBookmark({ token, id }) {
  const getDetailClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });
  const apiUrl = `api/client/training-class/${id}/bookmark`;

  return getDetailClass.post(apiUrl).then((response) => {
    return response.data;
  });
}

function deleteWorkoutDetailBookmark({ token, id }) {
  const getDetailClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });
  const apiUrl = `api/client/training-class/${id}/bookmark`;

  return getDetailClass.delete(apiUrl).then((response) => {
    return response.data;
  });
}

export default {
  getAchievements,
  getProgressDate,
  getProgressDetail,
  getProgressOverview,
  setWorkoutDetailBookmark,
  deleteWorkoutDetailBookmark,
};
