import axios from 'axios';
import { Config } from 'App/Config';
function getCollectionList({ token }) {
  console.log('Collection service token =>', token);
  const getAllClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getAllClass.post('/api/client/training-collection/index').then((response) => {
    return response.data;
  });
}

function getCollectionDetail({ id, token }) {
  console.log('Collection service id =>', id);
  console.log('Collection service token =>', token);
  const getDetailClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getDetailClass.get(`/api/client/training-collection/${id}`).then((response) => {
    return response.data;
  });
}

export const CollectionService = {
  getCollectionList,
  getCollectionDetail,
};
