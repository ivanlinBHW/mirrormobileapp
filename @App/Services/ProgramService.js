import axios from 'axios';
import { Config } from 'App/Config';
function getProgramList(token) {
  console.log('program service token =>', token);
  const getAllClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return getAllClass.post('/api/client/training-program/index').then((response) => {
    return response.data;
  });
}

async function getProgramDetail(id, token) {
  const getDetailClass = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  console.log('id =>', id);
  console.log('token =>', token);

  return (await getDetailClass.get(`/api/client/training-program/${id}`)).data;
}

async function startProgram(id, token) {
  const startProgramApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return (await startProgramApi.post(`/api/client/training-program/${id}/start`)).data;
}

async function finishProgramClass(id, token, body) {
  const startClassApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });
  try {
    const res = await startClassApi.put(`/api/client/training-class/${id}/finish`, body);
    return res.data;
  } catch (e) {
    throw e;
  }
}

function leaveProgram(id, token) {
  const startProgramApi = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  return startProgramApi
    .delete(`/api/client/training-program/${id}/leave`)
    .then((response) => {
      return response.data;
    });
}

export const ProgramService = {
  getProgramList,
  getProgramDetail,
  startProgram,
  leaveProgram,
  finishProgramClass,
};
