import axios from 'axios';
import { Config } from 'App/Config';
function getInstructor(id, token) {
  const getInstructorService = axios.create({
    baseURL: Config.API_ENDPOINT,
    headers: {
      Authorization: 'Bearer ' + token,
    },
    timeout: Config.TIMEOUT,
  });

  console.log('id =>', id);
  console.log('token =>', token);

  return getInstructorService.get(`/api/client/instructor/${id}`).then((response) => {
    return response.data;
  });
}

export const InstructorService = {
  getInstructor,
};
